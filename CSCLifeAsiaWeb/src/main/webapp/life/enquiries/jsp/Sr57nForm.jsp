

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "Sr57n";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>

<%Sr57nScreenVars sv = (Sr57nScreenVars) fw.getVariables();%>


	<%StringData generatedText14 = new StringData("------------------------- Surrender Details ----------------------------------");%>
	<%StringData generatedText17 = new StringData("Effective Date ");%>
	<%StringData generatedText16 = new StringData("Total     ");%>
	<%StringData generatedText18 = new StringData("Currency       ");%>
	<%StringData generatedText15 = new StringData("Total Fee ");%>
	<%StringData generatedText19 = new StringData("Total Amount   ");%>
	<%StringData generatedText20 = new StringData("Total %        ");%>



	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number   ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life   ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date      ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date    ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------- Coverage/Rider Details------------------------------");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Suffix No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                           Actual           Estimated");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Co/Rd Fund Typ Descriptn     Ccy    %                Value               Value");%>
	
	<%
	sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
	sv.clamant.setEnabled(BaseScreenData.DISABLED);
	sv.currcd.setEnabled(BaseScreenData.DISABLED);
	sv.totalfee.setEnabled(BaseScreenData.DISABLED);
	sv.totalamt.setEnabled(BaseScreenData.DISABLED);
	sv.prcnt.setEnabled(BaseScreenData.DISABLED);
	sv.taxamt.setEnabled(BaseScreenData.DISABLED);	
	%>
	
<%		appVars.rollup(new int[] {93});
%>
<%
if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
		%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
                    </label>
                   <table>
                   <td>
                   <%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
                   </td><td style="padding-left:1px;">
                   	<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                   
                  </td><td style="padding-left:1px;">
                   <%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:160px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                   
                   </td>
                   
                   </table>
                   
                   
                   
                   </div></div>
                     <div class="col-md-4">
                <div class="form-group">
                <!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
                    </label>
                    <table><tr><td>
                    	<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
                    </div></div>
                     <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
                    </label>
                      <table><tr><td>
                      <%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                     </td><td style="padding-left:1px;">
                      <%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                      
                      </td></tr></table>
                    
                    
                    </div></div>
                   
        </div>
                    <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
                    </label>
                   <table>
                   <td>
                   <%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
                   
                 </td><td style="padding-left:1px;">
                   <%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                   
                   
                   </td></table></div></div>
                   
                   </div>
                    <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%>
                    </label>
                   <table>
                   <td>
                   <%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
                   
                   
                  </td><td style="padding-left:1px;">
                   
                   		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                   </td></table></div></div></div>
                    <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%>
                    </label>
                   <table>
                   <td>
                   
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                </td><td style="padding-left:1px;">
                   
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
                   </td></table></div></div></div>
                    <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%>
                    </label>
                   <table>
                   <td>
                   
                   <%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
                   
                   </td></table></div></div>
                   
                   
                    <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%>
                    </label>
                   <table>
                   <td>
                   <%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></table></div></div>
  
                    <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix No")%>
                    </label>
                   <table>
                   <td>
                   <%					
		if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></table></div></div>
                   
                   </div>
                   <br>
                   <div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#surren_tab" data-toggle="tab"><b><%=resourceBundleHandler.gettingValueFromBundle("Surrender Details")%></b></a>
                        </li>
                        <li>
                        	<a href="#cover_tab" data-toggle="tab"><b><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></b></a>
                        </li>
                      
                    </ul>
                    </div>
                    </div>
                    <div class="tab-content">
                    	<div class="tab-pane fade in active" id="surren_tab">
							<div class="row">
								<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
										
								<%	
	longValue = sv.effdateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%} longValue = null;}%>						</div></div>
              			 
              			 	<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
										<table>
										<tr>
										<td>
										
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
										
										</td><td style="padding-left:1px;min-width:85px;">
										<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
										</td>
										</tr>
										</table>
								</div></div>
              			 
              			 <div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
										<table>
										<tr>
										<td>
										<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("currcd");
	optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.currcd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='currcd' type='list' style="width:140px;"
<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.currcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.currcd).getColor())){
%>
</div>
<%
} longValue = null;
%>

<%
} 
%>
</td></tr></table></div></div>
										
              			 
              			 </div>
					<div class="row">
								<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Total Fee")%></label>
										
										<div class="input-group" style="min-width:71px;">
										
		<%	
			qpsf = fw.getFieldXMLDef((sv.totalfee).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalfee,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.totalfee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp;</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
										
										</div></div>
										<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Total Amount")%></label>
										<div class="input-group" style="min-width:71px;">
										
											<%	
			qpsf = fw.getFieldXMLDef((sv.totalamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.totalamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='totalamt' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.totalamt) %>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.totalamt) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totalamt.getLength(), sv.totalamt.getScale(),3)%>'
maxLength='<%= sv.totalamt.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(totalamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.totalamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.totalamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.totalamt).getColor()== null  ? 
			"input_cell" :  (sv.totalamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
										
										</div></div></div>
											
										<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Total %")%></label>
										<div class="input-group" style="min-width:71px;">
											<%	
			qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='prcnt' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.prcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
	 <%}%>

size='<%= sv.prcnt.getLength()%>'
maxLength='<%= sv.prcnt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(prcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event);'

<% 
	if((new Byte((sv.prcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.prcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.prcnt).getColor()== null  ? 
			"input_cell" :  (sv.prcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
										</div>
										</div>
										
										
							</div></div>
							<div class="row">
								<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Tax on Fee")%></label>
										<div class="input-group" style="min-width:71px;">
										<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='prcnt' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
	 <%}%>

size='<%= sv.taxamt.getLength()%>'
maxLength='<%= sv.taxamt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event);'

<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
										
										
										</div></div></div></div></div>
										<div class="tab-pane fade" id="cover_tab">
										<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[9];
int totalTblWidth = 0;
int calculatedValue =0;
			if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.coverage.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*8;								
		} else {		
			calculatedValue = (sv.coverage.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[0]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rider.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
		} else {		
			calculatedValue = (sv.rider.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[1]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.fund.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
		} else {		
			calculatedValue = (sv.fund.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[2]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fieldType.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
		} else {		
			calculatedValue = (sv.fieldType.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[3]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.shortds.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
		} else {		
			calculatedValue = (sv.shortds.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[4]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.cnstcur.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*8;								
		} else {		
			calculatedValue = (sv.cnstcur.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[5]= calculatedValue;
											if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.percreqd.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
					} else {		
						calculatedValue = (sv.percreqd.getFormData()).length()*8;								
					}					
										totalTblWidth += calculatedValue;
	tblColumnWidth[6]= calculatedValue;
											if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.actvalue.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
					} else {		
						calculatedValue = (sv.actvalue.getFormData()).length()*12;								
					}					
										totalTblWidth += calculatedValue;
	tblColumnWidth[7]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.estMatValue.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
		} else {		
			calculatedValue = (sv.estMatValue.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[8]= calculatedValue;
	%>
<%
		GeneralTable sfl = fw.getTable("sr57nscreensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-Sr57n' width='100%'>
							<thead>
							<tr class='info'>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></center></th>
		         								
					<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Rider")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("%")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></center></th>
					
					
					</tr></thead>
					<tbody>
					<% Sr57nscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr57nscreensfl
	.hasMoreScreenRows(sfl)) {	
%>
	<tr id='<%="tablerow"+count%>' >
		<td 
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
									
									
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									
			</td>
				    									<td 
					<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
									
									
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									
			</td>
				    									<td 
					<%if((sv.fund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										
									
									
											
						<%= sv.fund.getFormData()%>
						
														 
				
									
			</td>
				    									<td 
					<%if((sv.fieldType).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										
									
									
											
						<%= sv.fieldType.getFormData()%>
						
														 
				
									
			</td>
				    									<td 
					<%if((sv.shortds).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
									
									
									
											
						<%= sv.shortds.getFormData()%>
						
														 
				
								
			</td>
				    									<td 
					<%if((sv.cnstcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
									
									
									
											
						<%= sv.cnstcur.getFormData()%>
						
														 
				
								
			</td>	
				<td 
				
				
				<%if((sv.percreqd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										
									
									
											
						<%-- <%= sv.percreqd.getFormData()%> --%>
						<%String input=sv.percreqd.getFormData();
											String output = input.trim();
									for( ;output.length() > 1 && output.charAt(0) == '0'; output = output.substring(1)); %>

								<%= output%>		
				
				
			</td>
				    				
			<td  
				    									
		<%if((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
									
					<%
						sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
					%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.actvalue,
 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 		if (!sv.actvalue.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>
 </td>
 
 <td  
				    									
		<%if((sv.estMatValue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
									
					<%
						sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
					%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.estMatValue,
 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 		if (!sv.estMatValue.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>
 </td>
				    									
						

		
		
	</tr>


	<%
	count = count + 1;
	Sr57nscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
					</tbody>
						
						
						</table></div></div></div>
										
										
										</div>
                   
                   
     </div>
     
     </div></div>
     <script>
       $(document).ready(function() {
    	   $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
		        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		    } ); 
              $('#dataTables-Sr57n').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script> 
<%@ include file="/POLACommon2NEW.jsp"%>

