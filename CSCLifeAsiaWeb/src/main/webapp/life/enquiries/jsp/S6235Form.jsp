

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6235";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6235ScreenVars sv = (S6235ScreenVars) fw.getVariables();%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Entity");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel   Sub A/C Code     Sub A/C Type     Current Balance  Currency");%>
<%		appVars.rollup(new int[] {93});
%>

	<div class="panel panel-default">
		
        
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr><td>
								<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' >
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td>
								<td>
						  
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td><td>
					  		
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width:250px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>	
								</td></tr></table>																							
								
					</div>
				</div>
				
				<div class="col-md-4">  </div>
				
				
					<div class="col-md-4">
					<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
									
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						
					    
						  <%--  <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
												"blank_cell" : "output_cell" %>'> 
						   <%=	(sv.register.getFormData()).toString()%>
						   </div> --%>
						   
						   <div style="max-width: 150px" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					   		<%
							longValue = null;
							formatValue = null;
							%>				
				</div>
				</div>
				
				
				 
			</div>
				
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						
						<%					
						if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
										
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
	
				  		
						<%					
						if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
				
					</div>
				</div>	
			
			
			<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							
  		
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>
						
					    
						   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
												"blank_cell" : "output_cell" %>'> 
						   <%=	(sv.cntcurr.getFormData()).toString()%>
						   </div>
						  <%--  <!--
						   
						   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					   		--> --%>
					   		<%
							longValue = null;
							formatValue = null;
							%>			
					</div>
				</div>	
				
				
				
				
			</div>
				
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<table><tr><td >
												  		
								<%					
								if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						
						</td><td>
							
					  		
							<%					
							if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
						
						</td></tr></table>					
					</div>
				</div>
				
				<div class="col-md-4"></div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table><tr><td >
						  		
							<%					
							if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						</td><td >
						
								
							<%					
							if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>					
						</td></tr></table>						
					</div>
				</div>	
			</div>

			<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-s6235" width="100%">
	                         	<thead>
	                            	<tr class='info'>
	                            		<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
	                                	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
	                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
	                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Curr")%></center></th>                                    
	                               </tr>
	                            </thead>
	                            <tbody>
	                            	<%
	                            	GeneralTable sfl = fw.getTable("s6235screensfl");
	                            	S6235screensfl.set1stScreenRow(sfl, appVars, sv);
	                            	int count = 1;
	                            	while (S6235screensfl.hasMoreScreenRows(sfl)) {
									%> 
									<tr>	
										<%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>		
							    		<td>
											<input type="checkbox" 
												 value='<%= sv.select.getFormData() %>' 
												 onFocus='doFocus(this)' onHelp='return fieldHelp("s6235screensfl" + "." +
												 "select")' onKeyUp='return checkMaxLength(this)' 
												 name='s6235screensfl.select_R<%=count%>'
												 id='s6235screensfl.select_R<%=count%>'
												 onClick="selectedRow('s6235screensfl.select_R<%=count%>')"
											/>
										</td>
										<%}else{%>
										<td></td>														
										<%}%>	
														 						 		
										<%if((new Byte((sv.component).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
			    						<td>									
											<%= sv.component.getFormData()%>
										</td>
										<%}else{%>
										<td></td>
										<%}%>
										
										<%if((new Byte((sv.sacscode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
			    						<td>									
											<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("sacscode");
											longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());  
											%>
									
											<%=sv.sacscode.getFormData()%>&nbsp;-&nbsp;<%=longValue%>
											<%
												longValue = null;
												formatValue = null;
											%>
										</td>
										<%}else{%>
										<td></td>
										<%}%>
	
										<%if((new Byte((sv.sacstyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
			    						<td>									
										<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstyp"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("sacstyp");
											longValue = (String) mappedItems.get((sv.sacstyp.getFormData()).toString().trim());  
										%>
										<%=sv.sacstyp.getFormData()%>&nbsp;-&nbsp;<%=longValue%>
										<%
											longValue = null;
											formatValue = null;
										%>
										</td>
										<%}else{%>
										<td></td>
										<%}%>
	
									<%if((new Byte((sv.sacscurbal).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
			    					<td>									
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.sacscurbal).getFieldName());						
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									<%
										formatValue = smartHF.getPicFormatted(qpsf,sv.sacscurbal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.sacscurbal).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
									%>
									</td>
									<%}else{%>
									<td></td>
									<%}%>									
	
									<%if((new Byte((sv.curr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									<td>									
										<%= sv.curr.getFormData()%>
									</td>
									<%}else{%>
									<td></td>
									<%}%>
										
								</tr>		
									<%
									count = count + 1;
									S6235screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>								
	                    	</tbody>
	                	</table>	
                	</div>				
				</div>
			</div>
		</div>
	</div>
	

<script>
$(document).ready(function() {
	$('#dataTables-s6235').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

