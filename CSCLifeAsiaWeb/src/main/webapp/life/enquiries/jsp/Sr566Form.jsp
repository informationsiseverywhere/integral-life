<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR566";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr566ScreenVars sv = (Sr566ScreenVars) fw.getVariables();%>

<%if (sv.Sr566windowWritten.gt(0)) {%>
	<%Sr566window.clearClassString(sv);%>
	<%if (sv.Sr566screenWritten.gt(0)) {%>
		<DIV class='disabledDiv' DISABLED>
	<%}%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 1, generatedText8)%>

	<%if (sv.Sr566screenWritten.gt(0)) {%>
		</DIV>
	<%}%>

<%}%>

<%if (sv.Sr566hideWritten.gt(0)) {%>
	<%Sr566hide.clearClassString(sv);%>
	<%if (sv.Sr566screenWritten.gt(0)) {%>
		<DIV class='disabledDiv' DISABLED>
	<%}%>

	<%
{
	}

	%>

	<%if (sv.Sr566screenWritten.gt(0)) {%>
		</DIV>
	<%}%>

<%}%>

<%if (sv.Sr566protectWritten.gt(0)) {%>
	<%Sr566protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                      <div class="col-md-2">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Installment No")%></label>                       
	                        </div>
	                    </div>
	                     <div class="col-md-2" >
	                     <div class="form-group" style="max-width:110px;text-align: center;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Due Date")%></label>	                       
	                        </div>
	                     </div>
	                   
	                      <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Installment Amount")%></label>	                       
	                        </div>
	                     </div>
	             </div>
	             
	                <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                       <%=smartHF.getHTMLVarExt(fw, sv.mnth01, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>                    
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">	
	                     
	                     
	                      <% if ((new Byte((sv.datedue01Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue01Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue01Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue01Disp, (sv.datedue01Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%>                     
	                        <%--  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue01Disp,(sv.datedue01Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div> --%>	                      
	                     </div>
	                    </div>
	                    <!-- <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                     <%=smartHF.getHTMLVarExt(fw, sv.linstamt01,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>                   
	                        </div>
	                     </div>
	             </div>
	             
	               <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                    <%=smartHF.getHTMLVarExt(fw, sv.mnth02, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>                
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">
	                      <% if ((new Byte((sv.datedue02Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue02Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue02Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue02Disp, (sv.datedue02Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%>   
	                    
	                            <%-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue02Disp,(sv.datedue02Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                	        
	                      </div> --%>
	                     </div>
	                    </div>
	                    <!--  <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                      <%=smartHF.getHTMLVarExt(fw, sv.linstamt02,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>	                       
	                        </div>
	                     </div>
	             </div>
	             
	                  <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                       <%=smartHF.getHTMLVarExt(fw, sv.mnth03, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>               
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">
	                      <% if ((new Byte((sv.datedue03Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue03Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue03Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue03Disp, (sv.datedue03Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%>   
	                     
	                     
	                    
	                           <%--  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue03Disp,(sv.datedue03Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                	        
	                      </div> --%>
	                     </div>
	                    </div>
	                     <!-- <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                     <%=smartHF.getHTMLVarExt(fw, sv.linstamt03,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>	                       
	                        </div>
	                     </div>
	             </div>
	              <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                     <%=smartHF.getHTMLVarExt(fw, sv.mnth04, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>  	                 
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">
	                     
	                      <% if ((new Byte((sv.datedue04Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue04Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue04Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue04Disp, (sv.datedue04Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%> 
	                                          
	                     <%--  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue04Disp,(sv.datedue04Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			              </div>	                    
	                        --%>
	                     </div>
	                    </div>
	                     <!-- <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                       <%=smartHF.getHTMLVarExt(fw, sv.linstamt04,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%> 
	                        </div>
	                     </div>
	             </div>
	             
	              <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                     <%=smartHF.getHTMLVarExt(fw, sv.mnth05, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>   	                   
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">	
	                     
	                      <% if ((new Byte((sv.datedue05Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue05Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue05Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue05Disp, (sv.datedue05Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%> 
	                                          	                      
	                      <%--  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue05Disp,(sv.datedue05Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>			                             
	                        </div> --%>
	                     </div>
	                    </div>
	                     <!-- <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                      <%=smartHF.getHTMLVarExt(fw, sv.linstamt05,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                     </div>
	             </div>
	             
	              <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                        <%=smartHF.getHTMLVarExt(fw, sv.mnth06, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%> 
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">
	                     
	                      <% if ((new Byte((sv.datedue06Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue06Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue06Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue06Disp, (sv.datedue06Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%> 
	                     
	                     <%-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue06Disp,(sv.datedue06Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>			                             
	                        </div> --%>                
	                       
	                     </div>
	                    </div>
	                    <!--  <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                      <%=smartHF.getHTMLVarExt(fw, sv.linstamt06,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>	                       
	                        </div>
	                     </div>
	             </div>
	             
	                 <div class="row">
                      <div class="col-md-1">
	                     <div class="form-group" style="min-width: 65px">
	                       <%=smartHF.getHTMLVarExt(fw, sv.mnth07, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>                
	                        </div>
	                    </div>
	                     <div class="col-md-1">
	                     </div>
	                     <div class="col-md-2">
	                     <div class="form-group">
	                     
	                     <% if ((new Byte((sv.datedue07Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <%=smartHF.getRichTextDateInput(fw, sv.datedue07Disp)%>
                                       
              			  <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datedue07Disp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datedue07Disp, (sv.datedue07Disp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
            			  <%}%> 
	                    
	                            <%-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effectiveDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue07Disp,(sv.datedue07Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                	        
	                      </div> --%>
	                     </div>
	                    </div>
	                     <!-- <div class="col-md-1">
	                     </div> -->
	                     <div class="col-md-3">
	                      <div class="form-group" style="min-width: 100px">
	                       <%=smartHF.getHTMLVarExt(fw, sv.linstamt07,  COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>	                       
	                        </div>
	                     </div>
	             </div>
	     </div>
</div>
	             


<%@ include file="/POLACommon2NEW.jsp"%>











