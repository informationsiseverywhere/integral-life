<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51E";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51eScreenVars sv = (Sr51eScreenVars) fw.getVariables();
%>
<%
	{
	}
%>
<!-- <style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style> -->

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td>

						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td> <label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable01"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable01");
								optionValue = makeDropDownList(mappedItems, sv.crtable01.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable01.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable01.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable01, fw, longValue, "crtable01", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable01discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable02"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable02");
								optionValue = makeDropDownList(mappedItems, sv.crtable02.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable02.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable02.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable02, fw, longValue, "crtable02", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable02discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable03"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable03");
								optionValue = makeDropDownList(mappedItems, sv.crtable03.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable03.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable03.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable03, fw, longValue, "crtable03", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable03discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable04"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable04");
								optionValue = makeDropDownList(mappedItems, sv.crtable04.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable04.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable04.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable04, fw, longValue, "crtable04", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable04discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable05"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable05");
								optionValue = makeDropDownList(mappedItems, sv.crtable05.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable05.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable05.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable05, fw, longValue, "crtable05", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable05discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable06"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable06");
								optionValue = makeDropDownList(mappedItems, sv.crtable06.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable06.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable06.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable06, fw, longValue, "crtable06", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable06discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable07"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable07");
								optionValue = makeDropDownList(mappedItems, sv.crtable07.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable07.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable07.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable07, fw, longValue, "crtable07", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable07discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable08"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable08");
								optionValue = makeDropDownList(mappedItems, sv.crtable08.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable08.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable08.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable08, fw, longValue, "crtable08", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable08discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable09"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable09");
								optionValue = makeDropDownList(mappedItems, sv.crtable09.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable09.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable09.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable09, fw, longValue, "crtable09", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable09discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable10"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable10");
								optionValue = makeDropDownList(mappedItems, sv.crtable10.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable10.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable10.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable10, fw, longValue, "crtable10", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable10discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable11"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable11");
								optionValue = makeDropDownList(mappedItems, sv.crtable11.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable11.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable11.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable11, fw, longValue, "crtable11", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable11discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable12"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable12");
								optionValue = makeDropDownList(mappedItems, sv.crtable12.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable12.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable12.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable12, fw, longValue, "crtable12", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable12discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable13"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable13");
								optionValue = makeDropDownList(mappedItems, sv.crtable13.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable13.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable13.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable13, fw, longValue, "crtable13", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable13discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable14"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable14");
								optionValue = makeDropDownList(mappedItems, sv.crtable14.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable14.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable14.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable14, fw, longValue, "crtable14", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable14discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable15"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable15");
								optionValue = makeDropDownList(mappedItems, sv.crtable15.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable15.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable15.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable15, fw, longValue, "crtable15", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable15discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable16"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable16");
								optionValue = makeDropDownList(mappedItems, sv.crtable16.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable16.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable16.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable16, fw, longValue, "crtable16", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable16discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable17"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable17");
								optionValue = makeDropDownList(mappedItems, sv.crtable17.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable17.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable17.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable17, fw, longValue, "crtable17", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable17discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable18"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable18");
								optionValue = makeDropDownList(mappedItems, sv.crtable18.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable18.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable18.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable18, fw, longValue, "crtable18", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable18discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable19"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable19");
								optionValue = makeDropDownList(mappedItems, sv.crtable19.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable19.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable19.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable19, fw, longValue, "crtable19", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable19discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"crtable20"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable20");
								optionValue = makeDropDownList(mappedItems, sv.crtable20.getFormData(), 4);
								longValue = (String) mappedItems.get((sv.crtable20.getFormData()).toString().trim());
								int colWidth = 0;
								int dropDownDescWidth = 0;
								dropDownDescWidth = ((sv.crtable20.getFormData()).length() * 12) + 88;
						%>
						<%
							if ((new Byte((sv.crtable20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<div class=dropdown_Div
							style=' position: relative; top: 0px; left:0px; width:<%=dropDownDescWidth%>px;height:20px;  padding-top: 0px;padding-left: 0px; z-index:1;'>

							<%=smartHF.getDropDownExt(sv.crtable20, fw, longValue, "crtable20", optionValue)%>
							<%
								if (longValue == null || "".equalsIgnoreCase(longValue)) {
							%>
							<script>
								getDefaultTwo(document
										.getElementById('crtable20discr'),
							<%=dropDownDescWidth - 20%>
								);
							</script>
							<%
								}
							%>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Band ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Comparision Operator ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Ann Income Up to % ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="width:46px;">
									<%
										if (((BaseScreenData) sv.age01) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age01, (sv.age01.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age01) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width:46px;">
						<%
							if (((BaseScreenData) sv.indc01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.indc01, (sv.indc01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.indc01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.indc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width:46px;">
						<%
							if (((BaseScreenData) sv.overdueMina01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.overdueMina01, (sv.overdueMina01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.overdueMina01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.overdueMina01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="width:46px;">
									<%
										if (((BaseScreenData) sv.age02) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age02, (sv.age02.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age02) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width:46px;">
						<%
							if (((BaseScreenData) sv.indc02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.indc02, (sv.indc02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.indc02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.indc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width:46px;">
						<%
							if (((BaseScreenData) sv.overdueMina02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.overdueMina02, (sv.overdueMina02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.overdueMina02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.overdueMina02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="width:46px;">
									<%
										if (((BaseScreenData) sv.age03) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age03, (sv.age03.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age03) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<%@ include file="/POLACommon2NEW.jsp"%>



