<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51B";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51bScreenVars sv = (Sr51bScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr51bscreenWritten.gt(0)) {
%>
<%
	Sr51bscreen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age Band");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Operator");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "SA");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Maximum Sum Assured");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Up to(%)");
%>
<%
	sv.age01.setClassString("");
%>
<%
	sv.indc01.setClassString("");
%>
<%
	sv.indc01.appendClassString("string_fld");
		sv.indc01.appendClassString("input_txt");
		sv.indc01.appendClassString("highlight");
%>
<%
	sv.overdueMina01.setClassString("");
%>
<%
	sv.sumins01.setClassString("");
%>
<%
	sv.age02.setClassString("");
%>
<%
	sv.indc02.setClassString("");
%>
<%
	sv.indc02.appendClassString("string_fld");
		sv.indc02.appendClassString("input_txt");
		sv.indc02.appendClassString("highlight");
%>
<%
	sv.overdueMina02.setClassString("");
%>
<%
	sv.sumins02.setClassString("");
%>
<%
	sv.age03.setClassString("");
%>
<%
	sv.indc03.setClassString("");
%>
<%
	sv.indc03.appendClassString("string_fld");
		sv.indc03.appendClassString("input_txt");
		sv.indc03.appendClassString("highlight");
%>
<%
	sv.overdueMina03.setClassString("");
%>
<%
	sv.sumins03.setClassString("");
%>
<%
	sv.age04.setClassString("");
%>
<%
	sv.indc04.setClassString("");
%>
<%
	sv.indc04.appendClassString("string_fld");
		sv.indc04.appendClassString("input_txt");
		sv.indc04.appendClassString("highlight");
%>
<%
	sv.overdueMina04.setClassString("");
%>
<%
	sv.sumins04.setClassString("");
%>
<%
	sv.age05.setClassString("");
%>
<%
	sv.indc05.setClassString("");
%>
<%
	sv.indc05.appendClassString("string_fld");
		sv.indc05.appendClassString("input_txt");
		sv.indc05.appendClassString("highlight");
%>
<%
	sv.overdueMina05.setClassString("");
%>
<%
	sv.sumins05.setClassString("");
%>
<%
	sv.age06.setClassString("");
%>
<%
	sv.indc06.setClassString("");
%>
<%
	sv.indc06.appendClassString("string_fld");
		sv.indc06.appendClassString("input_txt");
		sv.indc06.appendClassString("highlight");
%>
<%
	sv.overdueMina06.setClassString("");
%>
<%
	sv.sumins06.setClassString("");
%>
<%
	sv.age07.setClassString("");
%>
<%
	sv.indc07.setClassString("");
%>
<%
	sv.indc07.appendClassString("string_fld");
		sv.indc07.appendClassString("input_txt");
		sv.indc07.appendClassString("highlight");
%>
<%
	sv.overdueMina07.setClassString("");
%>
<%
	sv.sumins07.setClassString("");
%>
<%
	sv.age08.setClassString("");
%>
<%
	sv.indc08.setClassString("");
%>
<%
	sv.indc08.appendClassString("string_fld");
		sv.indc08.appendClassString("input_txt");
		sv.indc08.appendClassString("highlight");
%>
<%
	sv.overdueMina08.setClassString("");
%>
<%
	sv.sumins08.setClassString("");
%>
<%
	sv.age09.setClassString("");
%>
<%
	sv.indc09.setClassString("");
%>
<%
	sv.indc09.appendClassString("string_fld");
		sv.indc09.appendClassString("input_txt");
		sv.indc09.appendClassString("highlight");
%>
<%
	sv.overdueMina09.setClassString("");
%>
<%
	sv.sumins09.setClassString("");
%>
<%
	sv.age10.setClassString("");
%>
<%
	sv.indc10.setClassString("");
%>
<%
	sv.indc10.appendClassString("string_fld");
		sv.indc10.appendClassString("input_txt");
		sv.indc10.appendClassString("highlight");
%>
<%
	sv.overdueMina10.setClassString("");
%>
<%
	sv.sumins10.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.age01.setReverse(BaseScreenData.REVERSED);
				sv.age01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.age01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.overdueMina01.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.overdueMina01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.sumins01.setReverse(BaseScreenData.REVERSED);
				sv.sumins01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.sumins01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.age02.setReverse(BaseScreenData.REVERSED);
				sv.age02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.age02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.overdueMina02.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.overdueMina02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.sumins02.setReverse(BaseScreenData.REVERSED);
				sv.sumins02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.sumins02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.age03.setReverse(BaseScreenData.REVERSED);
				sv.age03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.age03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.overdueMina03.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.overdueMina03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.sumins03.setReverse(BaseScreenData.REVERSED);
				sv.sumins03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.sumins03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.age04.setReverse(BaseScreenData.REVERSED);
				sv.age04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.age04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.overdueMina04.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.overdueMina04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.sumins04.setReverse(BaseScreenData.REVERSED);
				sv.sumins04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.sumins04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.age05.setReverse(BaseScreenData.REVERSED);
				sv.age05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.age05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.overdueMina05.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.overdueMina05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.sumins05.setReverse(BaseScreenData.REVERSED);
				sv.sumins05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.sumins05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.age06.setReverse(BaseScreenData.REVERSED);
				sv.age06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.age06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.overdueMina06.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.overdueMina06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.sumins06.setReverse(BaseScreenData.REVERSED);
				sv.sumins06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.sumins06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.age07.setReverse(BaseScreenData.REVERSED);
				sv.age07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.age07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.overdueMina07.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.overdueMina07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.sumins07.setReverse(BaseScreenData.REVERSED);
				sv.sumins07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.sumins07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.age08.setReverse(BaseScreenData.REVERSED);
				sv.age08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.age08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.overdueMina08.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.overdueMina08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.sumins08.setReverse(BaseScreenData.REVERSED);
				sv.sumins08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.sumins08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.age09.setReverse(BaseScreenData.REVERSED);
				sv.age09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.age09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.overdueMina09.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.overdueMina09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.sumins09.setReverse(BaseScreenData.REVERSED);
				sv.sumins09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.sumins09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.age10.setReverse(BaseScreenData.REVERSED);
				sv.age10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.age10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.overdueMina10.setReverse(BaseScreenData.REVERSED);
				sv.overdueMina10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.overdueMina10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.sumins10.setReverse(BaseScreenData.REVERSED);
				sv.sumins10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.sumins10.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<!--  row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<!-- end row 1 -->
		<!-- row 2 -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td><label style="font-weight: bolder; font-size: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<!-- end row 2 -->
		<!-- row 3 -->
		<div class="row">
			<div class="col-md-12">
				<div class="from-group">
					<div class="table-responsive">
						<table class="table border-none  table-hover" width='100%'>
							<thead>
								<tr>
									<td><label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age Band")%></label><br>
										<label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=")%></label>
									</td>
									<td><label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Operator")%></label></td>
									<td><label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "SA")%></label><br>
										<label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Up to(%)")%></label>
									</td>
									<td><label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Maximum Sum Assured")%></label></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc01, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc02, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc03, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc04, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc05, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc06, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc07, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc08, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>

								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc09, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.age10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.indc10, 1, 46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.overdueMina10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS, 1,
						46)%></td>

									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins10, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 145)%></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- end row 3 -->
	</div>
</div>
<%
	}
%>
<%
	if (sv.Sr51bprotectWritten.gt(0)) {
%>
<%
	Sr51bprotect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>


<style>
div[id*='age'] {
	padding-right: 2px !important
}

div[id*='instpr'] {
	padding-right: 2px !important
}

div[id*='indc'] {
	padding-right: 1px !important
}

div[id*='overdueMina'] {
	padding-right: 2px !important
}

div[id*='sumins'] {
	padding-right: 2px !important
}

.input-group.three-controller>.input-group-addon {
	width: 46% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.table.border-none tr td, .table.border-none tr th {
	border: none !important;
}
</style>


<%@ include file="/POLACommon2NEW.jsp"%>
