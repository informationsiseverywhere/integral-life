<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sa524";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sa524ScreenVars sv = (Sa524ScreenVars) fw.getVariables();%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table>
		        		<tr>
		        			<td>
				        		<%					
									if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<%					
									if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
	        	</div>
	        </div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%					
									if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<label>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp;</label>
							</td>
							<td>
								<%					
									if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Holiday Process Rule")%></label>
					<%
						fieldItem=appVars.loadF4Fields(new String[] {"znfopt"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("znfopt");
						optionValue = makeDropDownList( mappedItems , sv.znfopt.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.znfopt.getFormData()).toString().trim());
					%>
					<div class='output_cell' style="width: 190px;" >
						<%=longValue == null ? "" : longValue %>
					</div>
					<%	
						longValue=null;
						mappedItems=null;
						optionValue=null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Months of Inforce")%></label>
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.minmthif).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
						%>
						<input name='minmthif' type='text'
							<%if ((sv.minmthif).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" 
							<%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.minmthif)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.minmthif);
							if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.minmthif)%>' 
							<%}%>
							size='<%=sv.minmthif.getLength()%>'
							maxLength='<%=sv.minmthif.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(minmthif)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.minmthif).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.minmthif).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" 
							<%} else {%>
								class=' <%=(sv.minmthif).getColor() == null ? "input_cell" : (sv.minmthif).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>
						/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Max Period for Premium Holiday (months)")%></label>
				<div class="form-group">
					<div class="input-group"">
					<%
							qpsf = fw.getFieldXMLDef((sv.maxphprd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
						%>
						<input name='maxphprd' type='text'
							<%if ((sv.maxphprd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" 
							<%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.maxphprd)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.maxphprd);
							if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.maxphprd)%>' 
							<%}%>
							size='<%=sv.maxphprd.getLength()%>'
							maxLength='<%=sv.maxphprd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(maxphprd)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.maxphprd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.maxphprd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" 
							<%} else {%>
								class=' <%=(sv.maxphprd).getColor() == null ? "input_cell" : (sv.maxphprd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>
						/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Set Base and Rider Premium Status as Premium Status")%></label>
					<input id="prmstatus" type="checkbox" name="prmstatus" onFocus="doFocus(this)" onHelp="return fieldHelp(prmstatus)" onKeyUp="return checkMaxLength(this)" disabled value="Y" checked
					<%
						if((sv.prmstatus).toString().trim().equalsIgnoreCase("Y")){
		      		%>	checked />
					<%}%>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>