<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SR51H";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51hScreenVars sv = (Sr51hScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr51hscreenWritten.gt(0)) {
%>
<%
	Sr51hscreen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Component Required");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	sv.crtable01.setClassString("");
%>
<%
	sv.crtable02.setClassString("");
%>
<%
	sv.crtable03.setClassString("");
%>
<%
	sv.crtable04.setClassString("");
%>
<%
	sv.crtable05.setClassString("");
%>
<%
	sv.crtable06.setClassString("");
%>
<%
	sv.crtable07.setClassString("");
%>
<%
	sv.crtable08.setClassString("");
%>
<%
	sv.crtable09.setClassString("");
%>
<%
	sv.crtable10.setClassString("");
%>
<%
	sv.crtable11.setClassString("");
%>
<%
	sv.crtable12.setClassString("");
%>
<%
	sv.crtable13.setClassString("");
%>
<%
	sv.crtable14.setClassString("");
%>
<%
	sv.crtable15.setClassString("");
%>
<%
	sv.crtable16.setClassString("");
%>
<%
	sv.crtable17.setClassString("");
%>
<%
	sv.crtable18.setClassString("");
%>
<%
	sv.crtable19.setClassString("");
%>
<%
	sv.crtable20.setClassString("");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Component not Allowed");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	sv.ctable01.setClassString("");
%>
<%
	sv.ctable02.setClassString("");
%>
<%
	sv.ctable03.setClassString("");
%>
<%
	sv.ctable04.setClassString("");
%>
<%
	sv.ctable05.setClassString("");
%>
<%
	sv.ctable06.setClassString("");
%>
<%
	sv.ctable07.setClassString("");
%>
<%
	sv.ctable08.setClassString("");
%>
<%
	sv.ctable09.setClassString("");
%>
<%
	sv.ctable10.setClassString("");
%>
<%
	sv.ctable11.setClassString("");
%>
<%
	sv.ctable12.setClassString("");
%>
<%
	sv.ctable13.setClassString("");
%>
<%
	sv.ctable14.setClassString("");
%>
<%
	sv.ctable15.setClassString("");
%>
<%
	sv.ctable16.setClassString("");
%>
<%
	sv.ctable17.setClassString("");
%>
<%
	sv.ctable18.setClassString("");
%>
<%
	sv.ctable19.setClassString("");
%>
<%
	sv.ctable20.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.crtable01.setReverse(BaseScreenData.REVERSED);
				sv.crtable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.crtable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.crtable02.setReverse(BaseScreenData.REVERSED);
				sv.crtable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.crtable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.crtable03.setReverse(BaseScreenData.REVERSED);
				sv.crtable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.crtable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.crtable04.setReverse(BaseScreenData.REVERSED);
				sv.crtable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.crtable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.crtable05.setReverse(BaseScreenData.REVERSED);
				sv.crtable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.crtable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.crtable06.setReverse(BaseScreenData.REVERSED);
				sv.crtable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.crtable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.crtable07.setReverse(BaseScreenData.REVERSED);
				sv.crtable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.crtable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.crtable08.setReverse(BaseScreenData.REVERSED);
				sv.crtable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.crtable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.crtable09.setReverse(BaseScreenData.REVERSED);
				sv.crtable09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.crtable09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.crtable10.setReverse(BaseScreenData.REVERSED);
				sv.crtable10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.crtable10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.crtable11.setReverse(BaseScreenData.REVERSED);
				sv.crtable11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.crtable11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.crtable12.setReverse(BaseScreenData.REVERSED);
				sv.crtable12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.crtable12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.crtable13.setReverse(BaseScreenData.REVERSED);
				sv.crtable13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.crtable13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.crtable14.setReverse(BaseScreenData.REVERSED);
				sv.crtable14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.crtable14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.crtable15.setReverse(BaseScreenData.REVERSED);
				sv.crtable15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.crtable15.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.crtable16.setReverse(BaseScreenData.REVERSED);
				sv.crtable16.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.crtable16.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.crtable17.setReverse(BaseScreenData.REVERSED);
				sv.crtable17.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.crtable17.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.crtable18.setReverse(BaseScreenData.REVERSED);
				sv.crtable18.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.crtable18.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.crtable19.setReverse(BaseScreenData.REVERSED);
				sv.crtable19.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.crtable19.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.crtable20.setReverse(BaseScreenData.REVERSED);
				sv.crtable20.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.crtable20.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.ctable01.setReverse(BaseScreenData.REVERSED);
				sv.ctable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.ctable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.ctable02.setReverse(BaseScreenData.REVERSED);
				sv.ctable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.ctable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.ctable03.setReverse(BaseScreenData.REVERSED);
				sv.ctable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.ctable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.ctable04.setReverse(BaseScreenData.REVERSED);
				sv.ctable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.ctable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.ctable05.setReverse(BaseScreenData.REVERSED);
				sv.ctable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.ctable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.ctable06.setReverse(BaseScreenData.REVERSED);
				sv.ctable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.ctable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.ctable07.setReverse(BaseScreenData.REVERSED);
				sv.ctable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.ctable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.ctable08.setReverse(BaseScreenData.REVERSED);
				sv.ctable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.ctable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.ctable09.setReverse(BaseScreenData.REVERSED);
				sv.ctable09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.ctable09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.ctable10.setReverse(BaseScreenData.REVERSED);
				sv.ctable10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.ctable10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.ctable11.setReverse(BaseScreenData.REVERSED);
				sv.ctable11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.ctable11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.ctable12.setReverse(BaseScreenData.REVERSED);
				sv.ctable12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.ctable12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.ctable13.setReverse(BaseScreenData.REVERSED);
				sv.ctable13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.ctable13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.ctable14.setReverse(BaseScreenData.REVERSED);
				sv.ctable14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.ctable14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.ctable15.setReverse(BaseScreenData.REVERSED);
				sv.ctable15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.ctable15.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind36.isOn()) {
				sv.ctable16.setReverse(BaseScreenData.REVERSED);
				sv.ctable16.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
				sv.ctable16.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind37.isOn()) {
				sv.ctable17.setReverse(BaseScreenData.REVERSED);
				sv.ctable17.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind37.isOn()) {
				sv.ctable17.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
				sv.ctable18.setReverse(BaseScreenData.REVERSED);
				sv.ctable18.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind38.isOn()) {
				sv.ctable18.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind39.isOn()) {
				sv.ctable19.setReverse(BaseScreenData.REVERSED);
				sv.ctable19.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind39.isOn()) {
				sv.ctable19.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind40.isOn()) {
				sv.ctable20.setReverse(BaseScreenData.REVERSED);
				sv.ctable20.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind40.isOn()) {
				sv.ctable20.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
						<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td> <label> <%=resourceBundleHandler.gettingValueFromBundle("To")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Required")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable01, (sv.crtable01.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable01')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable01)%>
								</div>
							</td>
							
							
							<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable02, (sv.crtable02.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable02)%>
								</div>
							</td>
							<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable03, (sv.crtable03.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable03)%>
								</div>
							</td><td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable04, (sv.crtable04.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable04)%>
								</div>
							</td>	</td><td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable05, (sv.crtable05.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable05)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable06, (sv.crtable06.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable06)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable07, (sv.crtable07.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable07)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable08, (sv.crtable08.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable08)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable09, (sv.crtable09.getLength()))%>
								<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable09)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable10, (sv.crtable10.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable10)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable11, (sv.crtable11.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable11)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable12, (sv.crtable12.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable12)%>
								</div>
							</td><td> &nbsp;&nbsp;&nbsp;</td>
							<td>	
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable13, (sv.crtable13.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable13)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable14, (sv.crtable14.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable14)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable15, (sv.crtable15.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable15)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable16, (sv.crtable16.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable16)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable17, (sv.crtable17.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable17)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable18, (sv.crtable18.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable18)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable19, (sv.crtable19.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable19)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable20, (sv.crtable20.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.crtable20)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("not")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Allowed")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable01, (sv.ctable01.getLength()))%>
										<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable01)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px" >
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable02, (sv.ctable02.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable02)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable03, (sv.ctable03.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable03)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable04, (sv.ctable04.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable04)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable05, (sv.ctable05.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable05)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable06, (sv.ctable06.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable06)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable07, (sv.ctable07.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable07)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable08, (sv.ctable08.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable08)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable09, (sv.ctable09.getLength()))%>
								<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable09)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable10, (sv.ctable10.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable10)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable11, (sv.ctable11.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable11)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable12, (sv.ctable12.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable12)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable13, (sv.ctable13.getLength()))%>
								<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable13)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable14, (sv.ctable14.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable14)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable15, (sv.ctable15.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable15)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable16, (sv.ctable16.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable16)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable17, (sv.ctable17.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable17)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable18, (sv.ctable18.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable18)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable19, (sv.ctable19.getLength()))%>
								<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable19)%>
								</div>
							</td>	<td> &nbsp;&nbsp;&nbsp;</td>
							<td>
								<div class="input-group" style="min-width:71px">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable20, (sv.ctable20.getLength()))%>
									<!-- <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> --><%=smartHF.getHTMLF4NSVarExt( fw, sv.ctable20)%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<%
	}
%>

<%
	if (sv.Sr51hprotectWritten.gt(0)) {
%>
<%
	Sr51hprotect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>