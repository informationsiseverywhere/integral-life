<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51G";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51gScreenVars sv = (Sr51gScreenVars) fw.getVariables();
%>
<%
	{
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td>

						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td> <label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Band ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Limit of Basic SA ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age01) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age01, (sv.age01.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age01) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins01, (sv.sumins01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins01,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age02) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age02, (sv.age02.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age02) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins02, (sv.sumins02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins02,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age03) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age03, (sv.age03.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age03) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins03) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins03, (sv.sumins03.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins03) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins03,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age04) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age04, (sv.age04.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age04) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins04) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins04, (sv.sumins04.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins04) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins04,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age05) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age05, (sv.age05.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age05) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins05) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins05, (sv.sumins05.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins05) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins05,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age06) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age06, (sv.age06.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age06) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins06) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins06, (sv.sumins06.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins06) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins06,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age07) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age07, (sv.age07.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age07) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins07) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins07, (sv.sumins07.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins07) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins07,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age08) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age08, (sv.age08.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age08) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins08) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins08, (sv.sumins08.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins08) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins08,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age09) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age09, (sv.age09.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age09) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins09) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins09, (sv.sumins09.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins09) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins09,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td>
								<div style="min-width:46px">
									<%
										if (((BaseScreenData) sv.age10) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age10, (sv.age10.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age10) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins10) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins10, (sv.sumins10.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins10) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins10,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,145)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->