<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5661";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5661ScreenVars sv = (S5661ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5661screenWritten.gt(0)) {
%>
<%
	S5661screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Default initial status ");
%>
<%
	sv.fupstat.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Complete statuses ");
%>
<%
	sv.fuposs01.setClassString("");
%>
<%
	sv.fuposs02.setClassString("");
%>
<%
	sv.fuposs03.setClassString("");
%>
<%
	sv.fuposs04.setClassString("");
%>
<%
	sv.fuposs05.setClassString("");
%>
<%
	sv.fuposs06.setClassString("");
%>
<%
	sv.fuposs07.setClassString("");
%>
<%
	sv.fuposs08.setClassString("");
%>
<%
	sv.fuposs09.setClassString("");
%>
<%
	sv.fuposs10.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Lead Days ");
%>
<%
	sv.zleadays.setClassString("");
%>
<%
	sv.zleadays.appendClassString("num_fld");
		sv.zleadays.appendClassString("input_txt");
		sv.zleadays.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reminder Days ");
%>
<%
	sv.zelpdays.setClassString("");
%>
<%
	sv.zelpdays.appendClassString("num_fld");
		sv.zelpdays.appendClassString("input_txt");
		sv.zelpdays.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Letter Type ");
%>
<%
	sv.zlettype.setClassString("");
%>
<%
	sv.zlettype.appendClassString("string_fld");
		sv.zlettype.appendClassString("input_txt");
		sv.zlettype.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reminder Letter Type ");
%>
<%
	sv.zlettyper.setClassString("");
%>
<%
	sv.zlettyper.appendClassString("string_fld");
		sv.zlettyper.appendClassString("input_txt");
		sv.zlettyper.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Print Status ");
%>
<%
	sv.zletstat.setClassString("");
%>
<%
	sv.zletstat.appendClassString("string_fld");
		sv.zletstat.appendClassString("input_txt");
		sv.zletstat.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reminder Print Status ");
%>
<%
	sv.zletstatr.setClassString("");
%>
<%
	sv.zletstatr.appendClassString("string_fld");
		sv.zletstatr.appendClassString("input_txt");
		sv.zletstatr.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Duplicate LETC Allowed ");
%>
<%
	sv.zdalind.setClassString("");
%>
<%
	sv.zdalind.appendClassString("string_fld");
		sv.zdalind.appendClassString("input_txt");
		sv.zdalind.appendClassString("highlight");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Doctor Required ");
%>
<%
	sv.zdocind.setClassString("");
%>
<%
	sv.zdocind.appendClassString("string_fld");
		sv.zdocind.appendClassString("input_txt");
		sv.zdocind.appendClassString("highlight");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Default Extended Text ");
%>
<%
	sv.message01.setClassString("");
%>
<%
	sv.message01.appendClassString("string_fld");
		sv.message01.appendClassString("input_txt");
		sv.message01.appendClassString("highlight");
%>
<%
	sv.message02.setClassString("");
%>
<%
	sv.message02.appendClassString("string_fld");
		sv.message02.appendClassString("input_txt");
		sv.message02.appendClassString("highlight");
%>
<%
	sv.message03.setClassString("");
%>
<%
	sv.message03.appendClassString("string_fld");
		sv.message03.appendClassString("input_txt");
		sv.message03.appendClassString("highlight");
%>
<%
	sv.message04.setClassString("");
%>
<%
	sv.message04.appendClassString("string_fld");
		sv.message04.appendClassString("input_txt");
		sv.message04.appendClassString("highlight");
%>
<%
	sv.message05.setClassString("");
%>
<%
	sv.message05.appendClassString("string_fld");
		sv.message05.appendClassString("input_txt");
		sv.message05.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.fupstat.setReverse(BaseScreenData.REVERSED);
				sv.fupstat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.fupstat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.fuposs01.setReverse(BaseScreenData.REVERSED);
				sv.fuposs01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.fuposs01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.fuposs02.setReverse(BaseScreenData.REVERSED);
				sv.fuposs02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.fuposs02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.fuposs03.setReverse(BaseScreenData.REVERSED);
				sv.fuposs03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.fuposs03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.fuposs04.setReverse(BaseScreenData.REVERSED);
				sv.fuposs04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.fuposs04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.fuposs05.setReverse(BaseScreenData.REVERSED);
				sv.fuposs05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.fuposs05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.fuposs06.setReverse(BaseScreenData.REVERSED);
				sv.fuposs06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.fuposs06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.fuposs07.setReverse(BaseScreenData.REVERSED);
				sv.fuposs07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.fuposs07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.fuposs08.setReverse(BaseScreenData.REVERSED);
				sv.fuposs08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.fuposs08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.fuposs09.setReverse(BaseScreenData.REVERSED);
				sv.fuposs09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.fuposs09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.fuposs10.setReverse(BaseScreenData.REVERSED);
				sv.fuposs10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.fuposs10.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Company")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.company.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.company.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.tabl.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.tabl.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.item.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.item.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>

</td><td>




						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "longdesc" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("longdesc");
									longValue = (String) mappedItems.get((sv.longdesc.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.longdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.longdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Initial Status")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.fupstat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupstat)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                    <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupstat)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupstat')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Complete Statuses")%></label>
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<table>
						<tr>
							<td>
							<%
                                         if ((new Byte((sv.fuposs01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							<%-- 	<div class="input-group" style="width: 100px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs01, (sv.fuposs01.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
											type="button"
											onclick="doFocus(document.getElementById('fuposs01')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> --%>
							</td>
							<td style="padding-left:1px;"> 
							<%
                                         if ((new Byte((sv.fuposs02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                              <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
							</td>
						
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								
							</td>
							<td style="padding-left:1px;">
							<%
                                         if ((new Byte((sv.fuposs10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width:46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.fuposs10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width:46px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fuposs10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fuposs10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Lead Days")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zleadays).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<input name='zleadays' type='text'
							<%if ((sv.zleadays).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zleadays.getFormData()).toString();%>
							<%if (formatValue.equals("000")) {%> value=''
							<%} else {
						if (formatValue.charAt(0) == '0') {
							formatValue = formatValue.substring(1, formatValue.length());
						}%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>' <%}%> value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zleadays.getLength()%>'
							maxLength='<%=sv.zleadays.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zleadays)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zleadays).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zleadays).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zleadays).getColor() == null ? "input_cell"
								: (sv.zleadays).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Reminder Days")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zelpdays).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<input name='zelpdays' type='text'
							<%if ((sv.zelpdays).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zelpdays.getFormData()).toString();%>
							<%if (formatValue.equals("000")) {%> value=''
							<%} else {
						if (formatValue.charAt(0) == '0') {
							formatValue = formatValue.substring(1, formatValue.length());
						}%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>' <%}%>
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zelpdays.getLength()%>'
							maxLength='<%=sv.zelpdays.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zelpdays)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zelpdays).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zelpdays).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zelpdays).getColor() == null ? "input_cell"
								: (sv.zelpdays).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Letter Type")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							/* if ((new Byte((sv.zlettype).getInvisible())).compareTo(new Byte(
																		BaseScreenData.INVISIBLE)) != 0) { */
								fieldItem = appVars.loadF4FieldsLong(new String[] { "zlettype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("zlettype");
								optionValue = makeDropDownList(mappedItems, sv.zlettype.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.zlettype.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.zlettype, fw, longValue, "zlettype", optionValue, 0, 220)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Reminder Letter Type")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							/* if ((new Byte((sv.zlettyper).getInvisible())).compareTo(new Byte(
																		BaseScreenData.INVISIBLE)) != 0) { */
								fieldItem = appVars.loadF4FieldsLong(new String[] { "zlettyper" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("zlettyper");
								optionValue = makeDropDownList(mappedItems, sv.zlettyper.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.zlettyper.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.zlettyper, fw, longValue, "zlettyper", optionValue, 0, 220)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Print Status")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zletstat).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<input name='zletstat' type='text'
							<%if ((sv.zletstat).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zletstat.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zletstat.getLength()%>'
							maxLength='<%=sv.zletstat.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zletstat)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zletstat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zletstat).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zletstat).getColor() == null ? "input_cell"
								: (sv.zletstat).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Reminder Print Status")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zletstatr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<input name='zletstatr' type='text'
							<%if ((sv.zletstatr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zletstatr.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zletstatr.getLength()%>'
							maxLength='<%=sv.zletstatr.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zletstatr)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zletstatr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zletstatr).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zletstatr).getColor() == null ? "input_cell"
								: (sv.zletstatr).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<%
				ScreenDataHelper duplicateAllowed = new ScreenDataHelper(fw.getVariables(), sv.zdalind);
			%>
			<div class="col-md-3"
				style="visibility:<%=duplicateAllowed.visibility()%>">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Duplicate LETC Allowed")%></label>
					<div style="width: 70px;">
						<%
							if (duplicateAllowed.isReadonly()) {
						%>
						<input name='zdalind' type='text'
							value='<%=duplicateAllowed.value()%>'
							class="<%=duplicateAllowed.clazz()%>" readonly
							onFocus='doFocus(this)' onHelp='return fieldHelp(zdocind)'
							onKeyUp='return checkMaxLength(this)'>
						<%
							} else {
						%>
						<select name="zdalind" type="list"
							class="<%=duplicateAllowed.clazz()%>">
							<option <%=duplicateAllowed.selected("Y")%> value="Y">Y</option>
							<option <%=duplicateAllowed.selected("N")%> value="N">N</option>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				ScreenDataHelper doctorRequired = new ScreenDataHelper(fw.getVariables(), sv.zdocind);
			%>
			<div class="col-md-3"
				style="visibility:<%=doctorRequired.visibility()%>">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Doctor Required")%></label>
					<div style="width: 70px;">
						<%
							if (doctorRequired.isReadonly()) {
						%>
						<input name='zdocind' type='text'
							value='<%=doctorRequired.value()%>'
							class="<%=doctorRequired.clazz()%>" readonly
							onFocus='doFocus(this)' onHelp='return fieldHelp(zdocind)'
							onKeyUp='return checkMaxLength(this)'>
						<%
							} else {
						%>
						<select name="zdocind" type="list"
							class="<%=doctorRequired.clazz()%>">
							<option <%=doctorRequired.selected("Y")%> value="Y">Y</option>
							<option <%=doctorRequired.selected("N")%> value="N">N</option>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Extended Text")%></label>
					<div class="form-group">
						<%=smartHF.getHTMLVarExt(fw, sv.message01)%>
						<%=smartHF.getHTMLVarExt(fw, sv.message02)%>
						<%=smartHF.getHTMLVarExt(fw, sv.message03)%>
						<%=smartHF.getHTMLVarExt(fw, sv.message04)%>
						<%=smartHF.getHTMLVarExt(fw, sv.message05)%>
					</div>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5661protectWritten.gt(0)) {
%>
<%
	S5661protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>

<%!private static class ScreenDataHelper {

		VarModel screen;
		BaseScreenData data;

		ScreenDataHelper(VarModel screen, BaseScreenData data) {
			super();
			if (screen == null)
				throw new AssertionError("Null VarModel");
			if (data == null)
				throw new AssertionError("Null BaseScreenData");
			this.screen = screen;
			this.data = data;
		}

		String value() {
			return data.getFormData().trim();
		}

		boolean isReadonly() {
			return (isProtected() || isDisabled());
		}

		boolean isProtected() {
			return screen.isScreenProtected();
		}

		boolean isDisabled() {
			return data.getEnabled() == BaseScreenData.DISABLED;
		}

		String readonly() {
			if (isProtected())
				return "readonly";
			else
				return "";
		}

		String visibility() {
			if (data.getInvisible() == BaseScreenData.INVISIBLE)
				return "hidden";
			else
				return "visible";
		}

		int length() {
			return value().length();
		}

		String clazz() {
			String clazz = "input_cell";
			if (isProtected())
				clazz = "blank_cell";
			else if (isDisabled())
				clazz = "output_cell";
			if (data.getHighLight() == BaseScreenData.BOLD)
				clazz += " bold_cell";
			if (BaseScreenData.RED.equals(data.getColor()))
				clazz += " red reverse";
			return clazz;
		}

		String selected(String value) {
			if (value().equalsIgnoreCase(value))
				return "selected='selected'";
			else
				return "";
		}

	}%>
<!-- ILIFE-2565 Life Cross Browser - Sprint 2 D2 : Task 1  -->


<style>
div[id*='fuposs'] {
	padding-right: 3px !important
}

div[id*='message'] {
	padding-right: 3px !important
}
</style>


<!-- ILIFE-2565 Life Cross Browser - Sprint 2 D2 : Task 1 -->




