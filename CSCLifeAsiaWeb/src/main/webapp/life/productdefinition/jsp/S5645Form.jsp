<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5645";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5645ScreenVars sv = (S5645ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5645screenWritten.gt(0)) {
%>
<%
	S5645screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subsidiary Ledger");
%>
<%
	generatedText5.appendClassString("label_txt");
		//generatedText5.appendClassString("underline");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"General Ledger");
%>
<%
	generatedText6.appendClassString("label_txt");
		//generatedText6.appendClassString("underline");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "DR/CR");
%>
<%
	generatedText7.appendClassString("label_txt");
		//generatedText7.appendClassString("underline");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Control Total");
%>
<%
	generatedText8.appendClassString("label_txt");
		//generatedText8.appendClassString("underline");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Code");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Account map");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(+/-)");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(1-12)");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1");
%>
<%
	sv.sacscode01.setClassString("");
%>
<%
	sv.sacscode01.appendClassString("string_fld");
		sv.sacscode01.appendClassString("input_txt");
		sv.sacscode01.appendClassString("highlight");
%>
<%
	sv.sacstype01.setClassString("");
%>
<%
	sv.sacstype01.appendClassString("string_fld");
		sv.sacstype01.appendClassString("input_txt");
		sv.sacstype01.appendClassString("highlight");
%>
<%
	sv.glmap01.setClassString("");
%>
<%
	sv.glmap01.appendClassString("string_fld");
		sv.glmap01.appendClassString("input_txt");
		sv.glmap01.appendClassString("highlight");
%>
<%
	sv.sign01.setClassString("");
%>
<%
	sv.sign01.appendClassString("string_fld");
		sv.sign01.appendClassString("input_txt");
		sv.sign01.appendClassString("highlight");
%>
<%
	sv.cnttot01.setClassString("");
%>
<%
	sv.cnttot01.appendClassString("num_fld");
		sv.cnttot01.appendClassString("input_txt");
		sv.cnttot01.appendClassString("highlight");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "2");
%>
<%
	sv.sacscode02.setClassString("");
%>
<%
	sv.sacscode02.appendClassString("string_fld");
		sv.sacscode02.appendClassString("input_txt");
		sv.sacscode02.appendClassString("highlight");
%>
<%
	sv.sacstype02.setClassString("");
%>
<%
	sv.sacstype02.appendClassString("string_fld");
		sv.sacstype02.appendClassString("input_txt");
		sv.sacstype02.appendClassString("highlight");
%>
<%
	sv.glmap02.setClassString("");
%>
<%
	sv.glmap02.appendClassString("string_fld");
		sv.glmap02.appendClassString("input_txt");
		sv.glmap02.appendClassString("highlight");
%>
<%
	sv.sign02.setClassString("");
%>
<%
	sv.sign02.appendClassString("string_fld");
		sv.sign02.appendClassString("input_txt");
		sv.sign02.appendClassString("highlight");
%>
<%
	sv.cnttot02.setClassString("");
%>
<%
	sv.cnttot02.appendClassString("num_fld");
		sv.cnttot02.appendClassString("input_txt");
		sv.cnttot02.appendClassString("highlight");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "3");
%>
<%
	sv.sacscode03.setClassString("");
%>
<%
	sv.sacscode03.appendClassString("string_fld");
		sv.sacscode03.appendClassString("input_txt");
		sv.sacscode03.appendClassString("highlight");
%>
<%
	sv.sacstype03.setClassString("");
%>
<%
	sv.sacstype03.appendClassString("string_fld");
		sv.sacstype03.appendClassString("input_txt");
		sv.sacstype03.appendClassString("highlight");
%>
<%
	sv.glmap03.setClassString("");
%>
<%
	sv.glmap03.appendClassString("string_fld");
		sv.glmap03.appendClassString("input_txt");
		sv.glmap03.appendClassString("highlight");
%>
<%
	sv.sign03.setClassString("");
%>
<%
	sv.sign03.appendClassString("string_fld");
		sv.sign03.appendClassString("input_txt");
		sv.sign03.appendClassString("highlight");
%>
<%
	sv.cnttot03.setClassString("");
%>
<%
	sv.cnttot03.appendClassString("num_fld");
		sv.cnttot03.appendClassString("input_txt");
		sv.cnttot03.appendClassString("highlight");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "4");
%>
<%
	sv.sacscode04.setClassString("");
%>
<%
	sv.sacscode04.appendClassString("string_fld");
		sv.sacscode04.appendClassString("input_txt");
		sv.sacscode04.appendClassString("highlight");
%>
<%
	sv.sacstype04.setClassString("");
%>
<%
	sv.sacstype04.appendClassString("string_fld");
		sv.sacstype04.appendClassString("input_txt");
		sv.sacstype04.appendClassString("highlight");
%>
<%
	sv.glmap04.setClassString("");
%>
<%
	sv.glmap04.appendClassString("string_fld");
		sv.glmap04.appendClassString("input_txt");
		sv.glmap04.appendClassString("highlight");
%>
<%
	sv.sign04.setClassString("");
%>
<%
	sv.sign04.appendClassString("string_fld");
		sv.sign04.appendClassString("input_txt");
		sv.sign04.appendClassString("highlight");
%>
<%
	sv.cnttot04.setClassString("");
%>
<%
	sv.cnttot04.appendClassString("num_fld");
		sv.cnttot04.appendClassString("input_txt");
		sv.cnttot04.appendClassString("highlight");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "5");
%>
<%
	sv.sacscode05.setClassString("");
%>
<%
	sv.sacscode05.appendClassString("string_fld");
		sv.sacscode05.appendClassString("input_txt");
		sv.sacscode05.appendClassString("highlight");
%>
<%
	sv.sacstype05.setClassString("");
%>
<%
	sv.sacstype05.appendClassString("string_fld");
		sv.sacstype05.appendClassString("input_txt");
		sv.sacstype05.appendClassString("highlight");
%>
<%
	sv.glmap05.setClassString("");
%>
<%
	sv.glmap05.appendClassString("string_fld");
		sv.glmap05.appendClassString("input_txt");
		sv.glmap05.appendClassString("highlight");
%>
<%
	sv.sign05.setClassString("");
%>
<%
	sv.sign05.appendClassString("string_fld");
		sv.sign05.appendClassString("input_txt");
		sv.sign05.appendClassString("highlight");
%>
<%
	sv.cnttot05.setClassString("");
%>
<%
	sv.cnttot05.appendClassString("num_fld");
		sv.cnttot05.appendClassString("input_txt");
		sv.cnttot05.appendClassString("highlight");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "6");
%>
<%
	sv.sacscode06.setClassString("");
%>
<%
	sv.sacscode06.appendClassString("string_fld");
		sv.sacscode06.appendClassString("input_txt");
		sv.sacscode06.appendClassString("highlight");
%>
<%
	sv.sacstype06.setClassString("");
%>
<%
	sv.sacstype06.appendClassString("string_fld");
		sv.sacstype06.appendClassString("input_txt");
		sv.sacstype06.appendClassString("highlight");
%>
<%
	sv.glmap06.setClassString("");
%>
<%
	sv.glmap06.appendClassString("string_fld");
		sv.glmap06.appendClassString("input_txt");
		sv.glmap06.appendClassString("highlight");
%>
<%
	sv.sign06.setClassString("");
%>
<%
	sv.sign06.appendClassString("string_fld");
		sv.sign06.appendClassString("input_txt");
		sv.sign06.appendClassString("highlight");
%>
<%
	sv.cnttot06.setClassString("");
%>
<%
	sv.cnttot06.appendClassString("num_fld");
		sv.cnttot06.appendClassString("input_txt");
		sv.cnttot06.appendClassString("highlight");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "7");
%>
<%
	sv.sacscode07.setClassString("");
%>
<%
	sv.sacscode07.appendClassString("string_fld");
		sv.sacscode07.appendClassString("input_txt");
		sv.sacscode07.appendClassString("highlight");
%>
<%
	sv.sacstype07.setClassString("");
%>
<%
	sv.sacstype07.appendClassString("string_fld");
		sv.sacstype07.appendClassString("input_txt");
		sv.sacstype07.appendClassString("highlight");
%>
<%
	sv.glmap07.setClassString("");
%>
<%
	sv.glmap07.appendClassString("string_fld");
		sv.glmap07.appendClassString("input_txt");
		sv.glmap07.appendClassString("highlight");
%>
<%
	sv.sign07.setClassString("");
%>
<%
	sv.sign07.appendClassString("string_fld");
		sv.sign07.appendClassString("input_txt");
		sv.sign07.appendClassString("highlight");
%>
<%
	sv.cnttot07.setClassString("");
%>
<%
	sv.cnttot07.appendClassString("num_fld");
		sv.cnttot07.appendClassString("input_txt");
		sv.cnttot07.appendClassString("highlight");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "8");
%>
<%
	sv.sacscode08.setClassString("");
%>
<%
	sv.sacscode08.appendClassString("string_fld");
		sv.sacscode08.appendClassString("input_txt");
		sv.sacscode08.appendClassString("highlight");
%>
<%
	sv.sacstype08.setClassString("");
%>
<%
	sv.sacstype08.appendClassString("string_fld");
		sv.sacstype08.appendClassString("input_txt");
		sv.sacstype08.appendClassString("highlight");
%>
<%
	sv.glmap08.setClassString("");
%>
<%
	sv.glmap08.appendClassString("string_fld");
		sv.glmap08.appendClassString("input_txt");
		sv.glmap08.appendClassString("highlight");
%>
<%
	sv.sign08.setClassString("");
%>
<%
	sv.sign08.appendClassString("string_fld");
		sv.sign08.appendClassString("input_txt");
		sv.sign08.appendClassString("highlight");
%>
<%
	sv.cnttot08.setClassString("");
%>
<%
	sv.cnttot08.appendClassString("num_fld");
		sv.cnttot08.appendClassString("input_txt");
		sv.cnttot08.appendClassString("highlight");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "9");
%>
<%
	sv.sacscode09.setClassString("");
%>
<%
	sv.sacscode09.appendClassString("string_fld");
		sv.sacscode09.appendClassString("input_txt");
		sv.sacscode09.appendClassString("highlight");
%>
<%
	sv.sacstype09.setClassString("");
%>
<%
	sv.sacstype09.appendClassString("string_fld");
		sv.sacstype09.appendClassString("input_txt");
		sv.sacstype09.appendClassString("highlight");
%>
<%
	sv.glmap09.setClassString("");
%>
<%
	sv.glmap09.appendClassString("string_fld");
		sv.glmap09.appendClassString("input_txt");
		sv.glmap09.appendClassString("highlight");
%>
<%
	sv.sign09.setClassString("");
%>
<%
	sv.sign09.appendClassString("string_fld");
		sv.sign09.appendClassString("input_txt");
		sv.sign09.appendClassString("highlight");
%>
<%
	sv.cnttot09.setClassString("");
%>
<%
	sv.cnttot09.appendClassString("num_fld");
		sv.cnttot09.appendClassString("input_txt");
		sv.cnttot09.appendClassString("highlight");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "10");
%>
<%
	sv.sacscode10.setClassString("");
%>
<%
	sv.sacscode10.appendClassString("string_fld");
		sv.sacscode10.appendClassString("input_txt");
		sv.sacscode10.appendClassString("highlight");
%>
<%
	sv.sacstype10.setClassString("");
%>
<%
	sv.sacstype10.appendClassString("string_fld");
		sv.sacstype10.appendClassString("input_txt");
		sv.sacstype10.appendClassString("highlight");
%>
<%
	sv.glmap10.setClassString("");
%>
<%
	sv.glmap10.appendClassString("string_fld");
		sv.glmap10.appendClassString("input_txt");
		sv.glmap10.appendClassString("highlight");
%>
<%
	sv.sign10.setClassString("");
%>
<%
	sv.sign10.appendClassString("string_fld");
		sv.sign10.appendClassString("input_txt");
		sv.sign10.appendClassString("highlight");
%>
<%
	sv.cnttot10.setClassString("");
%>
<%
	sv.cnttot10.appendClassString("num_fld");
		sv.cnttot10.appendClassString("input_txt");
		sv.cnttot10.appendClassString("highlight");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "11");
%>
<%
	sv.sacscode11.setClassString("");
%>
<%
	sv.sacscode11.appendClassString("string_fld");
		sv.sacscode11.appendClassString("input_txt");
		sv.sacscode11.appendClassString("highlight");
%>
<%
	sv.sacstype11.setClassString("");
%>
<%
	sv.sacstype11.appendClassString("string_fld");
		sv.sacstype11.appendClassString("input_txt");
		sv.sacstype11.appendClassString("highlight");
%>
<%
	sv.glmap11.setClassString("");
%>
<%
	sv.glmap11.appendClassString("string_fld");
		sv.glmap11.appendClassString("input_txt");
		sv.glmap11.appendClassString("highlight");
%>
<%
	sv.sign11.setClassString("");
%>
<%
	sv.sign11.appendClassString("string_fld");
		sv.sign11.appendClassString("input_txt");
		sv.sign11.appendClassString("highlight");
%>
<%
	sv.cnttot11.setClassString("");
%>
<%
	sv.cnttot11.appendClassString("num_fld");
		sv.cnttot11.appendClassString("input_txt");
		sv.cnttot11.appendClassString("highlight");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "12");
%>
<%
	sv.sacscode12.setClassString("");
%>
<%
	sv.sacscode12.appendClassString("string_fld");
		sv.sacscode12.appendClassString("input_txt");
		sv.sacscode12.appendClassString("highlight");
%>
<%
	sv.sacstype12.setClassString("");
%>
<%
	sv.sacstype12.appendClassString("string_fld");
		sv.sacstype12.appendClassString("input_txt");
		sv.sacstype12.appendClassString("highlight");
%>
<%
	sv.glmap12.setClassString("");
%>
<%
	sv.glmap12.appendClassString("string_fld");
		sv.glmap12.appendClassString("input_txt");
		sv.glmap12.appendClassString("highlight");
%>
<%
	sv.sign12.setClassString("");
%>
<%
	sv.sign12.appendClassString("string_fld");
		sv.sign12.appendClassString("input_txt");
		sv.sign12.appendClassString("highlight");
%>
<%
	sv.cnttot12.setClassString("");
%>
<%
	sv.cnttot12.appendClassString("num_fld");
		sv.cnttot12.appendClassString("input_txt");
		sv.cnttot12.appendClassString("highlight");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "13");
%>
<%
	sv.sacscode13.setClassString("");
%>
<%
	sv.sacscode13.appendClassString("string_fld");
		sv.sacscode13.appendClassString("input_txt");
		sv.sacscode13.appendClassString("highlight");
%>
<%
	sv.sacstype13.setClassString("");
%>
<%
	sv.sacstype13.appendClassString("string_fld");
		sv.sacstype13.appendClassString("input_txt");
		sv.sacstype13.appendClassString("highlight");
%>
<%
	sv.glmap13.setClassString("");
%>
<%
	sv.glmap13.appendClassString("string_fld");
		sv.glmap13.appendClassString("input_txt");
		sv.glmap13.appendClassString("highlight");
%>
<%
	sv.sign13.setClassString("");
%>
<%
	sv.sign13.appendClassString("string_fld");
		sv.sign13.appendClassString("input_txt");
		sv.sign13.appendClassString("highlight");
%>
<%
	sv.cnttot13.setClassString("");
%>
<%
	sv.cnttot13.appendClassString("num_fld");
		sv.cnttot13.appendClassString("input_txt");
		sv.cnttot13.appendClassString("highlight");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "14");
%>
<%
	sv.sacscode14.setClassString("");
%>
<%
	sv.sacscode14.appendClassString("string_fld");
		sv.sacscode14.appendClassString("input_txt");
		sv.sacscode14.appendClassString("highlight");
%>
<%
	sv.sacstype14.setClassString("");
%>
<%
	sv.sacstype14.appendClassString("string_fld");
		sv.sacstype14.appendClassString("input_txt");
		sv.sacstype14.appendClassString("highlight");
%>
<%
	sv.glmap14.setClassString("");
%>
<%
	sv.glmap14.appendClassString("string_fld");
		sv.glmap14.appendClassString("input_txt");
		sv.glmap14.appendClassString("highlight");
%>
<%
	sv.sign14.setClassString("");
%>
<%
	sv.sign14.appendClassString("string_fld");
		sv.sign14.appendClassString("input_txt");
		sv.sign14.appendClassString("highlight");
%>
<%
	sv.cnttot14.setClassString("");
%>
<%
	sv.cnttot14.appendClassString("num_fld");
		sv.cnttot14.appendClassString("input_txt");
		sv.cnttot14.appendClassString("highlight");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "15");
%>
<%
	sv.sacscode15.setClassString("");
%>
<%
	sv.sacscode15.appendClassString("string_fld");
		sv.sacscode15.appendClassString("input_txt");
		sv.sacscode15.appendClassString("highlight");
%>
<%
	sv.sacstype15.setClassString("");
%>
<%
	sv.sacstype15.appendClassString("string_fld");
		sv.sacstype15.appendClassString("input_txt");
		sv.sacstype15.appendClassString("highlight");
%>
<%
	sv.glmap15.setClassString("");
%>
<%
	sv.glmap15.appendClassString("string_fld");
		sv.glmap15.appendClassString("input_txt");
		sv.glmap15.appendClassString("highlight");
%>
<%
	sv.sign15.setClassString("");
%>
<%
	sv.sign15.appendClassString("string_fld");
		sv.sign15.appendClassString("input_txt");
		sv.sign15.appendClassString("highlight");
%>
<%
	sv.cnttot15.setClassString("");
%>
<%
	sv.cnttot15.appendClassString("num_fld");
		sv.cnttot15.appendClassString("input_txt");
		sv.cnttot15.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			appVars.rolldown();
			appVars.rollup();
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

</td><td>







						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-1">
				<div class="form-group">
					<label>Subsidiary Ledger</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>General Ledger</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>DR/CR</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Control Total</label>
					<div></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<label>Code</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Type</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Account map</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>(+/-)</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>(1-12)</label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>1</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					
					
					
					<%
			          if ((new Byte((sv.sacscode01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode01, (sv.sacscode01.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode01, (sv.sacscode01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('sacscode01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
			          if ((new Byte((sv.sacstype01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype01, (sv.sacscode01.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype01, (sv.sacstype01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
						
					<%
			          if ((new Byte((sv.glmap01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap01, (sv.glmap01.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap01, (sv.glmap01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign01).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>2</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode02, (sv.sacscode02.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode02, (sv.sacscode02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

					<%
			          if ((new Byte((sv.sacstype02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype02, (sv.sacstype02.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype02, (sv.sacstype02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				
				<%
			          if ((new Byte((sv.glmap02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap02, (sv.glmap02.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap02, (sv.glmap02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign02).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>3</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
								
				<%
			          if ((new Byte((sv.sacscode03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode03, (sv.sacscode03.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode03, (sv.sacscode03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.sacstype03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype03, (sv.sacstype03.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype03, (sv.sacstype03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

				
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				
				
					<%
			          if ((new Byte((sv.glmap03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap03, (sv.glmap03.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap03, (sv.glmap03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign03).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>4</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
								
					<%
			          if ((new Byte((sv.sacscode04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode04, (sv.sacscode04.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode04, (sv.sacscode04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
								
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
								
					<%
			          if ((new Byte((sv.sacstype04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype04, (sv.sacstype04.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype04, (sv.sacstype04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				
					<%
			          if ((new Byte((sv.glmap04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap04, (sv.glmap04.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap04, (sv.glmap04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
			
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign04).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>5</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
								
					<%
			          if ((new Byte((sv.sacscode05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode05, (sv.sacscode05.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode05, (sv.sacscode05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
			          if ((new Byte((sv.sacstype05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype05, (sv.sacstype05.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype05, (sv.sacstype05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
			          if ((new Byte((sv.glmap05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap05, (sv.glmap05.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap05, (sv.glmap05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign05).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>6</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode06, (sv.sacscode06.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode06, (sv.sacscode06.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode06')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacstype06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype06, (sv.sacstype06.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype06, (sv.sacstype06.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype06')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.glmap06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap06, (sv.glmap06.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap06, (sv.glmap06.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap06')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign06).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>7</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.sacscode07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode07, (sv.sacscode07.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode07, (sv.sacscode07.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode07')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

				<%
			          if ((new Byte((sv.sacstype07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype07, (sv.sacstype07.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype07, (sv.sacstype07.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype07')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.glmap07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap07, (sv.glmap07.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap07, (sv.glmap07.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap07')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign07).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>8</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode08, (sv.sacscode08.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode08, (sv.sacscode08.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode08')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

				<%
			          if ((new Byte((sv.sacstype08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype08, (sv.sacstype08.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype08, (sv.sacstype08.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype08')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				
				<%
			          if ((new Byte((sv.glmap08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap08, (sv.glmap08.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap08, (sv.glmap08.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap08')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign08).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>9</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.sacscode09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode09, (sv.sacscode09.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode09, (sv.sacscode09.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode09')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.sacstype09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype09, (sv.sacstype09.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype09, (sv.sacstype09.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype09')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<%
			          if ((new Byte((sv.glmap09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap09, (sv.glmap09.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap09, (sv.glmap09.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap09')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign09).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>10</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode10, (sv.sacscode10.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode10, (sv.sacscode10.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode10')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacstype10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype10, (sv.sacstype10.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype10, (sv.sacstype10.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype10')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					
					<%
			          if ((new Byte((sv.glmap10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap10, (sv.glmap10.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap10, (sv.glmap10.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap10')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign10).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>11</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				
					<%
			          if ((new Byte((sv.sacscode11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode11, (sv.sacscode11.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode11, (sv.sacscode11.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode11')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacstype11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype11, (sv.sacstype11.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype11, (sv.sacstype11.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype11')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
							
					<%
			          if ((new Byte((sv.glmap11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap11, (sv.glmap11.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap11, (sv.glmap11.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap11')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
				
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign11).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>12</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode12, (sv.sacscode12.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode12, (sv.sacscode12.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode12')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
				
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
			          if ((new Byte((sv.sacstype12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype12, (sv.sacstype12.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype12, (sv.sacstype12.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype12')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>

			
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.glmap12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap12, (sv.glmap12.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap12, (sv.glmap12.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap12')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign12).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>13</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
						<%
			          if ((new Byte((sv.sacscode13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode13, (sv.sacscode13.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode13, (sv.sacscode13.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode13')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

						<%
			          if ((new Byte((sv.sacstype13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype13, (sv.sacstype13.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype13, (sv.sacstype13.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype13')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
			
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.glmap13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap13, (sv.glmap13.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap13, (sv.glmap13.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap13')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign13).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>14</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
					<%
			          if ((new Byte((sv.sacscode14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode14, (sv.sacscode14.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode14, (sv.sacscode14.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode14')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
			
					<%
			          if ((new Byte((sv.sacstype14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype14, (sv.sacstype14.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype14, (sv.sacstype14.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype14')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
							
					<%
			          if ((new Byte((sv.glmap14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap14, (sv.glmap14.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap14, (sv.glmap14.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap14')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign14).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label>15</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				
				<%
			          if ((new Byte((sv.sacscode15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode15, (sv.sacscode15.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacscode15, (sv.sacscode15.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacscode15')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
			          if ((new Byte((sv.sacstype15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype15, (sv.sacstype15.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.sacstype15, (sv.sacstype15.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('sacstype15')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
			          if ((new Byte((sv.glmap15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			                                                      || fw.getVariables().isScreenProtected()) {
					%>
					
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap15, (sv.glmap15.getLength()))%>
					<%
					} else {
					%>
					<div class="input-group">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.glmap15, (sv.glmap15.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('glmap15')); doAction('sacscode06')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
			         }
			        %>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.sign15).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.cnttot15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" >
		<div class="col-md-9"></div>
		<div class="col-md-3">
				<div class="form-group">
				
			<div class="sectionbutton">
						
				<a class="btn btn-info" href="#" onClick=" JavaScript:doAction('PFKey91') ;">
				<%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
				<a class="btn btn-info" href="#" onClick=" JavaScript:doAction('PFKey90') ;">
				<%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
					
			</div>
			</div>
		</div>
				
		</div>
		
	
		
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%
	if (sv.S5645protectWritten.gt(0)) {
%>
<%
	S5645protect.clearClassString(sv);
%>
<%
	}
%>
		
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>


<script type="text/javascript">

	$(document).ready(function() {
		
		$("#sacscode01").width(45);
		$("#sacscode02").width(45);
		$("#sacscode03").width(45);
		$("#sacscode04").width(45);
		$("#sacscode05").width(45);
		$("#sacscode06").width(45);
		$("#sacscode07").width(45);
		$("#sacscode08").width(45);
		$("#sacscode09").width(45);
		$("#sacscode10").width(45);
		$("#sacscode11").width(45);
		$("#sacscode12").width(45);
		$("#sacscode13").width(45);
		$("#sacscode14").width(45);
		$("#sacscode15").width(45);
		
		$("#sacstype01").width(45);
		$("#sacstype02").width(45);
		$("#sacstype03").width(45);
		$("#sacstype04").width(45);
		$("#sacstype05").width(45);
		$("#sacstype06").width(45);
		$("#sacstype07").width(45);
		$("#sacstype08").width(45);
		$("#sacstype09").width(45);
		$("#sacstype10").width(45);
		$("#sacstype11").width(45);
		$("#sacstype12").width(45);
		$("#sacstype13").width(45);
		$("#sacstype14").width(45);
		$("#sacstype15").width(45);
				
		$("#glmap01").width(140);
		$("#glmap02").width(140);
		$("#glmap03").width(140);
		$("#glmap04").width(140);
		$("#glmap05").width(140);
		$("#glmap06").width(140);
		$("#glmap07").width(140);
		$("#glmap08").width(140);
		$("#glmap09").width(140);
		$("#glmap10").width(140);
		$("#glmap11").width(140);
		$("#glmap12").width(140);
		$("#glmap13").width(140);
		$("#glmap14").width(140);
		$("#glmap15").width(140);
		
		$("#sign01").width(40);
		$("#sign02").width(40);
		$("#sign03").width(40);
		$("#sign04").width(40);
		$("#sign05").width(40);
		$("#sign06").width(40);
		$("#sign07").width(40);
		$("#sign08").width(40);
		$("#sign09").width(40);
		$("#sign10").width(40);
		$("#sign11").width(40);
		$("#sign12").width(40);
		$("#sign13").width(40);
		$("#sign14").width(40);
		$("#sign15").width(40);
		
		$("#cnttot01").width(40);
		$("#cnttot02").width(40);
		$("#cnttot03").width(40);
		$("#cnttot04").width(40);
		$("#cnttot05").width(40);
		$("#cnttot06").width(40);
		$("#cnttot07").width(40);
		$("#cnttot08").width(40);
		$("#cnttot09").width(40);
		$("#cnttot10").width(40);
		$("#cnttot11").width(40);
		$("#cnttot12").width(40);
		$("#cnttot13").width(40);
		$("#cnttot14").width(40);
		$("#cnttot15").width(40);		
	});
</script>
<!-- ILIFE-2563 Life Cross Browser - Sprint 2 D1 : Task 5  -->

