<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5671";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5671ScreenVars sv = (S5671ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5671screenWritten.gt(0)) {
%>
<%
	S5671screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "2");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "3");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "4");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Program ");
%>
<%
	sv.pgm01.setClassString("");
%>
<%
	sv.pgm01.appendClassString("string_fld");
		sv.pgm01.appendClassString("input_txt");
		sv.pgm01.appendClassString("highlight");
%>
<%
	sv.pgm02.setClassString("");
%>
<%
	sv.pgm02.appendClassString("string_fld");
		sv.pgm02.appendClassString("input_txt");
		sv.pgm02.appendClassString("highlight");
%>
<%
	sv.pgm03.setClassString("");
%>
<%
	sv.pgm03.appendClassString("string_fld");
		sv.pgm03.appendClassString("input_txt");
		sv.pgm03.appendClassString("highlight");
%>
<%
	sv.pgm04.setClassString("");
%>
<%
	sv.pgm04.appendClassString("string_fld");
		sv.pgm04.appendClassString("input_txt");
		sv.pgm04.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subroutine ");
%>
<%
	sv.subprog01.setClassString("");
%>
<%
	sv.subprog01.appendClassString("string_fld");
		sv.subprog01.appendClassString("input_txt");
		sv.subprog01.appendClassString("highlight");
%>
<%
	sv.subprog02.setClassString("");
%>
<%
	sv.subprog02.appendClassString("string_fld");
		sv.subprog02.appendClassString("input_txt");
		sv.subprog02.appendClassString("highlight");
%>
<%
	sv.subprog03.setClassString("");
%>
<%
	sv.subprog03.appendClassString("string_fld");
		sv.subprog03.appendClassString("input_txt");
		sv.subprog03.appendClassString("highlight");
%>
<%
	sv.subprog04.setClassString("");
%>
<%
	sv.subprog04.appendClassString("string_fld");
		sv.subprog04.appendClassString("input_txt");
		sv.subprog04.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reversal Subroutine ");
%>
<%
	sv.trevsub01.setClassString("");
%>
<%
	sv.trevsub01.appendClassString("string_fld");
		sv.trevsub01.appendClassString("input_txt");
		sv.trevsub01.appendClassString("highlight");
%>
<%
	sv.trevsub02.setClassString("");
%>
<%
	sv.trevsub02.appendClassString("string_fld");
		sv.trevsub02.appendClassString("input_txt");
		sv.trevsub02.appendClassString("highlight");
%>
<%
	sv.trevsub03.setClassString("");
%>
<%
	sv.trevsub03.appendClassString("string_fld");
		sv.trevsub03.appendClassString("input_txt");
		sv.trevsub03.appendClassString("highlight");
%>
<%
	sv.trevsub04.setClassString("");
%>
<%
	sv.trevsub04.appendClassString("string_fld");
		sv.trevsub04.appendClassString("input_txt");
		sv.trevsub04.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Validation Item Key");
%>
<%
	sv.edtitm01.setClassString("");
%>
<%
	sv.edtitm01.appendClassString("string_fld");
		sv.edtitm01.appendClassString("input_txt");
		sv.edtitm01.appendClassString("highlight");
%>
<%
	sv.edtitm02.setClassString("");
%>
<%
	sv.edtitm02.appendClassString("string_fld");
		sv.edtitm02.appendClassString("input_txt");
		sv.edtitm02.appendClassString("highlight");
%>
<%
	sv.edtitm03.setClassString("");
%>
<%
	sv.edtitm03.appendClassString("string_fld");
		sv.edtitm03.appendClassString("input_txt");
		sv.edtitm03.appendClassString("highlight");
%>
<%
	sv.edtitm04.setClassString("");
%>
<%
	sv.edtitm04.appendClassString("string_fld");
		sv.edtitm04.appendClassString("input_txt");
		sv.edtitm04.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>




</td><td>




						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
		<div class="col-md-4"></div>
			<div class="col-md-2 ">
				<div class="form-group">
					<label>1</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>2</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>3</label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>4</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Program")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.pgm01).replaceAll("width:100%", "width:184%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.pgm02).replaceAll("width:100%", "width:184%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.pgm03).replaceAll("width:100%", "width:184%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.pgm04).replaceAll("width:100%", "width:184%")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.subprog01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.subprog02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.subprog03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.subprog04)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reversal Subroutine")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.trevsub01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.trevsub02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.trevsub03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.trevsub04)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Validation Item Key")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.edtitm01).replaceAll("width:100%", "width:180%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.edtitm02).replaceAll("width:100%", "width:180%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.edtitm03).replaceAll("width:100%", "width:180%")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.edtitm04).replaceAll("width:100%", "width:180%")%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5671protectWritten.gt(0)) {
%>
<%
	S5671protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>
