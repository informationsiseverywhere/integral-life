<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SR51D";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51dScreenVars sv = (Sr51dScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr51dscreenWritten.gt(0)) {
%>
<%
	Sr51dscreen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>

<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Agent Channel ");
%>

<%
	sv.srcebus01.setClassString("");
%>
<%
	sv.srcebus02.setClassString("");
%>
<%
	sv.srcebus03.setClassString("");
%>
<%
	sv.srcebus04.setClassString("");
%>
<%
	sv.srcebus05.setClassString("");
%>
<%
	sv.srcebus06.setClassString("");
%>
<%
	sv.srcebus07.setClassString("");
%>
<%
	sv.srcebus08.setClassString("");
%>
<%
	sv.srcebus09.setClassString("");
%>
<%
	sv.srcebus10.setClassString("");
%>
<%
	sv.srcebus11.setClassString("");
%>
<%
	sv.srcebus12.setClassString("");
%>
<%
	sv.srcebus13.setClassString("");
%>
<%
	sv.srcebus14.setClassString("");
%>
<%
	sv.srcebus15.setClassString("");
%>
<%
	sv.srcebus16.setClassString("");
%>
<%
	sv.srcebus17.setClassString("");
%>
<%
	sv.srcebus18.setClassString("");
%>
<%
	sv.srcebus19.setClassString("");
%>
<%
	sv.srcebus20.setClassString("");
%>
<%
	sv.srcebus21.setClassString("");
%>
<%
	sv.srcebus22.setClassString("");
%>
<%
	sv.srcebus23.setClassString("");
%>
<%
	sv.srcebus24.setClassString("");
%>
<%
	sv.srcebus25.setClassString("");
%>
<%
	sv.srcebus26.setClassString("");
%>
<%
	sv.srcebus27.setClassString("");
%>
<%
	sv.srcebus28.setClassString("");
%>
<%
	sv.srcebus29.setClassString("");
%>
<%
	sv.srcebus30.setClassString("");
%>
<%
	sv.srcebus31.setClassString("");
%>
<%
	sv.srcebus32.setClassString("");
%>
<%
	sv.srcebus33.setClassString("");
%>
<%
	sv.srcebus34.setClassString("");
%>
<%
	sv.srcebus35.setClassString("");
%>
<%
	sv.srcebus36.setClassString("");
%>
<%
	sv.srcebus37.setClassString("");
%>
<%
	sv.srcebus38.setClassString("");
%>
<%
	sv.srcebus39.setClassString("");
%>
<%
	sv.srcebus40.setClassString("");
%>
<%
	sv.srcebus41.setClassString("");
%>
<%
	sv.srcebus42.setClassString("");
%>
<%
	sv.srcebus43.setClassString("");
%>
<%
	sv.srcebus44.setClassString("");
%>
<%
	sv.srcebus45.setClassString("");
%>
<%
	sv.srcebus46.setClassString("");
%>
<%
	sv.srcebus47.setClassString("");
%>
<%
	sv.srcebus48.setClassString("");
%>
<%
	sv.srcebus49.setClassString("");
%>
<%
	sv.srcebus50.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.srcebus01.setReverse(BaseScreenData.REVERSED);
				sv.srcebus01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.srcebus01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.srcebus02.setReverse(BaseScreenData.REVERSED);
				sv.srcebus02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.srcebus02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.srcebus03.setReverse(BaseScreenData.REVERSED);
				sv.srcebus03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.srcebus03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.srcebus04.setReverse(BaseScreenData.REVERSED);
				sv.srcebus04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.srcebus04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.srcebus05.setReverse(BaseScreenData.REVERSED);
				sv.srcebus05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.srcebus05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.srcebus06.setReverse(BaseScreenData.REVERSED);
				sv.srcebus06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.srcebus06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.srcebus07.setReverse(BaseScreenData.REVERSED);
				sv.srcebus07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.srcebus07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.srcebus08.setReverse(BaseScreenData.REVERSED);
				sv.srcebus08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.srcebus08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.srcebus09.setReverse(BaseScreenData.REVERSED);
				sv.srcebus09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.srcebus09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.srcebus10.setReverse(BaseScreenData.REVERSED);
				sv.srcebus10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.srcebus10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.srcebus11.setReverse(BaseScreenData.REVERSED);
				sv.srcebus11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.srcebus11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.srcebus12.setReverse(BaseScreenData.REVERSED);
				sv.srcebus12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.srcebus12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.srcebus13.setReverse(BaseScreenData.REVERSED);
				sv.srcebus13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.srcebus13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.srcebus14.setReverse(BaseScreenData.REVERSED);
				sv.srcebus14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.srcebus14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.srcebus15.setReverse(BaseScreenData.REVERSED);
				sv.srcebus15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.srcebus15.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.srcebus16.setReverse(BaseScreenData.REVERSED);
				sv.srcebus16.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.srcebus16.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.srcebus17.setReverse(BaseScreenData.REVERSED);
				sv.srcebus17.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.srcebus17.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.srcebus18.setReverse(BaseScreenData.REVERSED);
				sv.srcebus18.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.srcebus18.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.srcebus19.setReverse(BaseScreenData.REVERSED);
				sv.srcebus19.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.srcebus19.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.srcebus20.setReverse(BaseScreenData.REVERSED);
				sv.srcebus20.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.srcebus20.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.srcebus21.setReverse(BaseScreenData.REVERSED);
				sv.srcebus21.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.srcebus21.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.srcebus22.setReverse(BaseScreenData.REVERSED);
				sv.srcebus22.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.srcebus22.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.srcebus23.setReverse(BaseScreenData.REVERSED);
				sv.srcebus23.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.srcebus23.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.srcebus24.setReverse(BaseScreenData.REVERSED);
				sv.srcebus24.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.srcebus24.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.srcebus25.setReverse(BaseScreenData.REVERSED);
				sv.srcebus25.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.srcebus25.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.srcebus26.setReverse(BaseScreenData.REVERSED);
				sv.srcebus26.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.srcebus26.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.srcebus27.setReverse(BaseScreenData.REVERSED);
				sv.srcebus27.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.srcebus27.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.srcebus28.setReverse(BaseScreenData.REVERSED);
				sv.srcebus28.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.srcebus28.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.srcebus29.setReverse(BaseScreenData.REVERSED);
				sv.srcebus29.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.srcebus29.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.srcebus30.setReverse(BaseScreenData.REVERSED);
				sv.srcebus30.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.srcebus30.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.srcebus31.setReverse(BaseScreenData.REVERSED);
				sv.srcebus31.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.srcebus31.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.srcebus32.setReverse(BaseScreenData.REVERSED);
				sv.srcebus32.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.srcebus32.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.srcebus33.setReverse(BaseScreenData.REVERSED);
				sv.srcebus33.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.srcebus33.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.srcebus34.setReverse(BaseScreenData.REVERSED);
				sv.srcebus34.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.srcebus34.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.srcebus35.setReverse(BaseScreenData.REVERSED);
				sv.srcebus35.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.srcebus35.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind36.isOn()) {
				sv.srcebus36.setReverse(BaseScreenData.REVERSED);
				sv.srcebus36.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
				sv.srcebus36.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind37.isOn()) {
				sv.srcebus37.setReverse(BaseScreenData.REVERSED);
				sv.srcebus37.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind37.isOn()) {
				sv.srcebus37.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
				sv.srcebus38.setReverse(BaseScreenData.REVERSED);
				sv.srcebus38.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind38.isOn()) {
				sv.srcebus38.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind39.isOn()) {
				sv.srcebus39.setReverse(BaseScreenData.REVERSED);
				sv.srcebus39.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind39.isOn()) {
				sv.srcebus39.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind40.isOn()) {
				sv.srcebus40.setReverse(BaseScreenData.REVERSED);
				sv.srcebus40.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind40.isOn()) {
				sv.srcebus40.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind41.isOn()) {
				sv.srcebus41.setReverse(BaseScreenData.REVERSED);
				sv.srcebus41.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind41.isOn()) {
				sv.srcebus41.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind42.isOn()) {
				sv.srcebus42.setReverse(BaseScreenData.REVERSED);
				sv.srcebus42.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind42.isOn()) {
				sv.srcebus42.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind43.isOn()) {
				sv.srcebus43.setReverse(BaseScreenData.REVERSED);
				sv.srcebus43.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind43.isOn()) {
				sv.srcebus43.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind44.isOn()) {
				sv.srcebus44.setReverse(BaseScreenData.REVERSED);
				sv.srcebus44.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind44.isOn()) {
				sv.srcebus44.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind45.isOn()) {
				sv.srcebus45.setReverse(BaseScreenData.REVERSED);
				sv.srcebus45.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind45.isOn()) {
				sv.srcebus45.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind46.isOn()) {
				sv.srcebus46.setReverse(BaseScreenData.REVERSED);
				sv.srcebus46.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind46.isOn()) {
				sv.srcebus46.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind47.isOn()) {
				sv.srcebus47.setReverse(BaseScreenData.REVERSED);
				sv.srcebus47.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind47.isOn()) {
				sv.srcebus47.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind48.isOn()) {
				sv.srcebus48.setReverse(BaseScreenData.REVERSED);
				sv.srcebus48.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind48.isOn()) {
				sv.srcebus48.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind49.isOn()) {
				sv.srcebus49.setReverse(BaseScreenData.REVERSED);
				sv.srcebus49.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind49.isOn()) {
				sv.srcebus49.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind50.isOn()) {
				sv.srcebus50.setReverse(BaseScreenData.REVERSED);
				sv.srcebus50.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind50.isOn()) {
				sv.srcebus50.setHighLight(BaseScreenData.BOLD);
			}
		}
%>



<!-- <style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style> -->

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><label> <%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
			<div class="col-md-10">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus01, (sv.srcebus01.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus01')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus02, (sv.srcebus02.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus02')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
										<%
                                         if ((new Byte((sv.srcebus03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus03, (sv.srcebus03.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus03')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus04, (sv.srcebus04.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus04')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus05, (sv.srcebus05.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus05')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
										<%
                                         if ((new Byte((sv.srcebus06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus06, (sv.srcebus06.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus06')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus07, (sv.srcebus07.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus07')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus08, (sv.srcebus08.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus08')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus09, (sv.srcebus09.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus09')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus10, (sv.srcebus10.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus10')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus11)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus11, (sv.srcebus11.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus11')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus12)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus12, (sv.srcebus12.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus12')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus13)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus13, (sv.srcebus13.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus13')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus14)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus14, (sv.srcebus14.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus14')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus15)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus15, (sv.srcebus15.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus15')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus16)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus16, (sv.srcebus16.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus16')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus17)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus17, (sv.srcebus17.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus17')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus18)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus18, (sv.srcebus18.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus18')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus19)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus19, (sv.srcebus19.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus19')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus20)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus20, (sv.srcebus20.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus20')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus21).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus21)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus21, (sv.srcebus21.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus21')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus22).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus22)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus22, (sv.srcebus22.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus22')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus23).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus23)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus23, (sv.srcebus23.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus23')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
										<%
                                         if ((new Byte((sv.srcebus24).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus24)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus24, (sv.srcebus24.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus24')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus25).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus25)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus25, (sv.srcebus25.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus25')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus26).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus26)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus26, (sv.srcebus26.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus26')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus27).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus27)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus27, (sv.srcebus27.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus27')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus28).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus28)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus28, (sv.srcebus28.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus28')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus29).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus29)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus29, (sv.srcebus29.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus29')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus30).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus30)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus30, (sv.srcebus30.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus30')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus31).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus31)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus31, (sv.srcebus31.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus31')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus32).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus32)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus32, (sv.srcebus32.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus32')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus33).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus33)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus33, (sv.srcebus33.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus33')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus34).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus34)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus34, (sv.srcebus34.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus34')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus35).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus35)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus35, (sv.srcebus35.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus35')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus36).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus36)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus36, (sv.srcebus36.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus36')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus37).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus37)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus37, (sv.srcebus37.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus37')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus38).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus38)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus38, (sv.srcebus38.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus38')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus39).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus39)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus39, (sv.srcebus39.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus39')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus40).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus40)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus40, (sv.srcebus40.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus40')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus41).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus41)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus41, (sv.srcebus41.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus41')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus42).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus42)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus42, (sv.srcebus42.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus42')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus43).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus43)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus43, (sv.srcebus43.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus43')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus44).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus44)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus44, (sv.srcebus44.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus44')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus45).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus45)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus45, (sv.srcebus45.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus45')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<table width='80%'>
					<tr>
									<td>
									<%
                                         if ((new Byte((sv.srcebus46).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus46)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus46, (sv.srcebus46.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus46')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus47).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus47)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus47, (sv.srcebus47.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus47')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
											<%
                                         if ((new Byte((sv.srcebus48).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus48)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus48, (sv.srcebus48.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus48')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.srcebus49).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus49)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus49, (sv.srcebus49.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus49')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
										<%
                                         if ((new Byte((sv.srcebus50).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.srcebus50)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.srcebus50, (sv.srcebus50.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('srcebus50')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
				</table>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sr51dprotectWritten.gt(0)) {
%>
<%
	Sr51dprotect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
