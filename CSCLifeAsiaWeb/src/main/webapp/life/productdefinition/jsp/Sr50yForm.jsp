<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR50Y";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr50yScreenVars sv = (Sr50yScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr50yscreenWritten.gt(0)) {
%>
<%
	Sr50yscreen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Validation Routine  ");
%>
<%
	sv.premsubr01.setClassString("");
%>
<%
	sv.premsubr02.setClassString("");
%>
<%
	sv.premsubr03.setClassString("");
%>
<%
	sv.premsubr04.setClassString("");
%>
<%
	sv.premsubr05.setClassString("");
%>
<%
	sv.premsubr06.setClassString("");
%>
<%
	sv.premsubr07.setClassString("");
%>
<%
	sv.premsubr08.setClassString("");
%>
<%
	sv.premsubr09.setClassString("");
%>
<%
	sv.premsubr10.setClassString("");
%>
<%
	sv.premsubr11.setClassString("");
%>
<%
	sv.premsubr12.setClassString("");
%>
<%
	sv.premsubr13.setClassString("");
%>
<%
	sv.premsubr14.setClassString("");
%>
<%
	sv.premsubr15.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.premsubr01.setReverse(BaseScreenData.REVERSED);
				sv.premsubr01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.premsubr01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.premsubr02.setReverse(BaseScreenData.REVERSED);
				sv.premsubr02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.premsubr02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.premsubr03.setReverse(BaseScreenData.REVERSED);
				sv.premsubr03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.premsubr03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.premsubr04.setReverse(BaseScreenData.REVERSED);
				sv.premsubr04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.premsubr04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.premsubr05.setReverse(BaseScreenData.REVERSED);
				sv.premsubr05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.premsubr05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.premsubr06.setReverse(BaseScreenData.REVERSED);
				sv.premsubr06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.premsubr06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.premsubr07.setReverse(BaseScreenData.REVERSED);
				sv.premsubr07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.premsubr07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.premsubr08.setReverse(BaseScreenData.REVERSED);
				sv.premsubr08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.premsubr08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.premsubr09.setReverse(BaseScreenData.REVERSED);
				sv.premsubr09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.premsubr09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.premsubr10.setReverse(BaseScreenData.REVERSED);
				sv.premsubr10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.premsubr10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.premsubr11.setReverse(BaseScreenData.REVERSED);
				sv.premsubr11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.premsubr11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.premsubr12.setReverse(BaseScreenData.REVERSED);
				sv.premsubr12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.premsubr12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.premsubr13.setReverse(BaseScreenData.REVERSED);
				sv.premsubr13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.premsubr13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.premsubr14.setReverse(BaseScreenData.REVERSED);
				sv.premsubr14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.premsubr14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.premsubr15.setReverse(BaseScreenData.REVERSED);
				sv.premsubr15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.premsubr15.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<!-- end row 1 -->
		<!-- row 2 -->
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Validation Routine  ")%></label>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr01)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr02)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr03)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr04)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr05)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr06)%>
					</div>
					
					</div></div></div>
					
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr07)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr08)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr09)%>
					</div>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr10)%>
					</div>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr11)%>
					</div>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr12)%>
					</div>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr13)%>
					</div>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr14)%>
					</div>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width:71px;">
					<%=smartHF.getHTMLVar(fw, sv.premsubr15)%>
					</div>
					
					</div></div></div>
		<!-- end row 2 -->
	</div>
</div>

<%
	}
%>
<%
	if (sv.Sr50yprotectWritten.gt(0)) {
%>
<%
	Sr50yprotect.clearClassString(sv);
%>
<%
	{
		}
%>
<%
	}
%>
<%@ include file="/POLACommon2NEW.jsp"%>


<!-- ILIFE-2577 Life Cross Browser - Sprint 2 D3 : Task 1  -->

<!-- ILIFE-2577 Life Cross Browser - Sprint 2 D3 : Task 1  -->


