

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR696";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr696ScreenVars sv = (Sr696ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Dates effective     ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Simplified");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Code");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sum Assured Band");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"From Sum Assured");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"To Sum Assured");
%>

<%
	{
		if (appVars.ind09.isOn()) {
			sv.item.setReverse(BaseScreenData.REVERSED);
			sv.item.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.item.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.simcrtable.setReverse(BaseScreenData.REVERSED);
			sv.simcrtable.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.simcrtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.frsumins01.setReverse(BaseScreenData.REVERSED);
			sv.frsumins01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.frsumins01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.tosumins01.setReverse(BaseScreenData.REVERSED);
			sv.tosumins01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.tosumins01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.saband01.setReverse(BaseScreenData.REVERSED);
			sv.saband01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.saband01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.tosumins02.setReverse(BaseScreenData.REVERSED);
			sv.tosumins02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.tosumins02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.tosumins03.setReverse(BaseScreenData.REVERSED);
			sv.tosumins03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.tosumins03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.tosumins04.setReverse(BaseScreenData.REVERSED);
			sv.tosumins04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.tosumins04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.tosumins05.setReverse(BaseScreenData.REVERSED);
			sv.tosumins05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.tosumins05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.tosumins06.setReverse(BaseScreenData.REVERSED);
			sv.tosumins06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.tosumins06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.tosumins07.setReverse(BaseScreenData.REVERSED);
			sv.tosumins07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.tosumins07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.tosumins08.setReverse(BaseScreenData.REVERSED);
			sv.tosumins08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.tosumins08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.tosumins09.setReverse(BaseScreenData.REVERSED);
			sv.tosumins09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.tosumins09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.tosumins10.setReverse(BaseScreenData.REVERSED);
			sv.tosumins10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.tosumins10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.frsumins02.setReverse(BaseScreenData.REVERSED);
			sv.frsumins02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.frsumins02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.frsumins03.setReverse(BaseScreenData.REVERSED);
			sv.frsumins03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.frsumins03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.frsumins04.setReverse(BaseScreenData.REVERSED);
			sv.frsumins04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.frsumins04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.frsumins05.setReverse(BaseScreenData.REVERSED);
			sv.frsumins05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.frsumins05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.frsumins06.setReverse(BaseScreenData.REVERSED);
			sv.frsumins06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.frsumins06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.frsumins07.setReverse(BaseScreenData.REVERSED);
			sv.frsumins07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.frsumins07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.frsumins08.setReverse(BaseScreenData.REVERSED);
			sv.frsumins08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.frsumins08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.frsumins09.setReverse(BaseScreenData.REVERSED);
			sv.frsumins09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.frsumins09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.frsumins10.setReverse(BaseScreenData.REVERSED);
			sv.frsumins10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.frsumins10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.saband02.setReverse(BaseScreenData.REVERSED);
			sv.saband02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.saband02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.saband03.setReverse(BaseScreenData.REVERSED);
			sv.saband03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.saband03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.saband04.setReverse(BaseScreenData.REVERSED);
			sv.saband04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.saband04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.saband05.setReverse(BaseScreenData.REVERSED);
			sv.saband05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.saband05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.saband06.setReverse(BaseScreenData.REVERSED);
			sv.saband06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.saband06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.saband07.setReverse(BaseScreenData.REVERSED);
			sv.saband07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.saband07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.saband08.setReverse(BaseScreenData.REVERSED);
			sv.saband08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.saband08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.saband09.setReverse(BaseScreenData.REVERSED);
			sv.saband09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.saband09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.saband10.setReverse(BaseScreenData.REVERSED);
			sv.saband10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.saband10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>









						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
<br>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Simplified")%></label>
					<div>
						<input name='simcrtable' type='text'
							<%if ((sv.simcrtable).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width:65px;" <%}%>
							<%formatValue = (sv.simcrtable.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.simcrtable.getLength()%>'
							maxLength='<%=sv.simcrtable.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(simcrtable)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.simcrtable).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell" style="width:65px;"
							<%} else if ((new Byte((sv.simcrtable).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.simcrtable).getColor() == null ? "input_cell"
						: (sv.simcrtable).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' style="width:65px;"
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured Band")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("From Sum Assured")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("To Sum Assured")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband01' type='text'
							<%if ((sv.saband01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband01.getLength()%>'
							maxLength='<%=sv.saband01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband01).getColor() == null ? "input_cell"
						: (sv.saband01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins01' type='text'
							<%if ((sv.frsumins01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins01.getLength(), sv.frsumins01.getScale(), 3)%>'
							maxLength='<%=sv.frsumins01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins01).getColor() == null ? "input_cell"
						: (sv.frsumins01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins01).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins01' type='text'
							<%if ((sv.tosumins01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins01.getLength(), sv.tosumins01.getScale(), 3)%>'
							maxLength='<%=sv.tosumins01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins01).getColor() == null ? "input_cell"
						: (sv.tosumins01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband02' type='text'
							<%if ((sv.saband02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband02.getLength()%>'
							maxLength='<%=sv.saband02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband02).getColor() == null ? "input_cell"
						: (sv.saband02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins02,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins02' type='text'
							<%if ((sv.frsumins02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins02.getLength(), sv.frsumins02.getScale(), 3)%>'
							maxLength='<%=sv.frsumins02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins02).getColor() == null ? "input_cell"
						: (sv.frsumins02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins02).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins02,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins02' type='text'
							<%if ((sv.tosumins02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins02.getLength(), sv.tosumins02.getScale(), 3)%>'
							maxLength='<%=sv.tosumins02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins02).getColor() == null ? "input_cell"
						: (sv.tosumins02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband03' type='text'
							<%if ((sv.saband03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband03.getLength()%>'
							maxLength='<%=sv.saband03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband03).getColor() == null ? "input_cell"
						: (sv.saband03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins03,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins03' type='text'
							<%if ((sv.frsumins03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins03.getLength(), sv.frsumins03.getScale(), 3)%>'
							maxLength='<%=sv.frsumins03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins03).getColor() == null ? "input_cell"
						: (sv.frsumins03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins03).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins03,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins03' type='text'
							<%if ((sv.tosumins03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins03.getLength(), sv.tosumins03.getScale(), 3)%>'
							maxLength='<%=sv.tosumins03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins03).getColor() == null ? "input_cell"
						: (sv.tosumins03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband04' type='text'
							<%if ((sv.saband04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband04.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband04.getLength()%>'
							maxLength='<%=sv.saband04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband04).getColor() == null ? "input_cell"
						: (sv.saband04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins04,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins04' type='text'
							<%if ((sv.frsumins04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins04.getLength(), sv.frsumins04.getScale(), 3)%>'
							maxLength='<%=sv.frsumins04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins04).getColor() == null ? "input_cell"
						: (sv.frsumins04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins04).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins01' type='text'
							<%if ((sv.tosumins01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins01.getLength(), sv.tosumins01.getScale(), 3)%>'
							maxLength='<%=sv.tosumins01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins01).getColor() == null ? "input_cell"
						: (sv.tosumins01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband05' type='text'
							<%if ((sv.saband05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband05.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband05.getLength()%>'
							maxLength='<%=sv.saband05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband05).getColor() == null ? "input_cell"
						: (sv.saband05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins05,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins05' type='text'
							<%if ((sv.frsumins05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins05.getLength(), sv.frsumins05.getScale(), 3)%>'
							maxLength='<%=sv.frsumins05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins05).getColor() == null ? "input_cell"
						: (sv.frsumins05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins05).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins01' type='text'
							<%if ((sv.tosumins01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins01.getLength(), sv.tosumins01.getScale(), 3)%>'
							maxLength='<%=sv.tosumins01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins01).getColor() == null ? "input_cell"
						: (sv.tosumins01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband06' type='text'
							<%if ((sv.saband06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband06.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband06.getLength()%>'
							maxLength='<%=sv.saband06.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband06).getColor() == null ? "input_cell"
						: (sv.saband06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins06,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins06' type='text'
							<%if ((sv.frsumins06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins06.getLength(), sv.frsumins06.getScale(), 3)%>'
							maxLength='<%=sv.frsumins06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins06).getColor() == null ? "input_cell"
						: (sv.frsumins06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins06).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins06,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins06' type='text'
							<%if ((sv.tosumins06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins06.getLength(), sv.tosumins06.getScale(), 3)%>'
							maxLength='<%=sv.tosumins06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins06).getColor() == null ? "input_cell"
						: (sv.tosumins06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband07' type='text'
							<%if ((sv.saband07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband07.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband07.getLength()%>'
							maxLength='<%=sv.saband07.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband07).getColor() == null ? "input_cell"
						: (sv.saband07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins07,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins07' type='text'
							<%if ((sv.frsumins07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins07.getLength(), sv.frsumins07.getScale(), 3)%>'
							maxLength='<%=sv.frsumins07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins07).getColor() == null ? "input_cell"
						: (sv.frsumins07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins07).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins07,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins07' type='text'
							<%if ((sv.tosumins07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins07.getLength(), sv.tosumins07.getScale(), 3)%>'
							maxLength='<%=sv.tosumins07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins07).getColor() == null ? "input_cell"
						: (sv.tosumins07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband08' type='text'
							<%if ((sv.saband08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband08.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband08.getLength()%>'
							maxLength='<%=sv.saband08.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband08).getColor() == null ? "input_cell"
						: (sv.saband08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins08,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins08' type='text'
							<%if ((sv.frsumins08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins08.getLength(), sv.frsumins08.getScale(), 3)%>'
							maxLength='<%=sv.frsumins08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins08).getColor() == null ? "input_cell"
						: (sv.frsumins08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins08).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins08,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins08' type='text'
							<%if ((sv.tosumins08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins08.getLength(), sv.tosumins08.getScale(), 3)%>'
							maxLength='<%=sv.tosumins08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins08).getColor() == null ? "input_cell"
						: (sv.tosumins08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband09' type='text'
							<%if ((sv.saband09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband09.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband09.getLength()%>'
							maxLength='<%=sv.saband09.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband09).getColor() == null ? "input_cell"
						: (sv.saband09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins09).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins09,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins09' type='text'
							<%if ((sv.frsumins09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins09.getLength(), sv.frsumins09.getScale(), 3)%>'
							maxLength='<%=sv.frsumins09.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins09).getColor() == null ? "input_cell"
						: (sv.frsumins09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins09).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins09,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins09' type='text'
							<%if ((sv.tosumins09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins09.getLength(), sv.tosumins09.getScale(), 3)%>'
							maxLength='<%=sv.tosumins09.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins09).getColor() == null ? "input_cell"
						: (sv.tosumins09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;">
						<input name='saband10' type='text'
							<%if ((sv.saband10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.saband10.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.saband10.getLength()%>'
							maxLength='<%=sv.saband10.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saband10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.saband10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.saband10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.saband10).getColor() == null ? "input_cell"
						: (sv.saband10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.frsumins10).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);

							valueThis = smartHF.getPicFormatted(qpsf, sv.frsumins10,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='frsumins10' type='text'
							<%if ((sv.frsumins10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.frsumins10.getLength(), sv.frsumins10.getScale(), 3)%>'
							maxLength='<%=sv.frsumins10.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(frsumins10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.frsumins10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.frsumins10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.frsumins10).getColor() == null ? "input_cell"
						: (sv.frsumins10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.tosumins10).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tosumins10,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tosumins10' type='text'
							<%if ((sv.tosumins10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tosumins10.getLength(), sv.tosumins10.getScale(), 3)%>'
							maxLength='<%=sv.tosumins10.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tosumins10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tosumins10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tosumins10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tosumins10).getColor() == null ? "input_cell"
						: (sv.tosumins10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="visibility: hidden;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Code")%></label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
