<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S5552";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5552ScreenVars sv = (S5552ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5552screenWritten.gt(0)) {
%>
<%
	S5552screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Effective");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Date");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Months");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Months");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Before");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "After");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Business Date");
%>
<%
	sv.bsmthsbf.setClassString("");
%>
<%
	sv.bsmthsaf.setClassString("");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Paid-to Date");
%>
<%
	sv.ptmthsbf.setClassString("");
%>
<%
	sv.ptmthsaf.setClassString("");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Billed-to must equal Paid-to? ");
%>
<%
	sv.bteqpt.setClassString("");
%>
<%
	sv.bteqpt.appendClassString("string_fld");
		sv.bteqpt.appendClassString("input_txt");
		sv.bteqpt.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.bsmthsbf.setReverse(BaseScreenData.REVERSED);
				sv.bsmthsbf.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.bsmthsbf.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.bsmthsaf.setReverse(BaseScreenData.REVERSED);
				sv.bsmthsaf.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.bsmthsaf.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.ptmthsbf.setReverse(BaseScreenData.REVERSED);
				sv.ptmthsbf.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.ptmthsbf.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.ptmthsaf.setReverse(BaseScreenData.REVERSED);
				sv.ptmthsaf.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.ptmthsaf.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>




</td><td>




						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<br />
		<div class="row">
		<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label> <label><%=smartHF.getLit(generatedText6)%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
		<div class="col-md-4"></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label> <label><%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label> <label><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.bsmthsbf, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.bsmthsaf, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.ptmthsbf, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.ptmthsaf, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText13)%></label>
					<div style="width: 50px;"><%=smartHF.getHTMLVarExt(fw, sv.bteqpt)%></div>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5552protectWritten.gt(0)) {
%>
<%
	S5552protect.clearClassString(sv);
%>
<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>
