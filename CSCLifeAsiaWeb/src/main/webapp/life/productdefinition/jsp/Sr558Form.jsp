<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR558";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr558ScreenVars sv = (Sr558ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr558screenWritten.gt(0)) {
%>
<%
	Sr558screen.clearClassString(sv);
%>
<%if (appVars.ind18.isOn()) {
	sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
}
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	sv.chdrnum.setClassString("");
%>
<%
	sv.chdrnum.appendClassString("string_fld");
		sv.chdrnum.appendClassString("output_txt");
		sv.chdrnum.appendClassString("highlight");
%>
<%
	sv.cnttype.setClassString("");
%>
<%
	sv.cnttype.appendClassString("string_fld");
		sv.cnttype.appendClassString("output_txt");
		sv.cnttype.appendClassString("highlight");
%>
<%
	sv.ctypedes.setClassString("");
%>
<%
	sv.ctypedes.appendClassString("string_fld");
		sv.ctypedes.appendClassString("output_txt");
		sv.ctypedes.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	sv.cntcurr.setClassString("");
%>
<%
	sv.cntcurr.appendClassString("string_fld");
		sv.cntcurr.appendClassString("output_txt");
		sv.cntcurr.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract Status ");
%>
<%
	sv.chdrstatus.setClassString("");
%>
<%
	sv.chdrstatus.appendClassString("string_fld");
		sv.chdrstatus.appendClassString("output_txt");
		sv.chdrstatus.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Premium Status ");
%>
<%
	sv.premstatus.setClassString("");
%>
<%
	sv.premstatus.appendClassString("string_fld");
		sv.premstatus.appendClassString("output_txt");
		sv.premstatus.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	sv.register.setClassString("");
%>
<%
	sv.register.appendClassString("string_fld");
		sv.register.appendClassString("output_txt");
		sv.register.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Life Assured    ");
%>
<%
	sv.lifenum.setClassString("");
%>
<%
	sv.lifenum.appendClassString("string_fld");
		sv.lifenum.appendClassString("output_txt");
		sv.lifenum.appendClassString("highlight");
%>
<%
	sv.lifename.setClassString("");
%>
<%
	sv.lifename.appendClassString("string_fld");
		sv.lifename.appendClassString("output_txt");
		sv.lifename.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Joint Life      ");
%>
<%
	sv.jlife.setClassString("");
%>
<%
	sv.jlife.appendClassString("string_fld");
		sv.jlife.appendClassString("output_txt");
		sv.jlife.appendClassString("highlight");
%>
<%
	sv.jlifename.setClassString("");
%>
<%
	sv.jlifename.appendClassString("string_fld");
		sv.jlifename.appendClassString("output_txt");
		sv.jlifename.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Owner           ");
%>
<%
	sv.cownnum.setClassString("");
%>
<%
	sv.cownnum.appendClassString("string_fld");
		sv.cownnum.appendClassString("output_txt");
		sv.cownnum.appendClassString("highlight");
%>
<%
	sv.ownername.setClassString("");
%>
<%
	sv.ownername.appendClassString("string_fld");
		sv.ownername.appendClassString("output_txt");
		sv.ownername.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Joint Owner     ");
%>
<%
	sv.jowner.setClassString("");
%>
<%
	sv.jowner.appendClassString("string_fld");
		sv.jowner.appendClassString("output_txt");
		sv.jowner.appendClassString("highlight");
%>
<%
	sv.jownername.setClassString("");
%>
<%
	sv.jownername.appendClassString("string_fld");
		sv.jownername.appendClassString("output_txt");
		sv.jownername.appendClassString("highlight");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Payment Meth    ");
%>
<%
	sv.mop.setClassString("");
%>
<%
	sv.mop.appendClassString("string_fld");
		sv.mop.appendClassString("output_txt");
		sv.mop.appendClassString("highlight");
%>
<%
	sv.pymdesc.setClassString("");
%>
<%
	sv.pymdesc.appendClassString("string_fld");
		sv.pymdesc.appendClassString("output_txt");
		sv.pymdesc.appendClassString("highlight");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Assignees  ");
%>
<%
	sv.indic.setClassString("");
%>
<%
	sv.indic.appendClassString("string_fld");
		sv.indic.appendClassString("output_txt");
		sv.indic.appendClassString("highlight");
%>
<%-- <%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Risk Comm Date  ");
%> --%>
<!-- ILJ-49 start -->
					<%
					StringData generatedText26 = null;
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Date ");%>
					<%} else { %>
					<%generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm Date  ");%>
						<%} %>
                   <!-- ILJ-49 ends -->	
<%
	sv.occdateDisp.setClassString("");
%>
<%
	sv.occdateDisp.appendClassString("string_fld");
		sv.occdateDisp.appendClassString("output_txt");
		sv.occdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Bill To Date ");
%>
<%
	sv.btdateDisp.setClassString("");
%>
<%
	sv.btdateDisp.appendClassString("string_fld");
		sv.btdateDisp.appendClassString("output_txt");
		sv.btdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Paid To Date  ");
%>
<%
	sv.ptdateDisp.setClassString("");
%>
<%
	sv.ptdateDisp.appendClassString("string_fld");
		sv.ptdateDisp.appendClassString("output_txt");
		sv.ptdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Next Bill Date  ");
%>
<%
	sv.nextinsdteDisp.setClassString("");
%>
<%
	sv.nextinsdteDisp.appendClassString("string_fld");
		sv.nextinsdteDisp.appendClassString("output_txt");
		sv.nextinsdteDisp.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Prem O/standing ");
%>
<%
	sv.outstamt.setClassString("");
%>
<%
	sv.outstamt.appendClassString("num_fld");
		sv.outstamt.appendClassString("output_txt");
		sv.outstamt.appendClassString("highlight");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"NFL Loan        ");
%>
<%
	sv.aplamt.setClassString("");
%>
<%
	sv.aplamt.appendClassString("num_fld");
		sv.aplamt.appendClassString("output_txt");
		sv.aplamt.appendClassString("highlight");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Interest       ");
%>
<%
	sv.aplint.setClassString("");
%>
<%
	sv.aplint.appendClassString("num_fld");
		sv.aplint.appendClassString("output_txt");
		sv.aplint.appendClassString("highlight");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Policy Loan     ");
%>
<%
	sv.loanvalue.setClassString("");
%>
<%
	sv.loanvalue.appendClassString("num_fld");
		sv.loanvalue.appendClassString("output_txt");
		sv.loanvalue.appendClassString("highlight");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Interest       ");
%>
<%
	sv.plint.setClassString("");
%>
<%
	sv.plint.appendClassString("num_fld");
		sv.plint.appendClassString("output_txt");
		sv.plint.appendClassString("highlight");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"---------------------------------------------------------------------------");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "OLD");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "NEW");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Billing Freq    ");
%>
<%
	sv.billfreq.setClassString("");
%>
<%
	sv.billfreq.appendClassString("string_fld");
		sv.billfreq.appendClassString("output_txt");
		sv.billfreq.appendClassString("highlight");
%>
<%
	sv.shortds01.setClassString("");
%>
<%
	sv.shortds01.appendClassString("string_fld");
		sv.shortds01.appendClassString("output_txt");
		sv.shortds01.appendClassString("highlight");
%>
<%
	sv.frequency.setClassString("");
%>
<%
	sv.frequency.appendClassString("string_fld");
		sv.frequency.appendClassString("input_txt");
		sv.frequency.appendClassString("highlight");
%>
<%
	sv.shortds02.setClassString("");
%>
<%
	sv.shortds02.appendClassString("string_fld");
		sv.shortds02.appendClassString("output_txt");
		sv.shortds02.appendClassString("highlight");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Instalment Prem ");
%>
<%
	sv.instpramt.setClassString("");
%>
<%
	sv.instpramt.appendClassString("num_fld");
		sv.instpramt.appendClassString("output_txt");
		sv.instpramt.appendClassString("highlight");
%>
<%
	sv.nextinsamt.setClassString("");
%>
<%
	sv.nextinsamt.appendClassString("num_fld");
		sv.nextinsamt.appendClassString("output_txt");
		sv.nextinsamt.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText2)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.chdrnum)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.cnttype)%> <%=smartHF.getHTMLF4NSVarExt(fw, sv.cnttype)%>
							</td>
							<td><div style="width: 300px;"><%=smartHF.getHTMLVarExt(fw, sv.ctypedes)%></div></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-2" style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.cntcurr)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntcurr)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText4)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.chdrstatus)%></div>
				</div>
			</div>
			<div class="col-md-2 " style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.premstatus)%></div>
				</div>
			</div>
			<div class="col-md-2 " style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText6)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.register)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.register)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.lifenum)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.lifename)%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText8)%></label>
				</div>
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.jlife)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.jlifename)%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.cownnum)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.ownername)%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.jowner)%><%=smartHF.getHTMLF4NSVarExt(fw, sv.jowner)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.jownername)%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText21)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><%=smartHF.getHTMLVarExt(fw, sv.mop)%> <%=smartHF.getHTMLF4NSVarExt(fw, sv.mop)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pymdesc)%></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-2" style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText22)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.indic)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText26)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.occdateDisp)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.occdateDisp)%>
					</div>
				</div>
			</div>
			<div class="col-md-2 " style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.btdateDisp)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.btdateDisp)%>
					</div>
				</div>
			</div>
			<div class="col-md-2 " style="margin-top: 7px;">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.ptdateDisp)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.ptdateDisp)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText23)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.nextinsdteDisp)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.nextinsdteDisp)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText13)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.outstamt)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText14)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.aplamt)%>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText16)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.aplint)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText15)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.loanvalue)%>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText17)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%=smartHF.getHTMLVarExt(fw, sv.plint)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText24)%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-md-offset-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText19)%></label>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText25)%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText18)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.billfreq)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.billfreq)%>

						<%=smartHF.getHTMLVarExt(fw, sv.shortds01)%>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<div class="form-group">
					<div class="input-group three-controller">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.frequency, (sv.frequency.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0pc 0pc 0pc 0pc;"
								type="button"
								onclick="doFocus(document.getElementById('frequency')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%=smartHF.getHTMLVarExt(fw, sv.shortds02)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="margin-top: 7px;">
					<label><%=smartHF.getLit(generatedText20)%></label>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<table>
						<tr class="input-group">
							<td><div style="width: 300px;"><%=smartHF.getHTMLVarExt(fw, sv.instpramt)%></div></td>
							<td><div style="width: 300px;"><%=smartHF.getHTMLVarExt(fw, sv.nextinsamt)%></div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%
	}
%>

<%
	if (sv.Sr558protectWritten.gt(0)) {
%>
<%
	Sr558protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
