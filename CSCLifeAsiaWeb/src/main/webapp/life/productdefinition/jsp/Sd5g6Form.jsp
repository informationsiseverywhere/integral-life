<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5g6";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%
	Sd5g6ScreenVars sv = (Sd5g6ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life Assured");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Amount for Risk Class");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk Amount Total");
%>

<%
	appVars.rollup(new int[]{93});
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=generatedText2%>
					</label>
					<%
						}
					%>
					<table><tr><td>
						<%
							if ((new Byte((sv.clntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td style="padding-left:1px;">

						<%
							if ((new Byte((sv.clntnam).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
R

						<%
							if (!((sv.clntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnam.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnam.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label>  <%=generatedText4%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.lrkcls).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lrkcls.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lrkcls.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lrkcls.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td style="padding-left:1px;">

						<%
							if ((new Byte((sv.ldesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ldesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ldesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 125px;text-align: right;" >
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label  style="white-space: nowrap;"> <%=generatedText5%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.tranamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.tranamt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
								formatValue = smartHF.getPicFormatted(qpsf, sv.tranamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);

								if (!((sv.tranamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		
		<%
			GeneralTable sfl = fw.getTable("Sd5g6screensfl");

		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-Sd5g6' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Status")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Comp Code")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Component Description")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Billing Freq")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Mode Prem")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Term")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Prem Term")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Amount")%></th>
									
								</tr>
							</thead>

							<tbody>
								<%
									Sd5g6screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sd5g6screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
								<td>
								<%=count%>
								</td>
								
									<%
										if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.chdrnum.getFormData()%>
									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.statuz).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.statuz).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.statuz.getFormData()%>
									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.compcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.compcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.compcode.getFormData()%>
									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.compdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.compdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.compdesc.getFormData()%>
									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<td 
														<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																																
														<%	
															sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.sumin).getFieldName());						
															qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);				
														%>
														
																			
															<%
																formatValue = smartHF.getPicFormatted(qpsf,sv.sumin);
																if(!(sv.sumin).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																	formatValue = formatValue( formatValue );
																}
															%>
															<%= formatValue%>
															<%
																	longValue = null;
																	formatValue = null;
															%>
																			</td>
						
											<td> <%
											fieldItem=appVars.loadF4FieldsLong(new String[] {"payfreq"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("payfreq");
											longValue = (String) mappedItems.get((sv.payfreq.getFormData()).toString().trim());  
										%>
										<%-- div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%> --%>
	   		
	   		<%=longValue%>
	   		
	   		<%-- <%}%>  --%> </td>
								
									<td
										<%if((sv.modeprem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																																
														<%	
															sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.modeprem).getFieldName());						
															qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);				
														%>
														
																			
															<%
																formatValue = smartHF.getPicFormatted(qpsf,sv.modeprem);
																if(!(sv.modeprem).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																	formatValue = formatValue( formatValue );
																}
															%>
															<%= formatValue%>
															<%
																	longValue = null;
																	formatValue = null;
															%>
									</td>
									<td
<%if((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																																
														<%	
															sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.riskCessTerm).getFieldName());						
															qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);				
														%>
														
																			
															<%
																formatValue = smartHF.getPicFormatted(qpsf,sv.riskCessTerm);
																if(!(sv.riskCessTerm).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																	formatValue = formatValue( formatValue );
																}
															%>
															<%= formatValue%>
															<%
																	longValue = null;
																	formatValue = null;
															%></td>
															<%
										if ((new Byte((sv.premCessTerm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td style="text-align:right;"><%=sv.premCessTerm.getFormData()%>
									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									
									<td
													<%if((sv.riskamnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
														<%	
															sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.riskamnt).getFieldName());						
															qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);				
														%>
															<%
																formatValue = smartHF.getPicFormatted(qpsf,sv.riskamnt);
																if(!(sv.riskamnt).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																	formatValue = formatValue( formatValue );
																}
															%>
															<%= formatValue%>
															<%
																	longValue = null;
																	formatValue = null;
															%></td>

								</tr>

								<%
									count = count + 1;
										Sd5g6screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
		</div>
		<input type="hidden" id="totalRecords" value="<%=count-1%>" />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#dataTables-Sd5g6').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
		language: {
			"lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
			"info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
			"sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
			"sEmptyTable": dtmessage,
			"paginate": {
				"next":       nextval,
				"previous":   previousval
			}
		},
		stateSave: true,
		"fnInfoCallback": function( settings, iStart, iEnd, iMax, iTotal, sPre ) {
			var iTotal = $('#totalRecords').val();
			return showval + iStart + toval+ iEnd + ofval + iTotal + entriesval;
		},
  	});
})
</script>

<!-- <script>
	$(document).ready(function() {
		$('#dataTables-Sd5g6').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script> -->

<%@ include file="/POLACommon2NEW.jsp"%>

<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script language="javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>
<%
	}
%>
