<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5g4";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>


<%
	Sd5g4ScreenVars sv = (Sd5g4ScreenVars) fw.getVariables();
%>
<%sv.riskclass01.setClassString("");%>
<%	sv.riskclass01.appendClassString("string_fld");
	sv.riskclass01.appendClassString("input_txt");
	sv.riskclass01.appendClassString("highlight");
%>
	<%sv.riskclassdesc01.setClassString("");%>
<%	sv.riskclassdesc01.appendClassString("string_fld");
	sv.riskclassdesc01.appendClassString("output_txt");
	sv.riskclassdesc01.appendClassString("highlight");
	%>
	<%sv.formula01.setClassString("");%>
	<%	sv.formula01.appendClassString("string_fld");
	sv.formula01.appendClassString("output_txt");
	sv.formula01.appendClassString("highlight");
	%>
		<%sv.factor10.setClassString("");%>
	<%	sv.factor10.appendClassString("string_fld");
	sv.factor10.appendClassString("output_txt");
	sv.factor10.appendClassString("highlight");
	%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Class");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Class Description");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Formula");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factor");%>

	

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
				<table><tr><td>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%></td><td style="padding-left:1px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%></td></tr></table>
					
				</div>
			</div>
		</div>
	
    
        <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sd5g4' width='100%'>

			 <thead>
			    <tr class='info'>
			      <th><center><%=generatedText6%></center></th>        								
			       <th><center><%=generatedText7%></center></th>
			       <th><center><%=generatedText8%></center></th>
			       <th><center><%=generatedText9%></center></th>
				</tr>
			</thead>
			<tbody>	
       
	  		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass01)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass01)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
					
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc01, (sv.riskclassdesc01.getLength()))%>
					
					</div>
				
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%-- <%=smartHF.getHTMLVarExt(fw, sv.formula01).replaceAll("width:10px", "width:13px")%>
						 --%>
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula01)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula01)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor11, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass02)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass02)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc02, (sv.riskclassdesc02.getLength()))%>
					
					</div>
				
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula02).replaceAll("width:10px", "width:13px")%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula02).replaceAll("width:10px", "width:13px")%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>
		
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass03)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass03)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
					
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc03, (sv.riskclassdesc03.getLength()))%>
					
					</div>
				
			       
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula03)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula03)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor03, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass04)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass04)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
					
					
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc04, (sv.riskclassdesc04.getLength()))%>
					
					</div>
					
			       
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula04)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula04)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor04, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass05)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass05)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc05, (sv.riskclassdesc05.getLength()))%>
					
					</div>
				
			       
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula05)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula05)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor05, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass06)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass06)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc06, (sv.riskclassdesc06.getLength()))%>
					
					</div>
					
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula06)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula06)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor06, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>		
	   <tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass07)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass07)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc07, (sv.riskclassdesc07.getLength()))%>
					
					</div>
					
			       
			        
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula07)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula07)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor07, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
					</div>
				</div>
			</td>
		
		</tr>		
	<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass08)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass08)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc08, (sv.riskclassdesc08.getLength()))%>
					
					</div>
				
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula08)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula08)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						
						<%=smartHF.getHTMLVarExt(fw, sv.factor08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</td>
		
		</tr>
		
		<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass09)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass09)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc09, (sv.riskclassdesc09.getLength()))%>
					
					</div>
				  
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula09)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula09)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</td>
		
		</tr>
	
	<tr>
	  		
					<td>
						<div class="form-group">
		
			        
			           <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.riskclass10)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.riskclass10)%>
	                        </div>
			        </div>
				</td>
	

			<td>
					<div class="form-group">		
				
					<div class="input-group" style="min-width:140px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.riskclassdesc10, (sv.riskclassdesc10.getLength()))%>
					
					</div>
				
					</div>
				</td>
			<td>
				<div class="form-group">
					<div class="input-group" style="max-width:120px;min-width:71px;">
						<%=smartHF.getHTMLSpaceVar( fw, sv.formula10)%>
						<%=smartHF.getHTMLF4NSVarExt( fw, sv.formula10)%>
					</div>
				</div>
			</td>
			<td>
				<div class="form-group">
					<div style="min-width:70px;" div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.factor10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</td>
		
		</tr>

		</tbody>
		        
		</table>
		</div>
		</div>
		</div>
		
		<script>
$(document).ready(function() {
	$('#dataTables-sd5g4').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
	</div>
	
</div>
	<!-- Close div panel-body -->

<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

