

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sr28x";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr28xScreenVars sv = (Sr28xScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Extract ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Func ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Update ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "VP/MS Model ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Link STATUZ ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Link Copybook ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"List of Externalized Field IDs: ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fields to Process on ENDP: ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Log: ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Format Subr.: ");
%>


<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>


					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">


						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
		</div>

		<!-- row 2 -->
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr28x' width='100%'>
						<thead>
							<tr class='info'>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Extract ")%></td>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Func ")%></td>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Extract ")%></td>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Func ")%></td>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Update ")%></td>
								<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Data Log: ")%></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract01, (sv.dataextract01.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func01, (sv.func01.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract06, (sv.dataextract06.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func06, (sv.func06.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataupdate01, (sv.dataupdate01.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataLog, (sv.dataLog.getLength()))%>
								</td>
							</tr>

							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract02, (sv.dataextract02.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func02, (sv.func02.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract07, (sv.dataextract07.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func07, (sv.func07.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataupdate02, (sv.dataupdate02.getLength()))%>
								</td>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract03, (sv.dataextract03.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func03, (sv.func03.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract08, (sv.dataextract08.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func08, (sv.func08.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataupdate03, (sv.dataupdate03.getLength()))%>
								</td>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract04, (sv.dataextract04.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func04, (sv.func04.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract09, (sv.dataextract09.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func09, (sv.func09.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataupdate04, (sv.dataupdate04.getLength()))%>
								</td>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract05, (sv.dataextract05.getLength()))%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.func05, (sv.func05.getLength()))%>
								</td>
								<td>
									<%-- <%=smartHF.getRichTextInputFieldLookup(fw, sv.dataextract10, (sv.dataextract10.getLength()))%> --%>
								</td>
								<td>
									<%-- <%=smartHF.getRichTextInputFieldLookup(fw, sv.func10, (sv.func10.getLength()))%> --%>
								</td>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.dataupdate05, (sv.dataupdate05.getLength()))%>
								</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("VP/MS Model")%></label>
					<div style="width: 150px;">
						<input name='vpmmodel' type='text'
							<%if ((sv.vpmmodel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.vpmmodel.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vpmmodel.getLength()%>'
							maxLength='<%=sv.vpmmodel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(vpmmodel)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vpmmodel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vpmmodel).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vpmmodel).getColor() == null
						? "input_cell"
						: (sv.vpmmodel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Link STATUZ")%></label>
					<div style="width: 150px;">
						<input name='linkstatuz' type='text'
							<%if ((sv.linkstatuz).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.linkstatuz.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.linkstatuz.getLength()%>'
							maxLength='<%=sv.linkstatuz.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(linkstatuz)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.linkstatuz).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.linkstatuz).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.linkstatuz).getColor() == null
						? "input_cell"
						: (sv.linkstatuz).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Link Copybook")%></label>
					<div style="width: 150px;">
						<input name='linkcopybk' type='text'
							<%if ((sv.linkcopybk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.linkcopybk.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.linkcopybk.getLength()%>'
							maxLength='<%=sv.linkcopybk.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(linkcopybk)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.linkcopybk).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.linkcopybk).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.linkcopybk).getColor() == null
						? "input_cell"
						: (sv.linkcopybk).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Format Subr.")%></label>
					<div style="width: 150px;">
						<input name='vpmsubr' type='text'
							<%if ((sv.vpmsubr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.vpmsubr.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.vpmsubr.getLength()%>'
							maxLength='<%=sv.vpmsubr.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(vpmsubr)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vpmsubr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vpmsubr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vpmsubr).getColor() == null
						? "input_cell"
						: (sv.vpmsubr).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "List of Externalized Field IDs: ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table width='100%'>
					<tr>
						<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid01, (sv.extfldid01.getLength()))%></td>
						<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid02, (sv.extfldid02.getLength()))%></td>
						<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid03, (sv.extfldid03.getLength()))%></td>
						<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid04, (sv.extfldid04.getLength()))%></td>
						<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid05, (sv.extfldid05.getLength()))%></td>
					</tr>
					<tr>
						<td style="padding-top: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid06, (sv.extfldid06.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid07, (sv.extfldid07.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid08, (sv.extfldid08.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid09, (sv.extfldid09.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid10, (sv.extfldid10.getLength()))%></td>
					</tr>
					<tr>
						<td style="padding-top: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid11, (sv.extfldid11.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid12, (sv.extfldid12.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid13, (sv.extfldid13.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid14, (sv.extfldid14.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid15, (sv.extfldid15.getLength()))%></td>
					</tr>
					<tr>
						<td style="padding-top: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid16, (sv.extfldid16.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid17, (sv.extfldid17.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid18, (sv.extfldid18.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid19, (sv.extfldid19.getLength()))%></td>
						<td style="padding-top: 2px; padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid20, (sv.extfldid20.getLength()))%></td>
					</tr>
				</table>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="from-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fields to Process on ENDP: ")%></label>
					<table width='100%'>
						<tbody>
							<tr>
								<td><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid21, (sv.extfldid21.getLength()))%></td>
								<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid22, (sv.extfldid22.getLength()))%></td>
								<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid23, (sv.extfldid23.getLength()))%></td>
								<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid24, (sv.extfldid24.getLength()))%></td>
								<td style="padding-left: 2px;"><%=smartHF.getRichTextInputFieldLookup(fw, sv.extfldid25, (sv.extfldid25.getLength()))%></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
<script>
	$(document).ready(function() {
		$('#dataTables-sr28x').DataTable({
			ordering : false,
			searching : false,
			paging : false,
			info : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
		});
	});
</script>
<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6 starts  -->
<!-- <style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}

div[id*='func'] {
	padding-right: 3px !important
}

div[id*='dataextract'] {
	padding-right: 3px !important
}

div[id*='extfldid'] {
	padding-right: 3px !important
}

div[id*='dataupdate'] {
	padding-right: 3px !important
}

.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.table.border-none tr td, .table.border-none tr th {
	border: none !important;
}
</style> -->
<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6  ends -->