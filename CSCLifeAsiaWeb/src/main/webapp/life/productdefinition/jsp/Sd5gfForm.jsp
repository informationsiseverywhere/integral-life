<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SD5GF";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
Sd5gfScreenVars sv = (Sd5gfScreenVars) fw.getVariables();
%>



<%{
		if (appVars.ind01.isOn()) {
			sv.gender01.setReverse(BaseScreenData.REVERSED);
			sv.gender01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.gender01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.gender02.setReverse(BaseScreenData.REVERSED);
			sv.gender02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.gender02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.gender03.setReverse(BaseScreenData.REVERSED);
			sv.gender03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.gender03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.gender04.setReverse(BaseScreenData.REVERSED);
			sv.gender04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.gender04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ratebas.setReverse(BaseScreenData.REVERSED);
			sv.ratebas.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ratebas.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<%String formattedValue =""; %>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


</td><td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date Basis")%></label>
					<div style="width: 70px;">
						<input name='ratebas' type='text'
							<%if ((sv.ratebas).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.ratebas.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.ratebas.getLength()%>'
							maxLength='<%=sv.ratebas.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(ratebas)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.ratebas).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ratebas).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ratebas).getColor() == null ? "input_cell"
						: (sv.ratebas).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("1-Policy Issue Date")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("2-Acknowledgement Date")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("3-Deemed Received Date")%></label>
					</div>
					<div style="width:350px;">
						<label><%=resourceBundleHandler.gettingValueFromBundle("4-Min(Deemed Received Date & Acknowledgement Date)")%></label>
					</div>
				</div>
			</div>
		</div>
		<br/><br/>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Gender")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("From Age")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("To Age")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cooling off period")%></label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width: 50px;">
					<%=smartHF.getHTMLVarExt(fw, sv.gender01,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer frmage01Int = Integer.parseInt(sv.frmage01.getFormData());
					 	if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.frmage01.getFormData())) ? "" : String.valueOf(frmage01Int);
					 	else
						 	formattedValue = ("000".equals(sv.frmage01.getFormData())) ? "0" : String.valueOf(frmage01Int);
					%>
					<input name='frmage01' id='frmage01' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.frmage01.getLength()%>'
						size='<%=sv.frmage01.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(frmage01)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.frmage01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.frmage01).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.frmage01).getColor()== null  ? "input_cell" :  (sv.frmage01).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer toage01Int = Integer.parseInt(sv.toage01.getFormData());
					 	if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.toage01.getFormData())) ? "" : String.valueOf(toage01Int);
					 	else
						 	formattedValue = ("000".equals(sv.toage01.getFormData())) ? "0" : String.valueOf(toage01Int);
					 		
					%>
					<input name='toage01' id='toage01' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.toage01.getLength()%>'
						size='<%=sv.toage01.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(toage01)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.toage01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.toage01).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.toage01).getColor()== null  ? "input_cell" :  (sv.toage01).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer coolingoff1Int = Integer.parseInt(sv.coolingoff1.getFormData());
						if(sv.isCreateMode==true)
				 			formattedValue = ("000".equals(sv.coolingoff1.getFormData())) ? "" : String.valueOf(coolingoff1Int);
						else
					 		formattedValue = ("000".equals(sv.coolingoff1.getFormData())) ? "0" : String.valueOf(coolingoff1Int);
					%>
					<input name='coolingoff1' id='coolingoff1' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.coolingoff1.getLength()%>'
						size='<%=sv.coolingoff1.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(coolingoff1)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.coolingoff1).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.coolingoff1).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.coolingoff1).getColor()== null  ? "input_cell" :  (sv.coolingoff1).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
				
			</div>
			<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width: 50px;">
					<%=smartHF.getHTMLVarExt(fw, sv.gender02,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer frmage02Int = Integer.parseInt(sv.frmage02.getFormData());
						if(sv.isCreateMode==true)
							formattedValue = ("000".equals(sv.frmage02.getFormData())) ? "" : String.valueOf(frmage02Int);
						else
					 		formattedValue = ("000".equals(sv.frmage02.getFormData())) ? "0" : String.valueOf(frmage02Int);
					%>
					<input name='frmage02' id='frmage02' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.frmage02.getLength()%>'
						size='<%=sv.frmage02.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(frmage02)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.frmage02).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.frmage02).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.frmage02).getColor()== null  ? "input_cell" :  (sv.frmage02).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer toage02Int = Integer.parseInt(sv.toage02.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.toage02.getFormData())) ? "" : String.valueOf(toage02Int);
						else
							formattedValue = ("000".equals(sv.toage02.getFormData())) ? "0" : String.valueOf(toage02Int);
					%>
					<input name='toage02' id='toage02' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.toage02.getLength()%>'
						size='<%=sv.toage02.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(toage02)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.toage02).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.toage02).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.toage02).getColor()== null  ? "input_cell" :  (sv.toage02).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer coolingoff2Int = Integer.parseInt(sv.coolingoff2.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.coolingoff2.getFormData())) ? "" : String.valueOf(coolingoff2Int);
						else
							formattedValue = ("000".equals(sv.coolingoff2.getFormData())) ? "0" : String.valueOf(coolingoff2Int);
					%>
					<input name='coolingoff2' id='coolingoff2' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.coolingoff2.getLength()%>'
						size='<%=sv.coolingoff2.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(coolingoff2)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.coolingoff2).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.coolingoff2).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.coolingoff2).getColor()== null  ? "input_cell" :  (sv.coolingoff2).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
				
			</div>
			<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width: 50px;">
					<%=smartHF.getHTMLVarExt(fw, sv.gender03,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer frmage03Int = Integer.parseInt(sv.frmage03.getFormData());
						if(sv.isCreateMode==true)
							formattedValue = ("000".equals(sv.frmage03.getFormData())) ? "" : String.valueOf(frmage03Int);
						else
					 		formattedValue = ("000".equals(sv.frmage03.getFormData())) ? "0" : String.valueOf(frmage03Int);
					%>
					<input name='frmage03' id='frmage03' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.frmage03.getLength()%>'
						size='<%=sv.frmage03.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(frmage03)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.frmage03).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.frmage03).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.frmage03).getColor()== null  ? "input_cell" :  (sv.frmage03).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer toage03Int = Integer.parseInt(sv.toage03.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.toage03.getFormData())) ? "" : String.valueOf(toage03Int);
						else
							formattedValue = ("000".equals(sv.toage03.getFormData())) ? "0" : String.valueOf(toage03Int);
					%>
					<input name='toage03' id='toage03' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.toage03.getLength()%>'
						size='<%=sv.toage03.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(toage03)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.toage03).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.toage03).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.toage03).getColor()== null  ? "input_cell" :  (sv.toage03).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer coolingoff3Int = Integer.parseInt(sv.coolingoff3.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.coolingoff3.getFormData())) ? "" : String.valueOf(coolingoff3Int);
						else
							formattedValue = ("000".equals(sv.coolingoff3.getFormData())) ? "0" : String.valueOf(coolingoff3Int);
					%>
					<input name='coolingoff3' id='coolingoff3' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.coolingoff3.getLength()%>'
						size='<%=sv.coolingoff3.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(coolingoff3)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.coolingoff3).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.coolingoff3).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.coolingoff3).getColor()== null  ? "input_cell" :  (sv.coolingoff3).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
				
			</div>
			<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<div class="input-group" style="min-width: 50px;">
					<%=smartHF.getHTMLVarExt(fw, sv.gender04,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer frmage04Int = Integer.parseInt(sv.frmage04.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.frmage04.getFormData())) ? "" : String.valueOf(frmage04Int);
						else
							formattedValue = ("000".equals(sv.frmage04.getFormData())) ? "0" : String.valueOf(frmage04Int);
					%>
					<input name='frmage04' id='frmage04' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.frmage04.getLength()%>'
						size='<%=sv.frmage04.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(frmage04)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.frmage04).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.frmage04).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.frmage04).getColor()== null  ? "input_cell" :  (sv.frmage04).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer toage04Int = Integer.parseInt(sv.toage04.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.toage04.getFormData())) ? "" : String.valueOf(toage04Int);
						else
							formattedValue = ("000".equals(sv.toage04.getFormData())) ? "0" : String.valueOf(toage04Int);
					%>
					<input name='toage04' id='toage04' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.toage04.getLength()%>'
						size='<%=sv.toage04.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(toage04)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.toage04).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.toage04).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.toage04).getColor()== null  ? "input_cell" :  (sv.toage04).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						Integer coolingoff4Int = Integer.parseInt(sv.coolingoff4.getFormData());
						if(sv.isCreateMode==true)
					 		formattedValue = ("000".equals(sv.coolingoff4.getFormData())) ? "" : String.valueOf(coolingoff4Int);
						else
							formattedValue = ("000".equals(sv.coolingoff4.getFormData())) ? "0" : String.valueOf(coolingoff4Int);
					%>
					<input name='coolingoff4' id='coolingoff4' type='text'
						value='<%=formattedValue%>'
						maxLength='<%=sv.coolingoff4.getLength()%>'
						size='<%=sv.coolingoff4.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(coolingoff4)' onKeyUp='return checkMaxLength(this)'
						style="width:50px;"
						<%
							if((new Byte((sv.coolingoff4).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	>
						
						<%
							}else if((new Byte((sv.coolingoff4).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						<%
							}else {
						%>
	
					class = ' <%=(sv.coolingoff4).getColor()== null  ? "input_cell" :  (sv.coolingoff4).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' >
	
					<%} %>
				</div>
			</div>
				
			</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
