

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR551";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr551ScreenVars sv = (Sr551ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "New IC No");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Old IC No");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Other IC No");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Proposal Date  ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Name           ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Date of Birth  ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Company Code   ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk Type      ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(1=Declined  2=Rates Up/Postponed  3=Hospl. 4=Death)");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Hospitalise    ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Days");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Medical Codes  ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Last Update    ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"System Flag    ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Extract Flag   ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Action Code    ");
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.mloldic.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mloldic.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.mloldic.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.mloldic.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.mlothic.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mlothic.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.mlothic.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.mlothic.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rskflg.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.rskflg.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.rskflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rskflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.mlhsperd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mlhsperd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.mlhsperd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.mlhsperd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.mlmedcde01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mlmedcde01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.mlmedcde01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.mlmedcde01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.mlmedcde02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mlmedcde02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.mlmedcde02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.mlmedcde02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.mlmedcde03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.mlmedcde03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.mlmedcde03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.mlmedcde03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind88.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New IC No")%></label>
					<div>
						<%
							if (!((sv.securityno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.securityno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.securityno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-bottom: 2px">
							<!-- ILIFE-2704 vdivisala-->
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Old IC No")%></label>
					<div class="input-group" style="min-width:100px">
						<input name='mloldic' type='text'
							<%formatValue = (sv.mloldic.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.mloldic.getLength()%>'
							maxLength='<%=sv.mloldic.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(mloldic)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.mloldic).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.mloldic).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.mloldic).getColor() == null ? "input_cell"
						: (sv.mloldic).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Other IC No")%></label>
					<div class="input-group" style="min-width:100px">
						<input name='mlothic' type='text'
							<%formatValue = (sv.mlothic.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.mlothic.getLength()%>'
							maxLength='<%=sv.mlothic.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(mlothic)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.mlothic).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.mlothic).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.mlothic).getColor() == null ? "input_cell"
						: (sv.mlothic).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<div>
						<%
							if (!((sv.mlentity.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mlentity.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mlentity.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-bottom: 2px">
							<!-- ILIFE-2704 vdivisala-->
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Proposal Date")%></label>
					<div class="input-group">
					<% if ((new Byte((sv.hpropdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.hpropdteDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="hpropdteDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.hpropdteDisp, (sv.hpropdteDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
					
					
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<input name='hpropdteDisp' type='text'
							value='<%=sv.hpropdteDisp.getFormData()%>'
							maxLength='<%=sv.hpropdteDisp.getLength()%>'
							size='<%=sv.hpropdteDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(hpropdteDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.hpropdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.hpropdteDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.hpropdteDisp).getColor() == null ? "input_cell"
						: (sv.hpropdteDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>
					</div> --%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Name")%></label>
					<div class="input-group">
						<input name='clntname' type='text'
							<%formatValue = (sv.clntname.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.clntname.getLength()%>'
							maxLength='<%=sv.clntname.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(clntname)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.clntname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.clntname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.clntname).getColor() == null ? "input_cell"
						: (sv.clntname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date of Birth")%></label>
					<div class="input-group" style="min-width:100px;">
					<% if ((new Byte((sv.dobDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.dobDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.dobDisp, (sv.dobDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
					
					</div>
					
					
					
					
					
					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<input name='dobDisp' type='text'
							value='<%=sv.<% if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                    || fw.getVariables().isScreenProtected()) {       %>
                <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
<%}else{%>
        <div class="input-group date form_date col-md-12" data-date=""
               data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
               data-link-format="dd/mm/yyyy" style="width: 150px;">
                      <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
                      <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                      </span>
        </div>
               
<%}%> .getFormData()%>'
							maxLength='<%=sv.dobDisp.getLength()%>'
							size='<%=sv.dobDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dobDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dobDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.dobDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
						<%
							} else {
						%>

						class = '
						<%=(sv.dobDisp).getColor() == null ? "input_cell"
						: (sv.dobDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>
					</div> --%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company Code")%></label>
					<!-- <div class="input-group " style="max-width:100px"> -->
					<table><tr><td>
						<%
							if (!((sv.mlcoycde.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mlcoycde.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mlcoycde.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:30px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


						</td><td>



						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "mlcoycde" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("mlcoycde");
							longValue = (String) mappedItems.get((sv.mlcoycde.getFormData()).toString().trim());
						%>



						<%
							if (!((sv.desi.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.desi.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.desi.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;min-width:100px">
							<!-- ILIFE-2704 vdivisala-->
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Type")%></label>
					<div style="width: 100px;">
						<%
							longValue = null;
							fieldItem = appVars.loadF4FieldsLong(new String[] { "rskflg" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("rskflg");
							optionValue = makeDropDownList(mappedItems, sv.rskflg.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.rskflg.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.rskflg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div
							class='<%=(sv.rskflg.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.rskflg.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.rskflg.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.rskflg.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
							%>
							<%
								if (sv.rskflg.getFormData().equalsIgnoreCase("1 ")) {
							%>
							<%=resourceBundleHandler.gettingValueFromBundle("Declined")%>
							<%
								}
							%>
							<%
								if (sv.rskflg.getFormData().equalsIgnoreCase("2 ")) {
							%>
							<%=resourceBundleHandler.gettingValueFromBundle("Rates")%>
							<%
								}
							%>
							<%
								if (sv.rskflg.getFormData().equalsIgnoreCase("3 ")) {
							%>
							<%=resourceBundleHandler.gettingValueFromBundle("Hospl")%>
							<%
								}
							%>
							<%
								if (sv.rskflg.getFormData().equalsIgnoreCase("4 ")) {
							%>
							<%=resourceBundleHandler.gettingValueFromBundle("Death")%>
							<%
								}
							%>
							<%-- 		<%=formatValue%> --%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<!-- ILIFE-1066 start kpalani6 -->
						<select value='<%=sv.rskflg.getFormData()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rskflg)'
							onKeyUp='return checkMaxLength(this)' name='rskflg'
							<%if ((new Byte((sv.rskflg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
							<%
								} else if ((new Byte((sv.rskflg).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%> class="bold_cell">

							<%
								} else {
							%> class = '<%=(sv.rskflg).getColor() == null ? "input_cell"
							: (sv.rskflg).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'>

							<%
								}
							%>
							<option value="">...</option>
							<option value="1 "
								<%if (sv.rskflg.getFormData().equalsIgnoreCase("1 ")) {%>
								Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Declined")%></option>
							<option value="2 "
								<%if (sv.rskflg.getFormData().equalsIgnoreCase("2 ")) {%>
								Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Rates")%></option>
							<option value="3 "
								<%if (sv.rskflg.getFormData().equalsIgnoreCase("3 ")) {%>
								Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Hospl")%></option>
							<option value="4 "
								<%if (sv.rskflg.getFormData().equalsIgnoreCase("4 ")) {%>
								Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Death")%></option>
						</select>
						<!-- IILFE-1066 end kpalani6 -->
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Hospitalise")%></label>
					<table>
						<tr>
							<td>
								<%
									qpsf = fw.getFieldXMLDef((sv.mlhsperd).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								%> <input name='mlhsperd' type='text'
								value='<%=smartHF.getPicFormatted(qpsf, sv.mlhsperd)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.mlhsperd);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.mlhsperd)%>' <%}%>
								size='<%=sv.mlhsperd.getLength()%>'
								maxLength='<%=sv.mlhsperd.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(mlhsperd)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.mlhsperd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.mlhsperd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.mlhsperd).getColor() == null ? "input_cell"
						: (sv.mlhsperd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							</td>
							<td>&nbsp;&nbsp;&nbsp;</td>
							<td style="font-size:14px"><label> <%=resourceBundleHandler.gettingValueFromBundle("Days")%></label></td>
						</tr>
					</table>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Medical Codes")%></label>
					<table>
						<tr >
							<td style="min-width:80px;">
								<%
									longValue = sv.mlmedcde01.getFormData();
								%> <%
 	if ((new Byte((sv.mlmedcde01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<!-- ILIFE-2704 vdivisala-->
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 		%>
 		<%
        if ((new Byte((sv.mlmedcde01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                      || fw.getVariables().isScreenProtected()) {
 %>

 <div class="input-group" style="width: 100px;">
               <%=smartHF.getHTMLVarExt(fw, sv.mlmedcde01)%>
                       
        
 </div>
 <%
        } else {
 %>
 <div class="input-group" style="width: 100px;">
        <%=smartHF.getRichTextInputFieldLookup(fw, sv.mlmedcde01)%>
        <span class="input-group-btn">
               <button class="btn btn-info"
                      style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                      type="button"
                      onClick="doFocus(document.getElementById('mlmedcde01')); doAction('PFKEY04')">
                      <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
        </span>
 </div>
 <%
        }
 %>		
 		
 		
 		
 		
 		
 		
 	<%
 		longValue = null;
 	}
 %>
							</td> 
							<td style="min-width:80px;">
								<%
									if (!((sv.descrip01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip01.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip01.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>

								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<!-- ILIFE-2704 vdivisala-->

									<%=XSSFilter.escapeHtml(formatValue)%>

								</div>
							</td>
							
							<td style="min-width:80px;padding-left:2px;">
								<%
									longValue = null;
									formatValue = null;
								%> <%
 	longValue = sv.mlmedcde02.getFormData();
 %> <%
 	if ((new Byte((sv.mlmedcde02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>

								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<!-- ILIFE-2704 vdivisala-->
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> 
 <%
        if ((new Byte((sv.mlmedcde02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                      || fw.getVariables().isScreenProtected()) {
 %>

 <div class="input-group" style="width: 100px;">
               <%=smartHF.getHTMLVarExt(fw, sv.mlmedcde02)%>
                       
        
 </div>
 <%
        } else {
 %>
 <div class="input-group" style="width: 100px;">
        <%=smartHF.getRichTextInputFieldLookup(fw, sv.mlmedcde02)%>
        <span class="input-group-btn">
               <button class="btn btn-info"
                      style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                      type="button"
                      onClick="doFocus(document.getElementById('mlmedcde02')); doAction('PFKEY04')">
                      <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
        </span>
 </div>
 <%
        }
 %>		
 
 
 

 	<%
 		longValue = null;
 	}
 %>
							</td>
							<td style="min-width:80px;">
								<%
									if (!((sv.descrip02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip02.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip02.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>


								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<!-- ILIFE-2704 vdivisala-->
									<%=XSSFilter.escapeHtml(formatValue)%>
							</td>
							
					
							<td style="min-width:80px;padding-left:2px;">
								<%
									longValue = null;
									formatValue = null;
								%> <%
 	longValue = sv.mlmedcde03.getFormData();
 %> <%
 	if ((new Byte((sv.mlmedcde03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>


								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<!-- ILIFE-2704 vdivisala-->
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
  <%
        if ((new Byte((sv.mlmedcde03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                      || fw.getVariables().isScreenProtected()) {
 %>

 <div class="input-group" style="width: 100px;">
               <%=smartHF.getHTMLVarExt(fw, sv.mlmedcde03)%>
                      <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
        
 </div>
 <%
        } else {
 %>
 <div class="input-group" style="width: 100px;">
        <%=smartHF.getRichTextInputFieldLookup(fw, sv.mlmedcde03)%>
        <span class="input-group-btn">
               <button class="btn btn-info"
                      style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                      type="button"
                      onClick="doFocus(document.getElementById('mlmedcde03')); doAction('PFKEY04')">
                      <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
        </span>
 </div>
 <%
        }
 %>		
 
 

 	<%
 		longValue = null;
 	}
 %>
							</td>
							<td style="min-width:80px;">
								<%
									if (!((sv.descrip03.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip03.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip03.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>

								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<!-- ILIFE-2704 vdivisala-->
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>

						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("System Flag")%></label>
					<div clas="input-group" >
						<%
							if (!((sv.indc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.indc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.indc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Extract Flag")%></label>
					<div clas="input-group" >
						<%
							if (!((sv.mind.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mind.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mind.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Action Code")%></label>
						<div clas="input-group">
						<%
							if (!((sv.actn.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.actn.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.actn.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Last Update")%></label>
					<div class="input-group">
					<% if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
					
					
					</div>
					
					
					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<input name='effdateDisp' type='text'
							value='<%=sv.effdateDisp.getFormData()%>'
							maxLength='<%=sv.effdateDisp.getLength()%>'
							size='<%=sv.effdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(effdateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.effdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.effdateDisp).getColor() == null ? "input_cell"
						: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>
					</div> --%>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;" class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("(1=Declined  2=Rates Up/Postponed  3=Hospl. 4=Death)")%></label>
					<div>
						<%
							if (!((sv.reptname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.reptname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.reptname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

