<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR674";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%Sr674ScreenVars sv = (Sr674ScreenVars) fw.getVariables();%>
<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Gross Premium");%>
<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fee");%>
<% StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium");%>
<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor          ");%>
<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");%>
<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Direct Debit   ");%>
<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Group          ");%>
<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary    ");%>
<%{ if (appVars.ind11.isOn()) {
			sv.totfld01.setReverse(BaseScreenData.REVERSED);
			sv.totfld01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.totfld01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.totfld02.setReverse(BaseScreenData.REVERSED);
			sv.totfld02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.totfld02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.totfld03.setReverse(BaseScreenData.REVERSED);
			sv.totfld03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.totfld03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.totfld04.setReverse(BaseScreenData.REVERSED);
			sv.totfld04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.totfld04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind77.isOn()) {
			sv.payind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.payind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind77.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//start ILIFE-794 Credit Card Invisible in Extra Info
		if (appVars.ind77.isOn()) {
			sv.crcind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.crcind.setEnabled(BaseScreenData.DISABLED);
		}
		//end ILIFE-794 Credit Card Invisible in Extra Info				
		if (appVars.ind77.isOn()) {
			sv.ddind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.ddind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind77.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind77.isOn()) {
			sv.grpind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.grpind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.aiind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.aiind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}
%>
<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cnt Stat ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prm Stat ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Incep Dt ");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid To  ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bill To  ");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner    ");%>
<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Assured  ");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency");%>
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Method   ");%>
<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Print    ");%>
<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "NFL Loan ");%>
<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Pol Loan ");%>
<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "O/S Prem ");%>
<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Interest ");%>
<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Interest ");%>
<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Suspense ");%>
<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     Yearly  ");%>
<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Half Yearly  ");%>
<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  Quarterly  ");%>
<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    Monthly  ");%>
<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    FortNightly  ");%>
<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Comp");%>
<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");%>
<%appVars.rollup(new int[] { 93 });%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind15.isOn()) {
			sv.billfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.billfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.mop.setReverse(BaseScreenData.REVERSED);
			sv.mop.setColor(BaseScreenData.RED);
		}
		if (appVars.ind15.isOn()) {
			sv.mop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.mop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind88.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind88.isOn()) {
			sv.prtshd.setInvisibility(BaseScreenData.INVISIBLE);
			sv.prtshd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.prtshd.setReverse(BaseScreenData.REVERSED);
			sv.prtshd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.prtshd.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-3735 starts*/
		if (appVars.ind72.isOn()) {
			sv.billday.setReverse(BaseScreenData.REVERSED);
			sv.billday.setColor(BaseScreenData.RED);
		}
		if (appVars.ind74.isOn()) {
			sv.billday.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind73.isOn()) {
			sv.billday.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind72.isOn()) {
			sv.billday.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-3735 ends*/
		if(appVars.ind42.isOn()){
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
			sv.zstpduty02.setInvisibility(BaseScreenData.INVISIBLE);
			sv.zstpduty03.setInvisibility(BaseScreenData.INVISIBLE);
			sv.zstpduty04.setInvisibility(BaseScreenData.INVISIBLE);
			sv.zstpduty05.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind29.isOn()) {
			sv.nextinsdte.setInvisibility(BaseScreenData.INVISIBLE);
			sv.nextinsdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind28.isOn()) {
			sv.tmpChg.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind25.isOn()) {
			sv.tmpChg.setReverse(BaseScreenData.REVERSED);
			sv.tmpChg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.tmpChg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.tmpChg.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>
<style>
.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

</style>
   <div class="panel panel-default" >
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
					</label>
					<%}%>
<table><tr><td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %><div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%}%>
							</td><td style="min-width:1px"></td><td>
								<%
									if ((new Byte((sv.cnttyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cnttyp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttyp.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttyp.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%}%>
							</td><td style="min-width:1px"></td>
							<td>
								<%
									if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="cntdesc" style="max-width: 170px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%}%>
							</td>

						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

					<table><tr><td>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "cntcurr" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
						%>
						<div class='output_cell'>
							<%=(sv.cntcurr.getFormData()).toString()%>
						</div>
</td><td style="min-width:1px"></td>
							<td>
						<div class='output_cell' style="max-width: 150px;" >
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>

				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<table><tr><td>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						%>
						<div class='output_cell'>
							<%=(sv.register.getFormData()).toString()%>
						</div>
</td><td style="min-width:1px"></td>
							<td>
						<div class='output_cell' >
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Cnt Stat")%>
					</label>
					<%
						}
					%>
					<div style="width: 140px;">
						<%
							if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Prm Stat")%>
					</label>
					<%}%>
					<div style="width: 110px;">
						<%
							if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Incep Dt")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Paid To")%>
					</label>
					<%
						}
					%>
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Bill To")%>
					</label>
					<%
						}
					%>
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					</label>
					<%
						}
					%>

					<table><tr><td>
						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<%
							if ((browerVersion.equals(Chrome)) || (browerVersion.equals(IE8))) {
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							}
						%>
						<%
							if ((browerVersion.equals(IE11))) {
						%>
						<div style="margin-left: -4px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td style="min-width:1px"></td>
							<td>
						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 170px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td></tr></table>

				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Assured")%>
					</label>
					<%
						}
					%>

					<table><tr><td>
						<%
							if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<%
							if ((browerVersion.equals(Chrome)) || (browerVersion.equals(IE8))) {
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							}
						%>
						<%
							if ((browerVersion.equals(IE11))) {
						%>
						<div 
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td style="min-width:1px"></td>
							<td>
						<%
							if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width: 170px;" >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td></tr></table>

				</div>
			</div>

		    <div class="col-md-4">
				<div class="form-group">
					<%
					if ((new Byte((sv.nextinsdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Next Bill Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 80px;">
						<%
					
							if ((new Byte((sv.nextinsdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.nextinsdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.nextinsdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.nextinsdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("billfreq");
								optionValue = makeDropDownList(mappedItems, sv.billfreq.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.billfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%
							if ("red".equals((sv.billfreq).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>
							<select name='billfreq' type='list' style="width: 170px;"
								<%if ((new Byte((sv.billfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.billfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.billfreq).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Method")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "mop" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("mop");
								optionValue = makeDropDownList(mappedItems, sv.mop.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.mop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%
							if ("red".equals((sv.mop).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>
							<select name='mop' type='list' style="width: 170px;"
								<%if ((new Byte((sv.mop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.mop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.mop).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<%
					if ((new Byte((sv.billday).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing day")%></label>
					<div>
						<%
							longValue = (sv.billday.getFormData()).toString().trim();
								if ((new Byte((sv.billday).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%
							if ("red".equals((sv.billday).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 80px;">
							<%
								}
							%>
							<select name='billday' style="width: 80px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(billday)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.billday).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.billday).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<option value="">--------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--------
								</option>
								<option value="01"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("01")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("01")%></option>
								<option value="02"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("02")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("02")%></option>
								<option value="03"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("03")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("03")%></option>
								<option value="04"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("04")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("04")%></option>
								<option value="05"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("05")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("05")%></option>
								<option value="06"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("06")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("06")%></option>
								<option value="07"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("07")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("07")%></option>
								<option value="08"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("08")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("08")%></option>
								<option value="09"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("09")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("09")%></option>
								<option value="10"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("10")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("10")%></option>
								<option value="11"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("11")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("11")%></option>
								<option value="12"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("12")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("12")%></option>
								<option value="13"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("13")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("13")%></option>
								<option value="14"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("14")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("14")%></option>
								<option value="15"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("15")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("15")%></option>
								<option value="16"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("16")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("16")%></option>
								<option value="17"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("17")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("17")%></option>
								<option value="18"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("18")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("18")%></option>
								<option value="19"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("19")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("19")%></option>
								<option value="20"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("20")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("20")%></option>
								<option value="21"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("21")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("21")%></option>
								<option value="22"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("22")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("22")%></option>
								<option value="23"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("23")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("23")%></option>
								<option value="24"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("24")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("24")%></option>
								<option value="25"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("25")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("25")%></option>
								<option value="26"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("26")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("26")%></option>
								<option value="27"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("27")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("27")%></option>
								<option value="28"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("28")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("28")%></option>
								<option value="29"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("29")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("29")%></option>
								<option value="30"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("30")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("30")%></option>
								<option value="31"
									<%if (((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("31")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("31")%></option>
							</select>
							<%
								if ("red".equals((sv.billday).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%
							}
								longValue = null;
						%>
					</div>
				</div>
				<%
					}
				%>
			</div>
			<!-- IBPLIFE-4822 start -->
						<div class="col-md-2">
				<%
					if ((new Byte((sv.tmpChg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Temporary Change")%></label>
					<div>
						<%
							longValue = (sv.tmpChg.getFormData()).toString().trim();
								if ((new Byte((sv.tmpChg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%
							if ("red".equals((sv.tmpChg).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 102px;">
							<%
								}
							%>
							<select name='tmpChg' style="width: 100px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(tmpChg)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.tmpChg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.tmpChg).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.tmpChg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
								<option value="N"
									<%if (((sv.tmpChg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
							</select>
							<%
								if ("red".equals((sv.tmpChg).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%
							}
								longValue = null;
						%>
					</div>
				</div>
				<%
					}
				%>
			</div>
			<!-- IBPLIFE-4822 end -->
			<div class="col-md-2">
				<%
					if ((new Byte((sv.prtshd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="form-group">
					<%
						if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Print")%>
					</label>
					<%
						}
					%>
					<div style="width: 50px;">
						<input name='prtshd' type='text'
							<%if ((sv.prtshd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.prtshd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.prtshd.getLength()%>'
							maxLength='<%=sv.prtshd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(prtshd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.prtshd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.prtshd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.prtshd).getColor() == null ? "input_cell"
							: (sv.prtshd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
				<%
					}
				%>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("NFL Loan")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							longValue = null;
							if ((new Byte((sv.zloanamt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Pol Loan")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zloanamt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("O/S Prem")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zloanamt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Interest")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zloanamt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Interest")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zloanamt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Suspense")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.zloanamt06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zloanamt06).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zloanamt06,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zloanamt06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
				 <table  id='dataTables-sr674' class="table table-striped table-bordered table-hover" width="100%"  >
						<thead>
							<tr class='info'>
								<th style="width: 60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Component")%></center></th>
								<th style="width: 80px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
	 <%if (sv.actionflag.compareTo("Y") == 0  || sv.actionflag.compareTo("") == 0) {%>
	 <th><center><%=resourceBundleHandler.gettingValueFromBundle("Yearly")%></center></th>
	 <%}%>
	 <%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%><th><center><%=resourceBundleHandler.gettingValueFromBundle("Half Yearly")%></center></th><%}%>
	 <%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%><th><center><%=resourceBundleHandler.gettingValueFromBundle("Quaterly")%></center></th><%}%>
	 <%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%><th><center><%=resourceBundleHandler.gettingValueFromBundle("Monthly")%></center></th><%}%>
	 <%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%><th><center><%=resourceBundleHandler.gettingValueFromBundle("FortNightly")%></center></th><%}%>
							</tr>
						</thead>
						<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[7];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.crtable.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 8;
			} else {
				calculatedValue = (sv.crtable.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.entity.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 8;
			} else {
				calculatedValue = (sv.entity.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.prmtrb01.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 8;
			} else {
				calculatedValue = (sv.prmtrb01.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.prmtrb02.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 8;
			} else {
				calculatedValue = (sv.prmtrb02.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.prmtrb03.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 8;
			} else {
				calculatedValue = (sv.prmtrb03.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.prmtrb04.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 8;
			} else {
				calculatedValue = (sv.prmtrb04.getFormData()).length() * 10;
			}
			//calculatedValue += 20;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr674screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
						<tbody>	<%
	Sr674screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr674screensfl
	.hasMoreScreenRows(sfl)) {	

%>

							<tr style="height: 25px;" class="tableRowTag">
			<td class="tableDataTag tableDataTagFixed" style="width:80px;" align="center">&nbsp;</td>									
			<td class="tableDataTag tableDataTagFixed" style="width:80px;" align="center">&nbsp;</td>

			<%if (sv.actionflag.compareTo("Y") == 0 || sv.actionflag.compareTo("") == 0) {%><td class="tableDataTag tableDataTagFixed" style="width:80px;" align="right"><%=sv.effdate01Disp.getFormData()%></td><%}%> 
			<%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%><td class="tableDataTag tableDataTagFixed" style="width:80px;" align="right"><%=sv.effdate02Disp.getFormData()%></td><%}%>
			<%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%><td class="tableDataTag tableDataTagFixed" style="width:80px;" align="right"><%=sv.effdate03Disp.getFormData()%></td><%}%>
			<%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%><td class="tableDataTag tableDataTagFixed" style="width:80px;" align="right"><%=sv.effdate04Disp.getFormData()%></td><%}%>

			<%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%><td class="tableDataTag tableDataTagFixed" style="width:80px;" align="right"><%=sv.effdate05Disp.getFormData()%></td><%}%>
		
</tr> 
<!-- ILIFE-2805 ends -->
	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.crtable).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:80px;" 
					<%if((sv.crtable).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >

						<%= sv.crtable.getFormData()%>

									</td>
		<%}else{%>
												<td class="tableDataTag tableDataTagFixed" style="width:80px;" >
					</td>														

					<%}%>
								<%if((new Byte((sv.entity).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.entity).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >									

						<%= sv.entity.getFormData()%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
		<% if (sv.csbil003flag.compareTo("N") != 0) { %>
		<%if (sv.actionflag.compareTo("Y") == 0 || sv.actionflag.compareTo("") == 0) {%>
		<%if((new Byte((sv.prmtrb01).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb01).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb01).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
				<%} %>
					<%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%>
								<%if((new Byte((sv.prmtrb02).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb02).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb02).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%><%}%>
		<%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%>
								<%if((new Byte((sv.prmtrb03).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb03).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb03).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
			<%}%>
	<%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%>
				<%if((new Byte((sv.prmtrb04).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:100px;" 
					<%if((sv.prmtrb04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb04).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb04).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
			<%}else{%>
						<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
			<%}%>
			<%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%>
	<%if((new Byte((sv.prmtrb05).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:100px;" 
					<%if((sv.prmtrb05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb05).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb05).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>

					<%}%><%}%>

					<%} else {%>

					<%if((new Byte((sv.prmtrb01).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb01).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb01).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
								<%if((new Byte((sv.prmtrb02).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb02).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb02).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
								<%if((new Byte((sv.prmtrb03).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:80px;" 
					<%if((sv.prmtrb03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb03).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb03).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
								<%if((new Byte((sv.prmtrb04).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:100px;" 
					<%if((sv.prmtrb04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb04).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb04).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>
			<%}else{%>
						<td class="tableDataTag" style="width:80px;" >

				    </td>

					<%}%>
								<%if((new Byte((sv.prmtrb05).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:100px;" 
					<%if((sv.prmtrb05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									

					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.prmtrb05).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);				
					%>

						<%
						formatValue = smartHF.getPicFormatted(qpsf,sv.prmtrb05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
							if(!(sv.prmtrb05).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>

									</td>

					<%} %>
						<%} %>

	</tr>
	<%
	count = count + 1;
	Sr674screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%></tbody>
</table>
				</div>
			</div>
		</div>
		<br />

<table>
<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label style="margin-top: 6px;"> <%=resourceBundleHandler.gettingValueFromBundle("Gross Premium")%>
					</label>
					<%
						}
					%>
				</div>
			</div>
			 <% if (sv.csbil003flag.compareTo("N") != 0) { %>
					<%if (sv.actionflag.compareTo("Y") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>

						<%
							if ((new Byte((sv.zgrsamt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zgrsamt01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else { %><div class="output_cell"  id="zgrsamt01"></div>
<%}%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zgrsamt02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else { %><div class="output_cell"  id="zgrsamt02"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zgrsamt03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else { %><div class="output_cell" id="zgrsamt03"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zgrsamt04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else { %><div class="output_cell"  id="zgrsamt04"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zgrsamt05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else { %><div class="output_cell"  id="zgrsamt05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%} else { %>
				
					<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zgrsamt01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zgrsamt01"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>		
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zgrsamt02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zgrsamt02"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>		
					<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zgrsamt03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zgrsamt03"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zgrsamt04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%} else {%><div class="output_cell" id="zgrsamt04"></div>

									<% 
										} 
									%> 
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zgrsamt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zgrsamt05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zgrsamt05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zgrsamt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zgrsamt05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
								<%} else {%><div class="output_cell"  id="zgrsamt05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
	
					<%} %>
		</div>




		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label style="margin-top: 6px;"> <%=resourceBundleHandler.gettingValueFromBundle("Fee")%>
					</label>
					<%
						}
					%>
				</div>
			</div>
			 <% if (sv.csbil003flag.compareTo("N") != 0) { %>
					<%if (sv.actionflag.compareTo("Y") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zchgamt01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%} else {%>
									
									<div class="output_cell" id="zchgamt01"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zchgamt02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
								<%} else {%>
								<div class="output_cell"  id="zchgamt02"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zchgamt03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%} else {%>
<div class="output_cell"  id="zchgamt03"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

							if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"   id="zchgamt04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zchgamt04"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
			<%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zchgamt05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zchgamt05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
					<%} else { %>

								<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zchgamt01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell" id="zchgamt01"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zchgamt02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zchgamt02"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zchgamt03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zchgamt03"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

							if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="zchgamt04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell" id="zchgamt04"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.zchgamt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.zchgamt05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zchgamt05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.zchgamt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="zchgamt05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="zchgamt05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
		</div>
		
		<% if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label style="margin-top: 6px;"><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
					</div>
				</div>
					<div class="col-md-2">
						<div class="form-group">
							<%
								qpsf = fw.getFieldXMLDef((sv.zstpduty01).getFieldName());
								formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!((sv.zstpduty01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue); 
									} else {
										formatValue = formatValue( longValue);
									}							
								}
							%>
								
							<div id='stampduty01' align="right" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<%
								qpsf = fw.getFieldXMLDef((sv.zstpduty02).getFieldName());
								formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!((sv.zstpduty02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue); 
									} else {
										formatValue = formatValue( longValue);
									}							
								}
							%>
								
							<div id='stampduty02' align="right" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<%
								qpsf = fw.getFieldXMLDef((sv.zstpduty03).getFieldName());
								formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!((sv.zstpduty03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue); 
									} else {
										formatValue = formatValue( longValue);
									}							
								}
							%>
								
							<div id='stampduty03' align="right" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<%
								qpsf = fw.getFieldXMLDef((sv.zstpduty04).getFieldName());
								formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!((sv.zstpduty04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue); 
									} else {
										formatValue = formatValue( longValue);
									}							
								}
							%>
								
							<div id='stampduty04' align="right" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<%
								qpsf = fw.getFieldXMLDef((sv.zstpduty05).getFieldName());
								formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!((sv.zstpduty05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue); 
									} else {
										formatValue = formatValue( longValue);
									}							
								}
							%>
								
							<div id='stampduty05' align="right" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
			</div>
		<% } %>
		
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label style="margin-top: 6px;"> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%>
					</label>
					<%
						}
					%>
				</div>
			</div>

		 <% if (sv.csbil003flag.compareTo("N") != 0) { %>
		<%if (sv.actionflag.compareTo("Y") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell" id="totfld01"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div><%} %>
					<%if (sv.actionflag.compareTo("H") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="totfld02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="totfld02"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
		<%if (sv.actionflag.compareTo("Q") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="totfld03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell" id="totfld03"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>
			<%if (sv.actionflag.compareTo("M") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="totfld04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell"  id="totfld04"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>

				</div>
			</div><%} %>
			<%if (sv.actionflag.compareTo("F") == 0 || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell"  id="totfld05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>	<%} %>

			<%} else { %>
						<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="totfld01">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell" id="totfld01"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld02).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld02">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell"  id="totfld02"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld03).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld03">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell" id="totfld03"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld04).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld04">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell" id="totfld04"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.totfld05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.totfld05).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totfld05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								if (!((sv.totfld05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="totfld05">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div><%} else {%><div class="output_cell"  id="totfld05"></div>

									<% 
										} 
									%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
				<%} %>
		</div>

		  <jsp:include page="Sr674FormPart.jsp"></jsp:include>
		  </table>
		<div id="mainForm_OPTS" style="visibility: hidden;"><li>
							<span>				
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>

                                <li>
                                <%
										if (sv.payind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='payind' id='payind' type='hidden' value="<%=sv.payind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.payind.getInvisible()== BaseScreenData.INVISIBLE|| sv.payind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Payor")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" 
                                        onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Payor")%>
                                    <%}%>

                                    <!-- icon -->
                                    <%
                                    if (sv.payind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.payind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
								 <li><li>
							<span>				
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>

                                <li>
                                 <%
										if (sv.ddind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='ddind' id='ddind' type='hidden' value="<%=sv.ddind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Direct_Debit")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" 
                                        onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Direct_Debit")%>
                                    <%}%>

                                    <!-- icon -->
                                    <%
                                    if (sv.ddind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.ddind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
								 </li><li>
							<span>				
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>

                                <li>
                                <%
										if (sv.crcind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='crcind' id='crcind' type='hidden' value="<%=sv.crcind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.crcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.crcind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Credit Card")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" 
                                        onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("crcind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Credit Card")%>
                                    <%}%>

                                    <!-- icon -->
                                    <%
                                    if (sv.crcind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.crcind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
								 </li><li>
							<span>				
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>

                                <li>
                           <%
										if (sv.grpind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='grpind' id='grpind' type='hidden' value="<%=sv.grpind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.grpind.getInvisible()== BaseScreenData.INVISIBLE|| sv.grpind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Group")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" 
                                        onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("grpind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Group")%>
                                    <%}%>

                                    <!-- icon -->
                                    <%
                                    if (sv.grpind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.grpind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
								 </li><li>
							<span>				
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>

                                <li>
                           <%
										if (sv.aiind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='aiind' id='aiind' type='hidden' value="<%=sv.aiind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.aiind.getInvisible()== BaseScreenData.INVISIBLE|| sv.aiind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" 
                                        onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aiind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%>
                                    <%}%>

                                    <!-- icon -->
                                    <%
                                    if (sv.aiind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.aiind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
								 </li>
		</div>
		<div style="visibility: hidden;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if ((new Byte((sv.freqdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<%
								if (!((sv.freqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.freqdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.freqdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
									formatValue = null;
							%><%}%>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if ((new Byte((sv.mopdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
<%
								if (!((sv.mopdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.mopdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.mopdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
									formatValue = null;
							%>
						<%}%>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Yearly")%>
						</label>
						<%}%>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Half Yearly")%>
						</label>
						<%}%>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Quarterly")%>
						</label>
						<%}%>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Monthly")%>
						</label>
						<%}%>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("FortNightly")%>
						</label>
						<%}%>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Comp")%>
						</label>
						<%}%>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<%
							if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Description")%>
						</label>
						<%}%>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		if (screen.height == 900) {

			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {

			$('#cntdesc').css('max-width','190px')
		} 
		$('#dataTables-sr674').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
        	scrollX: true,
        	info:false,
        	paging:false
      	});

    });
</script>
<script type="text/javascript">
$(document).ready(function(){

	$("#zgrsamt01").css("width","100px");
	$("#zgrsamt02").css("width","100px");
	$("#zgrsamt03").css("width","100px");
	$("#zgrsamt04").css("width","100px");
	$("#zgrsamt05").css("width","100px");
	$("#zchgamt01").css("width","100px");
	$("#zchgamt02").css("width","100px");
	$("#zchgamt03").css("width","100px");
	$("#zchgamt04").css("width","100px");
	$("#zchgamt05").css("width","100px");
	$("#totfld01").css("width","100px");
	$("#totfld02").css("width","100px");
	$("#totfld03").css("width","100px");
	$("#totfld04").css("width","100px");
	$("#totfld05").css("width","100px");
	$("#amtfld01").css("width","100px");
	$("#amtfld02").css("width","100px");
	$("#amtfld03").css("width","100px");
	$("#amtfld04").css("width","100px");
	$("#amtfld05").css("width","100px");
	$("#stampduty01").css("width","100px");
	$("#stampduty02").css("width","100px");
	$("#stampduty03").css("width","100px");
	$("#stampduty04").css("width","100px");
	$("#stampduty05").css("width","100px");

});
</script> 
<%@ include file="/POLACommon2NEW.jsp"%>