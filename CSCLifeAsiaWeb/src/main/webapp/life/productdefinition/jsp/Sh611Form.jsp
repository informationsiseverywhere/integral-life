

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH611";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sh611ScreenVars sv = (Sh611ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contribution Limits");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Min");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Max");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.frequency01.setReverse(BaseScreenData.REVERSED);
			sv.frequency01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.frequency01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cmin01.setReverse(BaseScreenData.REVERSED);
			sv.cmin01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cmin01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.cmax01.setReverse(BaseScreenData.REVERSED);
			sv.cmax01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.cmax01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.frequency02.setReverse(BaseScreenData.REVERSED);
			sv.frequency02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.frequency02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.cmin02.setReverse(BaseScreenData.REVERSED);
			sv.cmin02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.cmin02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cmax02.setReverse(BaseScreenData.REVERSED);
			sv.cmax02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cmax02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.frequency03.setReverse(BaseScreenData.REVERSED);
			sv.frequency03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.frequency03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.cmin03.setReverse(BaseScreenData.REVERSED);
			sv.cmin03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.cmin03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.cmax03.setReverse(BaseScreenData.REVERSED);
			sv.cmax03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.cmax03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.frequency04.setReverse(BaseScreenData.REVERSED);
			sv.frequency04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.frequency04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.cmin04.setReverse(BaseScreenData.REVERSED);
			sv.cmin04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.cmin04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.cmax04.setReverse(BaseScreenData.REVERSED);
			sv.cmax04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.cmax04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.frequency05.setReverse(BaseScreenData.REVERSED);
			sv.frequency05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.frequency05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.cmin05.setReverse(BaseScreenData.REVERSED);
			sv.cmin05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.cmin05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.cmax05.setReverse(BaseScreenData.REVERSED);
			sv.cmax05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.cmax05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.frequency06.setReverse(BaseScreenData.REVERSED);
			sv.frequency06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.frequency06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.cmin06.setReverse(BaseScreenData.REVERSED);
			sv.cmin06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.cmin06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.cmax06.setReverse(BaseScreenData.REVERSED);
			sv.cmax06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.cmax06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.frequency07.setReverse(BaseScreenData.REVERSED);
			sv.frequency07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.frequency07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.cmin07.setReverse(BaseScreenData.REVERSED);
			sv.cmin07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.cmin07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.cmax07.setReverse(BaseScreenData.REVERSED);
			sv.cmax07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.cmax07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.frequency08.setReverse(BaseScreenData.REVERSED);
			sv.frequency08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.frequency08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cmin08.setReverse(BaseScreenData.REVERSED);
			sv.cmin08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cmin08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.cmax08.setReverse(BaseScreenData.REVERSED);
			sv.cmax08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.cmax08.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.to {
	margin-top: 34px !important;
	margin-left: -34px !important;
}

.leftDIV {
	margin-left: -40px !important;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-2 col-md-offset-1"> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
			<!-- <div class="col-md-5 col-md-offset-1"> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>
						<!-- INPUT-2 IN ITEM -->
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>

		<!-- Valid -->
		<div class="row">

			<%-- <div class="col-md-5">
				<div class="form-group">
					<label class="to"><%=resourceBundleHandler.gettingValueFromBundle("To")%></label></div></div> --%>
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table><tr><td>
					
							<%
								if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> 
							<%
							 	longValue = null;
							 	formatValue = null;
							 %>
							 </td><td style="padding-left:8px;">
						<label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>
						</td><td style="padding-left:8px;">
						
							<%
								if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> 
							<%
							 	longValue = null;
							 	formatValue = null;
							 %>
						</td></tr></table>
				</div>
			</div>
		</div>
		<!-- End Valid -->




		<!-- Start Contribution -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contribution Limits")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- FREQUENCY -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("frequency01");
						optionValue = makeDropDownList(mappedItems, sv.frequency01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.frequency01.getFormData()).toString().trim());
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 Start-->
					<% 
	if((new Byte((sv.frequency01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency01' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					<%-- <%=smartHF.getDropDownExt(sv.frequency01, fw, longValue, "frequency01", optionValue, 0, 240)%> --%>
					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency02");
						optionValue = makeDropDownList( mappedItems , sv.frequency02.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency02.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 Start-->
					<% 
	if((new Byte((sv.frequency02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:150px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency02' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					
					<%-- <%=smartHF.getDropDownExt(sv.frequency02, fw, longValue, "frequency02", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency03"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency03");
						optionValue = makeDropDownList( mappedItems , sv.frequency03.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency03.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
					<% 
	if((new Byte((sv.frequency03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency03' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					
					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency03", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency04");
						optionValue = makeDropDownList( mappedItems , sv.frequency04.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency04.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
<% 
	if((new Byte((sv.frequency04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency04' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency04", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency05");
						optionValue = makeDropDownList( mappedItems , sv.frequency05.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency05.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
<% 
	if((new Byte((sv.frequency05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency05' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency05", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency06"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency06");
						optionValue = makeDropDownList( mappedItems , sv.frequency06.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency06.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
<% 
	if((new Byte((sv.frequency06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency06' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency06", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency07");
						optionValue = makeDropDownList( mappedItems , sv.frequency07.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency07.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
<% 
	if((new Byte((sv.frequency07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency07' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency07", optionValue,0,240)%> --%>

					<br>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("frequency08");
						optionValue = makeDropDownList( mappedItems , sv.frequency08.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.frequency08.getFormData()).toString().trim());  
					%>
					<!--  Ilife- Life Cross Browser - Sprint 2 D2 : Task 4 starts-->
<% 
	if((new Byte((sv.frequency08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency08' type='list' style="width:120px;"
<% 
	if((new Byte((sv.frequency08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					<%-- <%=smartHF.getDropDownExt(sv.frequency03, fw, longValue, "frequency08", optionValue,0,240)%> --%>
				</div>
			</div>
			<!-- MIN -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Min")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.cmin01).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='cmin01' type='text'
						<%if ((sv.cmin01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin01);
						if (valueThis != null && valueThis.trim().length() > 0) {%>title='<%=smartHF.getPicFormatted(qpsf, sv.cmin01)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin01.getLength(), sv.cmin01.getScale(), 3)%>'
						maxLength='<%=sv.cmin01.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return
					fieldHelp(cmin01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return digitsOnly(this,<%=qpsf.getDecimals()%>
					); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return
					doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class='<%=(sv.cmin01).getColor() == null ? "input_cell": (sv.cmin01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>
						> 
						<br>
					<%	
					qpsf = fw.getFieldXMLDef((sv.cmin02).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
					valueThis=smartHF.getPicFormatted(qpsf,sv.cmin02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='cmin02' type='text'
						<%if((sv.cmin02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.cmin02) %>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.cmin02);
	 				if(valueThis!=null&& valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin02)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin02.getLength(), sv.cmin02.getScale(), 3)%>'
						maxLength='<%=sv.cmin02.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin02).getColor() == null ? "input_cell"
						: (sv.cmin02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%} %>
						> <br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmin03).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin03' type='text'
						<%if ((sv.cmin03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin03);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin03)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin03.getLength(), sv.cmin03.getScale(), 3)%>'
						maxLength='<%=sv.cmin03.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin03).getColor() == null ? "input_cell"
						: (sv.cmin03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmin04).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin04' type='text'
						<%if ((sv.cmin04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin04)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin04.getLength(), sv.cmin04.getScale(), 3)%>'
						maxLength='<%=sv.cmin04.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin04).getColor() == null ? "input_cell"
						: (sv.cmin04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmin05).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin05' type='text'
						<%if ((sv.cmin05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin05);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin05)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin05.getLength(), sv.cmin05.getScale(), 3)%>'
						maxLength='<%=sv.cmin05.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin05).getColor() == null ? "input_cell"
						: (sv.cmin05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmin06).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin06' type='text'
						<%if ((sv.cmin06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin06);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin06)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin06.getLength(), sv.cmin06.getScale(), 3)%>'
						maxLength='<%=sv.cmin06.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin06).getColor() == null ? "input_cell"
						: (sv.cmin06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmin07).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin07' type='text'
						<%if ((sv.cmin07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin07);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin07)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin07.getLength(), sv.cmin07.getScale(), 3)%>'
						maxLength='<%=sv.cmin07.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin07).getColor() == null ? "input_cell"
						: (sv.cmin07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmin08).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmin08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmin08' type='text'
						<%if ((sv.cmin08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmin08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmin08);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmin08)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin08.getLength(), sv.cmin08.getScale(), 3)%>'
						maxLength='<%=sv.cmin08.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmin08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmin08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmin08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmin08).getColor() == null ? "input_cell"
						: (sv.cmin08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>


				</div>
			</div>
			<!-- END MIN --- START MAX -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Max")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmax01).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax01' type='text'
						<%if ((sv.cmax01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax01)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax01.getLength(), sv.cmax01.getScale(), 3)%>'
						maxLength='<%=sv.cmax01.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax01).getColor() == null ? "input_cell"
						: (sv.cmax01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						
						<br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmax02).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax02' type='text'
						<%if ((sv.cmax02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax02)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax02)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax02.getLength(), sv.cmax02.getScale(), 3)%>'
						maxLength='<%=sv.cmax02.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax02).getColor() == null ? "input_cell"
						: (sv.cmax02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmax03).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax03' type='text'
						<%if ((sv.cmax03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax03)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax03.getLength(), sv.cmax03.getScale(), 3)%>'
						maxLength='<%=sv.cmax03.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax03).getColor() == null ? "input_cell"
						: (sv.cmax03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmax04).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax04' type='text'
						<%if ((sv.cmax04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax04)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax04.getLength(), sv.cmax04.getScale(), 3)%>'
						maxLength='<%=sv.cmax04.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax04).getColor() == null ? "input_cell"
						: (sv.cmax04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmax05).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax05' type='text'
						<%if ((sv.cmax05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax05)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax05.getLength(), sv.cmax05.getScale(), 3)%>'
						maxLength='<%=sv.cmax05.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax05).getColor() == null ? "input_cell"
						: (sv.cmax05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>

					<%
						qpsf = fw.getFieldXMLDef((sv.cmax06).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax06' type='text'
						<%if ((sv.cmax06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax06)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax06.getLength(), sv.cmax06.getScale(), 3)%>'
						maxLength='<%=sv.cmax06.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax06).getColor() == null ? "input_cell"
						: (sv.cmax06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmax07).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax07' type='text'
						<%if ((sv.cmax07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax07)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax07.getLength(), sv.cmax07.getScale(), 3)%>'
						maxLength='<%=sv.cmax07.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.cmax07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.cmax07).getColor() == null ? "input_cell"
						: (sv.cmax07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <br>


					<%
						qpsf = fw.getFieldXMLDef((sv.cmax08).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.cmax08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='cmax08' type='text'
						<%if ((sv.cmax08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.cmax08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cmax08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.cmax08)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax08.getLength(), sv.cmax08.getScale(), 3)%>'
						maxLength='<%=sv.cmax08.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(cmax08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cmax08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.cmax08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.cmax08).getColor()== null  ? 
			"input_cell" :  (sv.cmax08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
				</div>
			</div>
		</div>
	</div>
</div>





<%@ include file="/POLACommon2NEW.jsp"%>

