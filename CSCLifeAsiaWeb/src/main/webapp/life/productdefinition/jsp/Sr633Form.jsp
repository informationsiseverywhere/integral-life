<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR633";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr633ScreenVars sv = (Sr633ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind18.isOn()) {
			sv.optind01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind71.isOn()) {
			sv.optind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind51.isOn()) {
			sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.optind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.optind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind72.isOn()) {
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind52.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind73.isOn()) {
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind53.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.optind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<%
	{
		if (appVars.ind31.isOn()) {
			sv.zmedfee.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
			sv.zmedfee.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind31.isOn()) {
			sv.zmedfee.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.zmedfee.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.zmedtyp.setReverse(BaseScreenData.REVERSED);
			sv.zmedtyp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.zmedtyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind18.isOn()) {
			sv.zmedtyp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Client No        ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Code    ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Status           ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Area Code        ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Date Appointed   ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Date Terminated  ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Pay Method       ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Opening Hours    ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Facilities");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type of Med ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Standard Fees");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"-----------------------------------------------------------------------------");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind11.isOn()) {
			sv.zmednum.setReverse(BaseScreenData.REVERSED);
			sv.zmednum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.zmednum.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.zmednum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.zmedsts.setReverse(BaseScreenData.REVERSED);
			sv.zmedsts.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.zmedsts.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.zmedsts.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.dteappDisp.setReverse(BaseScreenData.REVERSED);
			sv.dteappDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.dteappDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.dteappDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.dtetrmDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.dtetrmDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.dtetrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.dtetrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.zdesc.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.zdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.xoptind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind50.isOn()) {
			sv.xoptind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.paymth.setReverse(BaseScreenData.REVERSED);
			sv.paymth.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.paymth.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind21.isOn()) {
			sv.paymth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.zaracde.setReverse(BaseScreenData.REVERSED);
			sv.zaracde.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.zaracde.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind41.isOn()) {
			sv.zaracde.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
		<table>
		<tr>
		<td>
		
		<%
                                         if ((new Byte((sv.zmednum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.zmednum)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.zmednum)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('zmednum')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
		</td><td style="padding-left:2px;">
		<%
									if (!((sv.zmednam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zmednam.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zmednam.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="border-color: #cccccc !important;margin-left: -1px;width: 120px;color: #9999a3 !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
		</td>
		</tr>
		</table>
		
		
		</div>
		
		</div>
		
		
		
		
			
	
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></label>
					<div class="input-group">
						<%
							if (!((sv.zmedprv.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
					<table>
		<tr>
		<td>
		
		<%
                                         if ((new Byte((sv.zmedsts).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.zmedsts)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.zmedsts)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('zmedsts')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
		</td><td style="padding-left:2px;">
		<%
									if (!((sv.statdets.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.statdets.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.statdets.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="border-color: #cccccc !important;margin-left: -1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
		</td>
		</tr>
		</table>
		
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
					<table>
		<tr>
		<td>
		
		<%
                                         if ((new Byte((sv.zaracde).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.zaracde)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.zaracde)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('zaracde')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
		</td><td style="padding-left:2px;">
		<%
									if (!((sv.coverDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.coverDesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.coverDesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="border-color: #cccccc !important;margin-left: -1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
		</td>
		</tr>
		</table>
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date Appointed")%></label>
					
					<% if ((new Byte((sv.dteappDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div><%=smartHF.getRichTextDateInput(fw, sv.dteappDisp)%>
                                      
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="dteappDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.dteappDisp, (sv.dteappDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
				
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date Terminated")%></label>
					
					<% if ((new Byte((sv.dtetrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div style="width:80px;"><%=smartHF.getRichTextDateInput(fw, sv.dtetrmDisp)%>
                                      
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="dtetrmDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.dtetrmDisp, (sv.dtetrmDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
             
		
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Method")%></label>
					<table>
		<tr>
		<td>
		
		<%
                                         if ((new Byte((sv.paymth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.paymth)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.paymth)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('paymth')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
		</td><td style="padding-left:2px;">
		<%
									if (!((sv.pymdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pymdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pymdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="border-color: #cccccc !important; margin-left: -1px;max-width: 155px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
		</td>
		</tr>
		</table>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Opening Hours")%></label>
					<div class="input-group">
						<input name='zdesc' type='text'
							<%formatValue = (sv.zdesc.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.zdesc.getLength()%>'
							maxLength='<%=sv.zdesc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zdesc)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zdesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zdesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zdesc).getColor() == null
						? "input_cell"
						: (sv.zdesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<%
			GeneralTable sfl = fw.getTable("sr633screensfl");
		%>
	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
	         <table  id='dataTables-sr633' class="table table-striped table-bordered table-hover" width="100%" >
               <thead>
		
			        <tr class="info">
									<th style="text-align: center;width: 230px;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>

									<th style="text-align: center;width: 340px;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>

									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr633screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr633screensfl.hasMoreScreenRows(sfl)) {
								%>
								<%
									{

											if (appVars.ind31.isOn()) {
												sv.zmedfee.setReverse(BaseScreenData.REVERSED);
											}
											if (appVars.ind41.isOn()) {
												sv.zmedfee.setEnabled(BaseScreenData.DISABLED);
											}
											if (appVars.ind31.isOn()) {
												sv.zmedfee.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind31.isOn()) {
												sv.zmedfee.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind18.isOn()) {
												sv.zmedtyp.setReverse(BaseScreenData.REVERSED);
												sv.zmedtyp.setColor(BaseScreenData.RED);
											}
											if (appVars.ind02.isOn()) {
												sv.zmedtyp.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind18.isOn()) {
												sv.zmedtyp.setHighLight(BaseScreenData.BOLD);
											}
										}
								%>
								<tr>
								
									<td style="width: 100px;" >	
									<div class="input-group">												
																	
											
				      	<% if((new Byte((sv.zmedtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>	
				      	<%= sv.zmedtyp.getFormData()%>
				      	<%}else{ %>		
						<input name='<%="sr633screensfl" + "." +
						 "zmedtyp" + "_R" + count %>'
						id='<%="sr633screensfl" + "." +
						 "zmedtyp" + "_R" + count %>'
						type='text' 
						value='<%= sv.zmedtyp.getFormData() %>' 
						class = " <%=(sv.zmedtyp).getColor()== null  ? 
						"input_cell" :  
						(sv.zmedtyp).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.zmedtyp.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(sr633screensfl.zmedtyp)' onKeyUp='return checkMaxLength(this)' 
						 style = "max-width: 150px;"
						>		
									
			 <span
										class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; border-bottom-width: 0px !important; right: -3px !important; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('<%="sr633screensfl" + "." + "zmedtyp" + "_R" + count%>')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
									</span> 
						
						
					
						<%} %>					
								</div>	</td>
									<%-- <td class="input-group">
										<%
											if ((new Byte((sv.zmedtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
										%> <%=sv.zmedtyp.getFormData()%> <%
 	} else {
 %> <input name='<%="sr633screensfl" + "." + "zmedtyp" + "_R" + count%>'
										id='<%="sr633screensfl" + "." + "zmedtyp" + "_R" + count%>'
										type='text' value='<%=sv.zmedtyp.getFormData()%>'
										class=" <%=(sv.zmedtyp).getColor() == null
							? "input_cell"
							: (sv.zmedtyp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
										maxLength='<%=sv.zmedtyp.getLength()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr633screensfl.zmedtyp)'
										onKeyUp='return checkMaxLength(this)'
										style="width: <%=sv.zmedtyp.getLength() * 12%>px;"> <span
										class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('<%="sr633screensfl" + "." + "zmedtyp" + "_R" + count%>')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
									</span> <%
 	}
 %>
									</td>
									 --%>
									
									
									<td><%=sv.longdesc.getFormData()%></td>
									<td class="tableDataTag"  align="right">




										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.zmedfee).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.zmedfee,
														COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										%> <%
 	if ((new Byte((sv.zmedfee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
 %> <%=formatValue%> <%
 	} else {
 %> <input type='text' maxLength='<%=sv.zmedfee.getLength()%>'
										value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zmedfee.getLength(), sv.zmedfee.getScale(), 3)%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr633screensfl.zmedfee)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr633screensfl" + "." + "zmedfee" + "_R" + count%>'
										id='<%="sr633screensfl" + "." + "zmedfee" + "_R" + count%>'
										class="input_cell"
										style="text-align: right; width: <%=sv.zmedfee.getLength() * 8%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>

										<%
											}
										%>

									</td>
									<%
										/*bug #ILIFE-383 end*/
									%>
									<!-- ILIFE-836 END -- Medical Provider Enquiry should have all fields in enquiry mode -->
								</tr>

								<%
									count = count + 1;
										Sr633screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%--  <Div id='mainForm_OPTS' style='visibility: hidden;'>

	<%=smartHF.getMenuLink(sv.optind01,sv.optdsc01
			.getFormData().replace(".","").trim())%>
			<li>


<input name='optind02' id='optind02' type='hidden' value="<%=sv.optind02.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind02.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind02
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdsc02
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))' class="hyperLink"> 
										<%=sv.optdsc02
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.optind02.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optind02.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind02'))"></i> 
			 						<%}%>
			 						</a>
 
</li>
<li>


<input name='optind03' id='optind03' type='hidden' value="<%=sv.optind03.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind02.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind02
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdsc03
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind03"))' class="hyperLink"> 
										<%=sv.optdsc03
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.optind03.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optind03.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind02'))"></i> 
			 						<%}%>
			 						</a>
 
</li>
			<%=smartHF.getMenuLink(sv.xoptind,sv.optdscx
			.getFormData().replace(".","").trim())%>
			 --%>
	</Div>

<Div id='mainForm_OPTS' style='visibility:hidden; width:60;'>

<li>


<input name='optind01' id='optind01' type='hidden' value="<%=sv.optind01.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind01.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind01
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdsc01
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind01"))' class="hyperLink"> 
										<%=sv.optdsc01
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.optind01.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optind01.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind01'))"></i> 
			 						<%}%>
			 						</a>
 
</li>

<li>


<input name='optind02' id='optind02' type='hidden' value="<%=sv.optind02.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind02.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind02
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdsc02
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))' class="hyperLink"> 
										<%=sv.optdsc02
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.optind02.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optind02.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind02'))"></i> 
			 						<%}%>
			 						</a>
 
</li>
<li>


<input name='optind03' id='optind03' type='hidden' value="<%=sv.optind03.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind03.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind03
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdsc03
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind03"))' class="hyperLink"> 
										<%=sv.optdsc03
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.optind03.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optind03.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind02'))"></i> 
			 						<%}%>
			 						</a>
 
</li>
<li>


<input name='xoptind' id='xoptind' type='hidden' value="<%=sv.xoptind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.optind02.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind02
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=sv.optdscx
.getFormData().replace(".","").trim()%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("xoptind"))' class="hyperLink"> 
										<%=sv.optdscx
.getFormData().replace(".","").trim()%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.xoptind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.xoptind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('xoptind'))"></i> 
			 						<%}%>
			 						</a>
 
</li>



</div>


<script>
$(document).ready(function() {
	$('#dataTables-sr633').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
paging:   true,
        scrollCollapse: true,
  	});
})
</script>


<%@ include file="/POLACommon2NEW.jsp"%>


