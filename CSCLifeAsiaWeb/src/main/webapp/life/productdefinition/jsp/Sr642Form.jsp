<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR642";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr642ScreenVars sv = (Sr642ScreenVars) fw.getVariables();
%>
<!-- ILIFE-1229 Starts-->
<script>
	function mapq(nm, val) {
		var location = val.options[val.selectedIndex].value;
		document.getElementById(nm).value = location;

	}
</script>
<!-- ILIFE-1229 Ends-->


<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract no   ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency  ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract status  ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium status  ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register  ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life assured     ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint life       ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Owner            ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint owner      ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Service Agent    ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Service Branch   ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type of");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/L");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Entity");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "ME");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "ME Date");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fee Amount");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Invoice No ");
%>
<%
	appVars.rollup(new int[]{93});
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left:1px;">
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left:1px;max-width:250px;">
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div>
						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract status")%></label>
					<div>
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium status")%></label>
					<div>
						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div>
						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
					<table>
						<tr>
							<td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

	
							</td><td style="padding-left:1px;max-width:250px;">

						<%
							if (!((sv.lifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifedesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifedesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
					<table>
						<tr>
							<td>
						<%
							if (!((sv.jlifeno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifeno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifeno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.jlifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifedesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifedesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width: 71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.ownerdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownerdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownerdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					</div>
				</div>
			
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint owner")%></label>
						<table>
						<tr>
							<td>
						<%
							if (!((sv.jownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.lname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Service Agent")%></label>
					<table>
						<tr>
							<td>
						<%
							if (!((sv.servagnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servagnt.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servagnt.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.servagnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servagnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servagnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Service Branch")%></label>
					<table>
						<tr>
							<td>
						<%
							if (!((sv.servbr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servbr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.servbr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.brchname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.brchname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.brchname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		
		<%
			GeneralTable sfl = fw.getTable("sr642screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>

		<div class="row">
			<div class="col-md-12">
				 
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr642' width='100%'>
							<thead>
								<tr class='info'>
									<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></center></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr642screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									//ILIFE-1229
									double sflLine = 0.0;
									Map<String, Map<String, String>> zMedTypeMap = appVars.loadF4FieldsLong(new String[]{"zmedtyp"}, sv, "E",
											baseModel);
									Map<String, Map<String, String>> paidByMap = appVars.loadF4FieldsLong(new String[]{"paidby"}, sv, "E",
											baseModel);
									while (Sr642screensfl.hasMoreScreenRows(sfl)) {

										{
											if (appVars.ind01.isOn()) {
												sv.zmedtyp.setReverse(BaseScreenData.REVERSED);
												sv.zmedtyp.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.zmedtyp.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind01.isOn()) {
												sv.zmedtyp.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind02.isOn()) {
												sv.life.setReverse(BaseScreenData.REVERSED);
												sv.life.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.life.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind02.isOn()) {
												sv.life.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind03.isOn()) {
												sv.jlife.setReverse(BaseScreenData.REVERSED);
												sv.jlife.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.jlife.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind03.isOn()) {
												sv.jlife.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind04.isOn()) {
												sv.paidby.setReverse(BaseScreenData.REVERSED);
												sv.paidby.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.paidby.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind04.isOn()) {
												sv.paidby.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind05.isOn()) {
												sv.exmcode.setReverse(BaseScreenData.REVERSED);
												sv.exmcode.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.exmcode.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind05.isOn()) {
												sv.exmcode.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind07.isOn()) {
												sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
												sv.effdateDisp.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind07.isOn()) {
												sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind08.isOn()) {
												sv.zmedfee.setReverse(BaseScreenData.REVERSED);
											}
											if (appVars.ind10.isOn()) {
												sv.zmedfee.setEnabled(BaseScreenData.DISABLED);
											}
											if (appVars.ind08.isOn()) {
												sv.zmedfee.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind08.isOn()) {
												sv.zmedfee.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind06.isOn()) {
												sv.invref.setReverse(BaseScreenData.REVERSED);
												sv.invref.setColor(BaseScreenData.RED);
											}
											if (appVars.ind10.isOn()) {
												sv.invref.setEnabled(BaseScreenData.DISABLED);
											}
											if (!appVars.ind06.isOn()) {
												sv.invref.setHighLight(BaseScreenData.BOLD);
											}
										}
								%>
								
								
								
								<tr>
								<div class="form-group">
									<td>
									
								<input type='hidden' 
						               value='<%= sv.zmedtyp.getFormData() %>'
						
						                name='<%="sr642screensfl" + "." +
						                       "zmedtyp" + "_R" + count %>'
						                id='<%="sr642screensfl" + "." +
					                        	 "zmedtyp" + "_R" + count %>'

> 
				 	  					
			        <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"zmedtyp"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("zmedtyp");
						optionValue = makeDropDownList( mappedItems , sv.zmedtyp,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.zmedtyp.getFormData()).toString()); 
					%>
					<% if((new Byte((sv.zmedtyp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
					<div class='output_cell'> 
					   <%=XSSFilter.escapeHtml(longValue)%>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.zmedtyp).getColor())){
					%>
					<div style="border:1px;  border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='<%="sr642screensfl.zmedtyp_C"+count%>' id='<%="sr642screensfl.zmedtyp_C"+count%>' type='list'" 
				
				onchange ="mapq('<%="sr642screensfl.zmedtyp_R"+count%>',this)";
					
					class = 'input_cell'
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.zmedtyp).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%> 
</div>
									</td></div>
									<td class="tableDataTag" align="left"> 
									<div class="form-group">
									<input type='text' maxLength='<%=sv.life.getLength()%>'
						
						value='<%= sv.life.getFormData() %>'
						size='<%=sv.life.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(sr642screensfl.life)'
						onKeyUp='return checkMaxLength(this)'
						name='<%="sr642screensfl" + "." +
						 "life" + "_R" + count %>'
						id='<%="sr642screensfl" + "." +
						 "life" + "_R" + count %>'
						style="width: 90px;"
						<% 
	if((new Byte((sv.life).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" 
<%
	}else if((new Byte((sv.life).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"

<%
	}else { 
%>

	class = ' <%=(sv.life).getColor()== null  ? 
			"input_cell" :  (sv.life).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</td>
									<td class="tableDataTag" align="center">
									<div class="form-group">
						 						 <input type='text' 
						maxLength='<%=sv.jlife.getLength()%>'
						 value='<%= sv.jlife.getFormData() %>' 
						 size='<%=sv.jlife.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(sr642screensfl.jlife)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr642screensfl" + "." +
						 "jlife" + "_R" + count %>'
						 id='<%="sr642screensfl" + "." +
						 "jlife" + "_R" + count %>'
						  style = "width: 90px;"
						  
						  <% 
	if((new Byte((sv.jlife).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" 
<%
	}else if((new Byte((sv.jlife).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"

<%
	}else { 
%>

	class = ' <%=(sv.jlife).getColor()== null  ? 
			"input_cell" :  (sv.jlife).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
		</div>				  
						  						 
										</td>
									<td class="tableDataTag" align="center">
									<div class="form-group">
										<!-- ILIFE-1229 Starts-->  <input type='hidden' 
						value='<%= sv.paidby.getFormData() %>'
						
						name='<%="sr642screensfl" + "." +
						 "paidby" + "_R" + count %>'
						id='<%="sr642screensfl" + "." +
						 "paidby" + "_R" + count %>'

> 
											
				      				     <%	
						mappedItems = (Map) paidByMap.get("paidby");
						optionValue = makeDropDownList( mappedItems , sv.paidby,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.paidby.getFormData()).toString().trim());  
					%>
					<% if((new Byte((sv.paidby).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
					<div class='output_cell'> 
					   <%=XSSFilter.escapeHtml(longValue)%>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.paidby).getColor())){
					%>
					<div style="border:1px;  border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='<%="sr642screensfl.paidby_C"+count%>' id='<%="sr642screensfl.paidby_C"+count%>' type='list' style="width:140px;"
						onchange ="mapq('<%="sr642screensfl.paidby_R"+count%>',this)";
					class = 'input_cell'
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.paidby).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>
				     </div>
									</td>
									
									<td align="left" style="min-width:200px;">
									<div class="form-group">
									<div class="input-group">
												<input name='<%="sr642screensfl" + "." +
						 "exmcode" + "_R" + count %>'
						id='<%="sr642screensfl" + "." +
						 "exmcode" + "_R" + count %>'
						type='text' 
						value='<%= sv.exmcode.getFormData() %>' 
						class = " <%=(sv.exmcode).getColor()== null  ? 
						"input_cell" :  
						(sv.exmcode).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.exmcode.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(sr642screensfl.exmcode)' onKeyUp='return checkMaxLength(this)' 
						
						>		
<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('<%="sr642screensfl" + "." +
								 "exmcode" + "_R" + count %>')); doAction('PFKEY04');">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
										</div>
									
										
										</div> 
									</td>
									
									
									
									
									<td class="tableDataTag" align="left" style="min-width:250px;">
									<div class="form-group"><%=sv.name.getFormData()%>
                                        </div>


									</td>
									<td class="tableDataTag" align="left" style="min-width:150px;">
									<div class="form-group">
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
											data-link-format="dd/mm/yyyy">
											<input style="width: 150px;"
												name='<%="sr642screensfl" + "." + "effdateDisp" + "_R" + count%>'
												id='<%="sr642screensfl" + "." + "effdateDisp" + "_R" + count%>'
												type='text' value='<%=sv.effdateDisp.getFormData()%>'
												class=" <%=(sv.effdateDisp).getColor() == null
						? "input_cell"
						: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.effdateDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr642screensfl.effdateDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell">

											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
									</td>
									<td class="tableDataTag" align="center">
									<div class="form-group">
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.zmedfee).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.zmedfee,
														COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										%> <input type='text' maxLength='<%=sv.zmedfee.getLength()%>'
										value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zmedfee.getLength(), sv.zmedfee.getScale(), 3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(sr642screensfl.zmedfee)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr642screensfl" + "." + "zmedfee" + "_R" + count%>'
										id='<%="sr642screensfl" + "." + "zmedfee" + "_R" + count%>'
										style="width: <%=sv.zmedfee.getLength() * 12 - 20%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'
										<%if ((new Byte((sv.zmedfee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.zmedfee).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.zmedfee).getColor() == null
							? "input_cell"
							: (sv.zmedfee).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%>>


                             </div>
									</td>
									<td class="tableDataTag" align="center">
									<div class="form-group">
									<input type='text'
										maxLength='<%=sv.invref.getLength()%>'
										value='<%=sv.invref.getFormData()%>'
										size='<%=sv.invref.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr642screensfl.invref)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr642screensfl" + "." + "invref" + "_R" + count%>'
										id='<%="sr642screensfl" + "." + "invref" + "_R" + count%>'
										style="width: <%=sv.invref.getLength() * 12%>px;"
										<%if ((new Byte((sv.invref).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.invref).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.invref).getColor() == null
							? "input_cell"
							: (sv.invref).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%>>
										</div>
										</td>

								</tr>

								<%
									count = count + 1;
										//ILIFE-1229
										sflLine += 1;
										Sr642screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		 
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-sr642').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true

		});
		$('#dataTables-sr642').on('draw.dt', function () { 
		    $(".date").datepicker({
		    	format: "dd/mm/yyyy",
		    	autoclose: true
		    });
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<%
	if (browerVersion.equals(Firefox) || browerVersion.equals(IE11) || browerVersion.equals

	(Chrome)) {
%>

<%
	}
%>
<!-- ILIFE-2348 ENDS -->
<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script type="text/javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>
<%
	}
%>
