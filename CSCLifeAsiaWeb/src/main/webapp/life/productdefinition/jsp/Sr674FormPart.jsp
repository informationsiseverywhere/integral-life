<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr674FormPart";%>

<%@page import="com.csc.life.productdefinition.screens.Sr674ScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.quipoz.framework.util.AppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>



<%-- Start preparing data --%>
<%
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;
String formatValue = null;
QPScreenField qpsf = null;
String valueThis = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();

LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

LifeAsiaAppVars appVars = av;

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), request.getLocale());

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%Sr674ScreenVars sv = (Sr674ScreenVars) fw.getVariables();%>

<%!
public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}

public String formatValue(String aValue) {
	return aValue;
}
%>

<div class="row">
			<div class="col-md-2">
			 <label><%=resourceBundleHandler.gettingValueFromBundle("Ttl w/ Tax")%> </label>
				</div>

			 <% if (sv.csbil003flag.compareTo("N") != 0) { %>
		 <%if (sv.actionflag.compareTo("Y") == 0  || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld01).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld01">	
												<%= formatValue%>
											</div><%} else {%><div class="output_cell" id="amtfld01"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
					<%} %>
				 <%if (sv.actionflag.compareTo("H") == 0  || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld02).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld02">	
												<%= formatValue%>
											</div><%} else {%><div class="output_cell"  id="amtfld02"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
					<%} %>
				 <%if (sv.actionflag.compareTo("Q") == 0  || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld03).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="amtfld03">	
												<%= formatValue%>
											</div><%} else {%><div class="output_cell"  id="amtfld03"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div><%} %>
		 <%if (sv.actionflag.compareTo("M") == 0  || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld04).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS	
								if (!((sv.amtfld04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="amtfld04">	
												<%= formatValue%>
											</div><%} else {%><div class="output_cell" id="amtfld04"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>	<%} %>

		 <%if (sv.actionflag.compareTo("F") == 0  || sv.actionflag.compareTo("") == 0) {%>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld05).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS	
								if (!((sv.amtfld05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld05">	
												<%= formatValue%>
											</div><%} else {%><div class="output_cell"  id="amtfld05"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
					<%} %>

					<%} else {  %>
					<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld01).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld01,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld01">	
												<%= formatValue%>
											</div>
									<%} else {%>

											<div class="output_cell" id="amtfld01"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld02).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld02,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld02">	
												<%= formatValue%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="amtfld02"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld03).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld03,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS
								if (!((sv.amtfld03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld03">	
												<%= formatValue%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell" id="amtfld03"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%}%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld04).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld04,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS	
								if (!((sv.amtfld04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right" id="amtfld04">	
												<%= formatValue%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="amtfld04"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.amtfld05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.amtfld05).getFieldName());
								//ILIFE-1474 STARTS
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S9ZEROVS2);
								formatValue = smartHF.getPicFormatted(qpsf, sv.amtfld05,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
								//ILIFE-1474 ENDS	
								if (!((sv.amtfld05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" align="right"  id="amtfld05">	
												<%= formatValue%>
											</div>
									<%
										} else {
									%>

											<div class="output_cell"  id="amtfld05"></div>
									<%}%>
						<%
							longValue = null;
								formatValue = null;
						%><%}%>
					</div>
				</div>
			</div>
					<%}%>
		</div>