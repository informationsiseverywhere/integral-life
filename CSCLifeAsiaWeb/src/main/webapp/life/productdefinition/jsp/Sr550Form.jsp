<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR550";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	Sr550ScreenVars sv = (Sr550ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr550screenWritten.gt(0)) {
%>
<%
	Sr550screen.clearClassString(sv);
%>
<%
	StringData generatedText4 = new StringData("A - Create LIA Record Details");
%>
<%
	StringData generatedText5 = new StringData("B - Modify LIA Record Details");
%>
<%
	StringData generatedText2 = new StringData("C - Delete LIA Record Details");
%>
<%
	StringData generatedText3 = new StringData("D - Enquiry on LIA Record  Details");
%>
<%
	StringData generatedText6 = new StringData("New/Old IC No    ");
%>
<%
	sv.securityno.setClassString("");
%>
<%
	StringData generatedText7 = new StringData("Contract No      ");
%>
<%
	sv.mlentity.setClassString("");
%>
<%
	StringData generatedText8 = new StringData("Action  ");
%>
<%
	sv.action.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.securityno.setReverse(BaseScreenData.REVERSED);
				sv.securityno.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.securityno.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.mlentity.setReverse(BaseScreenData.REVERSED);
				sv.mlentity.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.mlentity.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.action.setReverse(BaseScreenData.REVERSED);
				sv.action.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.action.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New/Old IC No")%></label>
					<div class="input-group">
					<%
                                         if ((new Byte((sv.securityno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.securityno)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.securityno)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('securityno')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
					
					</div>
					
					
					<%-- <div class="input-group" style="max-width:110px">
						<input name='securityno' type='text'
							value='<%=sv.securityno.getFormData()%>'
							maxLength='<%=sv.securityno.getLength()%>'
							size='<%=sv.securityno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(securityno)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.securityno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.securityno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  
type="button" onClick="doFocus(document.getElementById('securityno')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 
 



						<%
							} else {
						%>

						class = '
						<%=(sv.securityno).getColor() == null ? "input_cell"
							: (sv.securityno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						><span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  
type="button" onClick="doFocus(document.getElementById('securityno')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							}
						%>
					</div> --%>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<div class="input-group">
					<%
                                         if ((new Byte((sv.mlentity).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.mlentity)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.mlentity)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('mlentity')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
					
					</div>
					
					
						<%-- <div class="input-group" style="max-width:110px">
						<input name='mlentity' type='text'
							value='<%=sv.mlentity.getFormData()%>'
							maxLength='<%=sv.mlentity.getLength()%>'
							size='<%=sv.mlentity.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(mlentity)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.mlentity).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.mlentity).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" ><span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  
type="button" onClick="doFocus(document.getElementById('mlentity')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							} else {
						%>

						class = '
						<%=(sv.mlentity).getColor() == null ? "input_cell"
							: (sv.mlentity).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  
type="button" onClick="doFocus(document.getElementById('mlentity')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 
						<%
							}
						%>
					</div> --%>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Create LIA Record Details")%></b>
					</label>
				</div>
			</div>
				<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Modify LIA Record Details")%></b>
					</label>
				</div>
			</div>
				<div class="col-md-2"></div>
		</div>
		<div class="row">
		
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Delete LIA Record Details")%></b>
					</label>
				</div>
			</div>
				<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "D")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Enquiry on LIA Record  Details")%></b>
				</label></div>
			</div>
		</div>
			<div class="col-md-2"></div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%
	}
%>

<%
	if (sv.Sr550protectWritten.gt(0)) {
%>
<%
	Sr550protect.clearClassString(sv);
%>
<%
	}
%>

<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>


<%@ include file="/POLACommon2NEW.jsp"%>
