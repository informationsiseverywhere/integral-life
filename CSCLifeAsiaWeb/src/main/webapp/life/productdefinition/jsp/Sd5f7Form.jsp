<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SD5F7";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
Sd5f7ScreenVars sv = (Sd5f7ScreenVars) fw.getVariables();
	
%>
<%
	if (sv.Sd5f7screenWritten.gt(0)) {
%>
<%
	Sd5f7screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Status Item");
%>
<!--<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Current Status Code");
%>
<!--<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Set Status to");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premium");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " Regular Single");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract Risk");
%>

<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Premium");
%>






<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3"> 
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
					
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>

							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>




</td><td style="padding-left:1px;">




						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					</div>
				</div>
			</div>
		
		<br />
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

	
		
		<label ><%=resourceBundleHandler.gettingValueFromBundle("Annual Statement ")%></label>
	
		<div class="row">
		<div class="col-md-4"> 
				  
				    	<label style="margin-left: 20px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency ")%></label>
				    	</div>
				    	<div class="col-md-4"> 
				    	
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("billfreq");
						optionValue = makeDropDownList( mappedItems , sv.billfreq.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
						%>
						
						<%
							if((new Byte((sv.billfreq).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell'>
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
						
						<% if("red".equals((sv.billfreq).getColor())){
											%>
											<div 
											style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;">
											<%
											}
											%>
											<select name='billfreq' type='list' 
											<%
										if((new Byte((sv.billfreq).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
									%>
										readonly="true"
										<%-- MIBT-38 --%>
										disabled  onclick="document.getElementById('billcdDisp').value='';"
										class="output_cell"
									<%
										}else if((new Byte((sv.billfreq).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>
											<%-- MIBT-38 --%>
											class="bold_cell"  onclick="document.getElementById('billcdDisp').value='';"
									<%
										}else {
									%>
									<%-- MIBT-38 --%>
									class = 'input_cell' onclick="document.getElementById('billcdDisp').value='';"
									<%
										}
									%>
									>
											<%=optionValue%>
											</select>
											<% if("red".equals((sv.billfreq).getColor())){
											%>
											</div>
											<%
											}
											%>
											<%
							}
						%>
				   
				 </div>
				  </div>
	  
		<br/>		  
				    
	<div class="row">
	<div class="col-md-4"> 
				  
				    	<label style="margin-left: 20px;"><%=resourceBundleHandler.gettingValueFromBundle("Anniversary Indicator ")%></label>
				    	</div>
			
			<div class="col-md-4">
			<table><tr><td>
				<div class="input-group" style="width: 35px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.annind, (sv.annind.getLength()))%> 

				</div>
				</td><td  style="padding-left: 7px;">
				<label><%=resourceBundleHandler.gettingValueFromBundle("P=Policy C=Company")%></label>
				 </td></tr></table>
			</div>
			
		</div>
		
		<br/>		  
				    
	<div class="row">
	<div class="col-md-4"> 
				  
				    	<label style="margin-left: 20px;"><%=resourceBundleHandler.gettingValueFromBundle("If Company Anniversary")%></label>
				    	</div>
			
			<div class="col-md-4">
				<table><tr><td>
				<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmpday, (sv.cmpday.getLength()))%> 
				</td><td  style="padding-left: 7px;">
				<label><%=resourceBundleHandler.gettingValueFromBundle("/")%></label>
				</td><td style="padding-left: 7px;">
					<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmpmonth, (sv.cmpmonth.getLength()))%>
					</td><td  style="padding-left: 7px;">
						<label><%=resourceBundleHandler.gettingValueFromBundle("(DD/MM)")%></label>
					</td></tr></table> 

				
			</div>
			
		</div>
<br/>
<div class="row">
		<div class="col-md-4"> 
				  
				    	<label ><%=resourceBundleHandler.gettingValueFromBundle("Transaction Statement From Ind")%></label>
				    	</div>
			
			<div class="col-md-4">
			<table><tr><td>
				<div class="input-group" style="width: 35px;">
				<%=smartHF.getRichTextInputFieldLookup(fw, sv.transind, (sv.transind.getLength()))%> 

				</div>
				</td><td  style="padding-left: 7px;">
				<label><%=resourceBundleHandler.gettingValueFromBundle("C=Last Closing T=Last Transaction")%></label>
				</td></tr></table>
			</div>
			
		</div>
	
		

		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sd5f7protectWritten.gt(0)) {
%>
<%
	Sd5f7protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
