

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR594";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr594ScreenVars sv = (Sr594ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "U/W");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Plan Type");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"    Standard Limit");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sub Standard Limit");
%>

<%
	{
		appVars.rolldown();
		appVars.rollup();
		if (appVars.ind14.isOn()) {
			sv.uwplntyp01.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit01.setReverse(BaseScreenData.REVERSED);
			sv.undlimit01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim01.setReverse(BaseScreenData.REVERSED);
			sv.usundlim01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp02.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit02.setReverse(BaseScreenData.REVERSED);
			sv.undlimit02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim02.setReverse(BaseScreenData.REVERSED);
			sv.usundlim02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp03.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit03.setReverse(BaseScreenData.REVERSED);
			sv.undlimit03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim03.setReverse(BaseScreenData.REVERSED);
			sv.usundlim03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp04.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit04.setReverse(BaseScreenData.REVERSED);
			sv.undlimit04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim04.setReverse(BaseScreenData.REVERSED);
			sv.usundlim04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp05.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit05.setReverse(BaseScreenData.REVERSED);
			sv.undlimit05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim05.setReverse(BaseScreenData.REVERSED);
			sv.usundlim05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp06.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit06.setReverse(BaseScreenData.REVERSED);
			sv.undlimit06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim06.setReverse(BaseScreenData.REVERSED);
			sv.usundlim06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp07.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit07.setReverse(BaseScreenData.REVERSED);
			sv.undlimit07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim07.setReverse(BaseScreenData.REVERSED);
			sv.usundlim07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp08.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit08.setReverse(BaseScreenData.REVERSED);
			sv.undlimit08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim08.setReverse(BaseScreenData.REVERSED);
			sv.usundlim08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.uwplntyp09.setReverse(BaseScreenData.REVERSED);
			sv.uwplntyp09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.uwplntyp09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.undlimit09.setReverse(BaseScreenData.REVERSED);
			sv.undlimit09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.undlimit09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.usundlim09.setReverse(BaseScreenData.REVERSED);
			sv.usundlim09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.usundlim09.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>


						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("U/W Plan Type")%></label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Standard Limit")%></label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Standard Limit")%></label>
				</div>
			</div>
		</div>
		<div class="row">
		
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
				
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit01' type='text'
							<%if ((sv.undlimit01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit01.getLength(), sv.undlimit01.getScale(), 3)%>'
							maxLength='<%=sv.undlimit01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit01).getColor() == null ? "input_cell"
						: (sv.undlimit01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim01' type='text'
							<%if ((sv.usundlim01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim01.getLength(), sv.usundlim01.getScale(), 3)%>'
							maxLength='<%=sv.usundlim01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim01).getColor() == null ? "input_cell"
						: (sv.usundlim01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit02,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit02' type='text'
							<%if ((sv.undlimit02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit02.getLength(), sv.undlimit02.getScale(), 3)%>'
							maxLength='<%=sv.undlimit02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit02).getColor() == null ? "input_cell"
						: (sv.undlimit02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim02,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim02' type='text'
							<%if ((sv.usundlim02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim02.getLength(), sv.usundlim02.getScale(), 3)%>'
							maxLength='<%=sv.usundlim02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim02).getColor() == null ? "input_cell"
						: (sv.usundlim02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit03,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit03' type='text'
							<%if ((sv.undlimit03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit03.getLength(), sv.undlimit03.getScale(), 3)%>'
							maxLength='<%=sv.undlimit03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit03).getColor() == null ? "input_cell"
						: (sv.undlimit03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim03,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim03' type='text'
							<%if ((sv.usundlim03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim03.getLength(), sv.usundlim03.getScale(), 3)%>'
							maxLength='<%=sv.usundlim03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim03).getColor() == null ? "input_cell"
						: (sv.usundlim03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
				
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit04,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit04' type='text'
							<%if ((sv.undlimit04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit04.getLength(), sv.undlimit04.getScale(), 3)%>'
							maxLength='<%=sv.undlimit04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit04).getColor() == null ? "input_cell"
						: (sv.undlimit04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim04,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim04' type='text'
							<%if ((sv.usundlim04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim04.getLength(), sv.usundlim04.getScale(), 3)%>'
							maxLength='<%=sv.usundlim04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim04).getColor() == null ? "input_cell"
						: (sv.usundlim04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit05,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit05' type='text'
							<%if ((sv.undlimit05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit05.getLength(), sv.undlimit05.getScale(), 3)%>'
							maxLength='<%=sv.undlimit05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit05).getColor() == null ? "input_cell"
						: (sv.undlimit05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim05,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim05' type='text'
							<%if ((sv.usundlim05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim05.getLength(), sv.usundlim05.getScale(), 3)%>'
							maxLength='<%=sv.usundlim05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim05).getColor() == null ? "input_cell"
						: (sv.usundlim05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit06,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit06' type='text'
							<%if ((sv.undlimit06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit06.getLength(), sv.undlimit06.getScale(), 3)%>'
							maxLength='<%=sv.undlimit06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit06).getColor() == null ? "input_cell"
						: (sv.undlimit06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim06,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim06' type='text'
							<%if ((sv.usundlim06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim06.getLength(), sv.usundlim06.getScale(), 3)%>'
							maxLength='<%=sv.usundlim06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim06).getColor() == null ? "input_cell"
						: (sv.usundlim06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit07,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit07' type='text'
							<%if ((sv.undlimit07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit07.getLength(), sv.undlimit07.getScale(), 3)%>'
							maxLength='<%=sv.undlimit07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit07).getColor() == null ? "input_cell"
						: (sv.undlimit07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim07,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim07' type='text'
							<%if ((sv.usundlim07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim07.getLength(), sv.usundlim07.getScale(), 3)%>'
							maxLength='<%=sv.usundlim07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim07).getColor() == null ? "input_cell"
						: (sv.usundlim07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit08,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit08' type='text'
							<%if ((sv.undlimit08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit08.getLength(), sv.undlimit08.getScale(), 3)%>'
							maxLength='<%=sv.undlimit08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit08).getColor() == null ? "input_cell"
						: (sv.undlimit08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim08,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim08' type='text'
							<%if ((sv.usundlim08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim08.getLength(), sv.usundlim08.getScale(), 3)%>'
							maxLength='<%=sv.usundlim08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim08).getColor() == null ? "input_cell"
						: (sv.usundlim08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.uwplntyp09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.uwplntyp09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 80px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.uwplntyp09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('uwplntyp09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.undlimit09).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.undlimit09,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='undlimit09' type='text'
							<%if ((sv.undlimit09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.undlimit09.getLength(), sv.undlimit09.getScale(), 3)%>'
							maxLength='<%=sv.undlimit09.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(undlimit09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.undlimit09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.undlimit09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.undlimit09).getColor() == null ? "input_cell"
						: (sv.undlimit09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.usundlim09).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.usundlim09,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='usundlim09' type='text'
							<%if ((sv.usundlim09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: left; width:85px;" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.usundlim09.getLength(), sv.usundlim09.getScale(), 3)%>'
							maxLength='<%=sv.usundlim09.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(usundlim09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.usundlim09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.usundlim09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.usundlim09).getColor() == null ? "input_cell"
						: (sv.usundlim09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>












