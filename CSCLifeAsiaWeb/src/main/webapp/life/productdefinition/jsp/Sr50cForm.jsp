

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR50C";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr50cScreenVars sv = (Sr50cScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Create Policy Notes");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Enquiry Policy Notes");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Categories      ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.pnotecat.setReverse(BaseScreenData.REVERSED);
			sv.pnotecat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.pnotecat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel panel-heading">Input</div>
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<div class="input-group" style="width:100px; ">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel, (sv.chdrsel.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Categories")%></label>
					</label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "pnotecat" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("pnotecat");
						optionValue = makeDropDownList(mappedItems, sv.pnotecat.getFormData(), 2, resourceBundleHandler);
					%>
					<select name='pnotecat' type='list' style="width: 180px;"
						<%if ((new Byte((sv.pnotecat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.pnotecat).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pnotecat).getColor() == null ? "input_cell"
						: (sv.pnotecat).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						<%=optionValue%>
					</select>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>

<div class="panel panel-default" id="secondPanel">
	<div class="panel panel-heading">Actions</div>
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-6">
				<label for="A" class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Create Policy Notes")%>
				</b> </label>
			</div>
			<div class="col-md-6">
				<label for="B" class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Enquiry Policy Notes")%>
				</b></label>
			</div>
		</div>
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

