<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5688";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5688ScreenVars sv = (S5688ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5688screenWritten.gt(0)) {
%>
<%
	S5688screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract Validation");
%>
<%
	generatedText7.appendClassString("label_txt");
		generatedText7.appendClassString("information_txt");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Minimum");
%>
<%
	generatedText8.appendClassString("label_txt");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Maximum");
%>
<%
	generatedText9.appendClassString("label_txt");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Default");
%>
<%
	generatedText10.appendClassString("label_txt");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Number of Policies on Plan ");
%>
<%
	sv.polmin.setClassString("");
%>
<%
	sv.polmax.setClassString("");
%>
<%
	sv.poldef.setClassString("");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Lives Per Policy");
%>
<!--<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");%>
	
	-->
<%
	sv.lifemin.setClassString("");
%>
<%
	sv.lifemax.setClassString("");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Joint Lives Per Life");
%>
<!--<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");%>
	-->
<%
	sv.jlifemin.setClassString("");
%>
<%
	sv.jlifemax.setClassString("");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract fee method ");
%>
<%
	sv.feemeth.setClassString("");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Default follow ups ");
%>
<%
	sv.defFupMeth.setClassString("");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Minimum deposit method ");
%>
<%
	sv.minDepMth.setClassString("");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Tax Relief Method ");
%>
<%
	sv.taxrelmth.setClassString("");
%>
<%
	sv.taxrelmth.appendClassString("string_fld");
		sv.taxrelmth.appendClassString("input_txt");
		sv.taxrelmth.appendClassString("highlight");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Cancel/Alter from Inception limit ");
%>
<%
	sv.cfiLim.setClassString("");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"New business allowed flag ");
%>
<%
	sv.nbusallw.setClassString("");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Non-Forfeiture Option ");
%>
<%
	sv.znfopt.setClassString("");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Bill into bankcode ");
%>
<%
	generatedText26.appendClassString("label_txt");
		generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
%>
<%
	sv.bankcode.setClassString("");
%>
<%
	sv.bankcode.appendClassString("string_fld");
		sv.bankcode.appendClassString("input_txt");
		sv.bankcode.setInvisibility(BaseScreenData.INVISIBLE);
%>
<%
	StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Allow input of CFI Refund Price Date ");
%>
<%
	sv.zcfidtall.setClassString("");
%>
<%
	sv.zcfidtall.appendClassString("string_fld");
		sv.zcfidtall.appendClassString("input_txt");
		sv.zcfidtall.appendClassString("highlight");
%>
<%
	StringData generatedText46 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Certificate Allowed ");
%>
<%
	sv.zcertall.setClassString("");
%>
<%
	sv.zcertall.appendClassString("string_fld");
		sv.zcertall.appendClassString("input_txt");
		sv.zcertall.appendClassString("highlight");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Life assured validation");
%>
<%
	generatedText27.appendClassString("label_txt");
		generatedText27.appendClassString("information_txt");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age ");
%>
<%
	sv.anbrqd.setClassString("");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Component");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Level");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Accounting");
%>
<%
	StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Smoking ");
%>
<%
	sv.smkrqd.setClassString("");
%>
<%
	StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Revenue");
%>
<%
	StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Accounting");
%>

<%
	StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Occupation ");
%>
<%
	sv.occrqd.setClassString("");
%>
<%
	StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"(Default is Cash Accounting)");
%><!--
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Allowable coverages and riders are held on the Contract Structure table (T5673)");
%>
<%
	generatedText42.appendClassString("label_txt");
		generatedText42.appendClassString("information_txt");
%><!--
	<%StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
<%generatedText43.appendClassString("label_txt");
				generatedText43.appendClassString("information_txt");%>
	<%StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
<%generatedText44.appendClassString("label_txt");
				generatedText44.appendClassString("information_txt");%>
	-->
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind76.isOn()) {
				sv.polmin.setReverse(BaseScreenData.REVERSED);
				sv.polmin.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind76.isOn()) {
				sv.polmin.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind77.isOn()) {
				sv.polmax.setReverse(BaseScreenData.REVERSED);
				sv.polmax.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind77.isOn()) {
				sv.polmax.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind78.isOn()) {
				sv.poldef.setReverse(BaseScreenData.REVERSED);
				sv.poldef.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind78.isOn()) {
				sv.poldef.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.lifemin.setReverse(BaseScreenData.REVERSED);
				sv.lifemin.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.lifemin.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.lifemax.setReverse(BaseScreenData.REVERSED);
				sv.lifemax.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.lifemax.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.jlifemin.setReverse(BaseScreenData.REVERSED);
				sv.jlifemin.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.jlifemin.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.jlifemax.setReverse(BaseScreenData.REVERSED);
				sv.jlifemax.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.jlifemax.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.feemeth.setReverse(BaseScreenData.REVERSED);
				sv.feemeth.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.feemeth.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.defFupMeth.setReverse(BaseScreenData.REVERSED);
				sv.defFupMeth.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.defFupMeth.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind84.isOn()) {
				sv.minDepMth.setReverse(BaseScreenData.REVERSED);
				sv.minDepMth.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind84.isOn()) {
				sv.minDepMth.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind85.isOn()) {
				sv.cfiLim.setReverse(BaseScreenData.REVERSED);
				sv.cfiLim.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind85.isOn()) {
				sv.cfiLim.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind80.isOn()) {
				sv.nbusallw.setReverse(BaseScreenData.REVERSED);
				sv.nbusallw.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind80.isOn()) {
				sv.nbusallw.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind01.isOn()) {
				sv.znfopt.setReverse(BaseScreenData.REVERSED);
				sv.znfopt.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.znfopt.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind75.isOn()) {
				sv.bankcode.setReverse(BaseScreenData.REVERSED);
				sv.bankcode.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind75.isOn()) {
				sv.bankcode.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.anbrqd.setReverse(BaseScreenData.REVERSED);
				sv.anbrqd.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.anbrqd.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.comlvlacc.setReverse(BaseScreenData.REVERSED);
				sv.comlvlacc.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.comlvlacc.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.smkrqd.setReverse(BaseScreenData.REVERSED);
				sv.smkrqd.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.smkrqd.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.revacc.setReverse(BaseScreenData.REVERSED);
				sv.revacc.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.revacc.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.occrqd.setReverse(BaseScreenData.REVERSED);
				sv.occrqd.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.occrqd.setHighLight(BaseScreenData.BOLD);
			}

			//ILIFE-1137 starts
			if (appVars.ind17.isOn()) {
				sv.occrqd.setReverse(BaseScreenData.REVERSED);
				sv.occrqd.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.occrqd.setHighLight(BaseScreenData.BOLD);
			}
			//ILIFE-1137 ends
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

						</td>
						<td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Validation")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 7px;"><%=resourceBundleHandler.gettingValueFromBundle("Number")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("of")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Policies")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("on")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Plan")%>&nbsp;</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.polmin, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.polmax, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.poldef, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 7px;">Lives&nbsp;Per&nbsp;Policy</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.lifemin, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.lifemax, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 7px;">Joint&nbsp;Lives&nbsp;Per&nbsp;Life</label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.jlifemin, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.jlifemax, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract fee method")%></label>
					<div class="input-group" style="width: 140px;">
						<%
							longValue = sv.feemeth.getFormData();
						%>

						<%
							if ((new Byte((sv.feemeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='feemeth' type='text' id="feemeth"
							value='<%=sv.feemeth.getFormData()%>'
							maxLength='<%=sv.feemeth.getLength()%>'
							size='<%=sv.feemeth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(feemeth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.feemeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.feemeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								sstyle="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('feemeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.feemeth).getColor() == null ? "input_cell"
								: (sv.feemeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('feemeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default follow ups")%></label>
					<div class="input-group" style="width: 140px;">
						<%
							longValue = sv.defFupMeth.getFormData();
						%>

						<%
							if ((new Byte((sv.defFupMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='defFupMeth' type='text' id="defFupMeth"
							value='<%=sv.defFupMeth.getFormData()%>'
							maxLength='<%=sv.defFupMeth.getLength()%>'
							size='<%=sv.defFupMeth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(defFupMeth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.defFupMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.defFupMeth).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('defFupMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.defFupMeth).getColor() == null ? "input_cell"
								: (sv.defFupMeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('defFupMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Deposit Method")%></label>
					<div class="input-group" style="width: 140px;">
						<%
							longValue = sv.minDepMth.getFormData();
						%>

						<%
							if ((new Byte((sv.minDepMth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='minDepMth' type='text' id="minDepMth"
							value='<%=sv.minDepMth.getFormData()%>'
							maxLength='<%=sv.minDepMth.getLength()%>'
							size='<%=sv.minDepMth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(minDepMth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.minDepMth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.minDepMth).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('minDepMth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.minDepMth).getColor() == null ? "input_cell"
								: (sv.minDepMth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('minDepMth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Relief Method")%></label>
					<div class="input-group" style="width: 140px;">
						<%
							longValue = sv.taxrelmth.getFormData();
						%>

						<%
							if ((new Byte((sv.taxrelmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='taxrelmth' type='text' id="taxrelmth"
							value='<%=sv.taxrelmth.getFormData()%>'
							maxLength='<%=sv.taxrelmth.getLength()%>'
							size='<%=sv.taxrelmth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(taxrelmth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.taxrelmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.taxrelmth).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('taxrelmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.taxrelmth).getColor() == null ? "input_cell"
								: (sv.taxrelmth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('taxrelmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cancel/Alter from Inception Limit")%></label>
					<div style="width: 140px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cfiLim).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cfiLim' type='text'
							<%if ((sv.cfiLim).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.cfiLim)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.cfiLim);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.cfiLim)%>' <%}%>
							size='<%=sv.cfiLim.getLength()%>'
							maxLength='<%=sv.cfiLim.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(cfiLim)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.cfiLim).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cfiLim).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cfiLim).getColor() == null ? "input_cell"
							: (sv.cfiLim).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Business Allowed Flag")%></label>
					<input type='checkbox' name='nbusallw' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(nbusallw)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.nbusallw).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.nbusallw).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.nbusallw).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('nbusallw')" /> <input type='checkbox'
						name='nbusallw' value=' '
						<%if (!(sv.nbusallw).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('nbusallw')" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Non-Forfeiture Option")%></label>
					<div class="input-group" style="width: 140px;">
						<%
							longValue = sv.znfopt.getFormData();
						%>

						<%
							if ((new Byte((sv.znfopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='znfopt' type='text' id="znfopt"
							value='<%=sv.znfopt.getFormData()%>'
							maxLength='<%=sv.znfopt.getLength()%>'
							size='<%=sv.znfopt.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(znfopt)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.znfopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.znfopt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('znfopt')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.znfopt).getColor() == null ? "input_cell"
								: (sv.znfopt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('znfopt')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Allow input of CFI Refund Price Date")%></label>
					<input type='checkbox' name='zcfidtall' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(zcfidtall)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.zcfidtall).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.zcfidtall).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.zcfidtall).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('zcfidtall')" /> <input type='checkbox'
						name='zcfidtall' value=' '
						<%if (!(sv.zcfidtall).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('zcfidtall')" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Certificate Allowed")%></label>
					<div style="width: 140px;">
						<input name='zcertall' type='text'
							<%if ((sv.zcertall).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zcertall.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zcertall.getLength()%>'
							maxLength='<%=sv.zcertall.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zcertall)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zcertall).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zcertall).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zcertall).getColor() == null ? "input_cell"
							: (sv.zcertall).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured Validation")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age")%></label>
					<div style="width: 70px;">
						<input name='anbrqd' type='text'
							<%if ((sv.anbrqd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.anbrqd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.anbrqd.getLength()%>'
							maxLength='<%=sv.anbrqd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(anbrqd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.anbrqd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.anbrqd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.anbrqd).getColor() == null ? "input_cell"
							: (sv.anbrqd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
					<div style="width: 70px;">
						<input name='smkrqd' type='text'
							<%if ((sv.smkrqd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.smkrqd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.smkrqd.getLength()%>'
							maxLength='<%=sv.smkrqd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(smkrqd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.smkrqd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.smkrqd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.smkrqd).getColor() == null ? "input_cell"
							: (sv.smkrqd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
					<div style="width: 70px;">
						<input name='occrqd' type='text'
							<%if ((sv.occrqd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.occrqd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.occrqd.getLength()%>'
							maxLength='<%=sv.occrqd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(occrqd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.occrqd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.occrqd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.occrqd).getColor() == null ? "input_cell"
							: (sv.occrqd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Revenue Accounting")%></label>
					<input type='checkbox' name='revacc' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(revacc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.revacc).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.revacc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.revacc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('revacc')" />

					<input type='checkbox' name='revacc' value=' '
						<%if (!(sv.revacc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('revacc')" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Level Accounting")%></label>
					<input type='checkbox' name='comlvlacc' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comlvlacc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.comlvlacc).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.comlvlacc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.comlvlacc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('comlvlacc')" /> <input type='checkbox'
						name='comlvlacc' value=' '
						<%if (!(sv.comlvlacc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('comlvlacc')" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Death Claim Flexibility")%></label>
					<div style="width: 140px;">
						<%
							longValue = sv.dflexmeth.getFormData();
						%>

						<%
							if ((new Byte((sv.dflexmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='dflexmeth' type='text'
							value='<%=sv.dflexmeth.getFormData()%>'
							maxLength='<%=sv.dflexmeth.getLength()%>'
							size='<%=sv.dflexmeth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dflexmeth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dflexmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.dflexmeth).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.dflexmeth).getColor() == null ? "input_cell"
								: (sv.dflexmeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>
						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;" class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bill into bankcode")%></label>
					<div class="input-group" style="">
						<%
							longValue = sv.bankcode.getFormData();
						%>

						<%
							if ((new Byte((sv.bankcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='bankcode' type='text' id="bankcode"
							value='<%=sv.bankcode.getFormData()%>'
							maxLength='<%=sv.bankcode.getLength()%>'
							size='<%=sv.bankcode.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(bankcode)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.bankcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.bankcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('bankcode')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>

						class = '
						<%=(sv.bankcode).getColor() == null ? "input_cell"
								: (sv.bankcode).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button"
								onclick="doFocus(document.getElementById('bankcode')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
									longValue = null;
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured Validation")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((browerVersion.equals(IE11)) || (browerVersion.equals(IE8))) {
					%>
					<label>&nbsp;&nbsp;(Default&nbsp;is&nbsp;Cash&nbsp;Accounting)</label>
					<%
						}
					%>
					<%
						if (browerVersion.equals(Chrome)) {
					%>
					<label>&nbsp;&nbsp;(Default&nbsp;is&nbsp;Cash&nbsp;Accounting)</label>
					<%
						}
					%>

					<%
						if ((browerVersion.equals(IE11)) || (browerVersion.equals(IE8))) {
					%>
					<label>Allowable&nbsp;coverages&nbsp;and&nbsp;riders&nbsp;are&nbsp;held&nbsp;on&nbsp;the&nbsp;Contract&nbsp;Structure&nbsp;table&nbsp;(T5673)</label>
					<%
						}
					%>
					<%
						if (browerVersion.equals(Chrome)) {
					%>
					<label>Allowable&nbsp;coverages&nbsp;and&nbsp;riders&nbsp;are&nbsp;held&nbsp;on&nbsp;the&nbsp;Contract&nbsp;Structure&nbsp;table&nbsp;(T5673)</label>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4"></div>
			
			 <% if (sv.csbil003flag.compareTo("N") != 0) { %>
			<div class="col-md-4">
				<div class="form-group">
				
				<% if (!appVars.ind21.isOn()) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency Change Not Allowed")%></label>
					<div style="width: 70px;">
						<input type='checkbox' name='bfreqallw' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(bfreqallw)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.bfreqallw).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.bfreqallw).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.bfreqallw).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('bfreqallw')" /> <input type='checkbox'
						name='bfreqallw' value=' '
						<%if (!(sv.bfreqallw).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('bfreqallw')" />
					</div><%}%>
				</div>
			</div>
			<%}%>
			
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5688protectWritten.gt(0)) {
%>
<%
	S5688protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>