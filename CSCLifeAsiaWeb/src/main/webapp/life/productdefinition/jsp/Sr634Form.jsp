

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR634";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr634ScreenVars sv = (Sr634ScreenVars) fw.getVariables();
%>
<!-- ILIFE-1016 start kpalani6  make disable field upon enquiry-->
<%
	{
		if (appVars.ind50.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
			sv.zdoctor.setColor(BaseScreenData.RED);
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind50.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind52.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.dtetrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.dtetrmDisp.setColor(BaseScreenData.RED);
			sv.dtetrmDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind53.isOn()) {
			sv.dtetrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.zmmccde.setReverse(BaseScreenData.REVERSED);
			sv.zmmccde.setColor(BaseScreenData.RED);
			sv.zmmccde.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind51.isOn()) {
			sv.zmmccde.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<!-- ILIFE-1016 end kpalani6  make disable field upon enquiry-->
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Client No     ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Code ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Dr No ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Dr Name");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "License No ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Eff Date");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cease Date");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------------------------------------------------------");
%>
<%
	appVars.rollup(new int[]{93});
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
					<div class="input-group">
						<%
							if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.clntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
				<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></label>
					<div>
						<%
							if (!((sv.zmedprv.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[5];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if ((resourceBundleHandler.gettingValueFromBundle("Header1").length()
					* 12) >= (sv.zdoctor.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = ((sv.zdoctor.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.name.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
			} else {
				calculatedValue = (sv.name.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.zmmccde.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.zmmccde.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Header4").length()
					* 12) >= (sv.effdateDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = ((sv.effdateDisp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Header5").length()
					* 12) >= (sv.dtetrmDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = ((sv.dtetrmDisp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr634screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row" style="margin-left: 2px;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover"
					id='dataTables-sr633' width='100%'>
					<thead>
						<tr class='info'>
							<th><%=resourceBundleHandler.gettingValueFromBundle("Doctor Number")%></th>

							<th><%=resourceBundleHandler.gettingValueFromBundle("Doctor Name")%></th>
							<th><%=resourceBundleHandler.gettingValueFromBundle("License Number")%></th>
							<th><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></th>
							<th><%=resourceBundleHandler.gettingValueFromBundle("Cease Date")%></th>
						</tr>
					</thead>

					<tbody>
						<%
							Sr634screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (Sr634screensfl.hasMoreScreenRows(sfl)) {
						%>
						<tr>
							<td class="tableDataTag tableDataTagFixed"
								style="width:<%=tblColumnWidth[0]%>px;" align="left">
								<div class="input-group" style="width: 150px;">
									<%
										if ((new Byte((sv.zdoctor).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| fw.getVariables().isScreenProtected()) {
									%>
									<%=sv.zdoctor.getFormData()%>
									<%
										} else {
									%>
									<input
										name='<%="sr634screensfl" + "." + "zdoctor" + "_R" + count%>'
										id='<%="sr634screensfl" + "." + "zdoctor" + "_R" + count%>'
										type='text' value='<%=sv.zdoctor.getFormData()%>'
										class=" <%=(sv.zdoctor).getColor() == null
							? "input_cell"
							: (sv.zdoctor).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
										maxLength='<%=sv.zdoctor.getLength()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr634screensfl.zdoctor)'
										onKeyUp='return checkMaxLength(this)'
										style="width: <%=sv.zdoctor.getLength() * 12%>px;" /> <span
										class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px;" type="button"
											onclick="doFocus(document.getElementById('<%="sr634screensfl" + "." + "zdoctor" + "_R" + count%>')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
									<%
										}
									%>
								</div>
							</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"
								align="left">
								<%
									if ((new Byte((sv.name).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| fw.getVariables().isScreenProtected()) {
								%> <%=sv.name.getFormData()%> <%
 	} else {
 %> <%=sv.name.getFormData()%> <%
 	}
 %>
							</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"
								align="left">
								<%
									if ((new Byte((sv.zmmccde).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| fw.getVariables().isScreenProtected()) {
								%> <%=sv.zmmccde.getFormData()%> <%
 	} else {
 %> <input type='text' maxLength='<%=sv.zmmccde.getLength()%>'
								value='<%=sv.zmmccde.getFormData()%>'
								size='<%=sv.zmmccde.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(sr634screensfl.zmmccde)'
								onKeyUp='return checkMaxLength(this)'
								name='<%="sr634screensfl" + "." + "zmmccde" + "_R" + count%>'
								class="input_cell"
								style="width: <%=sv.zmmccde.getLength() * 12%>px;" /> <%
 	}
 %>
							</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="left">
							<% if((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||fw.getVariables().isScreenProtected()){%>							
						<%= sv.effdateDisp.getFormData() %>
					 	<%} else { %>			
					 	<div class="input-group date form_date" data-date="" data-date-format="dd/mm/yyyy" 
					 	data-link-field="sr634screensfl.effdateDisp_R<%=count%>"
					 	data-link-format="dd/mm/yyyy"  onclick="document.getElementById('sr634screensfl.effdateDisp_R<%=count%>').value='';">								
													<input name='<%="sr634screensfl" + "." +
					 "effdateDisp" + "_R" + count %>'
					 id='sr634screensfl.effdateDisp_R<%=count%>'
					type='text' 
					value='<%=sv.effdateDisp.getFormData() %>' 
					class = " <%=(sv.effdateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.effdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.effdateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr634screensfl.effdateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.effdateDisp.getLength()*12%>px;"
					>
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					  			</div>
				<% } %>
											
									</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
											<% if((new Byte((sv.dtetrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||fw.getVariables().isScreenProtected()){%>							
						<%= sv.dtetrmDisp.getFormData() %>
					 	<%} else { %>	
					 	<div class="input-group date form_date" data-date="" data-date-format="dd/mm/yyyy" 
					 	data-link-field="sr634screensfl.dtetrmDisp_R<%=count%>"
					 	data-link-format="dd/mm/yyyy"  onclick="document.getElementById('sr634screensfl.dtetrmDisp_R<%=count%>').value='';">					
													<input name='<%="sr634screensfl" + "." +
					 "dtetrmDisp" + "_R" + count %>'
					 id='sr634screensfl.dtetrmDisp_R<%=count%>'
					type='text' 
					value='<%=sv.dtetrmDisp.getFormData() %>' 
					class = " <%=(sv.dtetrmDisp).getColor()== null  ? 
					"input_cell" :
					(sv.dtetrmDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.dtetrmDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr634screensfl.dtetrmDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.dtetrmDisp.getLength()*12%>px;"
					>
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					  			</div>
					  			
				<%}%>
											
									</td>
						</tr>
						<!-- ILIFE-1016 end kpalani6  make disable field upon enquiry-->
						<%
							count = count + 1;
								Sr634screensfl.setNextScreenRow(sfl, appVars, sv);
							}
						%>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-sr633').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

