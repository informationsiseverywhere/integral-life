<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr59x";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*" %>

<%Sr59xScreenVars sv = (Sr59xScreenVars) fw.getVariables();%>
 
<%if (sv.Sr59xscreenWritten.gt(0)) {%>
	<%Sr59xscreen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind91.isOn()) {
			sv.savsrisk.setReverse(BaseScreenData.REVERSED);
			sv.savsrisk.setColor(BaseScreenData.RED);
		}
		if (appVars.ind92.isOn()) {
			sv.otherscheallowed.setReverse(BaseScreenData.REVERSED);
			sv.otherscheallowed.setColor(BaseScreenData.RED);
		}
		if (appVars.ind93.isOn()) {
			sv.defaultScheme.setReverse(BaseScreenData.REVERSED);
			sv.defaultScheme.setColor(BaseScreenData.RED);
		}
	}

	%>
	
	
<div class="panel panel-default">
	<div class="panel-body">
	
	<div class="row">


	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>

		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>

		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		<table>
		<tr>
		<td>
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		<td>	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 180px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
  
		</div>
	</div>

	</div>
	
	<div class="row">


	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
		<table>
		<tr>
		<td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>

		<label>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%></label>
		</td>
		
		<td>

		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 3px;width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
  
		</div>
	</div>


	</div>
	
	
	<div class="row">

	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Savings / Risk")%></label>


			<input name='savsrisk'  style="width: 100px;"
			type='text'
			
			<%if((sv.savsrisk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
			
			<%
			
					formatValue = (sv.savsrisk.getFormData()).toString();
			
			%>
				value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
			
			size='<%= sv.savsrisk.getLength()%>'
			maxLength='<%= sv.savsrisk.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(savsrisk)' onKeyUp='return checkMaxLength(this)'  
			
			
			<% 
				if((new Byte((sv.savsrisk).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
				readonly="true"
				class="output_cell"
			<%
				}else if((new Byte((sv.savsrisk).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			
			<%
				}else { 
			%>
			
				class = ' <%=(sv.savsrisk).getColor()== null  ? 
						"input_cell" :  (sv.savsrisk).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
			 
			<%
				} 
			%>
			>
			</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Defaulted Scheme ")%></label>


			<%	
				longValue = sv.defaultScheme.getFormData();  
			%>
			
			<% 
				if((new Byte((sv.defaultScheme).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 150px;">  
				   		<%if(longValue != null){%>
				   		
				   		<%=XSSFilter.escapeHtml(longValue)%>
				   		
				   		<%}%>
				   </div>
			
			<%
			longValue = null;
			%>
			<% }else {  %> 
			<div class="input-group" style="width: 125px;">
			<input name='defaultScheme' id="defaultScheme" 
			type='text' 
			value='<%=sv.defaultScheme.getFormData()%>' 
			maxLength='<%=sv.defaultScheme.getLength()%>' 
			size='<%=sv.defaultScheme.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(defaultScheme)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.defaultScheme).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.defaultScheme).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			
			<span class="input-group-btn">
                <button class="btn btn-info"  type="button"
                     onClick="doFocus(document.getElementById('defaultScheme')); doAction('PFKEY04')">
                   <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                </button>
             </span>
			</div>
			<%
				}else { 
			%>
			<div class="input-group" style="width: 125px;"
			class = ' <%=(sv.defaultScheme).getColor()== null  ? 
			"input_cell" :  (sv.defaultScheme).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			
			
			<span class="input-group-btn">
                <button class="btn btn-info"  type="button"
                     onClick="doFocus(document.getElementById('defaultScheme')); doAction('PFKEY04')">
                   <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                </button>
             </span>
             </div>
			<%}longValue = null; }%>
			
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Other Schemes allowed?")%></label>

			<input type='checkbox' name='otherscheallowed' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(otherscheallowed)' onKeyUp='return checkMaxLength(this)'    
			<%
			
			if((sv.otherscheallowed).getColor()!=null){
						 %>style='background-color:#FF0000;'
					<%}
					if((sv.otherscheallowed).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.otherscheallowed).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('otherscheallowed')"/>
			
			<input type='checkbox' name='otherscheallowed' value=' ' 
			
			<% if(!(sv.otherscheallowed).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('otherscheallowed')"/>

		</div>
	</div>



	</div>
	
	<div class="row">
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Default Contribution")%></label>

			<%
				fieldItem=appVars.loadF4Fields(new String[] {"dfcontrn"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("dfcontrn");
				//ILIFE-808
				optionValue = makeDropDownList( mappedItems , sv.dfcontrn.getFormData(),2,resourceBundleHandler);
			longValue = (String) mappedItems.get((sv.dfcontrn.getFormData()).toString().trim());
			%>
			
			<%
				if((new Byte((sv.dfcontrn).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			<div class='output_cell' style="width: 200px;">
			   <%=XSSFilter.escapeHtml(longValue)%>
			</div>
			
			<%
			longValue = null;
			%>
			
				<% }else {%>
			<select name='dfcontrn' type='list' style="width:188px;"
			<%
				if((new Byte((sv.dfcontrn).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.dfcontrn).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
					class="bold_cell"
			<%
				}else {
			%>
				class = ' <%=(sv.dfcontrn).getColor()== null  ?
						"input_cell" :  (sv.dfcontrn).getColor().equals("red") ?
						"input_cell red reverse" : "input_cell" %>'
			<%
				}
			%>
			>
			<%=optionValue%>
			</select>
			<%
				longValue=null;}
			%>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Recovery Type")%></label>
			<%
				fieldItem=appVars.loadF4Fields(new String[] {"recoveryType"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("recoveryType");
				optionValue = makeDropDownList( mappedItems , sv.recoveryType.getFormData(),2,resourceBundleHandler);
				longValue = (String) mappedItems.get((sv.recoveryType.getFormData()).toString().trim());
			%>
			<%
				if((new Byte((sv.recoveryType).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){
			%>		<div class='output_cell' style="width: 150px;" >
						<%=longValue==null?"":XSSFilter.escapeHtml(longValue) %>
					</div>
			<%	} else {%>
			<select name="recoveryType" type="list" style="width:150px;">
					<%=optionValue.toString().trim() %>	
			</select>
			<%	} %>
			<%	
				longValue=null;
				mappedItems=null;
				optionValue=null;
			%>
		</div>
	</div>
	</div>
		<div class="row">
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Co-Contribution Payments Allowed")%></label>

			<input type='checkbox' name='cocontpay' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(cocontpay)' onKeyUp='return checkMaxLength(this)'    
			<%
			
			if((sv.cocontpay).getColor()!=null){
						 %>style='background-color:#FF0000;'
					<%}
					if((sv.cocontpay).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.cocontpay).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('cocontpay')"/>
			
			<input type='checkbox' name='cocontpay' value=' ' 
			
			<% if(!(sv.cocontpay).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('cocontpay')"/>

		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("LISC Payments Allowed")%></label>

			<input type='checkbox' name='liscpay' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(liscpay)' onKeyUp='return checkMaxLength(this)'    
			<%
			
			if((sv.liscpay).getColor()!=null){
						 %>style='background-color:#FF0000;'
					<%}
					if((sv.liscpay).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.liscpay).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('liscpay')"/>
			
			<input type='checkbox' name='liscpay' value=' ' 
			
			<% if(!(sv.liscpay).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('liscpay')"/>

		</div>
	</div>
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Fee in Risk Premium")%></label>

			<input type='checkbox' name='polfee' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(polfee)' onKeyUp='return checkMaxLength(this)'    
			<%
			
			if((sv.polfee).getColor()!=null){
						 %>style='background-color:#FF0000;'
					<%}
					if((sv.polfee).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.polfee).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('polfee')"/>
			
			<input type='checkbox' name='polfee' value=' ' 
			
			<% if(!(sv.polfee).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('polfee')"/>

		</div>
	</div>
	</div>
	
<%} %>


	</div>
</div>
<!-- BRD-NBP-014 ends -->

<%if (sv.Sr59xprotectWritten.gt(0)) {%>
	<%Sr59xprotect.clearClassString(sv);%>

	
<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>