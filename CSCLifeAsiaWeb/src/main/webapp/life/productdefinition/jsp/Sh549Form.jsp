<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SH549";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sh549ScreenVars sv = (Sh549ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sh549screenWritten.gt(0)) {
%>
<%
	Sh549screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Dates effective     ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Smoker             ");
%>
<%
	sv.indc01.setClassString("");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Non Smoker         ");
%>
<%
	sv.indc02.setClassString("");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Experience Factor  ");
%>
<%
	sv.expfactor.setClassString("");
%>
<%
	sv.expfactor.appendClassString("num_fld");
		sv.expfactor.appendClassString("input_txt");
		sv.expfactor.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reason");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subroutine");
%>
<%
	sv.opcda01.setClassString("");
%>
<%
	sv.subrtn01.setClassString("");
%>
<%
	sv.opcda02.setClassString("");
%>
<%
	sv.subrtn02.setClassString("");
%>
<%
	sv.opcda03.setClassString("");
%>
<%
	sv.subrtn03.setClassString("");
%>
<%
	sv.opcda04.setClassString("");
%>
<%
	sv.subrtn04.setClassString("");
%>
<%
	sv.opcda05.setClassString("");
%>
<%
	sv.subrtn05.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind02.isOn()) {
				sv.indc01.setReverse(BaseScreenData.REVERSED);
				sv.indc01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.indc01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.indc02.setReverse(BaseScreenData.REVERSED);
				sv.indc02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.indc02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.opcda01.setReverse(BaseScreenData.REVERSED);
				sv.opcda01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.opcda01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.opcda02.setReverse(BaseScreenData.REVERSED);
				sv.opcda02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.opcda02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.opcda03.setReverse(BaseScreenData.REVERSED);
				sv.opcda03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.opcda03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.opcda04.setReverse(BaseScreenData.REVERSED);
				sv.opcda04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.opcda04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.opcda05.setReverse(BaseScreenData.REVERSED);
				sv.opcda05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.opcda05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.subrtn01.setReverse(BaseScreenData.REVERSED);
				sv.subrtn01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.subrtn01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.subrtn02.setReverse(BaseScreenData.REVERSED);
				sv.subrtn02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.subrtn02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.subrtn03.setReverse(BaseScreenData.REVERSED);
				sv.subrtn03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.subrtn03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.subrtn04.setReverse(BaseScreenData.REVERSED);
				sv.subrtn04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.subrtn04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.subrtn05.setReverse(BaseScreenData.REVERSED);
				sv.subrtn05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.subrtn05.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>


</td><td>






						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoker")%></label>
					<div style="width: 70px;">
						<input name='indc01' type='text'
							<%if ((sv.indc01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.indc01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.indc01.getLength()%>'
							maxLength='<%=sv.indc01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(indc01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.indc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.indc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.indc01).getColor() == null ? "input_cell"
							: (sv.indc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Non Smoker")%></label>
					<div style="width: 70px;">
						<input name='indc02' type='text'
							<%if ((sv.indc02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.indc02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.indc02.getLength()%>'
							maxLength='<%=sv.indc02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(indc02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.indc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.indc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.indc02).getColor() == null ? "input_cell"
							: (sv.indc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Experience Factor")%></label>
					<div style="width: 110px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.expfactor).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.S1VS5);
						%>

						<input name='expfactor' type='text'
							<%if ((sv.expfactor).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.expfactor)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.expfactor);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.expfactor)%>' <%}%>
							size='<%=sv.expfactor.getLength()%>'
							maxLength='<%=sv.expfactor.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(expfactor)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.expfactor).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.expfactor).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.expfactor).getColor() == null ? "input_cell"
							: (sv.expfactor).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText13)%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda01, (sv.opcda01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('opcda01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.opcda01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.opcda01)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 46px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('opcda01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:140px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn01, (sv.subrtn01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('subrtn01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.subrtn01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.subrtn01)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 71px;max-width:130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('subrtn01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda02, (sv.opcda02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button"
								onclick="doFocus(document.getElementById('opcda02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
 --%>
 <%
                                         if ((new Byte((sv.opcda02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.opcda02)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 46px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('opcda02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
 
 				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:140px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn02, (sv.subrtn02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('subrtn02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.subrtn02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.subrtn02)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 71px;max-width:130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('subrtn02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda03, (sv.opcda03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('opcda03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.opcda03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.opcda03)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 46px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('opcda03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:140px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn03, (sv.subrtn03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('subrtn03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.subrtn03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.subrtn03)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 71px;max-width:130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('subrtn03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda04, (sv.opcda04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('opcda04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.opcda04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.opcda04)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                   <div class="input-group" style="min-width: 46px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('opcda04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:140px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn04, (sv.subrtn04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('subrtn04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.subrtn04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.subrtn04)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 71px;max-width:130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('subrtn04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda05, (sv.opcda05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('opcda05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.opcda05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.opcda05)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style="min-width: 46px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.opcda05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('opcda05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%-- <div class="input-group" style="width:140px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn05, (sv.subrtn05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('subrtn05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div> --%>
					<%
                                         if ((new Byte((sv.subrtn05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.subrtn05)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style="min-width: 71px;max-width:130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.subrtn05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('subrtn05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
					
					
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sh549protectWritten.gt(0)) {
%>
<%
	Sh549protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>