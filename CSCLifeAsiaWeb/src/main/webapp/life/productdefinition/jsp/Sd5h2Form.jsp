<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sd5h2";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%Sd5h2ScreenVars sv = (Sd5h2ScreenVars) fw.getVariables();%>
<%{
		if (appVars.ind31.isOn()) {
			sv.sacscode.setReverse(BaseScreenData.REVERSED);
			sv.sacscode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.sacscode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.sacstyp.setReverse(BaseScreenData.REVERSED);
			sv.sacstyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.sacstyp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.printLocation.setReverse(BaseScreenData.REVERSED);
			sv.printLocation.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.printLocation.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	
<div class="panel1 panel-default1">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table><tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%></td><td style="padding-left:1px;">
					<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</td></tr></table>
	        	</div>
	        </div>
	    </div>
	</div>
</div>

<div class="panel1 panel-default1">
	<div class="panel-heading1"><%=resourceBundleHandler.gettingValueFromBundle("Entity Rules and Data Capture")%>
    </div>
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Details Required")%></label>

					<% 
						if ((new Byte((sv.agntsdets).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
											
						if(((sv.agntsdets.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
							longValue=resourceBundleHandler.gettingValueFromBundle("Y");
						}
						if(((sv.agntsdets.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("N");
					    }
						 
					%>
					
					<% 
						if((new Byte((sv.agntsdets).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.agntsdets).getColor())){
										%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
										<%
										} 
										%>
					<select name='agntsdets' style="width:90px;" 	
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(agntsdets)'
						onKeyUp='return checkMaxLength(this)'
										<% 
									if((new Byte((sv.agntsdets).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.agntsdets).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
						class = 'input_cell' 
								<%
									} 
								%>
								>
					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
					<option value="Y"<% if(((sv.agntsdets.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
					<option value="N"<% if(((sv.agntsdets.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
					
					
										</select>
										<% if("red".equals((sv.agntsdets).getColor())){
										%>
										</div>
										<%
										} 
										%>
					
										<%
					}longValue = null;} 
										%>
					
					
					
					
					<div style='visibility:hidden;' class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
					</div>
	        		<div class="input-group three-controller">
					</div>
	        	</div>
	        </div>
	        <div class="col-md-3"></div>
	       	<div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Payer Details Required")%></label>
	        		<% 
						if ((new Byte((sv.payind).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
											
						if(((sv.payind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
							longValue=resourceBundleHandler.gettingValueFromBundle("Y");
						}
						if(((sv.payind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("N");
					    }
						 
					%>
					
					<% 
						if((new Byte((sv.payind).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.payind).getColor())){
										%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
										<%
										} 
										%>
					<select name='payind' style="width:90px;" 	
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(payind)'
						onKeyUp='return checkMaxLength(this)'
										<% 
									if((new Byte((sv.payind).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.payind).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
						class = 'input_cell' 
								<%
									} 
								%>
								>
					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
					<option value="Y"<% if(((sv.payind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
					<option value="N"<% if(((sv.payind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
					
					
										</select>
										<% if("red".equals((sv.payind).getColor())){
										%>
										</div>
										<%
										} 
										%>
					
										<%
					}longValue = null;} 
										%>
					
					
					
					
					<div style='visibility:hidden;' class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
					</div>
	        	</div>
	        </div> 
	    </div>
		
		<div class="row">
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Bank  Details Required")%></label>
	        		<% 
						if ((new Byte((sv.ddind).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
											
						if(((sv.ddind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
							longValue=resourceBundleHandler.gettingValueFromBundle("Y");
						}
						if(((sv.ddind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("N");
					    }
						 
					%>
					
					<% 
						if((new Byte((sv.ddind).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.ddind).getColor())){
										%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
										<%
										} 
										%>
					<select name='ddind' style="width:90px;" 	
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(ddind)'
						onKeyUp='return checkMaxLength(this)'
										<% 
									if((new Byte((sv.ddind).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.ddind).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
						class = 'input_cell' 
								<%
									} 
								%>
								>
					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
					<option value="Y"<% if(((sv.ddind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
					<option value="N"<% if(((sv.ddind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
					
					
										</select>
										<% if("red".equals((sv.ddind).getColor())){
										%>
										</div>
										<%
										} 
										%>
					
										<%
					}longValue = null;} 
										%>
					<div style='visibility:hidden;' class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
					</div>
	        	</div>
	        </div>
	        <div class="col-md-3"></div>
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Group Details Required")%></label>
	        		<% 
					if ((new Byte((sv.grpind).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {
										
					if(((sv.grpind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("Y");
					}
					if(((sv.grpind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
					longValue=resourceBundleHandler.gettingValueFromBundle("N");
				    }
					 
				%>
				
				<% 
					if((new Byte((sv.grpind).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=XSSFilter.escapeHtml(longValue)%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
					<% }else {%>
					
				<% if("red".equals((sv.grpind).getColor())){
									%>
				<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
									<%
									} 
									%>
				<select name='grpind' style="width:90px;" 	
					onFocus='doFocus(this)'
					onHelp='return fieldHelp(grpind)'
					onKeyUp='return checkMaxLength(this)'
									<% 
								if((new Byte((sv.grpind).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								disabled
								class="output_cell"
							<%
								}else if((new Byte((sv.grpind).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							<%
								}else { 
							%>
					class = 'input_cell' 
							<%
								} 
							%>
							>
				<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
				<option value="Y"<% if(((sv.grpind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
				<option value="N"<% if(((sv.grpind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
				
				
									</select>
									<% if("red".equals((sv.grpind).getColor())){
									%>
									</div>
									<%
									} 
									%>
				
									<%
				}longValue = null;} 
									%>
				<div style='visibility:hidden;' class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
				</div>
	        		<div class="input-group three-controller">
					</div>
	        	</div>
	        </div>
	    </div>	
	    
	    <div class="row">
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Credit Card Details Required")%></label>
	        		<% 
						if ((new Byte((sv.crcind).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
											
						if(((sv.crcind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
							longValue=resourceBundleHandler.gettingValueFromBundle("Y");
						}
						if(((sv.crcind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("N");
					    }
						 
					%>
					
					<% 
						if((new Byte((sv.crcind).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.crcind).getColor())){
										%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
										<%
										} 
										%>
					<select name='crcind' style="width:90px;" 	
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(crcind)'
						onKeyUp='return checkMaxLength(this)'
										<% 
									if((new Byte((sv.crcind).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.crcind).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
						class = 'input_cell' 
								<%
									} 
								%>
								>
					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
					<option value="Y"<% if(((sv.crcind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
					<option value="N"<% if(((sv.crcind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
					
					
										</select>
										<% if("red".equals((sv.crcind).getColor())){
										%>
										</div>
										<%
										} 
										%>
					
										<%
					}longValue = null;} 
										%>
					<div style='visibility:hidden;' class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
					</div>
	        	</div>
	        </div>
	        <div class="col-md-3"></div>
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Rollover Details Required")%></label>
	        		<% 
					if ((new Byte((sv.rollovrind).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {
										
					if(((sv.rollovrind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("Y");
					}
					if(((sv.rollovrind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
					longValue=resourceBundleHandler.gettingValueFromBundle("N");
				    }
					 
				%>
				
				<% 
					if((new Byte((sv.rollovrind).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=XSSFilter.escapeHtml(longValue)%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
					<% }else {%>
					
				<% if("red".equals((sv.rollovrind).getColor())){
									%>
				<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
									<%
									} 
									%>
				<select name='rollovrind' style="width:90px;" 	
					onFocus='doFocus(this)'
					onHelp='return fieldHelp(rollovrind)'
					onKeyUp='return checkMaxLength(this)'
									<% 
								if((new Byte((sv.rollovrind).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								disabled
								class="output_cell"
							<%
								}else if((new Byte((sv.rollovrind).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							<%
								}else { 
							%>
					class = 'input_cell' 
							<%
								} 
							%>
							>
				<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
				<option value="Y"<% if(((sv.rollovrind.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
				<option value="N"<% if(((sv.rollovrind.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
				
				
									</select>
									<% if("red".equals((sv.rollovrind).getColor())){
									%>
									</div>
									<%
									} 
									%>
				
									<%
				}longValue = null;} 
									%>
				<div style='visibility:hidden;' class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Y/N")%>
				</div>
	        		
	        	</div>
	        </div>
	    </div>	
	</div>
</div>

<div class="panel1 panel-default1">
	<div class="panel-heading1"><%=resourceBundleHandler.gettingValueFromBundle("Billing Processing")%>
    </div>
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("On Account")%></label>
	        		<%=smartHF.getHTMLVarExt(fw, sv.accInd)%>
	        	</div>
	        </div>
	        <div class="col-md-3"></div>
	        <div class="col-md-3">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Trigger Required")%></label>
	        		<%=smartHF.getHTMLVarExt(fw, sv.triggerRequired)%>
	        	</div>
	        </div>
	    </div>
	    
	    <div class="row">
	        <div class="col-md-6">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Ledger Code")%></label>
	        		<table><tr><td>
	        		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("sacscode");
						optionValue = makeDropDownList( mappedItems , sv.sacscode.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.sacscode).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.sacscode).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					
					<select name='sacscode' type='list' style="width:140px;" id='sacscode'
					<% 
						if((new Byte((sv.sacscode).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.sacscode).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.sacscode).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					} 
					%>
					</td><td>
					&nbsp;<label style="font-size: 13px;"><%=resourceBundleHandler.gettingValueFromBundle("Enter only when used for Groups")%></label></td></tr></table>
	        	</div>
	        </div>
	        <div class="col-md-6">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Ledger Type")%></label>
	        		<table><tr><td>
	        		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstyp"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("sacstyp");
						optionValue = makeDropDownList( mappedItems , sv.sacstyp.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.sacstyp.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.sacstyp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.sacstyp).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					
					<select name='sacstyp' type='list' style="width:140px;" id='sacstyp'
					<% 
						if((new Byte((sv.sacstyp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.sacstyp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.sacstyp).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					} 
					%></td><td>
					&nbsp;<label style="font-size: 13px;"><%=resourceBundleHandler.gettingValueFromBundle("Enter only when used for Groups")%></label></td></tr></table>
	        	</div>
	        </div>
	    </div>
	    
	    <div class="row">
	        <div class="col-md-6">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Media Required")%></label>
	        		<input type='checkbox' name='mediaRequired' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(mediaRequired)' onKeyUp='return checkMaxLength(this)'    
					<%
					
					if((sv.mediaRequired).getColor()!=null){
								 %>style='background-color:#FF0000;'
							<%}
							if((sv.mediaRequired).toString().trim().equalsIgnoreCase("Y")){
								%>checked
							
					      <% }if((sv.mediaRequired).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
					    	   disabled
							
							<%}%>
					class ='UICheck' onclick="handleCheckBox('mediaRequired')"/>
					
					<input type='checkbox' name='mediaRequired' value=' ' 
					
					<% if(!(sv.mediaRequired).toString().trim().equalsIgnoreCase("Y")){
								%>checked
							
					      <% }%>
					
					style="visibility: hidden" onclick="handleCheckBox('mediaRequired')"/>
	        	</div>
	        </div>
	        <div class="col-md-6">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Debit Note Print Location")%></label>
	        		<table><tr><td>
	        		<% 
						if ((new Byte((sv.printLocation).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
											
						if(((sv.printLocation.getFormData()).toString()).trim().equalsIgnoreCase("I")) {
							longValue=resourceBundleHandler.gettingValueFromBundle("I");
						}
						if(((sv.printLocation.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
						longValue=resourceBundleHandler.gettingValueFromBundle("B");
					    }
						 
					%>
					
					<% 
						if((new Byte((sv.printLocation).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.printLocation).getColor())){
										%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
										<%
										} 
										%>
					
					<select name='printLocation' style="width:90px;" 	
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(printLocation)'
						onKeyUp='return checkMaxLength(this)'
										<% 
									if((new Byte((sv.printLocation).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.printLocation).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
						class = 'input_cell' 
								<%
									} 
								%>
								>
										
					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
					<option value="I"<% if(((sv.printLocation.getFormData()).toString()).trim().equalsIgnoreCase("I")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("I")%></option>
					<option value="B"<% if(((sv.printLocation.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("B")%></option>
					
					
										</select>
										<% if("red".equals((sv.printLocation).getColor())){
										%>
										</div>
										<%
										} 
										%>
					
										<%
					}longValue = null;} 
										%>
					
					
					
					
					<div style='visibility:hidden;' class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("I/B")%>
					</div></td><td>
					&nbsp;<label style="font-size: 13px;"><%=resourceBundleHandler.gettingValueFromBundle("(Applicable to Polisy product only)")%></label></td></tr></table>
	        	</div>
	        </div>
	    </div>
	</div>
</div>
<style>
.panel1 {
    margin-bottom: 20px;
    /* background-color: #fff; */
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}

.panel-heading1 {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    
}
.panel-default1 {
    font-size: 90% !important;
    border-color: #ddd;
    }
    
</style>
	
 
 <%@ include file="/POLACommon2NEW.jsp"%>