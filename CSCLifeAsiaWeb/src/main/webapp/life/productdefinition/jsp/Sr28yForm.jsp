

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sr28y";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%Sr28yScreenVars sv = (Sr28yScreenVars) fw.getVariables();%>
<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"VP/MS Property ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Map To Field ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Copybook ");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No. of Occurence ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"VP/MS Attributes ");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Attr ID ");%>


<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
.panel{
height: 930px !important;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<!-- end row 1 -->
		<!-- row 2 -->
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("VP/MS Property")%></label>
					<div style="width: 400px;">
					<input name='vpmsprop' 
					type='text'
					
					<%if((sv.vpmsprop).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
					<%
					
							formatValue = (sv.vpmsprop.getFormData()).toString();
					
					%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.vpmsprop.getLength()%>'
					maxLength='<%= sv.vpmsprop.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(vpmsprop)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.vpmsprop).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.vpmsprop).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.vpmsprop).getColor()== null  ? 
								"input_cell" :  (sv.vpmsprop).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					</div>
				</div>
			</div>
		</div>
		<!-- end row 2 -->
		<!-- row 3 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Map To Field")%></label>
					<div style="width: 300px;">
					<input name='maptofield' 
						type='text'
						
						<%if((sv.maptofield).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.maptofield.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.maptofield.getLength()%>'
						maxLength='<%= sv.maptofield.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(maptofield)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.maptofield).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.maptofield).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.maptofield).getColor()== null  ? 
									"input_cell" :  (sv.maptofield).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Copybook")%></label>	
					<div style="width: 150px;">				
						<input name='copybk' 
						type='text'
						
						<%if((sv.copybk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.copybk.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.copybk.getLength()%>'
						maxLength='<%= sv.copybk.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(copybk)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.copybk).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.copybk).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.copybk).getColor()== null  ? 
									"input_cell" :  (sv.copybk).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("No. of Occurence")%></label>
					<div style="width: 70px;">
						<input name='numoccurs' 
						type='text'
						
						<%if((sv.numoccurs).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.numoccurs.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.numoccurs.getLength()%>'
						maxLength='<%= sv.numoccurs.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(numoccurs)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.numoccurs).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.numoccurs).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.numoccurs).getColor()== null  ? 
									"input_cell" :  (sv.numoccurs).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</div>
				</div>
			</div>
		</div>
		<br/>
		<!-- end row 3 -->
		<!-- row 4 -->
		<div class="row">
			<div class="col-md-12">
					<div class="table-responsive">
						<table class="table border-none table-striped table-bordered table-hover" width='100%'>
							<thead>
								<tr class="info">
									<td width="500px;"><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "VP/MS Attributes ")%></td>
									<td><%=resourceBundleHandler.gettingValueFromBundle(StringData.class, "Attr ID ")%></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl01,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue01,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl02,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue02,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl03,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue03,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl04,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue04,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl05,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue05,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl06,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue06,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl07,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue07,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl08,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue08,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl09,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue09,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl10,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue10,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl11,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue11,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl12,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue12,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl13,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue13,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl14,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue14,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl15,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue15,1,100)%></td>
								</tr>
								<tr>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrslbl16,1,400)%></td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.attrsvalue16,1,100)%></td>
								</tr>
							</tbody>
						</table>
					</div>
			</div>
		</div>
		<!-- end row 4 -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<button type="button" class="btn btn-primary btn-xs">
						<a href="#" onClick=" JavaScript:doAction('PFKey91') ;"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
					</button>
					<button type="button" class="btn btn-primary btn-xs">
						<a href= "#"  onClick=" JavaScript:doAction('PFKey90') ;"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
					</button>
				</div>
			</div>
		</div>
		<!--  -->
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6  ends -->
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}

div[id*='attrslbl'] {
	padding-right: 2px !important
}

.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
.table.border-none tr td, .table.border-none tr th{
	/* border:none !important; */
}
.btn.btn-primary a {
	color: white;
}
</style>
<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6  ends -->