<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sr58y";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%Sr58yScreenVars sv = (Sr58yScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage");%>

<%{
		if (appVars.ind21.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.ageIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm01.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.ageIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm02.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.unitPremPercent01.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.unitPremPercent01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.unitPremPercent02.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitPremPercent02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.unitPremPercent03.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.unitPremPercent03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.unitPremPercent04.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitPremPercent04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.unitPremPercent05.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.unitPremPercent05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.unitPremPercent06.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.unitPremPercent06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.unitPremPercent07.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.unitPremPercent07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.unitPremPercent08.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.unitPremPercent08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.unitPremPercent09.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.unitPremPercent09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.unitPremPercent10.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.unitPremPercent10.setHighLight(BaseScreenData.BOLD);
		}
		
		// allocation validation
		if (appVars.ind01.isOn()) {
			sv.ageIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm01.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.ageIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm02.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind03.isOn()) {
			sv.ageIssageFrm03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm03.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.ageIssageFrm04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm04.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.ageIssageFrm05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm05.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind06.isOn()) {
			sv.ageIssageFrm06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm06.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.ageIssageFrm07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm07.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind08.isOn()) {
			sv.ageIssageFrm08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm08.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.ageIssageFrm09.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm09.setColor(BaseScreenData.RED);
			
			sv.ageIssageTo09.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo09.setColor(BaseScreenData.RED);
		}
	}

	%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div class="input-group">
					
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
		<div
			class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
</div>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<div class="input-group">
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
		<div
			class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<table>
					<tr>
					<td>
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
		<div
			class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
					</td><td style="padding-left:1px;max-width:215px;">
					<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
		<div
			class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
									
					</td>
					</tr>
					</table>
					
					
		</div></div></div>
		<br>
		<div class="row">
			<div class="col-md-1">
					<label style="white-space: nowrap;" ><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Age Fr"))%></label>
			</div>
			<div class="col-md-1">
					<label style="white-space: nowrap;" ><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Age To"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Alloc%"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Alloc%"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Alloc%"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Alloc%"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund"))%></label>
			</div>
			<div class="col-md-1">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Alloc%"))%></label>
			</div>
					
		</div>
					
			<div class="row">
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageFrm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				
				<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund01"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("unitVirtualFund01");
				optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund01.getFormData(),2,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());  
			%> <% 
				if((new Byte((sv.unitVirtualFund01).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:70px;max-width:70px;">
					<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
					</div>
			
					<%
			longValue = null;
			%> <% }else {%> <% if("red".equals((sv.unitVirtualFund01).getColor())){
			%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
					<%
			} 
			%> <select name='unitVirtualFund01' type='list' style="width:70px;"
						<% 
				if((new Byte((sv.unitVirtualFund01).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>
						readonly="true" disabled class="output_cell"
						<%
				}else if((new Byte((sv.unitVirtualFund01).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
						class="bold_cell" <%
				}else { 
			%> class='input_cell' <%
				} 
			%>>
						<%=optionValue%>
					</select> <% if("red".equals((sv.unitVirtualFund01).getColor())){
			%>
					</div>
					<%
			} 
			%> <%
			}  
			%>
			
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent01' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent01) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent01);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent01) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent01).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent01).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent01).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund02");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund02.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund02).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund02).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund02' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund02).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund02).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund02).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent02' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent02) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent02);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent02) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent02).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent02).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent02).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund03"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund03");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund03.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund03).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund03).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund03' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund03).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund03).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund03).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent03' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent03) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent03);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent03) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent03).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent03).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent03).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent02).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund04");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund04.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund04).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund04).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund04' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund04).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund04).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund04).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent04' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent04) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent04);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent04) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent04).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent04).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent04).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent04).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund05");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund05.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund05).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund05).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund05' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund03).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund05).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund05).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent05' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent05) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent05);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent05) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent05).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent05).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent05).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent05).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
				
			</div>	
				<div class="row">
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageFrm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund06"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("unitVirtualFund06");
				optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund06.getFormData(),2,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());  
			%> <% 
				if((new Byte((sv.unitVirtualFund06).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
					<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
					</div>
			
					<%
			longValue = null;
			%> <% }else {%> <% if("red".equals((sv.unitVirtualFund06).getColor())){
			%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
					<%
			} 
			%> <select name='unitVirtualFund06' type='list' style="width:70px;"
						<% 
				if((new Byte((sv.unitVirtualFund06).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>
						readonly="true" disabled class="output_cell"
						<%
				}else if((new Byte((sv.unitVirtualFund06).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
						class="bold_cell" <%
				}else { 
			%> class='input_cell' <%
				} 
			%>>
						<%=optionValue%>
					</select> <% if("red".equals((sv.unitVirtualFund06).getColor())){
			%>
					</div>
					<%
			} 
			%> <%
			} 
			%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent06' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent06) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent06);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent06) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent06).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent06).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent06).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent01).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund07");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund07.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund07).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund07).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund07' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund07).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund07).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund07).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent07' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent07) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent07);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent07) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent07).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent07).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent07).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent07).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund08");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund08.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund08).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund08).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund08' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund08).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund08).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund08).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent08' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent08) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent08);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent08) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent08).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent08).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent08).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent08).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund09"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund09");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund09.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund09).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund09).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund09' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund09).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund09).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund09).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent09' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent09) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent09);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent09) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent09).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent09).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent09).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent09).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund10"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund10");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund10.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund10).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund10).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund10' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund10).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund10).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund10).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent10' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent10) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent10);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent10) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent10).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent10).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent10).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent10).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
				
			</div>
				<div class="row">
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageFrm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund11"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("unitVirtualFund11");
				optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund11.getFormData(),2,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.unitVirtualFund11.getFormData()).toString().trim());  
			%> <% 
				if((new Byte((sv.unitVirtualFund11).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
					<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
					</div>
			
					<%
			longValue = null;
			%> <% }else {%> <% if("red".equals((sv.unitVirtualFund11).getColor())){
			%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
					<%
			} 
			%> <select name='unitVirtualFund11' type='list' style="width:70px;"
						<% 
				if((new Byte((sv.unitVirtualFund11).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>
						readonly="true" disabled class="output_cell"
						<%
				}else if((new Byte((sv.unitVirtualFund11).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
						class="bold_cell" <%
				}else { 
			%> class='input_cell' <%
				} 
			%>>
						<%=optionValue%>
					</select> <% if("red".equals((sv.unitVirtualFund11).getColor())){
			%>
					</div>
					<%
			} 
			%> <%
			} 
			%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent11).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent11' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent11) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent11);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent11) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent11).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent11).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent11).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent11).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund12"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund12");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund12.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund12.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund12).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund12).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund12' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund12).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund12).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund12).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent12' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent12) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent12);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent12) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent12).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent12).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent12).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent12).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund13"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund13");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund13.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund13.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund13).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund13).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund13' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund13).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund13).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund13).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent13).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent13' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent13) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent13);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent13) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent13.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent13)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent13).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent13).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent13).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent13).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
						
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund14"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund14");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund14.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund14.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund14).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund14).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund14' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund14).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund14).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund14).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent14).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent14' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent14) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent14);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent14) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent14.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent14)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent14).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent14).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent14).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent14).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund15"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund15");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund15.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund15.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund15).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund15).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund15' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund15).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund15).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund15).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent15).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent15' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent15) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent15);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent15) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent15.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent15)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent15).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent15).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent15).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent15).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
				
			</div>
				<div class="row">
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageFrm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund16"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("unitVirtualFund16");
				optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund16.getFormData(),2,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.unitVirtualFund16.getFormData()).toString().trim());  
			%> <% 
				if((new Byte((sv.unitVirtualFund16).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
					<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
					</div>
			
					<%
			longValue = null;
			%> <% }else {%> <% if("red".equals((sv.unitVirtualFund16).getColor())){
			%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
					<%
			} 
			%> <select name='unitVirtualFund16' type='list' style="width:70px;"
						<% 
				if((new Byte((sv.unitVirtualFund16).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>
						readonly="true" disabled class="output_cell"
						<%
				}else if((new Byte((sv.unitVirtualFund16).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
						class="bold_cell" <%
				}else { 
			%> class='input_cell' <%
				} 
			%>>
						<%=optionValue%>
					</select> <% if("red".equals((sv.unitVirtualFund16).getColor())){
			%>
					</div>
					<%
			} 
			%> <%
			} 
			%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent16).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent16' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent16) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent16);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent16) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent16.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent16)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent16).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent16).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent16).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent16).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund17"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund17");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund17.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund17.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund17).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund17).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund17' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund17).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund17).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund17).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent17).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent17' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent17) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent17);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent17) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent17.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent17)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent17).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent17).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent17).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent17).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund18"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund18");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund18.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund18.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund18).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund18).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund18' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund18).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund18).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund18).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent18).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent18' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent18) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent18);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent18) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent18.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent18)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent18).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent18).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent18).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent18).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund19"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund19");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund19.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund18.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund19).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund18).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund19' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund19).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund19).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund19).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent19).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent19' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent19) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent19);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent19) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent19.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent19)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent19).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent19).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent19).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent19).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund20"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund20");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund20.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund20.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund20).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund20).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund20' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund20).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund20).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund20).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent20).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent20' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent20) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent20);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent20) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent20.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent20)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent20).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent20).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent20).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent20).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
				
			</div>
				<div class="row">
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageFrm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group" style="width: 46px;">
				<%=smartHF.getHTMLVarExt(fw, sv.ageIssageTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund21"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("unitVirtualFund21");
				optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund21.getFormData(),2,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.unitVirtualFund21.getFormData()).toString().trim());  
			%> <% 
				if((new Byte((sv.unitVirtualFund21).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
					<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
					</div>
			
					<%
			longValue = null;
			%> <% }else {%> <% if("red".equals((sv.unitVirtualFund21).getColor())){
			%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
					<%
			} 
			%> <select name='unitVirtualFund21' type='list' style="width:70px;"
						<% 
				if((new Byte((sv.unitVirtualFund21).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>
						readonly="true" disabled class="output_cell"
						<%
				}else if((new Byte((sv.unitVirtualFund21).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
						class="bold_cell" <%
				}else { 
			%> class='input_cell' <%
				} 
			%>>
						<%=optionValue%>
					</select> <% if("red".equals((sv.unitVirtualFund21).getColor())){
			%>
					</div>
					<%
			} 
			%> <%
			} 
			%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent21).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent16' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent21) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent21);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent21) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent21.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent21)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent21).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent21).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent21).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent21).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund22"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund22");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund22.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund22.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund22).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund22).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund22' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund22).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund22).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund22).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent22).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent22' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent22) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent22);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent22) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent22.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent22)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent22).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent22).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent22).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent22).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund23"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund23");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund23.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund23.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund23).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund23).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund23' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund23).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund23).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund23).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent23).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent23' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent23) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent23);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent23) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent23.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent23)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent23).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent23).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent23).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent23).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<div class="input-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund24"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund24");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund24.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund24.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund24).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund24).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund24' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund24).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund24).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund24).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
					</div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent24).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent19' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent24) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent24);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent24) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent24.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent24)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent24).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent24).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent24).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent24).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund25"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund25");
						optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund25.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.unitVirtualFund25.getFormData()).toString().trim());  
					%> <% 
						if((new Byte((sv.unitVirtualFund25).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:70px;max-width:70px;">
							<%if(longValue != null){%> <%=XSSFilter.escapeHtml(longValue)%> <%}%>
							</div>
					
							<%
					longValue = null;
					%> <% }else {%> <% if("red".equals((sv.unitVirtualFund25).getColor())){
					%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 70px;">
							<%
					} 
					%> <select name='unitVirtualFund25' type='list' style="width: 70px;"
								<% 
						if((new Byte((sv.unitVirtualFund25).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
								readonly="true" disabled class="output_cell"
								<%
						}else if((new Byte((sv.unitVirtualFund25).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
								class="bold_cell" <%
						}else { 
					%> class='input_cell' <%
						} 
					%>>
								<%=optionValue%>
							</select> <% if("red".equals((sv.unitVirtualFund25).getColor())){
					%>
							</div>
							<%
					} 
					%> <%
					} 
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<%	
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent25).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							
					%> <input name='unitPremPercent25' type='text'
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent25) %>'
							<%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unitPremPercent25);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.unitPremPercent25) %>'
							<%}%> size='1'
							maxLength='<%= sv.unitPremPercent25.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPremPercent25)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event, false);'
							<% 
					if((new Byte((sv.unitPremPercent25).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
							readonly="true" class="output_cell"
							<%
					}else if((new Byte((sv.unitPremPercent25).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
							class="bold_cell" <%
					}else { 
				%>
							class=' <%=(sv.unitPremPercent25).getColor()== null  ? 
							"input_cell" :  (sv.unitPremPercent25).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
							<%
					} 
				%>>
				</div>
			</div>
				
			</div>	
					
					
		</div></div>
		
		<script>
$(document).ready(function() {
   $('#unitVirtualFund24').width('58');
   
} );

</script> 

<%@ include file="/POLACommon2NEW.jsp"%>

