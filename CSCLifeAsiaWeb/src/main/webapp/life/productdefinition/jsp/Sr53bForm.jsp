<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR53B";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr53bScreenVars sv = (Sr53bScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr53bscreenWritten.gt(0)) {
%>
<%
	Sr53bscreen.clearClassString(sv);
%>
<%
	StringData generatedText2 = new StringData("Schedule Name         ");
%>
<%
	sv.scheduleName.setClassString("");
%>
<%
	sv.scheduleName.appendClassString("string_fld");
		sv.scheduleName.appendClassString("output_txt");
		sv.scheduleName.appendClassString("highlight");
%>
<%
	StringData generatedText3 = new StringData("Accounting Month      ");
%>
<%
	sv.acctmonth.setClassString("");
%>
<%
	sv.acctmonth.appendClassString("num_fld");
		sv.acctmonth.appendClassString("output_txt");
		sv.acctmonth.appendClassString("highlight");
%>
<%
	StringData generatedText4 = new StringData("Number       ");
%>
<%
	sv.scheduleNumber.setClassString("");
%>
<%
	sv.scheduleNumber.appendClassString("num_fld");
		sv.scheduleNumber.appendClassString("output_txt");
		sv.scheduleNumber.appendClassString("highlight");
%>
<%
	StringData generatedText5 = new StringData("Year       ");
%>
<%
	sv.acctyear.setClassString("");
%>
<%
	sv.acctyear.appendClassString("num_fld");
		sv.acctyear.appendClassString("output_txt");
		sv.acctyear.appendClassString("highlight");
%>
<%
	StringData generatedText6 = new StringData("Effective Date        ");
%>
<%
	sv.effdateDisp.setClassString("");
%>
<%
	sv.effdateDisp.appendClassString("string_fld");
		sv.effdateDisp.appendClassString("output_txt");
		sv.effdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = new StringData("Company               ");
%>
<%
	sv.bcompany.setClassString("");
%>
<%
	sv.bcompany.appendClassString("string_fld");
		sv.bcompany.appendClassString("output_txt");
		sv.bcompany.appendClassString("highlight");
%>
<%
	StringData generatedText8 = new StringData("Job Queue             ");
%>
<%
	sv.jobq.setClassString("");
%>
<%
	sv.jobq.appendClassString("string_fld");
		sv.jobq.appendClassString("output_txt");
		sv.jobq.appendClassString("highlight");
%>
<%
	StringData generatedText9 = new StringData("Branch                ");
%>
<%
	sv.bbranch.setClassString("");
%>
<%
	sv.bbranch.appendClassString("string_fld");
		sv.bbranch.appendClassString("output_txt");
		sv.bbranch.appendClassString("highlight");
%>
<%
	StringData generatedText10 = new StringData("Work Unit             ");
%>
<%
	sv.worku.setClassString("");
%>
<%
	StringData generatedText11 = new StringData("Extract to CSV file(Y/N)    ");
%>
<%
	sv.extrval.setClassString("");
%>
<%
	StringData generatedText12 = new StringData("Old Product           ");
%>
<%
	sv.contractType.setClassString("");
%>
<%
	sv.cntdesc.setClassString("");
%>
<%
	sv.cntdesc.appendClassString("output_txt");
		sv.cntdesc.appendClassString("highlight");
%>
<%
	StringData generatedText13 = new StringData("New Product           ");
%>
<%
	sv.cnttyp.setClassString("");
%>
<%
	sv.ctypdesc.setClassString("");
%>
<%
	StringData generatedText14 = new StringData("Old Component");
%>
<%
	generatedText14.appendClassString("label_txt");
		generatedText14.appendClassString("information_txt");
%>
<%
	StringData generatedText15 = new StringData("New Component");
%>
<%
	generatedText15.appendClassString("label_txt");
		generatedText15.appendClassString("information_txt");
%>
<%
	StringData generatedText16 = new StringData("New Component Description");
%>
<%
	generatedText16.appendClassString("label_txt");
		generatedText16.appendClassString("information_txt");
%>
<%
	sv.crtable01.setClassString("");
%>
<%
	sv.crcode01.setClassString("");
%>
<%
	sv.crtabled01.setClassString("");
%>
<%
	sv.crtable02.setClassString("");
%>
<%
	sv.crcode02.setClassString("");
%>
<%
	sv.crtabled02.setClassString("");
%>
<%
	sv.crtable03.setClassString("");
%>
<%
	sv.crcode03.setClassString("");
%>
<%
	sv.crtabled03.setClassString("");
%>
<%
	sv.crtable04.setClassString("");
%>
<%
	sv.crcode04.setClassString("");
%>
<%
	sv.crtabled04.setClassString("");
%>
<%
	sv.crtable05.setClassString("");
%>
<%
	sv.crcode05.setClassString("");
%>
<%
	sv.crtabled05.setClassString("");
%>
<%
	sv.crtable06.setClassString("");
%>
<%
	sv.crcode06.setClassString("");
%>
<%
	sv.crtabled06.setClassString("");
%>
<%
	sv.crtable07.setClassString("");
%>
<%
	sv.crcode07.setClassString("");
%>
<%
	sv.crtabled07.setClassString("");
%>
<%
	sv.crtable08.setClassString("");
%>
<%
	sv.crcode08.setClassString("");
%>
<%
	sv.crtabled08.setClassString("");
%>
<%
	sv.crtable09.setClassString("");
%>
<%
	sv.crcode09.setClassString("");
%>
<%
	sv.crtabled09.setClassString("");
%>
<%
	sv.crtable10.setClassString("");
%>
<%
	sv.crcode10.setClassString("");
%>
<%
	sv.crtabled10.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.worku.setReverse(BaseScreenData.REVERSED);
				sv.worku.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.worku.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.extrval.setReverse(BaseScreenData.REVERSED);
				sv.extrval.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.extrval.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.contractType.setReverse(BaseScreenData.REVERSED);
				sv.contractType.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.contractType.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.cnttyp.setReverse(BaseScreenData.REVERSED);
				sv.cnttyp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.cnttyp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.ctypdesc.setReverse(BaseScreenData.REVERSED);
				sv.ctypdesc.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.ctypdesc.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.crtable01.setReverse(BaseScreenData.REVERSED);
				sv.crtable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.crtable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.crcode01.setReverse(BaseScreenData.REVERSED);
				sv.crcode01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.crcode01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.crtabled01.setReverse(BaseScreenData.REVERSED);
				sv.crtabled01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.crtabled01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.crtable02.setReverse(BaseScreenData.REVERSED);
				sv.crtable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.crtable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.crcode02.setReverse(BaseScreenData.REVERSED);
				sv.crcode02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.crcode02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.crtabled02.setReverse(BaseScreenData.REVERSED);
				sv.crtabled02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.crtabled02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.crtable03.setReverse(BaseScreenData.REVERSED);
				sv.crtable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.crtable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.crcode03.setReverse(BaseScreenData.REVERSED);
				sv.crcode03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.crcode03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.crtabled03.setReverse(BaseScreenData.REVERSED);
				sv.crtabled03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.crtabled03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.crtable04.setReverse(BaseScreenData.REVERSED);
				sv.crtable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.crtable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.crcode04.setReverse(BaseScreenData.REVERSED);
				sv.crcode04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.crcode04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.crtabled04.setReverse(BaseScreenData.REVERSED);
				sv.crtabled04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.crtabled04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.crtable05.setReverse(BaseScreenData.REVERSED);
				sv.crtable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.crtable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.crcode05.setReverse(BaseScreenData.REVERSED);
				sv.crcode05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.crcode05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.crtabled05.setReverse(BaseScreenData.REVERSED);
				sv.crtabled05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.crtabled05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.crtable06.setReverse(BaseScreenData.REVERSED);
				sv.crtable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.crtable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.crcode06.setReverse(BaseScreenData.REVERSED);
				sv.crcode06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.crcode06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.crtabled06.setReverse(BaseScreenData.REVERSED);
				sv.crtabled06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.crtabled06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.crtable07.setReverse(BaseScreenData.REVERSED);
				sv.crtable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.crtable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.crcode07.setReverse(BaseScreenData.REVERSED);
				sv.crcode07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.crcode07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.crtabled07.setReverse(BaseScreenData.REVERSED);
				sv.crtabled07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.crtabled07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.crtable08.setReverse(BaseScreenData.REVERSED);
				sv.crtable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.crtable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.crcode08.setReverse(BaseScreenData.REVERSED);
				sv.crcode08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.crcode08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.crtabled08.setReverse(BaseScreenData.REVERSED);
				sv.crtabled08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.crtabled08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.crtable09.setReverse(BaseScreenData.REVERSED);
				sv.crtable09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.crtable09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.crcode09.setReverse(BaseScreenData.REVERSED);
				sv.crcode09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.crcode09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.crtabled09.setReverse(BaseScreenData.REVERSED);
				sv.crtabled09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.crtabled09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.crtable10.setReverse(BaseScreenData.REVERSED);
				sv.crtable10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.crtable10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.crcode10.setReverse(BaseScreenData.REVERSED);
				sv.crcode10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.crcode10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.crtabled10.setReverse(BaseScreenData.REVERSED);
				sv.crtabled10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.crtabled10.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name")%></label>
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleName.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleName.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="schname">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Number")%></label>
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleNumber.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleNumber.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group" style="width: 70px;">
						<%
							if ((new Byte((sv.bcompany).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bcompany.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bcompany.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
					<div class="input-group" style="width: 70px;">
						<%
							if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctmonth.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctmonth.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Year")%></label>
					<div class="input-group" style="width: 70px;">
						<%
							if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctyear.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctyear.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
					<div style="width: 150px;">
						<%
							if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jobq.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jobq.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					<div class="input-group" style="width: 70px;">
						<%
							if ((new Byte((sv.bbranch).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bbranch.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bbranch.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Work Unit")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.worku).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.worku.getFormData();
						%>

						<%
							if ((new Byte((sv.worku).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='worku' type='text'
							value='<%=sv.worku.getFormData()%>'
							maxLength='<%=sv.worku.getLength()%>'
							size='<%=sv.worku.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.worku).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.worku).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.worku).getColor() == null ? "input_cell"
									: (sv.worku).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Extract to CSV file(Y/N)")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.extrval).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.extrval.getFormData();
						%>

						<%
							if ((new Byte((sv.extrval).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='extrval' type='text'
							value='<%=sv.extrval.getFormData()%>'
							maxLength='<%=sv.extrval.getLength()%>'
							size='<%=sv.extrval.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(extrval)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.extrval).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.extrval).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.extrval).getColor() == null ? "input_cell"
									: (sv.extrval).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Old Product")%></label>
					<table>
						<tr class="input-group ">
							<td>
								<%
									if ((new Byte((sv.contractType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.contractType.getFormData();
 %> <%
 	if ((new Byte((sv.contractType).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='contractType' type='text'
								value='<%=sv.contractType.getFormData()%>'
								maxLength='<%=sv.contractType.getLength()%>'
								size='<%=sv.contractType.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(contractType)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.contractType).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.contractType).getHighLight()))
 						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	} else {
 %> class = ' <%=(sv.contractType).getColor() == null ? "input_cell"
									: (sv.contractType).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <%
 	}
 				longValue = null;
 			}
 		}
 %>
							</td>
							<td style="min-width:71px;padding-left:1px;">
								<%
									if ((new Byte((sv.cntdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cntdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 				if (longValue == null || longValue.equalsIgnoreCase("")) {
 					formatValue = formatValue((sv.cntdesc.getFormData()).toString());
 				} else {
 					formatValue = formatValue(longValue);
 				}
 			} else {
 				if (longValue == null || longValue.equalsIgnoreCase("")) {
 					formatValue = formatValue((sv.cntdesc.getFormData()).toString());
 				} else {
 					formatValue = formatValue(longValue);
 				}
 			}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 			formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Product ")%></label>
					<table class="input-group three-controller">
						<tr>
							<td>
								<%
									if ((new Byte((sv.cnttyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.cnttyp.getFormData();
 %> <%
 	if ((new Byte((sv.cnttyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='cnttyp' type='text'
								value='<%=sv.cnttyp.getFormData()%>'
								maxLength='<%=sv.cnttyp.getLength()%>'
								size='<%=sv.cnttyp.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(cnttyp)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.cnttyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.cnttyp).getHighLight()))
 						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	} else {
 %> class = ' <%=(sv.cnttyp).getColor() == null ? "input_cell"
									: (sv.cnttyp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								> <%
 	}
 				longValue = null;
 			}
 		}
 %>
							</td>
							<td style="min-width:71px;padding-left:1px;">
								<%
									if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.ctypdesc.getFormData();
 %> <%
 	if ((new Byte((sv.ctypdesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='ctypdesc' type='text'
								value='<%=sv.ctypdesc.getFormData()%>'
								maxLength='<%=sv.ctypdesc.getLength()%>'
								size='<%=sv.ctypdesc.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(ctypdesc)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.ctypdesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.ctypdesc).getHighLight()))
 						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	} else {
 %> class = ' <%=(sv.ctypdesc).getColor() == null ? "input_cell"
									: (sv.ctypdesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								> <%
 	}
 				longValue = null;
 			}
 		}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Old Component")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Component")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Component Description")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable01.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable01' type='text'
							value='<%=sv.crtable01.getFormData()%>' id='crtable01'
							maxLength='<%=sv.crtable01.getLength()%>'
							size='<%=sv.crtable01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable01).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable01')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 



						<%
							} else {
						%>

						class = '
						<%=(sv.crtable01).getColor() == null ? "input_cell"
									: (sv.crtable01).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable01')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode01.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode01' type='text'
							value='<%=sv.crcode01.getFormData()%>'
							maxLength='<%=sv.crcode01.getLength()%>'
							size='<%=sv.crcode01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode01).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode01).getColor() == null ? "input_cell"
									: (sv.crcode01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled01.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled01' type='text'
							value='<%=sv.crtabled01.getFormData()%>'
							maxLength='<%=sv.crtabled01.getLength()%>'
							size='<%=sv.crtabled01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled01).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled01).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled01).getColor() == null ? "input_cell"
									: (sv.crtabled01).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable02.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable02' type='text'
							value='<%=sv.crtable02.getFormData()%>' id='crtable02'
							maxLength='<%=sv.crtable02.getLength()%>'
							size='<%=sv.crtable02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable02).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable02).getColor() == null ? "input_cell"
									: (sv.crtable02).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode02.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode02' type='text'
							value='<%=sv.crcode02.getFormData()%>'
							maxLength='<%=sv.crcode02.getLength()%>'
							size='<%=sv.crcode02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode02).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode02).getColor() == null ? "input_cell"
									: (sv.crcode02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled02.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled02' type='text'
							value='<%=sv.crtabled02.getFormData()%>'
							maxLength='<%=sv.crtabled02.getLength()%>'
							size='<%=sv.crtabled02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled02).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled02).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled02).getColor() == null ? "input_cell"
									: (sv.crtabled02).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable03.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable03' type='text'
							value='<%=sv.crtable03.getFormData()%>' id='crtable03'
							maxLength='<%=sv.crtable03.getLength()%>'
							size='<%=sv.crtable03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable03).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable03')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable03).getColor() == null ? "input_cell"
									: (sv.crtable03).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable03')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode03.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode03' type='text'
							value='<%=sv.crcode03.getFormData()%>'
							maxLength='<%=sv.crcode03.getLength()%>'
							size='<%=sv.crcode03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode03).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode03).getColor() == null ? "input_cell"
									: (sv.crcode03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled03.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled03' type='text'
							value='<%=sv.crtabled03.getFormData()%>'
							maxLength='<%=sv.crtabled03.getLength()%>'
							size='<%=sv.crtabled03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled03).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled03).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled03).getColor() == null ? "input_cell"
									: (sv.crtabled03).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable04.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable04' type='text'
							value='<%=sv.crtable04.getFormData()%>' id='crtable04'
							maxLength='<%=sv.crtable04.getLength()%>'
							size='<%=sv.crtable04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable04).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable04')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							} else {
						%>

						class = '
						<%=(sv.crtable04).getColor() == null ? "input_cell"
									: (sv.crtable04).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' ><span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable04')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode04.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode04' type='text'
							value='<%=sv.crcode04.getFormData()%>'
							maxLength='<%=sv.crcode04.getLength()%>'
							size='<%=sv.crcode04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode04).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode04).getColor() == null ? "input_cell"
									: (sv.crcode04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled04.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled04' type='text'
							value='<%=sv.crtabled04.getFormData()%>'
							maxLength='<%=sv.crtabled04.getLength()%>'
							size='<%=sv.crtabled04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled04).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled04).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled04).getColor() == null ? "input_cell"
									: (sv.crtabled04).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable05.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable05' type='text'
							value='<%=sv.crtable05.getFormData()%>' id='crtable05'
							maxLength='<%=sv.crtable05.getLength()%>'
							size='<%=sv.crtable05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable05).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable05')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable05).getColor() == null ? "input_cell"
									: (sv.crtable05).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable05')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode05.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode05' type='text'
							value='<%=sv.crcode05.getFormData()%>'
							maxLength='<%=sv.crcode05.getLength()%>'
							size='<%=sv.crcode05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode05).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode05).getColor() == null ? "input_cell"
									: (sv.crcode05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled05.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled05' type='text'
							value='<%=sv.crtabled05.getFormData()%>'
							maxLength='<%=sv.crtabled05.getLength()%>'
							size='<%=sv.crtabled05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled05).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled05).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled05).getColor() == null ? "input_cell"
									: (sv.crtabled05).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable06.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable06' type='text'
							value='<%=sv.crtable06.getFormData()%>' id='crtable06'
							maxLength='<%=sv.crtable06.getLength()%>'
							size='<%=sv.crtable06.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable06).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable06')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable06).getColor() == null ? "input_cell"
									: (sv.crtable06).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' ><span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable06')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode06.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode06' type='text'
							value='<%=sv.crcode06.getFormData()%>'
							maxLength='<%=sv.crcode06.getLength()%>'
							size='<%=sv.crcode06.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode06).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode06).getColor() == null ? "input_cell"
									: (sv.crcode06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled06.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled06' type='text'
							value='<%=sv.crtabled06.getFormData()%>'
							maxLength='<%=sv.crtabled06.getLength()%>'
							size='<%=sv.crtabled06.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled06).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled06).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled06).getColor() == null ? "input_cell"
									: (sv.crtabled06).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable07.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable07' type='text'
							value='<%=sv.crtable07.getFormData()%>' id='crtable07'
							maxLength='<%=sv.crtable07.getLength()%>'
							size='<%=sv.crtable07.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable07).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable07')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable07).getColor() == null ? "input_cell"
									: (sv.crtable07).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable07')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode07.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode07' type='text'
							value='<%=sv.crcode07.getFormData()%>'
							maxLength='<%=sv.crcode07.getLength()%>'
							size='<%=sv.crcode07.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode07).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode07).getColor() == null ? "input_cell"
									: (sv.crcode07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled07.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled07' type='text'
							value='<%=sv.crtabled07.getFormData()%>'
							maxLength='<%=sv.crtabled07.getLength()%>'
							size='<%=sv.crtabled07.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled07).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled07).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled07).getColor() == null ? "input_cell"
									: (sv.crtabled07).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable08.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable08' type='text'
							value='<%=sv.crtable08.getFormData()%>' id='crtable08'
							maxLength='<%=sv.crtable08.getLength()%>'
							size='<%=sv.crtable08.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable08).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable08')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							} else {
						%>

						class = '
						<%=(sv.crtable08).getColor() == null ? "input_cell"
									: (sv.crtable08).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' ><span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable08')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode08.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode08' type='text'
							value='<%=sv.crcode08.getFormData()%>'
							maxLength='<%=sv.crcode08.getLength()%>'
							size='<%=sv.crcode08.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode08).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode08).getColor() == null ? "input_cell"
									: (sv.crcode08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled08.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled08' type='text'
							value='<%=sv.crtabled08.getFormData()%>'
							maxLength='<%=sv.crtabled08.getLength()%>'
							size='<%=sv.crtabled08.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled08).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled08).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled08).getColor() == null ? "input_cell"
									: (sv.crtabled08).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable09.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable09' type='text'
							value='<%=sv.crtable09.getFormData()%>' id='crtable09'
							maxLength='<%=sv.crtable09.getLength()%>'
							size='<%=sv.crtable09.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable09).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable09')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							} else {
						%>

						class = '
						<%=(sv.crtable09).getColor() == null ? "input_cell"
									: (sv.crtable09).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable09')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode09.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode09' type='text'
							value='<%=sv.crcode09.getFormData()%>'
							maxLength='<%=sv.crcode09.getLength()%>'
							size='<%=sv.crcode09.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode09).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode09).getColor() == null ? "input_cell"
									: (sv.crcode09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled09.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled09' type='text'
							value='<%=sv.crtabled09.getFormData()%>'
							maxLength='<%=sv.crtabled09.getLength()%>'
							size='<%=sv.crtabled09.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled09).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled09).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled09).getColor() == null ? "input_cell"
									: (sv.crtabled09).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.crtable10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtable10.getFormData();
						%>

						<%
							if ((new Byte((sv.crtable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtable10' type='text'
							value='<%=sv.crtable10.getFormData()%>' id='crtable10'
							maxLength='<%=sv.crtable10.getLength()%>'
							size='<%=sv.crtable10.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtable10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtable10).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable10')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 

						<%
							} else {
						%>

						class = '
						<%=(sv.crtable10).getColor() == null ? "input_cell"
									: (sv.crtable10).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important;
 right: -3px !important";  type="button"
  onClick="doFocus(document.getElementById('crtable10')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> 


						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.crcode10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crcode10.getFormData();
						%>

						<%
							if ((new Byte((sv.crcode10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crcode10' type='text'
							value='<%=sv.crcode10.getFormData()%>'
							maxLength='<%=sv.crcode10.getLength()%>'
							size='<%=sv.crcode10.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(worku)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crcode10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crcode10).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crcode10).getColor() == null ? "input_cell"
									: (sv.crcode10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						>

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtabled10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.crtabled10.getFormData();
						%>

						<%
							if ((new Byte((sv.crtabled10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtabled10' type='text'
							value='<%=sv.crtabled10.getFormData()%>'
							maxLength='<%=sv.crtabled10.getLength()%>'
							size='<%=sv.crtabled10.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtabled10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtabled10).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtabled10).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.crtabled10).getColor() == null ? "input_cell"
									: (sv.crtabled10).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sr53bprotectWritten.gt(0)) {
%>
<%
	Sr53bprotect.clearClassString(sv);
%>

<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>
