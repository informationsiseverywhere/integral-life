<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5673";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5673ScreenVars sv = (S5673ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5673screenWritten.gt(0)) {
%>
<%
	S5673screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage");
%>
<%
	generatedText7.appendClassString("label_txt");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Allowable Riders");
%>
<%
	generatedText8.appendClassString("label_txt");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Max");
%>
<%
	generatedText9.appendClassString("label_txt");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rqd?");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Covers");
%>
<%
	generatedText11.appendClassString("label_txt");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "2");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "3");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "4");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "5");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "6");
%>
<%
	sv.ctable01.setClassString("");
%>
<%
	sv.zrlifind01.setClassString("");
%>
<%
	sv.zrlifind01.appendClassString("string_fld");
		sv.zrlifind01.appendClassString("input_txt");
		sv.zrlifind01.appendClassString("highlight");
%>
<%
	sv.creq01.setClassString("");
%>
<%
	sv.creq01.appendClassString("string_fld");
		sv.creq01.appendClassString("input_txt");
		sv.creq01.appendClassString("highlight");
%>
<%
	sv.rtable01.setClassString("");
%>
<%
	sv.rreq01.setClassString("");
%>
<%
	sv.rreq01.appendClassString("string_fld");
		sv.rreq01.appendClassString("input_txt");
		sv.rreq01.appendClassString("highlight");
%>
<%
	sv.rtable02.setClassString("");
%>
<%
	sv.rreq02.setClassString("");
%>
<%
	sv.rreq02.appendClassString("string_fld");
		sv.rreq02.appendClassString("input_txt");
		sv.rreq02.appendClassString("highlight");
%>
<%
	sv.rtable03.setClassString("");
%>
<%
	sv.rreq03.setClassString("");
%>
<%
	sv.rreq03.appendClassString("string_fld");
		sv.rreq03.appendClassString("input_txt");
		sv.rreq03.appendClassString("highlight");
%>
<%
	sv.rtable04.setClassString("");
%>
<%
	sv.rreq04.setClassString("");
%>
<%
	sv.rreq04.appendClassString("string_fld");
		sv.rreq04.appendClassString("input_txt");
		sv.rreq04.appendClassString("highlight");
%>
<%
	sv.rtable05.setClassString("");
%>
<%
	sv.rreq05.setClassString("");
%>
<%
	sv.rreq05.appendClassString("string_fld");
		sv.rreq05.appendClassString("input_txt");
		sv.rreq05.appendClassString("highlight");
%>
<%
	sv.rtable06.setClassString("");
%>
<%
	sv.rreq06.setClassString("");
%>
<%
	sv.rreq06.appendClassString("string_fld");
		sv.rreq06.appendClassString("input_txt");
		sv.rreq06.appendClassString("highlight");
%>
<%
	sv.ctmaxcov01.setClassString("");
%>
<%
	sv.ctable02.setClassString("");
%>
<%
	sv.zrlifind02.setClassString("");
%>
<%
	sv.zrlifind02.appendClassString("string_fld");
		sv.zrlifind02.appendClassString("input_txt");
		sv.zrlifind02.appendClassString("highlight");
%>
<%
	sv.creq02.setClassString("");
%>
<%
	sv.creq02.appendClassString("string_fld");
		sv.creq02.appendClassString("input_txt");
		sv.creq02.appendClassString("highlight");
%>
<%
	sv.rtable07.setClassString("");
%>
<%
	sv.rreq07.setClassString("");
%>
<%
	sv.rreq07.appendClassString("string_fld");
		sv.rreq07.appendClassString("input_txt");
		sv.rreq07.appendClassString("highlight");
%>
<%
	sv.rtable08.setClassString("");
%>
<%
	sv.rreq08.setClassString("");
%>
<%
	sv.rreq08.appendClassString("string_fld");
		sv.rreq08.appendClassString("input_txt");
		sv.rreq08.appendClassString("highlight");
%>
<%
	sv.rtable09.setClassString("");
%>
<%
	sv.rreq09.setClassString("");
%>
<%
	sv.rreq09.appendClassString("string_fld");
		sv.rreq09.appendClassString("input_txt");
		sv.rreq09.appendClassString("highlight");
%>
<%
	sv.rtable10.setClassString("");
%>
<%
	sv.rreq10.setClassString("");
%>
<%
	sv.rreq10.appendClassString("string_fld");
		sv.rreq10.appendClassString("input_txt");
		sv.rreq10.appendClassString("highlight");
%>
<%
	sv.rtable11.setClassString("");
%>
<%
	sv.rreq11.setClassString("");
%>
<%
	sv.rreq11.appendClassString("string_fld");
		sv.rreq11.appendClassString("input_txt");
		sv.rreq11.appendClassString("highlight");
%>
<%
	sv.rtable12.setClassString("");
%>
<%
	sv.rreq12.setClassString("");
%>
<%
	sv.rreq12.appendClassString("string_fld");
		sv.rreq12.appendClassString("input_txt");
		sv.rreq12.appendClassString("highlight");
%>
<%
	sv.ctmaxcov02.setClassString("");
%>
<%
	sv.ctable03.setClassString("");
%>
<%
	sv.zrlifind03.setClassString("");
%>
<%
	sv.zrlifind03.appendClassString("string_fld");
		sv.zrlifind03.appendClassString("input_txt");
		sv.zrlifind03.appendClassString("highlight");
%>
<%
	sv.creq03.setClassString("");
%>
<%
	sv.creq03.appendClassString("string_fld");
		sv.creq03.appendClassString("input_txt");
		sv.creq03.appendClassString("highlight");
%>
<%
	sv.rtable13.setClassString("");
%>
<%
	sv.rreq13.setClassString("");
%>
<%
	sv.rreq13.appendClassString("string_fld");
		sv.rreq13.appendClassString("input_txt");
		sv.rreq13.appendClassString("highlight");
%>
<%
	sv.rtable14.setClassString("");
%>
<%
	sv.rreq14.setClassString("");
%>
<%
	sv.rreq14.appendClassString("string_fld");
		sv.rreq14.appendClassString("input_txt");
		sv.rreq14.appendClassString("highlight");
%>
<%
	sv.rtable15.setClassString("");
%>
<%
	sv.rreq15.setClassString("");
%>
<%
	sv.rreq15.appendClassString("string_fld");
		sv.rreq15.appendClassString("input_txt");
		sv.rreq15.appendClassString("highlight");
%>
<%
	sv.rtable16.setClassString("");
%>
<%
	sv.rreq16.setClassString("");
%>
<%
	sv.rreq16.appendClassString("string_fld");
		sv.rreq16.appendClassString("input_txt");
		sv.rreq16.appendClassString("highlight");
%>
<%
	sv.rtable17.setClassString("");
%>
<%
	sv.rreq17.setClassString("");
%>
<%
	sv.rreq17.appendClassString("string_fld");
		sv.rreq17.appendClassString("input_txt");
		sv.rreq17.appendClassString("highlight");
%>
<%
	sv.rtable18.setClassString("");
%>
<%
	sv.rreq18.setClassString("");
%>
<%
	sv.rreq18.appendClassString("string_fld");
		sv.rreq18.appendClassString("input_txt");
		sv.rreq18.appendClassString("highlight");
%>
<%
	sv.ctmaxcov03.setClassString("");
%>
<%
	sv.ctable04.setClassString("");
%>
<%
	sv.zrlifind04.setClassString("");
%>
<%
	sv.zrlifind04.appendClassString("string_fld");
		sv.zrlifind04.appendClassString("input_txt");
		sv.zrlifind04.appendClassString("highlight");
%>
<%
	sv.creq04.setClassString("");
%>
<%
	sv.creq04.appendClassString("string_fld");
		sv.creq04.appendClassString("input_txt");
		sv.creq04.appendClassString("highlight");
%>
<%
	sv.rtable19.setClassString("");
%>
<%
	sv.rreq19.setClassString("");
%>
<%
	sv.rreq19.appendClassString("string_fld");
		sv.rreq19.appendClassString("input_txt");
		sv.rreq19.appendClassString("highlight");
%>
<%
	sv.rtable20.setClassString("");
%>
<%
	sv.rreq20.setClassString("");
%>
<%
	sv.rreq20.appendClassString("string_fld");
		sv.rreq20.appendClassString("input_txt");
		sv.rreq20.appendClassString("highlight");
%>
<%
	sv.rtable21.setClassString("");
%>
<%
	sv.rreq21.setClassString("");
%>
<%
	sv.rreq21.appendClassString("string_fld");
		sv.rreq21.appendClassString("input_txt");
		sv.rreq21.appendClassString("highlight");
%>
<%
	sv.rtable22.setClassString("");
%>
<%
	sv.rreq22.setClassString("");
%>
<%
	sv.rreq22.appendClassString("string_fld");
		sv.rreq22.appendClassString("input_txt");
		sv.rreq22.appendClassString("highlight");
%>
<%
	sv.rtable23.setClassString("");
%>
<%
	sv.rreq23.setClassString("");
%>
<%
	sv.rreq23.appendClassString("string_fld");
		sv.rreq23.appendClassString("input_txt");
		sv.rreq23.appendClassString("highlight");
%>
<%
	sv.rtable24.setClassString("");
%>
<%
	sv.rreq24.setClassString("");
%>
<%
	sv.rreq24.appendClassString("string_fld");
		sv.rreq24.appendClassString("input_txt");
		sv.rreq24.appendClassString("highlight");
%>
<%
	sv.ctmaxcov04.setClassString("");
%>
<%
	sv.ctable05.setClassString("");
%>
<%
	sv.zrlifind05.setClassString("");
%>
<%
	sv.zrlifind05.appendClassString("string_fld");
		sv.zrlifind05.appendClassString("input_txt");
		sv.zrlifind05.appendClassString("highlight");
%>
<%
	sv.creq05.setClassString("");
%>
<%
	sv.creq05.appendClassString("string_fld");
		sv.creq05.appendClassString("input_txt");
		sv.creq05.appendClassString("highlight");
%>
<%
	sv.rtable25.setClassString("");
%>
<%
	sv.rreq25.setClassString("");
%>
<%
	sv.rreq25.appendClassString("string_fld");
		sv.rreq25.appendClassString("input_txt");
		sv.rreq25.appendClassString("highlight");
%>
<%
	sv.rtable26.setClassString("");
%>
<%
	sv.rreq26.setClassString("");
%>
<%
	sv.rreq26.appendClassString("string_fld");
		sv.rreq26.appendClassString("input_txt");
		sv.rreq26.appendClassString("highlight");
%>
<%
	sv.rtable27.setClassString("");
%>
<%
	sv.rreq27.setClassString("");
%>
<%
	sv.rreq27.appendClassString("string_fld");
		sv.rreq27.appendClassString("input_txt");
		sv.rreq27.appendClassString("highlight");
%>
<%
	sv.rtable28.setClassString("");
%>
<%
	sv.rreq28.setClassString("");
%>
<%
	sv.rreq28.appendClassString("string_fld");
		sv.rreq28.appendClassString("input_txt");
		sv.rreq28.appendClassString("highlight");
%>
<%
	sv.rtable29.setClassString("");
%>
<%
	sv.rreq29.setClassString("");
%>
<%
	sv.rreq29.appendClassString("string_fld");
		sv.rreq29.appendClassString("input_txt");
		sv.rreq29.appendClassString("highlight");
%>
<%
	sv.rtable30.setClassString("");
%>
<%
	sv.rreq30.setClassString("");
%>
<%
	sv.rreq30.appendClassString("string_fld");
		sv.rreq30.appendClassString("input_txt");
		sv.rreq30.appendClassString("highlight");
%>
<%
	sv.ctmaxcov05.setClassString("");
%>
<%
	sv.ctable06.setClassString("");
%>
<%
	sv.zrlifind06.setClassString("");
%>
<%
	sv.zrlifind06.appendClassString("string_fld");
		sv.zrlifind06.appendClassString("input_txt");
		sv.zrlifind06.appendClassString("highlight");
%>
<%
	sv.creq06.setClassString("");
%>
<%
	sv.creq06.appendClassString("string_fld");
		sv.creq06.appendClassString("input_txt");
		sv.creq06.appendClassString("highlight");
%>
<%
	sv.rtable31.setClassString("");
%>
<%
	sv.rreq31.setClassString("");
%>
<%
	sv.rreq31.appendClassString("string_fld");
		sv.rreq31.appendClassString("input_txt");
		sv.rreq31.appendClassString("highlight");
%>
<%
	sv.rtable32.setClassString("");
%>
<%
	sv.rreq32.setClassString("");
%>
<%
	sv.rreq32.appendClassString("string_fld");
		sv.rreq32.appendClassString("input_txt");
		sv.rreq32.appendClassString("highlight");
%>
<%
	sv.rtable33.setClassString("");
%>
<%
	sv.rreq33.setClassString("");
%>
<%
	sv.rreq33.appendClassString("string_fld");
		sv.rreq33.appendClassString("input_txt");
		sv.rreq33.appendClassString("highlight");
%>
<%
	sv.rtable34.setClassString("");
%>
<%
	sv.rreq34.setClassString("");
%>
<%
	sv.rreq34.appendClassString("string_fld");
		sv.rreq34.appendClassString("input_txt");
		sv.rreq34.appendClassString("highlight");
%>
<%
	sv.rtable35.setClassString("");
%>
<%
	sv.rreq35.setClassString("");
%>
<%
	sv.rreq35.appendClassString("string_fld");
		sv.rreq35.appendClassString("input_txt");
		sv.rreq35.appendClassString("highlight");
%>
<%
	sv.rtable36.setClassString("");
%>
<%
	sv.rreq36.setClassString("");
%>
<%
	sv.rreq36.appendClassString("string_fld");
		sv.rreq36.appendClassString("input_txt");
		sv.rreq36.appendClassString("highlight");
%>
<%
	sv.ctmaxcov06.setClassString("");
%>
<%
	sv.ctable07.setClassString("");
%>
<%
	sv.zrlifind07.setClassString("");
%>
<%
	sv.zrlifind07.appendClassString("string_fld");
		sv.zrlifind07.appendClassString("input_txt");
		sv.zrlifind07.appendClassString("highlight");
%>
<%
	sv.creq07.setClassString("");
%>
<%
	sv.creq07.appendClassString("string_fld");
		sv.creq07.appendClassString("input_txt");
		sv.creq07.appendClassString("highlight");
%>
<%
	sv.rtable37.setClassString("");
%>
<%
	sv.rreq37.setClassString("");
%>
<%
	sv.rreq37.appendClassString("string_fld");
		sv.rreq37.appendClassString("input_txt");
		sv.rreq37.appendClassString("highlight");
%>
<%
	sv.rtable38.setClassString("");
%>
<%
	sv.rreq38.setClassString("");
%>
<%
	sv.rreq38.appendClassString("string_fld");
		sv.rreq38.appendClassString("input_txt");
		sv.rreq38.appendClassString("highlight");
%>
<%
	sv.rtable39.setClassString("");
%>
<%
	sv.rreq39.setClassString("");
%>
<%
	sv.rreq39.appendClassString("string_fld");
		sv.rreq39.appendClassString("input_txt");
		sv.rreq39.appendClassString("highlight");
%>
<%
	sv.rtable40.setClassString("");
%>
<%
	sv.rreq40.setClassString("");
%>
<%
	sv.rreq40.appendClassString("string_fld");
		sv.rreq40.appendClassString("input_txt");
		sv.rreq40.appendClassString("highlight");
%>
<%
	sv.rtable41.setClassString("");
%>
<%
	sv.rreq41.setClassString("");
%>
<%
	sv.rreq41.appendClassString("string_fld");
		sv.rreq41.appendClassString("input_txt");
		sv.rreq41.appendClassString("highlight");
%>
<%
	sv.rtable42.setClassString("");
%>
<%
	sv.rreq42.setClassString("");
%>
<%
	sv.rreq42.appendClassString("string_fld");
		sv.rreq42.appendClassString("input_txt");
		sv.rreq42.appendClassString("highlight");
%>
<%
	sv.ctmaxcov07.setClassString("");
%>
<%
	sv.ctable08.setClassString("");
%>
<%
	sv.zrlifind08.setClassString("");
%>
<%
	sv.zrlifind08.appendClassString("string_fld");
		sv.zrlifind08.appendClassString("input_txt");
		sv.zrlifind08.appendClassString("highlight");
%>
<%
	sv.creq08.setClassString("");
%>
<%
	sv.creq08.appendClassString("string_fld");
		sv.creq08.appendClassString("input_txt");
		sv.creq08.appendClassString("highlight");
%>
<%
	sv.rtable43.setClassString("");
%>
<%
	sv.rreq43.setClassString("");
%>
<%
	sv.rreq43.appendClassString("string_fld");
		sv.rreq43.appendClassString("input_txt");
		sv.rreq43.appendClassString("highlight");
%>
<%
	sv.rtable44.setClassString("");
%>
<%
	sv.rreq44.setClassString("");
%>
<%
	sv.rreq44.appendClassString("string_fld");
		sv.rreq44.appendClassString("input_txt");
		sv.rreq44.appendClassString("highlight");
%>
<%
	sv.rtable45.setClassString("");
%>
<%
	sv.rreq45.setClassString("");
%>
<%
	sv.rreq45.appendClassString("string_fld");
		sv.rreq45.appendClassString("input_txt");
		sv.rreq45.appendClassString("highlight");
%>
<%
	sv.rtable46.setClassString("");
%>
<%
	sv.rreq46.setClassString("");
%>
<%
	sv.rreq46.appendClassString("string_fld");
		sv.rreq46.appendClassString("input_txt");
		sv.rreq46.appendClassString("highlight");
%>
<%
	sv.rtable47.setClassString("");
%>
<%
	sv.rreq47.setClassString("");
%>
<%
	sv.rreq47.appendClassString("string_fld");
		sv.rreq47.appendClassString("input_txt");
		sv.rreq47.appendClassString("highlight");
%>
<%
	sv.rtable48.setClassString("");
%>
<%
	sv.rreq48.setClassString("");
%>
<%
	sv.rreq48.appendClassString("string_fld");
		sv.rreq48.appendClassString("input_txt");
		sv.rreq48.appendClassString("highlight");
%>
<%
	sv.ctmaxcov08.setClassString("");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Continuation Item ");
%>
<%
	sv.gitem.setClassString("");
%>
<%
	sv.gitem.appendClassString("string_fld");
		sv.gitem.appendClassString("input_txt");
		sv.gitem.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind11.isOn()) {
				sv.ctable01.setReverse(BaseScreenData.REVERSED);
				sv.ctable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.ctable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.rtable01.setReverse(BaseScreenData.REVERSED);
				sv.rtable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.rtable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.rtable02.setReverse(BaseScreenData.REVERSED);
				sv.rtable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.rtable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.rtable03.setReverse(BaseScreenData.REVERSED);
				sv.rtable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.rtable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.rtable04.setReverse(BaseScreenData.REVERSED);
				sv.rtable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.rtable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.rtable05.setReverse(BaseScreenData.REVERSED);
				sv.rtable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.rtable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.rtable06.setReverse(BaseScreenData.REVERSED);
				sv.rtable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.rtable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.ctmaxcov01.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.ctmaxcov01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.ctable02.setReverse(BaseScreenData.REVERSED);
				sv.ctable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.ctable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.rtable07.setReverse(BaseScreenData.REVERSED);
				sv.rtable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.rtable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.rtable08.setReverse(BaseScreenData.REVERSED);
				sv.rtable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.rtable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.rtable09.setReverse(BaseScreenData.REVERSED);
				sv.rtable09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.rtable09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.rtable10.setReverse(BaseScreenData.REVERSED);
				sv.rtable10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.rtable10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.rtable11.setReverse(BaseScreenData.REVERSED);
				sv.rtable11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.rtable11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.rtable12.setReverse(BaseScreenData.REVERSED);
				sv.rtable12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.rtable12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.ctmaxcov02.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.ctmaxcov02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.ctable03.setReverse(BaseScreenData.REVERSED);
				sv.ctable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.ctable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.rtable13.setReverse(BaseScreenData.REVERSED);
				sv.rtable13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.rtable13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.rtable14.setReverse(BaseScreenData.REVERSED);
				sv.rtable14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.rtable14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.rtable15.setReverse(BaseScreenData.REVERSED);
				sv.rtable15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.rtable15.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.rtable16.setReverse(BaseScreenData.REVERSED);
				sv.rtable16.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.rtable16.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.rtable17.setReverse(BaseScreenData.REVERSED);
				sv.rtable17.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.rtable17.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.rtable18.setReverse(BaseScreenData.REVERSED);
				sv.rtable18.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.rtable18.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.ctmaxcov03.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.ctmaxcov03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.ctable04.setReverse(BaseScreenData.REVERSED);
				sv.ctable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.ctable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind36.isOn()) {
				sv.rtable19.setReverse(BaseScreenData.REVERSED);
				sv.rtable19.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
				sv.rtable19.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind37.isOn()) {
				sv.rtable20.setReverse(BaseScreenData.REVERSED);
				sv.rtable20.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind37.isOn()) {
				sv.rtable20.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
				sv.rtable21.setReverse(BaseScreenData.REVERSED);
				sv.rtable21.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind38.isOn()) {
				sv.rtable21.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind39.isOn()) {
				sv.rtable22.setReverse(BaseScreenData.REVERSED);
				sv.rtable22.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind39.isOn()) {
				sv.rtable22.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind40.isOn()) {
				sv.rtable23.setReverse(BaseScreenData.REVERSED);
				sv.rtable23.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind40.isOn()) {
				sv.rtable23.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind41.isOn()) {
				sv.rtable24.setReverse(BaseScreenData.REVERSED);
				sv.rtable24.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind41.isOn()) {
				sv.rtable24.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind42.isOn()) {
				sv.ctmaxcov04.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind42.isOn()) {
				sv.ctmaxcov04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind43.isOn()) {
				sv.ctable05.setReverse(BaseScreenData.REVERSED);
				sv.ctable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind43.isOn()) {
				sv.ctable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind44.isOn()) {
				sv.rtable25.setReverse(BaseScreenData.REVERSED);
				sv.rtable25.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind44.isOn()) {
				sv.rtable25.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind45.isOn()) {
				sv.rtable26.setReverse(BaseScreenData.REVERSED);
				sv.rtable26.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind45.isOn()) {
				sv.rtable26.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind46.isOn()) {
				sv.rtable27.setReverse(BaseScreenData.REVERSED);
				sv.rtable27.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind46.isOn()) {
				sv.rtable27.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind47.isOn()) {
				sv.rtable28.setReverse(BaseScreenData.REVERSED);
				sv.rtable28.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind47.isOn()) {
				sv.rtable28.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind48.isOn()) {
				sv.rtable29.setReverse(BaseScreenData.REVERSED);
				sv.rtable29.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind48.isOn()) {
				sv.rtable29.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind49.isOn()) {
				sv.rtable30.setReverse(BaseScreenData.REVERSED);
				sv.rtable30.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind49.isOn()) {
				sv.rtable30.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind50.isOn()) {
				sv.ctmaxcov05.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind50.isOn()) {
				sv.ctmaxcov05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind51.isOn()) {
				sv.ctable06.setReverse(BaseScreenData.REVERSED);
				sv.ctable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind51.isOn()) {
				sv.ctable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind52.isOn()) {
				sv.rtable31.setReverse(BaseScreenData.REVERSED);
				sv.rtable31.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind52.isOn()) {
				sv.rtable31.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind53.isOn()) {
				sv.rtable32.setReverse(BaseScreenData.REVERSED);
				sv.rtable32.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind53.isOn()) {
				sv.rtable32.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind54.isOn()) {
				sv.rtable33.setReverse(BaseScreenData.REVERSED);
				sv.rtable33.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind54.isOn()) {
				sv.rtable33.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind55.isOn()) {
				sv.rtable34.setReverse(BaseScreenData.REVERSED);
				sv.rtable34.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind55.isOn()) {
				sv.rtable34.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind56.isOn()) {
				sv.rtable35.setReverse(BaseScreenData.REVERSED);
				sv.rtable35.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind56.isOn()) {
				sv.rtable35.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind57.isOn()) {
				sv.rtable36.setReverse(BaseScreenData.REVERSED);
				sv.rtable36.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind57.isOn()) {
				sv.rtable36.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind58.isOn()) {
				sv.ctmaxcov06.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind58.isOn()) {
				sv.ctmaxcov06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind59.isOn()) {
				sv.ctable07.setReverse(BaseScreenData.REVERSED);
				sv.ctable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind59.isOn()) {
				sv.ctable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind60.isOn()) {
				sv.rtable37.setReverse(BaseScreenData.REVERSED);
				sv.rtable37.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind60.isOn()) {
				sv.rtable37.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind61.isOn()) {
				sv.rtable38.setReverse(BaseScreenData.REVERSED);
				sv.rtable38.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind61.isOn()) {
				sv.rtable38.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind62.isOn()) {
				sv.rtable39.setReverse(BaseScreenData.REVERSED);
				sv.rtable39.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind62.isOn()) {
				sv.rtable39.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind63.isOn()) {
				sv.rtable40.setReverse(BaseScreenData.REVERSED);
				sv.rtable40.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind63.isOn()) {
				sv.rtable40.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind64.isOn()) {
				sv.rtable41.setReverse(BaseScreenData.REVERSED);
				sv.rtable41.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind64.isOn()) {
				sv.rtable41.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind65.isOn()) {
				sv.rtable42.setReverse(BaseScreenData.REVERSED);
				sv.rtable42.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind65.isOn()) {
				sv.rtable42.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind66.isOn()) {
				sv.ctmaxcov07.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind66.isOn()) {
				sv.ctmaxcov07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind67.isOn()) {
				sv.ctable08.setReverse(BaseScreenData.REVERSED);
				sv.ctable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind67.isOn()) {
				sv.ctable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind68.isOn()) {
				sv.rtable43.setReverse(BaseScreenData.REVERSED);
				sv.rtable43.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind68.isOn()) {
				sv.rtable43.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind69.isOn()) {
				sv.rtable44.setReverse(BaseScreenData.REVERSED);
				sv.rtable44.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind69.isOn()) {
				sv.rtable44.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind70.isOn()) {
				sv.rtable45.setReverse(BaseScreenData.REVERSED);
				sv.rtable45.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind70.isOn()) {
				sv.rtable45.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind71.isOn()) {
				sv.rtable46.setReverse(BaseScreenData.REVERSED);
				sv.rtable46.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind71.isOn()) {
				sv.rtable46.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind72.isOn()) {
				sv.rtable47.setReverse(BaseScreenData.REVERSED);
				sv.rtable47.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind72.isOn()) {
				sv.rtable47.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind73.isOn()) {
				sv.rtable48.setReverse(BaseScreenData.REVERSED);
				sv.rtable48.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind73.isOn()) {
				sv.rtable48.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind74.isOn()) {
				sv.ctmaxcov08.setReverse(BaseScreenData.REVERSED);
				sv.ctmaxcov08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind74.isOn()) {
				sv.ctmaxcov08.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"  style="border-right: thin solid #dddddd !important;"
						width='100%'>
						<thead>
							<tr class='info'>
								<th>Coverage</th>
								<th>Life</th>
								<th>Rqd?</th>
								<th colspan="6"><center> Allowable Riders</center></th>
								<th>Max Covers</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><%=smartHF.getLit(generatedText12)%></td>
								<td><%=smartHF.getLit(generatedText13)%></td>
								<td><%=smartHF.getLit(generatedText14)%></td>
								<td><%=smartHF.getLit(generatedText15)%></td>
								<td><%=smartHF.getLit(generatedText16)%></td>
								<td><%=smartHF.getLit(generatedText17)%></td>
								<td></td>
							</tr>
							<tr>
								<td>
								<%
                                         if ((new Byte((sv.ctable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind01).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq01).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
								<%
                                         if ((new Byte((sv.rtable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable01)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq01)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq01).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable02)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq02)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq02).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable03)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq03)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq03).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable04)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq04)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq04).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable05)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq05)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq05).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable06)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq06)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq06).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
								<div class="input-group" style="width: 46px;">
								<%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
								</td>
							</tr>
							<tr>
								<td>
								<%
                                         if ((new Byte((sv.ctable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind02).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq02).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable07)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq07)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq07).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
								<%
                                         if ((new Byte((sv.rtable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable08)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq08)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq08).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable09)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq09)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq09).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable10)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq10)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq10).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable11)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq11)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable11)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable11')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq11).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable12)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq12)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable12)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable12')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq12).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td><div class="input-group" style="width: 46px;">
								<%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
								</td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind03).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq03).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable13)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq13)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable13)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable13')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq13).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable14)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq14)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable14)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable14')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq14).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable15)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq15)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable15)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable15')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq15).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable16)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq16)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable16)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable16')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq16).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable17)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq17)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable17)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable17')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq17).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable18)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq18)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable18)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable18')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq18).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
							</div>	</td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind04).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq04).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable19)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq19)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable19)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable19')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq19).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable20)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq20)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable20)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable20')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq20).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable21).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable21)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq21)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable21)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable21')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq21).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable22).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable22)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq22)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable22)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable22')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq22).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>	
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable23).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable23)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq23)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable23)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable23')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq23).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable24).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable24)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq24)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable24)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable24')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq24).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								</div></td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind05).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq05).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable25).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable25)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq25)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable25)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable25')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq25).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable26).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable26)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq26)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable26)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable26')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq26).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable27).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable27)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq27)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable27)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable27')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq27).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable28).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable28)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq28)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable28)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable28')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq28).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable29).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable29)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq29)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable29)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable29')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq29).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable30).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable30)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq30)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable30)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable30')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq30).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								</div></td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind06).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq06).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable31).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable31)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq31)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable31)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable31')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq31).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable32).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable32)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq32)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable32)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable32')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq32).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable33).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable33)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq33)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable33)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable33')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq33).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable34).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable34)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq34)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable34)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable34')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq34).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable35).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable35)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq35)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable35)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable35')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq35).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable36).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable36)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq36)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable36)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable36')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq36).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								</div></td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind07).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq07).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable37).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable37)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq37)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable37)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable37')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq37).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable38).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable38)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq38)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable38)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable38')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq38).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable39).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable39)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq39)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable39)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable39')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq39).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable40).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable40)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq40)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable40)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable40')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq40).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable41).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable41)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq41)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable41)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable41')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq41).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable42).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable42)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq42)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable42)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable42')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq42).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								</div></td>
							</tr>
							<tr>
								<td><%
                                         if ((new Byte((sv.ctable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 110px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
									
								</td>
								<td><div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.zrlifind08).replaceAll("width:10px", "width:15px")%></div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.creq08).replaceAll("width:10px", "width:15px")%>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable43).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable43)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq43)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable43)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable43')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq43).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable44).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable44)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq44)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable44)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable44')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq44).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable45).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable45)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq45)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable45)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable45')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq45).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable46).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable46)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq46)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable46)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable46')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq46).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable47).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable47)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq47)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable47)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable47')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq47).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td>
									<%
                                         if ((new Byte((sv.rtable48).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 93px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.rtable48)%>
                                    			<%=smartHF.getHTMLVarExt(fw, sv.rreq48)%>          
                                                       
                                         
                                  </div>
                                    
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.rtable48)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('rtable48')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                         <%=smartHF.getHTMLVarExt(fw, sv.rreq48).replaceAll("width:10px", "width:15px")%>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
								<td><div class="input-group" style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.ctmaxcov08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								</div></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br />
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.gitem)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>
<%
	if (sv.S5673protectWritten.gt(0)) {
%>
<%
	S5673protect.clearClassString(sv);
%>
<%
	}
%>

 


<%@ include file="/POLACommon2NEW.jsp"%>

