

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR553";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr553ScreenVars sv = (Sr553ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind03.isOn()) {
			sv.optind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.optind.setReverse(BaseScreenData.REVERSED);
			sv.optind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.optind.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Order by    ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"1-Client Name   2-IC Number   3-Contract Number ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Start       ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Opt");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client Name");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "IC No ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.actn.setReverse(BaseScreenData.REVERSED);
			sv.actn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.actn.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Order by")%></label>
					<div style="width: 150px;">
						<select value='<%=sv.actn.getFormData()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(actn)'
							onKeyUp='return checkMaxLength(this)' name='actn'
							class="input_cell">
							<!-- MIBT-182 -->
							<option value="1"
								<%if (sv.actn.getFormData().equalsIgnoreCase("1")) {
				out.println("selected");
			}%>><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></option>
							<option value="2"
								<%if (sv.actn.getFormData().equalsIgnoreCase("2")) {
				out.println("selected");
			}%>><%=resourceBundleHandler.gettingValueFromBundle("IC Number")%></option>
							<option value="3"
								<%if (sv.actn.getFormData().equalsIgnoreCase("3")) {
				out.println("selected");
			}%>><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></option>


							<%
								if ((new Byte((sv.actn).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
							%> readonly="true" class="output_cell"
							<%
								} else if ((new Byte((sv.actn).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%> class="bold_cell"

							<%
								} else {
							%> class = '
							<%=(sv.actn).getColor() == null
						? "input_cell"
						: (sv.actn).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'

							<%
								}
							%>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Start")%></label>
					<div>
						<input name='clntnaml' type='text'
							<%formatValue = (sv.clntnaml.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.clntnaml.getLength()%>'
							maxLength='<%=sv.clntnaml.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(clntnaml)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.clntnaml).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.clntnaml).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.clntnaml).getColor() == null
						? "input_cell"
						: (sv.clntnaml).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br />
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[3];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.clntlname.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.clntlname.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.idno.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.idno.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.mlentity.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.mlentity.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			totalTblWidth = totalTblWidth - 200;
			tblColumnWidth[2] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr553screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr553' width='100%'>
							<thead>
								<tr class='info'>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client name")%></center></th>

									<th><center><%=resourceBundleHandler.gettingValueFromBundle("IC number")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("contract number")%></center></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr553screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr553screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.optind.getLength()%>'
											value='<%=sv.optind.getFormData()%>'
											size='<%=sv.optind.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(sr553screensfl.optind)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="sr553screensfl" + "." + "optind" + "_R" + count%>'
											id='<%="sr553screensfl" + "." + "optind" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.optind.getLength() * 12%> px;">

									</div>




									</td>
									<!-- ILIFE-929 begins -->
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0] - 140%>px;"
										<%if ((sv.clntlname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.clntlname.getFormData()%>




									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[1]%>px;"
										<%if ((sv.idno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<!--    <%=sv.idno.getFormData()%>--> <a href="javascript:;"
										class='tableLink'
										onClick='document.getElementById("<%="sr553screensfl" + "." + "optind" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.idno.getFormData()%></span></a>



									</td>
									<!-- ILIFE-929 ends -->
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[2] - 60%>px;"
										<%if ((sv.mlentity).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.mlentity.getFormData()%>



									</td>

								</tr>

								<%
									count = count + 1;
										Sr553screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="visibility: hidden;">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (!((sv.reptname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.reptname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.reptname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-sr553').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2581 Life Cross Browser - Sprint 2 D3 : Task 5  -->
<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script language="javascript">
       window.onload= function(){
              setDisabledMoreBtn();
       }
       </script>
<%
	}
%>
<!-- ILIFE-2581 Life Cross Browser - Sprint 2 D3 : Task 5  -->
