<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51F";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51fScreenVars sv = (Sr51fScreenVars) fw.getVariables();
%>
<%
	{
	}
%>



<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:60px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td>

						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td></tr></table>
				
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td> <label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable01" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable01");
								optionValue = makeDropDownList(mappedItems, sv.crtable01.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable01.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable01, fw, longValue, "crtable01", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable02" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable02");
								optionValue = makeDropDownList(mappedItems, sv.crtable02.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable02.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable02, fw, longValue, "crtable02", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable03" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable03");
								optionValue = makeDropDownList(mappedItems, sv.crtable03.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable03.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable03, fw, longValue, "crtable03", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable04" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable04");
								optionValue = makeDropDownList(mappedItems, sv.crtable04.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable04.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable04, fw, longValue, "crtable04", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable05" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable05");
								optionValue = makeDropDownList(mappedItems, sv.crtable05.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable05.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable05, fw, longValue, "crtable05", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable06" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable06");
								optionValue = makeDropDownList(mappedItems, sv.crtable06.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable06.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable06, fw, longValue, "crtable06", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable07" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable07");
								optionValue = makeDropDownList(mappedItems, sv.crtable07.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable07.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable07, fw, longValue, "crtable07", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable08" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable08");
								optionValue = makeDropDownList(mappedItems, sv.crtable08.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable08.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable08, fw, longValue, "crtable08", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable09" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable09");
								optionValue = makeDropDownList(mappedItems, sv.crtable09.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable09.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable09, fw, longValue, "crtable09", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable10" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable10");
								optionValue = makeDropDownList(mappedItems, sv.crtable10.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable10.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable10, fw, longValue, "crtable10", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable11" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable11");
								optionValue = makeDropDownList(mappedItems, sv.crtable11.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable11.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable11, fw, longValue, "crtable11", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable12" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable12");
								optionValue = makeDropDownList(mappedItems, sv.crtable12.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable12.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable12, fw, longValue, "crtable12", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable13" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable13");
								optionValue = makeDropDownList(mappedItems, sv.crtable13.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable13.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable13, fw, longValue, "crtable13", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable14" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable14");
								optionValue = makeDropDownList(mappedItems, sv.crtable14.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable14.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable14, fw, longValue, "crtable14", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable15" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable15");
								optionValue = makeDropDownList(mappedItems, sv.crtable15.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable15.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable15, fw, longValue, "crtable15", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable16" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable16");
								optionValue = makeDropDownList(mappedItems, sv.crtable16.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable16.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable16, fw, longValue, "crtable16", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable17" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable17");
								optionValue = makeDropDownList(mappedItems, sv.crtable17.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable17.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable17, fw, longValue, "crtable17", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable18" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable18");
								optionValue = makeDropDownList(mappedItems, sv.crtable18.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable18.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable18, fw, longValue, "crtable18", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable19" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable19");
								optionValue = makeDropDownList(mappedItems, sv.crtable19.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable19.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable19, fw, longValue, "crtable19", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							if ((new Byte((sv.crtable20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "crtable20" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable20");
								optionValue = makeDropDownList(mappedItems, sv.crtable20.getFormData(), 3, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable20.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.crtable20, fw, longValue, "crtable20", optionValue, 0, 177)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Band ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Sum Assured ")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age01) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age01, (sv.age01.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age01) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins01, (sv.sumins01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins01,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
							<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age02) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age02, (sv.age02.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age02) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins02, (sv.sumins02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins02,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age03) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age03, (sv.age03.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age03) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins03) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins03, (sv.sumins03.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins03) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins03,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age04) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age04, (sv.age04.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age04) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins04) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins04, (sv.sumins04.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins04) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins04,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age05) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age05, (sv.age05.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age05) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins05) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins05, (sv.sumins05.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins05) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins05,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age06) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age06, (sv.age06.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age06) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins06) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins06, (sv.sumins06.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins06) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins06,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age07) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age07, (sv.age07.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age07) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins07) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins07, (sv.sumins07.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins07) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins07,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age08) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age08, (sv.age08.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age08) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins08) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins08, (sv.sumins08.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins08) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins08,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age09) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age09, (sv.age09.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age09) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins09) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins09, (sv.sumins09.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins09) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins09,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td style="padding-top: 7px;"><label><%=resourceBundleHandler.gettingValueFromBundle("<=")%></label></td>
								<td style="min-width:46px">
								<div>
									<%
										if (((BaseScreenData) sv.age10) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.age10, (sv.age10.getLength() + 1), null).replace("absolute",
						"relative")%>
									<%
										} else if (((BaseScreenData) sv.age10) instanceof DecimalData) {
									%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.age10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
									<%
										} else {
									%>
									hello
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.sumins10) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.sumins10, (sv.sumins10.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.sumins10) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVarExt(fw, sv.sumins10,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 130)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<!-- Ticket #ILIFE-1595 ends -->
<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>


<%@ include file="/POLACommon2NEW.jsp"%>