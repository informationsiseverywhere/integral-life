

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5687";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5687ScreenVars sv = (S5687ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid from ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premiums ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Calculation method ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"J/Life method ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"J/Life Allowed ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Guarantee period ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Re-calc. freq ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Single Payment ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Stamp Duty calc ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Stamp Duty pay ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reducing Term ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit billing ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Non-forfeiture ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Anniversary Proc ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Part Surrender ");
%>
<%
	StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Freq Alt Basis");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Commissions ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Regular");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Single");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Top-up");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Basic Calc Method ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Basic Pay Method ");
%>
<%
	StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"OR Calc Method ");
%>
<%
	StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"OR Pay  Method ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Servicing Pay Method ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Renewal Pay Method ");
%>
<%
	StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Commission paid on Basic Premium Only ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Default follow-ups ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Schedule Code ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prem Code  ");
%>
<%
	StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Statutory fund ");
%>
<%
	StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub-section ");
%>
<%
	StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reporting codes ");
%>
<%
	StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Maturity calc ");
%>
<%
	StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Loan Method ");
%>
<%
	StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Sch Calc ");
%>
<%
	StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Surrender calc ");
%>
<%
	StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Death calc ");
%>
<%
	StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid-up calc ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.premmeth.setReverse(BaseScreenData.REVERSED);
			sv.premmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.premmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.jlPremMeth.setReverse(BaseScreenData.REVERSED);
			sv.jlPremMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.jlPremMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.jlifePresent.setReverse(BaseScreenData.REVERSED);
			sv.jlifePresent.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.jlifePresent.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.premGuarPeriod.setReverse(BaseScreenData.REVERSED);
			sv.premGuarPeriod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.premGuarPeriod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rtrnwfreq.setReverse(BaseScreenData.REVERSED);
			sv.rtrnwfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rtrnwfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.singlePremInd.setReverse(BaseScreenData.REVERSED);
			sv.singlePremInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.singlePremInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.stampDutyMeth.setReverse(BaseScreenData.REVERSED);
			sv.stampDutyMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.stampDutyMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.sdtyPayMeth.setReverse(BaseScreenData.REVERSED);
			sv.sdtyPayMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.sdtyPayMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.zsredtrm.setReverse(BaseScreenData.REVERSED);
			sv.zsredtrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.zsredtrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.bbmeth.setReverse(BaseScreenData.REVERSED);
			sv.bbmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.bbmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.nonForfeitMethod.setReverse(BaseScreenData.REVERSED);
			sv.nonForfeitMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.nonForfeitMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.anniversaryMethod.setReverse(BaseScreenData.REVERSED);
			sv.anniversaryMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.anniversaryMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.partsurr.setReverse(BaseScreenData.REVERSED);
			sv.partsurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.partsurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.basicCommMeth.setReverse(BaseScreenData.REVERSED);
			sv.basicCommMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.basicCommMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.basscmth.setReverse(BaseScreenData.REVERSED);
			sv.basscmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.basscmth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.bastcmth.setReverse(BaseScreenData.REVERSED);
			sv.bastcmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.bastcmth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.bascpy.setReverse(BaseScreenData.REVERSED);
			sv.bascpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.bascpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.basscpy.setReverse(BaseScreenData.REVERSED);
			sv.basscpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.basscpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.bastcpy.setReverse(BaseScreenData.REVERSED);
			sv.bastcpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.bastcpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.srvcpy.setReverse(BaseScreenData.REVERSED);
			sv.srvcpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.srvcpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.rnwcpy.setReverse(BaseScreenData.REVERSED);
			sv.rnwcpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.rnwcpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.defFupMeth.setReverse(BaseScreenData.REVERSED);
			sv.defFupMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.defFupMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.schedCode.setReverse(BaseScreenData.REVERSED);
			sv.schedCode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.schedCode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.statFund.setReverse(BaseScreenData.REVERSED);
			sv.statFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.statFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.statSect.setReverse(BaseScreenData.REVERSED);
			sv.statSect.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.statSect.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.statSubSect.setReverse(BaseScreenData.REVERSED);
			sv.statSubSect.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.statSubSect.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.reptcd01.setReverse(BaseScreenData.REVERSED);
			sv.reptcd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.reptcd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.reptcd02.setReverse(BaseScreenData.REVERSED);
			sv.reptcd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.reptcd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.reptcd03.setReverse(BaseScreenData.REVERSED);
			sv.reptcd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.reptcd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.reptcd04.setReverse(BaseScreenData.REVERSED);
			sv.reptcd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.reptcd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.reptcd05.setReverse(BaseScreenData.REVERSED);
			sv.reptcd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.reptcd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.reptcd06.setReverse(BaseScreenData.REVERSED);
			sv.reptcd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.reptcd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.maturityCalcMeth.setReverse(BaseScreenData.REVERSED);
			sv.maturityCalcMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.maturityCalcMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.svMethod.setReverse(BaseScreenData.REVERSED);
			sv.svMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.svMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.dcmeth.setReverse(BaseScreenData.REVERSED);
			sv.dcmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.dcmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.pumeth.setReverse(BaseScreenData.REVERSED);
			sv.pumeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.pumeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.xfreqAltBasis.setReverse(BaseScreenData.REVERSED);
			sv.xfreqAltBasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.xfreqAltBasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.loanmeth.setReverse(BaseScreenData.REVERSED);
			sv.loanmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.loanmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.zrorcmrg.setReverse(BaseScreenData.REVERSED);
			sv.zrorcmrg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.zrorcmrg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.zrorpmrg.setReverse(BaseScreenData.REVERSED);
			sv.zrorpmrg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.zrorpmrg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.zrorcmsp.setReverse(BaseScreenData.REVERSED);
			sv.zrorcmsp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.zrorcmsp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.zrorpmsp.setReverse(BaseScreenData.REVERSED);
			sv.zrorpmsp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.zrorpmsp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.zrorcmtu.setReverse(BaseScreenData.REVERSED);
			sv.zrorcmtu.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.zrorcmtu.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.zrorpmtu.setReverse(BaseScreenData.REVERSED);
			sv.zrorpmtu.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.zrorpmtu.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.zsbsmeth.setReverse(BaseScreenData.REVERSED);
			sv.zsbsmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.zsbsmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.lnkgind.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>




</td><td>




						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> --></td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default follow-ups")%></label>
					<div class="input-group" style="min-width:71px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "defFupMeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("defFupMeth");
							optionValue = makeDropDownList(mappedItems, sv.defFupMeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.defFupMeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.defFupMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.defFupMeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='defFupMeth' type='list' style="width: 190px;"
								<%if ((new Byte((sv.defFupMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.defFupMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.defFupMeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Code")%></label>
					<div class="input-group" style="min-width:71px;">
						<input name='schedCode' type='text'
							<%if ((sv.schedCode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.schedCode.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.schedCode.getLength()%>'
							maxLength='<%=sv.schedCode.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(schedCode)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.schedCode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.schedCode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.schedCode).getColor() == null ? "input_cell"
						: (sv.schedCode).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem Code")%></label>
					<div class="input-group" style="min-width:71px;">
						<input name='zszprmcd' type='text'
							<%if ((sv.zszprmcd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.zszprmcd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zszprmcd.getLength()%>'
							maxLength='<%=sv.zszprmcd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zszprmcd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zszprmcd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zszprmcd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zszprmcd).getColor() == null ? "input_cell"
						: (sv.zszprmcd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Statutory fund")%></label>
				<div class="input-group" style="min-width:71px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "statFund" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statFund");
							optionValue = makeDropDownList(mappedItems, sv.statFund.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.statFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.statFund).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='statFund' type='list' style="width: 170px;"
								<%if ((new Byte((sv.statFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.statFund).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.statFund).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
					<div class="input-group" style="min-width:71px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "statSect" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statSect");
							optionValue = makeDropDownList(mappedItems, sv.statSect.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.statSect.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.statSect).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.statSect).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='statSect' type='list' style="width: 200px;"
								<%if ((new Byte((sv.statSect).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.statSect).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.statSect).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-section")%></label>
				<div class="input-group" style="min-width:71px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "statSubSect" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statSubSect");
							optionValue = makeDropDownList(mappedItems, sv.statSubSect.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.statSubSect.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.statSubSect).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.statSubSect).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='statSubSect' type='list' style="width: 240px;"
								<%if ((new Byte((sv.statSubSect).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.statSubSect).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.statSubSect).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reporting codes")%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd01");
							optionValue = makeDropDownList(mappedItems, sv.reptcd01.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd01' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd02" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd02");
							optionValue = makeDropDownList(mappedItems, sv.reptcd02.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd02.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd02' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd03" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd03");
							optionValue = makeDropDownList(mappedItems, sv.reptcd03.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd03.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd03).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd03' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd04" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd04");
							optionValue = makeDropDownList(mappedItems, sv.reptcd04.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd04.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd04).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd04' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd05" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd05");
							optionValue = makeDropDownList(mappedItems, sv.reptcd05.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd05.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd05).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd05' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "reptcd06" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reptcd06");
							optionValue = makeDropDownList(mappedItems, sv.reptcd06.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reptcd06.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.reptcd06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.reptcd06).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='reptcd06' type='list' style="width: 170px;"
								<%if ((new Byte((sv.reptcd06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.reptcd06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.reptcd06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity calc")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "maturityCalcMeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("maturityCalcMeth");
							optionValue = makeDropDownList(mappedItems, sv.maturityCalcMeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.maturityCalcMeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.maturityCalcMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.maturityCalcMeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='maturityCalcMeth' type='list' style="width: 240px;"
								<%if ((new Byte((sv.maturityCalcMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.maturityCalcMeth).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.maturityCalcMeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Method")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "loanmeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("loanmeth");
							optionValue = makeDropDownList(mappedItems, sv.loanmeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.loanmeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.loanmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.loanmeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='loanmeth' type='list' style="width: 240px;"
								<%if ((new Byte((sv.loanmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.loanmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.loanmeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Sch Calc")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "zsbsmeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("zsbsmeth");
							optionValue = makeDropDownList(mappedItems, sv.zsbsmeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.zsbsmeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.zsbsmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.zsbsmeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='zsbsmeth' type='list' style="width: 240px;"
								<%if ((new Byte((sv.zsbsmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.zsbsmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.zsbsmeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Surrender calc")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "svMethod" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("svMethod");
							optionValue = makeDropDownList(mappedItems, sv.svMethod.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.svMethod.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.svMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.svMethod).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='svMethod' type='list' style="width: 240px;"
								<%if ((new Byte((sv.svMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.svMethod).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.svMethod).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Death calc")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "dcmeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("dcmeth");
							optionValue = makeDropDownList(mappedItems, sv.dcmeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.dcmeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.dcmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.dcmeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='dcmeth' type='list' style="width: 240px;"
								<%if ((new Byte((sv.dcmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.dcmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.dcmeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-up calc")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "pumeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("pumeth");
							optionValue = makeDropDownList(mappedItems, sv.pumeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.pumeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.pumeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.pumeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='pumeth' type='list' style="width: 240px;"
								<%if ((new Byte((sv.pumeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.pumeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.pumeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active" id="current1" class="tabcurrent"><a
						href="#content1" href="javascript:;" onClick="change_option(2,1);"
						data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Premiums")%></label></a>
					</li>
					<li id="current2" class="tab"><a href="#content2"
						href="javascript:;" onClick="change_option(2,2);"
						data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Commissions")%></label></a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="content1" class="tab-pane fade in active">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Calculation method")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "premmeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("premmeth");
											optionValue = makeDropDownList(mappedItems, sv.premmeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.premmeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.premmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.premmeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='premmeth' type='list' style="width: 245px;"
												<%if ((new Byte((sv.premmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.premmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.premmeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life method")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "jlPremMeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("jlPremMeth");
											optionValue = makeDropDownList(mappedItems, sv.jlPremMeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.jlPremMeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.jlPremMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.jlPremMeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='jlPremMeth' type='list' style="width: 245px;"
												<%if ((new Byte((sv.jlPremMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.jlPremMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.jlPremMeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<%
										if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("J/Life Allowed")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.jlifePresent).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>

										<input type='checkbox' name='jlifePresent' value='Y'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(jlifePresent)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((sv.jlifePresent).getColor() != null) {%>
											style='background-color: #FF0000;'
											<%}
				if ((sv.jlifePresent).toString().trim().equalsIgnoreCase("Y")) {%>
											checked
											<%}
				if ((sv.jlifePresent).getEnabled() == BaseScreenData.DISABLED
						|| fw.getVariables().isScreenProtected()) {%>
											disabled <%}%> class='UICheck'
											onclick="handleCheckBox('jlifePresent')" /> <input
											type='checkbox' name='jlifePresent' value='N'
											<%if (!(sv.jlifePresent).toString().trim().equalsIgnoreCase("Y")) {%>
											checked <%}%> style="visibility: hidden"
											onclick="handleCheckBox('jlifePresent')" />
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Guarantee period")%></label>
									<div style="width: 80px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.premGuarPeriod).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='premGuarPeriod' type='text'
											<%if ((sv.premGuarPeriod).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											value='<%=smartHF.getPicFormatted(qpsf, sv.premGuarPeriod)%>'
											<%valueThis = smartHF.getPicFormatted(qpsf, sv.premGuarPeriod);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=smartHF.getPicFormatted(qpsf, sv.premGuarPeriod)%>'
											<%}%> size='<%=sv.premGuarPeriod.getLength()%>'
											maxLength='<%=sv.premGuarPeriod.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(premGuarPeriod)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event);'
											onBlur='return doBlurNumber(event);'
											<%if ((new Byte((sv.premGuarPeriod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.premGuarPeriod).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.premGuarPeriod).getColor() == null ? "input_cell"
						: (sv.premGuarPeriod).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Re-calc. freq")%></label>
									<div style="width: 80px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.rtrnwfreq).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='rtrnwfreq' type='text'
											<%if ((sv.rtrnwfreq).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											value='<%=smartHF.getPicFormatted(qpsf, sv.rtrnwfreq)%>'
											<%valueThis = smartHF.getPicFormatted(qpsf, sv.rtrnwfreq);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=smartHF.getPicFormatted(qpsf, sv.rtrnwfreq)%>'
											<%}%> size='<%=sv.rtrnwfreq.getLength()%>'
											maxLength='<%=sv.rtrnwfreq.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(rtrnwfreq)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event);'
											onBlur='return doBlurNumber(event);'
											<%if ((new Byte((sv.rtrnwfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.rtrnwfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.rtrnwfreq).getColor() == null ? "input_cell"
						: (sv.rtrnwfreq).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Single Payment")%></label>
									<div>
										<input type='checkbox' name='singlePremInd' value='Y'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(singlePremInd)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((sv.singlePremInd).getColor() != null) {%>
											style='background-color: #FF0000;'
											<%}
			if ((sv.singlePremInd).toString().trim().equalsIgnoreCase("Y")) {%>
											checked
											<%}
			if ((sv.singlePremInd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
											disabled <%}%> class='UICheck'
											onclick="handleCheckBox('singlePremInd')" /> <input
											type='checkbox' name='singlePremInd' value=' '
											<%if (!(sv.singlePremInd).toString().trim().equalsIgnoreCase("Y")) {%>
											checked <%}%> style="visibility: hidden"
											onclick="handleCheckBox('singlePremInd')" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty calc")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "stampDutyMeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("stampDutyMeth");
											optionValue = makeDropDownList(mappedItems, sv.stampDutyMeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.stampDutyMeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.stampDutyMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.stampDutyMeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='stampDutyMeth' type='list'
												style="width: 245px;"
												<%if ((new Byte((sv.stampDutyMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.stampDutyMeth).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.stampDutyMeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty pay")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "sdtyPayMeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("sdtyPayMeth");
											optionValue = makeDropDownList(mappedItems, sv.sdtyPayMeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.sdtyPayMeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.sdtyPayMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.sdtyPayMeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='sdtyPayMeth' type='list' style="width: 245px;"
												<%if ((new Byte((sv.sdtyPayMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.sdtyPayMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.sdtyPayMeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%></label>
									<div>
										<input type='checkbox' name='zsredtrm' value='Y'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zsredtrm)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((sv.zsredtrm).getColor() != null) {%>
											style='background-color: #FF0000;'
											<%}
			if ((sv.zsredtrm).toString().trim().equalsIgnoreCase("Y")) {%>
											checked
											<%}
			if ((sv.zsredtrm).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
											disabled <%}%> class='UICheck'
											onclick="handleCheckBox('zsredtrm')" /> <input
											type='checkbox' name='zsredtrm' value=' '
											<%if (!(sv.zsredtrm).toString().trim().equalsIgnoreCase("Y")) {%>
											checked <%}%> style="visibility: hidden"
											onclick="handleCheckBox('zsredtrm')" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit billing")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "bbmeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("bbmeth");
											optionValue = makeDropDownList(mappedItems, sv.bbmeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.bbmeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.bbmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.bbmeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
											<%
												}
											%>

											<select name='bbmeth' type='list' style="width: 170px;"
												<%if ((new Byte((sv.bbmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.bbmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.bbmeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Non-forfeiture")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "nonForfeitMethod" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("nonForfeitMethod");
											optionValue = makeDropDownList(mappedItems, sv.nonForfeitMethod.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.nonForfeitMethod.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.nonForfeitMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.nonForfeitMethod).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
											<%
												}
											%>

											<select name='nonForfeitMethod' type='list'
												style="width: 170px;"
												<%if ((new Byte((sv.nonForfeitMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.nonForfeitMethod).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.nonForfeitMethod).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div><div class="col-md-4"></div>							
							<%	if ((new Byte((sv.lnkgind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
			  			 <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Linked TPD")%></label>
		                      			
		                      			<input type='checkbox' name='lnkgind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(lnkgind)' onKeyUp='return checkMaxLength(this)'    
								<%
								
								if((sv.lnkgind).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.lnkgind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if(sv.lnkgind.getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('lnkgind')"/>
								
								<input type='checkbox' name='lnkgind' value='N' 
								
								<% if(!(sv.lnkgind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('lnkgind')"/>
										                      			
		                      </div>
	         			</div>
				<%}%>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Anniversary Proc")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "anniversaryMethod" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("anniversaryMethod");
											optionValue = makeDropDownList(mappedItems, sv.anniversaryMethod.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.anniversaryMethod.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.anniversaryMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.anniversaryMethod).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='anniversaryMethod' type='list'
												style="width: 245px;"
												<%if ((new Byte((sv.anniversaryMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.anniversaryMethod).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.anniversaryMethod).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Part Surrender")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "partsurr" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("partsurr");
											optionValue = makeDropDownList(mappedItems, sv.partsurr.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.partsurr.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.partsurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.partsurr).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='partsurr' type='list' style="width: 245px;"
												<%if ((new Byte((sv.partsurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.partsurr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.partsurr).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Freq Alt Basis")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "xfreqAltBasis" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("xfreqAltBasis");
											optionValue = makeDropDownList(mappedItems, sv.xfreqAltBasis.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.xfreqAltBasis.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.xfreqAltBasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.xfreqAltBasis).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
											<%
												}
											%>

											<select name='xfreqAltBasis' type='list'
												style="width: 170px;"
												<%if ((new Byte((sv.xfreqAltBasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.xfreqAltBasis).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.xfreqAltBasis).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>														
						</div>
					</div>

					<div id="content2" class="tab-pane fade">
						<div class="row">
						<div class="col-md-3"></div>
							<!-- <div class="col-md-3 col-md-offset-3"> -->
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Regular")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Single")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Top-up")%></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" style="margin-top: 5px;">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Calc Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "basicCommMeth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("basicCommMeth");
											optionValue = makeDropDownList(mappedItems, sv.basicCommMeth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.basicCommMeth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.basicCommMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.basicCommMeth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='basicCommMeth' type='list'
												style="width: 190px;"
												<%if ((new Byte((sv.basicCommMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.basicCommMeth).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.basicCommMeth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "basscmth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("basscmth");
											optionValue = makeDropDownList(mappedItems, sv.basscmth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.basscmth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.basscmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.basscmth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='basscmth' type='list' style="width: 190px;"
												<%if ((new Byte((sv.basscmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.basscmth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.basscmth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "bastcmth" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("bastcmth");
											optionValue = makeDropDownList(mappedItems, sv.bastcmth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.bastcmth.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.bastcmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.bastcmth).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='bastcmth' type='list' style="width: 190px;"
												<%if ((new Byte((sv.bastcmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.bastcmth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.bastcmth).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Pay Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "bascpy" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("bascpy");
											optionValue = makeDropDownList(mappedItems, sv.bascpy.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.bascpy.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.bascpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.bascpy).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='bascpy' type='list' style="width: 190px;"
												<%if ((new Byte((sv.bascpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.bascpy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.bascpy).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "basscpy" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("basscpy");
											optionValue = makeDropDownList(mappedItems, sv.basscpy.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.basscpy.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.basscpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.basscpy).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='basscpy' type='list' style="width: 190px;"
												<%if ((new Byte((sv.basscpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.basscpy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.basscpy).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "bastcpy" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("bastcpy");
											optionValue = makeDropDownList(mappedItems, sv.bastcpy.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.bastcpy.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.bastcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.bastcpy).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='bastcpy' type='list' style="width: 190px;"
												<%if ((new Byte((sv.bastcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.bastcpy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.bastcpy).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("OR Calc Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorcmrg' type='text'
											<%if ((sv.zrorcmrg).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorcmrg.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorcmrg.getLength()%>'
											maxLength='<%=sv.zrorcmrg.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorcmrg)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorcmrg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorcmrg).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorcmrg).getColor() == null ? "input_cell"
						: (sv.zrorcmrg).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorcmsp' type='text'
											<%if ((sv.zrorcmsp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorcmsp.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorcmsp.getLength()%>'
											maxLength='<%=sv.zrorcmsp.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorcmsp)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorcmsp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorcmsp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorcmsp).getColor() == null ? "input_cell"
						: (sv.zrorcmsp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorcmtu' type='text'
											<%if ((sv.zrorcmtu).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorcmtu.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorcmtu.getLength()%>'
											maxLength='<%=sv.zrorcmtu.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorcmtu)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorcmtu).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorcmtu).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorcmtu).getColor() == null ? "input_cell"
						: (sv.zrorcmtu).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("OR Pay  Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorpmrg' type='text'
											<%if ((sv.zrorpmrg).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorpmrg.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorpmrg.getLength()%>'
											maxLength='<%=sv.zrorpmrg.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorpmrg)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorpmrg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorpmrg).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorpmrg).getColor() == null ? "input_cell"
						: (sv.zrorpmrg).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorpmsp' type='text'
											<%if ((sv.zrorpmsp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorpmsp.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorpmsp.getLength()%>'
											maxLength='<%=sv.zrorpmsp.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorpmsp)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorpmsp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorpmsp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorpmsp).getColor() == null ? "input_cell"
						: (sv.zrorpmsp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div style="width: 100px;">
										<input name='zrorpmtu' type='text'
											<%if ((sv.zrorpmtu).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%>
											<%formatValue = (sv.zrorpmtu.getFormData()).toString();%>
											value='<%= XSSFilter.escapeHtml(formatValue)%>'
											<%if (formatValue != null && formatValue.trim().length() > 0) {%>
											title='<%=formatValue%>' <%}%>
											size='<%=sv.zrorpmtu.getLength()%>'
											maxLength='<%=sv.zrorpmtu.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrorpmtu)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.zrorpmtu).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.zrorpmtu).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.zrorpmtu).getColor() == null ? "input_cell"
						: (sv.zrorpmtu).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Pay Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "srvcpy" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("srvcpy");
											optionValue = makeDropDownList(mappedItems, sv.srvcpy.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.srvcpy.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.srvcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.srvcpy).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='srvcpy' type='list' style="width: 190px;"
												<%if ((new Byte((sv.srvcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.srvcpy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.srvcpy).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Commission paid on Basic Premium Only")%></label>
									<div>
										<input type='checkbox' name='zrrcombas' value='Y'
											onFocus='doFocus(this)' onHelp='return fieldHelp(zrrcombas)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((sv.zrrcombas).getColor() != null) {%>
											style='background-color: #FF0000;'
											<%}
			if ((sv.zrrcombas).toString().trim().equalsIgnoreCase("Y")) {%>
											checked
											<%}
			if ((sv.zrrcombas).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
											disabled <%}%> class='UICheck'
											onclick="handleCheckBox('zrrcombas')" /> <input
											type='checkbox' name='zrrcombas' value=' '
											<%if (!(sv.zrrcombas).toString().trim().equalsIgnoreCase("Y")) {%>
											checked <%}%> style="visibility: hidden"
											onclick="handleCheckBox('zrrcombas')" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Renewal Pay Method")%></label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "rnwcpy" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("rnwcpy");
											optionValue = makeDropDownList(mappedItems, sv.rnwcpy.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.rnwcpy.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.rnwcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.rnwcpy).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='rnwcpy' type='list' style="width: 190px;"
												<%if ((new Byte((sv.rnwcpy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.rnwcpy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.rnwcpy).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;" class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premiums")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Commissions")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='riind' type='text'
							<%if ((sv.riind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.riind.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.riind.getLength()%>'
							maxLength='<%=sv.riind.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(riind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.riind).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.riind).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.riind).getColor() == null ? "input_cell"
						: (sv.riind).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

