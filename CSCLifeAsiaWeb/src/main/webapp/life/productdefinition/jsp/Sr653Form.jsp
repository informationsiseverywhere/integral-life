

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!--<%=((SMARTHTMLFormatter) AppVars.hf).getHTMLCBVar(19, 25)%>-->
<%
	String screenName = "SR653";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr653ScreenVars sv = (Sr653ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Filter ");
%>

<%
	{
		if (appVars.ind07.isOn()) {
			sv.mdblactf.setReverse(BaseScreenData.REVERSED);
			sv.mdblactf.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.mdblactf.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zdocno.setReverse(BaseScreenData.REVERSED);
			sv.zdocno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zdocno.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.paidby.setReverse(BaseScreenData.REVERSED);
			sv.paidby.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.paidby.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.exmcode.setReverse(BaseScreenData.REVERSED);
			sv.exmcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.exmcode.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<%
	{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "S");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action/Desc");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Invoice Number");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid To/Entity/Entity Description");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "LF");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "JL");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "ME Date");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fee Amount");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Pay Date");
%>

<%
	appVars.rollup(new int[]{93});
%>


<%
	/* This block of jsp code is to calculate the variable width of the table at runtime.*/
	int[] tblColumnWidth = new int[13];
	int totalTblWidth = 0;
	int calculatedValue = 0;

	if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.mdblact.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
	} else {
		calculatedValue = (sv.mdblact.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[0] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.shortdesc.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
	} else {
		calculatedValue = (sv.shortdesc.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[1] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.chdrnum.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
	} else {
		calculatedValue = (sv.chdrnum.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[2] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.invref.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
	} else {
		calculatedValue = (sv.invref.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[3] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.payto.getFormData()).length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 8;
	} else {
		calculatedValue = (sv.payto.getFormData()).length() * 8;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[4] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.ent.getFormData()).length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 12;
	} else {
		calculatedValue = (sv.ent.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[5] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.descript.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 12;
	} else {
		calculatedValue = (sv.descript.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[6] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.life.getFormData()).length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 12;
	} else {
		calculatedValue = (sv.life.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[7] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.jlife.getFormData()).length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length()) * 12;
	} else {
		calculatedValue = (sv.jlife.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[8] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header10").length() >= (sv.zmedtyp.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header10").length()) * 12;
	} else {
		calculatedValue = (sv.zmedtyp.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[9] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header11").length() >= (sv.effdateDisp.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header11").length()) * 12;
	} else {
		calculatedValue = (sv.effdateDisp.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[10] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header12").length() >= (sv.premium.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header12").length()) * 12;
	} else {
		calculatedValue = (sv.premium.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[11] = calculatedValue;

	if (resourceBundleHandler.gettingValueFromBundle("Header13").length() >= (sv.paydteDisp.getFormData())
			.length()) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header13").length()) * 12;
	} else {
		calculatedValue = (sv.paydteDisp.getFormData()).length() * 12;
	}
	totalTblWidth += calculatedValue;
	tblColumnWidth[12] = calculatedValue;
%>
<%
	GeneralTable sfl = fw.getTable("sr653screensfl");
	int height;
	if (sfl.count() * 27 > 210) {
		height = 210;
	} else {
		height = sfl.count() * 27;
	}
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr653' width='100%'>
							<thead>
								<tr class='info'>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Action")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></center></th>

									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Invoice Number")%></center></th>

									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Paid To")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Entity")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Entity Description")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("LF")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("JL")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Medicine Type")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("ME Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fee Amount")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Pay Date")%></center></th>
								</tr>
							</thead>
							<tbody>
							<%
								Sr653screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sr653screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<div style='display: none; visiblity: hidden;'>
									<input type='text' maxLength='<%=sv.select.getLength()%>'
										value='<%=sv.select.getFormData()%>'
										size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr653screensfl.select)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr653screensfl" + "." + "select" + "_R" + count%>'
										id='<%="sr653screensfl" + "." + "select" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.select.getLength() * 12%>px;">
								</div>

								<td class="tableDataTag tableDataTagFixed"
									style="width:<%=tblColumnWidth[0]%>px;" align="left"><a
									href="javascript:;" class='tableLink'
									onClick='document.getElementById("<%="sr653screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.mdblact.getFormData()%></span></a>
								</td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"
									align="left"><%=sv.shortdesc.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"
									align="left"><%=sv.chdrnum.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[3]%>px;"
									align="left"><%=sv.invref.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[4]%>px;"
									align="left"><%=sv.payto.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[5]%>px;"
									align="left"><%=sv.ent.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[6]%>px;"
									align="left"><%=sv.descript.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[7]%>px;"
									align="left"><%=sv.life.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[8]%>px;"
									align="left"><%=sv.jlife.getFormData()%></td>

								<td class="tableDataTag" style="width:<%=tblColumnWidth[9]%>px;"
									align="left"><%=sv.zmedtyp.getFormData()%></td>

								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[10]%>px;" align="left"><%=sv.effdateDisp.getFormData()%>
								</td>

								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[11]%>px;" align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.premium).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.premium,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 		if (!sv.premium.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>
								</td>

								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[12]%>px;" align="left"><%=sv.paydteDisp.getFormData()%>
								</td>

							</tr>
							<%
								count = count + 1;
									Sr653screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
							

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Filter")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Action")%></label>
					<div class="input-group" style="width: 100px;">
						<input name='mdblactf' type='text'
						    id='mdblactf'
							value='<%=sv.mdblactf.getFormData()%>'
							maxLength='<%=sv.mdblactf.getLength()%>'
							size='<%=sv.mdblactf.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(mdblactf)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.mdblactf).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.mdblactf).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('mdblactf')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>
						class = '
						<%=(sv.mdblactf).getColor() == null
						? "input_cell"
						: (sv.mdblactf).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('mdblactf')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Invoice Number")%></label>
					<div class="input-group" style="width: 200px;">
						<input name='zdocno' type='text'
							<%formatValue = (sv.zdocno.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.zdocno.getLength()%>'
							maxLength='<%=sv.zdocno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zdocno)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zdocno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zdocno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zdocno).getColor() == null
						? "input_cell"
						: (sv.zdocno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<div class="input-group" style="width: 150px;">
						<input name='chdrsel' type='text'
						    id='chdrsel'
							value='<%=sv.chdrsel.getFormData()%>'
							maxLength='<%=sv.chdrsel.getLength()%>'
							size='<%=sv.chdrsel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(chdrsel)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.chdrsel).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>
						class = '
						<%=(sv.chdrsel).getColor() == null
						? "input_cell"
						: (sv.chdrsel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
						%>
					</div>
				</div>
			</div>
		
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To")%></label>
					<div class="input-group" style="width: 100px;">
						<input name='paidby' type='text'
						    id='paidby'
							value='<%=sv.paidby.getFormData()%>'
							maxLength='<%=sv.paidby.getLength()%>'
							size='<%=sv.paidby.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(paidby)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.paidby).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.paidby).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('paidby')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>
						class = '
						<%=(sv.paidby).getColor() == null
						? "input_cell"
						: (sv.paidby).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('paidby')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Entity")%></label>
					<div class="input-group" style="width: 200px;">
						<input name='exmcode' type='text'
						   id='exmcode'
							value='<%=sv.exmcode.getFormData()%>'
							maxLength='<%=sv.exmcode.getLength()%>'
							size='<%=sv.exmcode.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(exmcode)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.exmcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.exmcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('exmcode')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>
						class = '
						<%=(sv.exmcode).getColor() == null
						? "input_cell"
						: (sv.exmcode).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onclick="doFocus(document.getElementById('exmcode')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if (!((sv.optdsc01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if (!((sv.optdsc02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if (!((sv.optdsc03.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc03.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc03.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<div>
							<%
								if (!((sv.optdsc04.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc04.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.optdsc04.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<script>
	$(document).ready(function() {
		$('#dataTables-sr653').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		
	});
</script>


<%@ include file="/POLACommon2NEW.jsp"%>
