<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5h7";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>


<%
	Sd5h7ScreenVars sv = (Sd5h7ScreenVars) fw.getVariables();
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Calculate Frequency");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Capitalize Frequency");%>
<%
	{

		 if (appVars.ind02.isOn()) {
			sv.intrate.setReverse(BaseScreenData.REVERSED);
			sv.intrate.setColor(BaseScreenData.RED);
		} 
		
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText1)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText2)%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText3)%></label>
				<table><tr><td>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%></td><td style="padding-left:1px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%></td></tr></table>
					
				</div>
			</div>
		</div>
		 <div class="row">
		 <div class="col-md-6">
        			<div class="form-group">
        				<label><%=generatedText4%></label>
        				<table><tr><td>     				
        				<%if ((new Byte((sv.itmfrmDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>	
							<%					
							if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
					  	</td><td>
					  	<!-- //to -->				  	
					  	<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						&nbsp;
						<label><%=generatedText5%></label>
						&nbsp;
						<%}%>	  	
					  	<!-- end to -->
					  	</td><td>
					  	
						<%if ((new Byte((sv.itmtoDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<%					
							if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%></td></tr>
					  </table>
        			</div>
        		</div>
		</div>  
		
		<div class="row">
		
	        <div class="col-md-4"> 
				    		<div class="form-group"> 
	        	<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
			<div class="input-group">

				<%
					qpsf = fw.getFieldXMLDef((sv.intrate).getFieldName());
					qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS5);
				%>

				<input name='intrate' type='text'
					<%if ((sv.intrate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.intrate)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.intrate);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.intrate)%>' <%}%>
					size='<%=sv.intrate.getLength()%>'
					maxLength='<%=sv.intrate.getLength()%>' onFocus='doFocus(this)'
					onHelp='return fieldHelp(intrate)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.intrate).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.intrate).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%>
					class=' <%=(sv.intrate).getColor() == null ? "input_cell"
						: (sv.intrate).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>>

			</div>
		</div>
		
			</div>
		</div>
		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%> <%=resourceBundleHandler.gettingValueFromBundle("Interest Calculate Frequency")%>
						<%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.intcalfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "intcalfreq" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("intcalfreq");
								optionValue = makeDropDownList(mappedItems, sv.intcalfreq.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.intcalfreq.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.intcalfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.intcalfreq).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 180px;">
							<%
								}
							%>

							<select name='intcalfreq' type='list' style="width: 180px;"
								<%if ((new Byte((sv.intcalfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.intcalfreq).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.intcalfreq).getColor())) {
							%>
						</div>
						<!-- MIBT-217 -->
						<%
							}
						%>
						<%
							formatValue = null;
									longValue = null;
						%>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			

			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%> <%=resourceBundleHandler.gettingValueFromBundle("Interest Capitalize Frequency")%>
						<%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.intcapfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "intcapfreq" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("intcapfreq");
								optionValue = makeDropDownList(mappedItems, sv.intcapfreq.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.intcapfreq.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.intcapfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.intcapfreq).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 180px;">
							<%
								}
							%>

							<select name='intcapfreq' type='list' style="width: 180px;"
								<%if ((new Byte((sv.intcapfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.intcapfreq).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.intcapfreq).getColor())) {
							%>
						</div>
						<!-- MIBT-217 -->
						<%
							}
						%>
						<%
							formatValue = null;
									longValue = null;
						%>
						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		
		
		
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<script>
$(document).ready(function(){
$('#functiontype').css("width","70px");
});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

