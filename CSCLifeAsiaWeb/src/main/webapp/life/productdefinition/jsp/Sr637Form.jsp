

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR637";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr637ScreenVars sv = (Sr637ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bank Code ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Account  ");
%>
<!-- ILIFE-1016 start kpalani6  make disable field upon enquiry-->
<%
	{
		if (appVars.ind01.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
			sv.bankkey.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
			sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- ILIFE-1016 end kpalani6  make disable field upon enquiry-->


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
					<table>
						<tr class="input-group three-controller">
							<td class="input-group"><input name='bankkey' type='text'  id="bankkey"
								value='<%=sv.bankkey.getFormData()%>'
								maxLength='<%=sv.bankkey.getLength()%>'
								size='<%=sv.bankkey.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bankkey)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.bankkey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	if (browerVersion.equals(Chrome)) {
 %> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %> <%
 	if ((browerVersion.equals(IE11)) || (browerVersion.equals(IE8))) {
 %> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %> <%
 	} else {
 %> class = ' <%=(sv.bankkey).getColor() == null ? "input_cell"
						: (sv.bankkey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %></td>
							<td>
								<%
									if (!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bankdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bankdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
							
							<td>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>


							<td>
								<%
									if (!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.branchdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.branchdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <%
 	if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
 %>
								<div style="margin-left: -2px"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	}
 %> <%
 	if (browerVersion.equals(IE8)) {
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	}
 %>

							</td>
						</tr>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Account")%></label>
					<table>
						<tr class="input-group three-controller">
							<td class="input-group"><input name='bankacckey' type='text'  id="bankacckey"
								value='<%=sv.bankacckey.getFormData()%>'
								maxLength='<%=sv.bankacckey.getLength()%>'
								size='<%=sv.bankacckey.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bankacckey)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.bankacckey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	if (browerVersion.equals(Chrome)) {
 %> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankacckey'));changeF4Image(this); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %> <%
 	if ((browerVersion.equals(IE11)) || (browerVersion.equals(IE8))) {
 %> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankacckey'));changeF4Image(this); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %> <%
 	} else {
 %> class = ' <%=(sv.bankacckey).getColor() == null ? "input_cell"
						: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								> <span class="input-group-btn">
									<button class="btn btn-info"
										style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
										type="button"
										onclick="doFocus(document.getElementById('bankacckey'));changeF4Image(this); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span> <%
 	}
 %></td>
							<td>
								<%
									if (!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <%
 	if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
 %>
								<div style="margin-left: -2px"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	}
 %> <%
 	if (browerVersion.equals(IE8)) {
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	}
 %> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

