

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5646";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5646ScreenVars sv = (S5646ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Dates effective ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age Limit ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From Age");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To Age");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Factor");
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div class="input-group">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

</td><td>







						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td style="width:90px;">
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td style="width:90px;">
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Limit")%></label>
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.agelimit).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='agelimit' type='text'
							<%if ((sv.agelimit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.agelimit)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.agelimit);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.agelimit)%>' <%}%>
							size='<%=sv.agelimit.getLength()%>'
							maxLength='<%=sv.agelimit.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(agelimit)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.agelimit).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.agelimit).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.agelimit).getColor() == null ? "input_cell"
						: (sv.agelimit).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("From Age")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("To Age")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm01' type='text'
							<%if ((sv.ageIssageFrm01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm01)%>'
							<%}%> size='<%=sv.ageIssageFrm01.getLength()%>'
							maxLength='<%=sv.ageIssageFrm01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm01).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo01' type='text'
							<%if ((sv.ageIssageTo01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo01)%>'
							<%}%> size='<%=sv.ageIssageTo01.getLength()%>'
							maxLength='<%=sv.ageIssageTo01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo01).getColor() == null ? "input_cell"
						: (sv.ageIssageTo01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa01' type='text'
							<%if ((sv.factorsa01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa01)%>' <%}%>
							size='<%=sv.factorsa01.getLength()%>'
							maxLength='<%=sv.factorsa01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa01)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa01).getColor() == null ? "input_cell"
						: (sv.factorsa01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm02' type='text'
							<%if ((sv.ageIssageFrm02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm02)%>'
							<%}%> size='<%=sv.ageIssageFrm02.getLength()%>'
							maxLength='<%=sv.ageIssageFrm02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm02).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo02' type='text'
							<%if ((sv.ageIssageTo02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo02)%>'
							<%}%> size='<%=sv.ageIssageTo02.getLength()%>'
							maxLength='<%=sv.ageIssageTo02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo02).getColor() == null ? "input_cell"
						: (sv.ageIssageTo02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa02' type='text'
							<%if ((sv.factorsa02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa02)%>' <%}%>
							size='<%=sv.factorsa02.getLength()%>'
							maxLength='<%=sv.factorsa02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa02)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa02).getColor() == null ? "input_cell"
						: (sv.factorsa02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm03' type='text'
							<%if ((sv.ageIssageFrm03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm03)%>'
							<%}%> size='<%=sv.ageIssageFrm03.getLength()%>'
							maxLength='<%=sv.ageIssageFrm03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm03).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo03' type='text'
							<%if ((sv.ageIssageTo03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo03)%>'
							<%}%> size='<%=sv.ageIssageTo03.getLength()%>'
							maxLength='<%=sv.ageIssageTo03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo03).getColor() == null ? "input_cell"
						: (sv.ageIssageTo03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa03' type='text'
							<%if ((sv.factorsa03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa03)%>' <%}%>
							size='<%=sv.factorsa03.getLength()%>'
							maxLength='<%=sv.factorsa03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa03)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa03).getColor() == null ? "input_cell"
						: (sv.factorsa03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm04' type='text'
							<%if ((sv.ageIssageFrm04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm04)%>'
							<%}%> size='<%=sv.ageIssageFrm04.getLength()%>'
							maxLength='<%=sv.ageIssageFrm04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm04).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo04' type='text'
							<%if ((sv.ageIssageTo04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo04)%>'
							<%}%> size='<%=sv.ageIssageTo04.getLength()%>'
							maxLength='<%=sv.ageIssageTo04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo04).getColor() == null ? "input_cell"
						: (sv.ageIssageTo04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa04' type='text'
							<%if ((sv.factorsa04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa04)%>' <%}%>
							size='<%=sv.factorsa04.getLength()%>'
							maxLength='<%=sv.factorsa04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa04)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa04).getColor() == null ? "input_cell"
						: (sv.factorsa04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm05' type='text'
							<%if ((sv.ageIssageFrm05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm05)%>'
							<%}%> size='<%=sv.ageIssageFrm05.getLength()%>'
							maxLength='<%=sv.ageIssageFrm05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm05).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo05' type='text'
							<%if ((sv.ageIssageTo05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo05)%>'
							<%}%> size='<%=sv.ageIssageTo05.getLength()%>'
							maxLength='<%=sv.ageIssageTo05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo05).getColor() == null ? "input_cell"
						: (sv.ageIssageTo05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa05' type='text'
							<%if ((sv.factorsa05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa05)%>' <%}%>
							size='<%=sv.factorsa05.getLength()%>'
							maxLength='<%=sv.factorsa05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa05)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa05).getColor() == null ? "input_cell"
						: (sv.factorsa05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm06' type='text'
							<%if ((sv.ageIssageFrm06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm06)%>'
							<%}%> size='<%=sv.ageIssageFrm06.getLength()%>'
							maxLength='<%=sv.ageIssageFrm06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm06).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo06' type='text'
							<%if ((sv.ageIssageTo06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo06)%>'
							<%}%> size='<%=sv.ageIssageTo06.getLength()%>'
							maxLength='<%=sv.ageIssageTo06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo06).getColor() == null ? "input_cell"
						: (sv.ageIssageTo06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa06' type='text'
							<%if ((sv.factorsa06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa06)%>' <%}%>
							size='<%=sv.factorsa06.getLength()%>'
							maxLength='<%=sv.factorsa06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa06)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa06).getColor() == null ? "input_cell"
						: (sv.factorsa06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm07' type='text'
							<%if ((sv.ageIssageFrm07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm07)%>'
							<%}%> size='<%=sv.ageIssageFrm07.getLength()%>'
							maxLength='<%=sv.ageIssageFrm07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm07).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo07' type='text'
							<%if ((sv.ageIssageTo07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo07)%>'
							<%}%> size='<%=sv.ageIssageTo07.getLength()%>'
							maxLength='<%=sv.ageIssageTo07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo07).getColor() == null ? "input_cell"
						: (sv.ageIssageTo07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa07' type='text'
							<%if ((sv.factorsa07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa07)%>' <%}%>
							size='<%=sv.factorsa07.getLength()%>'
							maxLength='<%=sv.factorsa07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa07)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa07).getColor() == null ? "input_cell"
						: (sv.factorsa07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm08' type='text'
							<%if ((sv.ageIssageFrm08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm08)%>'
							<%}%> size='<%=sv.ageIssageFrm08.getLength()%>'
							maxLength='<%=sv.ageIssageFrm08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm08).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo08' type='text'
							<%if ((sv.ageIssageTo08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo08)%>'
							<%}%> size='<%=sv.ageIssageTo08.getLength()%>'
							maxLength='<%=sv.ageIssageTo08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo08).getColor() == null ? "input_cell"
						: (sv.ageIssageTo08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa08' type='text'
							<%if ((sv.factorsa08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa08)%>' <%}%>
							size='<%=sv.factorsa08.getLength()%>'
							maxLength='<%=sv.factorsa08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa08)'
							 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa08).getColor() == null ? "input_cell"
						: (sv.factorsa08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm09' type='text'
							<%if ((sv.ageIssageFrm09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm09)%>'
							<%}%> size='<%=sv.ageIssageFrm09.getLength()%>'
							maxLength='<%=sv.ageIssageFrm09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm09).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo09' type='text'
							<%if ((sv.ageIssageTo09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo09)%>'
							<%}%> size='<%=sv.ageIssageTo09.getLength()%>'
							maxLength='<%=sv.ageIssageTo09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo09).getColor() == null ? "input_cell"
						: (sv.ageIssageTo09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa09' type='text'
							<%if ((sv.factorsa09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa09)%>' <%}%>
							size='<%=sv.factorsa09.getLength()%>'
							maxLength='<%=sv.factorsa09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa09)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa09).getColor() == null ? "input_cell"
						: (sv.factorsa09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm10' type='text'
							<%if ((sv.ageIssageFrm10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm10)%>'
							<%}%> size='<%=sv.ageIssageFrm10.getLength()%>'
							maxLength='<%=sv.ageIssageFrm10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm10).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo10' type='text'
							<%if ((sv.ageIssageTo10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo10)%>'
							<%}%> size='<%=sv.ageIssageTo10.getLength()%>'
							maxLength='<%=sv.ageIssageTo10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo10).getColor() == null ? "input_cell"
						: (sv.ageIssageTo10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa10' type='text'
							<%if ((sv.factorsa10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa10)%>' <%}%>
							size='<%=sv.factorsa10.getLength()%>'
							maxLength='<%=sv.factorsa10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa10)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa10).getColor() == null ? "input_cell"
						: (sv.factorsa10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm11).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm11' type='text'
							<%if ((sv.ageIssageFrm11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm11)%>'
							<%}%> size='<%=sv.ageIssageFrm11.getLength()%>'
							maxLength='<%=sv.ageIssageFrm11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm11).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo11).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo11' type='text'
							<%if ((sv.ageIssageTo11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo11)%>'
							<%}%> size='<%=sv.ageIssageTo11.getLength()%>'
							maxLength='<%=sv.ageIssageTo11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo11).getColor() == null ? "input_cell"
						: (sv.ageIssageTo11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa11).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa11' type='text'
							<%if ((sv.factorsa11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa11)%>' <%}%>
							size='<%=sv.factorsa11.getLength()%>'
							maxLength='<%=sv.factorsa11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa11)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa11).getColor() == null ? "input_cell"
						: (sv.factorsa11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageFrm12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageFrm12' type='text'
							<%if ((sv.ageIssageFrm12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageFrm12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageFrm12)%>'
							<%}%> size='<%=sv.ageIssageFrm12.getLength()%>'
							maxLength='<%=sv.ageIssageFrm12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageFrm12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageFrm12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageFrm12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageFrm12).getColor() == null ? "input_cell"
						: (sv.ageIssageFrm12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.ageIssageTo12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ageIssageTo12' type='text'
							<%if ((sv.ageIssageTo12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo12)%>'
							<%}%> size='<%=sv.ageIssageTo12.getLength()%>'
							maxLength='<%=sv.ageIssageTo12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ageIssageTo12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ageIssageTo12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ageIssageTo12).getColor() == null ? "input_cell"
						: (sv.ageIssageTo12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.factorsa12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='factorsa12' type='text'
							<%if ((sv.factorsa12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.factorsa12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.factorsa12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.factorsa12)%>' <%}%>
							size='<%=sv.factorsa12.getLength()%>'
							maxLength='<%=sv.factorsa12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(factorsa12)'
						 
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							 
							<%if ((new Byte((sv.factorsa12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.factorsa12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.factorsa12).getColor() == null ? "input_cell"
						: (sv.factorsa12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

