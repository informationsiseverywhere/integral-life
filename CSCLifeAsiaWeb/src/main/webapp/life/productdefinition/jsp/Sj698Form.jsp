<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SJ698";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sj698ScreenVars sv = (Sj698ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sj698screenWritten.gt(0)) {
%>
<%
	Sj698screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Dates Effective     ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Frequency");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rebate %");
%>
<%
	sv.freqcy01.setClassString("");
%>
<%
	sv.zlfact01.setClassString("");
%>
<%
	sv.zlfact01.appendClassString("num_fld");
		sv.zlfact01.appendClassString("input_txt");
		sv.zlfact01.appendClassString("highlight");
%>
<%
	sv.freqcy02.setClassString("");
%>
<%
	sv.zlfact02.setClassString("");
%>
<%
	sv.zlfact02.appendClassString("num_fld");
		sv.zlfact02.appendClassString("input_txt");
		sv.zlfact02.appendClassString("highlight");
%>
<%
	sv.freqcy03.setClassString("");
%>
<%
	sv.zlfact03.setClassString("");
%>
<%
	sv.zlfact03.appendClassString("num_fld");
		sv.zlfact03.appendClassString("input_txt");
		sv.zlfact03.appendClassString("highlight");
%>
<%
	sv.freqcy04.setClassString("");
%>
<%
	sv.zlfact04.setClassString("");
%>
<%
	sv.zlfact04.appendClassString("num_fld");
		sv.zlfact04.appendClassString("input_txt");
		sv.zlfact04.appendClassString("highlight");
%>
<%
	sv.freqcy05.setClassString("");
%>
<%
	sv.zlfact05.setClassString("");
%>
<%
	sv.zlfact05.appendClassString("num_fld");
		sv.zlfact05.appendClassString("input_txt");
		sv.zlfact05.appendClassString("highlight");
%>
<%
	sv.freqcy06.setClassString("");
%>
<%
	sv.zlfact06.setClassString("");
%>
<%
	sv.zlfact06.appendClassString("num_fld");
		sv.zlfact06.appendClassString("input_txt");
		sv.zlfact06.appendClassString("highlight");
%>
<%
	sv.freqcy07.setClassString("");
%>
<%
	sv.zlfact07.setClassString("");
%>
<%
	sv.zlfact07.appendClassString("num_fld");
		sv.zlfact07.appendClassString("input_txt");
		sv.zlfact07.appendClassString("highlight");
%>
<%
	sv.freqcy08.setClassString("");
%>
<%
	sv.zlfact08.setClassString("");
%>
<%
	sv.zlfact08.appendClassString("num_fld");
		sv.zlfact08.appendClassString("input_txt");
		sv.zlfact08.appendClassString("highlight");
%>
<%
	sv.freqcy09.setClassString("");
%>
<%
	sv.zlfact09.setClassString("");
%>
<%
	sv.zlfact09.appendClassString("num_fld");
		sv.zlfact09.appendClassString("input_txt");
		sv.zlfact09.appendClassString("highlight");
%>
<%
	sv.freqcy10.setClassString("");
%>
<%
	sv.zlfact10.setClassString("");
%>
<%
	sv.zlfact10.appendClassString("num_fld");
		sv.zlfact10.appendClassString("input_txt");
		sv.zlfact10.appendClassString("highlight");
%>
<%
	sv.freqcy11.setClassString("");
%>
<%
	sv.zlfact11.setClassString("");
%>
<%
	sv.zlfact11.appendClassString("num_fld");
		sv.zlfact11.appendClassString("input_txt");
		sv.zlfact11.appendClassString("highlight");
%>
<%
	sv.freqcy12.setClassString("");
%>
<%
	sv.zlfact12.setClassString("");
%>
<%
	sv.zlfact12.appendClassString("num_fld");
		sv.zlfact12.appendClassString("input_txt");
		sv.zlfact12.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.freqcy01.setReverse(BaseScreenData.REVERSED);
				sv.freqcy01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.freqcy01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.freqcy02.setReverse(BaseScreenData.REVERSED);
				sv.freqcy02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.freqcy02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.freqcy03.setReverse(BaseScreenData.REVERSED);
				sv.freqcy03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.freqcy03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.freqcy05.setReverse(BaseScreenData.REVERSED);
				sv.freqcy05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.freqcy05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.freqcy04.setReverse(BaseScreenData.REVERSED);
				sv.freqcy04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.freqcy04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.freqcy07.setReverse(BaseScreenData.REVERSED);
				sv.freqcy07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.freqcy07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.freqcy06.setReverse(BaseScreenData.REVERSED);
				sv.freqcy06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.freqcy06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.freqcy08.setReverse(BaseScreenData.REVERSED);
				sv.freqcy08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.freqcy08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.freqcy09.setReverse(BaseScreenData.REVERSED);
				sv.freqcy09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.freqcy09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.freqcy10.setReverse(BaseScreenData.REVERSED);
				sv.freqcy10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.freqcy10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.freqcy11.setReverse(BaseScreenData.REVERSED);
				sv.freqcy11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.freqcy11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.freqcy12.setReverse(BaseScreenData.REVERSED);
				sv.freqcy12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.freqcy12.setHighLight(BaseScreenData.BOLD);
			}
		}
%>
<!-- ILIFE-2569 Life Cross Browser - Sprint 2 D2 : Task 5  -->

<!-- <style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}

.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}


</style> -->
<script>
	$("#demo")
			.append(
					'<span class="glyphicon glyphicon-search" aria-hidden="true"></span>');
</script>
<!-- ILIFE-2569 Life Cross Browser - Sprint 2 D2 : Task 5 -->

<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>


					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>

						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td><label style="font-weight: bolder; font-size: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rebate %")%></label>
					
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy11)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy11)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy11')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
                                         if ((new Byte((sv.freqcy12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.freqcy12)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="min-width: 71px;max-width:90px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.freqcy12)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('freqcy12')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					 <div class="input-group" style="min-width: 71px;max-width:90px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlfact12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					</div></div></div>
					
		
	</div>
</div>


<%
	}
%>
<%@ include file="/POLACommon2NEW.jsp"%>


<!-- ILIFE-2569 Life Cross Browser - Sprint 2 D2: Task 5  -->
<style>
div[id*='freqcy'] {
	padding-right: 3px !important
}

div[id*='zlfact'] {
	padding-right: 3px !important
}
</style>
<!-- ILIFE-2569 Life Cross Browser - Sprint 2 D2: Task 5  -->
