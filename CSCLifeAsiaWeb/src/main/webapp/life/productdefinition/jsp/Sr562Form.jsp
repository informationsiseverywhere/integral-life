<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR562";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%Sr562ScreenVars sv = (Sr562ScreenVars) fw.getVariables();%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Paying Register  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status   ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate                           ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interim Period                          ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Funds Source (Bank or Self-financed)    ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(B/S)");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Installment Option                      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rest Indicator                          ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Reference                          ");%>
<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Duration            ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Calculation Type");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.prat.setReverse(BaseScreenData.REVERSED);
			sv.prat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.prat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.prmdetails.setReverse(BaseScreenData.REVERSED);
			sv.prmdetails.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.prmdetails.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.mlinsopt.setReverse(BaseScreenData.REVERSED);
			sv.mlinsopt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.mlinsopt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mlresind.setReverse(BaseScreenData.REVERSED);
			sv.mlresind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mlresind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.coverc.setReverse(BaseScreenData.REVERSED);
			sv.coverc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.coverc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.mbnkref.setReverse(BaseScreenData.REVERSED);
			sv.mbnkref.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.mbnkref.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.mlresindvpms.setReverse(BaseScreenData.REVERSED);
			sv.mlresindvpms.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.mlresindvpms.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.mlresindvpms.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.loandur.setReverse(BaseScreenData.REVERSED);
			sv.loandur.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.loandur.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.loandur.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.intcaltype.setReverse(BaseScreenData.REVERSED);
			sv.intcaltype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.intcaltype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.intcaltype.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>



<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table >
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
											"blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							 	longValue = null;
							 	formatValue = null;
							 %>

							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell"%>' style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								 <%
								 	longValue = null;
								 	formatValue = null;
								 %>
							</td>

							<td>
								<%
									if (!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.descrip.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
											"blank_cell" : "output_cell"%>' style="margin-left: 1px;max-width: 300px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
									 <%
									 	longValue = null;
									 	formatValue = null;
									 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Paying Register")%></label>
					<div style="width:60px">
						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.cnRiskStat.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnRiskStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnRiskStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:50px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td>

						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell"%>' style="margin-left: 1px;max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.cnPremStat.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnPremStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnPremStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:50px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					<td>

						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.pstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.pstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell"%>' style="margin-left: 1px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div style="width:60px">
						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<hr>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.prat).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
						%>

						<input name='prat' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.prat)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.prat);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.prat)%>' <%}%>
							size='<%=sv.prat.getLength()%>'
							maxLength='<%=sv.prat.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(prat)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.prat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.prat).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.prat).getColor() == null ? "input_cell"
						: (sv.prat).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interim Period")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.coverc).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
						%>

						<input name='coverc' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.coverc)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.coverc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.coverc)%>' <%}%>
							size='<%=sv.coverc.getLength()%>'
							maxLength='<%=sv.coverc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(coverc)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.coverc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.coverc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.coverc).getColor() == null ? "input_cell"
						: (sv.coverc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Funds Source (Bank or Self-financed)")%></label>
					<div>
						<%
							if ((new Byte((sv.prmdetails).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.prmdetails.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Bank");
								}
								if (((sv.prmdetails.getFormData()).toString()).trim().equalsIgnoreCase("S")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Self Financed");
								}
						%>

						<%
							if ((new Byte((sv.prmdetails).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.prmdetails).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 142px;">
							<%
								}
							%>

							<select name='prmdetails' style="width: 140px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(prmdetails)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.prmdetails).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.prmdetails).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="B"
									<%if (((sv.prmdetails.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Bank")%></option>
								<option value="S"
									<%if (((sv.prmdetails.getFormData()).toString()).trim().equalsIgnoreCase("S")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Self Financed")%></option>


							</select>
							<%
								if ("red".equals((sv.prmdetails).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Installment Option")%></label>
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mlinsopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mlinsopt");
	optionValue = makeDropDownList( mappedItems , sv.mlinsopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mlinsopt.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mlinsopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mlinsopt).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:152px;"> 
<%
} 
%>

<select name='mlinsopt' type='list' style="width:150px;"
<% 
	if((new Byte((sv.mlinsopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mlinsopt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mlinsopt).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
				</div>
			</div>
			<div class="col-md-4">
			<div class="form-group">
				<%
					if (sv.mrtaFlag.compareTo("N") != 0) {
				%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("MRTA Reducing Frequency")%></label>
				
						<%  fieldItem=(Map)(appVars.loadF4(baseModel, "mlresindvpms")); 
	mappedItems = (Map) fieldItem.get("mlresindvpms");
	optionValue = makeDropDownList( mappedItems , sv.mlresindvpms.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mlresindvpms.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.mlresindvpms).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mlresindvpms).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:222px;"> 
<%
} 
%>

<select name='mlresindvpms' type='list' style="width:220px;"
<% 
	if((new Byte((sv.mlresindvpms).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mlresindvpms).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mlresindvpms).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%}else{%>


<label>
<%=resourceBundleHandler.gettingValueFromBundle("Rest Indicator")%>
</label>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mlresind"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mlresind");
	optionValue = makeDropDownList( mappedItems , sv.mlresind.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mlresind.getFormData()).toString().trim());  
%>


<% 
	if((new Byte((sv.mlresind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mlresind).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:192px;"> 
<%
} 
%>

<select name='mlresind' type='list' style="width:190px;"
<% 
	if((new Byte((sv.mlresind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mlresind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mlresind).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%} %>						</div>
					</div>
		
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Reference")%></label>
					
							<input name='mbnkref' 
type='text'

<%

		formatValue = (sv.mbnkref.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.mbnkref.getLength()%>'
maxLength='<%= sv.mbnkref.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mbnkref)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.mbnkref).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mbnkref).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mbnkref).getColor()== null  ? 
			"input_cell" :  (sv.mbnkref).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>
					</div>
			</div>
		 
		<br />
		<%
			if (sv.mrtaFlag.compareTo("N") != 0) {
		%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.loandur).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Duration")%></label>
						<div class="input-group" style="max-width:100px">
						<%	
			qpsf = fw.getFieldXMLDef((sv.loandur).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
			
	%>

<input name='loandur' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.loandur) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.loandur);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.loandur) %>'
	 <%}%>

size='<%= sv.loandur.getLength()%>'
maxLength='<%= sv.loandur.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(loandur)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.loandur).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.loandur).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.loandur).getColor()== null  ? 
			"input_cell" :  (sv.loandur).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
 
<%
	} 
%>
					</div></div>
				</div>
				<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Calculation Type")%></label>
						<%	
	if ((new Byte((sv.intcaltype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {

	if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("SI")) { //ILIFE-3722
		longValue=resourceBundleHandler.gettingValueFromBundle("Simple Interest");
	}
	if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("CO")) { //ILIFE-3722
	longValue=resourceBundleHandler.gettingValueFromBundle("Compound Interest");
	}
	 
%>

<% 
	if((new Byte((sv.intcaltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>

	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

<% if("red".equals((sv.intcaltype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:202px;"> 
<%
} 
%>
		
<select name='intcaltype' style="width:200px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(intcaltype)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.intcaltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.intcaltype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<!-- ILIFE-3722-STARTS -->
<!-- ILIFE-3709-STARTS -->
<option value="SI"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("SI")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Simple Interest")%></option>
<option value="CO"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("CO")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Compound Interest")%></option>
<option value="N/A"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("N/A")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Interest N/A")%></option>		
<!-- ILIFE-3709-ENDS		 -->
<!-- ILIFE-3722-ENDS		 -->
</select>
<% if("red".equals((sv.intcaltype).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>
					</div>
				</div>
		</div>
		<%
			}
		%>
		<br />
		<div style='visibility:hidden;'>
		<div class="row">
			<div class="col-md-4">
					<label><%=resourceBundleHandler.gettingValueFromBundle("(B/S)")%></label>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
