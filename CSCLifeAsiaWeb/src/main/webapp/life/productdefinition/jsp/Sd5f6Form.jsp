<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5f6";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sd5f6ScreenVars sv = (Sd5f6ScreenVars) fw.getVariables();
	if (appVars.ind01.isOn()) {
	//sv.autoType.setEnabled(BaseScreenData.DISABLED);
	}

%>

<%
	if (sv.Sd5f6screenWritten.gt(0)) {
%>
<%
	Sd5f6screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Status Item");
%>
<!--<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Current Status Code");
%>
<!--<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Set Status to");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premium");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " Regular Single");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund Pool Priority");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Premium");
%>
<%
	sv.fundPoolPri01.setClassString("");
	sv.fundPoolPri02.setClassString("");
	sv.fundPoolPri03.setClassString("");
	sv.fundPoolPri04.setClassString("");
	sv.fundPoolPri05.setClassString("");
	sv.fundPoolPri06.setClassString("");
	sv.fundPoolPri07.setClassString("");
	sv.fundPoolPri08.setClassString("");
	sv.fundPoolPri09.setClassString("");
	sv.fundPoolPri10.setClassString("");
	sv.fundPoolPri11.setClassString("");
	sv.fundPoolPri12.setClassString("");
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>

							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Holding:")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Or Coverage Level")%></label>
					<div style="width: 70px;">
						<!--  -->
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.fhbcl, (sv.fhbcl.getLength()))%>
						<!--  -->
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Specific Coverage Code")%></label>
					<div style="width: 100px;">
						<!--  -->
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.fhscc, (sv.fhscc.getLength()))%>
						<!--  -->
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Pool")%></label>
					<div class="input-group" style="width: 85px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.fhfp, (sv.fhfp.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px;" type="button"
								onclick="doFocus(document.getElementById('fhfp')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Debt Holding:")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Or Coverage Level")%></label>
					<div style="width: 70px;">
						<!--  -->
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.dhbcl, (sv.dhbcl.getLength()))%>
						<!--  -->
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Specific Coverage Code")%></label>
					<div style="width: 100px;">
						<!--  -->
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.dhscc, (sv.dhscc.getLength()))%>
						<!--  -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-Sd5f6' width='100%'>
						<tbody>
							<tr>
								<td style="padding-top: 15px;">Fund&nbsp;Pool&nbsp;Priority</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri01, (sv.fundPoolPri01.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri01')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri02, (sv.fundPoolPri02.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri02')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri03, (sv.fundPoolPri03.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri03')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri04, (sv.fundPoolPri04.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri04')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri05, (sv.fundPoolPri05.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri05')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri06, (sv.fundPoolPri06.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri06')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri07, (sv.fundPoolPri07.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri07')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri08, (sv.fundPoolPri08.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri08')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri09, (sv.fundPoolPri09.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri09')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri10, (sv.fundPoolPri10.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri10')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri11, (sv.fundPoolPri11.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri11')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td>
									<div class="input-group" style="width: 85px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.fundPoolPri12, (sv.fundPoolPri12.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px;" type="button"
												onclick="doFocus(document.getElementById('fundPoolPri12')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="16"></td>
							</tr>




						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br>


	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sd5f6protectWritten.gt(0)) {
%>
<%
	Sd5f6protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
