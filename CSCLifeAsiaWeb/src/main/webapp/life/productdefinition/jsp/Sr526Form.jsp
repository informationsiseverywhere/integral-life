

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR526";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr526ScreenVars sv = (Sr526ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind12.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Start ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1=Select");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sel");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Invoice Number");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Entity");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Descriptions");
%>
<%
	appVars.rollup(new int[]{93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Start")%></label>
					<div class="input-group">
						<input name='zdocno' type='text'
							<%formatValue = (sv.zdocno.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.zdocno.getLength()%>'
							maxLength='<%=sv.zdocno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zdocno)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.zdocno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zdocno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zdocno).getColor() == null
						? "input_cell"
						: (sv.zdocno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br />
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[5];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.invref.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 8;
			} else {
				calculatedValue = (sv.invref.getFormData()).length() * 8;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.chdrnum.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 14;
			} else {
				calculatedValue = (sv.chdrnum.getFormData()).length() * 14;
			}
			calculatedValue = calculatedValue - 20;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.exmcode.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 14;
			} else {
				calculatedValue = (sv.exmcode.getFormData()).length() * 14;
			}
			calculatedValue = calculatedValue - 30;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.longdesc.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = (sv.longdesc.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr526screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr526' width='100%'>
							<thead>
								<tr class='info'>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Invoice Number")%></center></th>

									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></center></th>

									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Entity")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr526screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr526screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.select.getLength()%>'
											value='<%=sv.select.getFormData()%>'
											size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(sr526screensfl.select)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="sr526screensfl" + "." + "select" + "_R" + count%>'
											id='<%="sr526screensfl" + "." + "select" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.select.getLength() * 12%> px;">

									</div>




									</td>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0]%>px;" align="left"><a
										href="javascript:;" class='tableLink'
										onClick='document.getElementById("<%="sr526screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.invref.getFormData()%></span></a>




									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[1]%>px;" align="left"><%=sv.chdrnum.getFormData()%>



									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[2]%>px;" align="left"><%=sv.exmcode.getFormData()%>



									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[3]%>px;" align="left"><%=sv.longdesc.getFormData()%>



									</td>

								</tr>

								<%
									count = count + 1;
										Sr526screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-sr526').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

