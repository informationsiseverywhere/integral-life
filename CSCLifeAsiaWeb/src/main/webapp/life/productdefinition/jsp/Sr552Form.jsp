

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR552";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr552ScreenVars sv = (Sr552ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind03.isOn()) {
			sv.optind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.optind.setReverse(BaseScreenData.REVERSED);
			sv.optind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.optind.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client      ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "IC No       ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Order by    ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"1-Client Name   2-IC No.   3-Contract No ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Start       ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Opt");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client Name");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "IC No ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.actn.setReverse(BaseScreenData.REVERSED);
			sv.actn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.actn.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					<table><tr><td>
						<%
							if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:110px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td><td style="padding-left:1px;">

						<%
							if (!((sv.cname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:155px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("IC No")%></label>
					<div class="input-group"  style="min-width:100px">
						<%
							if (!((sv.securityno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.securityno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.securityno.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Order by")%></label>
					<div class="input-group">
						<select value='<%=sv.actn.getFormData()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(actn)'
							onKeyUp='return checkMaxLength(this)' name='actn'
							class="input_cell">

							<option value="1 "
								<%if (sv.actn.getFormData().equalsIgnoreCase("1 ")) {%> Selected
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></option>
							<option value="2 "
								<%if (sv.actn.getFormData().equalsIgnoreCase("2 ")) {%> Selected
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("IC Number")%></option>
							<option value="3 "
								<%if (sv.actn.getFormData().equalsIgnoreCase("3 ")) {%> Selected
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></option>


							<%
								if ((new Byte((sv.actn).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| fw.getVariables().isScreenProtected()) {
							%> readonly="true" class="output_cell"
							<%
								} else if ((new Byte((sv.actn).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%> class="bold_cell"

							<%
								} else {
							%> class = '
							<%=(sv.actn).getColor() == null
						? "input_cell"
						: (sv.actn).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'

							<%
								}
							%>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Start")%></label>
					<div class="input-group">
						<input name='clntnaml' type='text'
							<%formatValue = (sv.clntnaml.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.clntnaml.getLength()%>'
							maxLength='<%=sv.clntnaml.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(clntnaml)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.clntnaml).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.clntnaml).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.clntnaml).getColor() == null
						? "input_cell"
						: (sv.clntnaml).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[4];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Opt").length() >= (sv.optind.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Opt").length()) * 12;
			} else {
				calculatedValue = (sv.optind.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Client Name").length() >= (sv.clntlname.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Client Name").length()) * 12;
			} else {
				calculatedValue = (sv.clntlname.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("IC No ").length() >= (sv.idno.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("IC No ").length()) * 12;
			} else {
				calculatedValue = (sv.idno.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Contract No ").length() >= (sv.mlentity.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Contract No ").length()) * 12;
			} else {
				calculatedValue = (sv.mlentity.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr552screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover"
					id='dataTables-sr552' width='100%'>
					<thead>
						<tr class='info'>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Opt")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("IC No")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></center></th>
						</tr>
					</thead>
					<%
						Sr552screensfl.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (Sr552screensfl
								//start ILIFE-837 Substandard Link on Lives Assured Screen S5005 Bombs
								.hasMoreScreenRows(sfl) /* && (!sv.optind.isEmptyOrNull()) */) { //MIBT-123
							//end ILIFE-837
					%>
					<tbody>

						<tr>
							<td
								<%if ((sv.optind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								align="right" <%} else {%> align="left" <%}%>><input
								type="radio" value='<%=sv.optind.getFormData()%>'
								onFocus='doFocus(this)'
								onHelp='return fieldHelp("sr552screensfl" + "." +
						 "optind")'
								onKeyUp='return checkMaxLength(this)'
								name='sr552screensfl.optind_R<%=count%>'
								id='sr552screensfl.optind_R<%=count%>'
								onClick="selectedRow('sr552screensfl.optind_R<%=count%>')"
								class="radio" /></td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"
								<%if ((sv.clntlname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								align="right" <%} else {%> align="left" <%}%>><%=sv.clntlname.getFormData()%>



							</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"
								<%if ((sv.idno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								align="right" <%} else {%> align="left" <%}%>><%=sv.idno.getFormData()%>



							</td>
							<td class="tableDataTag" style="width:<%=tblColumnWidth[3]%>px;"
								<%if ((sv.mlentity).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								align="right" <%} else {%> align="left" <%}%>><%=sv.mlentity.getFormData()%>



							</td>

						</tr>

						<%
							count = count + 1;
								Sr552screensfl.setNextScreenRow(sfl, appVars, sv);
							}
						%>
					</tbody>
				</table>
			</div>
		</div></div>

		<div class="row" style='visibility: hidden;'>
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("1-Client Name   2-IC No.   3-Contract No")%>
					</label>
				</div>
			</div>
		</div>

		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
       $(document).ready(function() {
              $('#dataTables-sr552').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
				      info:     false,
				      searching: false,
					  language: {
						  "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
						  "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
						  "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
						  "sEmptyTable": dtmessage,
						  "paginate": {
							  "next":       nextval,
							  "previous":   previousval
						  }
                  }
              });
       });
</script>  

<%@ include file="/POLACommon2NEW.jsp"%>

