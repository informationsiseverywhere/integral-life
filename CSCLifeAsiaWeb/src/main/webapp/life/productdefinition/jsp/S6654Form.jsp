<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6654";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S6654ScreenVars sv = (S6654ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6654screenWritten.gt(0)) {
%>
<%
	S6654screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Billing Processing ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Lead Time (Days) ");
%>
<%
	sv.leadDays.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Overdue Processing ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Lag Time");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Letter");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Days)");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subroutine");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Overdue 1 ");
%>
<%
	sv.daexpy01.setClassString("");
%>
<%
	sv.doctid01.setClassString("");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "2 ");
%>
<%
	sv.daexpy02.setClassString("");
%>
<%
	sv.doctid02.setClassString("");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "3 ");
%>
<%
	sv.daexpy03.setClassString("");
%>
<%
	sv.doctid03.setClassString("");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Arrears ");
%>
<%
	sv.nonForfietureDays.setClassString("");
%>
<%
	sv.doctid04.setClassString("");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Arrears Method ( when APL ) ");
%>
<%
	sv.arrearsMethod.setClassString("");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Default Premium Renewal Date as  ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Risk Commencement Date");
%>
<%
	sv.renwdate1.setClassString("");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"or    Risk Commencement Date + X Frequency");
%>
<%
	sv.renwdate2.setClassString("");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Frequency ");
%>
<%
	sv.zrfreq01.setClassString("");
%>
<%
	sv.zrfreq02.setClassString("");
%>
<%
	sv.zrfreq03.setClassString("");
%>
<%
	sv.zrfreq04.setClassString("");
%>
<%
	sv.zrfreq05.setClassString("");
%>
<%
	sv.zrfreq06.setClassString("");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Increment ");
%>
<%
	sv.zrincr01.setClassString("");
%>
<%
	sv.zrincr02.setClassString("");
%>
<%
	sv.zrincr03.setClassString("");
%>
<%
	sv.zrincr04.setClassString("");
%>
<%
	sv.zrincr05.setClassString("");
%>
<%
	sv.zrincr06.setClassString("");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Premium Collection Subroutine ");
%>
<%
	sv.collectsub.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.leadDays.setReverse(BaseScreenData.REVERSED);
				sv.leadDays.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.leadDays.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.daexpy01.setReverse(BaseScreenData.REVERSED);
				sv.daexpy01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.daexpy01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.doctid01.setReverse(BaseScreenData.REVERSED);
				sv.doctid01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.doctid01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.daexpy02.setReverse(BaseScreenData.REVERSED);
				sv.daexpy02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.daexpy02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.doctid02.setReverse(BaseScreenData.REVERSED);
				sv.doctid02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.doctid02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.daexpy03.setReverse(BaseScreenData.REVERSED);
				sv.daexpy03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.daexpy03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.doctid03.setReverse(BaseScreenData.REVERSED);
				sv.doctid03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.doctid03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.nonForfietureDays.setReverse(BaseScreenData.REVERSED);
				sv.nonForfietureDays.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.nonForfietureDays.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.doctid04.setReverse(BaseScreenData.REVERSED);
				sv.doctid04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.doctid04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.arrearsMethod.setReverse(BaseScreenData.REVERSED);
				sv.arrearsMethod.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.arrearsMethod.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.renwdate1.setReverse(BaseScreenData.REVERSED);
				sv.renwdate1.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.renwdate1.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.renwdate2.setReverse(BaseScreenData.REVERSED);
				sv.renwdate2.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.renwdate2.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.zrfreq01.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq01.setColor(BaseScreenData.RED);
				
			}
			if (!appVars.ind21.isOn()) {
				sv.zrfreq01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.zrfreq02.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq02.setColor(BaseScreenData.RED);
				
			}
			if (!appVars.ind23.isOn()) {
				sv.zrfreq02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.zrfreq03.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.zrfreq03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.zrfreq04.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.zrfreq04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.zrfreq05.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.zrfreq05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.zrfreq06.setReverse(BaseScreenData.REVERSED);
				sv.zrfreq06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.zrfreq06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.zrincr01.setReverse(BaseScreenData.REVERSED);
				sv.zrincr01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.zrincr01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.zrincr02.setReverse(BaseScreenData.REVERSED);
				sv.zrincr02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.zrincr02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.zrincr03.setReverse(BaseScreenData.REVERSED);
				sv.zrincr03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.zrincr03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.zrincr04.setReverse(BaseScreenData.REVERSED);
				sv.zrincr04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.zrincr04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.zrincr05.setReverse(BaseScreenData.REVERSED);
				sv.zrincr05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.zrincr05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.zrincr06.setReverse(BaseScreenData.REVERSED);
				sv.zrincr06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.zrincr06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.collectsub.setReverse(BaseScreenData.REVERSED);
				sv.collectsub.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.collectsub.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind50.isOn()) {
				sv.zrfreq01.setInvisibility(BaseScreenData.INVISIBLE);
				sv.zrfreq02.setInvisibility(BaseScreenData.INVISIBLE);
				sv.zrfreq03.setInvisibility(BaseScreenData.INVISIBLE);
				sv.zrfreq04.setInvisibility(BaseScreenData.INVISIBLE);
				sv.zrfreq05.setInvisibility(BaseScreenData.INVISIBLE);
				sv.zrfreq06.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind51.isOn()) {
				sv.zrincr01.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind52.isOn()) {
				sv.zrincr02.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind53.isOn()) {
				sv.zrincr03.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind54.isOn()) {
				sv.zrincr04.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind55.isOn()) {
				sv.zrincr05.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind56.isOn()) {
				sv.zrincr06.setInvisibility(BaseScreenData.INVISIBLE);
			}
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>


</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Processing")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lead Time (Days)")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.leadDays).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='leadDays' type='text'
							<%if ((sv.leadDays).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.leadDays)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.leadDays);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.leadDays)%>' <%}%>
							size='<%=sv.leadDays.getLength()%>'
							maxLength='<%=sv.leadDays.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(leadDays)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.leadDays).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.leadDays).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.leadDays).getColor() == null ? "input_cell"
							: (sv.leadDays).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Overdue Processing ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lag Time")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Letter")%></label>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("(Days)")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Overdue 1 ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.daexpy01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.doctid01).replaceAll("width:100%", "width:110%")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("2 ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.daexpy02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.doctid02).replaceAll("width:100%", "width:110%")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("3 ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.daexpy03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.doctid03).replaceAll("width:100%", "width:110%")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Arrears ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.nonForfietureDays,
						COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.doctid04).replaceAll("width:100%", "width:110%")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Arrears Method ( when APL )  ")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<%
                                         if ((new Byte((sv.arrearsMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 100px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.arrearsMethod)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group"style="width: 105px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.arrearsMethod)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('arrearsMethod')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Premium Renewal Date as")%></label>
					<br /> <label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%>
					</label>
					<%
						if ((browerVersion.equals(IE11))) {
					%>



					<input style="margin-left: -7px;" type='checkbox' name='renwdate1'
						value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(renwdate1)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.renwdate1).getColor() != null) {%>
						style='background-color:#FF0000;'
						<%}
					if ((sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.renwdate1).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('renwdate1')" /> <input type='checkbox'
						name='renwdate1' value=' '
						<%if (!(sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('renwdate1')" />

					<%
						}
					%>

					<%
						if ((browerVersion.equals(IE8))) {
					%>



					<input style="margin-left: 1px;" type='checkbox' name='renwdate1'
						value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(renwdate1)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.renwdate1).getColor() != null) {%>
						style='background-color:#FF0000;'
						<%}
					if ((sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.renwdate1).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('renwdate1')" /> <input type='checkbox'
						name='renwdate1' value=' '
						<%if (!(sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('renwdate1')" />

					<%
						}
					%>

					<%
						if ((browerVersion.equals(Chrome))) {
					%>



					<input style="margin-left: 8px;" type='checkbox' name='renwdate1'
						value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(renwdate1)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.renwdate1).getColor() != null) {%>
						style='background-color:#FF0000;'
						<%}
					if ((sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.renwdate1).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('renwdate1')" /> <input type='checkbox'
						name='renwdate1' value=' '
						<%if (!(sv.renwdate1).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('renwdate1')" />

					<%
						}
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("or Risk Commencement Date+X Frequency")%></label>

					<%
						if ((browerVersion.equals(Chrome))) {
					%>

					<input style="margin-left: 11px;" type='checkbox' name='renwdate2'
						value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(renwdate2)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.renwdate2).getColor() != null) {%>
						style='background-color:#FF0000;'
						<%}
					if ((sv.renwdate2).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.renwdate2).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('renwdate2')" /> <input type='checkbox'
						name='renwdate2' value=' '
						<%if (!(sv.renwdate2).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('renwdate2')" />
					<%
						}
					%>
					<%
						if ((browerVersion.equals(IE11)) || (browerVersion.equals(IE8))) {
					%>

					<input style="margin-left: 5px;" type='checkbox' name='renwdate2'
						value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(renwdate2)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.renwdate2).getColor() != null) {%>
						style='background-color:#FF0000;'
						<%}
					if ((sv.renwdate2).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.renwdate2).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('renwdate2')" /> <input type='checkbox'
						name='renwdate2' value=' '
						<%if (!(sv.renwdate2).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('renwdate2')" />
					<%
						}
						%>

				</div>
			</div>
		</div>
		
		<%if ((new Byte((sv.zrfreq01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="row">
			<div class="col-md-2">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency ")%></label>
				</div>
			</div>
			
			<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq01)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq02)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq03)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq04)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq05)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrfreq06)%></div>
				</div>
			</div>
		</div>
		<%} %>
		
		
		<div class="row">
			<div class="col-md-2">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Increment ")%></label>
			</div>
			</div>
				
				
			<div class="row">	
		  <%if ((new Byte((sv.zrincr01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
		  			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr01)%></div>
				</div>
			</div>
			  <% } %>	
			  
			 <%if ((new Byte((sv.zrincr02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>  
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr02)%></div>
				</div>
			</div>
			  <% } %>	
			
				 <%if ((new Byte((sv.zrincr03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>  
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr03)%></div>
				</div>
			</div>
			 <% } %>
			 
			 <%if ((new Byte((sv.zrincr04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>  
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr04)%></div>
				</div>
			</div>
			 <% } %>
			
			 <%if ((new Byte((sv.zrincr05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>  
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr05)%></div>
				</div>
			</div>
			 <% } %>
			
			 <%if ((new Byte((sv.zrincr06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>  
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.zrincr06)%></div>
				</div>
			</div>
			 <% } %>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Collection Subroutine")%></label>
					<div style="width: 100px;">
						<input name='collectsub' type='text'
							<%if ((sv.collectsub).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.collectsub.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.collectsub.getLength()%>'
							maxLength='<%=sv.collectsub.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(collectsub)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.collectsub).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.collectsub).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.collectsub).getColor() == null ? "input_cell"
							: (sv.collectsub).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S6654protectWritten.gt(0)) {
%>
<%
	S6654protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>