

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR517";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr517ScreenVars sv = (Sr517ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Waive itself                     ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Waive all life                   ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Waive Policy Fee                 ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Accelerated Crisis Waiver        ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Waive the following covers/riders ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Continuation Item ");
%>

<%
	{
		if (appVars.ind23.isOn()) {
			sv.zrwvflg01.setReverse(BaseScreenData.REVERSED);
			sv.zrwvflg01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.zrwvflg01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.zrwvflg02.setReverse(BaseScreenData.REVERSED);
			sv.zrwvflg02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.zrwvflg02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.ctable01.setReverse(BaseScreenData.REVERSED);
			sv.ctable01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.ctable01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.ctable02.setReverse(BaseScreenData.REVERSED);
			sv.ctable02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.ctable02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.ctable03.setReverse(BaseScreenData.REVERSED);
			sv.ctable03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.ctable03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.ctable04.setReverse(BaseScreenData.REVERSED);
			sv.ctable04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.ctable04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.ctable05.setReverse(BaseScreenData.REVERSED);
			sv.ctable05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.ctable05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.ctable06.setReverse(BaseScreenData.REVERSED);
			sv.ctable06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.ctable06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.ctable07.setReverse(BaseScreenData.REVERSED);
			sv.ctable07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.ctable07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.ctable08.setReverse(BaseScreenData.REVERSED);
			sv.ctable08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.ctable08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.ctable09.setReverse(BaseScreenData.REVERSED);
			sv.ctable09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.ctable09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.ctable10.setReverse(BaseScreenData.REVERSED);
			sv.ctable10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.ctable10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.ctable11.setReverse(BaseScreenData.REVERSED);
			sv.ctable11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.ctable11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.ctable12.setReverse(BaseScreenData.REVERSED);
			sv.ctable12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.ctable12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.ctable13.setReverse(BaseScreenData.REVERSED);
			sv.ctable13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.ctable13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.ctable14.setReverse(BaseScreenData.REVERSED);
			sv.ctable14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.ctable14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.ctable15.setReverse(BaseScreenData.REVERSED);
			sv.ctable15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.ctable15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.ctable16.setReverse(BaseScreenData.REVERSED);
			sv.ctable16.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.ctable16.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.ctable17.setReverse(BaseScreenData.REVERSED);
			sv.ctable17.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.ctable17.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.ctable18.setReverse(BaseScreenData.REVERSED);
			sv.ctable18.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.ctable18.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.ctable19.setReverse(BaseScreenData.REVERSED);
			sv.ctable19.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.ctable19.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.ctable20.setReverse(BaseScreenData.REVERSED);
			sv.ctable20.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.ctable20.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.ctable21.setReverse(BaseScreenData.REVERSED);
			sv.ctable21.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.ctable21.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.ctable22.setReverse(BaseScreenData.REVERSED);
			sv.ctable22.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.ctable22.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.ctable23.setReverse(BaseScreenData.REVERSED);
			sv.ctable23.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.ctable23.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.ctable24.setReverse(BaseScreenData.REVERSED);
			sv.ctable24.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.ctable24.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.ctable25.setReverse(BaseScreenData.REVERSED);
			sv.ctable25.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.ctable25.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.ctable26.setReverse(BaseScreenData.REVERSED);
			sv.ctable26.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.ctable26.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.ctable27.setReverse(BaseScreenData.REVERSED);
			sv.ctable27.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.ctable27.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.ctable28.setReverse(BaseScreenData.REVERSED);
			sv.ctable28.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.ctable28.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.ctable29.setReverse(BaseScreenData.REVERSED);
			sv.ctable29.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.ctable29.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.ctable30.setReverse(BaseScreenData.REVERSED);
			sv.ctable30.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.ctable30.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.ctable31.setReverse(BaseScreenData.REVERSED);
			sv.ctable31.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.ctable31.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.ctable32.setReverse(BaseScreenData.REVERSED);
			sv.ctable32.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.ctable32.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind58.isOn()) {
			sv.ctable33.setReverse(BaseScreenData.REVERSED);
			sv.ctable33.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.ctable33.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.ctable34.setReverse(BaseScreenData.REVERSED);
			sv.ctable34.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.ctable34.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.ctable35.setReverse(BaseScreenData.REVERSED);
			sv.ctable35.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.ctable35.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.ctable36.setReverse(BaseScreenData.REVERSED);
			sv.ctable36.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.ctable36.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.ctable37.setReverse(BaseScreenData.REVERSED);
			sv.ctable37.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.ctable37.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.ctable38.setReverse(BaseScreenData.REVERSED);
			sv.ctable38.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.ctable38.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.ctable39.setReverse(BaseScreenData.REVERSED);
			sv.ctable39.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.ctable39.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.ctable40.setReverse(BaseScreenData.REVERSED);
			sv.ctable40.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.ctable40.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.ctable41.setReverse(BaseScreenData.REVERSED);
			sv.ctable41.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.ctable41.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind67.isOn()) {
			sv.ctable42.setReverse(BaseScreenData.REVERSED);
			sv.ctable42.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.ctable42.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind68.isOn()) {
			sv.ctable43.setReverse(BaseScreenData.REVERSED);
			sv.ctable43.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.ctable43.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.ctable44.setReverse(BaseScreenData.REVERSED);
			sv.ctable44.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.ctable44.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.ctable45.setReverse(BaseScreenData.REVERSED);
			sv.ctable45.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.ctable45.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.ctable46.setReverse(BaseScreenData.REVERSED);
			sv.ctable46.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.ctable46.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind72.isOn()) {
			sv.ctable47.setReverse(BaseScreenData.REVERSED);
			sv.ctable47.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.ctable47.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.ctable48.setReverse(BaseScreenData.REVERSED);
			sv.ctable48.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.ctable48.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind74.isOn()) {
			sv.ctable49.setReverse(BaseScreenData.REVERSED);
			sv.ctable49.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind74.isOn()) {
			sv.ctable49.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			sv.ctable50.setReverse(BaseScreenData.REVERSED);
			sv.ctable50.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.ctable50.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind76.isOn()) {
			sv.zrwvflg03.setReverse(BaseScreenData.REVERSED);
			sv.zrwvflg03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind76.isOn()) {
			sv.zrwvflg03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			sv.zrwvflg04.setReverse(BaseScreenData.REVERSED);
			sv.zrwvflg04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.zrwvflg04.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind78.isOn()) {
			sv.zrwvflg05.setReverse(BaseScreenData.REVERSED);
			sv.zrwvflg05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.zrwvflg05.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind79.isOn()) {
	        sv.zrwvflg05.setInvisibility(BaseScreenData.INVISIBLE);
	    }
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<!-- end row 1 -->
		<!-- row 2 -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td><label style="font-weight: bolder; font-size: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("To")%></label></td>
							<td>&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- end row 2 -->
		<!-- row 3 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waive itself")%></label>
					<ul class="list-inline">
						<li><input name='zrwvflg01' type='text'
							<%formatValue = (sv.zrwvflg01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zrwvflg01.getLength()%>'
							maxLength='<%=sv.zrwvflg01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zrwvflg01)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %>
							<%if ((new Byte((sv.zrwvflg01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%> 
							disabled
							class="bold_cell" <%} }%>
							<%if ((new Byte((sv.zrwvflg01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrwvflg01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrwvflg01).getColor() == null ? "input_cell"
						: (sv.zrwvflg01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
						<li><label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
						</li>
					</ul>

				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waive all life")%></label>
					<ul class="list-inline">
						<li><input name='zrwvflg02' type='text'
							<%formatValue = (sv.zrwvflg02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zrwvflg02.getLength()%>'
							maxLength='<%=sv.zrwvflg02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zrwvflg02)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %> 
							<%if ((new Byte((sv.zrwvflg02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%> 
							disabled
							class="bold_cell" <%} }%>
							<%if ((new Byte((sv.zrwvflg02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrwvflg02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrwvflg02).getColor() == null ? "input_cell"
						: (sv.zrwvflg02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
						<li><label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label></li>
					</ul>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waive Policy Fee")%></label>
					<ul class="list-inline">
						<li><input name='zrwvflg03' type='text'
							<%formatValue = (sv.zrwvflg03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zrwvflg03.getLength()%>'
							maxLength='<%=sv.zrwvflg03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zrwvflg03)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %>
							<%if ((new Byte((sv.zrwvflg03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%> 
							disabled
							class="bold_cell"
							<%} }%>
							<%if ((new Byte((sv.zrwvflg03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrwvflg03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrwvflg03).getColor() == null ? "input_cell"
						: (sv.zrwvflg03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
						<li><label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row 3 -->
		<!-- row 4 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accelerated Crisis Waiver")%></label>
					<ul class="list-inline">
						<li><input name='zrwvflg04' type='text'
							<%formatValue = (sv.zrwvflg04.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zrwvflg04.getLength()%>'
							maxLength='<%=sv.zrwvflg04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zrwvflg04)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %>
							<%if ((new Byte((sv.zrwvflg04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%> 
							disabled
							class="bold_cell"
							<%} }%>
							<%if ((new Byte((sv.zrwvflg04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrwvflg04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrwvflg04).getColor() == null ? "input_cell"
						: (sv.zrwvflg04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
						<li>
						<li><label style="margin-left: -10px;"><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label></li>
						</li>
					</ul>
				</div>
			</div>
			
			     <%if ((new Byte((sv.zrwvflg05).getInvisible())).compareTo(
				        		new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Apply rerate on existing waiver claim’ ")%></label>
					<ul class="list-inline">
						<li><input name='zrwvflg05' type='text'
							<%formatValue = (sv.zrwvflg05.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.zrwvflg05.getLength()%>'
							maxLength='<%=sv.zrwvflg05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zrwvflg05)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %>
							<%if ((new Byte((sv.zrwvflg05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%> 
							disabled
							class="bold_cell"
							<%} }%>
							<%if ((new Byte((sv.zrwvflg05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrwvflg05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrwvflg05).getColor() == null ? "input_cell"
						: (sv.zrwvflg05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
						<li>
						<li><label style="margin-left: -10px;"><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label></li>
						</li>
					</ul>
				</div>
			</div>
			
		<%	} %>
		</div>
		<!-- end row 4 -->
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waive the following covers/riders")%></label>
					<div class="table-responsive">
						<table class="table border-none  table-hover" width='100%'>
							<tbody>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable01, (sv.ctable01.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable01')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable02, (sv.ctable02.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable02')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
										<%
                                         if ((new Byte((sv.ctable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable03, (sv.ctable03.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable03')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable04, (sv.ctable04.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable04')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable05, (sv.ctable05.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable05')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>

								<tr>
									<td>
										<%
                                         if ((new Byte((sv.ctable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable06, (sv.ctable06.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable06')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable07, (sv.ctable07.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable07')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable08, (sv.ctable08.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable08')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable09, (sv.ctable09.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable09')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable10, (sv.ctable10.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable10')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
								</tr>

								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable11)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable11, (sv.ctable11.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable11')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable12)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable12, (sv.ctable12.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable12')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable13)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable13, (sv.ctable13.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable13')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable14)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable14, (sv.ctable14.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable14')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable15)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable15, (sv.ctable15.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable15')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable16)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable16, (sv.ctable16.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable16')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable17)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable17, (sv.ctable17.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable17')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable18)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable18, (sv.ctable18.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable18')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable19)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable19, (sv.ctable19.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable19')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable20)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable20, (sv.ctable20.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable20')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable21).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable21)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable21, (sv.ctable21.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable21')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable22).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable22)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable22, (sv.ctable22.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable22')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable23).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable23)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable23, (sv.ctable23.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable23')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
										<%
                                         if ((new Byte((sv.ctable24).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable24)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable24, (sv.ctable24.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable24')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable25).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable25)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable25, (sv.ctable25.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable25')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable26).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable26)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable26, (sv.ctable26.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable26')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable27).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable27)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable27, (sv.ctable27.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable27')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable28).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable28)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable28, (sv.ctable28.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable28')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable29).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable29)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable29, (sv.ctable29.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable29')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable30).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable30)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable30, (sv.ctable30.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable30')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable31).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable31)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable31, (sv.ctable31.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable31')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable32).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable32)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable32, (sv.ctable32.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable32')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable33).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable33)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable33, (sv.ctable33.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable33')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable34).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable34)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable34, (sv.ctable34.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable34')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable35).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable35)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable35, (sv.ctable35.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable35')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable36).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable36)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable36, (sv.ctable36.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable36')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable37).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable37)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable37, (sv.ctable37.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable37')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable38).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable38)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable38, (sv.ctable38.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable38')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable39).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable39)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable39, (sv.ctable39.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable39')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable40).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable40)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable40, (sv.ctable40.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable40')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable41).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable41)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable41, (sv.ctable41.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable41')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable42).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable42)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable42, (sv.ctable42.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable42')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable43).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable43)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable43, (sv.ctable43.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable43')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable44).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable44)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable44, (sv.ctable44.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable44')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable45).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable45)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable45, (sv.ctable45.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable45')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
								<tr>
									<td>
									<%
                                         if ((new Byte((sv.ctable46).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable46)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable46, (sv.ctable46.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable46')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable47).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable47)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable47, (sv.ctable47.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable47')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
											<%
                                         if ((new Byte((sv.ctable48).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable48)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable48, (sv.ctable48.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable48')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
									<%
                                         if ((new Byte((sv.ctable49).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable49)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable49, (sv.ctable49.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable49')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
									<td>
										<%
                                         if ((new Byte((sv.ctable50).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="min-width: 71px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable50)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
										<div class="input-group" style="min-width:71px;max-width:120px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable50, (sv.ctable50.getLength()))%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('ctable50')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
										<%} %>
										
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4" style="left: 5px;">
				<div class="form-group">
					<ul class="list-inline">
						<li><label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label></li>
						<li><input name='contitem' type='text'
							<%formatValue = (sv.contitem.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.contitem.getLength()%>'
							maxLength='<%=sv.contitem.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(contitem)'
							onKeyUp='return checkMaxLength(this)'
							<%if(sv.isEnquiryMode.getFormData().toString().trim().equals("1")){ %>
							disabled
							class=' <%=(sv.contitem).getColor() == null ? "input_cell"
							: (sv.contitem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>	
							<%if ((new Byte((sv.contitem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.contitem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell"
							 <%} else {%>
							class=' <%=(sv.contitem).getColor() == null ? "input_cell"
						: (sv.contitem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.table.border-none tr td, .table.border-none tr th {
	border: none !important;
}
</style>
</style>

<%@ include file="/POLACommon2NEW.jsp"%>

