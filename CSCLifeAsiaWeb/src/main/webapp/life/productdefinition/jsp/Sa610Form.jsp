<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sa610";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sa610ScreenVars sv = (Sa610ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sa610screenWritten.gt(0)) {
%>
<%
	Sa610screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	sv.fupcdes01.setClassString("");
%>
<%
	sv.fupcdes02.setClassString("");
%>
<%
	sv.fupcdes03.setClassString("");
%>
<%
	sv.fupcdes04.setClassString("");
%>
<%
	sv.fupcdes05.setClassString("");
%>
<%
	sv.occcode01.setClassString("");
%>
<%
	sv.occcode02.setClassString("");
%>
<%
	sv.occcode03.setClassString("");
%>
<%
	sv.occcode04.setClassString("");
%>
<%
	sv.occcode05.setClassString("");
%>
<%
	sv.occcode06.setClassString("");
%>
<%
	sv.occcode07.setClassString("");
%>
<%
	sv.occcode08.setClassString("");
%>
<%
	sv.occcode09.setClassString("");
%>
<%
	sv.occcode10.setClassString("");
%>


<%
	sv.occclassdes01.setClassString("");
%>
<%
	sv.occclassdes02.setClassString("");
%>
<%
	sv.occclassdes03.setClassString("");
%>
<%
	sv.occclassdes04.setClassString("");
%>
<%
	sv.occclassdes05.setClassString("");
%>
<%
	sv.occclassdes06.setClassString("");
%>
<%
	sv.occclassdes07.setClassString("");
%>
<%
	sv.occclassdes08.setClassString("");
%>
<%
	sv.occclassdes09.setClassString("");
%>
<%
	sv.occclassdes10.setClassString("");
%>


<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>
<%
	sv.language.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.fupcdes01.setReverse(BaseScreenData.REVERSED);
				sv.fupcdes01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.fupcdes01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.fupcdes02.setReverse(BaseScreenData.REVERSED);
				sv.fupcdes02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.fupcdes02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.fupcdes03.setReverse(BaseScreenData.REVERSED);
				sv.fupcdes03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.fupcdes03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.fupcdes04.setReverse(BaseScreenData.REVERSED);
				sv.fupcdes04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.fupcdes04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.fupcdes05.setReverse(BaseScreenData.REVERSED);
				sv.fupcdes05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.fupcdes05.setHighLight(BaseScreenData.BOLD);
			}
			
			if (appVars.ind06.isOn()) {
				sv.occcode01.setReverse(BaseScreenData.REVERSED);
				sv.occcode01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.occcode01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.occcode02.setReverse(BaseScreenData.REVERSED);
				sv.occcode02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.occcode02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.occcode03.setReverse(BaseScreenData.REVERSED);
				sv.occcode03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.occcode03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.occcode04.setReverse(BaseScreenData.REVERSED);
				sv.occcode04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.occcode04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.occcode05.setReverse(BaseScreenData.REVERSED);
				sv.occcode05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.occcode05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.occcode06.setReverse(BaseScreenData.REVERSED);
				sv.occcode06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.occcode06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.occcode07.setReverse(BaseScreenData.REVERSED);
				sv.occcode07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.occcode07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.occcode08.setReverse(BaseScreenData.REVERSED);
				sv.occcode08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.occcode08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.occcode09.setReverse(BaseScreenData.REVERSED);
				sv.occcode09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.occcode09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.occcode10.setReverse(BaseScreenData.REVERSED);
				sv.occcode10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.occcode10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.occclassdes01.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.occclassdes01.setHighLight(BaseScreenData.BOLD);
			}
			
			if (appVars.ind17.isOn()) {
				sv.occclassdes02.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.occclassdes02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.occclassdes03.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.occclassdes03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.occclassdes04.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.occclassdes04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.occclassdes05.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.occclassdes05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.occclassdes06.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.occclassdes06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.occclassdes07.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.occclassdes07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.occclassdes08.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.occclassdes08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.occclassdes09.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.occclassdes09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.occclassdes10.setReverse(BaseScreenData.REVERSED);
				sv.occclassdes10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.occclassdes10.setHighLight(BaseScreenData.BOLD);
			}

		}
%>



<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		
		<div class="row" style="padding-top:20px;">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		
		
			<div class="row" style="padding-top:20px;">
			<div class="col-md-2">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>	
									</div>
					
				<div class="col-md-10">
				<div class="form-group">
				<table>
				<tr>
					<td style="padding-left:30px;">
						
						<%
                                         if ((new Byte((sv.occcode01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					</td>
					<td style="padding-left:30px;">
	 			
					
						<%
                                         if ((new Byte((sv.occcode02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						</td>
					<td style="padding-left:30px;">

						<%
                                         if ((new Byte((sv.occcode03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
              					</td>
					<td style="padding-left:30px;">

						
						<%
                                         if ((new Byte((sv.occcode04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
										</td>
					<td style="padding-left:30px;">

						            <%      if ((new Byte((sv.occcode05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
				</td></tr>
				</table></div></div></div>
				
		<div class="row" style="padding-top:20px;">
			<div class="col-md-2">
			<label></label>
			</div>
			
				<div class="col-md-10">
				<table>
				<tr>
						<td style="padding-left:30px;">
							
						 <%      if ((new Byte((sv.occcode06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								</td>
					<td style="padding-left:30px;">

						 <%      if ((new Byte((sv.occcode07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
											</td>
					<td style="padding-left:30px;">

						 <%      if ((new Byte((sv.occcode08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						</td>
					<td style="padding-left:30px;">
						 <%      if ((new Byte((sv.occcode09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						</td>
					<td style="padding-left:30px;">
						 <%      if ((new Byte((sv.occcode10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occcode10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style='min-width:80px !important; max-width:80px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occcode10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occcode10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
			</td></tr></table>
			</div></div>
				
			<div class="row" style="padding-top:20px;">
			<div class="col-md-2">
				
						<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
					</div>
					
					
					<div class="col-md-10">
					<table>
					<tr>
				
				<td style="padding-left:20px;">
						<%      if ((new Byte((sv.occclassdes01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                     <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						</td>
				
				<td style="padding-left:20px;">
						<%      if ((new Byte((sv.occclassdes02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                     <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						</td>
						
						<td style="padding-left:20px;">
						<%      if ((new Byte((sv.occclassdes03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                    <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
	</td><td style="padding-left:20px;">
							<%      if ((new Byte((sv.occclassdes04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                    <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                    <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</td><td style="padding-left:20px;">
						<%      if ((new Byte((sv.occclassdes05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                     <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                    <div class="input-group" style='min-width:90px !important; max-width:90px !important'">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
								</td></tr>
							</table>
			</div></div>
		
					
		
		<div class="row" style="padding-top:20px;">
			<div class="col-md-2">
			<label> </label>
				</div>
			
			
			
			<div class="col-md-10">
						<table>
					<tr>
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.occclassdes06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>
					
						<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.occclassdes07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.occclassdes08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.occclassdes09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
												
					</td>
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.occclassdes10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                               <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.occclassdes10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.occclassdes10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('occclassdes10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>									
				</tr></table>
				</div></div>
			
				
			<div class="row" style="padding-top:20px;">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Follow Up")%></label>
				</div>
							
												
							
				<div class="col-md-10">
						<table>
					<tr>
						<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.fupcdes01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupcdes01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupcdes01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
										</td>		
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.fupcdes02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupcdes02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupcdes02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>	
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.fupcdes03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupcdes03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupcdes03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>									
													
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.fupcdes04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                   <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupcdes04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupcdes04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>	
					
					<td style="padding-left:20px;">
						
						<%      if ((new Byte((sv.fupcdes05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                 <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                                <%=smartHF.getHTMLVarExt(fw, sv.fupcdes05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                <div class="input-group" style='min-width:90px !important; max-width:90px !important'>
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('fupcdes05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
							
						
					</td>																						
				</tr>
				
			</table>
</div>
</div>
</div>
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sa610protectWritten.gt(0)) {
%>
<%
	Sa610protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>
