<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5aw";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr5awScreenVars sv = (Sr5awScreenVars) fw.getVariables();%>
<%{
	if (appVars.ind02.isOn()) {
		sv.cngfrmtaxpyr.setReverse(BaseScreenData.REVERSED);
		sv.cngfrmtaxpyr.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind02.isOn()) {
		sv.cngfrmtaxpyr.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind03.isOn()) {
		sv.cngfrmbrthctry.setReverse(BaseScreenData.REVERSED);
		sv.cngfrmbrthctry.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.cngfrmbrthctry.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.cngfrmnatlty.setReverse(BaseScreenData.REVERSED);
		sv.cngfrmnatlty.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.cngfrmnatlty.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind05.isOn()) {
		sv.cngtotaxpyr.setReverse(BaseScreenData.REVERSED);
		sv.cngtotaxpyr.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.cngtotaxpyr.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.cngtobrthctry.setReverse(BaseScreenData.REVERSED);
		sv.cngtobrthctry.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind06.isOn()) {
		sv.cngtobrthctry.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind07.isOn()) {
		sv.cngtonatlty.setReverse(BaseScreenData.REVERSED);
		sv.cngtonatlty.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.cngtonatlty.setHighLight(BaseScreenData.BOLD);
	}
} %>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div class="input-group">
					<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<div class="input-group">
					<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<table>
					<tr>
					<td>
					<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</td><td style="padding-left:1px;">
					<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
									
					</td>
					</tr>
					</table>
					
					
		</div></div></div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Change From"))%></label>
					
					</div></div></div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("US Tax Payor"))%></label>
					<div class="input-group" style="min-width:90px;">
					<%	
	if ((new Byte((sv.cngfrmtaxpyr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngfrmtaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
	}
	if(((sv.cngfrmtaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("No");
	}
		 
%>

<% 
	if((new Byte((sv.cngfrmtaxpyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngfrmtaxpyr).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngfrmtaxpyr' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngfrmtaxpyr)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngfrmtaxpyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngfrmtaxpyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngfrmtaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(((sv.cngfrmtaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


</select>
<% if("red".equals((sv.cngfrmtaxpyr).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Birth Country"))%></label>
					<div class="input-group" style="min-width:90px;">
					<%	
	if ((new Byte((sv.cngfrmbrthctry).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngfrmbrthctry.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("USA");
	}
	if(((sv.cngfrmbrthctry.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Non-USA");
	}
		 
%>

<% 
	if((new Byte((sv.cngfrmbrthctry).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngfrmbrthctry).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngfrmbrthctry' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngfrmbrthctry)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngfrmbrthctry).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngfrmbrthctry).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngfrmbrthctry.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("USA")%></option>
<option value="N"<% if(((sv.cngfrmbrthctry.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Non-USA")%></option>


</select>
<% if("red".equals((sv.cngfrmbrthctry).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					
					
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
				
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Nationality"))%></label>
					<div class="input-group" style="min-width:90px;">
					
					<%	
	if ((new Byte((sv.cngfrmnatlty).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngfrmnatlty.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("USA");
	}
	if(((sv.cngfrmnatlty.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Non-USA");
	}
		 
%>

<% 
	if((new Byte((sv.cngfrmnatlty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngfrmnatlty).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngfrmnatlty' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngfrmnatlty)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngfrmnatlty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngfrmnatlty).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngfrmnatlty.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("USA")%></option>
<option value="N"<% if(((sv.cngfrmnatlty.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Non-USA")%></option>


</select>
<% if("red".equals((sv.cngfrmnatlty).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					
					
					
					
					</div></div></div>
					
					
					
					
					
					
					
					</div>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Change From"))%></label>
					
					</div></div></div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("US Tax Payor"))%></label>
					<div class="input-group" style="min-width:90px;">
					<%	
	if ((new Byte((sv.cngtotaxpyr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngtotaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
	}
	if(((sv.cngtotaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("No");
	}
		 
%>

<% 
	if((new Byte((sv.cngtotaxpyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngtotaxpyr).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngtotaxpyr' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngtotaxpyr)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngtotaxpyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngtotaxpyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngtotaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(((sv.cngtotaxpyr.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


</select>
<% if("red".equals((sv.cngtotaxpyr).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Birth Country"))%></label>
					<div class="input-group" style="min-width:90px;">
					<%	
	if ((new Byte((sv.cngtobrthctry).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngtobrthctry.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("USA");
	}
	if(((sv.cngtobrthctry.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Non-USA");
	}
		 
%>

<% 
	if((new Byte((sv.cngtobrthctry).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngtobrthctry).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngtobrthctry' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngtobrthctry)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngtobrthctry).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngtobrthctry).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngtobrthctry.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("USA")%></option>
<option value="N"<% if(((sv.cngtobrthctry.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Non-USA")%></option>


</select>
<% if("red".equals((sv.cngtobrthctry).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					
					</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Nationality"))%></label>
					<div class="input-group" style="min-width:90px;">
					<%	
	if ((new Byte((sv.cngtonatlty).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.cngtonatlty.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("USA");
	}
	if(((sv.cngtonatlty.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Non-USA");
	}
		 
%>

<% 
	if((new Byte((sv.cngtonatlty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cngtonatlty).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='cngtonatlty' style="width:75px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(cngtonatlty)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.cngtonatlty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cngtonatlty).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.cngtonatlty.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("USA")%></option>
<option value="N"<% if(((sv.cngtonatlty.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Non-USA")%></option>


</select>
<% if("red".equals((sv.cngtonatlty).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					
					</div></div></div>
					
					
					
					
					
					</div>
					
					
</div></div>

<%@ include file="/POLACommon2NEW.jsp"%>
