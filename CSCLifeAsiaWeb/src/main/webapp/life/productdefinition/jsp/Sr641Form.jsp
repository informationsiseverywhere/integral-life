

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR641";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr641ScreenVars sv = (Sr641ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Medical Bill Data Capture by Contract");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Medical Bill Data Capture by Invoice");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"C - Medical Bill Approval");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"D - Medical Bill Inquiry");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"E - Contract Follow Ups");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract number  ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Invoice number  ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action  ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.invcref.setReverse(BaseScreenData.REVERSED);
			sv.invcref.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.invcref.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					<%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Invoice number")%></label>
					<%
                                         if ((new Byte((sv.invcref).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.invcref)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                              
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.invcref)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('invcref')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline" style="white-space: nowrap;"> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Bill Data Capture by Contract")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline" style="white-space: nowrap;"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Bill Data Capture by Invoice")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Bill Approval")%>
					</b></label>
				</div>
			</div>
		
			</div>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "D")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Bill Inquiry")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "E")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Contract Follow Ups")%>
					</b></label>
				</div>
			</div></div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='action' type='hidden'
							value='<%=sv.action.getFormData()%>'
							size='<%=sv.action.getLength()%>'
							maxLength='<%=sv.action.getLength()%>' class="input_cell"
							onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
							onKeyUp='return checkMaxLength(this)'>
					</div>
				</div>
			</div>
		

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

