

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR643";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr643ScreenVars sv = (Sr643ScreenVars) fw.getVariables();
%>

<%
	
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Invoice Number  ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid to         ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Entity          ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/L");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "ME Type");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "ME Date");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fee Amount");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.paidby.setReverse(BaseScreenData.REVERSED);
			sv.paidby.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.paidby.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.exmcode.setReverse(BaseScreenData.REVERSED);
			sv.exmcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.exmcode.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Invoice Number")%></label>
					<div>
						<%
							if (!((sv.invref.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.invref.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.invref.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to")%></label>
					<div>
						<%
							//ILIFE-1231 Starts gkashyap2
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"paidby"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("paidby");
							optionValue = makeDropDownList(mappedItems, sv.paidby.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.paidby.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.paidby).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell'>
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<select name='paidby' type='list' style="width: 170px;"
							<%if ((new Byte((sv.paidby).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.paidby).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.paidby).getColor() == null
							? "input_cell"
							: (sv.paidby).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
						<%
							//ILIFE-1231 Ends gkashyap2
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Entity")%></label>
					<table>
						<tr>
							<td>
							<%
                                         if ((new Byte((sv.exmcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.exmcode)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.exmcode)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('exmcode')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					</td>
					
							<td style="padding-left:1px;"> 
								<%
									if (!((sv.name.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.name.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.name.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style=" width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
	
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[6];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if ((resourceBundleHandler.gettingValueFromBundle("Contract").length()
					* 12) >= (sv.chdrsel.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Contract").length()) * 12;
			} else {
				calculatedValue = ((sv.chdrsel.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Life").length() >= (sv.life.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Life").length()) * 12;
			} else {
				calculatedValue = (sv.life.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("J/L").length() >= (sv.jlife.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("J/L").length()) * 12;
			} else {
				calculatedValue = (sv.jlife.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("ME Type").length()
					* 12) >= (sv.zmedtyp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("ME Type").length()) * 12;
			} else {
				calculatedValue = ((sv.zmedtyp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("ME Date").length()
					* 12) >= (sv.effdateDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("ME Date").length()) * 12;
			} else {
				calculatedValue = ((sv.effdateDisp.getFormData()).length() * 12) + 32;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Fee Amount").length() >= (sv.zmedfee.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Fee Amount").length()) * 12 - 40;
			} else {
				calculatedValue = (sv.zmedfee.getFormData()).length() * 12 - 40;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr643screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>

		<div class="row">
			<div class="col-md-12">
				
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr643' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></th>

									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("J/L")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("ME Type")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("ME Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Fee Amount")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr643screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									Map<String, Map<String, String>> zMedTypeMap = appVars.loadF4FieldsLong(new String[]{"zmedtyp"}, sv, "E",
											baseModel);

									while (Sr643screensfl.hasMoreScreenRows(sfl)) {

										{
											if (appVars.ind03.isOn()) {
												sv.chdrsel.setReverse(BaseScreenData.REVERSED);
												sv.chdrsel.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind03.isOn()) {
												sv.chdrsel.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind04.isOn()) {
												sv.life.setReverse(BaseScreenData.REVERSED);
												sv.life.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind04.isOn()) {
												sv.life.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind05.isOn()) {
												sv.jlife.setReverse(BaseScreenData.REVERSED);
												sv.jlife.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind05.isOn()) {
												sv.jlife.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind06.isOn()) {
												sv.zmedtyp.setReverse(BaseScreenData.REVERSED);
												sv.zmedtyp.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind06.isOn()) {
												sv.zmedtyp.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind07.isOn()) {
												sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
												sv.effdateDisp.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind07.isOn()) {
												sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind08.isOn()) {
												sv.zmedfee.setReverse(BaseScreenData.REVERSED);
												sv.zmedfee.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind08.isOn()) {
												sv.zmedfee.setHighLight(BaseScreenData.BOLD);
											}
										}
								%>
								<tr>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0]%>px;" align="left">
										    				
						<div class="input-group">



											<input
												name='<%="sr643screensfl" + "." + "chdrsel" + "_R" + count%>'
												id='<%="sr643screensfl" + "." + "chdrsel" + "_R" + count%>'
												type='text' value='<%=sv.chdrsel.getFormData()%>'
												class=" <%=(sv.chdrsel).getColor() == null
						? "input_cell"
						: (sv.chdrsel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.chdrsel.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr643screensfl.chdrsel)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.chdrsel.getLength() * 10%>px;">

											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													onclick="doFocus(document.getElementById('<%="sr643screensfl" + "." + "chdrsel" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>

										</div>
									
										
									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[1]%>px;" align="center">





										<input type='text' maxLength='<%=sv.life.getLength()%>'
										value='<%=sv.life.getFormData()%>'
										size='<%=sv.life.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr643screensfl.life)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr643screensfl" + "." + "life" + "_R" + count%>'
										id='<%="sr643screensfl" + "." + "life" + "_R" + count%>'
										style="width: <%=sv.life.getLength() * 30%>px;"
										<%if ((new Byte((sv.life).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.life).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.life).getColor() == null
							? "input_cell"
							: (sv.life).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%>>





									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[2]%>px;" align="center">





										<input type='text' maxLength='<%=sv.jlife.getLength()%>'
										value='<%=sv.jlife.getFormData()%>'
										size='<%=sv.jlife.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr643screensfl.jlife)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr643screensfl" + "." + "jlife" + "_R" + count%>'
										id='<%="sr643screensfl" + "." + "jlife" + "_R" + count%>'
										style="width: <%=sv.jlife.getLength() * 30%>px;"
										<%if ((new Byte((sv.jlife).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.jlife).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.jlife).getColor() == null
							? "input_cell"
							: (sv.jlife).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%>>





									</td>
									<%
										//ILIFE-1231 Starts gkashyap2
									%>



									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[3]%>px;" align="left">
										<div class="form-group">
										<%
											mappedItems = (Map) zMedTypeMap.get("zmedtyp");
												optionValue = makeDropDownList(mappedItems, sv.zmedtyp, 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.zmedtyp.getFormData()).toString().trim());
										%> <%
 	if ((new Byte((sv.zmedtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
 %>
										<div class='output_cell'>
											<%=XSSFilter.escapeHtml(longValue)%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.zmedtyp).getColor())) {
 %>
										<div
											class="input_cell red reverse">
											<%
												}
											%>
											<select name='<%="sr643screensfl.zmedtyp_R" + count%>'
												id='<%="sr643screensfl.zmedtyp_R" + count%>' type='list'
												style="width: 230px;" class='input_cell'>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.zmedtyp).getColor())) {
											%>
										</div> <%
 	}
 %> <%
 	}
 %> <%
 	//ILIFE-1231 Ends gkashyap2
 %>
								</div>
									</td>










									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[4] + 20%>px;" align="left">
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
											data-link-format="dd/mm/yyyy">

											<input
												name='<%="sr643screensfl" + "." + "effdateDisp" + "_R" + count%>'
												id='<%="sr643screensfl" + "." + "effdateDisp" + "_R" + count%>'
												type='text' value='<%=sv.effdateDisp.getFormData()%>'
												class=" <%=(sv.effdateDisp).getColor() == null
						? "input_cell"
						: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.effdateDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr643screensfl.effdateDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell"
												style="width: <%=sv.effdateDisp.getLength() * 10%>px;">
											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>


										</div>
									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[5]%>px;" align="right">
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.zmedfee).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.zmedfee,
														COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										%> <input type='text' maxLength='<%=sv.zmedfee.getLength()%>'
										value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zmedfee.getLength(), sv.zmedfee.getScale(), 3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(sr643screensfl.zmedfee)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr643screensfl" + "." + "zmedfee" + "_R" + count%>'
										id='<%="sr643screensfl" + "." + "zmedfee" + "_R" + count%>'
										style="text-align: right; width: <%=sv.zmedfee.getLength() * 10%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'
										<%if ((new Byte((sv.zmedfee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.zmedfee).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.zmedfee).getColor() == null
							? "input_cell"
							: (sv.zmedfee).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%>>



									</td>

								</tr>

								<%
									count = count + 1;
										Sr643screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-sr643').DataTable({
			ordering : false,
			searching : false,
			scrollY: "350px",
			scrollCollapse: true,
			scrollX:true,

		});
		
		$('#dataTables-sr643').on('draw.dt', function () {
			$(".date").datepicker({
			format: "dd/mm/yyyy",
			autoclose: true
			});
			});
			$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script language="javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>
<%
	}
%>

