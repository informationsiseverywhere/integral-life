

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR632";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr632ScreenVars sv = (Sr632ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Medical Provider Registration");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Medical Provider Maintenance");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"C - Medical Provider Enquiry");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Code ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action ");
%>

<%
	{
		if (appVars.ind10.isOn()) {
			sv.zmedprv.setReverse(BaseScreenData.REVERSED);
			sv.zmedprv.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.zmedprv.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></label>
					<%
                                         if ((new Byte((sv.zmedprv).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 130px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.zmedprv)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.zmedprv)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('zmedprv')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>
		</div>
		<br />
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Provider Registration")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Provider Maintenance")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Medical Provider Enquiry")%>
					</b></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='action' type='hidden'
							value='<%=sv.action.getFormData()%>'
							size='<%=sv.action.getLength()%>'
							maxLength='<%=sv.action.getLength()%>' class="input_cell"
							onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
							onKeyUp='return checkMaxLength(this)'>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
