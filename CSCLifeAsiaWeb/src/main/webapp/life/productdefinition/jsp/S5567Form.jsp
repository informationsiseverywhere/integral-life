<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5567";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5567ScreenVars sv = (S5567ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid from ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid to ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract Fee");
%>

<%
	{
		if (appVars.ind37.isOn()) {
			sv.billfreq01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.billfreq01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind37.isOn()) {
			sv.billfreq01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.billfreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.billfreq02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind40.isOn()) {
			sv.billfreq02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind39.isOn()) {
			sv.billfreq02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.billfreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.billfreq03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind42.isOn()) {
			sv.billfreq03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind41.isOn()) {
			sv.billfreq03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.billfreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.billfreq04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind44.isOn()) {
			sv.billfreq04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
			sv.billfreq04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.billfreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.billfreq05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind46.isOn()) {
			sv.billfreq05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			sv.billfreq05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.billfreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.billfreq06.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind48.isOn()) {
			sv.billfreq06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind47.isOn()) {
			sv.billfreq06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.billfreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.billfreq07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind50.isOn()) {
			sv.billfreq07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind49.isOn()) {
			sv.billfreq07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.billfreq07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.billfreq08.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind52.isOn()) {
			sv.billfreq08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind51.isOn()) {
			sv.billfreq08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.billfreq08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.billfreq09.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind54.isOn()) {
			sv.billfreq09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind53.isOn()) {
			sv.billfreq09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.billfreq09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.billfreq10.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind56.isOn()) {
			sv.billfreq10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind55.isOn()) {
			sv.billfreq10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.billfreq10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.cntfee01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind58.isOn()) {
			sv.cntfee01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind57.isOn()) {
			sv.cntfee01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.cntfee01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.cntfee02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.cntfee02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind59.isOn()) {
			sv.cntfee02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.cntfee02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.cntfee03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind62.isOn()) {
			sv.cntfee03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind61.isOn()) {
			sv.cntfee03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.cntfee03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.cntfee04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind64.isOn()) {
			sv.cntfee04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind63.isOn()) {
			sv.cntfee04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.cntfee04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.cntfee05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind66.isOn()) {
			sv.cntfee05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind65.isOn()) {
			sv.cntfee05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.cntfee05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind67.isOn()) {
			sv.cntfee06.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind68.isOn()) {
			sv.cntfee06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) {
			sv.cntfee06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.cntfee06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.cntfee07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind70.isOn()) {
			sv.cntfee07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind69.isOn()) {
			sv.cntfee07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.cntfee07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.cntfee08.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind72.isOn()) {
			sv.cntfee08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.cntfee08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.cntfee08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.cntfee09.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind74.isOn()) {
			sv.cntfee09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind73.isOn()) {
			sv.cntfee09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.cntfee09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			sv.cntfee10.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind76.isOn()) {
			sv.cntfee10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind75.isOn()) {
			sv.cntfee10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.cntfee10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td>








						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					<table>
						<tr>
							<td style="min-width:90px;">
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("Valid to")%></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td style="min-width:90px;">
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Fee")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq01.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								
								if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq01)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq01)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq01')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
						
								
								
						
						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group" >
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee01,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee01' type='text'
							<%if ((sv.cntfee01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee01.getLength(), sv.cntfee01.getScale(), 3)%>'
							maxLength='<%=sv.cntfee01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee01).getColor() == null ? "input_cell"
						: (sv.cntfee01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq02.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq02)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq02)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq02')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
								
						

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee02,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee02' type='text'
							<%if ((sv.cntfee02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee02.getLength(), sv.cntfee02.getScale(), 3)%>'
							maxLength='<%=sv.cntfee02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee02).getColor() == null ? "input_cell"
						: (sv.cntfee02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq03.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								

								if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq03)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq03)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq03')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
								
								
								
								
					

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee03,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee03' type='text'
							<%if ((sv.cntfee03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee03.getLength(), sv.cntfee03.getScale(), 3)%>'
							maxLength='<%=sv.cntfee03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee03).getColor() == null ? "input_cell"
						: (sv.cntfee03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq04.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq04)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq04)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq04')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
						
						

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group"> 
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee04,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee04' type='text'
							<%if ((sv.cntfee04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee04.getLength(), sv.cntfee04.getScale(), 3)%>'
							maxLength='<%=sv.cntfee04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee04).getColor() == null ? "input_cell"
						: (sv.cntfee04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq05.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq05)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq05)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq05')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
						
						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee05,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee05' type='text'
							<%if ((sv.cntfee05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee05.getLength(), sv.cntfee05.getScale(), 3)%>'
							maxLength='<%=sv.cntfee05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee05).getColor() == null ? "input_cell"
						: (sv.cntfee05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq06.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq06)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq06)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq06')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
								
								
								
								
					
					

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee06,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee06' type='text'
							<%if ((sv.cntfee06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee06.getLength(), sv.cntfee06.getScale(), 3)%>'
							maxLength='<%=sv.cntfee06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee06).getColor() == null ? "input_cell"
						: (sv.cntfee06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq07.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq07)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq07)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq07')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
				

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="form-group">
										<div class="input-group" >
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee07,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee07' type='text'
							<%if ((sv.cntfee07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee07.getLength(), sv.cntfee07.getScale(), 3)%>'
							maxLength='<%=sv.cntfee07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee07).getColor() == null ? "input_cell"
						: (sv.cntfee07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq08.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								
								if ((new Byte((sv.billfreq08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq08)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq08)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq08')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
								
								
								
								
					

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" >
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee08,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee08' type='text'
							<%if ((sv.cntfee08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee08.getLength(), sv.cntfee08.getScale(), 3)%>'
							maxLength='<%=sv.cntfee08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee08).getColor() == null ? "input_cell"
						: (sv.cntfee08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq09.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
								if ((new Byte((sv.billfreq09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                        || fw.getVariables().isScreenProtected()) {
                   %>
            
                   <div class="input-group" style="width: 100px;">
                                 <%=smartHF.getHTMLVarExt(fw, sv.billfreq09)%>
                                         
                          
                   </div>
                   <%
                          } else {
                   %>
                   <div class="input-group" style="width: 100px;">
                          <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq09)%>
                          <span class="input-group-btn">
                                 <button class="btn btn-info"
                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('billfreq09')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                 </button>
                          </span>
                   </div>
                   <%
                          }
                   %>
							
								
								


						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="form-group">
										<div class="input-group" >
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee09).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee09,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee09' type='text'
							<%if ((sv.cntfee09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee09.getLength(), sv.cntfee09.getScale(), 3)%>'
							maxLength='<%=sv.cntfee09.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee09).getColor() == null ? "input_cell"
						: (sv.cntfee09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
										<div class="input-group" style="width:100px;">
						<%
							longValue = sv.billfreq10.getFormData();
						%>

						<%
							if ((new Byte((sv.billfreq10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
								
							
                                if ((new Byte((sv.billfreq10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                              || fw.getVariables().isScreenProtected()) {
                         %>
                  
                         <div class="input-group" style="width: 100px;">
                                       <%=smartHF.getHTMLVarExt(fw, sv.billfreq10)%>
                                               
                                
                         </div>
                         <%
                                } else {
                         %>
                         <div class="input-group" style="width: 100px;">
                                <%=smartHF.getRichTextInputFieldLookup(fw, sv.billfreq10)%>
                                <span class="input-group-btn">
                                       <button class="btn btn-info"
                                             style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                              type="button"
                                              onClick="doFocus(document.getElementById('billfreq10')); doAction('PFKEY04')">
                                              <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                       </button>
                                </span>
                         </div>
                         <%
                                }
                         %>
								
								
								
								
								
								
								
								
			

						<%
							/* } */
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.cntfee10).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cntfee10,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cntfee10' type='text'
							<%if ((sv.cntfee10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cntfee10.getLength(), sv.cntfee10.getScale(), 3)%>'
							maxLength='<%=sv.cntfee10.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cntfee10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cntfee10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {  %>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cntfee10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cntfee10).getColor() == null ? "input_cell"
						: (sv.cntfee10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

<style>
div[id*='cntfee'] {
	padding-right: 5px !important
}
</style>