<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5g0";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>


<%
	Sd5g0ScreenVars sv = (Sd5g0ScreenVars) fw.getVariables();
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%
	{

		 if (appVars.ind02.isOn()) {
			sv.salelicensetype.setReverse(BaseScreenData.REVERSED);
			sv.salelicensetype.setColor(BaseScreenData.RED);
		} 
		
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText1)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText2)%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText3)%></label>
				<table><tr><td>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%></td><td style="padding-left:1px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%></td></tr></table>
					
				</div>
			</div>
		</div>
		 <div class="row">
		 <div class="col-md-6">
        			<div class="form-group">
        				<label><%=generatedText4%></label>
        				<table><tr><td>     				
        				<%if ((new Byte((sv.itmfrmDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>	
							<%					
							if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
					  	</td><td>
					  	<!-- //to -->				  	
					  	<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						&nbsp;
						<label><%=generatedText5%></label>
						&nbsp;
						<%}%>	  	
					  	<!-- end to -->
					  	</td><td>
					  	
						<%if ((new Byte((sv.itmtoDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<%					
							if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%></td></tr>
					  </table>
        			</div>
        		</div>
		</div>  
		<div class="row">
		
			<div class="col-md-3"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Sale License Type")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"salelicensetype"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("salelicensetype");
						optionValue = makeDropDownList( mappedItems , sv.salelicensetype.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.salelicensetype.getFormData()).toString().trim());
						%>
						
						<%
							if((new Byte((sv.salelicensetype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell'>
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
						
						<% if("red".equals((sv.salelicensetype).getColor())){
											%>
											<div 
											style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;">
											<%
											}
											%>
											<select name='salelicensetype' type='list' 
											<%
										if((new Byte((sv.salelicensetype).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
									%>
										readonly="true"
										<%-- MIBT-38 --%>
										disabled  onclick="document.getElementById('salelicensetypeDisp').value='';"
										class="output_cell"
									<%
										}else if((new Byte((sv.salelicensetype).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>
											<%-- MIBT-38 --%>
											class="bold_cell"  onclick="document.getElementById('salelicensetypeDisp').value='';"
									<%
										}else {
									%>
									<%-- MIBT-38 --%>
									class = 'input_cell' onclick="document.getElementById('salelicensetypeDisp').value='';"
									<%
										}
									%>
									>
											<%=optionValue%>
											</select>
											<% if("red".equals((sv.salelicensetype).getColor())){
											%>
											</div>
											<%
											}
											%>
											<%
							}
						%>
				    </div>
				 </div>
			
			<div class="col-md-3">
	        		<div class="form-group"> 
	        			<label style="position:relative;text-align: right;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent Bank"))%></label>
	        			<br><%=smartHF.getHTMLCheckBoxYN(sv.agentbank, "agentbank", fw)%>
	        		</div>
	        </div>
	        <div class="col-md-3">
	        		<div class="form-group"> 
	        			<label style="position:relative;text-align: right;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Teller"))%></label>
	        			<br><%=smartHF.getHTMLCheckBoxYN(sv.teller, "agentbank", fw)%>
	        		</div>
	        </div>
	        <div class="col-md-3">
	        		<div class="form-group"> 
	        			<label style="position:relative;text-align: right;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Manager"))%></label>
	        			<br><%=smartHF.getHTMLCheckBoxYN(sv.bankmang, "bankmang", fw)%>
	        		</div>
	        </div>
		</div>
		
		
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<script>
$(document).ready(function(){
$('#functiontype').css("width","70px");
});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

