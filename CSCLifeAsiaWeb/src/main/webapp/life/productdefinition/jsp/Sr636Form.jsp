

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR636";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr636ScreenVars sv = (Sr636ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind10.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind11.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Order by ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"1 - Provider Code");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"2 - Provider Name");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Starting ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Filter   ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Area Code ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Med Type ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sex of Doctor ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Status ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1-Select");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sel");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Code");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Name");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Address");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.sel01.setReverse(BaseScreenData.REVERSED);
			sv.sel01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.sel01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.search01.setReverse(BaseScreenData.REVERSED);
			sv.search01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.search01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zaracde01.setReverse(BaseScreenData.REVERSED);
			sv.zaracde01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zaracde01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.sex01.setReverse(BaseScreenData.REVERSED);
			sv.sex01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.sex01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.zmedsts01.setReverse(BaseScreenData.REVERSED);
			sv.zmedsts01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.zmedsts01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.zmedtyp01.setReverse(BaseScreenData.REVERSED);
			sv.zmedtyp01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.zmedtyp01.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Order by")%></label>
					<div>
						<%
							if ((new Byte((sv.sel01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.sel01.getFormData()).toString()).trim().equalsIgnoreCase("1")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Provider Code");
								}
								if (((sv.sel01.getFormData()).toString()).trim().equalsIgnoreCase("2")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Provider Name");
								}
						%>

						<%
							if ((new Byte((sv.sel01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.sel01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='sel01' style="width: 140px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(sel01)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.sel01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.sel01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="1"
									<%if (((sv.sel01.getFormData()).toString()).trim().equalsIgnoreCase("1")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></option>
								<option value="2"
									<%if (((sv.sel01.getFormData()).toString()).trim().equalsIgnoreCase("2")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Provider Name")%></option>


							</select>
							<%
								if ("red".equals((sv.sel01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Starting")%></label>
					<div>
						<input name='search01' type='text'
							<%formatValue = (sv.search01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.search01.getLength()%>'
							maxLength='<%=sv.search01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(search01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.search01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.search01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.search01).getColor() == null
						? "input_cell"
						: (sv.search01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="panel panel-default">
			<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Filter")%>
         </div>

    	<div class="panel-body">     
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
							<div>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"zaracde01"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("zaracde01");
									optionValue = makeDropDownList(mappedItems, sv.zaracde01.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.zaracde01.getFormData()).toString().trim());
								%>

								<%
									if ((new Byte((sv.zaracde01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div>

								<%
									longValue = null;
								%>

								<%
									} else {
								%>

								<%
									if ("red".equals((sv.zaracde01).getColor())) {
								%>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
									<%
										}
									%>

									<select name='zaracde01' type='list' style="width: 170px;"
										<%if ((new Byte((sv.zaracde01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.zaracde01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.zaracde01).getColor())) {
									%>
								</div>
								<%
									}
								%>

								<%
									}
								%>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Med Type")%></label>
							<div>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"zmedtyp01"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("zmedtyp01");
									optionValue = makeDropDownList(mappedItems, sv.zmedtyp01.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.zmedtyp01.getFormData()).toString().trim());
								%>

								<%
									if ((new Byte((sv.zmedtyp01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div>

								<%
									longValue = null;
								%>

								<%
									} else {
								%>

								<%
									if ("red".equals((sv.zmedtyp01).getColor())) {
								%>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
									<%
										}
									%>

									<select name='zmedtyp01' type='list' style="width: 170px;"
										<%if ((new Byte((sv.zmedtyp01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.zmedtyp01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.zmedtyp01).getColor())) {
									%>
								</div>
								<%
									}
								%>

								<%
									}
								%>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sex of Doctor")%></label>
							<div>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"sex01"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("sex01");
									optionValue = makeDropDownList(mappedItems, sv.sex01.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.sex01.getFormData()).toString().trim());
								%>

								<%
									if ((new Byte((sv.sex01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div>

								<%
									longValue = null;
								%>

								<%
									} else {
								%>

								<%
									if ("red".equals((sv.sex01).getColor())) {
								%>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
									<%
										}
									%>

									<select name='sex01' type='list' style="width: 170px;"
										<%if ((new Byte((sv.sex01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.sex01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.sex01).getColor())) {
									%>
								</div>
								<%
									}
								%>

								<%
									}
								%>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
							<div>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"zmedsts01"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("zmedsts01");
									optionValue = makeDropDownList(mappedItems, sv.zmedsts01.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.zmedsts01.getFormData()).toString().trim());
								%>

								<%
									if ((new Byte((sv.zmedsts01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div>

								<%
									longValue = null;
								%>

								<%
									} else {
								%>

								<%
									if ("red".equals((sv.zmedsts01).getColor())) {
								%>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
									<%
										}
									%>

									<select name='zmedsts01' type='list' style="width: 170px;"
										<%if ((new Byte((sv.zmedsts01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.zmedsts01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.zmedsts01).getColor())) {
									%>
								</div>
								<%
									}
								%>

								<%
									}
								%>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Close div panel-body filer -->
		</div>
		<!-- Close div panel panel-default filer-->
	</div>
	<%
		/* This block of jsp code is to calculate the variable width of the table at runtime.*/
		int[] tblColumnWidth = new int[4];
		int totalTblWidth = 0;
		int calculatedValue = 0;

		if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData())
				.length()) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
		} else {
			calculatedValue = (sv.select.getFormData()).length() * 12;
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[0] = calculatedValue;

		if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.zmedprv.getFormData())
				.length()) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
		} else {
			calculatedValue = (sv.zmedprv.getFormData()).length() * 12;
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[1] = calculatedValue;

		if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.givname.getFormData())
				.length()) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
		} else {
			calculatedValue = (sv.givname.getFormData()).length() * 12;
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[2] = calculatedValue;

		if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.addrss.getFormData())
				.length()) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
		} else {
			calculatedValue = (sv.addrss.getFormData()).length() * 12;
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[3] = calculatedValue;
	%>
	<%
		GeneralTable sfl = fw.getTable("sr636screensfl");
		int height;
		if (sfl.count() * 27 > 210) {
			height = 210;
		} else {
			height = sfl.count() * 27;
		}
	%>
	<div class="row" style="margin-left: 3px;">
		<div class="col-md-12">
			<div class="form-group">
				<div id="load-more" class="col-md-offset-10">
					<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
						style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
					</a>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr636' width='100%'>
						<thead>
							<tr class='info'>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Provider Name")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Address")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								Sr636screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sr636screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<td class="tableDataTag tableDataTagFixed"
									style="width:<%=tblColumnWidth[0] - 60%>px;" align="left">



									<input type="radio" onFocus='doFocus(this)'
									onHelp='return fieldHelp("sr636screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='sr636screensfl.select_R<%=count%>'
									id='sr636screensfl.select_R<%=count%>'
									onClick="selectedRow('sr636screensfl.select_R<%=count%>')"
									class="radio" />




								</td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[1] - 50%>px;" align="left">



									<%=sv.zmedprv.getFormData()%>



								</td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[2]%>px;" align="left"><%=sv.givname.getFormData()%>



								</td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[3]%>px;" align="left"><%=sv.addrss.getFormData()%>



								</td>

							</tr>

							<%
								count = count + 1;
									Sr636screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<script>
	$(document).ready(function() {
		$('#dataTables-sr636').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script type="text/javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>
<%
	}
%>
