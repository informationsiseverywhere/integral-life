

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SJ699";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sj699ScreenVars sv = (Sj699ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Dates effective     ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Modal Factor");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.freqcy01.setReverse(BaseScreenData.REVERSED);
			sv.freqcy01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.freqcy01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.freqcy02.setReverse(BaseScreenData.REVERSED);
			sv.freqcy02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.freqcy02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.freqcy03.setReverse(BaseScreenData.REVERSED);
			sv.freqcy03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.freqcy03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.freqcy05.setReverse(BaseScreenData.REVERSED);
			sv.freqcy05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.freqcy05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.freqcy04.setReverse(BaseScreenData.REVERSED);
			sv.freqcy04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.freqcy04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.freqcy07.setReverse(BaseScreenData.REVERSED);
			sv.freqcy07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.freqcy07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.freqcy06.setReverse(BaseScreenData.REVERSED);
			sv.freqcy06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.freqcy06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.freqcy08.setReverse(BaseScreenData.REVERSED);
			sv.freqcy08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.freqcy08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.freqcy09.setReverse(BaseScreenData.REVERSED);
			sv.freqcy09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.freqcy09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.freqcy10.setReverse(BaseScreenData.REVERSED);
			sv.freqcy10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.freqcy10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.freqcy11.setReverse(BaseScreenData.REVERSED);
			sv.freqcy11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.freqcy11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.freqcy12.setReverse(BaseScreenData.REVERSED);
			sv.freqcy12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.freqcy12.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6  starts -->
<!-- <style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}

.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style> -->
<!-- ILIFE-2570 Life Cross Browser -Coding and UT- Sprint 2 D2: Task 6  ends -->
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>

					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>

					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					
<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td><label style="font-weight: bolder; font-size: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy01");
						optionValue = makeDropDownList(mappedItems, sv.freqcy01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy01.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy01).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy01' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy01).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy02");
						optionValue = makeDropDownList(mappedItems, sv.freqcy02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy02.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy02).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy01' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy02).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy03" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy03");
						optionValue = makeDropDownList(mappedItems, sv.freqcy03.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy03.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy03).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy03' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy03).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy04" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy04");
						optionValue = makeDropDownList(mappedItems, sv.freqcy04.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy04.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy04).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy04' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy04).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy05" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy05");
						optionValue = makeDropDownList(mappedItems, sv.freqcy05.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy04.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy05).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy01' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy05).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy06" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy06");
						optionValue = makeDropDownList(mappedItems, sv.freqcy06.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy06.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy06).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy06' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy06).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy07" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy07");
						optionValue = makeDropDownList(mappedItems, sv.freqcy07.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy07.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy07).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy07' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy07).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy08" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy08");
						optionValue = makeDropDownList(mappedItems, sv.freqcy08.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy08.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy08).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy08' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy08).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy09" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy09");
						optionValue = makeDropDownList(mappedItems, sv.freqcy09.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy09.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy09).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy09' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy09).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy10" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy10");
						optionValue = makeDropDownList(mappedItems, sv.freqcy10.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy10.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy10).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy10' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy10).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy11" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy11");
						optionValue = makeDropDownList(mappedItems, sv.freqcy11.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy11.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy11).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy11' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy11).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%><br>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy12" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy12");
						optionValue = makeDropDownList(mappedItems, sv.freqcy12.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy12.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.freqcy12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.freqcy12).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
						<%
							}
						%>
						<select name='freqcy12' type='list' style="width: 150px;"
							<%if ((new Byte((sv.freqcy12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy12).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Modal Factor")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact01' type='text'
							<%if ((sv.zlfact01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact01)%>' <%}%>
							size='<%=sv.zlfact01.getLength()%>'
							maxLength='<%=sv.zlfact01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact01).getColor() == null ? "input_cell"
						: (sv.zlfact01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact02' type='text'
							<%if ((sv.zlfact02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact02)%>' <%}%>
							size='<%=sv.zlfact02.getLength()%>'
							maxLength='<%=sv.zlfact02.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact02).getColor() == null ? "input_cell"
						: (sv.zlfact02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact03' type='text'
							<%if ((sv.zlfact03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact03)%>' <%}%>
							size='<%=sv.zlfact03.getLength()%>'
							maxLength='<%=sv.zlfact03.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact03).getColor() == null ? "input_cell"
						: (sv.zlfact03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact04' type='text'
							<%if ((sv.zlfact04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact04)%>' <%}%>
							size='<%=sv.zlfact04.getLength()%>'
							maxLength='<%=sv.zlfact04.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact04).getColor() == null ? "input_cell"
						: (sv.zlfact04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact05' type='text'
							<%if ((sv.zlfact05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact05)%>' <%}%>
							size='<%=sv.zlfact05.getLength()%>'
							maxLength='<%=sv.zlfact05.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact05).getColor() == null ? "input_cell"
						: (sv.zlfact05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact06' type='text'
							<%if ((sv.zlfact06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact06)%>' <%}%>
							size='<%=sv.zlfact06.getLength()%>'
							maxLength='<%=sv.zlfact06.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact06).getColor() == null ? "input_cell"
						: (sv.zlfact06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact07' type='text'
							<%if ((sv.zlfact07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact07)%>' <%}%>
							size='<%=sv.zlfact07.getLength()%>'
							maxLength='<%=sv.zlfact07.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact07).getColor() == null ? "input_cell"
						: (sv.zlfact07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact08' type='text'
							<%if ((sv.zlfact08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact08)%>' <%}%>
							size='<%=sv.zlfact08.getLength()%>'
							maxLength='<%=sv.zlfact08.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact08).getColor() == null ? "input_cell"
						: (sv.zlfact08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact09' type='text'
							<%if ((sv.zlfact09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact09)%>' <%}%>
							size='<%=sv.zlfact09.getLength()%>'
							maxLength='<%=sv.zlfact09.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact09).getColor() == null ? "input_cell"
						: (sv.zlfact09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact10' type='text'
							<%if ((sv.zlfact10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact10)%>' <%}%>
							size='<%=sv.zlfact10.getLength()%>'
							maxLength='<%=sv.zlfact10.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact10).getColor() == null ? "input_cell"
						: (sv.zlfact10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact11).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact11' type='text'
							<%if ((sv.zlfact11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact11)%>' <%}%>
							size='<%=sv.zlfact11.getLength()%>'
							maxLength='<%=sv.zlfact11.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact11).getColor() == null ? "input_cell"
						: (sv.zlfact11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zlfact12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='zlfact12' type='text'
							<%if ((sv.zlfact12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zlfact12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zlfact12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zlfact12)%>' <%}%>
							size='<%=sv.zlfact12.getLength()%>'
							maxLength='<%=sv.zlfact12.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zlfact12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zlfact12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zlfact12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zlfact12).getColor() == null ? "input_cell"
						: (sv.zlfact12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
					<br>
				</div>
			</div>
		</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

