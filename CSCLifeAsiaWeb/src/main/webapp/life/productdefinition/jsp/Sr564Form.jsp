<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR564";%>
<%@ include file="/POLACommon1NEW.jsp"%>

<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%Sr564ScreenVars sv = (Sr564ScreenVars) fw.getVariables();%>
<%
		GeneralTable sfl = fw.getTable("sr564screensfl");
		
		%>






<div class="panel panel-default" style='height: 750px;'>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract No"))%></label>
					
					<%=smartHF.getHTMLVarExt(fw, sv.chdrnum)%>
				</div>
			</div>



			<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<!-- <div class="input-group"> -->
						<table><tr><td>
							<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
		<!-- </div> -->
		</div>
		</div>

</div>




<div class="row">
<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life No"))%></label>
						<%=smartHF.getHTMLVarExt(fw, sv.life)%>
				</div>
			</div>


<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage No"))%></label>
						<%=smartHF.getHTMLVarExt(fw, sv.coverage)%>
				</div>
			</div>
			
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rider No"))%></label>
						<%=smartHF.getHTMLVarExt(fw, sv.rider)%>
				</div>
			</div>


	<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider")%></label>
						<table>
						<tr>
						<td>
						<%					
		if(!((sv.crcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td style="max-width:135px;">
		
		<%					
		if(!((sv.coverDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
		</div>
		</div>
</div>

<%StringData generatedText11 = new StringData("Reducing Frequency");%>
	<%sv.redfreq.setClassString("");%>
<%	sv.redfreq.appendClassString("string_fld");
	sv.redfreq.appendClassString("output_txt");
	sv.redfreq.appendClassString("highlight");
%>




<div class="row">
<div class="col-md-3">
				<div class="form-group">

<% if (sv.redfreq.compareTo("N") != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Reducing Frequency")%></label>
<div class="input-group" style="min-width:120px;">


<%  fieldItem=(Map)(appVars.loadF4(baseModel, "redfreq")); 
	//ILIFE-5319 
	if(fieldItem !=null && !fieldItem.isEmpty()){
		mappedItems = (Map) fieldItem.get("redfreq");
		optionValue = makeDropDownList( mappedItems , sv.redfreq.getFormData(),2,resourceBundleHandler);  
		longValue = (String) mappedItems.get((sv.redfreq.getFormData()).toString().trim());  
	}else{
		longValue = " ";
	}
	//ILIFE-5319 
%>
<% 
	if((new Byte((sv.redfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  			
  <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' >  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.redfreq).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px; padding-left: 25px"> 
<%
} 
%>

<select name='mlresindvpms' type='list' style="width:140px;"
<% 
	if((new Byte((sv.redfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.redfreq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.redfreq).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%}
%>
<%--ILIFE-3714 ends by slakkala --%>

  
		</div> 						
			</div></div>



</div>





		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-sr564' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align:center; width:100px">
									<!--ILIFE-7521-->
									<%if (sv.policyType.compareTo("N") == 0){%>
										<%=resourceBundleHandler.gettingValueFromBundle("Policy Year")%></th>
									<%}else{ %>
										<%=resourceBundleHandler.gettingValueFromBundle("Policy Month")%></th>
										<%} %>
									<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></th>
									<th style="text-align:center; width:250px"><%=resourceBundleHandler.gettingValueFromBundle("CSV")%></th>
									

								</tr>
							</thead>
							<tbody>
								<%
	Sr564screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	double sflLine = 0.1;
	smartHF.setSflLineOffset(9);
	while (Sr564screensfl
	.hasMoreScreenRows(sfl)) {	
%>

								<tr height="27" class="tableRowTag" id='<%="tablerow"+count%>' >
		<%-- <%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	 	<div style='display:none; visiblity:hidden;'>
		 	<input type='text' maxLength='<%=sv.select.getLength()%>' value='<%= sv.select.getFormData() %>' size='<%=sv.select.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(s0026screensfl.select)' onKeyUp='return checkMaxLength(this)' name='<%="s0026screensfl" + "." +
			"select" + "_R" + count %>' id='<%="s0026screensfl" + "." + "select" + "_R" + count %>' class = "input_cell"
			style = "width: <%=sv.select.getLength()*12%> px;" >
	 	</div>
						 						 
<%}%> --%>
		
		
		<td style="text-align:right"><div style="width:60px"><%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.yrsinf, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%></div></td>
				    		<td style="text-align:right"><%=smartHF.getTableHTMLVarQual(sflLine, 10, sfl, sv.sumins, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
				    		<td style="text-align:right"><div class="input-group" style="min-width:270px"><%=smartHF.getTableHTMLVarQual(sflLine, 36.5, sfl, sv.surrval, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
					
	<%
	count = count + 1;
	Sr564screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


							</tbody>

						</table>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>	
		
<!-- 		<script>
	$(document).ready(function() {
    	$('#dataTables-sr564').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: false,
			scrollCollapse: true,
      	});
    });
</script> -->
<!-- ILIFE-3049 ends by pmujavadiya -->
	

<script>
$(document).ready(function() {
	$('#dataTables-sr564').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
  	});
})
</script>




<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2582 Life Cross Browser -Coding and UT- Sprint 2 D3: Task 6  starts  -->
	<style>
	
		
		div[id*='sumins']{padding-right:1px !important}
		div[id*='yrsinf']{padding-right:1px !important}
		
	 </style> 