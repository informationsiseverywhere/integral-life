<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5679";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S5679ScreenVars sv = (S5679ScreenVars) fw.getVariables();
%>
<%
	if (sv.S5679screenWritten.gt(0)) {
%>
<%
	S5679screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>


<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Status Item");
%>
<!--<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Current Status Code");
%>
<!--<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Set Status to");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premium");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				" Regular Single");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract Risk");
%>
<%
	sv.cnRiskStat01.setClassString("");
%>
<%
	sv.cnRiskStat02.setClassString("");
%>
<%
	sv.cnRiskStat03.setClassString("");
%>
<%
	sv.cnRiskStat04.setClassString("");
%>
<%
	sv.cnRiskStat05.setClassString("");
%>
<%
	sv.cnRiskStat06.setClassString("");
%>
<%
	sv.cnRiskStat07.setClassString("");
%>
<%
	sv.cnRiskStat08.setClassString("");
%>
<%
	sv.cnRiskStat09.setClassString("");
%>
<%
	sv.cnRiskStat10.setClassString("");
%>
<%
	sv.cnRiskStat11.setClassString("");
%>
<%
	sv.cnRiskStat12.setClassString("");
%>
<%
	sv.setCnRiskStat.setClassString("");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract Premium");
%>
<%
	sv.cnPremStat01.setClassString("");
%>
<%
	sv.cnPremStat02.setClassString("");
%>
<%
	sv.cnPremStat03.setClassString("");
%>
<%
	sv.cnPremStat04.setClassString("");
%>
<%
	sv.cnPremStat05.setClassString("");
%>
<%
	sv.cnPremStat06.setClassString("");
%>
<%
	sv.cnPremStat07.setClassString("");
%>
<%
	sv.cnPremStat08.setClassString("");
%>
<%
	sv.cnPremStat09.setClassString("");
%>
<%
	sv.cnPremStat10.setClassString("");
%>
<%
	sv.cnPremStat11.setClassString("");
%>
<%
	sv.cnPremStat12.setClassString("");
%>
<%
	sv.setCnPremStat.setClassString("");
%>
<%
	sv.setSngpCnStat.setClassString("");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	sv.lifeStat01.setClassString("");
%>
<%
	sv.lifeStat02.setClassString("");
%>
<%
	sv.lifeStat03.setClassString("");
%>
<%
	sv.lifeStat04.setClassString("");
%>
<%
	sv.lifeStat05.setClassString("");
%>
<%
	sv.lifeStat06.setClassString("");
%>
<%
	sv.setLifeStat.setClassString("");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Joint Life");
%>
<!--<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	sv.jlifeStat01.setClassString("");
%>
<%
	sv.jlifeStat02.setClassString("");
%>
<%
	sv.jlifeStat03.setClassString("");
%>
<%
	sv.jlifeStat04.setClassString("");
%>
<%
	sv.jlifeStat05.setClassString("");
%>
<%
	sv.jlifeStat06.setClassString("");
%>
<%
	sv.setJlifeStat.setClassString("");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Coverage Risk");
%><!--
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	sv.covRiskStat01.setClassString("");                                                                 
%>
<%
	sv.covRiskStat02.setClassString("");
%>
<%
	sv.covRiskStat03.setClassString("");
%>
<%
	sv.covRiskStat04.setClassString("");
%>
<%
	sv.covRiskStat05.setClassString("");
%>
<%
	sv.covRiskStat06.setClassString("");
%>
<%
	sv.covRiskStat07.setClassString("");
%>
<%
	sv.covRiskStat08.setClassString("");
%>
<%
	sv.covRiskStat09.setClassString("");
%>
<%
	sv.covRiskStat10.setClassString("");
%>
<%
	sv.covRiskStat11.setClassString("");
%>
<%
	sv.covRiskStat12.setClassString("");
%>
<%
	sv.setCovRiskStat.setClassString("");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Coverage Premium");
%>
<!--<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	sv.covPremStat01.setClassString("");
%>
<%
	sv.covPremStat02.setClassString("");
%>
<%
	sv.covPremStat03.setClassString("");
%>
<%
	sv.covPremStat04.setClassString("");
%>
<%
	sv.covPremStat05.setClassString("");
%>
<%
	sv.covPremStat06.setClassString("");
%>
<%
	sv.covPremStat07.setClassString("");
%>
<%
	sv.covPremStat08.setClassString("");
%>
<%
	sv.covPremStat09.setClassString("");
%>
<%
	sv.covPremStat10.setClassString("");
%>
<%
	sv.covPremStat11.setClassString("");
%>
<%
	sv.covPremStat12.setClassString("");
%>
<%
	sv.setCovPremStat.setClassString("");
%>
<%
	sv.setSngpCovStat.setClassString("");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Rider Risk");
%>
<!--<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	sv.ridRiskStat01.setClassString("");
%>
<%
	sv.ridRiskStat02.setClassString("");
%>
<%
	sv.ridRiskStat03.setClassString("");
%>
<%
	sv.ridRiskStat04.setClassString("");
%>
<%
	sv.ridRiskStat05.setClassString("");
%>
<%
	sv.ridRiskStat06.setClassString("");
%>
<%
	sv.ridRiskStat07.setClassString("");
%>
<%
	sv.ridRiskStat08.setClassString("");
%>
<%
	sv.ridRiskStat09.setClassString("");
%>
<%
	sv.ridRiskStat10.setClassString("");
%>
<%
	sv.ridRiskStat11.setClassString("");
%>
<%
	sv.ridRiskStat12.setClassString("");
%>
<%
	sv.setRidRiskStat.setClassString("");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Rider Premium");
%>
<!--<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "");%>
	-->
<%
	sv.ridPremStat01.setClassString("");
%>
<%
	sv.ridPremStat02.setClassString("");
%>
<%
	sv.ridPremStat03.setClassString("");
%>
<%
	sv.ridPremStat04.setClassString("");
%>
<%
	sv.ridPremStat05.setClassString("");
%>
<%
	sv.ridPremStat06.setClassString("");
%>
<%
	sv.ridPremStat07.setClassString("");
%>
<%
	sv.ridPremStat08.setClassString("");
%>
<%
	sv.ridPremStat09.setClassString("");
%>
<%
	sv.ridPremStat10.setClassString("");
%>
<%
	sv.ridPremStat11.setClassString("");
%>
<%
	sv.ridPremStat12.setClassString("");
%>
<%
	sv.setRidPremStat.setClassString("");
%>
<%
	sv.setSngpRidStat.setClassString("");
%>

<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind09.isOn()) {
				sv.cnRiskStat01.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.cnRiskStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.cnRiskStat02.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.cnRiskStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.cnRiskStat03.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.cnRiskStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.cnRiskStat04.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.cnRiskStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.cnRiskStat05.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.cnRiskStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.cnRiskStat06.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.cnRiskStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.cnRiskStat07.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.cnRiskStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.cnRiskStat08.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.cnRiskStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.cnRiskStat09.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.cnRiskStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.cnRiskStat10.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.cnRiskStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.cnRiskStat11.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.cnRiskStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.cnRiskStat12.setReverse(BaseScreenData.REVERSED);
				sv.cnRiskStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
				sv.cnRiskStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind01.isOn()) {
				sv.setCnRiskStat.setReverse(BaseScreenData.REVERSED);
				sv.setCnRiskStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.setCnRiskStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.cnPremStat01.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.cnPremStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.cnPremStat02.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.cnPremStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.cnPremStat03.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.cnPremStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.cnPremStat04.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.cnPremStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.cnPremStat05.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.cnPremStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.cnPremStat06.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.cnPremStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.cnPremStat07.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.cnPremStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.cnPremStat08.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.cnPremStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.cnPremStat09.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.cnPremStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.cnPremStat10.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.cnPremStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.cnPremStat11.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.cnPremStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.cnPremStat12.setReverse(BaseScreenData.REVERSED);
				sv.cnPremStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.cnPremStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.setCnPremStat.setReverse(BaseScreenData.REVERSED);
				sv.setCnPremStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.setCnPremStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.setSngpCnStat.setReverse(BaseScreenData.REVERSED);
				sv.setSngpCnStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
				sv.setSngpCnStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.lifeStat01.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.lifeStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.lifeStat02.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.lifeStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.lifeStat03.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.lifeStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind36.isOn()) {
				sv.lifeStat04.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
				sv.lifeStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind37.isOn()) {
				sv.lifeStat05.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind37.isOn()) {
				sv.lifeStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
				sv.lifeStat06.setReverse(BaseScreenData.REVERSED);
				sv.lifeStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind38.isOn()) {
				sv.lifeStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.setLifeStat.setReverse(BaseScreenData.REVERSED);
				sv.setLifeStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.setLifeStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind39.isOn()) {
				sv.jlifeStat01.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind39.isOn()) {
				sv.jlifeStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind40.isOn()) {
				sv.jlifeStat02.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind40.isOn()) {
				sv.jlifeStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind41.isOn()) {
				sv.jlifeStat03.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind41.isOn()) {
				sv.jlifeStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind42.isOn()) {
				sv.jlifeStat04.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind42.isOn()) {
				sv.jlifeStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind43.isOn()) {
				sv.jlifeStat05.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind43.isOn()) {
				sv.jlifeStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind44.isOn()) {
				sv.jlifeStat06.setReverse(BaseScreenData.REVERSED);
				sv.jlifeStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind44.isOn()) {
				sv.jlifeStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.setJlifeStat.setReverse(BaseScreenData.REVERSED);
				sv.setJlifeStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.setJlifeStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind45.isOn()) {
				sv.covRiskStat01.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind45.isOn()) {
				sv.covRiskStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind46.isOn()) {
				sv.covRiskStat02.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind46.isOn()) {
				sv.covRiskStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind47.isOn()) {
				sv.covRiskStat03.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind47.isOn()) {
				sv.covRiskStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind48.isOn()) {
				sv.covRiskStat04.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind48.isOn()) {
				sv.covRiskStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind49.isOn()) {
				sv.covRiskStat05.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind49.isOn()) {
				sv.covRiskStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind50.isOn()) {
				sv.covRiskStat06.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind50.isOn()) {
				sv.covRiskStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind51.isOn()) {
				sv.covRiskStat07.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind51.isOn()) {
				sv.covRiskStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind62.isOn()) {
				sv.covRiskStat08.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind62.isOn()) {
				sv.covRiskStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind52.isOn()) {
				sv.covRiskStat09.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind52.isOn()) {
				sv.covRiskStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind53.isOn()) {
				sv.covRiskStat10.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind53.isOn()) {
				sv.covRiskStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind54.isOn()) {
				sv.covRiskStat11.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind54.isOn()) {
				sv.covRiskStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind55.isOn()) {
				sv.covRiskStat12.setReverse(BaseScreenData.REVERSED);
				sv.covRiskStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind55.isOn()) {
				sv.covRiskStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.setCovRiskStat.setReverse(BaseScreenData.REVERSED);
				sv.setCovRiskStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.setCovRiskStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind56.isOn()) {
				sv.covPremStat01.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind56.isOn()) {
				sv.covPremStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind57.isOn()) {
				sv.covPremStat02.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind57.isOn()) {
				sv.covPremStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind58.isOn()) {
				sv.covPremStat03.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind58.isOn()) {
				sv.covPremStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind59.isOn()) {
				sv.covPremStat04.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind59.isOn()) {
				sv.covPremStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind60.isOn()) {
				sv.covPremStat05.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind60.isOn()) {
				sv.covPremStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind61.isOn()) {
				sv.covPremStat06.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind61.isOn()) {
				sv.covPremStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind63.isOn()) {
				sv.covPremStat07.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind63.isOn()) {
				sv.covPremStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind64.isOn()) {
				sv.covPremStat08.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind64.isOn()) {
				sv.covPremStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind65.isOn()) {
				sv.covPremStat09.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind65.isOn()) {
				sv.covPremStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind66.isOn()) {
				sv.covPremStat10.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind66.isOn()) {
				sv.covPremStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind67.isOn()) {
				sv.covPremStat11.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind67.isOn()) {
				sv.covPremStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind68.isOn()) {
				sv.covPremStat12.setReverse(BaseScreenData.REVERSED);
				sv.covPremStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind68.isOn()) {
				sv.covPremStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.setCovPremStat.setReverse(BaseScreenData.REVERSED);
				sv.setCovPremStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.setCovPremStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.setSngpCovStat.setReverse(BaseScreenData.REVERSED);
				sv.setSngpCovStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind19.isOn()) {
				sv.setSngpCovStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind69.isOn()) {
				sv.ridRiskStat01.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind69.isOn()) {
				sv.ridRiskStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind70.isOn()) {
				sv.ridRiskStat02.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind70.isOn()) {
				sv.ridRiskStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind71.isOn()) {
				sv.ridRiskStat03.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind71.isOn()) {
				sv.ridRiskStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind72.isOn()) {
				sv.ridRiskStat04.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind72.isOn()) {
				sv.ridRiskStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind73.isOn()) {
				sv.ridRiskStat05.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind73.isOn()) {
				sv.ridRiskStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind74.isOn()) {
				sv.ridRiskStat06.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind74.isOn()) {
				sv.ridRiskStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind75.isOn()) {
				sv.ridRiskStat07.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind75.isOn()) {
				sv.ridRiskStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind76.isOn()) {
				sv.ridRiskStat08.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind76.isOn()) {
				sv.ridRiskStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind77.isOn()) {
				sv.ridRiskStat09.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind77.isOn()) {
				sv.ridRiskStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind78.isOn()) {
				sv.ridRiskStat10.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind78.isOn()) {
				sv.ridRiskStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind79.isOn()) {
				sv.ridRiskStat11.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind79.isOn()) {
				sv.ridRiskStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind80.isOn()) {
				sv.ridRiskStat12.setReverse(BaseScreenData.REVERSED);
				sv.ridRiskStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind80.isOn()) {
				sv.ridRiskStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.setRidRiskStat.setReverse(BaseScreenData.REVERSED);
				sv.setRidRiskStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.setRidRiskStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind81.isOn()) {
				sv.ridPremStat01.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind81.isOn()) {
				sv.ridPremStat01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind82.isOn()) {
				sv.ridPremStat02.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind82.isOn()) {
				sv.ridPremStat02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind83.isOn()) {
				sv.ridPremStat03.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind83.isOn()) {
				sv.ridPremStat03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind84.isOn()) {
				sv.ridPremStat04.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind84.isOn()) {
				sv.ridPremStat04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind85.isOn()) {
				sv.ridPremStat05.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind85.isOn()) {
				sv.ridPremStat05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind86.isOn()) {
				sv.ridPremStat06.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind86.isOn()) {
				sv.ridPremStat06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind87.isOn()) {
				sv.ridPremStat07.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind87.isOn()) {
				sv.ridPremStat07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind88.isOn()) {
				sv.ridPremStat08.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind88.isOn()) {
				sv.ridPremStat08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind89.isOn()) {
				sv.ridPremStat09.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind89.isOn()) {
				sv.ridPremStat09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind89.isOn()) {
				sv.ridPremStat10.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind89.isOn()) {
				sv.ridPremStat10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind89.isOn()) {
				sv.ridPremStat11.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind89.isOn()) {
				sv.ridPremStat11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind89.isOn()) {
				sv.ridPremStat12.setReverse(BaseScreenData.REVERSED);
				sv.ridPremStat12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind89.isOn()) {
				sv.ridPremStat12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.setRidPremStat.setReverse(BaseScreenData.REVERSED);
				sv.setRidPremStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.setRidPremStat.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.setSngpRidStat.setReverse(BaseScreenData.REVERSED);
				sv.setSngpRidStat.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.setSngpRidStat.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 75px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>

							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

						</td>
						<td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5679' width='100%' style="border-right: thin solid #dddddd !important;">
						<thead>
							<tr class='info'>
								<th><center> Status&nbsp;Item</center></th>
								<th colspan="12"><center>Current&nbsp;Status&nbsp;Code</center></th>
								<th colspan="3"><center>Set&nbsp;Status&nbsp;to</center></th>
							</tr>
							<tr class='info'>
								<th></th>
								<th colspan="12"></th>
								<th><center>Risk </center> </th>
								<th colspan="2"><center>Premium</center> </th>
							</tr>
							<tr class='info'>
								<th></th>
								<th colspan="12"></th>
								<th></th>
								<th><center>Regular</center></th>
								<th><center>Single</center></th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td style="padding-top: 15px;">Contract&nbsp;Risk</td>
								<td>
								
								<%
							          if ((new Byte((sv.cnRiskStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat01)%>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %>    
								
								
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.cnRiskStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.cnRiskStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
										
								<%
							          if ((new Byte((sv.cnRiskStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat04)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.cnRiskStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat05)%></div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
										<%
							          if ((new Byte((sv.cnRiskStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat06)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
										<%
							          if ((new Byte((sv.cnRiskStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat07)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								
								
										<%
							          if ((new Byte((sv.cnRiskStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat08)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
								
										<%
							          if ((new Byte((sv.cnRiskStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat09)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
								
										<%
							          if ((new Byte((sv.cnRiskStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat10)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
											<%
							          if ((new Byte((sv.cnRiskStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat11)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
											<%
							          if ((new Byte((sv.cnRiskStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnRiskStat12)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnRiskStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnRiskStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
											<%
							          if ((new Byte((sv.setCnRiskStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setCnRiskStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setCnRiskStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Contract&nbsp;Premium</td>
								<td>
								
								
									
											<%
							          if ((new Byte((sv.cnPremStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat01)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
											<%
							          if ((new Byte((sv.cnPremStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat02)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
											<%
							          if ((new Byte((sv.cnPremStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								
								
											<%
							          if ((new Byte((sv.cnPremStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								
											<%
							          if ((new Byte((sv.cnPremStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat05)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								
									
								</td>
								<td>
								
								
											<%
							          if ((new Byte((sv.cnPremStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat06)%>	</div>                                      
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								
								
											<%
							          if ((new Byte((sv.cnPremStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnPremStat07)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;"> 
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
											<%
							          if ((new Byte((sv.cnPremStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnPremStat08)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
									
											<%
							          if ((new Byte((sv.cnPremStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.cnPremStat09)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
									
											<%
							          if ((new Byte((sv.cnPremStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnPremStat10)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
										<%
							          if ((new Byte((sv.cnPremStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnPremStat11)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										<%
							          if ((new Byte((sv.cnPremStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cnPremStat12)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cnPremStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('cnPremStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								
									
								</td>
								<td></td>
								<td>
								
										<%
							          if ((new Byte((sv.setCnPremStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setCnPremStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setCnPremStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
										<%
							          if ((new Byte((sv.setSngpCnStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.setSngpCnStat)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setSngpCnStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setSngpCnStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								
								</td>
							</tr>

							<tr>
								<td colspan="16"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Life</td>
								<td>							
								
								<%
							          if ((new Byte((sv.lifeStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat01)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
																	
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.lifeStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.lifeStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.lifeStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.lifeStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat05)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>

								<td>
								
								<%
							          if ((new Byte((sv.lifeStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.lifeStat06)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifeStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('lifeStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								
									
								</td>
								<td colspan="6"></td>
								<td>
								
									<%
							          if ((new Byte((sv.setLifeStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setLifeStat)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setLifeStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setLifeStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 								
									
								</td>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Joint&nbsp;Life</td>
								<td>
								
								
									<%
							          if ((new Byte((sv.jlifeStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.jlifeStat01)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.jlifeStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.jlifeStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.jlifeStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.jlifeStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.jlifeStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.jlifeStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								
									<%
							          if ((new Byte((sv.jlifeStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.jlifeStat05)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>

								<td>
								
								
									<%
							          if ((new Byte((sv.jlifeStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.jlifeStat06)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.jlifeStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('jlifeStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td colspan="6"></td>
								<td>
								
									<%
							          if ((new Byte((sv.setJlifeStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setJlifeStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setJlifeStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setJlifeStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
							
								</td>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td colspan="16"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Coverage&nbsp;Risk</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.covRiskStat01)%></div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.covRiskStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.covRiskStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									
									<%
							          if ((new Byte((sv.covRiskStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.covRiskStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
																	
								</td>
								<td>
								
									
									<%
							          if ((new Byte((sv.covRiskStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat05)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat06)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat07)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat08)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat09)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covRiskStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat10)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.covRiskStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat11)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.covRiskStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat12)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covRiskStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covRiskStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.setCovRiskStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.setCovRiskStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setCovRiskStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setCovRiskStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
								
								</td>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Coverage&nbsp;Premium</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.covPremStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat01)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
								
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.covPremStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.covPremStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat03)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covPremStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covPremStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat05)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covPremStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat06)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 	
								
									
								</td>
								<td>
								<%
							          if ((new Byte((sv.covPremStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.covPremStat07)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.covPremStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat08)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.covPremStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat09)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.covPremStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat10)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.covPremStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat11)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.covPremStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.covPremStat12)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.covPremStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('covPremStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td></td>
								<td>
								
								<%
							          if ((new Byte((sv.setCovPremStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setCovPremStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setCovPremStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setCovPremStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.setSngpCovStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setSngpCovStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setSngpCovStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setSngpCovStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
							</tr>

							<tr>
								<td colspan="16"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Rider&nbsp;Risk</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.ridRiskStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat01)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
								
								<%
							          if ((new Byte((sv.ridRiskStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat02)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
								<%
							          if ((new Byte((sv.ridRiskStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat03)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
									
								<%
							          if ((new Byte((sv.ridRiskStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat04)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
								<%
							          if ((new Byte((sv.ridRiskStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat05)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.ridRiskStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat06)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								<%
							          if ((new Byte((sv.ridRiskStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat07)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.ridRiskStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat08)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
									
									<%
							          if ((new Byte((sv.ridRiskStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat09)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridRiskStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat10)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridRiskStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat11)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridRiskStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridRiskStat12)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridRiskStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridRiskStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.setRidRiskStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.setRidRiskStat)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setRidRiskStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setRidRiskStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td style="padding-top: 15px;">Rider&nbsp;Premium</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat01)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat01)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat01')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat02)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat02)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat02')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridPremStat03)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat03)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat03')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat04)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat04)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat04')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridPremStat05)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat05)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat05')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
										
									<%
							          if ((new Byte((sv.ridPremStat06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridPremStat06)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat06)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat06')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
								
											
									<%
							          if ((new Byte((sv.ridPremStat07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat07)%>	 </div>                                        
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat07)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat07')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
											
									<%
							          if ((new Byte((sv.ridPremStat08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat08)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat08)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat08')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
												
									<%
							          if ((new Byte((sv.ridPremStat09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
										<div style="width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.ridPremStat09)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat09)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat09')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
												
									<%
							          if ((new Byte((sv.ridPremStat10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat10)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat10)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat10')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.ridPremStat11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat11)%></div>	                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat11)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat11')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
									<%
							          if ((new Byte((sv.ridPremStat12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.ridPremStat12)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ridPremStat12)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('ridPremStat12')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td></td>
								<td>
								
								
									<%
							          if ((new Byte((sv.setRidPremStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setRidPremStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setRidPremStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setRidPremStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
									
								</td>
								<td>
								
								
									<%
							          if ((new Byte((sv.setSngpRidStat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							                                                      || fw.getVariables().isScreenProtected()) {
									%>          
									
									<div style="width: 40px;">	<%=smartHF.getHTMLVarExt(fw, sv.setSngpRidStat)%>	</div>                                         
							      
							        <%
										} else {
									%>
							        <div class="input-group" style="width: 75px;">
							            <%=smartHF.getRichTextInputFieldLookup(fw, sv.setSngpRidStat)%>
							             <span class="input-group-btn">
							             <button class="btn btn-info" type="button"
							                onClick="doFocus(document.getElementById('setSngpRidStat')); doAction('PFKEY04')">
							                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							            </button>
							        </div>
							        <%
							         }
							        %> 
								
									
								</td>
							</tr>
						
						
						
						</tbody>
					</table>
				</div>
				
				
			</div>
		</div>
	
<div class="row">	
			    	<div class="col-md-4">          
		<div class="form-group">  
		<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
	 <%if ((new Byte((sv.contitem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='contitem'  style="width: 100px;"
type='text'

<%if((sv.contitem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.contitem.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>    
		
		
		</div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>
	
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5679protectWritten.gt(0)) {
%>
<%
	S5679protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
