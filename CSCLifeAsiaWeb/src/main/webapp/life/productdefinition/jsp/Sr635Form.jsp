

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR635";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr635ScreenVars sv = (Sr635ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind50.isOn()) {
			sv.txtline.setReverse(BaseScreenData.REVERSED);
			sv.txtline.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.txtline.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind50.isOn()) {
			sv.txtline.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Client No      ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Provider Code  ");
%>
<%
	appVars.rollup(new int[]{93});
%>



<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
					<table><tr><td>
						<%
							if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

</td><td style="padding-left:1px;">
						<%
							if (!((sv.clntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:215px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Provider Code")%></label>
					<div>
						<%
							if (!((sv.zmedprv.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zmedprv.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[1];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.txtline.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = (sv.txtline.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0] = calculatedValue - 120;
		totalTblWidth = totalTblWidth - 120;
			%>
		<%
		GeneralTable sfl = fw.getTable("sr635screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>

		
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr635Table", {
				fixedCols : 0,					
				colWidths : [684],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width='100%'
							id='dataTables-sr635'>
							<thead>
								<tr class='info'>
		
		<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Termination Reason")%></center></th>
		         								
					
					
	</tr></thead>
	
	<tbody>
		
	<%
	Sr635screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr635screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag">
						    									<td class="tableDataTag" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																	
													
					
					 						 
						 
						 						 <input type='text' 
						maxLength='<%=sv.txtline.getLength()%>'
						 value='<%= sv.txtline.getFormData() %>' 
						 size='<%=sv.txtline.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(sr635screensfl.txtline)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr635screensfl" + "." +
						 "txtline" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.txtline.getLength()*12-120 %> px;"
						  
						  >
						  
						  						 
										
					
											
									</td>
					
	</tr>

	<%
	count = count + 1;
	Sr635screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	</tbody>
</table>
</div>
<!-- </DIV> -->
	
<br/></div></div></div></div></div>

<script>
	$(document).ready(function() {
		$('#dataTables-sr635').DataTable({
			ordering : false,
			searching : false,
			scrollY : "400px",
			scrollCollapse : true,
			scrollX : true

		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

