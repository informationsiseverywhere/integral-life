<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "ST579";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	St579ScreenVars sv = (St579ScreenVars) fw.getVariables();
%>

<%
	if (sv.St579screenWritten.gt(0)) {
%>
<%
	St579screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Dates effective     ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider ");
%>
<%
	sv.crtable01.setClassString("");
%>
<%
	sv.crtable01.appendClassString("string_fld");
		sv.crtable01.appendClassString("input_txt");
		sv.crtable01.appendClassString("highlight");
%>
<%
	sv.crtable02.setClassString("");
%>
<%
	sv.crtable02.appendClassString("string_fld");
		sv.crtable02.appendClassString("input_txt");
		sv.crtable02.appendClassString("highlight");
%>
<%
	sv.crtable03.setClassString("");
%>
<%
	sv.crtable03.appendClassString("string_fld");
		sv.crtable03.appendClassString("input_txt");
		sv.crtable03.appendClassString("highlight");
%>
<%
	sv.crtable04.setClassString("");
%>
<%
	sv.crtable04.appendClassString("string_fld");
		sv.crtable04.appendClassString("input_txt");
		sv.crtable04.appendClassString("highlight");
%>
<%
	sv.crtable05.setClassString("");
%>
<%
	sv.crtable05.appendClassString("string_fld");
		sv.crtable05.appendClassString("input_txt");
		sv.crtable05.appendClassString("highlight");
%>
<%
	sv.crtable06.setClassString("");
%>
<%
	sv.crtable06.appendClassString("string_fld");
		sv.crtable06.appendClassString("input_txt");
		sv.crtable06.appendClassString("highlight");
%>
<%
	sv.crtable07.setClassString("");
%>
<%
	sv.crtable07.appendClassString("string_fld");
		sv.crtable07.appendClassString("input_txt");
		sv.crtable07.appendClassString("highlight");
%>
<%
	sv.crtable08.setClassString("");
%>
<%
	sv.crtable08.appendClassString("string_fld");
		sv.crtable08.appendClassString("input_txt");
		sv.crtable08.appendClassString("highlight");
%>
<%
	sv.crtable09.setClassString("");
%>
<%
	sv.crtable09.appendClassString("string_fld");
		sv.crtable09.appendClassString("input_txt");
		sv.crtable09.appendClassString("highlight");
%>
<%
	sv.crtable10.setClassString("");
%>
<%
	sv.crtable10.appendClassString("string_fld");
		sv.crtable10.appendClassString("input_txt");
		sv.crtable10.appendClassString("highlight");
%>
<%
	sv.crtable11.setClassString("");
%>
<%
	sv.crtable11.appendClassString("string_fld");
		sv.crtable11.appendClassString("input_txt");
		sv.crtable11.appendClassString("highlight");
%>
<%
	sv.crtable12.setClassString("");
%>
<%
	sv.crtable12.appendClassString("string_fld");
		sv.crtable12.appendClassString("input_txt");
		sv.crtable12.appendClassString("highlight");
%>
<%
	sv.crtable13.setClassString("");
%>
<%
	sv.crtable13.appendClassString("string_fld");
		sv.crtable13.appendClassString("input_txt");
		sv.crtable13.appendClassString("highlight");
%>
<%
	sv.crtable14.setClassString("");
%>
<%
	sv.crtable14.appendClassString("string_fld");
		sv.crtable14.appendClassString("input_txt");
		sv.crtable14.appendClassString("highlight");
%>
<%
	sv.crtable15.setClassString("");
%>
<%
	sv.crtable15.appendClassString("string_fld");
		sv.crtable15.appendClassString("input_txt");
		sv.crtable15.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Aggregate Limit S/A ");
%>
<%
	sv.amta.setClassString("");
%>
<%
	sv.amta.appendClassString("num_fld");
		sv.amta.appendClassString("input_txt");
		sv.amta.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " F1");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.crtable01.setReverse(BaseScreenData.REVERSED);
				sv.crtable01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.crtable01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.crtable02.setReverse(BaseScreenData.REVERSED);
				sv.crtable02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.crtable02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.crtable03.setReverse(BaseScreenData.REVERSED);
				sv.crtable03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.crtable03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.crtable04.setReverse(BaseScreenData.REVERSED);
				sv.crtable04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.crtable04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.crtable05.setReverse(BaseScreenData.REVERSED);
				sv.crtable05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.crtable05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.crtable06.setReverse(BaseScreenData.REVERSED);
				sv.crtable06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.crtable06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.crtable07.setReverse(BaseScreenData.REVERSED);
				sv.crtable07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.crtable07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.crtable08.setReverse(BaseScreenData.REVERSED);
				sv.crtable08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.crtable08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.crtable09.setReverse(BaseScreenData.REVERSED);
				sv.crtable09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.crtable09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.crtable10.setReverse(BaseScreenData.REVERSED);
				sv.crtable10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.crtable10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.crtable11.setReverse(BaseScreenData.REVERSED);
				sv.crtable11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.crtable11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.crtable12.setReverse(BaseScreenData.REVERSED);
				sv.crtable12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.crtable12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.crtable13.setReverse(BaseScreenData.REVERSED);
				sv.crtable13.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.crtable13.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.crtable14.setReverse(BaseScreenData.REVERSED);
				sv.crtable14.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind14.isOn()) {
				sv.crtable14.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.crtable15.setReverse(BaseScreenData.REVERSED);
				sv.crtable15.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
				sv.crtable15.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.amta.setReverse(BaseScreenData.REVERSED);
				sv.amta.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
				sv.amta.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>









						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable01, (sv.crtable01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable02, (sv.crtable02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable03, (sv.crtable03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable04, (sv.crtable04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable05, (sv.crtable05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable06, (sv.crtable06.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable06')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable07, (sv.crtable07.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable07')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable08, (sv.crtable08.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable08')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable09, (sv.crtable09.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable09')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable10, (sv.crtable10.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable10')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable11, (sv.crtable11.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable11')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable12, (sv.crtable12.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable12')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable13, (sv.crtable13.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable13')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable14, (sv.crtable14.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable14')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.crtable15, (sv.crtable15.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('crtable15')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=smartHF.getLit(generatedText12)%></label>
						<div><%=smartHF.getHTMLVarExt(fw, sv.amta, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
					</div>
				</div>
			</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.St579protectWritten.gt(0)) {
%>
<%
	St579protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>

<!-- ILIFE-2590 Life Cross Browser -Coding and UT- Sprint 2 D4: Task 6 ends -->