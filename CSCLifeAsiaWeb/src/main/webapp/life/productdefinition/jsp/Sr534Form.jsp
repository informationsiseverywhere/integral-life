<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR534";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr534ScreenVars sv = (Sr534ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr534screenWritten.gt(0)) {
%>
<%
	Sr534screen.clearClassString(sv);
%>
<%
	StringData generatedText5 = new StringData("Schedule Name         ");
%>
<%
	sv.scheduleName.setClassString("");
%>
<%
	sv.scheduleName.appendClassString("string_fld");
		sv.scheduleName.appendClassString("output_txt");
		sv.scheduleName.appendClassString("highlight");
%>
<%
	StringData generatedText2 = new StringData("Accounting Month      ");
%>
<%
	sv.acctmonth.setClassString("");
%>
<%
	sv.acctmonth.appendClassString("num_fld");
		sv.acctmonth.appendClassString("output_txt");
		sv.acctmonth.appendClassString("highlight");
%>
<%
	StringData generatedText6 = new StringData("Number       ");
%>
<%
	sv.scheduleNumber.setClassString("");
%>
<%
	sv.scheduleNumber.appendClassString("num_fld");
		sv.scheduleNumber.appendClassString("output_txt");
		sv.scheduleNumber.appendClassString("highlight");
%>
<%
	StringData generatedText4 = new StringData("Year       ");
%>
<%
	sv.acctyear.setClassString("");
%>
<%
	sv.acctyear.appendClassString("num_fld");
		sv.acctyear.appendClassString("output_txt");
		sv.acctyear.appendClassString("highlight");
%>
<%
	StringData generatedText1 = new StringData("Effective Date        ");
%>
<%
	sv.effdateDisp.setClassString("");
%>
<%
	sv.effdateDisp.appendClassString("string_fld");
		sv.effdateDisp.appendClassString("output_txt");
		sv.effdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText8 = new StringData("Company               ");
%>
<%
	sv.bcompany.setClassString("");
%>
<%
	sv.bcompany.appendClassString("string_fld");
		sv.bcompany.appendClassString("output_txt");
		sv.bcompany.appendClassString("highlight");
%>
<%
	StringData generatedText3 = new StringData("Job Queue             ");
%>
<%
	sv.jobq.setClassString("");
%>
<%
	sv.jobq.appendClassString("string_fld");
		sv.jobq.appendClassString("output_txt");
		sv.jobq.appendClassString("highlight");
%>
<%
	StringData generatedText7 = new StringData("Branch                ");
%>
<%
	sv.bbranch.setClassString("");
%>
<%
	sv.bbranch.appendClassString("string_fld");
		sv.bbranch.appendClassString("output_txt");
		sv.bbranch.appendClassString("highlight");
%>
<%
	StringData generatedText13 = new StringData("Contract Issue Date Range ");
%>
<%
	StringData generatedText14 = new StringData("                    From  ");
%>
<%
	sv.datefrmDisp.setClassString("");
%>
<%
	StringData generatedText15 = new StringData("                      To  ");
%>
<%
	sv.datetoDisp.setClassString("");
%>
<%
	StringData generatedText16 = new StringData("Contract Number Range     ");
%>
<%
	StringData generatedText17 = new StringData("                    From  ");
%>
<%
	sv.chdrnum.setClassString("");
%>
<%
	StringData generatedText18 = new StringData("                      To  ");
%>
<%
	sv.chdrnum1.setClassString("");
%>
<%
	StringData generatedText19 = new StringData("Single Contract Indicator ");
%>
<%
	sv.indc.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
				sv.datefrmDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
				sv.datetoDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.chdrnum.setReverse(BaseScreenData.REVERSED);
				sv.chdrnum.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.chdrnum.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
				sv.chdrnum1.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.indc.setReverse(BaseScreenData.REVERSED);
				sv.indc.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.indc.setHighLight(BaseScreenData.BOLD);
			}
		}
%>



<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
					<div class="input-group ">
						<%
							if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleName.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.scheduleName.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>

						<%
							qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.scheduleNumber);

								if (!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="max-width:30px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" />

						<%
							}

								longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
				<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
					<div class="input-group ">
						<%
							if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctmonth.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctmonth.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:30px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>



						<%
							if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctyear.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.acctyear.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
						<div class="input-group">
						<%
							if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
					<div style="width: 150px;">
						<%
							if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jobq.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jobq.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				fieldItem = appVars.loadF4FieldsLong(new String[] { "bcompany" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("bcompany");
					longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());
			%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div style="width: 150px;">
						<div style="position: relative; margin-left: 1px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<%
				fieldItem = appVars.loadF4FieldsLong(new String[] { "bbranch" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("bbranch");
					longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());
			%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<div class="input-group";>
						<div style="position: relative; margin-left: 1px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=XSSFilter.escapeHtml(longValue)%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Issue Date Range")%></label>
					<table>
						<tr>
							<td><div class="input-group date form_date col-md-12"
									data-date="" data-date-format="dd/mm/yyyy"
									data-link-field="dobDisp" data-link-format="dd/mm/yyyy"
									style="width: 150px;">
									<%
										if ((new Byte((sv.datefrmDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										longValue = sv.datefrmDisp.getFormData();
									%>

									<%
										if ((new Byte((sv.datefrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=XSSFilter.escapeHtml(longValue)%>

										<%
											}
										%>
									</div>

									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<input name='datefrmDisp' type='text'
										value='<%=sv.datefrmDisp.getFormData()%>'
										maxLength='<%=sv.datefrmDisp.getLength()%>'
										size='<%=sv.datefrmDisp.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(datefrmDisp)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.datefrmDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.datefrmDisp).getHighLight()))
															.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
									<%
										} else {
									%>

									class = '
									<%=(sv.datefrmDisp).getColor() == null ? "input_cell"
									: (sv.datefrmDisp).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>

									<%
										}
													longValue = null;
												}
											}
									%>
								</div></td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To ")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%
										if ((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										longValue = sv.datetoDisp.getFormData();
									%>

									<%
										if ((new Byte((sv.datetoDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=XSSFilter.escapeHtml(longValue)%>

										<%
											}
										%>
									</div>

									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<input name='datetoDisp' type='text'
										value='<%=sv.datetoDisp.getFormData()%>'
										maxLength='<%=sv.datetoDisp.getLength()%>'
										size='<%=sv.datetoDisp.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(datetoDisp)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.datetoDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.datetoDisp).getHighLight()))
															.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
									<%
										} else {
									%>

									class = '
									<%=(sv.datetoDisp).getColor() == null ? "input_cell"
									: (sv.datetoDisp).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > <span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>

									<%
										}
													longValue = null;
												}
											}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number Range")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.chdrnum.getFormData();
 %> <%
 	if ((new Byte((sv.chdrnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='chdrnum' type='text'
								value='<%=sv.chdrnum.getFormData()%>'
								maxLength='<%=sv.chdrnum.getLength()%>'
								size='<%=sv.chdrnum.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(chdrnum)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.chdrnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.chdrnum).getHighLight()))
 						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	} else {
 %> class = ' <%=(sv.chdrnum).getColor() == null ? "input_cell"
									: (sv.chdrnum).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <%
 	}
 				longValue = null;
 			}
 		}
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To ")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.chdrnum1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.chdrnum1.getFormData();
 %> <%
 	if ((new Byte((sv.chdrnum1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='chdrnum1' type='text'
								value='<%=sv.chdrnum1.getFormData()%>'
								maxLength='<%=sv.chdrnum1.getLength()%>'
								size='<%=sv.chdrnum1.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(chdrnum1)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.chdrnum1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.chdrnum1).getHighLight()))
 						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <%
 	} else {
 %> class = ' <%=(sv.chdrnum1).getColor() == null ? "input_cell"
									: (sv.chdrnum1).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <%
 	}
 				longValue = null;
 			}
 		}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Single Contract Indicator")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.indc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							longValue = sv.indc.getFormData();
						%>

						<%
							if ((new Byte((sv.indc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='indc' type='text' value='<%=sv.indc.getFormData()%>'
							maxLength='<%=sv.indc.getLength()%>'
							size='<%=sv.indc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(indc)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.indc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.indc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" >
						<%
							} else {
						%>

						class = '
						<%=(sv.indc).getColor() == null ? "input_cell"
									: (sv.indc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' >

						<%
							}
										longValue = null;
									}
								}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.Sr534protectWritten.gt(0)) {
%>
<%
	Sr534protect.clearClassString(sv);
%>

<%
	}
%>
<%@ include file="/POLACommon2NEW.jsp"%>
