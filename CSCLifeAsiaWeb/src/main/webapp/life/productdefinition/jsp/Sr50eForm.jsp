

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR50E";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr50eScreenVars sv = (Sr50eScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.message.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number  ");
%>

<!-- <ILIFE-2758 by opal - starts> -->

<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>

<!-- <ILIFE-2758 by opal - ends> -->

<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner   ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured     ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Category         ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tran Date ");
%>
<%
	appVars.rollup(new int[] { 93 });
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.pnotecat.setReverse(BaseScreenData.REVERSED);
			sv.pnotecat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.pnotecat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.trandateDisp.setReverse(BaseScreenData.REVERSED);
			sv.trandateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.trandateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td>
								<div
									class='<%=(sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<div
									class='<%=(sv.cnttype.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>' style="max-width:50px;min-width:50px">
									<%
										if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.cnttype.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.cnttype.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="max-width:130px;min-width:100px">
								<div
									class='<%=(sv.ctypdesc.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>' style="max-width:130px;min-width:100px">
									<%
										if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4" >
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<div class="input-group">
						<div
							class='<%=(sv.rstate.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div class="input-group">
						<div
							class='<%=(sv.pstate.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>' style="max-width:130px;min-width:100px">
							<%
								if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table><tr><td>
						<div
							class='<%=(sv.cownnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<div style="margin-left: 1px;max-width: 200px;"
							class='<%=(sv.jlname01.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.jlname01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlname01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlname01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
			<div class="col-md-4" >
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table><tr><td>
						<div
							class='<%=(sv.lifcnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<div style="margin-left: 1px;max-width: 300px;"
							class='<%=(sv.jlname02.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.jlname02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlname02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlname02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Category")%></label>
					<div class="input-group" style="min-width: 100px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "pnotecat" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("pnotecat");
							optionValue = makeDropDownList(mappedItems, sv.pnotecat.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.pnotecat.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.pnotecat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell'>
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.pnotecat).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='pnotecat' type='list' style="width: 170px;"
								<%if ((new Byte((sv.pnotecat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.pnotecat).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.pnotecat).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4" >
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tran Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">


						<input name='trandateDisp' type='text'
							value='<%=sv.trandateDisp.getFormData()%>'
							maxLength='<%=sv.trandateDisp.getLength()%>'
							size='<%=sv.trandateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(trandateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.trandateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.trandateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.trandateDisp).getColor() == null ? "input_cell"
						: (sv.trandateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>
					</div>
				</div>
			</div>







			<div class="col-md-1">
				<div class="form-group">
					<label style="visibility: hidden">Action</label>
					<a class="btn btn-info" href= "#" onClick="doAction('PFKEY0')"><%=resourceBundleHandler.gettingValueFromBundle("Search")%></a>
				</div>
				
			 
			</div>
		</div>
		<%
			GeneralTable sfl = fw.getTable("sr50escreensfl");
		%>
		<br/><br/>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr50e' width='100%'>
						<thead>
							<tr class='info'>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								String backgroundcolor = "#FFFFFF";

								Sr50escreensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sr50escreensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<td
									style="color: #434343; padding: 5px; width: 720px; z-index: 8; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft +1); border-right: 1px solid #dddddd;"
									align="left"><%=sv.message.getFormData()%></td>
							</tr>
							<%
								if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
										backgroundcolor = "#ededed";
									} else {
										backgroundcolor = "#FFFFFF";
									}
									count = count + 1;
									Sr50escreensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-sr50e').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
			info: false,
			paging: true
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>&nbsp; &nbsp;<br />


				<div
					class='<%=(sv.desc.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
					<%
						if (!((sv.desc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.desc.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.desc.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
 	longValue = null;
 	formatValue = null;
 %>
			</td>
		</tr>
	</table>
</div>
