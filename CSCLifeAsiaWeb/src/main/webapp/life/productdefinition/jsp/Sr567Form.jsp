

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR567";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%Sr567ScreenVars sv = (Sr567ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Installment No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Due Date");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Installment Amount");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mnth01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.mnth01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mnth01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.linstamt01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.linstamt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.linstamt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.mnth02.setReverse(BaseScreenData.REVERSED);
			sv.mnth02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.mnth02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.mnth03.setReverse(BaseScreenData.REVERSED);
			sv.mnth03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.mnth03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.mnth04.setReverse(BaseScreenData.REVERSED);
			sv.mnth04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.mnth04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mnth05.setReverse(BaseScreenData.REVERSED);
			sv.mnth05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mnth05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.mnth06.setReverse(BaseScreenData.REVERSED);
			sv.mnth06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.mnth06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.mnth07.setReverse(BaseScreenData.REVERSED);
			sv.mnth07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.mnth07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.mnth07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.linstamt02.setReverse(BaseScreenData.REVERSED);
			sv.linstamt02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.linstamt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.linstamt03.setReverse(BaseScreenData.REVERSED);
			sv.linstamt03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.linstamt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.linstamt04.setReverse(BaseScreenData.REVERSED);
			sv.linstamt04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.linstamt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.linstamt05.setReverse(BaseScreenData.REVERSED);
			sv.linstamt05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.linstamt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.linstamt06.setReverse(BaseScreenData.REVERSED);
			sv.linstamt06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.linstamt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.linstamt07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.linstamt07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.linstamt07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.linstamt07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.datedue01Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue01Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.datedue01Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.datedue01Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.datedue02Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue02Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.datedue02Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.datedue02Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.datedue03Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue03Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.datedue03Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.datedue03Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.datedue04Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue04Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.datedue04Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.datedue04Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.datedue05Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue05Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.datedue05Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.datedue05Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.datedue06Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue06Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.datedue06Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.datedue06Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.datedue07Disp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.datedue07Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
			sv.datedue07Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.datedue07Disp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
<div class="panel-body">
<div class="row">
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Installment No")%></label>
</div>
</div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Due Date")%></label>
</div>
</div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Installment Amount")%></label>
</div>
</div>
</div>


<div class="row">
<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>
<input name='mnth01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth01) %>'
	 <%}%>

size='<%= sv.mnth01.getLength()%>'
maxLength='<%= sv.mnth01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth01).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth01).getColor()== null  ? 
			"input_cell" :  (sv.mnth01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue01Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue01Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue01Disp' 
type='text' 
value='<%=sv.datedue01Disp.getFormData()%>' 
maxLength='<%=sv.datedue01Disp.getLength()%>' 
size='<%=sv.datedue01Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue01Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue01Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue01Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

	<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue01Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue01Disp,(sv.datedue01Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
 


<%
	}else { 
%>

class = ' <%=(sv.datedue01Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue01Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue01Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue01Disp,(sv.datedue01Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>

</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>
<input name='linstamt01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt01) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt01.getLength(), sv.linstamt01.getScale(),3)%>'
maxLength='<%= sv.linstamt01.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt01).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt01).getColor()== null  ? 
			"input_cell" :  (sv.linstamt01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>  
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%	
			qpsf = fw.getFieldXMLDef((sv.mnth02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>
<input name='mnth02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth02) %>'
	 <%}%>

size='<%= sv.mnth02.getLength()%>'
maxLength='<%= sv.mnth02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth02).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth02).getColor()== null  ? 
			"input_cell" :  (sv.mnth02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue02Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue02Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue02Disp' 
type='text' 
value='<%=sv.datedue02Disp.getFormData()%>' 
maxLength='<%=sv.datedue02Disp.getLength()%>' 
size='<%=sv.datedue02Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue02Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue02Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue02Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('datedue02Disp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue02Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue02Disp,(sv.datedue02Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%
	}else { 
%>

class = ' <%=(sv.datedue02Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue02Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue02Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue02Disp,(sv.datedue02Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
				valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>
<input name='linstamt02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt02) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt02.getLength(), sv.linstamt02.getScale(),3)%>'
maxLength='<%= sv.linstamt02.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt02).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt02).getColor()== null  ? 
			"input_cell" :  (sv.linstamt02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>


<div class="row">
<div class="col-md-3">
<div class="form-group">
<%	
			qpsf = fw.getFieldXMLDef((sv.mnth03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>
<input name='mnth03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth03) %>'
	 <%}%>

size='<%= sv.mnth03.getLength()%>'
maxLength='<%= sv.mnth03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth03).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth03).getColor()== null  ? 
			"input_cell" :  (sv.mnth03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue03Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue03Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue03Disp' 
type='text' 
value='<%=sv.datedue03Disp.getFormData()%>' 
maxLength='<%=sv.datedue03Disp.getLength()%>' 
size='<%=sv.datedue03Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue03Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue03Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue03Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue03Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue03Disp,(sv.datedue03Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%
	}else { 
%>

class = ' <%=(sv.datedue03Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue03Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue03Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue03Disp,(sv.datedue03Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt03).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>
<input name='linstamt03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt03) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt03.getLength(), sv.linstamt03.getScale(),3)%>'
maxLength='<%= sv.linstamt03.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt03).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt03).getColor()== null  ? 
			"input_cell" :  (sv.linstamt03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>
<input name='mnth04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth04) %>'
	 <%}%>

size='<%= sv.mnth04.getLength()%>'
maxLength='<%= sv.mnth04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth04).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth04).getColor()== null  ? 
			"input_cell" :  (sv.mnth04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue04Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue04Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue04Disp' 
type='text' 
value='<%=sv.datedue04Disp.getFormData()%>' 
maxLength='<%=sv.datedue04Disp.getLength()%>' 
size='<%=sv.datedue04Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue04Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue04Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue04Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue04Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue04Disp,(sv.datedue04Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%
	}else { 
%>

class = ' <%=(sv.datedue04Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue04Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue04Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue04Disp,(sv.datedue04Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>


<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt04).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>
<input name='linstamt04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt04) %>'
	 <%}%>

size='<%= sv.linstamt04.getLength()+2%>'                
maxLength='<%= sv.linstamt04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(linstamt04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt04).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt04).getColor()== null  ? 
			"input_cell" :  (sv.linstamt04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">

	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='mnth05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth05) %>'
	 <%}%>

size='<%= sv.mnth05.getLength()%>'
maxLength='<%= sv.mnth05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth05).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth05).getColor()== null  ? 
			"input_cell" :  (sv.mnth05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue05Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue05Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue05Disp' 
type='text' 
value='<%=sv.datedue05Disp.getFormData()%>' 
maxLength='<%=sv.datedue05Disp.getLength()%>' 
size='<%=sv.datedue05Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue05Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue05Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue05Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue05Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue05Disp,(sv.datedue05Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>


<%
	}else { 
%>

class = ' <%=(sv.datedue05Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue05Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue05Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue05Disp,(sv.datedue05Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt05).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
				valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);	%>
<input name='linstamt05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt05) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt05.getLength(), sv.linstamt05.getScale(),3)%>'
maxLength='<%= sv.linstamt05.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt05).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt05).getColor()== null  ? 
			"input_cell" :  (sv.linstamt05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">

	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='mnth06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth06) %>'
	 <%}%>

size='<%= sv.mnth06.getLength()%>'
maxLength='<%= sv.mnth06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth06).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth06).getColor()== null  ? 
			"input_cell" :  (sv.mnth06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>


<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue06Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue06Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue06Disp' 
type='text' 
value='<%=sv.datedue06Disp.getFormData()%>' 
maxLength='<%=sv.datedue06Disp.getLength()%>' 
size='<%=sv.datedue06Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue06Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue06Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue06Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('datedue06Disp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue06Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue06Disp,(sv.datedue06Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%
	}else { 
%>

class = ' <%=(sv.datedue06Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue06Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('datedue06Disp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue06Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue06Disp,(sv.datedue06Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt06).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt06,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>
<input name='linstamt06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt06) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt06.getLength(), sv.linstamt06.getScale(),3)%>'
maxLength='<%= sv.linstamt06.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt06).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt06).getColor()== null  ? 
			"input_cell" :  (sv.linstamt06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">


	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='mnth07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth07) %>'
	 <%}%>

size='<%= sv.mnth07.getLength()%>'
maxLength='<%= sv.mnth07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth07).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth07).getColor()== null  ? 
			"input_cell" :  (sv.mnth07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%	
	longValue = sv.datedue07Disp.getFormData();  
%>

<% 
	if((new Byte((sv.datedue07Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datedue07Disp' 
type='text' 
value='<%=sv.datedue07Disp.getFormData()%>' 
maxLength='<%=sv.datedue07Disp.getLength()%>' 
size='<%=sv.datedue07Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datedue07Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datedue07Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datedue07Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

 
 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue07Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue07Disp,(sv.datedue07Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
<%
	}else { 
%>

class = ' <%=(sv.datedue07Disp).getColor()== null  ? 
"input_cell" :  (sv.datedue07Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datedue07Disp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datedue07Disp,(sv.datedue07Disp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>

<%} }%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt07).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
				valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt07,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='linstamt07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt07) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt07.getLength(), sv.linstamt07.getScale(),3)%>'
maxLength='<%= sv.linstamt07.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.linstamt07).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt07).getColor()== null  ? 
			"input_cell" :  (sv.linstamt07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
