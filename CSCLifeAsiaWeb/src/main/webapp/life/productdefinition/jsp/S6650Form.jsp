<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S6650";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	S6650ScreenVars sv = (S6650ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6650screenWritten.gt(0)) {
%>
<%
	S6650screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid from ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid to ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sum Assured To");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Factor");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rate");
%>
<%
	sv.sumin01.setClassString("");
%>
<%
	sv.sumin01.appendClassString("num_fld");
		sv.sumin01.appendClassString("input_txt");
		sv.sumin01.appendClassString("highlight");
%>
<%
	sv.fact01.setClassString("");
%>
<%
	sv.fact01.appendClassString("num_fld");
		sv.fact01.appendClassString("input_txt");
		sv.fact01.appendClassString("highlight");
%>
<%
	sv.rrat01.setClassString("");
%>
<%
	sv.rrat01.appendClassString("num_fld");
		sv.rrat01.appendClassString("input_txt");
		sv.rrat01.appendClassString("highlight");
%>
<%
	sv.sumin02.setClassString("");
%>
<%
	sv.sumin02.appendClassString("num_fld");
		sv.sumin02.appendClassString("input_txt");
		sv.sumin02.appendClassString("highlight");
%>
<%
	sv.fact02.setClassString("");
%>
<%
	sv.fact02.appendClassString("num_fld");
		sv.fact02.appendClassString("input_txt");
		sv.fact02.appendClassString("highlight");
%>
<%
	sv.rrat02.setClassString("");
%>
<%
	sv.rrat02.appendClassString("num_fld");
		sv.rrat02.appendClassString("input_txt");
		sv.rrat02.appendClassString("highlight");
%>
<%
	sv.sumin03.setClassString("");
%>
<%
	sv.sumin03.appendClassString("num_fld");
		sv.sumin03.appendClassString("input_txt");
		sv.sumin03.appendClassString("highlight");
%>
<%
	sv.fact03.setClassString("");
%>
<%
	sv.fact03.appendClassString("num_fld");
		sv.fact03.appendClassString("input_txt");
		sv.fact03.appendClassString("highlight");
%>
<%
	sv.rrat03.setClassString("");
%>
<%
	sv.rrat03.appendClassString("num_fld");
		sv.rrat03.appendClassString("input_txt");
		sv.rrat03.appendClassString("highlight");
%>
<%
	sv.sumin04.setClassString("");
%>
<%
	sv.sumin04.appendClassString("num_fld");
		sv.sumin04.appendClassString("input_txt");
		sv.sumin04.appendClassString("highlight");
%>
<%
	sv.fact04.setClassString("");
%>
<%
	sv.fact04.appendClassString("num_fld");
		sv.fact04.appendClassString("input_txt");
		sv.fact04.appendClassString("highlight");
%>
<%
	sv.rrat04.setClassString("");
%>
<%
	sv.rrat04.appendClassString("num_fld");
		sv.rrat04.appendClassString("input_txt");
		sv.rrat04.appendClassString("highlight");
%>
<%
	sv.sumin05.setClassString("");
%>
<%
	sv.sumin05.appendClassString("num_fld");
		sv.sumin05.appendClassString("input_txt");
		sv.sumin05.appendClassString("highlight");
%>
<%
	sv.fact05.setClassString("");
%>
<%
	sv.fact05.appendClassString("num_fld");
		sv.fact05.appendClassString("input_txt");
		sv.fact05.appendClassString("highlight");
%>
<%
	sv.rrat05.setClassString("");
%>
<%
	sv.rrat05.appendClassString("num_fld");
		sv.rrat05.appendClassString("input_txt");
		sv.rrat05.appendClassString("highlight");
%>
<%
	sv.sumin06.setClassString("");
%>
<%
	sv.sumin06.appendClassString("num_fld");
		sv.sumin06.appendClassString("input_txt");
		sv.sumin06.appendClassString("highlight");
%>
<%
	sv.fact06.setClassString("");
%>
<%
	sv.fact06.appendClassString("num_fld");
		sv.fact06.appendClassString("input_txt");
		sv.fact06.appendClassString("highlight");
%>
<%
	sv.rrat06.setClassString("");
%>
<%
	sv.rrat06.appendClassString("num_fld");
		sv.rrat06.appendClassString("input_txt");
		sv.rrat06.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 46px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

</td><td>







						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("Valid to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured To")%></label>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
						.replaceAll("width:152px", "width:141px")%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 46px;"><%=smartHF.getHTMLVarExt(fw, sv.fact06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.rrat06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S6650protectWritten.gt(0)) {
%>
<%
	S6650protect.clearClassString(sv);
%>
<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>