<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR565";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%Sr565ScreenVars sv = (Sr565ScreenVars) fw.getVariables();%>

	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.yrsinf.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind05.isOn()) {
			sv.yrsinf.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.yrsinf.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.yrsinf.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.sumins.setReverse(BaseScreenData.REVERSED);
			sv.sumins.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.sumins.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.surrval.setReverse(BaseScreenData.REVERSED);
			sv.surrval.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.surrval.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage/Rider ");%>
	<%--ILIFE-3737 starts --%>
	 <% if (sv.mrt1Flag.compareTo("N") != 0) { %>
		 <%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reducing Fequency ");%>
         <%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Value ");%> 
		 <%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Monthly Duration");%>
	     <%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"UVAL Rate Monthly");%>
	     <%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Monthly Reducing Benefit/SA");%>
	<%}else{%>
	
		<!--ILIFE-7521-->
		<%if (sv.policyType.compareTo("N") == 0){%>
				<%=resourceBundleHandler.gettingValueFromBundle("Policy Year")%></th>
		<%}else{ %>
				<%=resourceBundleHandler.gettingValueFromBundle("Policy Month")%></th>
		<%} %>
	     <%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured");%>
	     <%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CSV");%>
	 <%}%>
	 	<%--ILIFE-3737 ends --%>
		
<%		appVars.rollup(new int[] {93});
%>








<div class="panel panel-default">    	
 <div class="panel-body">

	<div class="row">
		
		<div class="col-md-4"> 
			<div class="form-group">  	  
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
			
			  		<div>
					<%					
					if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width:150px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  	</div>
			</div>
		</div>
		
		<div class="col-md-4"> 
			<div class="form-group">  	  
				<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
				<div>
					<%					
					if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  		</div>
			  	</div>
			</div>
			
			
			<div class="col-md-4"> 
				<div class="form-group">  	  
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
					<div>
					<%					
					if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rider.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rider.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  		</div>
				</div>
			</div>
		
	</div>
	<div class="row">
		<div class="col-md-4"> 
			<div class="form-group">  	  
				<label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%> </label>
			
				<div>
			  		
					<%					
					if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.life.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.life.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  	</div>			
			 </div>
			</div>
			
			<div class="col-md-4"> 
				<div class="form-group">  	  
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
			
					<table>
					<tr>
					<td>			
			  		
							<%					
							if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 65px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
						</td>
						<td>
					
					  		
							<%					
							if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 200px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
			  			</td>
			  			</tr>
			  			</table>
			  		</div>
			  	</div>
		

				<div class="col-md-4"> 
					<div class="form-group">  	  
						<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider")%></label>

							<table>
							<tr>
							<td>
			
					  		
							<%					
							if(!((sv.crcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crcode.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crcode.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 65x;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td>
					  <td>
						
					  		
							<%					
							if(!((sv.coverDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.coverDesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.coverDesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' id="cntdesc" style="max-width: 190px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	</td>
					  	</tr>
					  	</table>
					  	</div>
					  </div>
		
	</div>


<!-- ILIFE-3737 starts -->
	<div class="row">
			
			<div class="col-md-4"> 
				<div class="form-group">  	  
					<% if (sv.redfreq.compareTo("N") != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reducing Frequency")%></label>
			
					<div class="input-group" style="min-width:71px;">  	 
			  		
					<%					
					if(!((sv.redfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.redfreq.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.redfreq.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					
					<%}%>
					
			  	</div>	
			  			
						
					  		
					</div>
				</div>
			<div class="col-md-4"> 
				<div class="form-group" style="width: 150px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Unit Value")%></label> 
					<div class="input-group" style="min-width:71px;">  	 
					<%=smartHF.getHTMLVar(0, 0, fw, null, sv.unitval, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				</div>
			</div>
			
	</div>
<!-- ILIFE-3737 ends -->




	<div class="row">
		<div class="col-md-12">
		<div class="form-group">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id='dataTables-sr565' width='100%'>
				<thead>
					<tr class='info'>
					<% if (sv.mrt1Flag.compareTo("N") != 0) { %>
		
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Monthly Duration")%></th>
		         								
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("UVAL Rate Monthly")%></th>
	
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Monthly Reducing Benefit/SA")%></th>
					 <% }else{%>
					<!--  BRD-139-ENDS -->	
					
					<!--ILIFE-7521-->
					<%if (sv.policyType.compareTo("N") == 0){%>
							<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Policy Year")%></th>
					<%}else{ %>
							<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Policy Month")%></th>
					<%} %>		 
					
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></th>
	
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("CSV")%></th>
					<%} %>	
					</tr>
				</thead>
				<tbody>
				
					
					<%
					GeneralTable sfl = fw.getTable("sr565screensfl");
					/* int height;
					if(sfl.count()*27 > 210) {
					height = 210 ;
					} else {
					height = sfl.count()*27;
					}	 */
					%>					 
				<%
				Sr565screensfl
				.set1stScreenRow(sfl, appVars, sv);
				int count = 1;
				while (Sr565screensfl
				.hasMoreScreenRows(sfl)) {	
			%>

			<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    									<td  
					<%if((sv.yrsinf).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >
																			
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.yrsinf).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
							formatValue = smartHF.getPicFormatted(qpsf,sv.yrsinf);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.yrsinf.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.yrsinf.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(sr565screensfl.yrsinf)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr565screensfl" + "." +
						 "yrsinf" + "_R" + count %>'
						 id='<%="sr565screensfl" + "." +
						 "yrsinf" + "_R" + count %>'
						 class = "input_cell"
						       disabled="disabled"
                		style="width: 100% !important;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td  
					<%if((sv.sumins).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.sumins.getLength()%>'
						 value='<%=formatValue %>' 
						  size='3'
						 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sr565screensfl.sumins)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr565screensfl" + "." +
						 "sumins" + "_R" + count %>'
						 id='<%="sr565screensfl" + "." +
						 "sumins" + "_R" + count %>'
						 class = "input_cell new_style"
			               disabled="disabled"
               		       
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td 
					<%if((sv.surrval).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.surrval).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.surrval,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.surrval.getLength()%>'
						 value='<%=formatValue %>' 
						  size='3'
						 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sr565screensfl.surrval)' onKeyUp='return checkMaxLength(this)' 
						name='<%="sr565screensfl" + "." +
						 "surrval" + "_R" + count %>'
						 id='<%="sr565screensfl" + "." +
						 "surrval" + "_R" + count %>'
						 class = "input_cell"
						  disabled="disabled"
						  style="width: 100% !important;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
					
					</tr>
				
					<%
					count = count + 1;
					Sr565screensfl
					.setNextScreenRow(sfl, appVars, sv);
					/*ILIFE-7501*/
					if(sv.yrsinf.equals(0)&&sv.sumins.equals(0)&&sv.surrval.equals(0)){
						break;
					}
					}
					%>
				
				</tbody>
			</table>
		</div>
		</div>
		</div>
		
	
	
	
	
	</div>






</div>
</div>
<style type="text/css">	
.new_style {
 width: 100% !important;
}
</style>

<script>
$(document).ready(function() {
if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','215px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','205px')
	} 
	$('#dataTables-sr565').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
