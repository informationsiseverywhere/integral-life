<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51C";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51cScreenVars sv = (Sr51cScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr51cscreenWritten.gt(0)) {
%>
<%
	Sr51cscreen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age Band");
%>
<%
	generatedText7.appendClassString("label_txt");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Multiple Amount");
%>
<%
	generatedText20.appendClassString("label_txt");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age01.setClassString("");
%>
<%
	sv.sumins01.setClassString("");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age02.setClassString("");
%>
<%
	sv.sumins02.setClassString("");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age03.setClassString("");
%>
<%
	sv.sumins03.setClassString("");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age04.setClassString("");
%>
<%
	sv.sumins04.setClassString("");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age05.setClassString("");
%>
<%
	sv.sumins05.setClassString("");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age06.setClassString("");
%>
<%
	sv.sumins06.setClassString("");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age07.setClassString("");
%>
<%
	sv.sumins07.setClassString("");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age08.setClassString("");
%>
<%
	sv.sumins08.setClassString("");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age09.setClassString("");
%>
<%
	sv.sumins09.setClassString("");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age10.setClassString("");
%>
<%
	sv.sumins10.setClassString("");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age11.setClassString("");
%>
<%
	sv.sumins11.setClassString("");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "<=");
%>
<%
	sv.age12.setClassString("");
%>
<%
	sv.sumins12.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind20.isOn()) {
				sv.age01.setReverse(BaseScreenData.REVERSED);
				sv.age01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind20.isOn()) {
				sv.age01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.age02.setReverse(BaseScreenData.REVERSED);
				sv.age02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind21.isOn()) {
				sv.age02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind22.isOn()) {
				sv.age03.setReverse(BaseScreenData.REVERSED);
				sv.age03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
				sv.age03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind23.isOn()) {
				sv.age04.setReverse(BaseScreenData.REVERSED);
				sv.age04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
				sv.age04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind24.isOn()) {
				sv.age05.setReverse(BaseScreenData.REVERSED);
				sv.age05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind24.isOn()) {
				sv.age05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind25.isOn()) {
				sv.age06.setReverse(BaseScreenData.REVERSED);
				sv.age06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind25.isOn()) {
				sv.age06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind26.isOn()) {
				sv.age07.setReverse(BaseScreenData.REVERSED);
				sv.age07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind26.isOn()) {
				sv.age07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
				sv.age08.setReverse(BaseScreenData.REVERSED);
				sv.age08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind27.isOn()) {
				sv.age08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
				sv.age09.setReverse(BaseScreenData.REVERSED);
				sv.age09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind28.isOn()) {
				sv.age09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind29.isOn()) {
				sv.age10.setReverse(BaseScreenData.REVERSED);
				sv.age10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
				sv.age10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind30.isOn()) {
				sv.age11.setReverse(BaseScreenData.REVERSED);
				sv.age11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
				sv.age11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind31.isOn()) {
				sv.age12.setReverse(BaseScreenData.REVERSED);
				sv.age12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind31.isOn()) {
				sv.age12.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
				sv.sumins01.setReverse(BaseScreenData.REVERSED);
				sv.sumins01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind32.isOn()) {
				sv.sumins01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind33.isOn()) {
				sv.sumins02.setReverse(BaseScreenData.REVERSED);
				sv.sumins02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind33.isOn()) {
				sv.sumins02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
				sv.sumins03.setReverse(BaseScreenData.REVERSED);
				sv.sumins03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind34.isOn()) {
				sv.sumins03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.sumins04.setReverse(BaseScreenData.REVERSED);
				sv.sumins04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
				sv.sumins04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind36.isOn()) {
				sv.sumins05.setReverse(BaseScreenData.REVERSED);
				sv.sumins05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
				sv.sumins05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind37.isOn()) {
				sv.sumins06.setReverse(BaseScreenData.REVERSED);
				sv.sumins06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind37.isOn()) {
				sv.sumins06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
				sv.sumins07.setReverse(BaseScreenData.REVERSED);
				sv.sumins07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind38.isOn()) {
				sv.sumins07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind39.isOn()) {
				sv.sumins08.setReverse(BaseScreenData.REVERSED);
				sv.sumins08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind39.isOn()) {
				sv.sumins08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind40.isOn()) {
				sv.sumins09.setReverse(BaseScreenData.REVERSED);
				sv.sumins09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind40.isOn()) {
				sv.sumins09.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind41.isOn()) {
				sv.sumins10.setReverse(BaseScreenData.REVERSED);
				sv.sumins10.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind41.isOn()) {
				sv.sumins10.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind42.isOn()) {
				sv.sumins11.setReverse(BaseScreenData.REVERSED);
				sv.sumins11.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind42.isOn()) {
				sv.sumins11.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind43.isOn()) {
				sv.sumins12.setReverse(BaseScreenData.REVERSED);
				sv.sumins12.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind43.isOn()) {
				sv.sumins12.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
				<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : ""%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>

					
				</div>
			</div>
		</div>
		<!-- end row 1 -->
		<!-- row 2 -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;</td>
							<td><label style="font-weight: bolder; font-size: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- end row 2 -->
		<!-- row 3 -->
		<div class="row">
			<div class="col-md-9">
				<div class="from-group">
					<div class="table-responsive">
						<table class="table border-none  table-hover" width='100%'>
							<thead>
								<tr>
									<td><label><%=smartHF.getLit(generatedText7)%></label></td>
									<td><label><%=smartHF.getLit(generatedText20)%></label></td>

								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText8)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText9)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText10)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText11)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText12)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText13)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText14)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText15)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText16)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText17)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins10, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText18)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins11, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>

								<tr>
									<td>
										<ul class="list-inline">
											<li><%=smartHF.getLit(generatedText19)%></li>
											<li><%=smartHF.getHTMLVarExt(fw, sv.age12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS,1,46)%></li>
										</ul>
									</td>
									<td><%=smartHF.getHTMLVarExt(fw, sv.sumins12, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS,
						1, 71)%></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- end row 3 -->
	</div>
</div>


<%
	}
%>

<%
	if (sv.Sr51cprotectWritten.gt(0)) {
%>
<%
	Sr51cprotect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>


<style>
div[id*='age'] {
	padding-right: 2px !important
}

div[id*='instpr'] {
	padding-right: 2px !important
}

div[id*='indc'] {
	padding-right: 1px !important
}

div[id*='overdueMina'] {
	padding-right: 2px !important
}

div[id*='sumins'] {
	padding-right: 2px !important
}
/* for IE 8 */
@media \0screen\,screen\9 {
	.output_cell {
		margin-right: 3px;
	}
}

.input-group.three-controller>.input-group-addon {
	width: 71% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.table.border-none tr td, .table.border-none tr th {
	border: none !important;
}
</style>


<%@ include file="/POLACommon2NEW.jsp"%>
