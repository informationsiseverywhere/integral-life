

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR51I";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sr51iScreenVars sv = (Sr51iScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Comparison");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Operator");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Term ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(LE/EQ/LT   , LE - Less than and Equal                                                        EQ - Equal                                                                      LT - Less than)");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premium Term ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(LE/EQ/LT)");
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
					<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
				
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td> <label> <%=resourceBundleHandler.gettingValueFromBundle("To")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:85px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Comparison Operator")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Term")%></label>
					<div class="input-group" style="min-width:71px">
						<%
							if ((new Byte((sv.indc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("LE")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Less than and Equal");
								}
								if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("EQ")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Equal");
								}
								if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("LT")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Less than");
								}
						%>

						<%
							if ((new Byte((sv.indc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div 
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.indc01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='indc01' style="width: 170px; "
								onFocus='doFocus(this)' onHelp='return fieldHelp(indc01)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.indc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.indc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="LE"
									<%if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("LE")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Less than and Equal")%></option>
								<option value="EQ"
									<%if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("EQ")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Equal")%></option>
								<option value="LT"
									<%if (((sv.indc01.getFormData()).toString()).trim().equalsIgnoreCase("LT")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Less than")%></option>

							</select>
							<%
								if ("red".equals((sv.indc01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Term")%></label>
					<div class="input-group" style="min-width:71px">
						<%
							if ((new Byte((sv.indc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("LE")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Less than and Equal");
								}
								if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("EQ")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Equal");
								}
								if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("LT")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Less than");
								}
						%>

						<%
							if ((new Byte((sv.indc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div 
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.indc02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='indc02' style="width: 170px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(indc02)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.indc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.indc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="LE"
									<%if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("LE")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Less than and Equal")%></option>
								<option value="EQ"
									<%if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("EQ")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Equal")%></option>
								<option value="LT"
									<%if (((sv.indc02.getFormData()).toString()).trim().equalsIgnoreCase("LT")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Less than")%></option>

							</select>
							<%
								if ("red".equals((sv.indc02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;" class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Comparison")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Operator")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(
					"(LE/EQ/LT   , LE - Less than and Equal                                                        EQ - Equal                                                                      LT - Less than)")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("(LE/EQ/LT)")%></label>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
