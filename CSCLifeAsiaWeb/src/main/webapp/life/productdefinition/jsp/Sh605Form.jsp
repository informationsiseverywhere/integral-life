<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SH605";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sh605ScreenVars sv = (Sh605ScreenVars) fw.getVariables();
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Override based on Agent OR Details");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "       ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Insurance");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Association");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Code");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "  ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reinstatement");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Interest");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rate");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"              ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Underwriting");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "required");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                    ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Enable Agency");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Movement");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                   ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sales Unit Required                       ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Agent Maintenance Consolidated Cheq Default");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bonus");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Workbench");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Extraction");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>
<%
	StringData generatedText31 = new StringData("Commission Rls upon Acknowledgement Slip  ");
%>
<%
	sv.comind.setClassString("");
%>
<%
	StringData generatedText32 = new StringData("(Y/N)");
%>
<%
	StringData generatedText33 = new StringData("Cooling off period ");
%>
<%
	sv.tdayno.setClassString("");
%>
<%
	StringData generatedText34 = new StringData("days ");
%>
<%
	StringData generatedText35 = new StringData("Date Basis  ");
%>
<%
	sv.ratebas.setClassString("");
%>
<%
	StringData generatedText36 = new StringData("1-Risk commencement date");
%>
<%
	StringData generatedText37 = new StringData("2-Issue date");
%>
<%
	StringData generatedText38 = new StringData("3-Max(Money receive date, U/W approval date)");
%>
<%
	StringData generatedText39 = new StringData("4-Acknowledgement or Deemed Received date");
%>
<%
	StringData generatedText40 = new StringData("5-Acknowledgement or Deemed Rcvd or Issue date");
%>

<%
	{
		if (appVars.ind59.isOn()) {
			sv.agccqind.setReverse(BaseScreenData.REVERSED);
			sv.agccqind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.agccqind.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


</td><td>






						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Override based on Agent OR Details")%></label>
					<div>
						<input type='checkbox' name='indic' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(indic)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.indic).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.indic).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.indic).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck' onclick="handleCheckBox('indic')" />

						<input type='checkbox' name='indic' value=' '
							<%if (!(sv.indic).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('indic')" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Life Insurance Association Company Code")%></label>
					<div style="width: 70px;">
						<input name='liacoy' type='text'
							<%if ((sv.liacoy).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: center" <%}%>
							<%formatValue = (sv.liacoy.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.liacoy.getLength()%>'
							maxLength='<%=sv.liacoy.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(liacoy)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.liacoy).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.liacoy).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.liacoy).getColor() == null ? "input_cell"
						: (sv.liacoy).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reinstatement")%></label>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest")%></label>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate")%></label>
					<div class="input-group" style="min-width:71px;">
						<%=smartHF.getRichText(0, 0, fw, sv.intRate, (sv.intRate.getLength() + 1), COBOLHTMLFormatter.S1VS5)
					.replace("absolute", "relative")%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Underwriting required")%></label>
					<div>
						<input type='checkbox' name='uwind' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(uwind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.uwind).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.uwind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.uwind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck' onclick="handleCheckBox('uwind')" />

						<input type='checkbox' name='uwind' value=' '
							<%if (!(sv.uwind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('uwind')" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Enable Agency Movement")%></label>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Movement")%></label>
					<div>
						<input type='checkbox' name='crtind' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(crtind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.crtind).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.crtind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.crtind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck' onclick="handleCheckBox('crtind')" />

						<input type='checkbox' name='crtind' value=' '
							<%if (!(sv.crtind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('crtind')" />
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Unit Required")%></label>
					<div class="input-group" style="min-width:71px;">
						<input name='tsalesind' type='text'
							<%if ((sv.tsalesind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.tsalesind.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.tsalesind.getLength()%>'
							maxLength='<%=sv.tsalesind.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(tsalesind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.tsalesind).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tsalesind).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tsalesind).getColor() == null ? "input_cell"
						: (sv.tsalesind).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Agent Maintenance Consolidated Cheq Default")%></label>
					<div>
						<input type='checkbox' name='agccqind' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(agccqind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.agccqind).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.agccqind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.agccqind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck'
							onclick="handleCheckBox('agccqind')" /> <input type='checkbox'
							name='agccqind' value=' '
							<%if (!(sv.agccqind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('agccqind')" />
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus")%></label>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Workbench")%></label>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Extraction")%></label>
					<div>
						<input type='checkbox' name='bonusInd' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(bonusInd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.bonusInd).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.bonusInd).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.bonusInd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck'
							onclick="handleCheckBox('bonusInd')" /> <input type='checkbox'
							name='bonusInd' value=' '
							<%if (!(sv.bonusInd).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('bonusInd')" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Commission Rls upon Acknowledgement Slip")%></label>
					<div>
						<input type='checkbox' name='comind' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(comind)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.comind).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.comind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.comind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck' onclick="handleCheckBox('comind')" />

						<input type='checkbox' name='comind' value=' '
							<%if (!(sv.comind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('comind')" />
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cooling off period ")%></label>
					<table>
						<tr>
							<td><input name='tdayno' type='text'
								<%if ((sv.tdayno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%>
								<%formatValue = (sv.tdayno.getFormData()).toString();%>
								value='<%= XSSFilter.escapeHtml(formatValue)%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=formatValue%>' <%}%>
								size='<%=sv.tdayno.getLength()%>'
								maxLength='<%=sv.tdayno.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(tdayno)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.tdayno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.tdayno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.tdayno).getColor() == null ? "input_cell"
						: (sv.tdayno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>></td>
							<td>&nbsp;&nbsp;</td>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle(" days")%></label>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date Basis")%></label>
					<div style="width: 70px;">
						<input name='ratebas' type='text'
							<%if ((sv.ratebas).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.ratebas.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.ratebas.getLength()%>'
							maxLength='<%=sv.ratebas.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(ratebas)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.ratebas).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ratebas).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ratebas).getColor() == null ? "input_cell"
						: (sv.ratebas).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("1-Risk commencement date")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("2-Issue date")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("3-Max(Money receive date, U/W approval date)")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("4-Acknowledgement or Deemed Received date")%></label>
					</div>
					<div>
						<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("5-Acknowledgement or Deemed Rcvd or Issue date")%></label>
					</div>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;" class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
					</div>
					<div>
						<label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
