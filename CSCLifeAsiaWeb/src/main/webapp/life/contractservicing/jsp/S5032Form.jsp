

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5032";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5032ScreenVars sv = (S5032ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Refusal/Reversal");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Increase Enquiry");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No     ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action          ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>					    	
					    		<div class="input-group" style="max-width:150px;">
						    		<input name='chdrsel' 
										id='chdrsel'			    		
										type='text' 
										value='<%=sv.chdrsel.getFormData()%>' 
										maxLength='<%=sv.chdrsel.getLength()%>' 
										size='<%=sv.chdrsel.getLength()%>'
										onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
										
										<% 
											if((new Byte((sv.chdrsel).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
										readonly="true"
										class="output_cell"	 >
										
										<%
											}else if((new Byte((sv.chdrsel).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
											
										%>	
										class="bold_cell" >
										 
										
										<%
											}else { 
										%>
										
										class = ' <%=(sv.chdrsel).getColor()== null  ? 
										"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>' >
										
										<%} %>

						    		<span class="input-group-btn">
										<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
				      			</div>
				    		</div>
			    	</div>
                </div>
              </div>
             </div>
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	 <div class="panel-body">     
			<div class="row">	
			    <div class="col-md-6">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Refusal/Reversal")%></b>
					</label>
				</div>
				<div class="col-md-6">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Increase Enquiry")%></b>
					</label>
				</div>
	           </div>
        </div>
  </div>	
	

<%@ include file="/POLACommon2NEW.jsp"%>

