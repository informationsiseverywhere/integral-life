<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6350";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S6350ScreenVars sv = (S6350ScreenVars) fw.getVariables();%>

<%if (sv.S6350screenWritten.gt(0)) {%>
	<%S6350screen.clearClassString(sv);%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText16)%>


<%}%>

<%if (sv.S6350screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s6350screensfl");
	savedInds = appVars.saveAllInds();
	S6350screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 11;
	String height = smartHF.fmty(11);
	smartHF.setSflLineOffset(12);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(12)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (S6350screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.select.setClassString("");%>
	<%sv.planSuffix.setClassString("");%>
<%	sv.planSuffix.appendClassString("num_fld");
	sv.planSuffix.appendClassString("output_txt");
	sv.planSuffix.appendClassString("highlight");
%>
	<%sv.covRiskStat.setClassString("");%>
<%	sv.covRiskStat.appendClassString("string_fld");
	sv.covRiskStat.appendClassString("output_txt");
	sv.covRiskStat.appendClassString("highlight");
%>
	<%sv.rstatdesc.setClassString("");%>
<%	sv.rstatdesc.appendClassString("string_fld");
	sv.rstatdesc.appendClassString("output_txt");
	sv.rstatdesc.appendClassString("highlight");
%>
	<%sv.covPremStat.setClassString("");%>
<%	sv.covPremStat.appendClassString("string_fld");
	sv.covPremStat.appendClassString("output_txt");
	sv.covPremStat.appendClassString("highlight");
%>
	<%sv.pstatdesc.setClassString("");%>
<%	sv.pstatdesc.appendClassString("string_fld");
	sv.pstatdesc.appendClassString("output_txt");
	sv.pstatdesc.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>
	<%sv.rider.setClassString("");%>
	<%sv.coverage.setClassString("");%>
	<%sv.life.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.select)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 6, sfl, sv.planSuffix)%>
		<%=smartHF.getHTMLSFSpaceVar(sflLine, 12, sfl, sv.covRiskStat)%>
		<%=smartHF.getHTMLF4SSVar(sflLine, 12, sfl, sv.covRiskStat)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 15, sfl, sv.rstatdesc)%>
		<%=smartHF.getHTMLSFSpaceVar(sflLine, 46, sfl, sv.covPremStat)%>
		<%=smartHF.getHTMLF4SSVar(sflLine, 46, sfl, sv.covPremStat)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 49, sfl, sv.pstatdesc)%>




		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S6350screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.S6350protectWritten.gt(0)) {%>
	<%S6350protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S6350screenctlWritten.gt(0)) {%>
	<%S6350screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s6350screensfl");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Selection");%>
<%	generatedText8.appendClassString("label_txt");
	generatedText8.appendClassString("underline");
	generatedText8.appendClassString("highlight");
%>
	<%sv.textfield.setClassString("");%>
<%	sv.textfield.appendClassString("string_fld");
	sv.textfield.appendClassString("output_txt");
	sv.textfield.setColor(BaseScreenData.RED);
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Frequency ");%>
	<%sv.payfreq.setClassString("");%>
<%	sv.payfreq.appendClassString("string_fld");
	sv.payfreq.appendClassString("output_txt");
	sv.payfreq.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date   ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm Date   ");%>
	<%sv.currfromDisp.setClassString("");%>
<%	sv.currfromDisp.appendClassString("string_fld");
	sv.currfromDisp.appendClassString("output_txt");
	sv.currfromDisp.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method    ");%>
	<%sv.mop.setClassString("");%>
<%	sv.mop.appendClassString("string_fld");
	sv.mop.appendClassString("output_txt");
	sv.mop.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed to Date ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel Plan  Risk Status                       Premium Status");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
		if (appVars.ind99.isOn()
		&& !appVars.ind89.isOn()) {
			sv.textfield.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<%=smartHF.getLit(1, 34, generatedText8)%>

	<%=smartHF.getHTMLVar(1, 52, fw, sv.textfield)%>

	<%=smartHF.getLit(3, 3, generatedText1)%>

	<%=smartHF.getHTMLVar(3, 13, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 25, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 25, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.ctypedes)%>

	<%=smartHF.getLit(3, 65, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 75, fw, sv.cntcurr)%>
	<%=smartHF.getHTMLF4NSVar(3, 75, fw, sv.cntcurr)%>

	<%=smartHF.getLit(4, 3, generatedText3)%>

	<%=smartHF.getHTMLVar(4, 20, fw, sv.chdrstatus)%>

	<%=smartHF.getLit(4, 31, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 47, fw, sv.premstatus)%>

	<%=smartHF.getLit(4, 65, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(4, 75, fw, sv.register)%>
	<%=smartHF.getHTMLF4NSVar(4, 75, fw, sv.register)%>

	<%=smartHF.getLit(5, 3, generatedText6)%>

	<%=smartHF.getHTMLVar(5, 20, fw, sv.lifenum)%>

	<%=smartHF.getHTMLVar(5, 31, fw, sv.lifename)%>

	<%=smartHF.getLit(6, 3, generatedText7)%>

	<%=smartHF.getHTMLVar(6, 20, fw, sv.jlife)%>

	<%=smartHF.getHTMLVar(6, 31, fw, sv.jlifename)%>

	<%=smartHF.getLit(8, 2, generatedText10)%>

	<%=smartHF.getHTMLVar(8, 20, fw, sv.numpols, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(8, 31, generatedText11)%>

	<%=smartHF.getHTMLVar(8, 50, fw, sv.payfreq)%>

	<%=smartHF.getLit(8, 53, generatedText14)%>

	<%=smartHF.getHTMLSpaceVar(8, 69, fw, sv.ptdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(8, 69, fw, sv.ptdateDisp)%>

	<%=smartHF.getLit(9, 2, generatedText13)%>

	<%=smartHF.getHTMLSpaceVar(9, 20, fw, sv.currfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 20, fw, sv.currfromDisp)%>

	<%=smartHF.getLit(9, 31, generatedText12)%>

	<%=smartHF.getHTMLSpaceVar(9, 50, fw, sv.mop)%>
	<%=smartHF.getHTMLF4NSVar(9, 50, fw, sv.mop)%>

	<%=smartHF.getLit(9, 53, generatedText15)%>

	<%=smartHF.getHTMLSpaceVar(9, 69, fw, sv.btdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 69, fw, sv.btdateDisp)%>

	<%=smartHF.getLit(11, 2, generatedText9)%>





<%}%>


<%@ include file="/POLACommon2.jsp"%>
