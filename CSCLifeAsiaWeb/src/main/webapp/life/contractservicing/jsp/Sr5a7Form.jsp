<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5a7";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr5a7ScreenVars sv = (Sr5a7ScreenVars) fw.getVariables();%>

<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner");%>
		
	<%
	 if (sv.fatcastatusFlag.toString().trim().equals("Y")) {
   	  sv.fatcastatus.setInvisibility(BaseScreenData.INVISIBLE);
    
     }
	if(appVars.ind02.isOn()){
	sv.feddflag.setEnabled(BaseScreenData.DISABLED);
	}
	else
	{
		sv.feddflag.setEnabled(BaseScreenData.ENABLED);
	}
		
	
	
	if(appVars.ind04.isOn()){
		sv.fpvflag.setEnabled(BaseScreenData.DISABLED);
		}
	else
	{
		sv.fpvflag.setEnabled(BaseScreenData.ENABLED);
	}
	
	 %>

<div class="panel panel-default">
    	<div class="panel-body">
    	
   <div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Number"))%></label> 	
					<table>
					<tr>
					<td>
					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					
					
					
					</td><td style="padding-left:1px;">
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					
					</td><td style="padding-left:1px;max-width:150px;">
					<%if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					
					
					</td>
					</tr>
					</table>
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Status"))%></label> 	
				<div class="input-group">
				
				<%=smartHF.getHTMLVar(0, 0, fw, sv.chdrstatus, true)%>
				</div></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Status"))%></label> 	
				<div class="input-group">
				
				<%=smartHF.getHTMLVar(0, 0, fw, sv.premstatus, true)%> 
				</div></div></div>	
					
	</div>
	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Member"))%></label> 
					<table>
					<tr>
					<td>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%>

					</td><td style="padding-left:1px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.ownername, true)%>					
					
					</td>
					</tr>
					</table>
					</div></div></div>	
					<div class="row">
					 <%if ((new Byte((sv.fatcastatus).getInvisible())).compareTo(new Byte(
	    		BaseScreenData.INVISIBLE)) != 0) { %>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("FATCA Status "))%></label> 
					<table>
					<tr>
					<td>
					<input name='fatcastatus' id='fatcastatus'
type='text'
value='<%=sv.fatcastatus.getFormData()%>'
maxLength='<%=sv.fatcastatus.getLength()+50%>'
size='<%=sv.fatcastatus.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(fatcastatus)' onKeyUp='return checkMaxLength(this)'
style="width:55px;"
<%-- MIBT-77 --%>
readonly="true"  class="output_cell">

					</td><td style="padding-left:1px;">
					<input name='fatcastatusdesc' id='fatcastatusdesc'
type='text'
value='<%=sv.fatcastatusdesc.getFormData()%>'
maxLength='<%=sv.fatcastatusdesc.getLength()+50%>'
size='<%=sv.fatcastatusdesc.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(fatcastatusdesc)' onKeyUp='return checkMaxLength(this)'
style="width:180px;"
<%-- MIBT-77 --%>
readonly="true"  class="output_cell">				
					
					</td>
					</tr>
					</table>
					</div></div>
					<%} %>
					</div>	
					<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label style="margin-top:7px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("EDD Completed"))%></label>
					</div></div>
					<div class="col-md-2">
				<div class="form-group" style="margin-left:-30px;">
					<%     
       if ((new Byte((sv.feddflag).getInvisible())).compareTo(new Byte(
                                                       BaseScreenData.INVISIBLE)) != 0) {
                                         
       if(((sv.feddflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
              longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
       }
       if(((sv.feddflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
       longValue=resourceBundleHandler.gettingValueFromBundle("No");
       }
              
%>

<% 
       if((new Byte((sv.feddflag).getEnabled()))
       .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
                                                "blank_cell" : "output_cell" %>'>  
                     <%if(longValue != null){%>
                     
                     <%=longValue%>
                     
                     <%}%>
          </div>

<%
longValue = null;
%>

       <% }else {%>
       
<% if("red".equals((sv.feddflag).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='feddflag' style="width:70px; "    
       onFocus='doFocus(this)'
       onHelp='return fieldHelp(feddflag)'
       onKeyUp='return checkMaxLength(this)'
<% 
       if((new Byte((sv.feddflag).getEnabled()))
       .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
       readonly="true"
       disabled
       class="output_cell"
<%
       }else if((new Byte((sv.feddflag).getHighLight())).
              compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>     
              class="bold_cell" 
<%
       }else { 
%>
       class = 'input_cell' 
<%
       } 
%>
>

<%-- <option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option> --%>
<option value="Yes"<% if(((sv.feddflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="No"<% if(((sv.feddflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


</select>
<% if("red".equals((sv.feddflag).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					</div></div>
					<div class="col-md-2">
				<div class="form-group">
					<label style="margin-top:7px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Privacy Waiver"))%></label>
					
					</div></div>
					<div class="col-md-2">
				<div class="form-group" style="margin-left:-30px;">
					<%   

       if ((new Byte((sv.fpvflag).getInvisible())).compareTo(new Byte(
                                                       BaseScreenData.INVISIBLE)) != 0) {
                                         
       if(((sv.fpvflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
              longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
       }
       if(((sv.fpvflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
       longValue=resourceBundleHandler.gettingValueFromBundle("No");
       }
              
%>

<% 
       if((new Byte((sv.fpvflag).getEnabled()))
       .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
                                                "blank_cell" : "output_cell" %>'>  
                     <%if(longValue != null){%>
                     
                     <%=longValue%>
                     
                     <%}%>
          </div>

<%
longValue = null;
%>

       <% }else {%>
       
<% if("red".equals((sv.fpvflag).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='fpvflag' style="width:70px; "    
       onFocus='doFocus(this)'
       onHelp='return fieldHelp(fpvflag)'
       onKeyUp='return checkMaxLength(this)'
<% 
       if((new Byte((sv.fpvflag).getEnabled()))
       .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
       readonly="true"
       disabled
       class="output_cell"
<%
       }else if((new Byte((sv.fpvflag).getHighLight())).
              compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>     
              class="bold_cell" 
<%
       }else { 
%>
       class = 'input_cell' 
<%
       } 
%>
>


<option value="Yes"<% if(((sv.fpvflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="No"<% if(((sv.fpvflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


</select>
<% if("red".equals((sv.fpvflag).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
					</div></div>
					
					
					
					
					</div> 
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
 
 
 
 
 
</div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

