

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5049";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5049ScreenVars sv = (S5049ScreenVars) fw.getVariables();%>


<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.rfundflg.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
	
	
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>					    	
					    		<%-- <div class="input-group" style="max-width:170px;">
						    		<input name='chdrsel' 
						    		id='chdrsel'
									type='text' 
									value='<%=sv.chdrsel.getFormData()%>' 
									maxLength='<%=sv.chdrsel.getLength()%>' 
									size='<%=sv.chdrsel.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
									
									<% 
										if((new Byte((sv.chdrsel).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
									%>  
									readonly="true"
									class="output_cell"	 >
									
									<%
										}else if((new Byte((sv.chdrsel).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										
									%>	
									class="bold_cell" >
									 
									
									<%
										}else { 
									%>
									
									class = ' <%=(sv.chdrsel).getColor()== null  ? 
									"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>' >
									
									
									
									<%} %>
															    		

						    		<span class="input-group-btn" style="right: 6px">
										<button class="btn btn-info" style="font-size:19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
				      			</div> --%>
				      			
				      			
				    <%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 150px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                      style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>   			
				    		</div>
			    	</div>
                </div>
              </div>
             </div>
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	 <div class="panel-body">     
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Assignment")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Despatch Address")%></b>
					</label>
				</div>
	           </div>
	           <div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Servicing Agent")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Owners")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Suppress Overdue Processing")%></b>
					</label>
				</div>
	           </div>
	           <div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "G")%><%=resourceBundleHandler.gettingValueFromBundle("Suppress Billing")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "H")%><%=resourceBundleHandler.gettingValueFromBundle("Suppress Overdue Notices")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "I")%><%=resourceBundleHandler.gettingValueFromBundle("Suppress Renewal Commission")%></b>
					</label>
				</div>
	           </div>
	           
	           <div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "J")%><%=resourceBundleHandler.gettingValueFromBundle("Non-forfeiture Option")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "K")%><%=resourceBundleHandler.gettingValueFromBundle("Suppress IBF Interest")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "M")%><%=resourceBundleHandler.gettingValueFromBundle("Change of Dividend Option")%></b>
					</label>
				</div>
	           </div>
	           
	            <div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "N")%><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary/Life Assured Name")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
				<b>		<%=smartHF.buildRadioOption(sv.action, "action", "P")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Trustee")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "Q")%><%=resourceBundleHandler.gettingValueFromBundle("Payout Method / Payee Change")%></b>
					</label>
				</div>
	           </div>

	            <div class="row">
			 <%
				 if (sv.fuflag.compareTo("N") != 0) {
			 %>

			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "T")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Follow Ups")%></b>
					</label>
				</div>

			 <%
				 }
			 %>
 	           <!-- ICIL-658 -->
          	 <%
	           if (sv.rfundflg.getInvisible() != BaseScreenData.INVISIBLE) {
			 %>
			    <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "U")%><%=resourceBundleHandler.gettingValueFromBundle("Refund Overpayment Option")%></b>
					</label>
				</div>
			 <%
				 }
			 %>
 	           </div>
	           <!-- START OF ILIFE-6160 -->
	           	<%
					if (sv.actionflag.compareTo("N") != 0) {		
				%>
				
		            <div class="row">	
				    	<div class="col-md-4">
							<label class="radio-inline">
							<b>	<%=smartHF.buildRadioOption(sv.action, "action", "S")%><%=resourceBundleHandler.gettingValueFromBundle("EDD Privacy Waiver Update")%></b>
							</label>
						</div>
						<div class="col-md-4">
							<label class="radio-inline">
						<b>		<%=smartHF.buildRadioOption(sv.action, "action", "R")%><%=resourceBundleHandler.gettingValueFromBundle("EDD Privacy Waiver Enquiry")%></b>
							</label>
						</div>
		           	</div>
		      	<%
					}
				%>
	           <!-- END OF ILIFE-6160 -->
        </div>
  </div>	

<%@ include file="/POLACommon2NEW.jsp"%>

