<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52F";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr52fScreenVars sv = (Sr52fScreenVars) fw.getVariables();%>
<%{
}%>










<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                        <div class="form-group" style="width:50px">
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>


<div class="col-md-1"> </div>
<div class="col-md-3"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                        <div class="form-group" style="width:100px">
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div></div>
<div class="col-md-1"> </div>

<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                   
  <table><tr><td>
                        
                        <%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' >
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  id="idesc" style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
                        
                      </div></div>





</div>



<div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
  <table>                     
<tr>
<td>

<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>
</td>

<td>
&nbspto&nbsp
</td>

<td Style="width:100px">
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%>
</td>
</tr>
</table>


</div></div>


</div>


<br>
<div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Tax Type")%></label>
                       
                       </div></div>
     
    
     <div class="col-md-4"> 
			    	     <div class="form-group">
                       

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"txtype01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("txtype01");
	optionValue = makeDropDownList( mappedItems , sv.txtype01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.txtype01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.txtype01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.txtype01).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='txtype01' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.txtype01).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.txtype01).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.txtype01).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
            </div></div>
            
            
            <div class="col-md-3"> 
			    	     <div class="form-group">
      
                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"txtype02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("txtype02");
	optionValue = makeDropDownList( mappedItems , sv.txtype02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.txtype02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.txtype02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.txtype02).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='txtype02' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.txtype02).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.txtype02).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.txtype02).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	
	</div></div>
                       </div>

<br><br>

<div class="row">


     <div class="col-md-3"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Amount Up to   ")%></label>
                       

</div></div>
               
               
               
               <div class="col-md-2"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Rate  ")%></label>


</div></div>



<div class="col-md-2"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Fix Amt   ")%></label>


</div></div>



<div class="col-md-2"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Rate   ")%></label>


</div></div>



<div class="col-md-2"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Fix Amt   ")%></label>


</div></div>      


</div>  
                       
                       
                       
                       
                       
                       <div class="row">


     <div class="col-md-3"> 
			    	      
                            <%if(((BaseScreenData)sv.dtyamt01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dtyamt01,( sv.dtyamt01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.dtyamt01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dtyamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>


<div class="col-md-2"> 
			    	      
			    	     <%if(((BaseScreenData)sv.txratea01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txratea01,( sv.txratea01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txratea01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txratea01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>


<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamta1) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamta1,( sv.txfxamta1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamta1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamta1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txrateb01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txrateb01,( sv.txrateb01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txrateb01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txrateb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamtb1) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamtb1,( sv.txfxamtb1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamtb1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamtb1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
			    	     </div>
			    	     
			    	                            <div class="row" style="padding-top:5px;">


     <div class="col-md-3"> 
			    	      
                            <%if(((BaseScreenData)sv.dtyamt02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dtyamt01,( sv.dtyamt01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.dtyamt01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dtyamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>


<div class="col-md-2"> 
			    	      
			    	     <%if(((BaseScreenData)sv.txratea02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txratea01,( sv.txratea01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txratea01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txratea01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>


<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamta2) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamta1,( sv.txfxamta1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamta1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamta1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txrateb02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txrateb01,( sv.txrateb01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txrateb01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txrateb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamtb2) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamtb1,( sv.txfxamtb1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamtb1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamtb1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
			    	     </div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	   <div class="row" style="padding-top:5px;">


     <div class="col-md-3"> 
			    	      
                            <%if(((BaseScreenData)sv.dtyamt03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dtyamt01,( sv.dtyamt01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.dtyamt01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dtyamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>


<div class="col-md-2"> 
			    	      
			    	     <%if(((BaseScreenData)sv.txratea03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txratea01,( sv.txratea01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txratea01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txratea01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>


<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamta3) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamta1,( sv.txfxamta1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamta1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamta1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txrateb03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txrateb01,( sv.txrateb01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txrateb01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txrateb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamtb3) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamtb1,( sv.txfxamtb1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamtb1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamtb1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
			    	     </div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	    <div class="row" style="padding-top:5px;">


     <div class="col-md-3"> 
			    	      
                            <%if(((BaseScreenData)sv.dtyamt04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dtyamt01,( sv.dtyamt01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.dtyamt01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dtyamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>


<div class="col-md-2"> 
			    	      
			    	     <%if(((BaseScreenData)sv.txratea04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txratea01,( sv.txratea01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txratea01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txratea01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>


<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamta4) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamta1,( sv.txfxamta1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamta1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamta1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txrateb04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txrateb01,( sv.txrateb01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txrateb01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txrateb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamtb4) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamtb1,( sv.txfxamtb1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamtb1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamtb1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
			    	     </div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	    <div class="row" style="padding-top:5px;">


     <div class="col-md-3"> 
			    	      
                            <%if(((BaseScreenData)sv.dtyamt05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dtyamt01,( sv.dtyamt01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.dtyamt01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dtyamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>


<div class="col-md-2"> 
			    	      
			    	     <%if(((BaseScreenData)sv.txratea05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txratea01,( sv.txratea01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txratea01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txratea01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>


<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamta5) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamta1,( sv.txfxamta1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamta1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamta1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txrateb05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txrateb01,( sv.txrateb01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txrateb01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txrateb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
<div class="col-md-2"> 			    	      
			    	     <%if(((BaseScreenData)sv.txfxamtb5) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txfxamtb1,( sv.txfxamtb1.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txfxamtb1) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txfxamtb1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
			    	     </div>
			    	     
			    	     
			    	     </div>
			    	     




<br>
<div class="row">

<div class="col-md-4"></div>



     <div class="col-md-2"> 
<label><%=resourceBundleHandler.gettingValueFromBundle("Coy absorb?")%></label>
<div class="form-group" style="width:50px;">
<%if(((BaseScreenData)sv.txabsind01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txabsind01,( sv.txabsind01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txabsind01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txabsind01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div></div>


<div class="col-md-3"></div>


 <div class="col-md-2"> 
<label><%=resourceBundleHandler.gettingValueFromBundle("Coy absorb?")%></label>
<div class="form-group" style="width:50px;">
<%if(((BaseScreenData)sv.txabsind02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txabsind02,( sv.txabsind02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txabsind02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txabsind02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div></div>
           


</div></div>






<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
