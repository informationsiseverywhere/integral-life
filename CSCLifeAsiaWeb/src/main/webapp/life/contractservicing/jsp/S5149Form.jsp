

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5149";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5149ScreenVars sv = (S5149ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Single Premium Top-Up");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Single Premium Top-Up with Increase SA");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Component Inquiry");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No  ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- ILIFE-2722 Life Cross Browser - Sprint 4 D5 : Task 2  starts-->
	<style>
		@media \0screen\,screen\9
		{
			.iconPos{margin-bottom:1px}
		}
		

</style>
<!-- ILIFE-2722 Life Cross Browser - Sprint 4 D5 : Task 2  ends-->

	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group" >  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    		<div class="input-group" style="min-width:120px">
						    		<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div>
			    </div>
			</div>
	</div>
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Single Premium Top-Up")%>
					</b></label>
				</div>
				<div class="col-md-5">			
					<label class="radio-inline">
						<b><%= smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Single Premium Top-Up with Increase SA")%>
					</b></label>			
			    </div>	
			    <div class="col-md-3">			
					<label class="radio-inline">
						<b><%= smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Component Inquiry")%>
					</b></label>			
			    </div>	        
			</div>
			<div class="row">	
			    <div class="col-md-4">			
					<input name='action' 
						type='hidden'
						value='<%=sv.action.getFormData()%>'
						size='<%=sv.action.getLength()%>'
						maxLength='<%=sv.action.getLength()%>' 
						class = "input_cell"
						onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >			
			    </div>
			</div>
		</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>

