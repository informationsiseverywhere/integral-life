<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR57H";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr57hScreenVars sv = (Sr57hScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                     ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Number");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                             ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                        ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account Number ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"              ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                     ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mandref.setReverse(BaseScreenData.REVERSED);
			sv.mandref.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mandref.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class='outerDiv'>
<table>
<!-- TSD 321 STARTs -->
<tr style='height:22px;'>
	<td width='300px'>
		<div class="label_txt"><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></div>
		<br/>
		<input name='payrnum' 
			type='text' 
			value='<%=sv.payrnum.getFormData()%>' 
			maxLength='<%=sv.payrnum.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(payrnum)' onKeyUp='return checkMaxLength(this)'  
			<% 
				if( sv.payrnum.getEnabled() == BaseScreenData.DISABLED ) { 
			%>  
					readonly="true"
					class="output_cell1"	 >
			<%
				}else if( sv.payrnum.getHighLight() == BaseScreenData.BOLD ) {
				
			%>	class="bold_cell" >
			 
			<a href="javascript:;" onClick="doFocus(document.getElementById('payrnum')); changeF4Image(this); doAction('PFKEY04')"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a>
			<%
				}else { 
			%>
				class='<%=(sv.payrnum).getColor()==null?"input_cell":(sv.payrnum).getColor().equals("red")?"input_cell red reverse" : "input_cell" %>' >

				<a href="javascript:;" onClick="doFocus(document.getElementById('payrnum')); changeF4Image(this); doAction('PFKEY04')"> 
					<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a>

			<%} %>
			<!--
			<%
				if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
				} else {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
				}

			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>'>
					<%=formatValue%>
			</div>	
			<%
				longValue = null;
				formatValue = null;
			%>		
	  		-->
			<%					
				if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
				} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
				}
			%>
				<div class='<%=((formatValue==null)||("".equals(formatValue.trim())))?"blank_cell1" : "output_cell1" %>'>
					<%=formatValue%>
				</div>	
		<%
			longValue = null;
			formatValue = null;
		%>
			
	</td>
</tr>
<!-- TSD 321 ENDs -->
<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Mandate Number")%>
</div>



<br/>
 
<input name='mandref' 
type='text' 
value='<%=sv.mandref.getFormData()%>' 
maxLength='<%=sv.mandref.getLength()%>' 
size='<%=sv.mandref.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mandref)' onKeyUp='return checkMaxLength(this)'  

<% 
	if( sv.mandref.getEnabled() == BaseScreenData.DISABLED) { 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if( sv.mandref.getHighLight() == BaseScreenData.BOLD) {
%>	
	class="bold_cell" >
 
	<a href="javascript:;" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
	<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
	</a>

<%
	}else { 
%>

class = ' <%=(sv.mandref).getColor()== null  ? 
"input_cell" :  (sv.mandref).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>


</td>

<td width='251'></td><td width='251'></td></tr><tr style='height:22px;'><td width='753'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%>
</div>



<br/>

<input name='bankkey' 
type='text' 
value='<%=sv.bankkey.getFormData()%>' 
maxLength='<%=sv.bankkey.getLength()%>' 
size='<%=sv.bankkey.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.bankkey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.bankkey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('bankkey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.bankkey).getColor()== null  ? 
"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('bankkey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>






	
  		
		<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

</tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Account Number")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>

</table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>

</tr></table></div><br/></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/btn_continue.png" width='84' height='22' alt='<%=resourceBundleHandler.gettingValueFromBundle("Continue")%>'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/btn_refresh.png" width='84' height='22' alt='<%=resourceBundleHandler.gettingValueFromBundle("Refresh")%>'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="clearFField();changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/btn_previous.png" width='84' height='22' alt='<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/btn_exit.png" width='84' height='22' alt='<%=resourceBundleHandler.gettingValueFromBundle("Exit")%>'></a></li>
</ul>
</div>

<%@ include file="/POLACommon2.jsp"%>

