<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5076";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5076ScreenVars sv = (S5076ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind50.isOn()) {
	sv.jownsel.setEnabled(BaseScreenData.DISABLED);
}
}%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
    	
			<div class="row">	
			
			
					    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    			
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Owner")%></label>	  	                            
	  	                     
				    	        <table><tr>
				    	        <td class="input-group" style="padding-right: 2px;">
					    				<%=smartHF.getRichTextExt( fw, sv.jownsel,(sv.jownsel.getLength()),null).replace("absolute","relative").replace(" bold","").replace("input_cell","bold_cell")%>
								
											
					    			 <%=smartHF.getHTMLF4NSVarExt(fw, sv.jownsel).replace("absolute","relative")%>
									</td>
									<td>	
										<%if ((new Byte((sv.jownname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
											<% if(!((sv.jownname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jownname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}						
												}else{
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.jownname.getFormData()).toString()); 
													}else{
														formatValue = formatValue( longValue);
													}
												}
											%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
										style="min-width:100px;margin-left: -2px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
											<%
											longValue = null;
											formatValue = null;
											%>
										<%}%>

			
										
									</td></tr>
									</table>
									</div>
									
											
								</div>
							</div>
			
			
			
				    	<div class="row">	
				    	<div class="col-md-4">
				
						<div class="form-group">	
						
						
						
<%StringData newLabelgeneratedText2=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Address");%> <!-- ILIFE-3212 -->
<%if ((new Byte((newLabelgeneratedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Address")%>
</label>

<%}%>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%					
				if(!((sv.cltaddr01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:160px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%					
				if(!((sv.cltaddr02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:160px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

</div>

</div>

</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%					
				if(!((sv.cltaddr03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:160px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

</div>

</div>

</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%					
				if(!((sv.cltaddr04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:160px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  

</div>

</div>

</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">
<%					
				if(!((sv.cltaddr05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:160px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

</div>
</div>
</div>
						</div>
				   </div>	 </div>
			    		<div class="row">
			    			<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
							<div class="input-group">
						    		
<%if ((new Byte((sv.cltpcode).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cltpcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:160px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->





<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
