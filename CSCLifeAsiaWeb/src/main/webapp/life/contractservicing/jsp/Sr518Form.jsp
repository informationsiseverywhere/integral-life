
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR518";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sr518ScreenVars sv = (Sr518ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind02.isOn()) {
			sv.zrsumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.zrsumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.frqdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		//ILIFE-1853 Starts vchawda
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind88.isOn()) {//ILJ-47
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}	
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind41.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
			sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
			sv.linstamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.anntind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind33.isOn()) {
			sv.anntind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */

		if (appVars.ind53.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
			//generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.waitperiod.setColor(BaseScreenData.RED);
			sv.waitperiod.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind58.isOn()) {
			sv.waitperiod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.waitperiod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind54.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.bentrm.setColor(BaseScreenData.RED);
			sv.bentrm.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind60.isOn()) {
			sv.bentrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.bentrm.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind55.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind62.isOn()) {
			sv.poltyp.setColor(BaseScreenData.RED);
			sv.poltyp.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind62.isOn()) {
			sv.poltyp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.poltyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind56.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind64.isOn()) {
			sv.prmbasis.setColor(BaseScreenData.RED);
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind64.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind57.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3403-STARTS
		if (appVars.ind66.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind67.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3403-ENDS
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind73.isOn()) {
			sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends

	}
%>

<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>

					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

										if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 320px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
						</tr>
					</table>

				</div>
			</div>

		</div>






		<div class="row">
			<div class="col-md-7">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.lifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.lifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.linsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.linsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px; max-width: 300px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>

				</div>
			</div>



			<div class="col-md-1"></div>




			<div class="col-md-3">
				<div class="form-group">

					<%
						if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>

					<%
						}
					%>

					<%
						if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

							if (!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 50px;">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
							formatValue = null;
					%>

					<%
						}
					%>

				</div>
			</div>







		</div>







		<div class="row">
			<div class="col-md-7">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										} else {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "min-width:65px;padding:1px"
						: "min-width:65px;"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td style="padding-left: 1px;">
								<%
									if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlinsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										} else {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlinsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 300px; min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>

							</td>
						</tr>
					</table>

				</div>
			</div>



			<div class="col-md-1"></div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

					<%
						if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numpols.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numpols.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>






			<div class="col-md-2">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

					<%
						if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.planSuffix.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.planSuffix.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>






		</div>




		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>

					<%
						if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>



				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

					<%
						if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
					<%
						if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>

					<%
						if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statFund");
							longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

					<%
						if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub sect")%></label>

					<%
						if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>

		</div>

		<br>


		<hr>
		<br>



		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>


					<%
						if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 140px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>





				</div>
			</div>



			<%
				if ((new Byte((sv.zrsumin).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Amount")%></label>



					<%
						qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.zrsumin,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zrsumin' type='text'
						<%if ((sv.zrsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zrsumin.getLength(), sv.zrsumin.getScale(), 3)%>'
						maxLength='<%=sv.zrsumin.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zrsumin)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.zrsumin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zrsumin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zrsumin).getColor() == null ? "input_cell"
							: (sv.zrsumin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>
			<%
				}
			%>




			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mortcls");
						optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="width: 140px;">
						<%=longValue%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mortcls).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 202px;">
						<%
							}
						%>
						<select name='mortcls' type='list' style="width: 210px;"
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mortcls).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>


				</div>
			</div>







			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>


					<%
						if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("currcd");
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 140px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>




				</div>
			</div>


		</div>






		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<!-- ILJ-47 Starts -->
               		 <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
        			 <%} %> 
        			<!-- ILJ-47 End -->

					<!-- <div class="input-group"> -->
					<table>
						<tr>
							<td>
								<%	
			qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='riskCessAge' 
type='text'
<%if((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:  63px "<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
	 <%}%>

size='<%= sv.riskCessAge.getLength()%>'
maxLength='<%= sv.riskCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.riskCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskCessAge).getColor()== null  ? 
			"input_cell" :  (sv.riskCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
							</td>
							<td style="padding-left: 1px;">
								<%
									if (((BaseScreenData) sv.riskCessTerm) instanceof StringBase) {
								%>
								<%=smartHF.getRichText(0, 0, fw, sv.riskCessTerm, (sv.riskCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%>
								<%
									} else if (((BaseScreenData) sv.riskCessTerm) instanceof DecimalData) {
								%>
								<%
									if (sv.riskCessTerm.equals(0)) {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
								<%
									} else {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell bold\' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
								<%
									}
								%>
								<%
									} else {
								%> hello <%
									}
								%>
							</td>
						</tr>
					</table>

				</div>
			</div>





			<div class="col-md-3">
				<div class="form-group">
					<!-- ILJ-47 Starts -->
					<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
					<%} else { %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess date"))%></label>
					<%} %>
					<!-- ILJ-47 End -->



					<%
						if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp)%>


					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>


				</div>
			</div>









			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("liencd");
						optionValue = makeDropDownList(mappedItems, sv.liencd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 148px;">
						<%
							}
						%>
						<select name='liencd' type='list' style="width: 146px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.liencd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>




			<%
				if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>

					<%
						if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "waitperiod" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("waitperiod");
								optionValue = makeDropDownList(mappedItems, sv.waitperiod.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.waitperiod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.waitperiod).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 147px;">
						<%
							}
						%>

						<select name='waitperiod' type='list' style="width: 146px;"
							<%if ((new Byte((sv.waitperiod).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.waitperiod).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.waitperiod).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>



				</div>
			</div>
			<%
				}
			%>


		</div>




		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age/Term")%></label>
					<table>
						<tr>
							<td>
								<%
									if (((BaseScreenData) sv.premCessAge) instanceof StringBase) {
								%> <%=smartHF.getRichText(0, 0, fw, sv.premCessAge, (sv.premCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%>
								<%
									} else if (((BaseScreenData) sv.premCessAge) instanceof DecimalData) {
								%>
								<%
									if (sv.premCessAge.equals(0)) {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 63px; \' ")%>
								<%
									} else {
								%> <%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
								<%
									}
								%> <%
 	} else {
 %> hello <%
 	}
 %>
							</td>
							<td style="padding-left: 1px;">
								<%
									if (((BaseScreenData) sv.premCessTerm) instanceof StringBase) {
								%>
								<%=smartHF.getRichText(0, 0, fw, sv.premCessTerm, (sv.premCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%>
								<%
									} else if (((BaseScreenData) sv.premCessTerm) instanceof DecimalData) {
								%>
								<%
									if (sv.premCessTerm.equals(0)) {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
								<%
									} else {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
								<%
									}
								%> <%
 	} else {
 %> hello <%
 	}
 %>
							</td>
						</tr>
					</table>

				</div>
			</div>






			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>




					<%
						if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp)%>


					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>

				</div>
			</div>




			<%
				if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
					<%
						if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "prmbasis" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("prmbasis");
								optionValue = makeDropDownList(mappedItems, sv.prmbasis.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.prmbasis).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 148px;">
						<%
							}
						%>

						<select name='prmbasis' type='list' style="width: 146px;"
							<%if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.prmbasis).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.prmbasis).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>



				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>

					<%
						if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "bentrm" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("bentrm");
								optionValue = makeDropDownList(mappedItems, sv.bentrm.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.bentrm).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 148px;">
						<%
							}
						%>

						<select name='bentrm' type='list' style="width: 146px;"
							<%if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else
							if ((new Byte((sv.bentrm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.bentrm).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>

				</div>
			</div>
			<%
				}
			%>


		</div>




		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age/Term")%></label>
					<table>
						<tr>
							<td>
								<%
									if (((BaseScreenData) sv.benCessAge) instanceof StringBase) {
								%> <%=smartHF.getRichText(0, 0, fw, sv.benCessAge, (sv.benCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%>
								<%
									} else if (((BaseScreenData) sv.benCessAge) instanceof DecimalData) {
								%>
								<%
									if (sv.benCessAge.equals(0)) {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.benCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 63px; \' ")%>
								<%
									} else {
								%> <%=smartHF.getHTMLVar(0, 0, fw, sv.benCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
								<%
									}
								%>
								<%
									} else {
								%> hello <%
									}
								%>

							</td>
							<td style="padding-left: 1px;">
								<%
									if (((BaseScreenData) sv.benCessTerm) instanceof StringBase) {
								%> <%=smartHF.getRichText(0, 0, fw, sv.benCessTerm, (sv.benCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%>
								<%
									} else if (((BaseScreenData) sv.benCessTerm) instanceof DecimalData) {
								%>
								<%
									if (sv.benCessTerm.equals(0)) {
								%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.benCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 40px; \' ")%>
								<%
									} else {
								%> <%=smartHF.getHTMLVar(0, 0, fw, sv.benCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
								<%
									}
								%> <%
 	} else {
 %> hello <%
 	}
 %>

							</td>
						</tr>
					</table>
				</div>
			</div>



			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess date")%></label>



					<%
						if ((new Byte((sv.benCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp)%>


					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="benCessDateDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp, (sv.benCessDateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>


				</div>
			</div>

			<%
				if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>


					<%
						if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dialdownoption" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dialdownoption");
								optionValue = makeDropDownList(mappedItems, sv.dialdownoption.getFormData(), 2,
										resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.dialdownoption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.dialdownoption).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 148px;">
						<%
							}
						%>

						<select name='dialdownoption' type='list' style="width: 146px;"
							<%if ((new Byte((sv.dialdownoption).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.dialdownoption).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.dialdownoption).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>






			<%
				if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
					<%
						if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "poltyp" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("poltyp");
								optionValue = makeDropDownList(mappedItems, sv.poltyp.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.poltyp).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 148px;">
						<%
							}
						%>

						<select name='poltyp' type='list' style="width: 146px;"
							<%if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else
							if ((new Byte((sv.poltyp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.poltyp).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>



				</div>
			</div>
			<%
				}
			%>






		</div>

		<br>


		<hr>
		<br>
		<%
			if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="row">
			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>

					<%
						if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
										.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
					%>
					<%
						if (sv.adjustageamt.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>

			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>

					<%
						if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
					%>
					<%
						if (sv.rateadj.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>

					<%
						if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
					%>
					<%
						if (sv.fltmort.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>

					<%
						if (((BaseScreenData) sv.loadper) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
					%>
					<%
						if (sv.loadper.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>



		</div>

		<%
			}
		%>




		<div class="row">

			<%
				if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
					<%
						if (((BaseScreenData) sv.premadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.premadj, (sv.premadj.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
					%>
					<%
						if (sv.premadj.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>

				</div>
			</div>
			<%
				}
				}
			%>


		
			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
				<!-- Ilife-8323 starts -->
				<%	if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
				<%} 
				
				if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0) {
			%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
				<%} 
					if (((BaseScreenData) sv.zlinstprem) instanceof StringBase) {
					%>
					<!-- Ilife-8323 ends-->
					<%=smartHF.getRichText(0, 0, fw, sv.zlinstprem, (sv.zlinstprem.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zlinstprem) instanceof DecimalData) {
					%>
					<%
						if (sv.zlinstprem.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zlinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zlinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<%
				if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

					<%
						if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem, (sv.zbinstprem.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
					%>
					<%
						if (sv.zbinstprem.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.zbinstprem,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.zbinstprem,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>

				</div>
			</div>
			<%
				}
				}
			%>


			<%
				if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>

					<%
						if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
					%>
					<%
						if (sv.zstpduty01.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>




		</div>






		<div class="row">


			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zrinstPrem' type='text'
						<%if ((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.instPrem.getLength(), sv.instPrem.getScale(), 3)%>'
						maxLength='<%=sv.instPrem.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zrinstPrem)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.instPrem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.instPrem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.instPrem).getColor() == null ? "input_cell"
						: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>


				</div>
			</div>




			<%
				if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='taxamt' type='text'
						<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3)%>'
						maxLength='<%=sv.taxamt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(instPrem)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
							: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<%
				}
			%>







			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%></label>


					<%
						qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.linstamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
					%>

					<input name='linstamt' type='text'
						<%if ((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.linstamt.getLength(), sv.linstamt.getScale(), 3)%>'
						maxLength='<%=sv.linstamt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(linstamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);' style="width: 115px"
						<%if ((new Byte((sv.linstamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.linstamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.linstamt).getColor() == null ? "input_cell"
						: (sv.linstamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>







		</div>


	</div>
</div>



<Div id='mainForm_OPTS' style='visibility: hidden;'>

	<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
	<%=smartHF.getMenuLink(sv.comind,
					resourceBundleHandler.gettingValueFromBundle("Agent Commission Split"))%>
	<%
		if (sv.pbind.getInvisible() != BaseScreenData.INVISIBLE
				|| sv.pbind.getEnabled() != BaseScreenData.DISABLED) {
	%>
	<%=smartHF.getMenuLink(sv.pbind,
						resourceBundleHandler.gettingValueFromBundle("Premium Breakdown"))%>

	<%
		} else if (sv.pbind.getEnabled() == BaseScreenData.DISABLED) {
	%>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></a>
	<%
		}
	%>
	<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"), true)%>
	<%=smartHF.getMenuLink(sv.exclind, resourceBundleHandler.gettingValueFromBundle("Exclusions"))%>


</div>





<%@ include file="/POLACommon2NEW.jsp"%>