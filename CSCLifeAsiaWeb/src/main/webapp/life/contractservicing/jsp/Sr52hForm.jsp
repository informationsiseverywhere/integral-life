<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52H";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr52hScreenVars sv = (Sr52hScreenVars) fw.getVariables();%>
<%{
}%>





<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
	   <div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Taxable Amount")%></label>
              </div></div>
                  
                  
       <div class="col-md-4"> 
			    	     <div class="form-group" style="width:205px">
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.aprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 60)%>
                            
              </div></div>              
                  </div>
                  </div>
                  
                  
                  
                  
                  
                  <div class="row">	
	   <div class="col-md-4"> 
			    	      <div class="form-group">
                       <div class="input-group"> 
                       
                        <%if ((new Byte((sv.descript01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) 

!= 0) {%>
	<% if(!((sv.descript01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.descript01.getFormData

()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( 

(sv.descript01.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : 

"output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
              </div> </div></div>
                 <!--  <div class="col-md-2"></div>  -->
                  
       <div class="col-md-4"> 
			    	      <div class="form-group">
                          <%=smartHF.getHTMLVarExt(fw, sv.taxamt01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 205)%>
               </div>       </div>       
                  </div>
                  







   <div class="row">	
	   <div class="col-md-4"> 
			    	     <div class="form-group">
                        <div class="input-group">  <%if ((new Byte((sv.descript02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) 

!= 0) {%>
	<% if(!((sv.descript02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.descript02.getFormData

()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( 

(sv.descript02.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : 

"output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
              </div></div>
                  </div>
                  
                  
                 <!--  <div class="col-md-2"></div>  -->
                  
                  
       <div class="col-md-4"> 
			    	     <div class="form-group">
                         <%=smartHF.getHTMLVarExt(fw, sv.taxamt02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 205)%>
              </div></div>              
                  </div>
                  





               
                  <div class="row">	
	   <div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Total Tax")%></label>
              </div></div>
                 <!-- <div class="col-md-2"></div>  -->
                  
       <div class="col-md-4"> 
			    	     <div class="form-group">
                           <%=smartHF.getHTMLVarExt(fw, sv.gndtotal, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0, 205)%>
              </div></div>              
                  </div>








</div></div>


<script>
       $(document).ready(function() {
    	   $('#gndtotal').width(109);
    	   $('#aprem').width(109);
    	   $('#taxamt01').width(109);
    	   $('#taxamt02').width(109); 
       });
       </script> 




<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 

ends-->
