<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5086";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S5086ScreenVars sv = (S5086ScreenVars) fw.getVariables();%>

<%if (sv.S5086screenWritten.gt(0)) {%>
	<%S5086screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%sv.chdrnum.setClassString("");%>
	<%sv.cnttype.setClassString("");%>
	<%sv.ctypdesc.setClassString("");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Source ");%>
	<%sv.srcebus.setClassString("");%>
<%	sv.srcebus.appendClassString("string_fld");
	sv.srcebus.appendClassString("output_txt");
	sv.srcebus.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prm Status ");%>
	<%sv.rstate.setClassString("");%>
	<%sv.pstate.setClassString("");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%sv.occdateDisp.setClassString("");%>
<%	sv.occdateDisp.appendClassString("string_fld");
	sv.occdateDisp.appendClassString("output_txt");
	sv.occdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%sv.reptype.setClassString("");%>
<%	sv.reptype.appendClassString("string_fld");
	sv.reptype.appendClassString("output_txt");
	sv.reptype.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%sv.cownnum.setClassString("");%>
	<%sv.ownername.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Prem ");%>
	<%sv.instpramt.setClassString("");%>
<%	sv.instpramt.appendClassString("num_fld");
	sv.instpramt.appendClassString("output_txt");
	sv.instpramt.appendClassString("highlight");
%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Overdue ");%>
	<%sv.lapind.setClassString("");%>
<%	sv.lapind.appendClassString("string_fld");
	sv.lapind.appendClassString("output_txt");
	sv.lapind.appendClassString("highlight");
%>
	<%sv.lapsfromDisp.setClassString("");%>
<%	sv.lapsfromDisp.appendClassString("string_fld");
	sv.lapsfromDisp.appendClassString("output_txt");
	sv.lapsfromDisp.appendClassString("highlight");
%>
	<%sv.lapstoDisp.setClassString("");%>
<%	sv.lapstoDisp.appendClassString("string_fld");
	sv.lapstoDisp.appendClassString("output_txt");
	sv.lapstoDisp.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing ");%>
	<%sv.billind.setClassString("");%>
<%	sv.billind.appendClassString("string_fld");
	sv.billind.appendClassString("output_txt");
	sv.billind.appendClassString("highlight");
%>
	<%sv.billfromDisp.setClassString("");%>
<%	sv.billfromDisp.appendClassString("string_fld");
	sv.billfromDisp.appendClassString("output_txt");
	sv.billfromDisp.appendClassString("highlight");
%>
	<%sv.billtoDisp.setClassString("");%>
<%	sv.billtoDisp.appendClassString("string_fld");
	sv.billtoDisp.appendClassString("output_txt");
	sv.billtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency ");%>
	<%sv.billfreq.setClassString("");%>
<%	sv.billfreq.appendClassString("string_fld");
	sv.billfreq.appendClassString("output_txt");
	sv.billfreq.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Notices ");%>
	<%sv.notind.setClassString("");%>
<%	sv.notind.appendClassString("string_fld");
	sv.notind.appendClassString("output_txt");
	sv.notind.appendClassString("highlight");
%>
	<%sv.notsfromDisp.setClassString("");%>
<%	sv.notsfromDisp.appendClassString("string_fld");
	sv.notsfromDisp.appendClassString("output_txt");
	sv.notsfromDisp.appendClassString("highlight");
%>
	<%sv.notstoDisp.setClassString("");%>
<%	sv.notstoDisp.appendClassString("string_fld");
	sv.notstoDisp.appendClassString("output_txt");
	sv.notstoDisp.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method ");%>
	<%sv.mop.setClassString("");%>
<%	sv.mop.appendClassString("string_fld");
	sv.mop.appendClassString("output_txt");
	sv.mop.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal  ");%>
	<%sv.renind.setClassString("");%>
<%	sv.renind.appendClassString("string_fld");
	sv.renind.appendClassString("output_txt");
	sv.renind.appendClassString("highlight");
%>
	<%sv.rnwlfromDisp.setClassString("");%>
<%	sv.rnwlfromDisp.appendClassString("string_fld");
	sv.rnwlfromDisp.appendClassString("output_txt");
	sv.rnwlfromDisp.appendClassString("highlight");
%>
	<%sv.rnwltoDisp.setClassString("");%>
<%	sv.rnwltoDisp.appendClassString("string_fld");
	sv.rnwltoDisp.appendClassString("output_txt");
	sv.rnwltoDisp.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date  ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonuses ");%>
	<%sv.bnsind.setClassString("");%>
<%	sv.bnsind.appendClassString("string_fld");
	sv.bnsind.appendClassString("output_txt");
	sv.bnsind.appendClassString("highlight");
%>
	<%sv.bnsfromDisp.setClassString("");%>
<%	sv.bnsfromDisp.appendClassString("string_fld");
	sv.bnsfromDisp.appendClassString("output_txt");
	sv.bnsfromDisp.appendClassString("highlight");
%>
	<%sv.bnstoDisp.setClassString("");%>
<%	sv.bnstoDisp.appendClassString("string_fld");
	sv.bnstoDisp.appendClassString("output_txt");
	sv.bnstoDisp.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to Date  ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Details ---------------------------------------------------- ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suppress Commission ");%>
	<%sv.comind.setClassString("");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From ");%>
	<%sv.currfromDisp.setClassString("");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%sv.currtoDisp.setClassString("");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason ");%>
	<%sv.reasoncd.setClassString("");%>
	<%sv.resndesc.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cnttype.setReverse(BaseScreenData.REVERSED);
			sv.cnttype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cnttype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ctypdesc.setReverse(BaseScreenData.REVERSED);
			sv.ctypdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ctypdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.rstate.setReverse(BaseScreenData.REVERSED);
			sv.rstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.rstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pstate.setReverse(BaseScreenData.REVERSED);
			sv.pstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cownnum.setReverse(BaseScreenData.REVERSED);
			sv.cownnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cownnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ownername.setReverse(BaseScreenData.REVERSED);
			sv.ownername.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ownername.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.currfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.currfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.currfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.currtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.currtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.currtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 2, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 19, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 30, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 30, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 35, fw, sv.ctypdesc)%>

	<%=smartHF.getLit(3, 67, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(3, 77, fw, sv.srcebus)%>
	<%=smartHF.getHTMLF4NSVar(3, 77, fw, sv.srcebus)%>

	<%=smartHF.getLit(4, 2, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 19, fw, sv.rstate)%>

	<%=smartHF.getHTMLVar(4, 35, fw, sv.pstate)%>

	<%=smartHF.getLit(4, 49, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(4, 55, fw, sv.occdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(4, 55, fw, sv.occdateDisp)%>

	<%=smartHF.getLit(4, 67, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(4, 77, fw, sv.reptype)%>
	<%=smartHF.getHTMLF4NSVar(4, 77, fw, sv.reptype)%>

	<%=smartHF.getLit(5, 67, generatedText7)%>

	<%=smartHF.getHTMLSpaceVar(5, 77, fw, sv.register)%>
	<%=smartHF.getHTMLF4NSVar(5, 77, fw, sv.register)%>

	<%=smartHF.getLit(6, 2, generatedText8)%>

	<%=smartHF.getHTMLVar(6, 19, fw, sv.cownnum)%>

	<%=smartHF.getHTMLVar(6, 30, fw, sv.ownername)%>

	<%=smartHF.getLit(8, 60, generatedText9)%>

	<%=smartHF.getLit(8, 74, generatedText10)%>

	<%=smartHF.getLit(9, 2, generatedText11)%>

	<%=smartHF.getHTMLVar(9, 19, fw, sv.instpramt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(9, 43, generatedText27)%>

	<%=smartHF.getHTMLVar(9, 52, fw, sv.lapind)%>

	<%=smartHF.getHTMLSpaceVar(9, 57, fw, sv.lapsfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 57, fw, sv.lapsfromDisp)%>

	<%=smartHF.getHTMLSpaceVar(9, 70, fw, sv.lapstoDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 70, fw, sv.lapstoDisp)%>

	<%=smartHF.getLit(10, 2, generatedText12)%>

	<%=smartHF.getHTMLSpaceVar(10, 19, fw, sv.cntcurr)%>
	<%=smartHF.getHTMLF4NSVar(10, 19, fw, sv.cntcurr)%>

	<%=smartHF.getLit(10, 43, generatedText14)%>

	<%=smartHF.getHTMLVar(10, 52, fw, sv.billind)%>

	<%=smartHF.getHTMLSpaceVar(10, 57, fw, sv.billfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(10, 57, fw, sv.billfromDisp)%>

	<%=smartHF.getHTMLSpaceVar(10, 70, fw, sv.billtoDisp)%>
	<%=smartHF.getHTMLCalNSVar(10, 70, fw, sv.billtoDisp)%>

	<%=smartHF.getLit(11, 2, generatedText13)%>

	<%=smartHF.getHTMLSpaceVar(11, 19, fw, sv.billfreq)%>
	<%=smartHF.getHTMLF4NSVar(11, 19, fw, sv.billfreq)%>

	<%=smartHF.getLit(11, 43, generatedText17)%>

	<%=smartHF.getHTMLVar(11, 52, fw, sv.notind)%>

	<%=smartHF.getHTMLSpaceVar(11, 57, fw, sv.notsfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(11, 57, fw, sv.notsfromDisp)%>

	<%=smartHF.getHTMLSpaceVar(11, 70, fw, sv.notstoDisp)%>
	<%=smartHF.getHTMLCalNSVar(11, 70, fw, sv.notstoDisp)%>

	<%=smartHF.getLit(12, 2, generatedText15)%>

	<%=smartHF.getLit(12, 10, generatedText16)%>

	<%=smartHF.getHTMLSpaceVar(12, 19, fw, sv.mop)%>
	<%=smartHF.getHTMLF4NSVar(12, 19, fw, sv.mop)%>

	<%=smartHF.getLit(12, 43, generatedText19)%>

	<%=smartHF.getHTMLVar(12, 52, fw, sv.renind)%>

	<%=smartHF.getHTMLSpaceVar(12, 57, fw, sv.rnwlfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(12, 57, fw, sv.rnwlfromDisp)%>

	<%=smartHF.getHTMLSpaceVar(12, 70, fw, sv.rnwltoDisp)%>
	<%=smartHF.getHTMLCalNSVar(12, 70, fw, sv.rnwltoDisp)%>

	<%=smartHF.getLit(13, 2, generatedText18)%>

	<%=smartHF.getHTMLSpaceVar(13, 19, fw, sv.ptdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(13, 19, fw, sv.ptdateDisp)%>

	<%=smartHF.getLit(13, 43, generatedText21)%>

	<%=smartHF.getHTMLVar(13, 52, fw, sv.bnsind)%>

	<%=smartHF.getHTMLSpaceVar(13, 57, fw, sv.bnsfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(13, 57, fw, sv.bnsfromDisp)%>

	<%=smartHF.getHTMLSpaceVar(13, 70, fw, sv.bnstoDisp)%>
	<%=smartHF.getHTMLCalNSVar(13, 70, fw, sv.bnstoDisp)%>

	<%=smartHF.getLit(14, 2, generatedText20)%>

	<%=smartHF.getHTMLSpaceVar(14, 19, fw, sv.btdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(14, 19, fw, sv.btdateDisp)%>

	<%=smartHF.getLit(17, 2, generatedText22)%>

	<%=smartHF.getLit(19, 2, generatedText23)%>

	<%=smartHF.getHTMLVar(19, 23, fw, sv.comind)%>

	<%=smartHF.getLit(19, 30, generatedText24)%>

	<%=smartHF.getHTMLSpaceVar(19, 37, fw, sv.currfromDisp)%>
	<%=smartHF.getHTMLCalNSVar(19, 37, fw, sv.currfromDisp)%>

	<%=smartHF.getLit(19, 52, generatedText25)%>

	<%=smartHF.getHTMLSpaceVar(19, 57, fw, sv.currtoDisp)%>
	<%=smartHF.getHTMLCalNSVar(19, 57, fw, sv.currtoDisp)%>

	<%=smartHF.getLit(20, 15, generatedText26)%>

	<%=smartHF.getHTMLSpaceVar(20, 23, fw, sv.reasoncd)%>
	<%=smartHF.getHTMLF4NSVar(20, 23, fw, sv.reasoncd)%>

	<%=smartHF.getHTMLVar(20, 30, fw, sv.resndesc)%>




<%}%>

<%if (sv.S5086protectWritten.gt(0)) {%>
	<%S5086protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2.jsp"%>
