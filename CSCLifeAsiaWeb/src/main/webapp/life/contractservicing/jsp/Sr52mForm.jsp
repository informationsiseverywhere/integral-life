<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52M";%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %><!---Ticket ILIFE-758 ends-->
<%Sr52mScreenVars sv = (Sr52mScreenVars) fw.getVariables();%>
<%{
}%>






<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                        
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:40px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>



<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                        
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:80px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>




<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                       <table><tr><td>
                        <%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' >
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  id="idesc" style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
       </td></tr></table>                 
                        
                        
                       </div></div>
                        
                        



</div>




		 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Refund Basis")%></label>
                        
<div  style="width:40px">
<%if(((BaseScreenData)sv.ratebas) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.ratebas,( sv.ratebas.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.ratebas) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.ratebas, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%></div>

<label><%=resourceBundleHandler.gettingValueFromBundle("( P - Refund full premium)")%> </label> <br>
<label><%=resourceBundleHandler.gettingValueFromBundle("(U - Refund unit )")%></label>
</div></div>




		    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("If Refund Unit NAV Basis")%></label>
<div  style="width:40px">
<%if(((BaseScreenData)sv.basind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.basind,( sv.basind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.basind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.basind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>

<label><%=resourceBundleHandler.gettingValueFromBundle("(1 - Issue date)")%> </label> <br>

<label><%=resourceBundleHandler.gettingValueFromBundle("(2 - Cancellation date)")%>  </label><br>

<label><%=resourceBundleHandler.gettingValueFromBundle("(3 - Input date)")%></label>

</div></div>




		    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Editable")%></label>

<div  style="width:40px">
<%if(((BaseScreenData)sv.fieldEditCode) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fieldEditCode,( sv.fieldEditCode.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fieldEditCode) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fieldEditCode, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
<label> <%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>

</div></div>

</div>








		 <div class="row">	
			    	<div class="col-md-8"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Medical Fee Clawback(Refer Medical Bill Payment )")%></label>
           
             </div></div>
      </div>





		 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage from Agent")%></label>
<div class="input-group" style="width:65px">
<%if(((BaseScreenData)sv.medpc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.medpc01,( sv.medpc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.medpc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.medpc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
     </div>
             
             </div></div>
             
             
             			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage from Assured")%></label>
<div  style="width:65px">
<%if(((BaseScreenData)sv.medpc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.medpc02,( sv.medpc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.medpc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.medpc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
     </div>
             
             </div></div>




      </div>
      
      
      		 <div class="row">	
			    	<div class="col-md-8"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Adminstration Charges Clawback")%></label>
           
             </div></div>
      </div>
      



		 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Admin Charges")%></label>
<div  style="width:65px">
<%if(((BaseScreenData)sv.admfee) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.admfee,( sv.admfee.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.admfee) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.admfee, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
     </div>
             
             </div></div>
             
             
             			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage from Agent")%></label>

<div  style="width:65px">
<%if(((BaseScreenData)sv.admpc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.admpc01,( sv.admpc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.admpc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.admpc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
    </div> 
             
             </div></div>



             			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage from Assured")%></label>
<div  style="width:65px">
<%if(((BaseScreenData)sv.admpc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.admpc02,( sv.admpc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.admpc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.admpc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
     
   </div>          
             </div></div>
</div>



</div></div>





<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
