
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR51J";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr51jScreenVars sv = (Sr51jScreenVars) fw.getVariables();%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Validation Errors Detected");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J-Life");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cover");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payer");%>
<%		appVars.rollup(new int[] {93});
%>




<div class="panel panel-default">
    	    
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	
				    		<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<%} %>
					    		<table><tr><td>
					    		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td><td style="padding-left:1px;">



<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td><td style="padding-left:1px;">




<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:270px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

		</td></tr></table>			      	        
			                     					    		
				      		</div>
				    </div>
         </div>
         
         <br>
         
         <div class="row">
			<div class="col-md-12">
				
					
					<div class="table-responsive">
	         <table  id='dataTables-sr51j' class="table table-striped table-bordered table-hover"  width='100%'>
               <thead>
		
			        <tr class="info">
			        <th><center><%=resourceBundleHandler.gettingValueFromBundle("Validation Errors Detected")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Life")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Cover")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Payer")%></center></th>
			     	 
		 	        </tr>
			 </thead>
			 

 <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
/* int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Validation Errors Detected").length() >= (sv.erordsc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Validation Errors Detected").length())*12;								
			} else {		
				calculatedValue = (sv.erordsc.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Life").length() >= (sv.life.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Life").length())*12;								
			} else {		
				calculatedValue = (sv.life.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("J-Life").length() >= (sv.jlife.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("J-Life").length())*12;								
			} else {		
				calculatedValue = (sv.jlife.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Cover").length() >= (sv.coverage.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Cover").length())*12;								
			} else {		
				calculatedValue = (sv.coverage.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Rider").length() >= (sv.rider.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Rider").length())*12;								
			} else {		
				calculatedValue = (sv.rider.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Payer").length() >= (sv.payrseqno.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Payer").length())*12;								
			} else {		
				calculatedValue = (sv.payrseqno.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue; */
			%>
		<%
		GeneralTable sfl = fw.getTable("sr51jscreensfl");
		/* int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	 */
		%>
<tbody>
   <%
	Sr51jscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr51jscreensfl
	.hasMoreScreenRows(sfl)) {	
%>

		







	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.erordsc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td  
					<%if((sv.erordsc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
								
											
									
											
						<%= sv.erordsc.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td  >
					</td>														
										
					<%}%>
								<%if((new Byte((sv.life).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:200px;" 
					<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.life.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td  >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.jlife).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td 
					<%if((sv.jlife).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.jlife.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td   
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td  >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.rider).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td  
					<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td  >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.payrseqno).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td  
					<%if((sv.payrseqno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.payrseqno).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.payrseqno);
							if(!(sv.payrseqno).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td  >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
		count = count + 1;
		Sr51jscreensfl
		.setNextScreenRow(sfl, appVars, sv);
	}
	%>

	
</tbody>
		</table>
		</div>
</div></div>




</div></div> 

<script>
       $(document).ready(function() {
              $('#dataTables-sr51j').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script> 





<%@ include file="/POLACommon2NEW.jsp"%>

