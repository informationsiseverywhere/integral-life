<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR575";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr575ScreenVars sv = (Sr575ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. fund ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Section ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Policies ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy number ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to 1");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum assured ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity age/term ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity date ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium cessation age/term ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium  date ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life (J/L) ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Commission Split ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms ");%> 
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium with Tax ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Details ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prorated Prem to be collected  ");%>



<%{
		appVars.rolldown(new int[] {27});
		appVars.rollup(new int[] {27});
		if (appVars.ind33.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind42.isOn()) {
			sv.planSuffix.setColor(BaseScreenData.WHITE);
		}
		if (appVars.ind42.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind42.isOn()) {
			generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.sumin.setReverse(BaseScreenData.REVERSED);
			sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.matage.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.matage.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.matage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.matage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.mattrm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind15.isOn()) {
			sv.mattrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.mattrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.mattrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.mattcessDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind18.isOn()) {
			sv.mattcessDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.mattcessDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.mattcessDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.premcessDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind11.isOn()) {
			sv.premcessDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.premcessDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.premcessDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind23.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.singlePremium.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.singlePremium.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind26.isOn()) {
			sv.singlePremium.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.singlePremium.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind35.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind34.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind41.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.comind.setEnabled(BaseScreenData.DISABLED);
			sv.comind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind41.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		if (appVars.ind54.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
							}

	if (appVars.ind59.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
							}
		if (appVars.ind60.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
							}
		if (appVars.ind46.isOn()) {
			generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
							}
		if (appVars.ind47.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind55.isOn()) {
			sv.taxamt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind46.isOn()) {
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);	
		}
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		
		//ILIFE-3403-STARTS
		if (appVars.ind57.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3403-ENDS
	}

	%>
<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
.bold_cell{margin-left:1px}
.blank_cell{margin-left:1px}
}
 /*ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts*/
@media screen and (-webkit-min-device-pixel-ratio:0) {
.blank_cell{padding-right:1px}
}
@media all and (-ms-high-contrast:none) {
.blank_cell{padding-right:1px}}
/*ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends*/
</style>










<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no")%></label>
    	 					
    	 					<TABLE><TR><TD>
    	 					
    	 					<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</TD><td style="min-width:1px"></td><td>
  <%  	if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  			</TD><td style="min-width:1px"></td><td>
<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
	

    	 					
    	 					</td></TR></TABLE></div></div>
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
    	 				<table><tr><td>

	
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



	</TD><td style="min-width:1px"></td><td>

	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 300px;min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

    	 					</td></tr></table></div></div>
    	 					
    	 					
    	 					
    	 					<div class="col-md-1"> </div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					
    	 					
    	 			<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id='zagelit' class='label_txt'class='label_txt'  
				onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
  
			
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 						<table><tr><td>

		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



	</TD><td style="min-width:1px"></td><td>

	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 300px;min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 					</td></tr></table></div></div>
    	 					
    	 					
    	 					
    	 						<div class="col-md-1"> 
			    	    </div>
			    	     
			    	     
    	 					
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

	<%					
		if(!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			    	     
			    	     </div></div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     <div class="col-md-2"> 
			    	     <div class="form-group">
    	 					
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy number")%></label>

	<%					
		if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>  

    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
    	 					
	<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
	<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
	<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>
<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>				
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

	<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>


<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:90px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	
    	 					
    	 				</div></div>
    	 				
    	 				</div>
    	 					
    	 				<br>  	 					
    	 					
    	 					
    	 				<hr>
    	 				<br>	
    	 					
    	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>

  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 				
    	 				
    	 				</div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>	


<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='sumin' 
type='text'
<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left;width:145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%= sv.sumin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    
    <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>	

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:140px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mortcls).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
<%
} 
%>

<select name='mortcls' type='list' style="width:200px;"
<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mortcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mortcls).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td width='181'>
<%
longValue = null; 
formatValue = null;
%>
    	 					
    	 					</div></div>			
    	 					
    	 <div class="col-md-3 "> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>	
    	 					

<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
    	 					
    	 					
    	 					</div></div>
    	 						 				
    	 				
    	 				</div>	
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity age/term")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>

			    	<%	
			qpsf = fw.getFieldXMLDef((sv.matage).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='matage' 
type='text'
<%if((sv.matage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.matage) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.matage);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.matage) %>'
	 <%}%>

size='<%= sv.matage.getLength()%>'
maxLength='<%= sv.matage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(matage)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.matage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.matage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.matage).getColor()== null  ? 
			"input_cell" :  (sv.matage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td>
<td>

	




	<%	
			qpsf = fw.getFieldXMLDef((sv.mattrm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mattrm' 
type='text'
<%if((sv.mattrm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;margin-left: 1px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mattrm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mattrm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mattrm) %>'
	 <%}%>

size='<%= sv.mattrm.getLength()%>'
maxLength='<%= sv.mattrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mattrm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mattrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mattrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mattrm).getColor()== null  ? 
			"input_cell" :  (sv.mattrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>		
   
    	 </td>
    	 </tr>
    	 </table>
    	 
    	 			</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity date")%></label>
    	 					<div class="input-group">
<% if ((new Byte((sv.mattcessDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-4">
                                   <%=smartHF.getRichTextDateInput(fw, sv.mattcessDisp)%>
                                       
                                 </div>
                                         <%}else{%>
                                  <div class="input-group date form_date col-md-12" data-date=""
                                         data-date-format="dd/mm/yyyy" data-link-field="mattcessDisp"
                                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                                          <%=smartHF.getRichTextDateInput(fw, sv.mattcessDisp, (sv.mattcessDisp.getLength()))%>
                                           <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                           </span>
                                  </div>
                                  
                                  <%}%>



</div></div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
    	 					
<div class="input-group">
<%	
	longValue = sv.liencd.getFormData();  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:140px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<%
                                         if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div style="width: 140px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.liencd)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 140px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('liencd')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>		

<% } %>
</div></div></div></div>
    	 					
    	 					
    	 				
    	 						<div class="row"> 
			    	        	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium cessation age/term")%></label>
    	 					
    	 					<table>
    	 					<tr>
    	 					<td>


	<%	
			qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='premCessAge' 
type='text'
<%if((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
	 <%}%>

size='<%= sv.premCessAge.getLength()%>'
maxLength='<%= sv.premCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessAge).getColor()== null  ? 
			"input_cell" :  (sv.premCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td>
<td>

	<%	
			qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='premCessTerm' 
type='text'
<%if((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;margin-left: 1px;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessTerm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
	 <%}%>

size='<%= sv.premCessTerm.getLength()%>'
maxLength='<%= sv.premCessTerm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessTerm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessTerm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessTerm).getColor()== null  ? 
			"input_cell" :  (sv.premCessTerm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 	
    	 	</td>
    	 	</tr>
    	 	</table>				
    	 					</div></div>
    	 					
    	 			
    	 					
    	 			<div class="col-md-2"> 
			    	     <div class="form-group"style="min-width:150px;" >		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium cessation date")%></label>
    <div class="input-group">
<% if ((new Byte((sv.premcessDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-4">
                                   <%=smartHF.getRichTextDateInput(fw, sv.premcessDisp)%>
                                       
                                 </div>
                                         <%}else{%>
                                  <div class="input-group date form_date col-md-12" data-date=""
                                         data-date-format="dd/mm/yyyy" data-link-field="premcessDisp"
                                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                                          <%=smartHF.getRichTextDateInput(fw, sv.premcessDisp, (sv.premcessDisp.getLength()))%>
                                           <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                           </span>
                                  </div>
                                  
                                  <%}%>
	

</div>
	
    	 				</div></div></div>
    	 				<br>	
    	 				
    	 				
    	 					
    	 					
    	 					
    	 				<hr>	
    	 				<br>	
    	 					
    	<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %> 					
    	 <div class="row">
    	<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
    	 					
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'min-width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div>  <%} %>
                        
                  <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>         </div></div><%} %>
                        
                        
                        
                   <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>   </div></div><%} %>
                        
                        
                    <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                        </div>   <%} %>	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					 <div class="row">
    	 					 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 					<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%} }%>	
    	 					
    	 					
    	 				<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">	
			    	     <!-- ILIFE-8323 Start -->	    
 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
<%} %>
 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0){ %>
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
<%} %>
<!-- ILIFE-8323 ends -->
<%if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
<%if(sv.zlinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: left;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%} %>
    	 				<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>
<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'min-width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'min-width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>    	 					
    	 					</div></div><%}} %>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					    	 					 <div class="row">
    	 					    	 					 
    	 					    	 					 
    	 					    	 					 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
<%	
			qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='singlePremium' 
type='text'
<%if((sv.singlePremium).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left;width: 145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.singlePremium.getLength(), sv.singlePremium.getScale(),3)%>'
maxLength='<%= sv.singlePremium.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(singlePremium)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.singlePremium).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.singlePremium).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.singlePremium).getColor()== null  ? 
			"input_cell" :  (sv.singlePremium).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>

<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

								
<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());


			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	}%>

<input name='taxamt' 
type='text'
<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:left;width: 145px !important; "<% }%>

	value='<%=valueThis %>'
			 <%

	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)-3%>'
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="readonly"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>

	

    	 					
    	 					</div></div><%}%>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 						<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prorated Prem to be collected")%></label>
<div class="form-group" style="width:130px">

	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
			
	%>

<input name='instPrem' 
type='text'
<%if((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:left;width: 145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%	  
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)%>'
maxLength='<%= sv.instPrem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 					
    	 					</div></div></div>
    	 					
    	 					
    	 					
    	 						
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					


</div></div>



<BODY >
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<input name='comind' id='comind' type='hidden'  value="<%=sv.comind
.getFormData()%>">
	<%if (sv.comind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.comind.getEnabled() != BaseScreenData.DISABLED){%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))' class="hyperLink"> 
										<%=resourceBundleHandler
					.gettingValueFromBundle("Agent Commission Split")%> <%
									 	}
									 %>
									<!-- icon -->
									<%
										if (sv.comind.getFormData().equals("+")) {
									%>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.comind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind
.getFormData()%>">
<%if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optextind.getEnabled() != BaseScreenData.DISABLED){%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%> <%
									 	}
									 %>
									<!-- icon -->
									<%
									 	if (sv.optextind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.optextind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
									<!-- text -->
									<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%><a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Tax Details")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.taxind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.taxind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
							
							</ul>
					</span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>










<%@ include file="/POLACommon2NEW.jsp"%>