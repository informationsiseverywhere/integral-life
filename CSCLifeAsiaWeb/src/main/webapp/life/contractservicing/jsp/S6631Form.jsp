<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6631";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S6631ScreenVars sv = (S6631ScreenVars) fw.getVariables();%>

<%if (sv.S6631screenWritten.gt(0)) {%>
	<%S6631screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Months Premiums Required     ");%>
	<%sv.durmnth.setClassString("");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Auto Frequency Alteration            ");%>
	<%sv.autofreq.setClassString("");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method Change");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To     ");%>
	<%sv.payto.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From     ");%>
	<%sv.payfrom01.setClassString("");%>
	<%sv.payfrom02.setClassString("");%>
	<%sv.payfrom03.setClassString("");%>
	<%sv.payfrom04.setClassString("");%>
	<%sv.payfrom05.setClassString("");%>
	<%sv.payfrom06.setClassString("");%>
	<%sv.payfrom07.setClassString("");%>
	<%sv.payfrom08.setClassString("");%>
	<%sv.payfrom09.setClassString("");%>
	<%sv.payfrom10.setClassString("");%>
	<%sv.payfrom11.setClassString("");%>
	<%sv.payfrom12.setClassString("");%>
	<%sv.payfrom13.setClassString("");%>
	<%sv.payfrom14.setClassString("");%>
	<%sv.payfrom15.setClassString("");%>
	<%sv.payfrom16.setClassString("");%>
	<%sv.payfrom17.setClassString("");%>
	<%sv.payfrom18.setClassString("");%>
	<%sv.payfrom19.setClassString("");%>
	<%sv.payfrom20.setClassString("");%>
	<%sv.payfrom21.setClassString("");%>
	<%sv.payfrom22.setClassString("");%>
	<%sv.payfrom23.setClassString("");%>
	<%sv.payfrom24.setClassString("");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lapse Following Benefits     ");%>
	<%sv.crtable01.setClassString("");%>
	<%sv.crtable02.setClassString("");%>
	<%sv.crtable03.setClassString("");%>
	<%sv.crtable04.setClassString("");%>
	<%sv.crtable05.setClassString("");%>
	<%sv.crtable06.setClassString("");%>
	<%sv.crtable07.setClassString("");%>
	<%sv.crtable08.setClassString("");%>
	<%sv.crtable09.setClassString("");%>
	<%sv.crtable10.setClassString("");%>
	<%sv.crtable11.setClassString("");%>
	<%sv.crtable12.setClassString("");%>
	<%sv.crtable13.setClassString("");%>
	<%sv.crtable14.setClassString("");%>
	<%sv.crtable15.setClassString("");%>
	<%sv.crtable16.setClassString("");%>
	<%sv.crtable17.setClassString("");%>
	<%sv.crtable18.setClassString("");%>
	<%sv.crtable19.setClassString("");%>
	<%sv.crtable20.setClassString("");%>
	<%sv.crtable21.setClassString("");%>
	<%sv.crtable22.setClassString("");%>
	<%sv.crtable23.setClassString("");%>
	<%sv.crtable24.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.durmnth.setReverse(BaseScreenData.REVERSED);
			sv.durmnth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.durmnth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.autofreq.setReverse(BaseScreenData.REVERSED);
			sv.autofreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.autofreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.crtable01.setReverse(BaseScreenData.REVERSED);
			sv.crtable01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.crtable01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.crtable02.setReverse(BaseScreenData.REVERSED);
			sv.crtable02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.crtable02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.crtable03.setReverse(BaseScreenData.REVERSED);
			sv.crtable03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.crtable03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.crtable04.setReverse(BaseScreenData.REVERSED);
			sv.crtable04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.crtable04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.crtable05.setReverse(BaseScreenData.REVERSED);
			sv.crtable05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.crtable05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.crtable06.setReverse(BaseScreenData.REVERSED);
			sv.crtable06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.crtable06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.crtable07.setReverse(BaseScreenData.REVERSED);
			sv.crtable07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.crtable07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.crtable08.setReverse(BaseScreenData.REVERSED);
			sv.crtable08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.crtable08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.crtable09.setReverse(BaseScreenData.REVERSED);
			sv.crtable09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.crtable09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.crtable10.setReverse(BaseScreenData.REVERSED);
			sv.crtable10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.crtable10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.crtable11.setReverse(BaseScreenData.REVERSED);
			sv.crtable11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.crtable11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.crtable12.setReverse(BaseScreenData.REVERSED);
			sv.crtable12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.crtable12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.crtable13.setReverse(BaseScreenData.REVERSED);
			sv.crtable13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.crtable13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.crtable14.setReverse(BaseScreenData.REVERSED);
			sv.crtable14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.crtable14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.crtable15.setReverse(BaseScreenData.REVERSED);
			sv.crtable15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.crtable15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.crtable16.setReverse(BaseScreenData.REVERSED);
			sv.crtable16.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.crtable16.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.crtable17.setReverse(BaseScreenData.REVERSED);
			sv.crtable17.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.crtable17.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.crtable18.setReverse(BaseScreenData.REVERSED);
			sv.crtable18.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.crtable18.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.crtable19.setReverse(BaseScreenData.REVERSED);
			sv.crtable19.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.crtable19.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.crtable20.setReverse(BaseScreenData.REVERSED);
			sv.crtable20.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.crtable20.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.crtable21.setReverse(BaseScreenData.REVERSED);
			sv.crtable21.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.crtable21.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.crtable22.setReverse(BaseScreenData.REVERSED);
			sv.crtable22.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.crtable22.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.crtable23.setReverse(BaseScreenData.REVERSED);
			sv.crtable23.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.crtable23.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.crtable24.setReverse(BaseScreenData.REVERSED);
			sv.crtable24.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.crtable24.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.payto.setReverse(BaseScreenData.REVERSED);
			sv.payto.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.payto.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.payfrom01.setReverse(BaseScreenData.REVERSED);
			sv.payfrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.payfrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payfrom02.setReverse(BaseScreenData.REVERSED);
			sv.payfrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.payfrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.payfrom03.setReverse(BaseScreenData.REVERSED);
			sv.payfrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.payfrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.payfrom04.setReverse(BaseScreenData.REVERSED);
			sv.payfrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.payfrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.payfrom05.setReverse(BaseScreenData.REVERSED);
			sv.payfrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.payfrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.payfrom06.setReverse(BaseScreenData.REVERSED);
			sv.payfrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.payfrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.payfrom07.setReverse(BaseScreenData.REVERSED);
			sv.payfrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.payfrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.payfrom08.setReverse(BaseScreenData.REVERSED);
			sv.payfrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.payfrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.payfrom09.setReverse(BaseScreenData.REVERSED);
			sv.payfrom09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.payfrom09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.payfrom10.setReverse(BaseScreenData.REVERSED);
			sv.payfrom10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.payfrom10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.payfrom11.setReverse(BaseScreenData.REVERSED);
			sv.payfrom11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.payfrom11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.payfrom12.setReverse(BaseScreenData.REVERSED);
			sv.payfrom12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.payfrom12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.payfrom13.setReverse(BaseScreenData.REVERSED);
			sv.payfrom13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.payfrom13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.payfrom14.setReverse(BaseScreenData.REVERSED);
			sv.payfrom14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.payfrom14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.payfrom15.setReverse(BaseScreenData.REVERSED);
			sv.payfrom15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.payfrom15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.payfrom16.setReverse(BaseScreenData.REVERSED);
			sv.payfrom16.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.payfrom16.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.payfrom17.setReverse(BaseScreenData.REVERSED);
			sv.payfrom17.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.payfrom17.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.payfrom18.setReverse(BaseScreenData.REVERSED);
			sv.payfrom18.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.payfrom18.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.payfrom19.setReverse(BaseScreenData.REVERSED);
			sv.payfrom19.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.payfrom19.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.payfrom20.setReverse(BaseScreenData.REVERSED);
			sv.payfrom20.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.payfrom20.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.payfrom21.setReverse(BaseScreenData.REVERSED);
			sv.payfrom21.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.payfrom21.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.payfrom22.setReverse(BaseScreenData.REVERSED);
			sv.payfrom22.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.payfrom22.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.payfrom23.setReverse(BaseScreenData.REVERSED);
			sv.payfrom23.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.payfrom23.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.payfrom24.setReverse(BaseScreenData.REVERSED);
			sv.payfrom24.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.payfrom24.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 2, generatedText1)%>

	<%=smartHF.getHTMLSpaceVar(3, 12, fw, sv.company)%>
	<%=smartHF.getHTMLF4NSVar(3, 12, fw, sv.company)%>

	<%=smartHF.getLit(3, 15, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 23, fw, sv.tabl)%>
	<%=smartHF.getHTMLF4NSVar(3, 23, fw, sv.tabl)%>

	<%=smartHF.getLit(3, 30, generatedText3)%>

	<%=smartHF.getHTMLVar(3, 37, fw, sv.item)%>

	<%=smartHF.getHTMLVar(3, 48, fw, sv.longdesc)%>

	<%=smartHF.getLit(7, 6, generatedText5)%>

	<%=smartHF.getHTMLVar(7, 45, fw, sv.durmnth, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getLit(10, 6, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(10, 46, fw, sv.autofreq, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
	<%=smartHF.getHTMLF4NSVar(10, 46, fw, sv.autofreq, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getLit(13, 6, generatedText7)%>

	<%=smartHF.getLit(15, 20, generatedText8)%>

	<%=smartHF.getHTMLVar(15, 29, fw, sv.payto)%>

	<%=smartHF.getLit(16, 18, generatedText9)%>

	<%=smartHF.getHTMLVar(16, 29, fw, sv.payfrom01)%>

	<%=smartHF.getHTMLVar(16, 32, fw, sv.payfrom02)%>

	<%=smartHF.getHTMLVar(16, 35, fw, sv.payfrom03)%>

	<%=smartHF.getHTMLVar(16, 38, fw, sv.payfrom04)%>

	<%=smartHF.getHTMLVar(16, 41, fw, sv.payfrom05)%>

	<%=smartHF.getHTMLVar(16, 44, fw, sv.payfrom06)%>

	<%=smartHF.getHTMLVar(16, 47, fw, sv.payfrom07)%>

	<%=smartHF.getHTMLVar(16, 50, fw, sv.payfrom08)%>

	<%=smartHF.getHTMLVar(16, 53, fw, sv.payfrom09)%>

	<%=smartHF.getHTMLVar(16, 56, fw, sv.payfrom10)%>

	<%=smartHF.getHTMLVar(16, 59, fw, sv.payfrom11)%>

	<%=smartHF.getHTMLVar(16, 62, fw, sv.payfrom12)%>

	<%=smartHF.getHTMLVar(17, 29, fw, sv.payfrom13)%>

	<%=smartHF.getHTMLVar(17, 32, fw, sv.payfrom14)%>

	<%=smartHF.getHTMLVar(17, 35, fw, sv.payfrom15)%>

	<%=smartHF.getHTMLVar(17, 38, fw, sv.payfrom16)%>

	<%=smartHF.getHTMLVar(17, 41, fw, sv.payfrom17)%>

	<%=smartHF.getHTMLVar(17, 44, fw, sv.payfrom18)%>

	<%=smartHF.getHTMLVar(17, 47, fw, sv.payfrom19)%>

	<%=smartHF.getHTMLVar(17, 50, fw, sv.payfrom20)%>

	<%=smartHF.getHTMLVar(17, 53, fw, sv.payfrom21)%>

	<%=smartHF.getHTMLVar(17, 56, fw, sv.payfrom22)%>

	<%=smartHF.getHTMLVar(17, 59, fw, sv.payfrom23)%>

	<%=smartHF.getHTMLVar(17, 62, fw, sv.payfrom24)%>

	<%=smartHF.getLit(19, 6, generatedText10)%>

	<%=smartHF.getHTMLSpaceVar(20, 29, fw, sv.crtable01)%>
	<%=smartHF.getHTMLF4NSVar(20, 29, fw, sv.crtable01)%>

	<%=smartHF.getHTMLSpaceVar(20, 35, fw, sv.crtable02)%>
	<%=smartHF.getHTMLF4NSVar(20, 35, fw, sv.crtable02)%>

	<%=smartHF.getHTMLSpaceVar(20, 41, fw, sv.crtable03)%>
	<%=smartHF.getHTMLF4NSVar(20, 41, fw, sv.crtable03)%>

	<%=smartHF.getHTMLSpaceVar(20, 47, fw, sv.crtable04)%>
	<%=smartHF.getHTMLF4NSVar(20, 47, fw, sv.crtable04)%>

	<%=smartHF.getHTMLSpaceVar(20, 53, fw, sv.crtable05)%>
	<%=smartHF.getHTMLF4NSVar(20, 53, fw, sv.crtable05)%>

	<%=smartHF.getHTMLSpaceVar(20, 59, fw, sv.crtable06)%>
	<%=smartHF.getHTMLF4NSVar(20, 59, fw, sv.crtable06)%>

	<%=smartHF.getHTMLSpaceVar(20, 65, fw, sv.crtable07)%>
	<%=smartHF.getHTMLF4NSVar(20, 65, fw, sv.crtable07)%>

	<%=smartHF.getHTMLSpaceVar(20, 71, fw, sv.crtable08)%>
	<%=smartHF.getHTMLF4NSVar(20, 71, fw, sv.crtable08)%>

	<%=smartHF.getHTMLSpaceVar(21, 29, fw, sv.crtable09)%>
	<%=smartHF.getHTMLF4NSVar(21, 29, fw, sv.crtable09)%>

	<%=smartHF.getHTMLSpaceVar(21, 35, fw, sv.crtable10)%>
	<%=smartHF.getHTMLF4NSVar(21, 35, fw, sv.crtable10)%>

	<%=smartHF.getHTMLSpaceVar(21, 41, fw, sv.crtable11)%>
	<%=smartHF.getHTMLF4NSVar(21, 41, fw, sv.crtable11)%>

	<%=smartHF.getHTMLSpaceVar(21, 47, fw, sv.crtable12)%>
	<%=smartHF.getHTMLF4NSVar(21, 47, fw, sv.crtable12)%>

	<%=smartHF.getHTMLSpaceVar(21, 53, fw, sv.crtable13)%>
	<%=smartHF.getHTMLF4NSVar(21, 53, fw, sv.crtable13)%>

	<%=smartHF.getHTMLSpaceVar(21, 59, fw, sv.crtable14)%>
	<%=smartHF.getHTMLF4NSVar(21, 59, fw, sv.crtable14)%>

	<%=smartHF.getHTMLSpaceVar(21, 65, fw, sv.crtable15)%>
	<%=smartHF.getHTMLF4NSVar(21, 65, fw, sv.crtable15)%>

	<%=smartHF.getHTMLSpaceVar(21, 71, fw, sv.crtable16)%>
	<%=smartHF.getHTMLF4NSVar(21, 71, fw, sv.crtable16)%>

	<%=smartHF.getHTMLSpaceVar(22, 29, fw, sv.crtable17)%>
	<%=smartHF.getHTMLF4NSVar(22, 29, fw, sv.crtable17)%>

	<%=smartHF.getHTMLSpaceVar(22, 35, fw, sv.crtable18)%>
	<%=smartHF.getHTMLF4NSVar(22, 35, fw, sv.crtable18)%>

	<%=smartHF.getHTMLSpaceVar(22, 41, fw, sv.crtable19)%>
	<%=smartHF.getHTMLF4NSVar(22, 41, fw, sv.crtable19)%>

	<%=smartHF.getHTMLSpaceVar(22, 47, fw, sv.crtable20)%>
	<%=smartHF.getHTMLF4NSVar(22, 47, fw, sv.crtable20)%>

	<%=smartHF.getHTMLSpaceVar(22, 53, fw, sv.crtable21)%>
	<%=smartHF.getHTMLF4NSVar(22, 53, fw, sv.crtable21)%>

	<%=smartHF.getHTMLSpaceVar(22, 59, fw, sv.crtable22)%>
	<%=smartHF.getHTMLF4NSVar(22, 59, fw, sv.crtable22)%>

	<%=smartHF.getHTMLSpaceVar(22, 65, fw, sv.crtable23)%>
	<%=smartHF.getHTMLF4NSVar(22, 65, fw, sv.crtable23)%>

	<%=smartHF.getHTMLSpaceVar(22, 71, fw, sv.crtable24)%>
	<%=smartHF.getHTMLF4NSVar(22, 71, fw, sv.crtable24)%>




<%}%>

<%if (sv.S6631protectWritten.gt(0)) {%>
	<%S6631protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2.jsp"%>
