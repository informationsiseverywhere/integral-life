

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5155";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5155ScreenVars sv = (S5155ScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>



	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Full Contract Reversal");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commence ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Premium");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Outstanding Amount ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to Date ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Freq ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suppress Billing?");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill Suppress Date ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran No ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind10.isOn()) {
			sv.supflag.setReverse(BaseScreenData.REVERSED);
			sv.supflag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.supflag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.suppressToDisp.setReverse(BaseScreenData.REVERSED);
			sv.suppressToDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.suppressToDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
		if (appVars.ind19.isOn())
		{
			sv.supflag.setEnabled(BaseScreenData.DISABLED);
			sv.suppressToDisp.setEnabled(BaseScreenData.DISABLED);
		//	generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		//	generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>






<div class="panel panel-default">

    	<div class="panel-body" >     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">



<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
 <table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%>
									</td><td>
							<%=smartHF.getHTMLVarExt(fw, sv.cnttype, 2)%>
							  
				  		</td><td style="max-width:300px;">
									<%=smartHF.getHTMLVarExt(fw, sv.ctypedes, 2)%>
									</td></tr></table>

</div></div>	

<div class="col-md-4"> </div>
<div class="col-md-4"> 
				    		<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%}%>




<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>





	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

	<div class="input-group">



	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
<div class="input-group">

	
  		
		<%			
		fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("register");
		longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
				
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div></div>

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	

<div class="input-group">



	
  		
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="col-md-4"></div>
<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%></label>





	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.cntinst).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.cntinst,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.cntinst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="min-width: 100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>

	<div class="input-group">
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="col-md-4"></div>
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Outstanding Amount")%></label>
<div class="input-group" style="min-width:70px">




	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.osbal).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.osbal,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.osbal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div></div>
</div>
<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
				    		
<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>

<div class="input-group">



	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
</div>

 
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>


	
  		
		<%		
		
						fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("billfreq");
						longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
									
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>



			    	<div class="col-md-4"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>

	<div class="input-group" >
  		
		<%		
		
		fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("mop");
		longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());  
				
							
		if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px;min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div></div></div>


<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">

<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Suppress Billing?")%></label>

<%}%>




<%	
	if ((new Byte((sv.supflag).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.supflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
	}
	if(((sv.supflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("No");
	}
	 
%>

<% 
	if((new Byte((sv.supflag).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.supflag).getColor())){
%>
<div style="border:2px; border-style: solid; border-color: #ec7572;  width:103px;"> 
<%
} 
%>

<select name='supflag' style="width:100px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(supflag)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.supflag).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.supflag).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.supflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(((sv.supflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


</select>
<% if("red".equals((sv.supflag).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>

</div></div>

	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Bill Suppress Date")%></label>
	<div class="input-group">





							<%
								if((new Byte((sv.suppressToDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.suppressToDisp,(sv.suppressToDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.suppressToDisp,(sv.suppressToDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
							<br>
				   </div></div></div></div>




	<!-- Super table -->
	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5155' width='100%'>
							<thead>
								<tr class='info'>
									<th><center>   <%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center>   </th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction Number")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Code")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>

								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s5155screensfl");
									
									S5155screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S5155screensfl.hasMoreScreenRows(sfl)) {
								%>
<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
	}

	%>
								<tr height="27" class="tableRowTag" id='<%="tablerow"+count%>' >
		
	<%--ILIFE-5437--%>
		<td class="tableDataTag tableDataTagFixed"  align="center" >
		<% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
			 <input type="hidden"
				 value='<%= sv.select.getFormData() %>'
				 onFocus='doFocus(this)' onHelp='return fieldHelp("s5155screensfl" + "." +
				 "select")' onKeyUp='return checkMaxLength(this)'
				 name='s5155screensfl.select_R<%=count%>'
				 id='s5155screensfl.select_R<%=count%>'
				 onClick="selectedRow('s5155screensfl.select_R<%=count%>')"
				 class="UICheck" disabled
			 />
			 <%}else{%>
		 <input type="checkbox"
				 value='<%= sv.select.getFormData() %>'
				 onFocus='doFocus(this)' onHelp='return fieldHelp("s5155screensfl" + "." +
				 "select")' onKeyUp='return checkMaxLength(this)'
				 name='s5155screensfl.select_R<%=count%>'
				 id='s5155screensfl.select_R<%=count%>'
				 onClick="selectedRow('s5155screensfl.select_R<%=count%>')"
				 class="UICheck"
			 />
		<%} %>
		</td>
		<td class="tableDataTag " 
		<%if((sv.tranno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >
			<%if((new Byte((sv.tranno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>			
				<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5155screensfl" + "." +
				"select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.tranno.getFormData()%></span></a>					
			<%}%>
		</td>
		<td class="tableDataTag"  
		<%if((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
				<%= sv.effdateDisp.getFormData()%>
			<%}%>
		</td>
		<td class="tableDataTag " 
		<%if((sv.trcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.trcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
				<%-- <input type='checkbox' name='validflagDisp' value='1' 
				
						<%
						if((sv.validflag).toString().trim().equalsIgnoreCase("1")){
							%>checked
				
				       <%}%>
						disabled="disabled" class ='UICheck'/> --%>
				<%= sv.trcode.getFormData()%>
			<%}%>
		</td>
		<td class="tableDataTag" 
		<%if((sv.trandesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.trandesc).getInvisible()).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0)){%>	
				<%= sv.trandesc.getFormData()%>
			<%}%>
		</td>		
	</tr>

	<%
	count = count + 1;
	S5155screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


							</tbody>

						</table>
						
					</div>
				</div>
			</div>
			
				<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			
		</div>

	</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;	
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	$('#dataTables-s5155').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }
          }     
  	});
})
</script>

		





<%@ include file="/POLACommon2NEW.jsp"%>

