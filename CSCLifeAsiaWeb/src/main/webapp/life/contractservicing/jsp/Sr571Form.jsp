
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR571";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr571ScreenVars sv = (Sr571ScreenVars) fw.getVariables();%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. fund ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-sect ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to 1");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality class ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl Method ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special terms ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life (J/L) ");%>
<!-- ILIFE-1722 STARTS -->
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium with Tax ");%>
<!-- ILIFE-1722 ENDS -->
	
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Details ");%>
	
<%
		{
			appVars.rolldown(new int[] {10});
			appVars.rollup(new int[] {10});
			if (appVars.ind42.isOn()) {
		sv.zagelit.setHighLight(BaseScreenData.BOLD);
			}
			if (!appVars.ind05.isOn()) {
		sv.planSuffix.setColor(BaseScreenData.WHITE);
			}
			if (appVars.ind50.isOn()) {
		sv.planSuffix.setHighLight(BaseScreenData.BOLD);
			}
			if (!appVars.ind50.isOn()) {
		generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind01.isOn()) {
		generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind14.isOn()) {
		sv.sumin.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind02.isOn()) {
		sv.sumin.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind14.isOn()) {
		sv.sumin.setColor(BaseScreenData.RED);
			}
			if (appVars.ind01.isOn()) {
		sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind14.isOn()) {
		sv.sumin.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
		sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind03.isOn()) {
		sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind15.isOn()) {
		sv.riskCessAge.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind15.isOn()) {
		sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
		sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind04.isOn()) {
		sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind16.isOn()) {
		sv.riskCessTerm.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind16.isOn()) {
		sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
		sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind29.isOn()) {
		sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
		sv.riskCessDateDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind29.isOn()) {
		sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
		sv.premCessAge.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind24.isOn()) {
		sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind17.isOn()) {
		sv.premCessAge.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind17.isOn()) {
		sv.premCessAge.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
		sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind25.isOn()) {
		sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind18.isOn()) {
		sv.premCessTerm.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind18.isOn()) {
		sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind28.isOn()) {
		sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind30.isOn()) {
		sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
		sv.premCessDateDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind30.isOn()) {
		sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
		generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind19.isOn()) {
		sv.mortcls.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind06.isOn()) {
		sv.mortcls.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind19.isOn()) {
		sv.mortcls.setColor(BaseScreenData.RED);
			}
			if (appVars.ind05.isOn()) {
		sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind19.isOn()) {
		sv.mortcls.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
		generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind20.isOn()) {
		sv.liencd.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind08.isOn()) {
		sv.liencd.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind20.isOn()) {
		sv.liencd.setColor(BaseScreenData.RED);
			}
			if (appVars.ind07.isOn()) {
		sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind20.isOn()) {
		sv.liencd.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
		generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind21.isOn()) {
		sv.optextind.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind09.isOn()) {
		sv.optextind.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind21.isOn()) {
		sv.optextind.setColor(BaseScreenData.RED);
			}
			if (appVars.ind09.isOn()) {
		sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind21.isOn()) {
		sv.optextind.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
		generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind22.isOn()) {
		sv.instPrem.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind31.isOn()) {
		sv.instPrem.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind32.isOn()) {
		sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind22.isOn()) {
		sv.instPrem.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind22.isOn()) {
		sv.instPrem.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
		sv.comind.setReverse(BaseScreenData.REVERSED);
		sv.comind.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind35.isOn()) {
		sv.comind.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind27.isOn()) {
		generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind23.isOn()) {
		sv.select.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind26.isOn()) {
		sv.select.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind27.isOn()) {
		sv.select.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind23.isOn()) {
		sv.select.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind23.isOn()) {
		sv.select.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind34.isOn()) {
		generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind33.isOn()) {
		sv.pbind.setReverse(BaseScreenData.REVERSED);
		sv.pbind.setColor(BaseScreenData.RED);
			}
			if (appVars.ind34.isOn()) {
		sv.pbind.setEnabled(BaseScreenData.DISABLED);
		sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind33.isOn()) {
		sv.pbind.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind38.isOn()) {
		generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind36.isOn()) {
		sv.bappmeth.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind37.isOn()) {
		sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind38.isOn()) {
		sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind36.isOn()) {
		sv.bappmeth.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind36.isOn()) {
		sv.bappmeth.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind32.isOn()) {
		generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind42.isOn()) {
		generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind42.isOn()) {
		sv.taxamt01.setInvisibility(BaseScreenData.INVISIBLE);
		                    }
		     if (appVars.ind74.isOn()) {
		sv.taxamt01.setEnabled(BaseScreenData.DISABLED);
			}
		     if (appVars.ind75.isOn()) {
		            			sv.taxamt02.setEnabled(BaseScreenData.DISABLED);
		            		}
			if (appVars.ind42.isOn()) {
		generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
						}
			if (appVars.ind43.isOn()) {
		sv.taxind.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind42.isOn()) {
		sv.taxind.setEnabled(BaseScreenData.DISABLED);
		sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);	
			}
			/*BRD-306 START */
			if (appVars.ind32.isOn()) {
		sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind32.isOn()) {
		sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind54.isOn()) {
		sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind52.isOn()) {
		sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind55.isOn()) {
		sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
			}
			/*BRD-306 END */
			//ILIFE-3423:Start
			if (appVars.ind56.isOn()) {
		sv.prmbasis.setColor(BaseScreenData.RED);
			}else{
		sv.prmbasis.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind57.isOn()) {
		sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind58.isOn()) {
		sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
			}
			//ILIFE-3423:End
			//BRD-NBP-011 starts
			if (appVars.ind72.isOn()) {
		sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
		sv.dialdownoption.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind72.isOn()) {
		sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind70.isOn()) {
		sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind71.isOn()) {
		sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
			}
			//BRD-NBP-011 ends
		if (appVars.ind73.isOn()) {
		sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind90.isOn()) {
		sv.fuind.setReverse(BaseScreenData.REVERSED);
		sv.fuind.setColor(BaseScreenData.RED);
			}
			if (appVars.ind61.isOn()) {
		sv.fuind.setEnabled(BaseScreenData.DISABLED);
		sv.fuind.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (!appVars.ind90.isOn()) {
		sv.fuind.setHighLight(BaseScreenData.BOLD);
			}
			//ICIL-560 FWANG3
			if (appVars.ind76.isOn()) {
		sv.cashvalarer.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			//ILJ-46
			if (appVars.ind123.isOn()) {
				sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
			}
			//end
		}
	%>
	<!-- ILIFE-6586 -->
<%
List<String> listitems = new ArrayList<String>();
if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("dialdownoption");
}
if ((new Byte((sv.mortcls).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("mortcls");
}
if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("currcd");
}
if ((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("liencd");
}
if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("prmbasis");
}
if ((new Byte((sv.bappmeth).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {
	listitems.add("bappmeth");
}
fieldItem=appVars.loadF4FieldsLong(listitems.toArray(new String[listitems.size()]),sv,"E",baseModel);

%>
<div class="panel panel-default">
	<div class="panel-body">    
		<div class="row">	
			<div class="col-md-6"> 
				<div class="form-group">			    	     
    	 			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>    	 					
    	 					<table><tr><td>						
    	 					<% formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); %>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'   >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td>
							<td>
							<% formatValue = formatValue( (sv.cnttype.getFormData()).toString());  %>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left:1px;" >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	

  						</td>
  						<td>
						<% formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); %>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px; max-width:270px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	

    	 				</td></tr></table>	
    	 					</div>
    	 				</div>
    	 					
    	 					</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<table>
							<tr>
								<td>		
								<% formatValue = formatValue((sv.lifcnum.getFormData()).toString()); %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								</td>
								<td>
								<% formatValue = formatValue((sv.linsname.getFormData()).toString()); %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px;margin-left:1px;max-width:150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								</td>
							</tr>
						</table>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<% formatValue = formatValue( (sv.zagelit.getFormData()).toString()); %>
				<label>	<%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
					<%
		formatValue = null;
		%>


					<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				formatValue = formatValue( formatValue );
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
					<div class="output_cell" style="width: 50px;">
						<%= XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
			} else {
		%>

					<div class="blank_cell" style="width: 50px;">
						&nbsp;</div>

					<% 
			} 
		%>
				</div>
			</div>

		</div>

		<div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 					<table><tr><td>
							<% formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); %>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' 
										style="min-width:71px"	>  
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							</td>
						<td>
  		
						<% formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style="min-width:71px;margin-left:1px;">  
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
    	 					
    	 				</td></tr></table></div></div>
    	 			
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

<% formatValue = formatValue( (sv.numpols.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
			    	     </div></div>
			    	     
			    	     <div class="col-md-2"> 
			    	     <div class="form-group">
    	 					
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

<% formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
    	 					

<% formatValue = formatValue( (sv.life.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
    	 				</div></div>	
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

<% formatValue = formatValue( (sv.coverage.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 				</div></div>
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

<% formatValue = formatValue( (sv.rider.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
    	 				</div></div>
    	 				
    	 				<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>

<%	
    Map shortfieldItem = new HashMap();
    shortfieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) shortfieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:80px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
formatValue = null;
%>
    	 					
    	 				</div></div> <%} %>
    	 				
    	 				<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

<div 	
	class='<%= (sv.statSect.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="width:80px;">
<% formatValue = formatValue( (sv.statSect.getFormData()).toString()); %>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
    	 					
    	 				</div></div><%} %>
    	 				
    	 				
    	 			<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 			
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>
<div 	
	class='<%= (sv.statSect.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="width:80px;">
<% formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); %>
		<%=XSSFilter.escapeHtml(formatValue)%>
		</div>
    	 				</div></div><%} %>
    	 				
    	 				</div>
    	 					
    	 					
    	 				<br>	
    	 					
    	 				<hr>
    	 					
    	 				<br>	
   
  <%-- IBPLIFE-2132 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  
	<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#sum_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured / Premium")%></label></a>
						</li>
						<li><a href="#cover_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></label></a>
						</li>
					</ul>
				</div>   
		</div>       
		
		<div class="tab-content">
			<div class="tab-pane fade in active" id="sum_tab">
	<%} %>
<%-- IBPLIFE-2132 end --%> 
    	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>

  		
		<% formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); %>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:100px;" : "width:100px;" %>'>  
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
    	 				
    	 				</div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>	

	<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='sumin' 
type='text'

<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>

	value='<%=valueThis%>'
			 <%
	 //valueThis=smartHF.getPicFormatted(qpsf,sv.sumin);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%= sv.sumin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'


<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    <div class="col-md-1"> </div>
    <div class="col-md-3"> 
			    	     <div class="form-group" >		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>	

<%	

	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' >  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mortcls).getColor())){
%>
<div style="border:1px; border-color: #B55050;  width:210px;"> 
<%
} 
%>

<select name='mortcls' type='list'
<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mortcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
style="width:210px;">
<%=optionValue%>
</select>
<% if("red".equals((sv.mortcls).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
} 
%>	
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
    	 				
    	 					
    	 					
    	 					
    	 					
    	 	<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 					
    	 <div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>	
    	 					
<%
longValue = null;
formatValue=null;
%>
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	

		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:100px;" : "width:150px;" %>'>  
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
    	 					
    	 					</div></div> <%} %>
    	 				</div>	
    	 				
    	 				 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">	
			    	     <!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
								<% } %>
								<!--  END  -->		    	     
    	 					
<!-- <div class="input-group"> -->

<%	
			qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
<table><tr><td>

<input name='riskCessAge' 
type='text'
<%if((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
	 <%}%>

size='<%= sv.riskCessAge.getLength()%>'
maxLength='<%= sv.riskCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.riskCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskCessAge).getColor()== null  ? 
			"input_cell" :  (sv.riskCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td>



	<%	
			qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
<td>

<input name='riskCessTerm' 
type='text'
<%if((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;margin-left: 2px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessTerm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessTerm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessTerm) %>'
	 <%}%>

size='<%= sv.riskCessTerm.getLength()%>'
maxLength='<%= sv.riskCessTerm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.riskCessTerm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskCessTerm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskCessTerm).getColor()== null  ? 
			"input_cell" :  (sv.riskCessTerm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></td></tr></table></div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">	
			    	     	 <!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
								<% } %>
								<!--  END  -->		    	     
    	 					
   <div class="input-group ">
<%	
	longValue = sv.riskCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
						style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:100px;" : "width:100px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 



<input name='riskCessDateDisp' 
type='text' 
value='<%=sv.riskCessDateDisp.getFormData()%>' 
maxLength='<%=sv.riskCessDateDisp.getLength()%>' 
size='<%=sv.riskCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessDateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"		style="width:140px;" >

<%
	}else if((new Byte((sv.riskCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" 	style="width:120px;">
 
 
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>



<%
	}else { 
%>



class = ' <%=(sv.riskCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.riskCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:140px;" >


<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>



<%} longValue = null;}%>
    	 					
    	 					
    	 					 </div></div></div>
    	 					
    	 					
    	 					
    	 						<div class="col-md-1"> </div>
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
    	 					

<%	

	mappedItems = (Map) fieldItem.get("liencd"); 
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:100px;" : "width:100px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.liencd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='liencd' type='list' style="width:140px;"
<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.liencd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.liencd).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
} 
%>
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>

<%	
	if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

		mappedItems = (Map) fieldItem.get("prmbasis");
		optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
		longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
	%>
		<% 
		if((new Byte((sv.prmbasis).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
		%>  
  			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   			<%if(longValue != null){%>
			   		<%=longValue%>
		   		<%}%>
		   	</div>
			<%
			longValue = null;
			%>

		<%}else {%>
			<%if("red".equals((sv.prmbasis).getColor())){%>
			<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
			<%}%>
			<select name='prmbasis' type='list' style="width:145px;"
			<% 
			if((new Byte((sv.prmbasis).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
			}else if((new Byte((sv.prmbasis).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
				class="bold_cell" 
			<%
			}else { 
			%>
				class = 'input_cell' 
			<%}%>
			>
				<%=optionValue%>
			</select>
			<% if("red".equals((sv.prmbasis).getColor())){%>
			</div>
			<%}%>
		<%}
	}%>
	<% longValue = null;%>
    	 					
    	 					
    	 					</div></div><%} %>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 				 	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
<%	
			qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
<table><tr><td>
<input name='premCessAge' 
type='text'
<%if((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
	 <%}%>

size='<%= sv.premCessAge.getLength()%>'
maxLength='<%= sv.premCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessAge).getColor()== null  ? 
			"input_cell" :  (sv.premCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td>

<td>
	<%	
			qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='premCessTerm' 
type='text'

<%if((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px;margin-left: 2px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessTerm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
	 <%}%>

size='<%= sv.premCessTerm.getLength()%>'
maxLength='<%= sv.premCessTerm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessTerm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessTerm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessTerm).getColor()== null  ? 
			"input_cell" :  (sv.premCessTerm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></td></tr></table></div></div>
    	 					
    	 					
    	 			<div class="col-md-3"> 
			    	     <div class="form-group" style="min-width:200px">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
<div class="input-group" style="width:130px">
<%	
	longValue = sv.premCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
						style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:100px;" : "width:100px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<input name='premCessDateDisp' 
type='text' 
value='<%=sv.premCessDateDisp.getFormData()%>' 
maxLength='<%=sv.premCessDateDisp.getLength()%>' 
size='<%=sv.premCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	style="width:140px;">

<%
	}else if((new Byte((sv.premCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:120px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
 
<%
	}else { 
%>

class = ' <%=(sv.premCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.premCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:120px;" >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'),  '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
 
<%}longValue = null; }%>
    	 				</div></div></div>
    	 				
    	 				
    	 				
    	 				
    	 				<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 				
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%></label>
<%if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='select' 
type='text'

<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.select.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.select.getLength()%>'
maxLength='<%= sv.select.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(select)' onKeyUp='return checkMaxLength(this)'  
	style="width:100px;"

<% 
	if((new Byte((sv.select).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.select).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.select).getColor()== null  ? 
			"input_cell" :  (sv.select).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>


    	 				
    	 				</div></div><%} %>
    	 				
    	 				
    	 				<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
    	 				
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>

	<%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

		mappedItems = (Map) fieldItem.get("dialdownoption");
		optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
		longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
	%>
		<% 
		if((new Byte((sv.dialdownoption).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
		%>  
  			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style='min-width: 70px'>  
	   			<%if(longValue != null){%>
			   		<%=longValue%>
		   		<%}%>
		   	</div>
			<%
			longValue = null;
			%>

		<%}else {%>
			<%if("red".equals((sv.dialdownoption).getColor())){%>
			<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
			<%}%>
			<select name='dialdownoption' type='list' style="width:145px;"
			<% 
			if((new Byte((sv.dialdownoption).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
			}else if((new Byte((sv.dialdownoption).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
				class="bold_cell" 
			<%
			}else { 
			%>
				class = 'input_cell' 
			<%}%>
			>
				<%=optionValue%>
			</select>
			<% if("red".equals((sv.dialdownoption).getColor())){%>
			</div>
			<%}%>
		<%}
	}%>
	<% longValue = null;%>
    	 				
    	 				</div></div>		<%} %>
    	 					
    	 					
    	 					</div>
    	 					
    	 					<br>
    	 					
    	 					
  	 					
    	 					                               <hr>
    	 					<br>
    	 					
    	 					
    	 					
    	 					
    <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
			 <div class="row">
			 <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
    	 					
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div><%} %>
                        
                        <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>

<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>

<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                        </div>  <%} %>	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					 <div class="row">
    	 					 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

    	 					
    	 					</div></div><%}} %>
    	 					
    	 					
    	 					<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		 
			    	     <!-- ILIFE-8323 Starts -->  
<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> 	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
<%}
if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0){ %> 	     
	<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
	<!-- ILIFE-8313 ends -->
<%}
if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
<!-- ILIFE-8323 ends -->  
<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
<%if(sv.zlinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
	
    	 					
    	 					</div></div><%} %>
    	 					
    	 					<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 					<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%} }%>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 		 <div class="row">
    	 					    	 					 
    	 					    	 					 
    	 			 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>

<%if ((new Byte((sv.linstamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='linstamt' 
type='text'
<%if((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)-3%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt)' onKeyUp='return checkMaxLength(this)'  


	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.linstamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt).getColor()== null  ? 
			"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

<%} %>
<%
longValue = null;
formatValue = null;
%>		 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>

<%if ((new Byte((sv.taxamt01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<!-- ILIFE-1722 STARTS -->
								
<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt01).getFieldName());


			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='taxamt01' 	
type='text'
<%if((sv.taxamt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width:145px; "<% }%>

	value='<%=valueThis %>'
			 <%

	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt01.getLength(), sv.taxamt01.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt01.getLength(), sv.taxamt01.getScale(),3)-3%>'
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt01)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxamt01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt01).getColor()== null  ? 
			"input_cell" :  (sv.taxamt01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
    	 	>				
    	 					</div></div><%
	} 
%>
   
    	 					
    	 					
    	 					
    	 					
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%></label>

	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
			
	%>

<input name='instPrem' 
type='text'
<%if((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)%>'
maxLength='<%= sv.instPrem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%} %>
	
    	 					
    	 					</div></div>
    	 					
			<%
				if ((new Byte((sv.cashvalarer).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Value in Arrears")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.cashvalarer).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.cashvalarer,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.cashvalarer.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell"
						style="width: 175px !important; text-align: right;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 175px !important;">
						&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
							formatValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 					<div class="row">
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>

<%	
	if ((new Byte((sv.bappmeth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {

	mappedItems = (Map) fieldItem.get("bappmeth");
	optionValue = makeDropDownList( mappedItems , sv.bappmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.bappmeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='bappmeth' type='list' style="width:140px;"
<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.bappmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.bappmeth).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>


</div></div></div><%}%>
</div>   	 					
<%-- IBPLIFE-2132 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  

<div class="tab-pane fade" id="cover_tab">
	<div class="row">
				
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
			<table><tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcode, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcdedesc, true)%></td>
			</tr></table>
			</div>
		</div>
		
		<div class="col-md-1"></div>
		<div class="col-md-2">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					
					<%--
	
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dateeffDisp" data-link-format="dd/mm/yyyy"  onclick="document.getElementById('riskCessDateDisp').value='';">
			                    <%=smartHF.getRichTextDateInput(fw, sv.dateeffDisp,(sv.dateeffDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
						--%>
						<div class="form-group">
								<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.dateeffDisp.getFormData()%>' maxLength='<%=sv.dateeffDisp.getLength()%>' 
										size='<%=sv.dateeffDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>
								</div>
						</div>   			
		</div>
		
	</div>
	
	<div class="row">
				
				
		<div class="col-md-8">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose ")%></label>		
			<div class="form-group">

<table><tr>
<td>

<textarea name='covrprpse' style='width:400px;height:70px;resize:none;border: 2px solid !important;'
type='text' 

<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.covrprpse.getFormData()).toString();

%>
 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>

size='<%= sv.covrprpse.getLength() %>'
maxLength='<%= sv.covrprpse.getLength() %>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.covrprpse).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.covrprpse).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.covrprpse).getColor()== null  ? 
			"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%=XSSFilter.escapeHtml(formatValue)%></textarea>

</td></tr>

</table>

</div>
</div>
</div>
</div>
<%} %>
<%-- IBPLIFE-2132 end --%> 					


</div>
</div></div>

<Div id='mainForm_OPTS' style='visibility:hidden;'>
<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
 <%=smartHF.getMenuLink(sv.pbind, resourceBundleHandler.gettingValueFromBundle("Premium Breakdown"))%>
<%=smartHF.getMenuLink(sv.comind, resourceBundleHandler.gettingValueFromBundle("Agent Commission Split"))%>

<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"))%>
<%-- <%=smartHF.getMenuLink(sv.tagd, resourceBundleHandler.gettingValueFromBundle("Tied Agent Details"))%> --%>
<%=smartHF.getMenuLink(sv.fuind, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>

</div>

<%@ include file="/POLACommon2NEW.jsp"%>

