<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sd5js";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sd5jsScreenVars sv = (Sd5jsScreenVars) fw.getVariables();%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table>
		        		<tr>
		        			<td>
				        		<%					
									if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<%					
									if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
	        	</div>
	        </div>
		</div>
		<div class="row" style="margin-top: 70px;"></div>
		<%-- <div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%					
									if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=formatValue%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<label>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp;</label>
							</td>
							<td>
								<%					
									if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=formatValue%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div> --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Calculation Required")%></label>
					<input id="taxCalcReqdCb" type="checkbox" name="taxCalcReqdCb" onFocus="doFocus(this)" onHelp="return fieldHelp(taxCalcReqd)" onKeyUp="return checkMaxLength(this)"
						<%
							if((sv.taxCalcReqd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){
						%>		disabled
						<%
								if((sv.taxCalcReqd).toString().trim().equalsIgnoreCase("Y")){
		      			%>			checked
						<%		}
		      					else if((sv.taxCalcReqd).toString().trim().equalsIgnoreCase("N")){
		      			%>			unchecked
						<%		}
							}
							else {
	      						if((sv.taxCalcReqd).toString().trim().equalsIgnoreCase("Y")){
	      				%>			checked
						<%		}
	      						else if((sv.taxCalcReqd).toString().trim().equalsIgnoreCase("N")){
	      				%>			unchecked
						<%	} %> onChange="checkboxChange();" />
								<input type='text' id="taxCalcReqd" name='taxCalcReqd' value=' ' style="visibility: hidden"/>
						<%	} %>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	checkboxChange();
});
function checkboxChange(){
	if($("#taxCalcReqdCb").prop("checked")){
		$("#taxCalcReqd").val("Y");
	}
	else{
		$("#taxCalcReqd").val("N");
	}
}
</script>
<%@ include file="/POLACommon2NEW.jsp"%>