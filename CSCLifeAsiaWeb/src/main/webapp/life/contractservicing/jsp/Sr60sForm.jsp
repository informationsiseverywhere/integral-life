<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR60S";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sr60sScreenVars sv = (Sr60sScreenVars) fw.getVariables();%>
	
<%{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind04.isOn()) {
			sv.ownersel.setReverse(BaseScreenData.REVERSED);
			sv.ownersel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ownersel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			<div class="col-md-2"> 
			
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
					    		<%
                                         if ((new Byte((sv.ownersel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ownersel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ownersel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ownersel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>						    	
					    		
				    		</div>
			    	</div>
			    	<div class="col-md-2"></div>
			    		
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>		
					    		<%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>						    	
					    		
				    		</div>
			    	</div>
			    	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Scheme No")%></label>	
					    		<%
                                         if ((new Byte((sv.schmno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.schmno)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.schmno)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('schmno')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>					    	
					    	
				    		</div>
			    	</div>
                </div>
              </div>
             </div>
             <br><br><br>
             
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	 <div class="panel-body">     
			<div class="row">	
			 <div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Modify Contribution Details")%></b>
					</label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
					<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Enquire Contribution Details")%></b>
					</label>			
			    </div>	
			    <%if(!sv.s290hideflag.trim().equals("Y")){ %>	   
				    <div class="col-md-4">			
						<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("S290 Notice Correction")%></b>
						</label>			
				    </div>	     							 
			    <%}%> 
</div>

<td>

<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
</div></div>


<%@ include file="/POLACommon2NEW.jsp"%>

