<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6668";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6668ScreenVars sv = (S6668ScreenVars) fw.getVariables();%>

<%if (sv.S6668screenWritten.gt(0)) {%>
	<%S6668screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill To Date ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commencement Date ");%>
	<%sv.occdateDisp.setClassString("");%>
<%	sv.occdateDisp.appendClassString("string_fld");
	sv.occdateDisp.appendClassString("output_txt");
	sv.occdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner           ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.ownername.setClassString("");%>
<%	sv.ownername.appendClassString("string_fld");
	sv.ownername.appendClassString("output_txt");
	sv.ownername.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Owner     ");%>
	<%sv.jownnum.setClassString("");%>
<%	sv.jownnum.appendClassString("string_fld");
	sv.jownnum.appendClassString("output_txt");
	sv.jownnum.appendClassString("highlight");
%>
	<%sv.jownername.setClassString("");%>
<%	sv.jownername.appendClassString("string_fld");
	sv.jownername.appendClassString("output_txt");
	sv.jownername.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------------------------------------------------------------------");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency     ");%>
	<%sv.billfreq.setClassString("");%>
	<%sv.freqdesc.setClassString("");%>
<%	sv.freqdesc.appendClassString("string_fld");
	sv.freqdesc.appendClassString("output_txt");
	sv.freqdesc.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Method        ");%>
	<%sv.mop.setClassString("");%>
	<%sv.mopdesc.setClassString("");%>
<%	sv.mopdesc.appendClassString("string_fld");
	sv.mopdesc.appendClassString("output_txt");
	sv.mopdesc.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Day           ");%>
	<%sv.billday.setClassString("");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Date   ");%>
	<%sv.billcdDisp.setClassString("");%>
<%	sv.billcdDisp.appendClassString("string_fld");
	sv.billcdDisp.appendClassString("output_txt");
	sv.billcdDisp.appendClassString("highlight");
%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary   ");%>
	<%sv.aiind.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.payind.setClassString("");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Direct");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Debit");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.ddind.setClassString("");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Group  ");%>
	<%sv.grpind.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.billday.setReverse(BaseScreenData.REVERSED);
			sv.billday.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.billday.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.grpind.setReverse(BaseScreenData.REVERSED);
			sv.grpind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.grpind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.mop.setReverse(BaseScreenData.REVERSED);
			sv.mop.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.mop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.payind.setReverse(BaseScreenData.REVERSED);
			sv.payind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.payind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.aiind.setReverse(BaseScreenData.REVERSED);
			sv.aiind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind10.isOn()) {
			sv.aiind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.aiind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.aiind.setHighLight(BaseScreenData.BOLD);
		}
	}

	
	
	
	%>
	
	
	
	
	<div class="panel panel-default">
	<div class="panel-body">     
				 <div class="row">	
				    	                                 
	                                 
	                                 
	                                 <div class="col-md-8"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText2)%></label>
	                           
	                        <table><tr><td>
	                                 <%=smartHF.getHTMLVar(fw, sv.chdrnum)%>
                                 </td><td>
	                                 <%=smartHF.getHTMLSpaceVar(fw, sv.cnttype)%>
	                                 <%=smartHF.getHTMLF4NSVar(fw, sv.cnttype)%>
                                 </td><td>
	                                 <%=smartHF.getHTMLVar(fw, sv.ctypedes)%>
	                                 </td></tr></table>
	                                 
	                                 </div></div>
	                                 
	                                 
	                                 
	                                 
	                                 
	                                 <div class="col-md-2"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText3)%></label>
	                                 <div  style="width:60px">
	                                 <%=smartHF.getHTMLSpaceVar(fw, sv.cntcurr)%>
	                                 <%=smartHF.getHTMLF4NSVar(fw, sv.cntcurr)%>
	                                 </div>
	                                 
	                                 </div></div>
	                                 
	                                 
	                                 
	                                 </div>
	                                 
	                                 
	                                 
	                                 
	                                 
	                            <div class="row">	
	                            
	                            
				    	<div class="col-md-4"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText4)%></label>
	                                <div  style="width:110px">
	                             <%=smartHF.getHTMLVar(fw, sv.chdrstatus)%>
	
	                         </div></div></div>
	
	
	
	                   <div class="col-md-4"> 
				    	     <div class="form-group">
				    	      
	    	 					<label><%=smartHF.getLit(generatedText5)%></label>
	                                <div  style="width:110px">
	                             <%=smartHF.getHTMLVar(fw, sv.premstatus)%>
	
	                         </div></div></div>
	
	
	
	                     <div class="col-md-4"> 
	 			    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText6)%></label>
	                                <div  style="width:60px">
	                             <%=smartHF.getHTMLSpaceVar(fw, sv.register)%>
	                               <%=smartHF.getHTMLF4NSVar(fw, sv.register)%>
	                                 </div>
	                         </div></div>
	
	
	
	
	
	</div>
	
	
	
	
	
	                  <div class="row">	
	                            
	                            
				    	<div class="col-md-4"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText14)%></label>
	                                     <div  style="width:120px">  
	                                       
	                                       <%=smartHF.getHTMLSpaceVar( fw, sv.btdateDisp)%>
	                                        
	                           </div></div></div> 
	
	
	
	               <div class="col-md-4"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText18)%></label>
	                                       <div  style="width:120px">
	                                        
	                                       <%=smartHF.getHTMLSpaceVar( fw, sv.occdateDisp)%>
	                                        
	                           </div></div></div> 
	                           
	                           
	                           <div class="col-md-4"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText15)%></label>
	                                       <div  style="width:120px">
	                                        
	                                       <%=smartHF.getHTMLSpaceVar( fw, sv.ptdateDisp)%>
	                                        
	                           </div></div></div> 
	
	</div>
	
	
	
	
	
	
	
	
	
	  <div class="row">	
	                            
	                            
				    	<div class="col-md-6"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText7)%></label>
	                                       
	                                         <table><tr><td>
	                                       <%=smartHF.getHTMLVar( fw, sv.lifenum)%>
	                                       </td><td>

	                                        <%=smartHF.getHTMLVar( fw, sv.lifename)%>
	                                        </td></tr></table>
	
	</div></div></div>
	                                       
	                                       
	<div class="row">	
	<div class="col-md-6"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText8)%></label>
	                                       
	                                        
	                                <table><tr><td>
	                                    <%=smartHF.getHTMLVar( fw, sv.jlife)%>
                                     </td><td>
	                                    <%=smartHF.getHTMLVar( fw, sv.jlifename)%>
	                                 </td></tr></table>
	</div></div> 
	
	
	</div>
	
	
	
	
	
	
	
	
	 <div class="row">	
	                            
	                            
				    	<div class="col-md-6"> 
				    	     
				    	     <div class="form-group">
	    	 					<label><%=smartHF.getLit(generatedText9)%></label>
	                                 <table><tr><td>      
	                                       <div class="input-group">
	                                <%=smartHF.getHTMLVar( fw, sv.cownnum)%>
                                      </td><td>
	                                <%=smartHF.getHTMLVar( fw, sv.ownername)%>
	                                </td></tr></table>
	
	</div></div> </div>
	                                       
	                                       
	<div class="row">	
	<div class="col-md-6"> 
				    	     <div class="form-group">
				    	     
	    	 					<label><%=smartHF.getLit(generatedText10)%></label>
	                                       
	                                       <table><tr><td>
	
	                                   <%=smartHF.getHTMLVar( fw, sv.jownnum)%>
                                          </td><td>
	                                    <%=smartHF.getHTMLVar( fw, sv.jownername)%>
	                                    </td></tr></table>
	
	</div></div></div>
	
	
	  
	

<hr>



 <div class="row">	
	                            
	                            
				    	<div class="col-md-6"> 
				    	     <div class="form-group">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText12)%></label>
	                                       
	                                       <div class="input-group">
                                                <%=smartHF.getHTMLSpaceVar( fw, sv.billfreq)%>
	                                             <%=smartHF.getHTMLF4NSVar( fw, sv.billfreq)%>

	                                             <%=smartHF.getHTMLVar( fw, sv.freqdesc)%>

                         </div></div></div>

                  </div>
                  
                  
                  
                  
                  <div class="row">	
	                            
	                            
				    	<div class="col-md-6"> 
				    	     <div class="form-group">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText13)%></label>
	                                       
	                                       <div class="input-group">
                                                 <%=smartHF.getHTMLSpaceVar( fw, sv.mop)%>
	                                             <%=smartHF.getHTMLF4NSVar( fw, sv.mop)%>

	                                              <%=smartHF.getHTMLVar( fw, sv.mopdesc)%>

                         </div></div> 

                  </div></div>
                  
                  
                  
                  
                  <div class="row">	
	                            
	                            
				    	<div class="col-md-3"> 
				    	     <div class="form-group">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText19)%></label>
	    	 					<div  style="width:60px">
	                                       
	                                        <%=smartHF.getHTMLVar( fw, sv.billday)%>

                         </div></div></div>



                        <div class="col-md-3"> 
				    	     <div class="form-group">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText16)%></label>
	                                       
	                                       <div  style="width:120px">
	                                       	<%=smartHF.getHTMLSpaceVar( fw, sv.billcdDisp)%>
	                                          

                         </div></div></div> 


                  </div>
                  
                  
                  
                  
                   <div class="row">	
	                            
	                            <div class="col-md-4"></div>
	                            
	                            
				    	<div class="col-md-4"> 
				    	     <div class="form-group" style="width:60px">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText25)%></label>
	    	 				 
                                  <%=smartHF.getHTMLVar( fw, sv.aiind)%>
	                   
	    	 					</div></div>
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					</div>
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					<div class="row">	
	                          
				    	<div class="col-md-4"> 
				    	     <div class="form-group" style="width:60px">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText20)%></label>
	    	 				 
                                  <%=smartHF.getHTMLVar( fw, sv.payind)%>
	                   
	    	 					</div></div>
	    	 					
	    	 					
	    	 					
	    	 					<div class="col-md-4" > 
				    	     <div class="form-group" >
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText22)%> &nbsp <%=smartHF.getLit( generatedText23)%></label>
	    	 				 <div class="form-group" style="width:60px">
                                  <%=smartHF.getHTMLVar( fw, sv.ddind)%>
	                   
	    	 					</div></div></div>
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					<div class="col-md-4"> 
				    	     <div class="form-group" style="width:60px">
				    	     
	    	 					<label> <%=smartHF.getLit( generatedText17)%></label>
	    	 				 
                                  <%=smartHF.getHTMLVar( fw, sv.grpind)%>
	                   
	    	 					</div></div>
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					</div>
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					
	    	 					
                  

	</div></div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	<%-- <%=smartHF.getLit(3, 2, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 12, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 24, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.ctypedes)%>

	<%=smartHF.getLit(3, 64, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(3, 74, fw, sv.cntcurr)%>
	<%=smartHF.getHTMLF4NSVar(3, 74, fw, sv.cntcurr)%>

	<%=smartHF.getLit(4, 2, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 19, fw, sv.chdrstatus)%>

	<%=smartHF.getLit(4, 31, generatedText5)%>

	<%=smartHF.getHTMLVar(4, 51, fw, sv.premstatus)%>

	<%=smartHF.getLit(4, 64, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(4, 74, fw, sv.register)%>
	<%=smartHF.getHTMLF4NSVar(4, 74, fw, sv.register)%>

	<%=smartHF.getLit(5, 2, generatedText14)%>

	<%=smartHF.getHTMLSpaceVar(5, 19, fw, sv.btdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 19, fw, sv.btdateDisp)%>

	<%=smartHF.getLit(5, 31, generatedText18)%>

	<%=smartHF.getHTMLSpaceVar(5, 51, fw, sv.occdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 51, fw, sv.occdateDisp)%>

	<%=smartHF.getLit(6, 2, generatedText15)%>

	<%=smartHF.getHTMLSpaceVar(6, 19, fw, sv.ptdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(6, 19, fw, sv.ptdateDisp)%>

	<%=smartHF.getLit(8, 2, generatedText7)%>

	<%=smartHF.getHTMLVar(8, 19, fw, sv.lifenum)%>

	<%=smartHF.getHTMLVar(8, 30, fw, sv.lifename)%>

	<%=smartHF.getLit(9, 2, generatedText8)%>

	<%=smartHF.getHTMLVar(9, 19, fw, sv.jlife)%>

	<%=smartHF.getHTMLVar(9, 30, fw, sv.jlifename)%>

	<%=smartHF.getLit(10, 2, generatedText9)%>

	<%=smartHF.getHTMLVar(10, 19, fw, sv.cownnum)%>

	<%=smartHF.getHTMLVar(10, 30, fw, sv.ownername)%>

	<%=smartHF.getLit(11, 2, generatedText10)%>

	<%=smartHF.getHTMLVar(11, 19, fw, sv.jownnum)%>

	<%=smartHF.getHTMLVar(11, 30, fw, sv.jownername)%>

	<%=smartHF.getLit(12, 2, generatedText11)%>

	<%=smartHF.getLit(14, 2, generatedText12)%>

	<%=smartHF.getHTMLSpaceVar(14, 25, fw, sv.billfreq)%>
	<%=smartHF.getHTMLF4NSVar(14, 25, fw, sv.billfreq)%>

	<%=smartHF.getHTMLVar(14, 30, fw, sv.freqdesc)%>

	<%=smartHF.getLit(16, 2, generatedText13)%>

	<%=smartHF.getHTMLSpaceVar(16, 25, fw, sv.mop)%>
	<%=smartHF.getHTMLF4NSVar(16, 25, fw, sv.mop)%>

	<%=smartHF.getHTMLVar(16, 30, fw, sv.mopdesc)%>

	<%=smartHF.getLit(18, 2, generatedText19)%>

	<%=smartHF.getHTMLVar(18, 25, fw, sv.billday)%>

	<%=smartHF.getLit(18, 30, generatedText16)%>

	<%=smartHF.getHTMLSpaceVar(18, 46, fw, sv.billcdDisp)%>
	<%=smartHF.getHTMLCalNSVar(18, 46, fw, sv.billcdDisp)%>

	<%=smartHF.getHTMLSTTSHyperLinkVar(21, 30, fw,generatedText25,sv.aiind)%>

	<%=smartHF.getHTMLTSAInputBoxVar(21, 45, fw, sv.aiind)%>

	<%=smartHF.getHTMLSTTSHyperLinkVar(22, 2, fw,generatedText20,sv.payind)%>

	<%=smartHF.getLit(22, 8, generatedText21)%>

	<%=smartHF.getHTMLTSAInputBoxVar(22, 10, fw, sv.payind)%>

	<%=smartHF.getLit(22, 30, generatedText22)%>

	<%=smartHF.getLit(22, 37, generatedText23)%>

	<%=smartHF.getLit(22, 43, generatedText24)%>

	<%=smartHF.getHTMLTSAInputBoxVar(22, 45, fw, sv.ddind)%>

	<%=smartHF.getHTMLSTTSHyperLinkVar(22, 61, fw,generatedText17,sv.grpind)%>

	<%=smartHF.getHTMLTSAInputBoxVar(22, 69, fw, sv.grpind)%>
 --%>



<%}%>

<%if (sv.S6668protectWritten.gt(0)) {%>
	<%S6668protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2NEW.jsp"%>
