<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6351";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>

<%
	S6351ScreenVars sv = (S6351ScreenVars) fw.getVariables();
%>
<%
	
	appVars.rollup(new int[] { 93 });
	if (appVars.ind04.isOn()) {
		sv.chdrstatus.setReverse(BaseScreenData.REVERSED);
		sv.chdrstatus.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.chdrstatus.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind05.isOn()) {
		sv.premstatus.setReverse(BaseScreenData.REVERSED);
		sv.premstatus.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.premstatus.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		sv.entity.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind99.isOn() && !appVars.ind89.isOn()) {
		sv.textfield.setInvisibility(BaseScreenData.INVISIBLE);
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
		<div class="col-md-4">
			<!-- <div class="col-md-3" style="min-width: 450px;"> -->
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>	
					
					<%
							formatValue = formatValue((sv.chdrnum.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.cnttype.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.ctypedes.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' 
							style="margin-left:1px; min-width:100px; max-width:300px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						
						</td></tr></table>
					
				</div>
			</div>
		<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group" >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div class="input-group" >
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("register");
						optionValue = makeDropDownList(mappedItems, sv.register, 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.register.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 250px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div></div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<%
						formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 150px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%
						formatValue = formatValue((sv.premstatus.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<%
						formatValue = formatValue((sv.cntcurr.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>

			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.lifenum.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
			</td><td>
						<%
							formatValue = formatValue((sv.lifename.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							 style="margin-left:1px;max-width: 200px;min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%></div>
							</table></tr></td>
						<!-- </div> -->
					</div>
				</div>
			
		<!-- </div> -->
		
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.jlife.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.jlifename.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 71px;margin-left:1px;max-width: 170px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						<!-- </div> -->
						</td></tr></table>
					</div>
				</div>
		
		
		
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
					<table>
					<tr>
						<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
					
								if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									formatValue = formatValue( formatValue );
								}
					
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="max-width:40px">
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
									<div class="blank_cell" style="width:40px"> &nbsp; </div>
							<%
								}
							%>
					</td>
					<td>
							<% formatValue = formatValue( (sv.entity.getFormData()).toString()); %>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
											"blank_cell" : "output_cell" %>'style="margin-left:2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
					</td>
					<td style="min-width:70px;">
					<div style="margin-left:1px;">
							<%
								qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
					
								if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									formatValue = formatValue( formatValue );
								}
					
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="margin-left:1px; min-width:100px;max-width:135px;">
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
									<div class="blank_cell" style="margin-left:1px;min-width:100px;max-width:135px;"> &nbsp; </div>
							<%
								}
							%>
							</div>
						</td>
						
					</tr>					
					</table>
				</div>
			</div>
		</div>
		
		

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6351' width='100%'>
							<thead>
								<tr class='info'>
									<!-- ILIFE-4905 Start -->
									<th><center> <%=resourceBundleHandler.gettingValueFromBundle(" ")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
									<th style="min-width:275px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
									<!-- ILIFE-4905 End -->
								</tr>
							</thead>

							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s6351screensfl");
									GeneralTable sfl1 = fw.getTable("s6351screensfl");
									S6351screensfl.set1stScreenRow(sfl, appVars, sv);
									int height = 372;
								%>
								<%
									S6351screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S6351screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
										//PINNACLE-2242
										if (appVars.ind02.isOn()) {
											sv.select.setReverse(BaseScreenData.REVERSED);
											sv.select.setColor(BaseScreenData.RED);
										}
										if (appVars.ind04.isOn()) {
											sv.select.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind03.isOn()) {
											sv.select.setInvisibility(BaseScreenData.INVISIBLE);
										}
										if (!appVars.ind01.isOn()) {
											sv.select.setHighLight(BaseScreenData.BOLD);
										}
										//PINNACLE-2242
								%>

								<tr class="tableRowTag" id='<%="tablerow" + count%>'>
									<td class="tableDataTag " style="width: 80px;" align="left">



										<%=sv.asterisk.getFormData()%>



									</td>
									
									
										<td align="center">
										<%
											/* if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { */
											 if (!appVars.ind03.isOn()) {
										%>
										 <input type="checkbox" value='<%=sv.select.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s6351screensfl" + "." +
						 				"select")'
										onKeyUp='return checkMaxLength(this)'
										 name='s6351screensfl.select_R<%=count%>'
										id='s6351screensfl.select_R<%=count%>'
										onClick="selectedRow('s6351screensfl.select_R<%=count%>')"
										class="UICheck"
										<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										disabled="disabled" <%}%>
										<% if((sv.flag).toString().trim().equalsIgnoreCase("Y") && (sv.hrider).toInt()== 0){	%>										
										disabled="disabled"
										<%} if((sv.flagdisable).toString().trim().equalsIgnoreCase("Y")
												&& (sv.asterisk.toString().trim().equalsIgnoreCase(""))){	%>										
										disabled="disabled"
										<%} if((sv.flag).toString().trim().equalsIgnoreCase("Y")
												&& (sv.asterisk.toString().trim().equalsIgnoreCase("*"))){	%>										
										disabled="disabled"
										<%}if (!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0")
									|| (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
										checked="checked" <%}%> /> <%
									 	}
									 %>
									</td>
									
																	
									
									
									<td><%=sv.cmpntnum.getFormData()%>/<%=sv.component.getFormData()%>
									</td>
									<td class="tableDataTag" style="width: 100px;" align="left">



										<%=sv.deit.getFormData()%>



									</td>
									<td class="tableDataTag" style="width: 150px;" align="left">



										<%=sv.statcode.getFormData()%>



									</td>
									<td class="tableDataTag" style="width: 150px;" align="left">
										<%=sv.pstatcode.getFormData()%>
									</td>
								</tr>
								<%
									count = count + 1;
										S6351screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {
		$('#dataTables-s6351').DataTable({
			ordering : false,
			searching : false,
			paging: false,
			scrollY: "350px",
			scrollCollapse: true,
			scrollX:true,
			info: false
		});		
	});
</script>



<%@ include file="/POLACommon2NEW.jsp"%>

