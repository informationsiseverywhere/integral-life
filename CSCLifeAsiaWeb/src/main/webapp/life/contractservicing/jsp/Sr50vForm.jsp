
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR50V";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr50vScreenVars sv = (Sr50vScreenVars) fw.getVariables();%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"up");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Created");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"By");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Modified");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"By ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date ");%>
<%		appVars.rollup(new int[] {93});
%>
<!-- 					ILIFE-697 start-sgadkari -->
<%{
		if (appVars.ind01.isOn()) {
			sv.message.setReverse(BaseScreenData.REVERSED);
			sv.message.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.message.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.message.setHighLight(BaseScreenData.BOLD);
		}
}

%>
<!-- ILIFE-2730 Life Cross Browser -Coding and UT- Sprint 4 D1: Task 7 starts -->
<!-- <style>
@media \0screen\,screen\9
{.blank_cell{margin-left:1px;}
.table1 {border-spacing:0;}
}
.table1 {border-spacing: 2.3px\9\0;}
@media screen and (-webkit-min-device-pixel-ratio:0) {
.table1 {border-spacing: 2.3px;}
}
</style>
 -->


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4" >
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract  Number")%></label>
					    		 <table>
					    		 <tr>
					    		 <td>
						    		<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					</td>
					<td>
	

  		
					<%					
					if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					</td>
					<td>

						<%					
						if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' id="cntdesc" style="max-width:150px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						    		
						</td>
						</tr>
						</table>
				    		</div>
					</div>
				    		
				    		
				  <div class="col-md-4" >
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
							<table>
							<tr>
							<td>
						    		<%					
					if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					</td>
					<td>
  		
					<%					
					if(!((sv.clntname01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.clntname01.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.clntname01.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>

						
					</td>
					</tr>
					</table>
						</div>
				   </div>		<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Joint  Owner")%></label>
							
									<table>
									<tr>
									<td>
						    		<%					
if(!((sv.jownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	</td>
	<td>

  		
<%					
if(!((sv.clntname02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td>
</tr>
</table>
						</div>
				   </div>	
		    </div>
				   
				  	<div class="row">	
				  	
				  	<div class="col-md-4"></div>
			    	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					    		<table>
					    		<tr>
					    		<td>
				   
						    		<%					
if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
						
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:100px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	</td>
	<td>  		
<%					
if(!((sv.clntname03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname03.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname03.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
						    		
</td>
</tr>
</table>
				    		</div>
					</div>
				  <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint  Life")%></label>
							<table>
							<tr>
							<td>
			    	
						    		<%					
							if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width:71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>

						</td>
						<td>


							<%					
							if(!((sv.clntname04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname04.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname04.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'  style="width:71px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>

				   </td>
				   </tr>
				   </table>
				    		</div>
					</div></div>
				    		<br>
				    		<hr>
				    		<br>
				    <div class="row">   <div class="col-md-4" >
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Follow-up Code")%></label>
						  
							<table>
							<tr>
							<td>
							
						    	<%					
					if(!((sv.fupcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					<td>
					<td>

	
					<%					
					if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:215px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>

		
					</td>
					</tr>
					</table>
						</div>
				   </div>		
			
			    	  <div class="col-md-4" >
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Created By")%></label>
						  
							<table>
							<tr>
							<td>
						   
						    	<%					
if(!((sv.crtuser.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtuser.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtuser.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:71px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td>
<td>
  		
<%					
if(!((sv.name01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.name01.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.name01.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:71px;max-width:150px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
						    		
</td>
</tr>
</table>
										
						</div>
				   </div>	
		   
			    <div class="col-md-2" >
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Date")%></label>
					    		     <div class="input-group">
						    		<%					
if(!((sv.crtdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	

				      			     </div>
				    		</div>
					</div></div>
				    		
				    		
				      <div class="row">
				      
				        <div class="col-md-4" ></div>
				      
				       <div class="col-md-4" >
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Modified By")%></label>
						  <table>
						  <tr>
						  <td>
						    		  		
<%					
if(!((sv.lstupuser.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lstupuser.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lstupuser.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  >
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td>
<td>
	
  		
<%					
if(!((sv.name02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.name02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.name02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;max-width:150px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
						    		
</td>
</tr>
</table>
						</div>
				   </div>		
			
			    	  <div class="col-md-4" >
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Date")%></label>
							 <div class="input-group"  style="max-width:100px;">
						    		<%					
if(!((sv.zlstupdtDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zlstupdtDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zlstupdtDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	
						    		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				
				
				<br><br>   
				
		<div class="table-responsive">
	         <table id="sr50vTable" class="table table-striped table-bordered table-hover"  id="sr50vTable" width="100%">
            
			 <thead style="display:none;">
			 <tr>
			 <td></td>
			 </tr>
			 </thead>

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[1];
int totalTblWidth = 0;
int calculatedValue =0;

	if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.message.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
	} else {		
		calculatedValue = (sv.message.getFormData()).length()*12;								
	}					
	totalTblWidth += calculatedValue;
	tblColumnWidth[0]= calculatedValue;
%>



		
		<%
		GeneralTable sfl = fw.getTable("sr50vscreensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>

		

                   <%
					
			
						
						Sr50vscreensfl
						.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (Sr50vscreensfl.hasMoreScreenRows(sfl)) {
						
					%>

		




<tbody>

            <tr class="tableRowTag" id='<%="tablerow"+count%>' >
			<td class="tableDataTag tableDataTagFixed" style="width:200px;" title="<%= sv.message.getFormData() %>" >
		<%-- <tr style="background:<%= backgroundcolor%>;">
					<td style="color:#434343; padding: 5px;width:<%=tblColumnWidth[0  ]%>" align="left"> --%>														
<!-- 					ILIFE-697 start-sgadkari -->
						 <input type='text' 
							maxLength='<%=sv.message.getLength()%>'
							 value='<%= sv.message.getFormData() %>' 
							 <%if(((new Byte((sv.message).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (sv.isScreenProtected())){%>
							 
				readonly="true" class="output_cell"  style = "width:920px;">  <!-- width changed for ILJ-462-->
<%
	}else {
%>						 size='<%=sv.message.getLength()*1.5%>'
							 onFocus='doFocus(this)' onHelp='return fieldHelp(sr50vscreensfl.message)' onKeyUp='return checkMaxLength(this)' 
							 name='<%="sr50vscreensfl" + "." +
							 "message" + "_R" + count %>'
							 class = "input_cell"							 
						
							  style = "width: <%=sv.message.getLength()*5%> px;">      <!--  ILIFE-2087   -->
							  <% } %>
<!-- 							  ILIFE-697 end -->
					</td>			
				</tr>
	<%
		
			count = count + 1;
			Sr50vscreensfl
			.setNextScreenRow(sfl, appVars, sv);
		}
	%>
	</table>
 
	</div> 


				
				
				
				</div></div> 
				
			
				
					<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
		$('#sr50vTable').DataTable({
			ordering : false,
			searching : false,
			scrollY : "326px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false
		});
	});
	
</script> 
	
				
				
<%@ include file="/POLACommon2NEW.jsp"%>

