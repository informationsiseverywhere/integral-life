<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SD5IJ";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sd5ijScreenVars sv = (Sd5ijScreenVars) fw.getVariables();%>
<%{
	if (appVars.ind01.isOn()) {
		sv.owner1.setReverse(BaseScreenData.REVERSED);
		sv.owner1.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.owner1.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind02.isOn()) {
		sv.owner1.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind03.isOn()) {
		sv.owner1.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind04.isOn()) {
		sv.owner2.setReverse(BaseScreenData.REVERSED);
		sv.owner2.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.owner2.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind05.isOn()) {
		sv.owner2.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind06.isOn()) {
		sv.owner2.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind07.isOn()) {
		sv.owner3.setReverse(BaseScreenData.REVERSED);
		sv.owner3.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.owner3.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind08.isOn()) {
		sv.owner3.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind09.isOn()) {
		sv.owner3.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind10.isOn()) {
		sv.owner4.setReverse(BaseScreenData.REVERSED);
		sv.owner4.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind10.isOn()) {
		sv.owner4.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind11.isOn()) {
		sv.owner4.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind12.isOn()) {
		sv.owner4.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind13.isOn()) {
		sv.owner5.setReverse(BaseScreenData.REVERSED);
		sv.owner5.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind13.isOn()) {
		sv.owner5.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind14.isOn()) {
		sv.owner5.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind15.isOn()) {
		sv.owner5.setInvisibility(BaseScreenData.INVISIBLE);
	}
}%>


<div class="panel panel-default">
    <div class="panel-body">     
    		<div class="row">	
				<div class="col-md-4"> 
				    <div class="form-group">  				    		
				 <%if ((new Byte((sv.owner1).getInvisible())).compareTo(new Byte(
                      BaseScreenData.INVISIBLE)) != 0) { %>
                      <div class="col-md-4" >
                          <div class="form-group">
                          <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 1")%></label>
                          <table>
	                          <tr>
	                          	<td>
	                          	<%
						          if ((new Byte((sv.owner1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
								%>                   
								<div><%=smartHF.getHTMLVarExt(fw, sv.owner1)%></div>	
						        <%
									} else {
								%>
						        <div class="input-group" >
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.owner1)%>
						        </div>
					        	<% } %> 
                          		</td>	
                          		<td>
									<%
									longValue = null;
									formatValue = null;
									%>
									<div class='<%= (sv.ownname1.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style='margin-left:1px;width: 140px;'>												
									<%
									if(!((sv.ownname1.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname1.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}							
												} else  {							
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname1.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>               
                          		</td>
                            </tr>
                          </table>
                        </div>
                      </div>
				 <%} %>	
				</div><!-- End of Form Group -->		
				</div>
				</div><!-- End of 1st Row -->
				
				<div class="row">	
				<div class="col-md-4"> 
				    	<div class="form-group">  	  
					    	<%if ((new Byte((sv.owner2).getInvisible())).compareTo(new Byte(
                      BaseScreenData.INVISIBLE)) != 0) { %>
                      <div class="col-md-4" >
                          <div class="form-group">
                          <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 2")%></label>
                          <table>
	                          <tr>
	                          	<td>
	                          	<%
						          if ((new Byte((sv.owner2).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                      || fw.getVariables().isScreenProtected()) {
								%>                   
								<div style="width:65px;"><%=smartHF.getHTMLVarExt(fw, sv.owner2)%></div>	
						        <%
									} else {
								%>
						        <div class="input-group" style="width: 125px;">
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.owner2)%>
						             <span class="input-group-btn">
						             <button class="btn btn-info"
						                style="font-size: 19px; border-bottom-width: 2px !important;"
						                type="button"
						                onClick="doFocus(document.getElementById('owner2')); doAction('PFKEY04')">
						                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						            </button>
						        </div>
					        	<% } %> 
                          		</td>	
                          		<td>
									<%
									longValue = null;
									formatValue = null;
									%>
									<div class='<%= (sv.ownname2.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style='margin-left:1px;width: 140px;'>												
									<%
									if(!((sv.ownname2.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname2.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}							
												} else  {							
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname2.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>               
                          		</td>
                            </tr>
                          </table>
                        </div>
                      </div>
				 <%} %>				
						</div><!-- End of Form Group -->		
					</div>
				</div><!-- End of 2nd Row -->
				<div class="row">	
				<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<%if ((new Byte((sv.owner3).getInvisible())).compareTo(new Byte(
                      BaseScreenData.INVISIBLE)) != 0) { %>
                      <div class="col-md-4" >
                          <div class="form-group">
                          <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 3")%></label>
                          <table>
	                          <tr>
	                          	<td>
	                          	<%
						          if ((new Byte((sv.owner3).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                      || fw.getVariables().isScreenProtected()) {
								%>                   

								<div style="width:65px;"><%=smartHF.getHTMLVarExt(fw, sv.owner3)%></div>	

						        <%
									} else {
								%>
						        <div class="input-group" style="width: 125px;">
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.owner3)%>
						             <span class="input-group-btn">
						             <button class="btn btn-info"
						                style="font-size: 19px; border-bottom-width: 2px !important;"
						                type="button"
						                onClick="doFocus(document.getElementById('owner3')); doAction('PFKEY04')">
						                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						            </button>
						        </div>
					        	<% } %> 
                          		</td>	
                          		<td>
									<%
									longValue = null;
									formatValue = null;
									%>
									<div class='<%= (sv.ownname3.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style='margin-left:1px;width: 140px;'>												
									<%
									if(!((sv.ownname3.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname3.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}							
												} else  {							
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname3.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>               
                          		</td>
                            </tr>
                          </table>
                        </div>
                      </div>
				 <%} %>	
						</div><!-- End of Form Group -->		
					</div>
				</div><!-- End of 3rd Row -->
				
				<div class="row">	
				<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<%if ((new Byte((sv.owner4).getInvisible())).compareTo(new Byte(
                      BaseScreenData.INVISIBLE)) != 0) { %>
                      <div class="col-md-4" >
                          <div class="form-group">
                          <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 4")%></label>
                          <table>
	                          <tr>
	                          	<td>
	                          	<%
						          if ((new Byte((sv.owner4).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                      || fw.getVariables().isScreenProtected()) {
								%>                   
								<div style="width:65px;"><%=smartHF.getHTMLVarExt(fw, sv.owner4)%></div>	
						        <%
									} else {
								%>
						        <div class="input-group" style="width: 125px;">
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.owner4)%>
						             <span class="input-group-btn">
						             <button class="btn btn-info"
						                style="font-size: 19px; border-bottom-width: 2px !important;"
						                type="button"
						                onClick="doFocus(document.getElementById('owner4')); doAction('PFKEY04')">
						                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						            </button>
						        </div>
					        	<% } %> 
                          		</td>	
                          		<td>
									<%
									longValue = null;
									formatValue = null;
									%>
									<div class='<%= (sv.ownname4.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style='margin-left:1px;width: 140px;'>												
									<%
									if(!((sv.ownname4.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname4.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}							
												} else  {							
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname4.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>               
                          		</td>
                            </tr>
                          </table>
                        </div>
                      </div>
				 <%} %>	
						</div><!-- End of Form Group -->		
					</div>
				</div><!-- End of 4th Row -->
				<div class="row">	
				<div class="col-md-4"> 
				    	<div class="form-group">  	
				    		<%if ((new Byte((sv.owner5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
                      <div class="col-md-4" >
                          <div class="form-group">
                          <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 5")%></label>
                          <table>
	                          <tr>
	                          	<td>
	                          	<%
						          if ((new Byte((sv.owner5).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                      || fw.getVariables().isScreenProtected()) {
								%>                   
								<div style="width:65px"><%=smartHF.getHTMLVarExt(fw, sv.owner5)%></div>	
						        <%
									} else {
								%>
						        <div class="input-group" style="width: 125px;">
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.owner5)%>
						             <span class="input-group-btn">
						             <button class="btn btn-info"
						                style="font-size: 19px; border-bottom-width: 2px !important;"
						                type="button"
						                onClick="doFocus(document.getElementById('owner5')); doAction('PFKEY04')">
						                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						            </button>
						        </div>
					        	<% } %> 
                          		</td>	
                          		<td>
									<%
									longValue = null;
									formatValue = null;
									%>
									<div class='<%= (sv.ownname5.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style='margin-left:1px;width: 140px;'>												
									<%
									if(!((sv.ownname5.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname5.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}							
												} else  {							
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownname5.getFormData()).toString());
														} else {
															formatValue = formatValue( longValue);
														}
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>               
                          		</td>
                            </tr>
                          </table>
                        </div>
                      </div>
				 <%} %>	
						</div><!-- End of Form Group -->		
					</div>
				</div><!-- End of 5th Row -->
			    	
	</div>  <!-- panel-body-->
</div>  <!--panel-default  -->


<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
