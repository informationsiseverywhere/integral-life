<%@ page language="java" pageEncoding="UTF-8" %>  
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5131";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5131ScreenVars sv = (S5131ScreenVars) fw.getVariables();%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.opcda.setReverse(BaseScreenData.REVERSED);
			sv.opcda.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.opcda.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.agerate.setReverse(BaseScreenData.REVERSED);
			sv.agerate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.agerate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.oppc.setReverse(BaseScreenData.REVERSED);
			sv.oppc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.oppc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.insprm.setReverse(BaseScreenData.REVERSED);
			sv.insprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.insprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.extCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.extCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.extCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.reasind.setReverse(BaseScreenData.REVERSED);
			sv.reasind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.znadjperc.setReverse(BaseScreenData.REVERSED);
			sv.znadjperc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.znadjperc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.zmortpct.setReverse(BaseScreenData.REVERSED);
			sv.zmortpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.zmortpct.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage/rider ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reas");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flat");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ind");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured%");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Load%");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjust ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duration");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality%");%>

<%{
		if (appVars.ind50.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>




<div class="panel panel-default">
 <div class="panel-body">   
 	<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
 <label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</label>
<%}%>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div></div>
 	<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life_no")%>
</label>
<%}%>

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div>

<div class="col-md-4"> 
	<div class="form-group">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Coverage_no")%>
</label>
<%}%>
<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>

<div class="col-md-4"> 
	<div class="form-group">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Rider_no")%>
</label>
<%}%>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div>
<div class="row">	
<div class="col-md-4"> 
	<div class="form-group">
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life assured")%>
</label>
<%}%>

<table>
<tr>
<td>
<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="lifcnum" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
<td>
<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="linsname" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> col-md-9' style="min-width: 150px;margin-left: 1px !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td>
  </tr>
	</table>
</div>
</div>
</div>

<div class="row">	
<div class="col-md-4"> 
	<div class="form-group">
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Smoking")%>
</label>
<%}%>


<%if ((new Byte((sv.smoking01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.smoking01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>

<div class="col-md-4"> 
	<div class="form-group">
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Occupation")%>
</label>
<%}%>

<%if ((new Byte((sv.occup01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.occup01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occup01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occup01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</div>
	</div>
				<div class="col-md-4">
					<div class="form-group">
						<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<div class="label_txt">
							<%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%>
						</div>
						<%}%>

						<table>
<tr>
<td>
							<%if ((new Byte((sv.pursuit01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


							<%					
		if(!((sv.pursuit01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div id="pursuit01"
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>
							<%}%>
</td>
<td>
							<%if ((new Byte((sv.pursuit02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


							<%					
		if(!((sv.pursuit02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div id="pursuit02"
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 150px;margin-left: 1px !important;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>
							<%}%>
							</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

 	<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Joint life")%>
</label>
<%}%>
<table>
<tr>
<td>
<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="jlifcnum" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td>
<td>
<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="jlinsname"  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> col-md-9' style="min-width: 150px;margin-left: 1px !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td>
	</tr>
</table>
</div></div>
</div>


<div class="row">	
	<div class="col-md-4"> 
		<div class="form-group">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Smoking")%>
</label>
<%}%>

<%if ((new Byte((sv.smoking02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.smoking02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>


 	<div class="col-md-4"> 
  		<div class="form-group">
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Occupation")%>
</label>
<%}%>


<br/>

<%if ((new Byte((sv.occup02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.occup02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occup02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occup02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:100px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div>

<div class="col-md-4"> 
  		<div class="form-group">
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%>
</label>
<%}%>
<table>
<tr>
<td>
<%if ((new Byte((sv.pursuit03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.pursuit03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="pursuit03" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td>
<td>
<%if ((new Byte((sv.pursuit04).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.pursuit04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="pursuit04" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 150px;margin-left: 1px !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td>
  </tr>
</table>
</div></div></div>


<div class="row">	
	<div class="col-md-4"> 
		<div class="form-group">
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Coverage/rider")%>
</label>
<%}%>

<table>
<tr>
<td>
<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="crtable" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td>
<td>
<%if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="crtabdesc" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 150px;margin-left: 1px !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td>
	</tr>
</table></div></div></div>


<%-- <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[11];
int totalTblWidth = 0;
int calculatedValue =0;

																			if((resourceBundleHandler.gettingValueFromBundle("Header1").length()*12) >= (sv.opcda.getFormData()).length()*12 ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = ((sv.opcda.getFormData()).length()*12);								
						}		
									totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.shortdesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*6;								
			} else {		
				calculatedValue = (sv.shortdesc.getFormData()).length()*6;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.reasind.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*6;								
						} else {		
							calculatedValue = (sv.reasind.getFormData()).length()*4;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.znadjperc.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
						} else {		
							calculatedValue = (sv.znadjperc.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.agerate.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
						} else {		
							calculatedValue = (sv.agerate.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.oppc.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
						} else {		
							calculatedValue = (sv.oppc.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.insprm.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*6;								
						} else {		
							calculatedValue = (sv.insprm.getFormData()).length()*6;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
		/* BRD-306 START */
								if(resourceBundleHandler.gettingValueFromBundle("Header11").length() >= (sv.premadj.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header11").length())*6;								
								} else {		
									calculatedValue = (sv.premadj.getFormData()).length()*6;								
								}					
																totalTblWidth += calculatedValue;
						tblColumnWidth[7]= calculatedValue;
		/* BRD-306 END */
														if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.extCessTerm.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*8;								
						} else {		
							calculatedValue = (sv.extCessTerm.getFormData()).length()*6;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[8]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*8;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*6;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[9]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header10").length() >= (sv.zmortpct.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header10").length())*6;								
						} else {		
							calculatedValue = (sv.zmortpct.getFormData()).length()*6;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[10]= calculatedValue;
		totalTblWidth +=100;
			%> --%>
		<%
		GeneralTable sfl = fw.getTable("s5131screensfl");
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>

<!-- ILIFE-2721 Life Cross Browser - Sprint 4 D4 : Task 2 fake container starts -->	

<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id='dataTables-s5131'>
	              <thead  class="fixedThead">
					<tr class='info'>
			
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
			         								
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
		
						<%if (sv.uwFlag == 1) { %> 
			     	 	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header12")%></th>
			     	 	<%} %>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>	
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></th>	
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></th>
						
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></th>
						</tr>
	  				</thead>
	             <tbody>
			<%
		
			S5131screensfl.set1stScreenRow(sfl, appVars, sv);
			int count = 1;
			Map<String,Map<String,String>> opCdaMap = appVars.loadF4FieldsLong(new String[] {"opcda"},sv,"E",baseModel);
			Map<String,Map<String,String>> reasIndMap = appVars.loadF4FieldsLong(new String[] {"reasind"},sv,"E",baseModel);
		
			while (S5131screensfl.hasMoreScreenRows(sfl)) {	
		%>

<%{
		if (appVars.ind01.isOn()) {
			sv.opcda.setReverse(BaseScreenData.REVERSED);
			sv.opcda.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.opcda.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.agerate.setReverse(BaseScreenData.REVERSED);
			sv.agerate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.agerate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.oppc.setReverse(BaseScreenData.REVERSED);
			sv.oppc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.oppc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.insprm.setReverse(BaseScreenData.REVERSED);
			sv.insprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.insprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.extCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.extCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.extCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.reasind.setReverse(BaseScreenData.REVERSED);
			sv.reasind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.znadjperc.setReverse(BaseScreenData.REVERSED);
			sv.znadjperc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.znadjperc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.zmortpct.setReverse(BaseScreenData.REVERSED);
			sv.zmortpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.zmortpct.setHighLight(BaseScreenData.BOLD);
		}
		/*  BRD-306 START */
		if (appVars.ind11.isOn()) {
			sv.premadj.setReverse(BaseScreenData.REVERSED);
			sv.premadj.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.premadj.setHighLight(BaseScreenData.BOLD);
		}
		/*  BRD-306 END */
		if(sv.uwFlag == 1){
			if (appVars.ind12.isOn()) {
				sv.uwoverwrite.setReverse(BaseScreenData.REVERSED);
				sv.uwoverwrite.setColor(BaseScreenData.RED);
			}
			if (appVars.ind22.isOn()) {
				sv.uwoverwrite.setEnabled(BaseScreenData.DISABLED);
				sv.uwoverwrite.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.opcda.setEnabled(BaseScreenData.DISABLED);
				sv.opcda.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind14.isOn()) {
				sv.agerate.setEnabled(BaseScreenData.DISABLED);
				sv.agerate.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind15.isOn()) {
				sv.oppc.setEnabled(BaseScreenData.DISABLED);
				sv.oppc.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind16.isOn()) {
				sv.insprm.setEnabled(BaseScreenData.DISABLED);
				sv.insprm.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind17.isOn()) {
				sv.extCessTerm.setEnabled(BaseScreenData.DISABLED);
				sv.extCessTerm.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind18.isOn()) {
				sv.reasind.setEnabled(BaseScreenData.DISABLED);
				sv.reasind.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind19.isOn()) {
				sv.znadjperc.setEnabled(BaseScreenData.DISABLED);
				sv.znadjperc.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind20.isOn()) {
				sv.zmortpct.setEnabled(BaseScreenData.DISABLED);
				sv.zmortpct.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind21.isOn()) {
				sv.premadj.setEnabled(BaseScreenData.DISABLED);
				sv.premadj.setHighLight(BaseScreenData.BOLD);
			}
		}
	}
	%>
	
	<tr class="tableRowTag " id='<%="tablerow"+count%>' >
			<td class="tableDataTag tableDataTagFixed" align="left">														
												
				      		 		     <%	
						mappedItems = (Map) opCdaMap.get("opcda");
						optionValue = makeDropDownList( mappedItems , sv.opcda,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.opcda.getFormData()).toString().trim());  
					%>
					<% if((new Byte((sv.opcda).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					<div class='output_cell' style="width:160px;">
					<!--ILIFE-1035 start kpalani6 -->
					  <%if(longValue != null){ %> 
					   <%=longValue%>
					   <% } %>
					</div>
					<!--ILIFE-1035 end kpalani6 -->
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.opcda).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
					<%
					} 
					%>
					<select name='<%="s5131screensfl.opcda_R"+count%>' id='<%="s5131screensfl.opcda_R"+count%>' type='list' 
					
					class = 'input_cell'
					style="width:160px;"
					<%if(sv.autoFlag.getFormData().trim().equalsIgnoreCase("Y")){ %>
					disabled="disabled"
					<% } %>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.opcda).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>
									</td>
				    									<td class="tableDataTag"  align="center">
				    									
				    									
				    																	
							<% 
								
						mappedItems = (Map) reasIndMap.get("reasind");
						optionValue = makeDropDownList( mappedItems , sv.reasind,2,resourceBundleHandler);  
						//longValue = (String) mappedItems.get((sv.reasind.getFormData()).toString().trim());  
					
   longValue=sv.reasind.getFormData();
   if("1".equals(longValue)){
   longValue=resourceBundleHandler.gettingValueFromBundle("Apply to Premium only");
   }else if("2".equals(longValue)){
    longValue=resourceBundleHandler.gettingValueFromBundle("Apply to Reassurance Premium only");}
   else if("3".equals(longValue)){
    longValue=resourceBundleHandler.gettingValueFromBundle("Apply to both");
    }
	if((new Byte((sv.reasind.getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()))){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:256px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
<% if("red".equals((sv.reasind).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;"> 
					<%
					} 
					%>
					
										

						

					<select name='reasind' type='list' style="width:256px;"
					<% 
				if((new Byte((sv.reasind).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.reasind).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
	class = 'input_cell' 
			<%
				} 
			%>
			>		<%
						if((sv.opcda.getFormData().toString().trim())!=null && !(sv.opcda.getFormData().toString().trim().equals(""))){ 
						%>
							<option value=""   >-----------------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>-----------------	</option>
							<option value="1" <%if("1".equals(sv.reasind.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("Apply to Premium only")%></option>
							<option value="2" <%if("2".equals(sv.reasind.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("Apply to Reassurance Premium only")%> </option>
							<option value="3" <%if("3".equals(sv.reasind.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("Apply to both")%> </option>
					
					
					<%}	else{%>
							<option value=""  SELECTED >-----------------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>----------------- </option>
							<option value="1"  ><%=resourceBundleHandler.gettingValueFromBundle("Apply to Premium only")%></option>
							<option value="2"  ><%=resourceBundleHandler.gettingValueFromBundle("Apply to Reassurance Premium only")%></option>
							<option value="3" ><%=resourceBundleHandler.gettingValueFromBundle("Apply to both")%></option>
					<%} %>
				
					</select>
					
					
					
					<% if("red".equals((sv.reasind).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					} 
					%>
	
																	
					
									</td>
														  <%if (sv.uwFlag == 1) { %> 
			   
			   <td  align="left">														
					<%	
					 	longValue=sv.uwoverwrite.getFormData();
					   	if("N".equals(longValue)){
					    	longValue="No";
					    }
					   	else if("Y".equals(longValue)){
					    	longValue="Yes";
					    }
						if((new Byte((sv.uwoverwrite.getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()))){ 
					%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
											<%if(longValue != null){%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%}%>
						</div> <%
							longValue = null;
						%> <% }else {%> 
							
						<select	name='<%="s5131screensfl" + "." +
						 "uwoverwrite" + "_R" + count %>'
												id='<%="s5131screensfl" + "." +
						 "uwoverwrite" + "_R" + count %>'
												type='list' style="width: 120px;"
												onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.uwoverwrite)'
												onKeyUp='return checkMaxLength(this)'
												class="input_cell"
			   			<%
							if ((new Byte((sv.uwoverwrite).getEnabled())).compareTo(new Byte(
							BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
							%>
									readonly="true" class="output_cell"
							<%
							} else if ((new Byte((sv.uwoverwrite).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
												class="bold_cell" <%
							} else {
							%>
												class='
							<%=(sv.uwoverwrite).getColor() == null ? "input_cell"
							: (sv.uwoverwrite).getColor().equalsIgnoreCase("red") ? "input_cell red reverse"
							: "input_cell"%>'
												<%
							}
							%>
							<%if(sv.skipautouw != 1 || !sv.autoFlag.getFormData().trim().equalsIgnoreCase("Y")){ %>
							disabled="disabled"
							<% } %>
							>
								<% if(sv.autoFlag.getFormData().trim().equalsIgnoreCase("Y")) {%>
								<option value="Y"
									<% if(sv.uwoverwrite.getFormData().trim().equalsIgnoreCase("Y")) {%>
									Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<% if(sv.uwoverwrite.getFormData().trim().equalsIgnoreCase("N")) {%>
									Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
								<% } else { %>
								<option value=""></option>
								<% } %>

											</select>
											<%
					} 
					%>
							 </td>
							 <%} %>				
									<!-- ILIFE-1035 start kpalani6 -->
				    									<td class="tableDataTag" style="width:180px;" align="center">									
										<!-- ILIFE-1035 end kpalani6 -->							
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.znadjperc).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.znadjperc);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.znadjperc.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.znadjperc.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.znadjperc)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "znadjperc" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "znadjperc" + "_R" + count %>'
						  <% if((new Byte((sv.znadjperc).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled
							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
						  style = "width: 90px; text-align: right"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>

									<!-- ILIFE-1035 start kpalani6 -->
				    									<td class="tableDataTag" align="center">									
										<!-- ILIFE-1035 end kpalani6 -->							
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.agerate).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.agerate);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.agerate.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.agerate.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.agerate)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "agerate" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "agerate" + "_R" + count %>'
						 
						  <% if((new Byte((sv.agerate).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled
							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
						 class = "input_cell"
						  style = "width:35px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td><!-- ILIFE-1035 start kpalani6 -->
				    									<td class="tableDataTag" align="center">									
										<!-- ILIFE-1035 end kpalani6 -->			
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.oppc).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.oppc);
							
						%>					
						
						<input type='text' 
						maxLength='<%=sv.oppc.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.oppc.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.oppc)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "oppc" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "oppc" + "_R" + count %>'
						   <% if((new Byte((sv.oppc).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled
							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
						 class = "input_cell"
						  style = "width:53px; text-align: right"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event,true);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td style="color:#434343; padding: 5px; width:80px;font-weight: bold;" align="left">									
														
								
				<%	
			qpsf = sfl.getCurrentScreenRow().getFieldXMLDef((sv.insprm).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.insprm,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input type='text' 
	<%if((sv.insprm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 95px;margin-top: 3px;"<% }%>
	maxLength='<%=sv.insprm.getLength()%>'
	 value='<%=valueThis %>' 
	 size='<%=sv.insprm.getLength()%>'
	 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s5131screensfl.insprm)' onKeyUp='return checkMaxLength(this)' 
	 name='<%="s5131screensfl" + "." +
	 "insprm" + "_R" + count %>'
	id='<%="s5131screensfl" + "." +
	 "insprm" + "_R" + count %>'
	  <% if((new Byte((sv.insprm).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled
							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
	 class = "input_cell"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'>
				
</td>
					<!-- BRD-306 START -->
<td style="color:#434343; padding: 5px; width:80px;font-weight: bold;" align="left">									
														
								
				<%	
			qpsf = sfl.getCurrentScreenRow().getFieldXMLDef((sv.premadj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.premadj,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input type='text' 
	<%if((sv.premadj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 110px;margin-top: 3px;"<% }%>
	maxLength='<%=sv.premadj.getLength()%>'
	 value='<%=valueThis %>' 
	 size='<%=sv.premadj.getLength()%>'
	 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s5131screensfl.premadj)' onKeyUp='return checkMaxLength(this)' 
	 name='<%="s5131screensfl" + "." +
	 "premadj" + "_R" + count %>'
	id='<%="s5131screensfl" + "." +
	 "premadj" + "_R" + count %>'
	  <% if((new Byte((sv.premadj).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							class="output_cell" 
					         readonly="true"
							disabled
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
	 class = "input_cell"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'>
			
					</td>
									<!--  BRD-306 END -->
				    									<td class="tableDataTag" align="center">									
															
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.extCessTerm).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.extCessTerm);
							
						%>					
						<input type='text' 
						maxLength='<%=sv.extCessTerm.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.extCessTerm.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.extCessTerm)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "extCessTerm" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "extCessTerm" + "_R" + count %>'
						  <% if((new Byte((sv.extCessTerm).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
						 class = "input_cell"
						  style = "width: 70px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
									</td>
				    									<td class="tableDataTag" align="center">						
												
						<%
		/* ILIFE-1838 started by vjain60 */
		formatValue = (sv.select.getFormData()).toString();
					
					 /* ILIFE-1838 ended by vjain60 */
%>				 
						 
						 						 <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%=formatValue%>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "output_cell"
						 style = "width: 70px;"
						 readonly="true"
						  >
									</td>
				    									<td class="tableDataTag" align="center">									
						
						 						 <input type='text' 
						maxLength='<%=sv.zmortpct.getLength()%>'
						 
						 <%
						if((sv.opcda.getFormData().toString().trim())!=null && !(sv.opcda.getFormData().toString().trim().equals(""))){ 
							if(!sv.zmortpct.getFormData().equals("000")){
						%>					

							value='<%=sv.zmortpct.getFormData() %>' 

						<% }
						else {%>
							value=''
						
						<%}}%>
						  
						 size='<%=sv.zmortpct.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5131screensfl.zmortpct)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5131screensfl" + "." +
						 "zmortpct" + "_R" + count %>'
						 id='<%="s5131screensfl" + "." +
						 "zmortpct" + "_R" + count %>'
						  <% if((new Byte((sv.zmortpct).getEnabled()))
				             .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
							 %>
							readonly="true"
							disabled
							class="output_cell" 
					         	
					        <% }else{ %>
					     
						 class = "input_cell"
						  <%} %>
						 class = "input_cell"
						  style = "width: 90px; text-align: right"
						  
						  >
									</td>					
	</tr>

	<%
	count = count + 1;
	S5131screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

	                            </tbody>
	                     </table>
	                 </div>
	             </div>
	         </div>
	     </div>
</div>
</div>
<script>
$(document).ready(function() {	
	var table = $('#dataTables-s5131').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollCollapse:true,
    	paging: false,
    	info: false
   	});
});
</script>
<style>
th, td { white-space: nowrap; }
@media screen and (max-width: 1600px){
	.dataTables_scrollBody {
	    max-height: 200px !important;
	}
}
@media screen and (max-width: 1366px){
	.dataTables_scrollBody {
	    max-height: 155px !important;
	}
}
@media screen and (max-width: 1280px){
	.dataTables_scrollBody {
	    border-top: 1px solid #ccc;
	    max-height: 265px !important
	} 
}
</style>

<%@ include file="/POLACommon2NEW.jsp"%>


<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("No")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Code")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Code")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>
<%}%>

</tr></table></div>