

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5047";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S5047ScreenVars sv = (S5047ScreenVars) fw.getVariables();%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------- Refusal   Details ----------------------------------");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversal (Y/N)          ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cease all Future offers          ");%>

<%{
		if (appVars.ind04.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.reversalInd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.reversalInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.reversalInd.setReverse(BaseScreenData.REVERSED);
			sv.reversalInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reversalInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.ceaseInd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.ceaseInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.ceaseInd.setReverse(BaseScreenData.REVERSED);
			sv.ceaseInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.ceaseInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>


<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No       ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CCD   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life   ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to Date ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Increase Due ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-------------------------------- Increase Records ----------------------------");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New/Old");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New/Old");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ref");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov/Rid");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date Incr");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Premium");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flag");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind07.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.btdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.btdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.btdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>




<div class="panel panel-default">
        <div class="panel-body">
        	<div class="row">
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract No"))%></label>
        				<table><tr><td>
						    		
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  
	</td><td>
	
  		
									<%					
									if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
  
	</td><td>

  		
								<%					
									if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 150px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
						  
	</td></tr></table>
        		</div>		
			</div>

				<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status"))%></label>
        				<table>
        				<tr>
        					<td>
        						<%					
								if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
										} else {
										formatValue = formatValue( longValue);
									}
							
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
			        	</td>
			        	<td>
			        		<%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
			        	</td>
        				</tr>
        				</table>
        				
        				</div>
        		</div>
        				
        		<div class="col-md-4">
        			<div class="form-group">
        			<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("CCD"))%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
        				
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width:80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
        			</div>
        			</div>	
</div>

<div class="row">
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Owner"))%></label>
        				<table>
        				<tr>
        					<td>
        						<%					
										if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					      </td>
					      <td>
					      		<%					
								if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 150px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
					      </td>
        				</tr>
        				</table>
        				
        			</div>
        		</div>	
        		
        		
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						<td>
  
						<%					
						if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
        				</table>
        			</div>
        		</div>		
        		
        		
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("J/Life"))%></label>
        				<table><tr><td>
        				<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:70px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        				</td></tr></table>
        			</div>
        		</div>			
        	</div>	
        	
        	<div class="row">
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Paid-to Date"))%></label>
        				
        				<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
        			</div>
        		</div>
        		
        	
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Billed-to Date"))%></label>
        				<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
        			</div>
        		</div>	
        		
        		
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Increase Due"))%></label>
        				<%					
		if(!((sv.cpiDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cpiDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cpiDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>		
        	</div>			

			<br>
           <div class="row">
				<div class="col-md-12">
					
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Increase Records")%></label></a>
						</li>
						<li><a href="#tab2" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Refusal Details")%></label></a>
						</li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1">
						
						<div class="row">		
            <div class="col-md-12">
               <div class="table-responsive">
    	 	      <table class="table table-striped table-bordered table-hover" id='dataTables-s5047' width="100%">
    	 	      <thead>
    	 	      <tr class='info'>	
    	 	      	<th style="text-align: center; min-width:100px;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
		            <th style="text-align: center; min-width:100px;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
    	 	     	<th style="text-align: center; min-width:100px;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
					<th style="text-align: center; min-width:150px;"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></th>
	              </tr>
	              </thead>
	              <tbody>
	              <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[9];
int totalTblWidth = 0;
int calculatedValue =0;
										if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*3;								
					} else {		
						calculatedValue = (sv.select.getFormData()).length()*3;								
					}					
										totalTblWidth += calculatedValue;
	tblColumnWidth[0]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.coverage.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
		} else {		
			calculatedValue = (sv.coverage.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[1]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.rider.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
		} else {		
			calculatedValue = (sv.rider.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[2]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.crrcdDisp.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
		} else {		
			calculatedValue = (sv.crrcdDisp.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[3]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.newsumi.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*10;								
		} else {		
			calculatedValue = (sv.newsumi.getFormData()).length()*10;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[4]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.lastSumi.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*8;								
		} else {		
			calculatedValue = (sv.lastSumi.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[5]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.newinst.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
		} else {		
			calculatedValue = (sv.newinst.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[6]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.lastInst.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*8;								
		} else {		
			calculatedValue = (sv.lastInst.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[7]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.refusalFlag.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*8;								
		} else {		
			calculatedValue = (sv.refusalFlag.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[8]= calculatedValue;
	%>
	<%
		GeneralTable sfl = fw.getTable("s5047screensfl");
		int height;
		if(sfl.count()*27 > 90) {
		height = 90 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>
		
		<%

	String backgroundcolor="#FFFFFF";
	
	S5047screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;

	while (S5047screensfl
	.hasMoreScreenRows(sfl)) {
		//ILIFE-476 start adubey33
	
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		
		//ILIFE-476 end adubey33
%>

	              
	              
	             <tr class="tableRowTag" id='<%="tablerow"+count%>'  >
	
								<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px; margin-left:-2px; border-right: 1px solid #dddddd;"   align="center" >		
					
								
				
									 
					 
					 <!-- MIBT-224 -->		 
					<% if(count >= 1)
					{
						
						//ILIFE-476 start adubey33
						if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%> 			
						 <input type="checkbox"  id='s5047screensfl.select_R<%=count%>'
							 value='<%= sv.select.getFormData() %>' 
							 onFocus='doFocus(this)' onHelp='return fieldHelp("s5047screensfl" + "." +
							 "select")' onKeyUp='return checkMaxLength(this)' 
							 name='s5047screensfl.select_R<%=count%>'
							 onClick="selectedRow('s5047screensfl.select_R<%=count%>')"
							 class="UICheck"
						 />
					<% }
						//ILIFE-476 end adubey33
					
					}
					%> 
					 					
					 <!-- MIBT-224 ENDS-->		 
											
									</td>
				    							
													<td style="width:<%=tblColumnWidth[1]%>px; font-weight: bold; " align="left">									
										
											&nbsp;<%= formatValue( sv.coverage.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[2]%>px; font-weight: bold;" align="left">									
										
											&nbsp;<%= formatValue( sv.rider.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[3]%>px; font-weight: bold;" align="left">									
										
											&nbsp;<%= formatValue( sv.crrcdDisp.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[4]%>px;  font-weight: bold;" style="text-align: right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.newsumi).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.newsumi,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) )%>&nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[5]%>px; font-weight: bold;" style="text-align: right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.lastSumi).getFieldName());						
//						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.lastSumi,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) )%>&nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[6]%>px; font-weight: bold;" style="text-align: right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.newinst).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.newinst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) )%>&nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[7]%>px; font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.lastInst).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.lastInst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) )%>&nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[8]%>px; font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.refusalFlag.getFormData() )%>	 
										
				 
				
					</td>	
						

		
		
	</tr>


	<%
	if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
		backgroundcolor="#E0FFFF";
		}else{
		backgroundcolor="#FFFFFF";
		}
	count = count + 1;
	S5047screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	</tbody>
	</table>
	<script>
	$(document).ready(function() {
		var showval= document.getElementById('show_lbl').value;
		var toval= document.getElementById('to_lbl').value;
		var ofval= document.getElementById('of_lbl').value;
		var entriesval= document.getElementById('entries_lbl').value;	
		var nextval= document.getElementById('nxtbtn_lbl').value;
		var previousval= document.getElementById('prebtn_lbl').value;
    	$('#dataTables-s5047').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
			scrollX: true,
			info: true,
			 language: {
                 "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
                 "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
                 "paginate": {                
                     "next":       nextval,
                     "previous":   previousval
                 }
               }     
			
      	});
    });
</script>
	           
		

	
		
</div>
</div>

</div>
</div>
<div class="tab-pane fade" id="tab2">
							<div class="row">
                          <div class="col-md-4"> 
    	 					<div class="form-group"> 
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Reversal")%></label>
    	 				    
								<input type='checkbox' name='reversalInd' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(reversalInd)' onKeyUp='return checkMaxLength(this)'    
								<%
								
								if((sv.reversalInd).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.reversalInd).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.reversalInd).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('reversalInd')"/>
    	 					</div>
    	 				</div>	
    	 				
    	 				<!-- <div class="col-md-2"> </div> -->
    	 				 <div class="col-md-4"> 
    	 					<div class="form-group"> 
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Cease All Future Offers")%></label>
    	 					<input type='checkbox' name='ceaseInd' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(ceaseInd)' onKeyUp='return checkMaxLength(this)'    
									<%
									
									if((sv.ceaseInd).getColor()!=null){
												 %>style='background-color:#FF0000;'
											<%}
											if((sv.ceaseInd).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }if((sv.ceaseInd).getEnabled() == BaseScreenData.DISABLED){%>
									    	   disabled
											
											<%}%>
									class ='UICheck' onclick="handleCheckBox('ceaseInd')"/>
									
									<input type='checkbox' name='ceaseInd' value=' ' 
									
									<% if(!(sv.ceaseInd).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }%>
									
									style="visibility: hidden" onclick="handleCheckBox('ceaseInd')"/>
    	 					</div>
    	 				</div>	
    	 				
<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
    	 				
</div>
</div>



</div>
</div>
</div>


<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("------------------------- Refusal   Details ----------------------------------")%>
</div>

</tr></table></div>


</div>
</div>





<%@ include file="/POLACommon2NEW.jsp"%>

