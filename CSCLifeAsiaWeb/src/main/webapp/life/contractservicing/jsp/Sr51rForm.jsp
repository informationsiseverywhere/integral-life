
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR51R";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr51rScreenVars sv = (Sr51rScreenVars) fw.getVariables();%>
<%if (sv.Sr51rscreenWritten.gt(0)) {%>
	<%Sr51rscreen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("A - Premium Holiday Process");%>
	<%StringData generatedText3 = new StringData("B - Premium Holiday Enquiry");%>
	<%StringData generatedText4 = new StringData("Contract Number ");%>
	<%sv.chdrsel.setClassString("");%>
	<%StringData generatedText5 = new StringData("Action ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
	
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    			<div class="input-group" style="width:130px;">  
					    		
					    		<input name='chdrsel' id='chdrsel'
		type='text'
		value='<%=sv.chdrsel.getFormData()%>'
		maxLength='<%=sv.chdrsel.getLength()%>'
		size='<%=sv.chdrsel.getLength()%>'
		onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'
		
		<%
			if((new Byte((sv.chdrsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
		%>
		readonly="true"
		class="output_cell"	 >
		
		<%
			}else if((new Byte((sv.chdrsel).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		
		%>
		class="bold_cell" >
		<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
		
		<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')">
		<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
		</a>
		 --%>
		<%
			}else {
		%>
		
		class = ' <%=(sv.chdrsel).getColor()== null  ?
		"input_cell" :  (sv.chdrsel).getColor().equals("red") ?
		"input_cell red reverse" : "input_cell" %>' >
		<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
		
		<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')">
		<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
		</a> --%>
		
		<%} %>
						    		
		
				      			</div>
				    		</div>
			    	</div>
			      
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-6">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Premium Holiday Process")%></b>
					</label>
				</div>
				<div class="col-md-6">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Premium Holiday Enquiry")%></b>
					</label>			
			    </div>		        
			</div>
			
										
		</div>
	</div>	
	
	
	
	
	
	
	
	


<%}%>

<%if (sv.Sr51rprotectWritten.gt(0)) {%>
	<%Sr51rprotect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2NEW.jsp"%>
