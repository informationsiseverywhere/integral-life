<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR570";%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon1NEW.jsp"%><!---Ticket ILIFE-758 ends-->
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr570ScreenVars sv = (Sr570ScreenVars) fw.getVariables();%>
<%
	{
if (appVars.ind02.isOn()) {
	sv.sumin.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
	sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind03.isOn()) {
	sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind04.isOn()) {
	sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind24.isOn()) {
	sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind25.isOn()) {
	sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind06.isOn()) {
	sv.mortcls.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind05.isOn()) {
	sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind37.isOn()) {
	sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind38.isOn()) {
	sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind08.isOn()) {
	sv.liencd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind07.isOn()) {
	sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind31.isOn()) {
	sv.instPrem.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind32.isOn()) {
	sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind22.isOn()) {
	sv.instPrem.setReverse(BaseScreenData.REVERSED);
	sv.instPrem.setColor(BaseScreenData.RED);
}
if (!appVars.ind22.isOn()) {
	sv.instPrem.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind43.isOn()) {
	sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind74.isOn()) {
	sv.taxamt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind34.isOn()) {
	sv.pbind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind34.isOn()) {
	sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind44.isOn()) {
	sv.taxind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind44.isOn()) {
	sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind26.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind27.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind40.isOn()) {
	sv.zdivopt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.payeesel.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.paymth.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.paycurr.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind59.isOn()) {
	sv.linstamt.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind60.isOn()) {
	sv.linstamt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind61.isOn()) {
	sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind59.isOn()) {
	sv.linstamt.setColor(BaseScreenData.RED);
}
if (!appVars.ind59.isOn()) {
	sv.linstamt.setHighLight(BaseScreenData.BOLD);
}
/*BRD-306 starts */
if (appVars.ind62.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind52.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind63.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
/*BRD-306 ends */
//ILIFE-3423:Start
if (appVars.ind64.isOn()) {
	sv.prmbasis.setColor(BaseScreenData.RED);
}else{
	sv.prmbasis.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind65.isOn()) {
	sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind66.isOn()) {
	sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILIFE-3423:End
	//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
	sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
	sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
	sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
	sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
	sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
	//BRD-NBP-011 ends
	
	
	if (appVars.ind73.isOn()) {
	sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
		}
	/* if (appVars.ind73.isOn()) {
		sv.exclind.setEnabled(BaseScreenData.DISABLED);
	} */

	if (appVars.ind91.isOn()) {
		sv.fuind.setEnabled(BaseScreenData.DISABLED);
		sv.fuind.setInvisibility(BaseScreenData.INVISIBLE);
	}

	//ICIL-560 FWANG3
	if (appVars.ind75.isOn()) {
		sv.cashvalarer.setInvisibility(BaseScreenData.INVISIBLE);
	}	
	
		//ILJ-46
		if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		//end
	}
%>
<style>
#instPrem{width:145px}
</style>

<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>
    	 					
    	 					<table>
    	 					<tr>
    	 					<td>
    	 					
    	 					
    	 					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="max-width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
	</td>
	<td>
	<%  	if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:80px;margin-left: 1px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
</td>
<td>
  		
<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
	
<%}%>

</td>
</tr>
</table>
    	 					
    	 					
</div></div>
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>

<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:80px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>


<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
style="max-width:300px;min-width:100px;margin-left: 1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>				
    	 				</td>
    	 				</tr>
    	 				</table>
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					<div class="col-md-1"> </div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
<%}%>
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> ' style="min-width:80px;"> 
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>
	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;min-width:100px;margin-left: 1px;"> 
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 					
    	 </td>
    	 </tr>
    	 </table>
    	 					</div></div>
    	 					
    	 					
    	 					
    	 						<div class="col-md-1"> 
			    	    </div>
			    	     
			    	     
    	 					
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


			    	     
			    	     
			    	     </div></div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     <div class="col-md-2"> 
			    	     <div class="form-group">
    	 					
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
    	 					

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>

<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>

<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:70px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				</div>
    	 					
    	 					
    	 				<br>    	 					
    	 					
    	 				<hr>
    	 				<br>
    	 					
 <%-- IBPLIFE-2136 start  --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  
	<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#sum_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured / Premium")%></label></a>
						</li>
						<li><a href="#cover_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></label></a>
						</li>
					</ul>
				</div>   
		</div>       
		
		<div class="tab-content">
			<div class="tab-pane fade in active" id="sum_tab">
	<%} %>
<%-- IBPLIFE-2136 end --%> 
    	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>

  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:75px" : "width:75px;" %>'> 
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 				
    	 				
    	 				</div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>	

	<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='sumin' 
type='text'

<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%
	 //valueThis=smartHF.getPicFormatted(qpsf,sv.sumin);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%= sv.sumin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'


<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    
    <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>	

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.mortcls).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					<select name='mortcls' type='list' style="width:210px;"
					<% 
				if((new Byte((sv.mortcls).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.mortcls).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.mortcls).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 <div class="col-md-2 "> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>	
    	 					

<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:75px" : "width:75px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
    	 						 				
    	 				
    	 				</div>	
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">	
							<!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
								<% } %>
							<!--  END  -->		    	     
    	 					
<!-- <div class="input-group"> -->
<table>
<tr>
<td>

							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>


						
						
						</td>
						<td>
						



							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						
						</td>
						</tr>
						</table>
					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">	
			    	     	<!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
								<% } %>
							<!--  END  -->		    	     
    	 					
    	 					 
<div class="input-group " style="width:117px;" >
<%	
	longValue = sv.riskCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
						style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:75px;" : "width:75px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='riskCessDateDisp' 
type='text' 
value='<%=sv.riskCessDateDisp.getFormData()%>' 
maxLength='<%=sv.riskCessDateDisp.getLength()%>' 
size='<%=sv.riskCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessDateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 style="width:75px;" 

<%
	}else if((new Byte((sv.riskCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" 	style="width:75px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('riskCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.riskCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.riskCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:140px;">

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('riskCessDateDisp'), '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} longValue = null;}%>
    	 					
    	 					
    	 					</div></div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
    	 					

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.liencd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
					<%
					} 
					%>
					<select name='liencd' type='list' style="width:200px;"
					<% 
				if((new Byte((sv.liencd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.liencd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.liencd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 				
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>

<%	
	if ((new Byte((sv.bappmeth).getInvisible())).compareTo(new Byte(
								BaseScreenData.VISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bappmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bappmeth");
	optionValue = makeDropDownList( mappedItems , sv.bappmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.bappmeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:147px;"> 
<%
} 
%>

<select name='bappmeth' type='list' style="width:145px;"
<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.bappmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.bappmeth).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 				 	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>

						
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>



						
						</td>
						<td>
						


							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
					
						</td>
						</tr>
						</table>
					
    	 				
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				
    	 					
    	 					
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
    	<!--  	<div style="min-width:200px"> -->				
<div class="input-group" style="width:117px;">
<%	
	longValue = sv.premCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
						style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:100px;" : "width:100px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
 
<input name='premCessDateDisp' 
type='text' 
value='<%=sv.premCessDateDisp.getFormData()%>' 
maxLength='<%=sv.premCessDateDisp.getLength()%>' 
size='<%=sv.premCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	style="width:100px;">

<%
	}else if((new Byte((sv.premCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:120px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
 
<%
	}else { 
%>

class = ' <%=(sv.premCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.premCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:100px;" >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'),  '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
 
<%}longValue = null; }%>
    	 				</div></div></div>
    	 				
    	 				
    	 				
    	 				
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%></label>
<%if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='select' 
type='text'

<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.select.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.select.getLength()%>'
maxLength='<%= sv.select.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(select)' onKeyUp='return checkMaxLength(this)'  
	style="width:50px;"

<% 
	if((new Byte((sv.select).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.select).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.select).getColor()== null  ? 
			"input_cell" :  (sv.select).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>


    	 				
    	 				</div></div>
    	 				
    	 				
    	 				<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>

	<%	
	if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("prmbasis");
		optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
		longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
	%>
		<% 
		if((new Byte((sv.prmbasis).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
		%>  
  			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px;" %>'>  
	   			<%if(longValue != null){%>
			   		<%=longValue%>
		   		<%}%>
		   	</div>
			<%
			longValue = null;
			%>

		<%}else {%>
			<%if("red".equals((sv.prmbasis).getColor())){%>
			<div style="border:1px; border-style: solid; border-color: #ec7572;  width:183px;"> 
			<%}%>
			<select name='prmbasis' type='list' style="width:180px;"
			<% 
			if((new Byte((sv.prmbasis).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
			}else if((new Byte((sv.prmbasis).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
				class="bold_cell" 
			<%
			}else { 
			%>
				class = 'input_cell' 
			<%}%>
			>
				<%=optionValue%>
			</select>
			<% if("red".equals((sv.prmbasis).getColor())){%>
			</div>
			<%}%>
		<%}
	}%>
	<% longValue = null;%>
    	 				
    	 				</div></div>	<%} %>	
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">
    	 					<div class="col-md-6"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
 <table>
 <tr>
 <td>
 <%
                                         if ((new Byte((sv.payeesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.payeesel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.payeesel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('payeesel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
 	
 
 


</td>
<td>
<%if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
style="max-width: 265px;margin-left: -1px;min-width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	
    	</td>
    	</tr>
    	</table> 					
    	 					
    	 					</div></div>
 					
 					
 					<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="col-md-3"> </div>
 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
    	 					
<%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.dialdownoption).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:145px;"> 
<%
} 
%>

<select name='dialdownoption' type='list' style="width:145px;"
<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dialdownoption).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dialdownoption).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					</div>
    	 					<%
								} 
							%>
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Dividend Option")%></label>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"zdivopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("zdivopt");
	optionValue = makeDropDownList( mappedItems , sv.zdivopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.zdivopt.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.zdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.zdivopt).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
					<%
					} 
					%>
					<select name='zdivopt' type='list' style="width:200px;"
					<% 
				if((new Byte((sv.zdivopt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.zdivopt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.zdivopt).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
    	 					
    	 			</div></div>
    	 			
    	 			
    	 			
    	 		
    	 			
    	 			
    	 			    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("MOP")%></label>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paymth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paymth");
	optionValue = makeDropDownList( mappedItems , sv.paymth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.paymth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paymth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.paymth).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
					<%
					} 
					%>
					<select name='paymth' type='list' style="width:200px;"
					<% 
				if((new Byte((sv.paymth).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.paymth).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.paymth).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
    	 			</div></div>
    	 			
    	 			
    	 			
    	 			
    	 			
    	 			
    	 	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%></label>
    	 					<div class="input-group" >
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paycurr");
	optionValue = makeDropDownList( mappedItems , sv.paycurr.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.paycurr).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
					<%
					} 
					%>
					<select name='paycurr' type='list' style="width:200px;"
					<% 
				if((new Byte((sv.paycurr).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.paycurr).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.paycurr).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
<%-- <%=smartHF.getDropDownExt(sv.paycurr, fw, longValue, "paycurr", optionValue) %> --%>
			
    	 			</div></div></div>
    	 			
    	 			
    	 			
    	 		
    	 			
    	 			
    	 			
    	 			</div>		
    	 		<br>	
    	 					
    	 					
    	 					
    	 				<hr>	
    	 		<br>	
    	 					
    	 <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>					
    	 <div class="row">
    	 <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
    	 					
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div>  <%} %>
                        
                        <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                       <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>

<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>

<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                        </div> 
                          <%} %>	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					 <div class="row">
    	 					 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 					 <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

    	 					
    	 					</div></div><%} }%>	
    	 					
    	 					
    	 					<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">	
			    	     
			    	     <!-- ILIFE-8323 Start -->
	 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>	    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
<%}
	  if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0){ %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
<%}
if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	   <!-- ILIFE-8323 Ends -->
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width: 145px !important;padding-right:5px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="width: 145px !important;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
    	 					
    	 					</div></div><%} %>
    	 					
    	 					<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 					<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%}} %>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    <div class="row">
    	<div class="col-md-3">
			<div class="form-group">		    	     
    	 	<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
				<div class="input-group" style="width: 145px !important;">
				<%if(((BaseScreenData)sv.instPrem) instanceof StringBase) {%>
				<%=smartHF.getRichText(0,0,fw,sv.instPrem,( COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)+1),null).replace("absolute","relative")%>
				<%}else if (((BaseScreenData)sv.instPrem) instanceof DecimalData){%>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.instPrem, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				<%}else {%>
				hello
				<%}%>
    	 		</div>
    	 	</div>
		</div>
	
    	 					
    	<div class="col-md-3"> 
			<div class="form-group">		    	     
    	 	<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
				<div class="input-group" style="width:145px;">
				<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
				<%	
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
				
				
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
				
				<input name='taxamt' 	
				type='text'
				<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width: 145px !important; "<% }%>
				
					value='<%=valueThis %>'
							 <%
				
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis %>'
					 <%}%>
				
				size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)%>'
				maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)-3%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.taxamt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.taxamt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.taxamt).getColor()== null  ? 
							"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				
				<%
					} 
				%>
				>
				
					
				<%}%>
    	 					
    	 	</div>
    	 					
		</div>
	</div>
    	 					
    	 					
    <div class="col-md-3"> 
		<div class="form-group">		    	     
    	 <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%></label>

					<%	
							qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
							
					%>
				
				<input name='linstamt' 
				type='text'
				<%if((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px !important;"<% }%>
				
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)%>'
				maxLength='<%= sv.linstamt.getLength()%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.linstamt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.linstamt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.linstamt).getColor()== null  ? 
							"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
	
		</div>
	</div>
    	 					
	<%
		if ((new Byte((sv.cashvalarer).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	%>
    <div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Value in Arrears")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.cashvalarer).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.cashvalarer,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.cashvalarer.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell"
						style="width: 175px !important; text-align: right;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 175px !important;">
						&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
							formatValue = null;
					%>

				</div>
			</div> 					
			<%
				}
			%>
    	 					
	</div>
</div>
<%-- IBPLIFE-2136 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  

<div class="tab-pane fade" id="cover_tab">
	<div class="row">
				
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
			<table><tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcode, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcdedesc, true)%></td>
			</tr></table>
			</div>
		</div>
		
		<div class="col-md-1"></div>
		<div class="col-md-2">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					
					<%--
	
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dateeffDisp" data-link-format="dd/mm/yyyy"  onclick="document.getElementById('riskCessDateDisp').value='';">
			                    <%=smartHF.getRichTextDateInput(fw, sv.dateeffDisp,(sv.dateeffDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
						--%>
						<div class="form-group">
								<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.dateeffDisp.getFormData()%>' maxLength='<%=sv.dateeffDisp.getLength()%>' 
										size='<%=sv.dateeffDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>
								</div>
						</div>   			
		</div>
		
	</div>
	
	<div class="row">
				
				
		<div class="col-md-8">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose ")%></label>		
			<div class="form-group">

<table><tr>
<td>

<textarea name='covrprpse' style='width:400px;height:70px;resize:none;border: 2px solid !important;'
type='text' 

<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.covrprpse.getFormData()).toString();

%>
 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>

size='<%= sv.covrprpse.getLength() %>'
maxLength='<%= sv.covrprpse.getLength() %>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.covrprpse).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.covrprpse).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.covrprpse).getColor()== null  ? 
			"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%=XSSFilter.escapeHtml(formatValue)%></textarea>

</td></tr>

</table>

</div>
</div>
</div>
</div>
<%} %>
<%-- IBPLIFE-2136 end --%> 		

</div>
</div>
</div>

<BODY >
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<input name='bankaccreq' id='bankaccreq' type='hidden'  value="<%=sv.bankaccreq.getFormData()%>">
									<!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bankaccreq"))' class="hyperLink"> 
										<%=resourceBundleHandler
					.gettingValueFromBundle("Bank Account")%>
									<!-- icon -->
									<%
										if (sv.bankaccreq.getFormData().equals("+")) {
									%>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.bankaccreq.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind.getFormData()%>">
									<!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
									<!-- icon -->
									<%
									 	if (sv.optextind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.optextind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
									<!-- text -->
									<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.taxind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.taxind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='comind' id='comind' type='hidden'  value="<%=sv.comind
.getFormData()%>"><!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%>
									
									<!-- icon -->
									<%
									 	if (sv.comind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.comind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='pbind' id='pbind' type='hidden'  value="<%=sv.pbind
.getFormData()%>">
<%if (sv.pbind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.pbind.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>
								<%} else if (sv.pbind.getEnabled() == BaseScreenData.DISABLED) {%>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></a>
	<%} %>
									<!-- icon -->
									<%
									 	if (sv.pbind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.pbind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<%if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.exclind.getEnabled() != BaseScreenData.DISABLED){%>
<%	if(sv.exclind
.getInvisible()!= BaseScreenData.INVISIBLE){
%> 					<li>
									<input name='exclind' id='exclind' type='hidden'  value="<%=sv.exclind.getFormData()%>">

<%-- <%if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.exclind.getEnabled() != BaseScreenData.DISABLED){%>
					        <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
									<%} %> --%>
									<!-- icon -->
									<%
if (sv.exclind
.getFormData().equals("+")) {
%>

<i class="fa fa-tasks fa-fw sidebar-icon"></i>
<%}else {

	
    if (sv.exclind.getFormData().equals("X") || sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
	        || sv.exclind.getEnabled() == BaseScreenData.DISABLED) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}



}
if (sv.exclind
.getFormData().equals("X")) {
%>
	
   <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> 
								<%-- <%if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.exclind.getEnabled() != BaseScreenData.DISABLED){%> --%>
					        <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
									<%-- <%} %> --%>
									</a></li>
								
								<% }} %>
								<li>
									<input name='fuind' id='fuind' type='hidden'  value="<%=sv.fuind.getFormData()%>">
									<!-- text -->
									<%if (sv.fuind.getInvisible() != BaseScreenData.INVISIBLE
											|| sv.fuind.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fuind"))' class="hyperLink">
											<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
									<%} %>
											<!-- icon -->
									<%
										if (sv.fuind.getFormData().equals("+")) {
									%>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
											}
											if (sv.fuind.getFormData().equals("X")) {
										%> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
											}
										%> </a>
								</li>
							</ul>
					</span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>









<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->


<!-- ILIFE-2734 Life Cross Browser - Sprint 4 D2 : Task 1  -->
<script>
$(document).ready(function(){
	createDropdownNotInTable("paycurr",7);
});
	</script>
<!-- ILIFE-2734 Life Cross Browser - Sprint 4 D2 : Task 1  -->

