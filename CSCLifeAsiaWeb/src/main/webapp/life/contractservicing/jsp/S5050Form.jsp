

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5050";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5050ScreenVars sv = (S5050ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Source ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prm Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Prem ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Overdue ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing  ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Notices ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission  ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to Date  ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonuses ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Details ---------------------------------------------------- ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suppress O/Due ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason ");%>

<%{
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cnttype.setReverse(BaseScreenData.REVERSED);
			sv.cnttype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cnttype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ctypdesc.setReverse(BaseScreenData.REVERSED);
			sv.ctypdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ctypdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.rstate.setReverse(BaseScreenData.REVERSED);
			sv.rstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.rstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pstate.setReverse(BaseScreenData.REVERSED);
			sv.pstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cownnum.setReverse(BaseScreenData.REVERSED);
			sv.cownnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cownnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ownername.setReverse(BaseScreenData.REVERSED);
			sv.ownername.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ownername.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.currfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.currfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.currfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.currtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.currtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.currtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>
<style>
#owner{
	max-width: 250px !important;width: 246px !important;
}
</style>
<div class="panel panel-default">
       <div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
				
				<table>
					<tr>
					<td>
						<%	if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
					</td>
					
					<td>
						<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="width:50px;margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
					</td>
					<td>
						<%					
								if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left:1px;" id="ctypdesc">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
					</td>
					</tr>
				</table>
			
			</div>



			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
					</td><td >
						<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div 
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="max-width:215px; margin-left:1px;">
							
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>

					</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->
					<div class="input-group">
					<%					
							if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;margin-left:1px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
							longValue = null;
							formatValue = null;
							%>

				</div>
			</div>
		</div></div>


		<div class="row"> 
	                   <div class="col-md-4">	
	                     <div class="form-group">                      
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prm Status")%></label>
	                       <%					
							if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                       </div>
	                  	</div>
	                  <div class="col-md-4">	 
	                     <div class="form-group">                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
	                      	<%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </div>
	                  </div>
	                 <div class="col-md-4">	     
	                     <div class="form-group">                
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
	                      <div class="input-group">
	                      <%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								optionValue = makeDropDownList( mappedItems , sv.register,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
								
							if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.register.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.register.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
						</div>
	                      </div>
	                  </div>
	             </div>
	             
	              <div class="row"> 
	                  <div class="col-md-4">
	                     <div class="form-group">                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Prem")%></label>
	                      <div class="input-group">
	                      <%	
							qpsf = fw.getFieldXMLDef((sv.instpramt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.instpramt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.instpramt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
	
 
				</div>
	                     </div>
	                  </div>
	                  
	                   <div class="col-md-4">	  
	                     <div class="form-group">                    
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
	                      <%					
							if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
	                      
	                      </div>
	                  </div>
	                  
	                   <div class="col-md-4">	
	                      <div class="form-group">                      
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>
	                      	<%					
							if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	
	                       </div>
	                  </div>
	             </div>
	           
	            <div class="row"> 
	                  <div class="col-md-4">	
	                    <div class="form-group">                      
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                      <%					
						if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
	                      </div>
	                  </div>
	                  
	                  <div class="col-md-4">	 
	                       <div class="form-group">                 
	                     		 <label><%=resourceBundleHandler.gettingValueFromBundle("Payment")%></label>
	                      			<%		
										fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
										mappedItems = (Map) fieldItem.get("mop");
										optionValue = makeDropDownList( mappedItems , sv.mop,2,resourceBundleHandler);  
										longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
													
									if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mop.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mop.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 160px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
	                       </div>
	                  </div>
	                  <div class="col-md-4">
	                      <div class="form-group"> 	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label> 
	                      <%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("billfreq");
									optionValue = makeDropDownList( mappedItems , sv.billfreq,2,resourceBundleHandler);  
									longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
													
								if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
	                      </div>
	                  </div>    
	             </div>
	             
	              <div class="row"> 
	                    <div class="col-md-4">	  
	                      <div class="form-group">                    
	                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Source")%></label>
	                      			<div class="input-group">
	                      			<%		
									fieldItem=appVars.loadF4FieldsLong(new String[] {"srcebus"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("srcebus");
									optionValue = makeDropDownList( mappedItems , sv.srcebus,2,resourceBundleHandler);  
									longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());
											
								if(!((sv.srcebus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.srcebus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.srcebus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  							</div>
	                      </div>
	                  </div>
	                  
	                  <div class="col-md-4">	  
	                      <div class="form-group">                   
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>   
	                      	<div class="input-group">
							<%		
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reptype"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reptype");
								optionValue = makeDropDownList( mappedItems , sv.reptype,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.reptype.getFormData()).toString().trim());
											
							if(!((sv.reptype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.reptype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.reptype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 160px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</div>
	                      </div>
	                  </div>
	             </div>
	             
	            <div class="row">
	                  <div class="col-md-3">
	                    <div class="form-group">   	                     
	                      <label></label>
	                    </div>
	                   </div>
	                   <div class="col-md-1">	
	                   	 <div class="form-group">                       
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Indicator")%></label>	                                      
	                     </div>
	                   </div>
	                   <div class="col-md-2">	
	                     <div class="form-group">                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("From")%></label>	                      	
	                    </div>
	                   </div>
	                   <div class="col-md-2">
	                    <div class="form-group">  	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>
	                    </div>
	                   </div>
	         	</div>
	         	<div class="row">
                      <div class="col-md-3">
                          <div class="form-group">	                     
	                           <label><%=resourceBundleHandler.gettingValueFromBundle("Billing")%></label>
	                      </div>
	                    </div>
	                  <div class="col-md-1">	
	                   	 <div class="form-group">
	                   	 	<input type='checkbox' readonly='readonly' disabled="disabled" name='billind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(billind)' onKeyUp='return checkMaxLength(this)'    
							<%
							if(!(sv.billind).toString().trim().equalsIgnoreCase("X")){
										 %>
							<%}
							if((sv.billind).getColor()!=null){
										 %>style='background-color:#FF0000;'
									<%}
									if((sv.billind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }if((sv.billind).getEnabled() == BaseScreenData.DISABLED){%>
							    	   disabled = 'disabled'
									
									<%}%>
							class ='UICheck' onclick="handleCheckBox('billind')"/>
							
							<input type='checkbox' readonly="readonly"  name='billind' value=' ' 
							
							<% if(!(sv.billind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }%>
							
							style="visibility: hidden; " onclick="handleCheckBox('billind')"/>
					                   	 
	                   	  </div>	
	                </div>
	                <div class="col-md-2">
	                     <div class="form-group">
	                    <%					
		if(!((sv.billfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  
	                      </div>	
	                </div>
	                
	                <div class="col-md-2">
	                     <div class="form-group">
	                     <%					
		if(!((sv.billtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	                      </div>	
	                </div>
	       </div>
	       
	       
	       <div class="row">
                     <div class="col-md-3">
                       <div class="form-group">	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Notices")%></label>
	                   </div>
	                   </div>
	                 <div class="col-md-1">
	                   <div class="form-group">	 
	                   <input type='checkbox' readonly='readonly' disabled="disabled" name='notind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(notind)' onKeyUp='return checkMaxLength(this)'    
							<%
							if(!(sv.notind).toString().trim().equalsIgnoreCase("X")){
										 %>
							<%}
							if((sv.notind).getColor()!=null){
										 %>style='background-color:#FF0000;'
									<%}
									if((sv.notind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }if((sv.notind).getEnabled() == BaseScreenData.DISABLED){%>
							    	   disabled = 'disabled'
									
									<%}%>
							class ='UICheck' onclick="handleCheckBox('notind')"/>
							
							<input type='checkbox' readonly="readonly"  name='notind' value=' ' 
							
							<% if(!(sv.notind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }%>
							
							style="visibility: hidden; " onclick="handleCheckBox('notind')"/>

	                   
	                   </div>	
	                </div>
	                 <div class="col-md-2">
	                   <div class="form-group">	 
	                   <%					
						if(!((sv.notsfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.notsfromDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.notsfromDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
	                   </div>	
	                </div>
	                <div class="col-md-2">
	                   <div class="form-group">	 
	                   <%					
							if(!((sv.notstoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.notstoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.notstoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                   </div>	
	                </div>
	       </div>
	       
	       
	       <div class="row">
                      <div class="col-md-3">
                       <div class="form-group">	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Renewal")%></label>
	                   </div>
	                   </div>
	                   <div class="col-md-1">
	                   	 <div class="form-group">
	                   	 <input type='checkbox' readonly='readonly' disabled="disabled" name='renind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(renind)' onKeyUp='return checkMaxLength(this)'    
			<%
			if(!(sv.renind).toString().trim().equalsIgnoreCase("X")){
						 %>
			<%}
			if((sv.renind).getColor()!=null){
						 %>style='background-color:#FF0000;'
					<%}
					if((sv.renind).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.renind).getEnabled() == BaseScreenData.DISABLED){%>
			    	   disabled = 'disabled'
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('renind')"/>
			
			<input type='checkbox' readonly="readonly"  name='renind' value=' ' 
			
			<% if(!(sv.renind).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden; " onclick="handleCheckBox('renind')"/>

  
	
	                   	  </div>	
	                </div>
	                
	                 <div class="col-md-2">
	                   	 <div class="form-group">
	                   	 <%					
		if(!((sv.rnwlfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rnwlfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rnwlfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                   	  </div>	
	                </div>
	                
	                 <div class="col-md-2">
	                   	 <div class="form-group">
	                   	 <%					
		if(!((sv.rnwltoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rnwltoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rnwltoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	                   	  </div>	
	                </div>
	       </div>	
	       
	       <div class="row">
                     <div class="col-md-3">	
                      	<div class="form-group">                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Commission")%></label>
	                   	</div>
	                  </div>
	                <div class="col-md-1">
	                    <div class="form-group"> 
	                    	<input type='checkbox' readonly='readonly' disabled="disabled" name='comind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(comind)' onKeyUp='return checkMaxLength(this)'    
							<%
							if(!(sv.comind).toString().trim().equalsIgnoreCase("X")){
										 %>
							<%}
							if((sv.comind).getColor()!=null){
										 %>style='background-color:#FF0000;'
									<%}
									if((sv.comind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }if((sv.comind).getEnabled() == BaseScreenData.DISABLED){%>
							    	   disabled = 'disabled'
									
									<%}%>
							class ='UICheck' onclick="handleCheckBox('comind')"/>
							
							<input type='checkbox' readonly="readonly"  name='comind' value=' ' 
							
							<% if(!(sv.comind).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }%>
							
							style="visibility: hidden; " onclick="handleCheckBox('comind')"/>
	                     </div>	
	                </div>
	                <div class="col-md-2">
	                    <div class="form-group"> 
	                    
								<%					
								if(!((sv.commfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.commfromDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.commfromDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
	                     </div>	
	                </div>
	                 <div class="col-md-2">
	                    <div class="form-group"> 
	                    <%					
							if(!((sv.commtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.commtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.commtoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                     </div>	
	                </div>
	       </div>	
	       
	       <div class="row">
                     <div class="col-md-3">
	                      <div class="form-group">	                     
		                      <label><%=resourceBundleHandler.gettingValueFromBundle("Bonuses")%></label>
		                    </div>
	                  </div>
	                <div class="col-md-1">
		                   <div class="form-group">	
		                   <input type='checkbox' readonly='readonly' disabled="disabled" name='bnsind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(bnsind)' onKeyUp='return checkMaxLength(this)'    
								<%
								if(!(sv.bnsind).toString().trim().equalsIgnoreCase("X")){
											 %>
								<%}
								if((sv.bnsind).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.bnsind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.bnsind).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled = 'disabled'
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('bnsind')"/>
								
								<input type='checkbox' readonly="readonly"  name='bnsind' value=' ' 
								
								<% if(!(sv.bnsind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden; " onclick="handleCheckBox('bnsind')"/>
		                   
		                   </div>	
	                </div>
	                
	                <div class="col-md-2">
	                   <div class="form-group">
	                   <%					
							if(!((sv.bnsfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnsfromDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnsfromDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  	
	                   </div>	
	                </div>
	                <div class="col-md-2">
	                   <div class="form-group">	
	                   <%					
							if(!((sv.bnstoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnstoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnstoDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
	                   </div>	
	                </div>
	       </div>	
	       <br />
	       <div class="row">
                      <div class="col-md-4">	
                      <div class="form-group">	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Details")%></label>
	                   </div>    
	                </div>
	               </div>  
	             <div class="row">
                     <div class="col-md-4">	
	                      <div class="form-group">	                     
		                      <label><%=resourceBundleHandler.gettingValueFromBundle("Suppress O/Due")%></label>
		                      </br>
		                      <input type='checkbox' readonly='readonly' disabled="disabled" name='billind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(billind)' onKeyUp='return checkMaxLength(this)'    
								<%
								if(!(sv.billind).toString().trim().equalsIgnoreCase("X")){
											 %>
								<%}
								if((sv.billind).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.billind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.billind).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled = 'disabled'
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('billind')"/>
								
								<input type='checkbox' readonly="readonly"  name='billind' value=' ' 
								
								<% if(!(sv.billind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden; " onclick="handleCheckBox('billind')"/>
		                   </div>    
	                </div>
	                
	               
	                <div class="col-md-2">
	               	        <div class="form-group">      
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("From")%></label>	                         	                    
		                <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.currfromDisp, (sv.currfromDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					</div>
					</div>
	                  <div class="col-md-2">	
	                  	<div class="form-group">                      
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>	                         	                    
			                <div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.currtoDisp, (sv.currtoDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
							</div>
					</div>  
					</div>   
	              </div> 
	             <div class="row">
                      <div class="col-md-12" >	
                      <div class="form-group">                       
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>
	                      <div class="input-group">
	                   <!--  </div>   
	                 </div> 
	                  </div>	                 
	                 <div class="row">
	                     <div class="col-md-2">
	                     <div class="form-group"> -->
	                     <table><tr><td>                     
	                      <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
					<%
					} 
					%>
					
					<select class='sel' name='reasoncd' type='list' style="width:70px;"
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.reasoncd).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					onchange="change()"
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					} 
					%>
				<!-- </div>
				</div>
				
							 <div class="col-md-1">
							  <div class="form-group">  -->
							  </td><td> 
							
							<input style="width:200px; margin-left:1px;" "id='resn' name='resndesc' 
							type='text'
							
							<%
							
									fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("reasoncd");
									optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
									formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
									
							%>
							<%if(formatValue==null) {
								formatValue="";
							}
							 %>
							 
							 <% String str=(sv.resndesc.getFormData()).toString().trim(); %>
										<% if(str.equals("") || str==null) {
											str=formatValue;
										}
										
									%>
							 
								value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
							
							size='50'
							maxLength='50' 
							 
							
							
							<% 
								if((new Byte((sv.resndesc).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.resndesc).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.resndesc).getColor()== null  ? 
										"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
	                      <!-- </div>
	                   </div> -->
	                   </td>
	                   </tr>
	                   </table>
	                   </div>
	                   </div>
	                   </div>
	                       
	               </div>
	             </div> 
	                    	 
  </div>	  
<script>
	$(document).ready(function() {
	
		if (screen.height == 900) {
			
			$('#ctypdesc').css('max-width','215px');
		}
		if (screen.width == 1440) {
			
			$('#ctypdesc').css('max-width','200px');
		}
		
	});
</script> 

<%@ include file="/POLACommon2NEW.jsp"%>



