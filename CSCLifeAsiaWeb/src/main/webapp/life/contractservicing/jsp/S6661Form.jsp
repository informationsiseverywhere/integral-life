<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6661";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S6661ScreenVars sv = (S6661ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Subroutines ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Used by Contract Reversal ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Forward Transaction Code  ");%>

<%{
		if (appVars.ind13.isOn()) {
			sv.subprog01.setReverse(BaseScreenData.REVERSED);
			sv.subprog01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.subprog01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.subprog02.setReverse(BaseScreenData.REVERSED);
			sv.subprog02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.subprog02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.contRevFlag.setReverse(BaseScreenData.REVERSED);
			sv.contRevFlag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.contRevFlag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.trcode.setReverse(BaseScreenData.REVERSED);
			sv.trcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.trcode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		
					    		<div class="input-group" style="max-width:100px;">  
					    			
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
				    		</div>
					</div></div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							
							<div class="input-group" style="max-width:100px;">  
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

	
						</div>
				   </div>		</div>
			
			    	<div class="col-md-4">
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						  
							 <div class="input-group"  style="max-width:100px;">
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:700px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						</div>
						</div>
				   </div>	
		    </div>
				   
				 
				   <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Processing Subroutines")%></label> 
				    	      	<div class="input-group"> 
				    	      	
<input name='subprog01' 
type='text'

<%

		formatValue = (sv.subprog01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subprog01.getLength()%>'
maxLength='<%= sv.subprog01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subprog01)' onKeyUp='return checkMaxLength(this)'  


<% 
		if ((new Byte((sv.subprog01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				|| fw.getVariables().isScreenProtected()) {
		
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.subprog01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.subprog01).getColor()== null  ? 
			"input_cell" :  (sv.subprog01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
				      		 </div></div></div>
				      </div>
				      
				       <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    	      	<div class="input-group"> 
				    	      	
<input name='subprog02' 
type='text'

<%

		formatValue = (sv.subprog02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subprog02.getLength()%>'
maxLength='<%= sv.subprog02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subprog02)' onKeyUp='return checkMaxLength(this)'  


<% 
		if ((new Byte((sv.subprog02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				|| fw.getVariables().isScreenProtected()) {
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.subprog02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.subprog02).getColor()== null  ? 
			"input_cell" :  (sv.subprog02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
				      		 </div></div></div>
				      </div>
				      
				       <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Used by Contract Reversal")%></label> 
				    	      	<div class="input-group"> 
				    	      	
<input name='contRevFlag' 
type='text'

<%

		formatValue = (sv.contRevFlag.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contRevFlag.getLength()%>'
maxLength='<%= sv.contRevFlag.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contRevFlag)' onKeyUp='return checkMaxLength(this)'  
<%
	if ((new Byte((sv.contRevFlag).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				|| fw.getVariables().isScreenProtected()) {
%>
  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contRevFlag).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contRevFlag).getColor()== null  ? 
			"input_cell" :  (sv.contRevFlag).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
				      		 </div></div></div>
				      		 <div class="col-md-4"></div>
				      		 <div class="col-md-4"></div>
				      </div>
				      
				       <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Forward Transaction Code")%></label> 
				    	      	<div class="input-group" style="max-width:150px"> 
				    	      	
<input name='trcode' id='trcode'
type='text' 
value='<%=sv.trcode.getFormData()%>' 
maxLength='<%=sv.trcode.getLength()%>' 
size='<%=sv.trcode.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(trcode)' onKeyUp='return checkMaxLength(this)'  
	<%
	if ((new Byte((sv.trcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	|| fw.getVariables().isScreenProtected()) {
%>
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.trcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
	<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('trcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.trcode).getColor()== null  ? 
"input_cell" :  (sv.trcode).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('trcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%} %>

				    	      	
				      		 </div></div><div class="col-md-4"></div><div class="col-md-4"></div></div>
				      </div>
			        
				   </div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

