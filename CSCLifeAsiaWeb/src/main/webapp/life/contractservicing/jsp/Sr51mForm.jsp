
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR51M";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr51mScreenVars sv = (Sr51mScreenVars) fw.getVariables();%>
<%if (sv.Sr51mscreenctlWritten.gt(0)) {%>
	<%Sr51mscreenctl.clearClassString(sv);%>
	<%sv.acdes.setClassString("");%>
<%	sv.acdes.appendClassString("string_fld");
	sv.acdes.appendClassString("output_txt");
	sv.acdes.appendClassString("underline");
	sv.acdes.appendClassString("highlight");
%>
	<%StringData generatedText1 = new StringData("Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText2 = new StringData("Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText3 = new StringData("Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = new StringData("Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText4 = new StringData("Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText6 = new StringData("Contract Owner ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.ownername.setClassString("");%>
<%	sv.ownername.appendClassString("string_fld");
	sv.ownername.appendClassString("output_txt");
	sv.ownername.appendClassString("highlight");
%>
	<%StringData generatedText7 = new StringData("Life Assured ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = new StringData("Inception Date ");%>
	<%sv.occdateDisp.setClassString("");%>
<%	sv.occdateDisp.appendClassString("string_fld");
	sv.occdateDisp.appendClassString("output_txt");
	sv.occdateDisp.appendClassString("highlight");
					%>			
	<%StringData generatedText15 = new StringData("Effective Date ");%>
	<%sv.effdateDisp.setClassString("");%>
<%	sv.effdateDisp.appendClassString("string_fld");
	sv.effdateDisp.appendClassString("output_txt");
	sv.effdateDisp.appendClassString("highlight");
		%>
	<%StringData generatedText9 = new StringData("Frequency ");%>
	<%sv.billfreq.setClassString("");%>
<%	sv.billfreq.appendClassString("string_fld");
	sv.billfreq.appendClassString("output_txt");
	sv.billfreq.appendClassString("highlight");
					%>			
	<%StringData generatedText10 = new StringData("Paid To Date ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
		%>
	<%StringData generatedText11 = new StringData("Billed To Date ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
					%>			
	<%StringData generatedText16 = new StringData("Method ");%>
	<%sv.mop.setClassString("");%>
<%	sv.mop.appendClassString("string_fld");
	sv.mop.appendClassString("output_txt");
	sv.mop.appendClassString("highlight");
		%>
	<%StringData generatedText12 = new StringData("Suspense ");%>
	<%sv.susamt.setClassString("");%>
<%	sv.susamt.appendClassString("num_fld");
	sv.susamt.appendClassString("output_txt");
	sv.susamt.appendClassString("highlight");
					%>			
	<%StringData generatedText17 = new StringData("O/S Premium ");%>
	<%sv.osbal.setClassString("");%>
<%	sv.osbal.appendClassString("num_fld");
	sv.osbal.appendClassString("output_txt");
	sv.osbal.appendClassString("highlight");
		%>
	<%StringData generatedText18 = new StringData("Back Pay ");%>
	<%sv.apind.setClassString("");%>
	<%StringData generatedText19 = new StringData("Toleranc ");%>
	<%sv.tolerance.setClassString("");%>
<%	sv.tolerance.appendClassString("num_fld");
	sv.tolerance.appendClassString("output_txt");
	sv.tolerance.appendClassString("highlight");
	%>
	<%					
{
		appVars.rollup(new int[] {93});
		if (appVars.ind70.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
							}
		if (appVars.ind48.isOn()) {
			sv.apind.setReverse(BaseScreenData.REVERSED);
							}
		if (appVars.ind68.isOn()) {
			sv.apind.setEnabled(BaseScreenData.DISABLED);
					}
		if (appVars.ind48.isOn()) {
			sv.apind.setColor(BaseScreenData.RED);
							}
		if (!appVars.ind48.isOn()) {
			sv.apind.setHighLight(BaseScreenData.BOLD);
							}
		if (appVars.ind71.isOn()) {
			sv.tolerance.setInvisibility(BaseScreenData.INVISIBLE);
							}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
}
					
		%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		<%}%>
					    		    <table><tr><td>
						    		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  </td><td>
  
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  </td><td>
  
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width:300px;margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td></tr></table>
				      			    
				    		</div>
					</div>
				    		
				    			<div class="col-md-4"> 
				    		<div class="form-group">  </div></div>
				    		
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">
						<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>	

				      			     </div>
						</div>
				   </div>		
			
			    
		    </div>
		    
		   
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					    		<%}%>
					    		     <div class="input-group">
						    		<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

				      			     </div>
				    		</div>
					</div>
					
				    <div class="col-md-4">
						<div class="form-group">
						<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							<%}%>
							<div class="input-group">
						    <%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
<%}%>		

				      			     </div>
						</div>
				   </div>
				   
				   <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%}%>
							<div class="input-group">
						    <%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>		

				      			     </div>
						</div>
				   </div></div>
				   	
				  <div class="row">
				      <div class="col-md-4" >
						<div class="form-group">	
						<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
							<%}%>
							<table><tr><td>
						    		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="min-width:120px;max-width:200px;margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>

						</div>
				   </div>	</div>
				   		
				    <div class="row">  <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
							<%}%>
							<table><tr><td>
						    	<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="min-width:120px;max-width:200px;margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>	

				      	</td></tr></table>		    
						</div>
				   </div>					
			
			    
		    </div>
		
				   
								   <div class="row">	
				   <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Inception Date")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>	

				      			     </div>
						</div>
				   </div>	
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
<%}%>	

				      			     </div>
						</div>
				   </div>			
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>	

				      			     </div>
						</div>
				   </div>					
			
			    
		    </div>
		 	
		    				   <div class="row">	
				   <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>	

				      			     </div>
						</div>
				   </div>	
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
<%}%>	

				      			     </div>
						</div>
				   </div>			
				    <div class="col-md-4">
						<div class="form-group">
						<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Method")%></label>
							<%}%>
							<div class="input-group">
						    	<%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>	

				      			     </div>
						</div>
				   </div>					
			
			    
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense")%></label>
					    		<%}%>
					    		     <div class="input-group">
						    		
<%
			qpsf = fw.getFieldXMLDef((sv.susamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.susamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
		%>
		
				<div class="output_cell">
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("O/S Premium")%></label>
							<%}%>
							<div class="input-group">
						    	<%	
			qpsf = fw.getFieldXMLDef((sv.osbal).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.osbal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.osbal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				
						<div class="output_cell">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Back Pay")%></label>
							<div class="input-group">
						    	<input name='apind' 
type='text'
<%

		formatValue = (sv.apind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.apind.getLength()%>'
maxLength='<%= sv.apind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(exclAgmt)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.apind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.apind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.apind).getColor()== null  ? 
			"input_cell" :  (sv.apind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Toleranc")%></label>
					    		   <%}%>
					    		     <div class="input-group">
						    		<%	
			qpsf = fw.getFieldXMLDef((sv.tolerance).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tolerance,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.tolerance.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				
						<div class="output_cell">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				   
		    </div>
		    <%} %>
		

		    
				<br>
				
				<%if (sv.Sr51mscreensflWritten.gt(0)) {
%>
	<%GeneralTable sfl = fw.getTable("sr51mscreensfl");
	savedInds = appVars.saveAllInds();
	Sr51mscreensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 3;
	smartHF.setSflLineOffset(12);
	%>
<%-- <% 	
	int[] tblColumnWidth = new int[9];
int totalTblWidth = 0;
int calculatedValue =0;
int arraySize=0;
calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Select").length())*12;								
totalTblWidth += calculatedValue;
tblColumnWidth[0]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
		} else {		
			calculatedValue = (sv.select.getFormData()).length()*12;								
			}					
		totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.life.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
		} else {		
			calculatedValue = (sv.life.getFormData()).length()*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.coverage.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
		} else {		
			calculatedValue = (sv.coverage.getFormData()).length()*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.rider.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
		} else {		
			calculatedValue = (sv.rider.getFormData()).length()*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= ((sv.crtable.getFormData()).length() + (sv.crtabled.getFormData()).length()) ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
		} else {		
			calculatedValue = ((sv.crtable.getFormData()).length() + (sv.crtabled.getFormData()).length())*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= ((sv.statcde.getFormData()).length() + (sv.pstatcode.getFormData()).length()) ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
		} else {		
			calculatedValue = ((sv.statcde.getFormData()).length() + (sv.pstatcode.getFormData()).length())*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.sumins.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
		} else {		
			calculatedValue = (sv.sumins.getFormData()).length()*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[7]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.instprem.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
		} else {		
			calculatedValue = (sv.instprem.getFormData()).length()*12;								
		}
		totalTblWidth += calculatedValue;
		tblColumnWidth[8]= calculatedValue;
if (appVars.isPagedownEnabled()) {
}
totalTblWidth -= tblColumnWidth[0];
if(totalTblWidth>730){
totalTblWidth=730;
}
arraySize=tblColumnWidth.length;

%> --%>
<%
		GeneralTable sfl1 = fw.getTable("sr51mscreensfl");
		Sr51mscreensfl.set1stScreenRow(sfl, appVars, sv);
		int height;
		if(sfl.count()*3 > 210) {
		height = 90 ;
		} else if(sfl.count()*3 > 118) {
		height = 90;
		} else {
		height = 90;
		}	
%>

	
<%-- <script>
        $(document).ready(function(){
	
			new superTable("sr51mTable", {
				fixedCols : 0,					
				colWidths : [60,60,80,80,220,80,100,100],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script> --%>
    


<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-sr51m'>
							<thead>
								<tr class='info'>
			       <th><center><%=resourceBundleHandler.gettingValueFromBundle("Req?")%></center></th>
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Life")%></center></th>
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></center></th>
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></center></th>
	<th style="min-width:300px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>							
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></center></th>
	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></center></th>
		 	        </tr>
			 </thead>
			 
			
			 <tbody>
	
<%
Sr51mscreensfl
.set1stScreenRow(sfl, appVars, sv);
int count = 1;
    boolean hyperLinkFlag;
while (Sr51mscreensfl
.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>


<%sv.select.setClassString("");%>
<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%sv.crtable.setClassString("");%>
<%	sv.crtable.appendClassString("string_fld");
	sv.crtable.appendClassString("output_txt");
	sv.crtable.appendClassString("highlight");
%>
	<%sv.crtabled.setClassString("");%>
<%	sv.crtabled.appendClassString("string_fld");
	sv.crtabled.appendClassString("output_txt");
	sv.crtabled.appendClassString("highlight");
%>
	<%sv.statcde.setClassString("");%>
<%	sv.statcde.appendClassString("string_fld");
	sv.statcde.appendClassString("output_txt");
	sv.statcde.appendClassString("highlight");
%>
	<%sv.pstatcode.setClassString("");%>
<%	sv.pstatcode.appendClassString("string_fld");
	sv.pstatcode.appendClassString("output_txt");
	sv.pstatcode.appendClassString("highlight");
%>
	<%sv.sumins.setClassString("");%>
<%	sv.sumins.appendClassString("num_fld");
	sv.sumins.appendClassString("output_txt");
	sv.sumins.appendClassString("highlight");
%>
	<%sv.instprem.setClassString("");%>
<%	sv.instprem.appendClassString("num_fld");
	sv.instprem.appendClassString("output_txt");
	sv.instprem.appendClassString("highlight");
%>
	<%sv.zrwvflg01.setClassString("");%>
	<%sv.zrwvflg02.setClassString("");%>
	<%sv.zrwvflg03.setClassString("");%>
	<%sv.zrwvflg04.setClassString("");%>
	<%sv.waiverprem.setClassString("");%>
	<%sv.ind.setClassString("");%>
	<%sv.wvfind.setClassString("");%>
	<%sv.genarea.setClassString("");%>
	<%sv.datakey.setClassString("");%>
	<%sv.zbinstprem.setClassString("");%>
	<%sv.zlinstprem.setClassString("");%>
	<%sv.rerateDateDisp.setClassString("");%>
	<%sv.singp.setClassString("");%>
	<%sv.origSum.setClassString("");%>
	<%sv.workAreaData.setClassString("");%>
	<%sv.rtrnwfreq.setClassString("");%>
	<%sv.screenIndicArea.setClassString("");%>
	<%sv.benpln.setClassString("");%>
	<%sv.zrwvflg05.setClassString("");%>
	<%sv.livesno.setClassString("");%>
	<%sv.waivercode.setClassString("");%>
	<%sv.chgflag.setClassString("");%>
	<%sv.zunit.setClassString("");%>
	<%sv.bftpaym.setClassString("");%>
	<%sv.premind.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind51.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	<tr>
			<td>
				<%-- <td style="width:<%=tblColumnWidth[1 ]%>px;"> --%>
																			
	
			<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
				<input type='checkbox' 
				onclick="handleCheckBox('sr51mscreensfl.select_R<%=count%>')"
				value='Y' name='sr51mscreensfl.select_R<%=count%>' id='sr51mscreensfl.select_R<%=count%>'
				onFocus="doFocus('sr51mscreensfl.select_R<%=count%>')" 
				onHelp='return fieldHelp("sr51mscreensfl" + "." +"select")' class="UICheck"
				 <%  if((sv.select).getColor()!=null){
			 		%>style='background-color:#FF0000;'
					<%} if(((sv.select.getFormData()).toString().trim().equalsIgnoreCase("Y"))) {%>
					    checked="checked"
					<%} if((sv.select).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
		   			disabled
				<% } %>
				>
				<input type='checkbox' style="visibility: hidden" value='N' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr51mscreensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr51mscreensfl.select_R<%=count%>' id='sr51mscreensfl.select_R<%=count%>'
						 <%if(!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("Y"))) {%>
					    checked="checked"
					<%}%>
					onclick="handleCheckBox('sr51mscreensfl.select_R<%=count%>')"
					 />
			<%}%>
	</td>
	
	<td
	<%-- <td style="width:<%=tblColumnWidth[2 ]%>px;"  --%>
		<%if(!(((BaseScreenData)sv.life) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >
		<%
			longValue=sv.life.getFormData();
		%>
	<div id="sr51mscreensfl.life_R<%=count%>" name="sr51mscreensfl.life_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
	</td>
	<td 
		<%if(!(((BaseScreenData)sv.coverage) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
		<%if((new Byte((sv.coverage).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.coverage.getFormData();
		%>
	<div id="sr51mscreensfl.coverage_R<%=count%>" name="sr51mscreensfl.coverage_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
		<%}%>
	</td>
	<td 
		<%if(!(((BaseScreenData)sv.rider) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
		<%if((new Byte((sv.rider).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.rider.getFormData();
		%>
	<div id="sr51mscreensfl.rider_R<%=count%>" name="sr51mscreensfl.rider_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
		<%}%>
	</td>
	<td  
	
	
		<%if(!(((BaseScreenData)sv.crtable) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
		<%if((new Byte((sv.crtable).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.crtable.getFormData();
		%>
	<div id="sr51mscreensfl.crtable_R<%=count%>" name="sr51mscreensfl.crtable_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
	<%}%>
		<%if((new Byte((sv.crtabled).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.crtabled.getFormData();
		%>
	<div id="sr51mscreensfl.crtabled_R<%=count%>" name="sr51mscreensfl.crtabled_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
		<%}%>
	</td>
   	<td 
		<%if(!(((BaseScreenData)sv.statcde) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >
		<div>									
		<%if((new Byte((sv.statcde).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.statcde.getFormData();
		%>
	<div id="sr51mscreensfl.statcde_R<%=count%>" name="sr51mscreensfl.statcde_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
		<%}%>
		</div><div style="
    margin-top: -18px;
    margin-left: 25px;
">
	<%="/"%>
	</div><div style="
    margin-top: -18px;
    margin-left: 36px;
">
	<%if((new Byte((sv.pstatcode).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%
			longValue=sv.pstatcode.getFormData();
		%>
	<div id="sr51mscreensfl.pstatcode_R<%=count%>" name="sr51mscreensfl.pstatcode_R<%=count%>">
		<%=longValue%>
	</div>
		<%
			longValue = null;
		%>
		<%}%>
		</div>
	</td>
	<td 
		<%if(!(((BaseScreenData)sv.sumins) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
		<%if((new Byte((sv.sumins).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%	
			sm = sfl.getCurrentScreenRow();
			qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());						
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
		%>
		<%
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			if(!(sv.sumins).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
				formatValue = formatValue( formatValue );
			}
		%>
 	<div id="sr51mscreensfl.sumins_R<%=count%>" name="sr51mscreensfl.sumins_R<%=count%>">
		<%= formatValue%>
	</div>
		<%
			longValue = null;
			formatValue = null;
		%>
		<%}%>
	</td>
	<td 
		<%if(!(((BaseScreenData)sv.instprem) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
		<%if((new Byte((sv.instprem).getInvisible()))
			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%	
			sm = sfl.getCurrentScreenRow();
			qpsf = sm.getFieldXMLDef((sv.instprem).getFieldName());						
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
		%>
		<%
			formatValue = smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			if(!(sv.instprem).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
				formatValue = formatValue( formatValue );
			}
		%>
 	<div id="sr51mscreensfl.instprem_R<%=count%>" name="sr51mscreensfl.instprem_R<%=count%>">
		<%= formatValue%>
	</div>
		<%
			longValue = null;
			formatValue = null;
		%>
		<%}%>
	</td>
	</tr>
	<%
	count = count + 1;
	Sr51mscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


</tbody>
</table></div></div></div></div>
<%appVars.restoreAllInds(savedInds);%>
<%}%>

<%if (sv.Sr51mprotectWritten.gt(0)) {%>
	<%Sr51mprotect.clearClassString(sv);%>
  		
		<%					
{
							}
							
	%>
							
								
<%}%>
			
			


<script>
	$(document).ready(function() {
    	$('#dataTables-sr51m').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "400px",
			scrollCollapse: true,
			scrollX: true,
			info: false
      	});
    });
</script>



	<%if (sv.Sr51mscreenWritten.gt(0)) { %>
	<%Sr51mscreen.clearClassString(sv);%>
	<%StringData generatedText25 = new StringData("Projected PTD ");%>
	<%sv.hprjptdateDisp.setClassString("");%>
<%	sv.hprjptdateDisp.appendClassString("string_fld");
	sv.hprjptdateDisp.appendClassString("output_txt");
	sv.hprjptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText20 = new StringData("Total ");%>
	<%sv.xamt.setClassString("");%>
<%	sv.xamt.appendClassString("num_fld");
	sv.xamt.appendClassString("output_txt");
	sv.xamt.appendClassString("highlight");
%>
	<%StringData generatedText22 = new StringData("Cnt Fee ");%>
	<%sv.totalfee.setClassString("");%>
<%	sv.totalfee.appendClassString("num_fld");
	sv.totalfee.appendClassString("output_txt");
	sv.totalfee.appendClassString("highlight");
%>
	<%StringData generatedText23 = new StringData("Modal Premium ");%>
	<%sv.cmax.setClassString("");%>
<%	sv.cmax.appendClassString("num_fld");
	sv.cmax.appendClassString("output_txt");
	sv.cmax.appendClassString("highlight");
%>
	<%StringData generatedText21 = new StringData("Adj Amt ");%>
	<%sv.manadj.setClassString("");%>
	<%StringData generatedText24 = new StringData("Reinsts Total ");%>
	<%sv.reqamt.setClassString("");%>
<%	sv.reqamt.appendClassString("num_fld");
	sv.reqamt.appendClassString("output_txt");
	sv.reqamt.appendClassString("highlight");
%>
	<%sv.optdsc.setClassString("");%>
<%	sv.optdsc.appendClassString("string_fld");
	sv.optdsc.appendClassString("output_txt");
	sv.optdsc.appendClassString("highlight");
%>
	<%sv.optind.setClassString("");%>

	<%
{
		if (appVars.ind03.isOn()) {
			sv.optind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind53.isOn()) {
			sv.optind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind63.isOn()) {
			sv.optind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.optind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.optind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.manadj.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind75.isOn()) {
			sv.manadj.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			sv.manadj.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.manadj.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind76.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
	
	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Projected PTD")%></label>
					    		   <%}%>  <div class="input-group">
						    		<%if ((new Byte((sv.hprjptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%					
		if(!((sv.hprjptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.hprjptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.hprjptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
							<%}%>
							<div class="input-group">
						    	<%	
			qpsf = fw.getFieldXMLDef((sv.xamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.xamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.xamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				
						<div class="output_cell">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>	

				      			     </div>
						</div>
				   </div>		</div>	
				   
				   
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Cnt Fee")%></label>
					    		    <%}%>
					    		     <div class="input-group">
						    		<%	
			qpsf = fw.getFieldXMLDef((sv.totalfee).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalfee,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.totalfee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				
						<div class="output_cell">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Modal Premium")%></label>
							<%}%> 
							<div class="input-group">
						    		
<%	
			qpsf = fw.getFieldXMLDef((sv.cmax).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.cmax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.cmax.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				
						<div class="output_cell">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
				      			     </div>
						</div>
				   </div>		</div>	
				   
				   
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> 		  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Adj Amt")%></label>
					    		  <%}%> <div class="input-group">
						    		<input name='manadj' 
	type='text' 
	maxLength='<%=sv.manadj.getLength()%>' 
	size='<%=sv.manadj.getLength()%>'
	onFocus='doFocus(this)' onHelp='return fieldHelp(manadj)' onKeyUp='return checkMaxLength(this)'  
<%	
			qpsf = fw.getFieldXMLDef((sv.manadj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.manadj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.manadj.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
		<%if (appVars.ind75.isOn()) {%>
				class="output_cell" readonly
				<%}else{%>
				class="input_cell"
				<%} %>
				value='<%= formatValue%>'
		<%
			} else {
				formatValue = formatValue( "0.00" );
				%>
				<%if (appVars.ind75.isOn()) {%>
				class="output_cell" readonly
				<%}else{%>
				class="input_cell"
				<%}%>
				value='<%= formatValue%>'
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">
						<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Reinsts Total")%></label>
							<%}%>
							<div class="input-group">
						    	<%	
			qpsf = fw.getFieldXMLDef((sv.reqamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.reqamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.reqamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
				formatValue = formatValue( "0.00" );
		%>
				
				<div class="output_cell">
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>	

				      			     </div>
						</div>
				   </div>		</div>			
				
			<%}%>
			
			
		
<Div id='mainForm_OPTS' style='visibility:hidden;'>

 <%=smartHF.getMenuLink(sv.optind, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>


</div>		
			
			
			<%-- <div id='mainForm_OPTS' style='visibility:hidden'>
<%
if(!(sv.optind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>    
</a>
</div>	
</div>
<%} %>
<div>
<input name='optind' id='optind' type='hidden'  value="<%=sv.optind.getFormData()%>">
</div>

</div>	 --%>
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->




<%@ include file="/POLACommon2NEW.jsp"%>