

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6751";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6751ScreenVars sv = (S6751ScreenVars) fw.getVariables();%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Contract Enquiry");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Windforward Registration");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Insert Batch Transaction");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Windforward Confirmation");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Display Windforward List");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F - Cancel Windforward");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

<div class="panel-body">  

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
    	 					<div class="input-group">


<input name='chdrsel' 
 id='chdrsel'
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>


</div></div></div>
<div class="col-md-4"> </div>
<div class="col-md-4"> </div>
</div>



</div></div>






<div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

<div class="panel-body">  

			 <div class="row">	
			 <div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Windforward Registration")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Insert Batch Transaction")%>
					</b></label>
				</div>
			</div>
			    	




</div>

			 <div class="row">	
			 <div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "D")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Windforward Confirmation")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "E")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Display Windforward List")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "F")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Cancel Windforward")%>
					</b></label>
				</div>
			</div>
			    	




</div>



</div></div>






<%@ include file="/POLACommon2NEW.jsp"%>

