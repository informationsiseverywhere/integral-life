<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SD5LM";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%//@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sd5lmScreenVars sv = (Sd5lmScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind02.isOn()) {
			sv.liscPayment.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.coCopPayment.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {			
			sv.revsgtpct.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {			
			sv.revoerpct.setColor(BaseScreenData.RED);
		}
		if (appVars.ind06.isOn()) {			
			sv.revdedpct.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {			
			sv.revundpct.setColor(BaseScreenData.RED);
		}
		if (appVars.ind08.isOn()) {			
			sv.revspspct.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {			
			sv.revslypct.setColor(BaseScreenData.RED);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">

			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					<table>
						<tr>
							<td>
								<%					
			if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
									style="width: 60px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
			longValue = null;
			formatValue = null;
			%>

							</td>
							<td>
								<%					
			if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
									style="width: 30px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
			longValue = null;
			formatValue = null;
			%>
							</td>
							<td>
								<%					
			if(!((sv.cntypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cntypdesc.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cntypdesc.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
									style="width: 200px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
			longValue = null;
			formatValue = null;
			%>
							</td>
						</tr>
					</table>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
					<table>
						<tr>
							<td>
								<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
									style='margin-right: 1px;'>
									<%=sv.lifenum.getFormData()%>
								</div> <%
		longValue = null;
		formatValue = null;
		%>
							</td>
							<td>
								<%					
		if(!((sv.lifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
									style="max-width: 200px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
		longValue = null;
		formatValue = null;
		%>
							</td>
						</tr>
					</table>

				</div>
			</div>


		</div>

		<div class="row">


			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax period")%></label>
					<table>
						<tr>
							<td>
								 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp)%> 
								


							</td>


							<td style="padding-left: 5px;"><label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label>
							</td>

							<td style="padding-left: 5px;">
							
								<%=smartHF.getRichTextDateInput(fw, sv.datetoDisp)%>
								
							</td>
						</tr>
					</table>

				</div>
			</div>


		</div>

		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<label></label>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contribution Amount")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cont%")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label style="display: inline;"><%=resourceBundleHandler.gettingValueFromBundle("Revised Contribution Amount")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Cont%")%></label>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Employer contribution")%></label>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Super guarantee")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='contrAmount01' type='text'
						<%if((sv.contrAmount01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <% }%> readonly="true"
						value='<%=valueThis%>'
						<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount01.getLength(), sv.contrAmount01.getScale(),3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount01.getLength(), sv.contrAmount01.getScale(),3)-3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zsgtamt)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">
				</div>
			</div>

			
			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
						<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt01).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			%>
							<td><input name='contrPrcnt01' type='text'
								<%if((sv.contrPrcnt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 145px;" <% }%> readonly="true"
								value='<%=valueThis%>'
								<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								title='<%=valueThis%>' <%}%>
								size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt01.getLength(), sv.contrPrcnt01.getScale(),3)%>'
								maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt01.getLength(), sv.contrPrcnt01.getScale(),3)-3%>'
								onFocus='doFocus(this),onFocusRemoveCommas(this)'
								onHelp='return fieldHelp(zsgtamt)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="output_cell"></td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.revsgtamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revsgtamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='revsgtamt' id='revsgtamt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revsgtamt.getLength()%>'
			size='<%=sv.revsgtamt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revsgtamt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revsgtamt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revsgtamt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revsgtamt).getColor()== null  ?
			"input_cell" :  (sv.revsgtamt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
				</div> 
			</div>

			<%	
			qpsf = fw.getFieldXMLDef((sv.revsgtpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revsgtpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
							<td>
							<input name='revsgtpct' id='revsgtpct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revsgtpct.getLength()%>'
			size='<%=sv.revsgtpct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revsgtpct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revsgtpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revsgtpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revsgtpct).getColor()== null  ?
			"input_cell" :  (sv.revsgtpct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
							</td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Other")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='contrAmount02' type='text'
						<%if((sv.contrAmount02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <% }%> readonly="true"
						value='<%=valueThis%>'
						<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount02.getLength(), sv.contrAmount02.getScale(),3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount02.getLength(), sv.contrAmount02.getScale(),3)-3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zsgtamt)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">
				</div>
			</div>

	
			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
								<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt02).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
							<td><input name='contrPrcnt02' type='text'
								<%if((sv.contrPrcnt02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 145px;" <% }%> readonly="true"
								value='<%=valueThis%>'
								<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								title='<%=valueThis%>' <%}%>
								size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt02.getLength(), sv.contrPrcnt02.getScale(),3)%>'
								maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt02.getLength(), sv.contrPrcnt02.getScale(),3)-3%>'
								onFocus='doFocus(this),onFocusRemoveCommas(this)'
								onHelp='return fieldHelp(zsgtamt)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="output_cell"></td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.revoeramt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revoeramt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='revoeramt' id='revoeramt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revoeramt.getLength()%>'
			size='<%=sv.revoeramt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revoeramt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revoeramt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revoeramt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revoeramt).getColor()== null  ?
			"input_cell" :  (sv.revoeramt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
				</div>
			</div>

			<%	
			qpsf = fw.getFieldXMLDef((sv.revoerpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revoerpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
							<td><input name='revoerpct' id='revoerpct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revoerpct.getLength()%>'
			size='<%=sv.revoerpct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revoerpct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revoerpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revoerpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revoerpct).getColor()== null  ?
			"input_cell" :  (sv.revoerpct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>></td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Member contributions")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Concessional")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='contrAmount03' type='text'
						<%if((sv.contrAmount03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <% }%> readonly="true"
						value='<%=valueThis%>'
						<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount03.getLength(), sv.contrAmount03.getScale(),3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount03.getLength(), sv.contrAmount03.getScale(),3)-3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zsgtamt)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
							<td>
										<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt03).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
	<input name='contrPrcnt03' type='text'
								<%if((sv.contrPrcnt03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 145px;" <% }%> readonly="true"
								value='<%=valueThis%>'
								<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								title='<%=valueThis%>' <%}%>
								size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt03.getLength(), sv.contrPrcnt03.getScale(),3)%>'
								maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt03.getLength(), sv.contrPrcnt03.getScale(),3)-3%>'
								onFocus='doFocus(this),onFocusRemoveCommas(this)'
								onHelp='return fieldHelp(zsgtamt)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="output_cell"></td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">

					<%	
							qpsf = fw.getFieldXMLDef((sv.revdedamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revdedamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

					<input name='revdedamt' id='revdedamt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revdedamt.getLength()%>'
			size='<%=sv.revdedamt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revdedamt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revdedamt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revdedamt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revdedamt).getColor()== null  ?
			"input_cell" :  (sv.revdedamt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<table>
						<tr>
							<%	
			qpsf = fw.getFieldXMLDef((sv.revdedpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revdedpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
							<td><input name='revdedpct' id='revdedpct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revdedpct.getLength()%>'
			size='<%=sv.revdedpct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revdedpct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revdedpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revdedpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revdedpct).getColor()== null  ?
			"input_cell" :  (sv.revdedpct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>></td>

							<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

	<div class="row">
		<div class="col-md-2">
				<div class="form-group">
					<label></label>
				</div>
			</div>
		<div class="col-md-2">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Non Concessional")%></label>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='contrAmount04' type='text'
					<%if((sv.contrAmount04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <% }%> readonly="true"
					value='<%=valueThis%>'
					<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount04.getLength(), sv.contrAmount04.getScale(),3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount04.getLength(), sv.contrAmount04.getScale(),3)-3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(zsgtamt)'
					onKeyUp='return checkMaxLength(this)' readonly="true"
					class="output_cell">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
							<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt04).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%> <input name='contrPrcnt04' type='text'
							<%if((sv.contrPrcnt04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt04.getLength(), sv.contrPrcnt04.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt04.getLength(), sv.contrPrcnt04.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(zsgtamt)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
						</td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.revundamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revundamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='revundamt' id='revundamt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revundamt.getLength()%>'
			size='<%=sv.revundamt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revundamt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revundamt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revundamt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revundamt).getColor()== null  ?
			"input_cell" :  (sv.revundamt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
								<%	
			qpsf = fw.getFieldXMLDef((sv.revundpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revundpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						<input name='revundpct' id='revundpct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revundpct.getLength()%>'
			size='<%=sv.revundpct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revundpct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revundpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revundpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revundpct).getColor()== null  ?
			"input_cell" :  (sv.revundpct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Spouse contributions")%></label>
			</div>
		</div>
	

		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='contrAmount05' type='text'
					<%if((sv.contrAmount05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <% }%> readonly="true"
					value='<%=valueThis%>'
					<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount05.getLength(), sv.contrAmount05.getScale(),3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount05.getLength(), sv.contrAmount05.getScale(),3)-3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(zsgtamt)'
					onKeyUp='return checkMaxLength(this)' readonly="true"
					class="output_cell">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
								<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt05).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						<input name='contrPrcnt05' type='text'
							<%if((sv.contrPrcnt05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt05.getLength(), sv.contrPrcnt05.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt05.getLength(), sv.contrPrcnt05.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(zsgtamt)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell"></td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.revspsamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revspsamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='revspsamt' id='revspsamt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revspsamt.getLength()%>'
			size='<%=sv.revspsamt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revspsamt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revspsamt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revspsamt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revspsamt).getColor()== null  ?
			"input_cell" :  (sv.revspsamt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
								<%	
			qpsf = fw.getFieldXMLDef((sv.revspspct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revspspct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						<input name='revspspct' id='revspspct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revspspct.getLength()%>'
			size='<%=sv.revspspct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revspspct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revspspct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revspspct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revspspct).getColor()== null  ?
			"input_cell" :  (sv.revspspct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>></td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>


	</div>
	<!-- ILIFE-7345 start-->
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Salary Sacrifice")%></label>
			</div>
		</div>



		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.contrAmount06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrAmount06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='contrAmount06' type='text'
					<%if((sv.contrAmount06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <% }%> readonly="true"
					value='<%=valueThis%>'
					<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount06.getLength(), sv.contrAmount06.getScale(),3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrAmount06.getLength(), sv.contrAmount06.getScale(),3)-3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(zsgtamt)'
					onKeyUp='return checkMaxLength(this)' readonly="true"
					class="output_cell">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
						
								<%	
			qpsf = fw.getFieldXMLDef((sv.contrPrcnt06).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.contrPrcnt06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						<input name='contrPrcnt06' type='text'
							<%if((sv.contrPrcnt06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt06.getLength(), sv.contrPrcnt06.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrPrcnt06.getLength(), sv.contrPrcnt06.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(zsgtamt)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell"></td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.revslyamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revslyamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='revslyamt' id='revslyamt'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revslyamt.getLength()%>'
			size='<%=sv.revslyamt.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revslyamt)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:145px; text-align:right;"
			<%
				if((new Byte((sv.revslyamt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revslyamt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revslyamt).getColor()== null  ?
			"input_cell" :  (sv.revslyamt).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<table>
					<tr>
						<td>
						
								<%	
			qpsf = fw.getFieldXMLDef((sv.revslypct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.revslypct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						<input name='revslypct' id='revslypct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.revslypct.getLength()%>'
			size='<%=sv.revslypct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(revslypct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:60px; text-align:right;"
			<%
				if((new Byte((sv.revslypct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.revslypct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.revslypct).getColor()== null  ?
			"input_cell" :  (sv.revslypct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>></td>

						<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<%if ((new Byte((sv.coCopPayment).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Co-Contributions Payment")%></label>
			</div>
		</div>
	

		<div class="col-md-8">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.coCopPayment).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.coCopPayment,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='coCopPayment' type='text'
					<%if((sv.coCopPayment).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <% }%> readonly="true"
					value='<%=valueThis%>'
					<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.coCopPayment.getLength(), sv.coCopPayment.getScale(),3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.coCopPayment.getLength(), sv.coCopPayment.getScale(),3)-3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(zsgtamt)'
					onKeyUp='return checkMaxLength(this)' readonly="true"
					class="output_cell">
			</div>
		</div>
		</div>
		<%} %>
		<%if ((new Byte((sv.liscPayment).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Lisc Payment")%></label>
			</div>
		</div>
	
		<div class="col-md-8">
			<div class="form-group">

				<%	
							qpsf = fw.getFieldXMLDef((sv.liscPayment).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.liscPayment,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>

				<input name='liscPayment' type='text'
					<%if((sv.liscPayment).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <% }%> readonly="true"
					value='<%=valueThis%>'
					<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.liscPayment.getLength(), sv.liscPayment.getScale(),3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.liscPayment.getLength(), sv.liscPayment.getScale(),3)-3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(zsgtamt)'
					onKeyUp='return checkMaxLength(this)' readonly="true"
					class="output_cell">
			</div>
		</div>
		</div>
	<%} %>
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group"></div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total Contribution")%></label>
				<table>
					<tr>
						<td>
							<%	
							qpsf = fw.getFieldXMLDef((sv.totContrAmount).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.totContrAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%> <input name='totContrAmount' type='text'
							<%if((sv.totContrAmount).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totContrAmount.getLength(), sv.totContrAmount.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totContrAmount.getLength(), sv.totContrAmount.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(zsgtamt)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">

						</td>


					</tr>
				</table>

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total Revised Contribution")%></label>
				<table>
					<tr>
						<td>
							<%	
							qpsf = fw.getFieldXMLDef((sv.revTotContrAmount).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.revTotContrAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%> <input name='revTotContrAmount' type='text'
							<%if((sv.revTotContrAmount).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.revTotContrAmount.getLength(), sv.revTotContrAmount.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.revTotContrAmount.getLength(), sv.revTotContrAmount.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(zsgtamt)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">

						</td>


					</tr>
				</table>

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("S290 Notice Received")%></label>
				<table>
					<tr>
						<td>
							<%					
			if(!((sv.notRecvd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.notRecvd.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.notRecvd.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
								style="width: 60px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			longValue = null;
			formatValue = null;
			%>

						</td>
					</tr>
				</table>
			</div>
		</div>






		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Premium")%></label>
				<table>
					<tr>
						<td>
							<%	
							qpsf = fw.getFieldXMLDef((sv.riskPremium).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.riskPremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%> <input name='riskPremium' type='text'
							<%if((sv.riskPremium).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.riskPremium.getLength(), sv.riskPremium.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.riskPremium.getLength(), sv.riskPremium.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(riskPremium)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">

						</td>


					</tr>
				</table>

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contribution Tax")%></label>
				<table>
					<tr>
						<td>
							<%	
							qpsf = fw.getFieldXMLDef((sv.contrTax).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.contrTax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
					%> <input name='contrTax' type='text'
							<%if((sv.contrTax).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width: 145px;" <% }%> readonly="true"
							value='<%=valueThis%>'
							<%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrTax.getLength(), sv.contrTax.getScale(),3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.contrTax.getLength(), sv.contrTax.getScale(),3)-3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(contrTax)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">

						</td>


					</tr>
				</table>

			</div>
		</div>
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>