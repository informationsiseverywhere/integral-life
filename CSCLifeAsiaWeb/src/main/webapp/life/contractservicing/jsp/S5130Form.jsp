<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5130";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S5130ScreenVars sv = (S5130ScreenVars) fw.getVariables();%>

<%if (sv.S5130screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s5130screensfl");
	savedInds = appVars.saveAllInds();
	S5130screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 14;
	String height = smartHF.fmty(14);
	smartHF.setSflLineOffset(9);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(9)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (S5130screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.action.setClassString("");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%sv.elemkey.setClassString("");%>
<%	sv.elemkey.appendClassString("string_fld");
	sv.elemkey.appendClassString("output_txt");
	sv.elemkey.appendClassString("highlight");
%>
	<%sv.elemdesc.setClassString("");%>
<%	sv.elemdesc.appendClassString("string_fld");
	sv.elemdesc.appendClassString("output_txt");
	sv.elemdesc.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>
	<%sv.hlife.setClassString("");%>
	<%sv.hcoverage.setClassString("");%>
	<%sv.hrider.setClassString("");%>
	<%sv.hcrtable.setClassString("");%>
	<%sv.hcovt.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.action)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 8, sfl, sv.life)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 13, sfl, sv.coverage)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 17, sfl, sv.rider)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 23, sfl, sv.elemkey)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 33, sfl, sv.elemdesc)%>






		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S5130screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.S5130protectWritten.gt(0)) {%>
	<%S5130protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S5130screenWritten.gt(0)) {%>
	<%S5130screen.clearClassString(sv);%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F9 - Full pre-issue validation");%>
<%	generatedText9.appendClassString("label_txt");
	generatedText9.appendClassString("information_txt");
%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 3, generatedText9)%>


<%}%>

<%if (sv.S5130screenctlWritten.gt(0)) {%>
	<%S5130screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5130screensfl");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.ownername.setClassString("");%>
<%	sv.ownername.appendClassString("string_fld");
	sv.ownername.appendClassString("output_txt");
	sv.ownername.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Select, 2 - Add to");%>
<%	generatedText8.appendClassString("subfile_hdg");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act?");%>
<%	generatedText4.appendClassString("subfile_hdg");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
<%	generatedText5.appendClassString("subfile_hdg");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov");%>
<%	generatedText6.appendClassString("subfile_hdg");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
<%	generatedText7.appendClassString("subfile_hdg");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

	<%=smartHF.getLit(3, 6, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 20, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 30, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 30, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 35, fw, sv.ctypedes)%>

	<%=smartHF.getLit(4, 6, generatedText3)%>

	<%=smartHF.getHTMLVar(4, 20, fw, sv.cownnum)%>

	<%=smartHF.getHTMLVar(4, 30, fw, sv.ownername)%>

	<%=smartHF.getLit(6, 3, generatedText8)%>

	<%=smartHF.getLit(8, 2, generatedText4)%>

	<%=smartHF.getLit(8, 7, generatedText5)%>

	<%=smartHF.getLit(8, 12, generatedText6)%>

	<%=smartHF.getLit(8, 16, generatedText7)%>





<%}%>



<%@ include file="/POLACommon2.jsp"%>
