<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5134";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S5134ScreenVars sv = (S5134ScreenVars) fw.getVariables();%>

<%if (sv.S5134screenWritten.gt(0)) {%>
	<%S5134screen.clearClassString(sv);%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText11)%>


<%}%>

<%if (sv.S5134screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s5134screensfl");
	savedInds = appVars.saveAllInds();
	S5134screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 13;
	String height = smartHF.fmty(13);
	smartHF.setSflLineOffset(11);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(11)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (S5134screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.asterisk.setClassString("");%>
<%	sv.asterisk.appendClassString("string_fld");
	sv.asterisk.appendClassString("output_txt");
	sv.asterisk.appendClassString("highlight");
%>
	<%sv.select.setClassString("");%>
	<%sv.cmpntnum.setClassString("");%>
<%	sv.cmpntnum.appendClassString("string_fld");
	sv.cmpntnum.appendClassString("output_txt");
	sv.cmpntnum.appendClassString("highlight");
%>
	<%sv.component.setClassString("");%>
<%	sv.component.appendClassString("string_fld");
	sv.component.appendClassString("output_txt");
	sv.component.appendClassString("highlight");
%>
	<%sv.deit.setClassString("");%>
<%	sv.deit.appendClassString("string_fld");
	sv.deit.appendClassString("output_txt");
	sv.deit.appendClassString("highlight");
%>
	<%sv.statcode.setClassString("");%>
<%	sv.statcode.appendClassString("string_fld");
	sv.statcode.appendClassString("output_txt");
	sv.statcode.appendClassString("highlight");
%>
	<%sv.pstatcode.setClassString("");%>
<%	sv.pstatcode.appendClassString("string_fld");
	sv.pstatcode.appendClassString("output_txt");
	sv.pstatcode.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>
	<%sv.hlifcnum.setClassString("");%>
	<%sv.hlifeno.setClassString("");%>
	<%sv.hsuffix.setClassString("");%>
	<%sv.hcoverage.setClassString("");%>
	<%sv.hrider.setClassString("");%>
	<%sv.hcrtable.setClassString("");%>
	<%sv.linetype.setClassString("");%>
	<%sv.hcovt.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 2, sfl, sv.asterisk)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 4, sfl, sv.select)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 7, sfl, sv.cmpntnum)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 10, sfl, sv.component)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 20, sfl, sv.deit)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 59, sfl, sv.statcode)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 70, sfl, sv.pstatcode)%>









		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S5134screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.S5134protectWritten.gt(0)) {%>
	<%S5134protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S5134screenctlWritten.gt(0)) {%>
	<%S5134screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5134screensfl");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan  ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%sv.entity.setClassString("");%>
	<%sv.planSuffix.setClassString("");%>
<%	sv.planSuffix.appendClassString("num_fld");
	sv.planSuffix.appendClassString("output_txt");
	sv.planSuffix.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Component   Description                            Risk Stat  Prem Stat");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
		if (appVars.ind06.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.entity.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 3, generatedText1)%>

	<%=smartHF.getHTMLVar(3, 13, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 24, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.ctypedes)%>

	<%=smartHF.getLit(3, 64, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 75, fw, sv.cntcurr)%>
	<%=smartHF.getHTMLF4NSVar(3, 75, fw, sv.cntcurr)%>

	<%=smartHF.getLit(4, 3, generatedText3)%>

	<%=smartHF.getHTMLVar(4, 20, fw, sv.chdrstatus)%>

	<%=smartHF.getLit(4, 31, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 47, fw, sv.premstatus)%>

	<%=smartHF.getLit(4, 64, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(4, 75, fw, sv.register)%>
	<%=smartHF.getHTMLF4NSVar(4, 75, fw, sv.register)%>

	<%=smartHF.getLit(5, 3, generatedText6)%>

	<%=smartHF.getHTMLVar(5, 20, fw, sv.lifenum)%>

	<%=smartHF.getHTMLVar(5, 31, fw, sv.lifename)%>

	<%=smartHF.getLit(6, 3, generatedText7)%>

	<%=smartHF.getHTMLVar(6, 20, fw, sv.jlife)%>

	<%=smartHF.getHTMLVar(6, 31, fw, sv.jlifename)%>

	<%=smartHF.getLit(8, 3, generatedText8)%>

	<%=smartHF.getHTMLVar(8, 22, fw, sv.numpols, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLSpaceVar(8, 31, fw, sv.entity)%>
	<%=smartHF.getHTMLF4NSVar(8, 31, fw, sv.entity)%>

	<%=smartHF.getHTMLVar(8, 48, fw, sv.planSuffix, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(10, 3, generatedText9)%>





<%}%>



<%@ include file="/POLACommon2.jsp"%>
