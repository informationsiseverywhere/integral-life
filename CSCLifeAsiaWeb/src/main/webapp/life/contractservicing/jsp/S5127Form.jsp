<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5127";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5127ScreenVars sv = (S5127ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no   ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. fund ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-sect ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life    ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Policies ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Num ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to 1");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess Age / Term ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess date ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess Age / Term ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess date ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality class ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl Method ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Commission Split ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life (J/L) ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total premium with Tax ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Detail ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prorated Premium to be collected  ");%>
	
    
	<% 	
      // ilife-3395
	String[][] items = {{ "statFund","mortcls","currcd","liencd","prmbasis"}, {}, {} };
		Map longDesc = appVars.getLongDesc(items, "E", baseModel.getCompany().toString().trim(), baseModel, sv);
	%>




<%{
		appVars.rolldown(new int[] {10});
		appVars.rollup(new int[] {10});
		if (appVars.ind42.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind05.isOn()) {
			sv.planSuffix.setColor(BaseScreenData.WHITE);
		}
		if (appVars.ind50.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind50.isOn()) {
			generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
			sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind88.isOn()) {//ILJ-47
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}		
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}	
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
			
		}
		//ILIFE-680 START -- Screen S5157 Agent on Component Enquiry -- COMIND made visible
		/*  
		if (appVars.ind32.isOn()) {
			sv.comind.setInvisibility(BaseScreenData.INVISIBLE);
		}*/
		//ILIFE-680 END -- Screen S5157 Agent on Component Enquiry -- COMIND made visible
		if (appVars.ind27.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
			sv.pbind.setColor(BaseScreenData.RED);
			
		}
	
		if(appVars.ind33.isOn()){
		sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind33.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
			
		}
		if (appVars.ind38.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
			sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		
		
		
		if (appVars.ind36.isOn()) {
			sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind42.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind42.isOn()) {
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		//ILIFE-1050 STARTS
			if (appVars.ind74.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		/*ILIFE-7730 starts*/
	   /*if (appVars.ind31.isOn()) {
			sv.linstamt.setEnabled(BaseScreenData.DISABLED);
		} */
	    if (appVars.ind75.isOn()) {
			sv.linstamt.setEnabled(BaseScreenData.DISABLED);
		}
		/*ILIFE-7730 ends*/
		if (appVars.ind37.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
		}
		//ILIFE-1050 ENDS
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//ILIFE-3399-STARTS
		if (appVars.ind54.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		// ILIFE-3399-ENDS
		if (appVars.ind57.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
		}
		
		if (appVars.ind58.isOn()) {
			sv.prmbasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		/* ILIFE-7118-starts */
		if (appVars.ind77.isOn()) {
			sv.tpdtype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind78.isOn()) {
			sv.tpdtype.setEnabled(BaseScreenData.DISABLED);
		}
		/* ILIFE-7118-ends */
		//ICIL-560 FWANG3
		if (appVars.ind79.isOn()) {
			sv.cashvalarer.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if(appVars.ind80.isOn()){
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		/*ILIFE-8248 start*/
		if (!appVars.ind62.isOn()) {
			sv.lnkgno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind63.isOn()) {
			sv.lnkgsubrefno.setInvisibility(BaseScreenData.INVISIBLE);
		}
	 	if (appVars.ind60.isOn()) {
			sv.lnkgno.setEnabled(BaseScreenData.DISABLED);
		} 
		if (appVars.ind61.isOn()) {
			sv.lnkgsubrefno.setEnabled(BaseScreenData.DISABLED);
		}
		/*ILIFE-8248 end*/
		/* IBPLIFE-2137 Start */
		if (sv.nbprp126lag.compareTo("N") != 0){
		if (appVars.ind82.isOn()) {
		sv.covrprpse.setReverse(BaseScreenData.REVERSED);
		sv.covrprpse.setColor(BaseScreenData.RED);
		}
		 if (appVars.ind85.isOn()) {
			sv.covrprpse.setEnabled(BaseScreenData.DISABLED);
		} 
		if (!appVars.ind82.isOn()) {
			sv.covrprpse.setHighLight(BaseScreenData.BOLD);
		}
		}
		/* IBPLIFE-2137 End */
		
	    if (appVars.ind34.isOn()) {//IBPLIFE-7254
	            sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
	            sv.pbind.setEnabled(BaseScreenData.DISABLED);
	        }

	}

	%>
	<style>
		#stampduty{
			width: 145px !important;
		}
	</style>

<script>
$(document).ready(function(){
	
	$("#chdrnum").attr("class","input-group-addon");
	$("#ctypedes").attr("class","form-control");
	$("#cnttype").attr("class","input-group-addon");
	$("#jlinsname").attr("class","form-control");
	$("#jlifcnum").attr("class","input-group-addon");
	$("#linsname").attr("class","form-control");
	$("#lifcnum").attr("class","input-group-addon");
	$("#sum").css("width","150px");
})
</script>
<!-- ILIFE-2721 Life Cross Browser - Sprint 4 D5 : Task 2  ends-->
<div class="panel panel-default">
 <div class="panel-body">   
 	<div class="row">	
	    	<div class="col-md-8"> 
		    		<div class="form-group">
 <label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</label>
<!--     		<div class="input-group"> -->
<table><tr><td>
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="margin-left: 1px;"  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="margin-left: 1px;min-width: 180px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
		<!-- </div> -->

</div></div></div>


<div class="row">	
   	<div class="col-md-7"> 
    		<div class="form-group">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
</label>
<%}%>
<table><tr><td>
<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 60px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td><td>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div   class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td></tr></table>
</div></div>

   	<div class="col-md-1"> </div>
   	<div class="col-md-3"> 
    		<div class="form-group">
<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
	<div class="input-group">
  		
		<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 </div>
	</div></div></div>
	<div class="row">
	<div class="col-md-7">
				<div class="form-group">

<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Joint life")%>
</label>
<%}%>

<table><tr><td>
<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td><td>




<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:71px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td></tr></table>
</div></div>
<div class="col-md-1"> </div>

<div class="col-md-2">
	<div class="form-group">
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%>
</label>
<%}%>
<div class="input-group">

<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div></div>
<div class="col-md-2">
	<div class="form-group">
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%>
</label>
<%}%>
<div class="input-group">
<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </div>
</div></div></div>
<div class="row">	
   	<div class="col-md-2"> 
    		<div class="form-group">

<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life no")%>
</label>
<%}%>

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div>
	<div class="col-md-2"> 
    		<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%>
</label>
<%}%>


<br/>

<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
	<div class="col-md-2"> 
    		<div class="form-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Rider no")%>
</label>
<%}%>


<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>


	<div class="col-md-2"> 
    		<div class="form-group">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%>
</label>
<%}%>

<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
	<%	
	//fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) longDesc.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div>

	<div class="col-md-2"> 
    		<div class="form-group">
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Section")%>
</label>
<%}%>


<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>

	<div class="col-md-2"> 
    		<div class="form-group">
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%>
</label>
<%}%>


<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div></div>


<!-- IBPLIFE-2137 Start -->	
	<% if (sv.nbprp126lag.compareTo("Y") != 0){%>
		<br>
		<hr>
		<br>
		<%} else{%>
			<div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assure / Premium")%></a>
                        </li>
                       
                        <li>
                        	<a href="#prem_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></a>
                        </li>
                    </ul>
                    </div>
                  </div>
             <div class="tab-content">
               	<div class="tab-pane fade in active" id="basic_tab">
               	<%} %>
               	<div class="row">
	<div class="col-md-3"> 
		<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</label>


	<div class="input-group">
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:140px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
</div></div>
	<div class="col-md-3"> 
		<div class="form-group">

<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%>
</label>
<%}%>

<div class="input-group">
<%if ((new Byte((sv.sumin).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input id="sum" name='sumin' 
type='text'

<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis %>'
			 <%
	// valueThis=smartHF.getPicFormatted(qpsf,sv.sumin);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%= sv.sumin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
</div>
</div></div>
	<div class="col-md-3"> 
		<div class="form-group">
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%>
</label>
<%}%>


<%	
	if ((new Byte((sv.mortcls).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	//fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) longDesc.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>   
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:200px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mortcls).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:203px;"> 
<%
} 
%>

<select name='mortcls' type='list' style="width:200px;"
<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mortcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mortcls).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>
</div></div>
	<div class="col-md-3"> 
		<div class="form-group">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
</label>
<%}%>

<%
		longValue = null;
		formatValue = null;
		%>

<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
	//	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) longDesc.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:130px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div></div>
<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<!-- ILJ-47 Starts -->
               		<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
        			<%} %>
        			<!-- ILJ-47 End -->
					<table>
					<tr>
						<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 60px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)' style="width:70px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								style="width:70px;" <%}%>>

						</td>
						<td>

							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 60px; margin-left:1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)' style="width:70px;margin-left:1px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								style="width:70px;margin-left:1px;" <%}%>>
						</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<!-- ILJ-47 Starts -->
				<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
				<%} else { %>
        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess date"))%></label>
        		<%} %>
				<!-- ILJ-47 End -->
					<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->

					<%
						if ((new Byte((sv.riskCessDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp)%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-2" data-date="" id="riskCessDateDisp"
						data-date-format="dd/mm/yyyy" style="width: 150px;"
						data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp,
						(sv.riskCessDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv,
								"E", baseModel);
						mappedItems = (Map) fieldItem.get("liencd");
						optionValue = makeDropDownList(mappedItems,
								sv.liencd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.liencd.getFormData())
								.toString().trim());
						//longValue="RA";
					%>
					<%
						if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div style="width:140px"
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
						: "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div style="border: 1px; border-style: solid; border-color: #ec7572;width:203px;">
						<%}%>
						<select name='liencd' type='list' style="width: 200px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.liencd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>
<div class="col-md-3">
				<%--ILIFE-3423:Start --%>
				<%
					if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
				<%}%>
				<%
					if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
						fieldItem = appVars.loadF4FieldsLong(
								new String[] { "prmbasis" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("prmbasis");
						optionValue = makeDropDownList(mappedItems,
								sv.prmbasis.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems
								.get((sv.prmbasis.getFormData()).toString().trim());
				%>
				<%
					if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
				%>
				<div
					class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
					<%
						if (longValue != null) {
					%>

					<%=longValue%>
					<%
						}
					%>
				</div>

				<%
					longValue = null;
				%>

				<%
					} else {
				%>

				<%
					if ("red".equals((sv.prmbasis).getColor())) {
				%>
				<div
					style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
					<%
						}
					%>
					<select name='prmbasis' type='list' style="width: 145px;"
						<%if ((new Byte((sv.prmbasis).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.prmbasis).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>
						<%=optionValue%>
					</select>
					<%
						if ("red".equals((sv.prmbasis).getColor())) {
					%>
				</div>
				<%
					}
				%>
				<%
					}
					}
				%>
				<%
					longValue = null;
				%>
			</div>
		</div>




<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Prem cess Age / Term")%></label>
					<table>
					<tr>
						<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 60px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)' style="width:70px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.premCessAge).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								style="width:70px;" <%}%>>
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
						</td>
						<td>
							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 60px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;margin-left: 1px;"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								style="width:70px;margin-left: 1px;" <%}%>>
						</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Prem cess date")%>
					</label>
					<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->

					<%
						if ((new Byte((sv.premCessDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp,
						(sv.premCessDateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-2" data-date="" 
						data-date-format="dd/mm/yyyy" style="width: 150px;"
						data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp,
						(sv.premCessDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%>
					</label> <input name='select' type='text'
						<%formatValue = (sv.select.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.select.getLength()%>'
						maxLength='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(select)'
						onKeyUp='return checkMaxLength(this)' style="width: 50px;"
						<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.select).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.select).getColor() == null ? "input_cell"
							: (sv.select).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
						<%}%>>
					<%
						}
					%>
				</div>
			</div>
<!-- BRD-NBP-011 starts -->
<div class="col-md-3">
				<div class="form-group">
<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%>
</label>
<%}%>

<%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.dialdownoption).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:146px;"> 
<%
} 
%>

<select name='dialdownoption' type='list' style="width:146px;"
<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dialdownoption).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dialdownoption).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	
</div></div></div>
<div class="row">
<!-- ILIFE-7118-starts -->
<div class="col-md-3">
				<%
					if ((new Byte((sv.tpdtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("TPD Type")%>
				</label>
				<%
					}
				%>

				<%
					if ((new Byte((sv.tpdtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(
										new String[] { "tpdtype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("tpdtype");
								
								LinkedHashMap<String, String> lmap = new LinkedHashMap<String, String>();
								Iterator entries = mappedItems.entrySet().iterator();
								while (entries.hasNext()) {
									Map.Entry entry = (Map.Entry) entries.next();
									String key = (String) entry.getKey();
									String value = (String) entry.getValue();
									if ((sv.crtabdesc.toString().trim()
											.equals("Total Permanent Disability"))
											&& !(key.substring(0, 4).equals("TPD1"))) {
										continue;
									}
									if ((sv.crtabdesc.toString().trim()
											.equals("Total Perm. Disability Super"))
											&& !(key.substring(0, 4).equals("TPS1"))) {
										continue;
									}
									lmap.put(key, value);
								}

								optionValue = makeDropDownList(lmap, sv.tpdtype.getFormData(),
										2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.tpdtype.getFormData())
										.toString().trim());
						%>

				<%
					if ((new Byte((sv.tpdtype).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables()
										.isScreenProtected())) {
				%>
				<div
					class='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "width:82px;" : "width:140px;"%>'>
					<%
						if (longValue != null) {
					%>

					<%=longValue%>

					<%
						}
					%>
				</div>

				<%
					longValue = null;
				%>

				<%
					} else {
				%>

				<%
					if ("red".equals((sv.tpdtype).getColor())) {
				%>
				<div
					style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
					<%
						}
					%>
					<select name='tpdtype' type='list' style="width: 145px;"
						<%if ((new Byte((sv.tpdtype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.tpdtype).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>
						<%=optionValue%>
					</select>
					<%
						if ("red".equals((sv.tpdtype).getColor())) {
					%>
				</div>
				<%
					}
				%>
				<%
					}
					
				%>
				<%
					longValue = null;
				}
				%>
			</div>
<!-- ILIFE-7118-ends -->
</div>
<br>
<hr>
<br>
<div class="row">
<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %> <!-- ILIFE-3399 -->
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%>
</label>
<%} %>
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%> 
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div>
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%>
</label>
<%} %>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div>
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%>
</label>
<%} %>

<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div>
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%>
</label>
<%} %>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div>
 <%} %>
</div>
<div class="row">
<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte( 
 		BaseScreenData.INVISIBLE)) != 0){ %> 
 <!-- ILIFE-3399-ENDS --> 
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%>
</label>
<%} %>

<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div>
 <%} %><!-- ILIFE-3399 --> 

<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<!-- ILIFE-8323 Start -->								
<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte( 
 		BaseScreenData.INVISIBLE)) != 0){ %> 
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%>
</label>
<%}

if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte( 
 		BaseScreenData.INVISIBLE)) == 0){ %> 
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%>
</label>
<%}
}%>
<!-- ILIFE-8323 ends -->

<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width: 145px !important;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width: 145px !important; "> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
</div>
 <%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte( 
	BaseScreenData.INVISIBLE)) != 0){ %> <!-- ILIFE-3399 --> 
<div class="col-md-3">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%> 
</label>
<%} %>

<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;padding-right:5px; text-align: right;\' ")%>
<%} %>
<%}else {%>

<%}%>
 </div>
 <%} %> 
</div>
<!-- ILIFE-3399-STARTS --> 

<div class="row">
<!-- ILIFE-3399-ENDS -->
<div class="col-md-3">
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%>
</label>
<%}%>


<%if ((new Byte((sv.linstamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input id="linstamt" name='linstamt' 
type='text'

<%if((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>

	value='<%=valueThis %>'
			 <%
	 //valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)%>'
maxLength='<%= sv.linstamt.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.linstamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt).getColor()== null  ? 
			"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%}%>
</div>


<!-- ILIFE-994 BEGINS -->
<div class="col-md-3">
<!--
<%StringData TAXAMT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium with Tax");%>
<%=smartHF.getLit(0, 0, TAXAMT_LBL).replace("absolute","relative")%>
-->
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%>
</label>
<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<% 
//<!-- ILIFE-1722 STARTS -->
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input id="taxamt" name='taxamt' 
type='text'

<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width:145px; "<% }%>

	value='<%=valueThis %>'
			 <%

	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)%>'
maxLength='<%= sv.taxamt.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt01)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
	<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
<%}%>

<!-- ILIFE-1722 ENDS -->
</div>   					
<!-- ILIFE-994 ENDS -->
<div class="col-md-3">

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%>
</label>


	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
			
	%>

<input id="instPrem" name='instPrem' 
type='text'
<%if((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.linstamt.getScale(),3)%>'
maxLength='<%= sv.instPrem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
	
<%--ILIFE-7730 starts --%>
<%-- <% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  

	readonly="true"
	class="output_cell"
	
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  

<%
	}else { 
%>

	class = ' <%=(sv.linstamt).getColor()== null  ? 
			"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%> --%>
<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  

	readonly="true"
	class="output_cell"
	
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
<%--ILIFE-7730 ends --%>
>

</div>
		<%
			if ((new Byte((sv.cashvalarer).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>	
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Value in Arrears")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.cashvalarer).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.cashvalarer,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.cashvalarer.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell"
						style="width: 145px !important; text-align: right;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 145px !important;">
						&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
							formatValue = null;
					%>

				</div>
			</div>

			<%
				}
			%>
			
			<% if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
						<%
							qpsf = fw.getFieldXMLDef((sv.zstpduty01).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!((sv.zstpduty01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( formatValue); 
								} else {
									formatValue = formatValue( longValue);
								}							
							}
						%>
							
						<div id='stampduty' class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			<% } %>
			
	<!--ILIFE-8248 Starts -->	
			<%
				if ((new Byte((sv.lnkgno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%> 
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Number")%></label>
					<div style="width: 175px;">
						<%
							if (((BaseScreenData) sv.lnkgno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgno, (sv.lnkgno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
	 			<%
				}
			%>
			
				<%
				if ((new Byte((sv.lnkgsubrefno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Sub Ref Number")%></label>
					<div style="width: 175px;">
						<%
							if (((BaseScreenData) sv.lnkgsubrefno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgsubrefno, (sv.lnkgsubrefno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgsubrefno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgsubrefno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
				<%
				}
			%>
<!--ILIFE-8248 end -->		
</div>

<% if (sv.nbprp126lag.compareTo("N") != 0){%>
	</div>
	<div class="tab-pane fade" id="prem_tab">
           		<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
								<table>
								<tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trancd, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trandesc, true)%></td>
							</tr>
							</table>
						</div>
				  </div>
				  
				  <div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
								<%
						if ((new Byte((sv.effdatexDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.effdatexDisp.getFormData()%>' maxLength='<%=sv.effdatexDisp.getLength()%>' 
										size='<%=sv.effdatexDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>								
					
					</div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdatexDisp"
						data-link-format="dd/mm/yyyy" >
						<%=smartHF.getRichTextDateInput(fw, sv.effdatexDisp, (sv.effdatexDisp.getLength()))%>
						<span class="input-group-addon" style="visibility: hidden;"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<%
						}
					%>
  </div> 
  </div>
             </div>
             
             <div class="row">
             	<div class="col-md-9">
             	<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose")%></label>

					<table><tr>
							<td>
							
							<textarea name='covrprpse' style='width:400px;height:70px;resize:none;border: 2px solid !important;'
							type='text' 
							
							<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
							
							<%
							
									formatValue = (sv.covrprpse.getFormData()).toString();
							
							%>
							 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>
							
							size='<%= sv.covrprpse.getLength() %>'
							maxLength='<%= sv.covrprpse.getLength() %>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.covrprpse).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
								disabled
							<%
								}else if((new Byte((sv.covrprpse).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.covrprpse).getColor()== null  ? 
										"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							><%=XSSFilter.escapeHtml(formatValue)%></textarea>
							
							</td></tr>
							
							</table>
					
              </div>
             </div>
             </div>
             
             
             </div>
             </div>
             <%} %>  
      <!-- IBPLIFE-2137 End -->             	

<Div id='mainForm_OPTS' style='visibility: hidden;'>

	
	
	<% if((new Byte((sv.pbind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
	<%=smartHF.getMenuLink(sv.pbind, resourceBundleHandler.gettingValueFromBundle("Premium Breakdown"))%>
	<%} %>
	<%if (sv.comind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.comind.getEnabled() != BaseScreenData.DISABLED){%>
	<%=smartHF.getMenuLink(sv.comind,
					resourceBundleHandler.gettingValueFromBundle("Agent Commission Split"))%>
					<%} %>
					<%if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optextind.getEnabled() != BaseScreenData.DISABLED){%>
	<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
	<%} %>
	<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%>
	<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"))%>
	<%} %>
	<%=smartHF.getMenuLink(sv.exclind, resourceBundleHandler.gettingValueFromBundle("Exclusions"))%>
	



</div>
<%-- <Div id='mainForm_OPTS' style='visibility:hidden'>
<li>
		<input name='pbind' id='pbind' type='hidden'  value="<%=sv.pbind.getFormData()%>">
<% if((new Byte((sv.pbind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
		<a href="javascript:;"
		onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))'>
			<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>
		
			<!-- icon -->
			<%
			if (sv.pbind.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.pbind.getFormData().equals("X")) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>  
			<%}%>
		</a>
		<%} %>
	</li>

	<li>
		<input name='comind' id='comind' type='hidden'  value="<%=sv.comind.getFormData()%>">
	<%if (sv.comind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.comind.getEnabled() != BaseScreenData.DISABLED){%>
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))'>
			<%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%></a>	
		
			<%if (sv.comind.getFormData().equals("+")) {%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.comind.getFormData().equals("X")) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>  
			<%}%>
		</a>
		<%} %>
	</li>

	<li>
		<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind.getFormData()%>">
<%if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optextind.getEnabled() != BaseScreenData.DISABLED){%>
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))'>
			<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></a>
			
			<%
			if (sv.optextind.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.optextind.getFormData().equals("X") ) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>
			<%}%>
		</a>
		<%} %>
	</li>
	
	<li>
		<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%>
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'>
			<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%></a>
			
			<%
			if (sv.taxind.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.taxind.getFormData().equals("X") ) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>
			<%}%>
		</a>
		<%} %>
	</li>
	<%	if(sv.exclind
	.getInvisible()!= BaseScreenData.INVISIBLE){
	%> 
	<li>
		<input name='exclind' id='exclind' type='hidden'  value="<%=sv.exclind.getFormData()%>">
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))'>
			<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%></a>
			
			<%
			if (sv.exclind.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.exclind.getFormData().equals("X") ) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>
			<%}%>
		</a>
	</li>	
		<%} %>
</div> --%>							


<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
<td>



</td>
</tr></table></div></div></div>


<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2721 Life Cross Browser - Sprint 4 D5 : Task 2  starts-->
<style>
		
		div[class*='blank_cell']{padding-right:1px !important} 
		 
	
</style>
<!-- ILIFE-2721 Life Cross Browser - Sprint 4 D5 : Task 2  ends-->
