<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52E";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr52eScreenVars sv = (Sr52eScreenVars) fw.getVariables();%>
<%{
	
	if (!appVars.ind01.isOn()) {
		sv.taxind13.setInvisibility(BaseScreenData.INVISIBLE);
	}
}%>








<div class="panel panel-default">

<div class="panel-body">  


			 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                       <div class="form-group" style="width:50px">
                                 <%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>

<div class="col-md-1"> </div>
<div class="col-md-3"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                       <div class="form-group" style="width:100px">
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>



</div></div></div>



<div class="col-md-1"> </div>
<div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                      <table><tr><td>
                       <%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' id="item">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'id="idesc" style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
                       
        </td></tr></table>               
                                        
                       </div></div>

</div>



<div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
  <table>                     
<tr>
<td>

<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>
</td>

<td>
&nbspto&nbsp
</td>

<td style="width:100px">
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%>
</td>
</tr>
</table>


</div></div>



</div>

<br>



<div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Tax Basis     ")%></label>

                        </div></div>

 </div>
 
 
 
 
 
 <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Modal Premium")%></label>
  <div class="form-group" style="width:50;"> 
<%if(((BaseScreenData)sv.taxind01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind01,( sv.taxind01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("(Tax Applied on Basic Premium)")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.zbastyp) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbastyp,( sv.zbastyp.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbastyp) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbastyp, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>
                       
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Fee")%></label>
  <div class="form-group" style="width:50;"> 
<%if(((BaseScreenData)sv.taxind02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind02,( sv.taxind02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>

 </div>
 
 
 
 
 
 
 
 
  <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Charge")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind03,( sv.taxind03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

                       </div> </div></div>
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Reinstatement Fee")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind12,( sv.taxind12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>
                       
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Agent Commission")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind04,( sv.taxind04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>

 </div>
 
 <br>
  <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("(Non Traditional Rules)     ")%></label>
                       
                       </div></div></div>
 
 
 
 
  <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Non Invest Premium")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind05,( sv.taxind05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
         </div> </div></div>
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Charge")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind06,( sv.taxind06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>
                       
                       
                   
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Periodic Fee")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind07,( sv.taxind07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>

 </div>
 
 
 
   <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Issue")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind08,( sv.taxind08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>


         </div> </div></div>
                       
                       
                       
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Admin Charges")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind09,( sv.taxind09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>


                       </div> </div></div>
                       
                       
                   
                       <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Top up Fee")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind10,( sv.taxind10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
                       </div> </div></div>

 </div>
 
 
 
 <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Switch Fee")%></label>
  <div class="form-group" style="width:50;"> 
 
<%if(((BaseScreenData)sv.taxind11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind11,( sv.taxind11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
 </div></div></div>
 
 </div>
 <!-- ALS-4706 -->
 <div class="col-md-4"> 
			    	     <div class="form-group">
			    	     <%
					if ((new Byte((sv.taxind13).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Management Fee")%></label>
  <div class="form-group" style="width:50;"> 

<%if(((BaseScreenData)sv.taxind13) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind13,( sv.taxind13.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind13) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
<%}%>
<%}%>
                       </div> </div></div>
 
 
 
  <div class="row">


     <div class="col-md-4"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Tax Rate Item")%></label>
  <div class="form-group" style="width:70;"> 
<%if(((BaseScreenData)sv.txitem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.txitem,( sv.txitem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.txitem) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.txitem, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
 </div></div></div>
 
 </div>
 


</div></div>








<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
