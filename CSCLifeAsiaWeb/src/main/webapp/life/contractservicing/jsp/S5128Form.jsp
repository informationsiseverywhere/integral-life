<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5128";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5128ScreenVars sv = (S5128ScreenVars) fw.getVariables();%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<!-- ILIFE-1403 START by nnazeer -->
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan  ");%>
	<!-- ILIFE-1403 END -->
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select?");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandatory");%>
<%		appVars.rollup(new int[] {93});
%>

<script type="text/javascript">
function perFormOperationForContinue(act){ 
	if(selectedRow1!=null && selectedRow1!=""){
		document.getElementById(selectedRow1).value=act;		
	}else{
		var elementSelected=false;		
		for (var index = 0; index < idRowArray.length; ++index) {
			var item = idRowArray[index];
			if(item!=null && item!=""){
				document.getElementById(item).value=act;
				elementSelected=true;
			}
		}
		//if(elementSelected){
		//	doAction('PFKEY0'); 
		//}
	}
}
</script>
<%-- MIBT-97 STARTS --%>
<script>

function selectedRowChecIdVAlue(idt1,idt2,count,idValue)
{
	count=count-1;
	if(document.getElementById(idt2).checked){
		document.getElementById(idt1).value=idValue;
		idRowArray[count]=idt1;
	}else{
		document.getElementById(idt1).value=' ';
		idRowArray[count]="";
	}
}

function selectedRowIdValue(idt,idValue){

	//document.getElementById(idt).value='1';
	document.getElementById(idt).selected=true;
	document.getElementById(idt).value=idValue;
	firstTimeOver=true;
	selectedRow1=idt;
}
</script>

<style>
.panel{
height: 550px !important;
}
</style>

<div class="panel panel-default">
 <div class="panel-body">   
 	<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">
 <label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</label>
<!-- <div class="input-group"> -->
<table><tr><td>
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>	
		</td><td>	
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="margin-left: 1px;"  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  id="cntdesc" style="margin-left:1px;max-width:155px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
</div>


</div>
 	
   	<div class="col-md-4"> 
   	<div class="form-group">
    	
				<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
				</label>
				<div class="input-group">
				
		<%			
				fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("register");
			longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:900px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</div>		

</div>
</div>
</div>

<div class="row">	
   	<div class="col-md-4"> 
    		<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
</label>
	<div class="input-group">
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
</div>
</div>

	
   	<div class="col-md-4"> 
    		<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
</label>
  		<div class="input-group">
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:150px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
</div>
</div>
       
<!-- ILIFE-1403 START by nnazeer -->
   	<div class="col-md-4"> 
    		<div class="form-group">
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
</label>
<%}%>
<div class="input-group">

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>
</div>
</div>



<!-- <td width='251'></td><td width='251'></td></tr>-->
<!-- ILIFE-1403 END -->

<div class="row">	
   	<div class="col-md-4"> 
   	<div class="form-group">
    	
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
</label>
<table><tr><td>
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>





	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="margin-left: 1px;max-width: 170px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
  
</div></div>


	    	<div class="col-md-4"> 
		    		<div class="form-group">
					 <label>
					<%=resourceBundleHandler.gettingValueFromBundle("Joint life")%>
					</label>
					
					<table><tr><td>
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="jlife" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  id="jlifcnum"  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
  
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="jlinsname" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;margin-left: 2px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
</div></div>



	    	<div class="col-md-4"> 
		    		<div class="form-group">
					 <label>
					<%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%>
					</label>
					<table><tr><td>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
			
			if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div style="max-width: 30px !important;" id="numpols" class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div style="max-width: 30px !important;" id="numpols" class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</td><td style="width:90px;">

	
  		
		<%					
		if(!((sv.entity.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.entity.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.entity.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="entity" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
</div></div></div>

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[5];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.crcode.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
			} else {		
				calculatedValue = (sv.crcode.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.rtable.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
			} else {		
				calculatedValue = (sv.rtable.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.longdesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
			} else {		
				calculatedValue = (sv.longdesc.getFormData()).length()*13;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.hrequired.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
			} else {		
				calculatedValue = (sv.hrequired.getFormData()).length()*25;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			%>
		<%
		int hTable = 160;
		GeneralTable sfl = fw.getTable("s5128screensfl");
		int height;
		if(sfl.count()*27 >395) {
		height = 395 ;
		} else {
		height = sfl.count()*27;
		}	
		%>


<div class="row">
				<div class="col-md-12">
					
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id='dataTables-s5128' width='100%'>
              <thead  class="fixedThead">
				<tr class='info'>
		
					<th style="width:50px"><center>   <%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
		         								
					<th style="width:100px"><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
	
					<th style="width:150px"><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
					
					<th style="width:400px"><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
					
					<th style="width:420px"><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
                </tr>
             </thead>
             <tbody  class="scrollTbody">
		
<!-- ILIFE-2721 Life Cross Browser - Sprint 4 D4 : Task 2  fake container ends-->		
		
	<%
	S5128screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5128screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    									<td class="tableDataTag tableDataTagFixed" style="width:50px;" align="left">														
										<%if(("+".equalsIgnoreCase(sv.select.getFormData()))||("X".equalsIgnoreCase(sv.select.getFormData())))
							{ %>								
													
					
					 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5128screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5128screensfl.select_R<%=count%>'
						 id='s5128screensfl.select_R<%=count%>'
						 <%--MIBT-94 --%>
						 onClick="selectedRowIdValue('s5128screensfl.select_R<%=count%>','<%= sv.select.getFormData()%>')"
						 class="UICheck" disabled
					 />
					 
					 <%}else{ %>
											 
					 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5128screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5128screensfl.select_R<%=count%>'
						 id='s5128screensfl.select_R<%=count%>'
						  <%--MIBT-94 --%>
						 onClick="selectedRowChecIdVAlue('s5128screensfl.select_R<%=count%>', 's5128screensfl.select_R<%=count%>', '<%=count%>','X')"
						 class="UICheck" 
					 />
					 
					 
					 	<%} %>					
					
											
									</td>
				    									<td class="tableDataTag" style="width:104px;" align="left">									
																
									
											
						<%= sv.crcode.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:157px;" align="left">									
																
									
											
						<%= sv.rtable.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:421px;" align="left">									
																
									
											
						<%= sv.longdesc.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:430px;" align="left">									
																
									
											
						<%= sv.hrequired.getFormData()%>
						
														 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5128screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	                            </tbody>
	                     </table>
	                 </div>
	         </div>
	     </div>
	  
	   <table>
	   <tr>
		<td>
		
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation('X')" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></a>
			</div>
		
		</td>
		</tr>
		</table>
			
			
			
			
			  
	     <div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("-")%>
</div>

</tr></table></div>
	   
	
	
</div>
</div>




<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
		$('#dataTables-s5128').DataTable({
			ordering : false,
			searching : false,
			scrollY: "236px",
			scrollCollapse: true,
			scrollX:true,
			paging: false,
			info: false
		});
	});
</script>

	


<%@ include file="/POLACommon2NEW.jsp"%>

