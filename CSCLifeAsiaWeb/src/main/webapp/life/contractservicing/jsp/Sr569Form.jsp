

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR569";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sr569ScreenVars sv = (Sr569ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Component Selection");
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"U/W Date        ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policies in Plan  ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sel  Component   Description                            Risk Stat  Prem Stat");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind04.isOn()) {
			sv.chdrstatus.setReverse(BaseScreenData.REVERSED);
			sv.chdrstatus.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrstatus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.premstatus.setReverse(BaseScreenData.REVERSED);
			sv.premstatus.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.premstatus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.entity.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind99.isOn() && !appVars.ind89.isOn()) {
			sv.textfield.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.huwdcdteDisp.setEnabled(BaseScreenData.DISABLED);
		}
		//TSD-266 start
		if (appVars.ind08.isOn()) {
			sv.lifenum.setReverse(BaseScreenData.REVERSED);
			sv.lifenum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.lifenum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		//TSD-266 end
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table >
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left:1px;">
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left:1px;">
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:300px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div style="width: 120px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());

							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<div style="width: 120px;">
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div style="width: 120px;">
						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table><tr><td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:150px;">

						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<table>
						<tr>
							<td style="min-width:71px;">
								<%
									if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="min-width:71px;padding-left:1px;">
								<%
									if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' 
									>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
					<table><tr><td>
						<%
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.numpols);

							if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:150px;">

						<%
							if (!((sv.entity.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.entity.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.entity.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("U/W Date")%></label>
					<div class="input-group">
					<% if ((new Byte((sv.huwdcdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.huwdcdteDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="huwdcdteDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.huwdcdteDisp, (sv.huwdcdteDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
					
					
					
					</div>
					
					
					
				</div>
			</div>
		</div>
		<br />

		<%
			GeneralTable sfl = fw.getTable("sr569screensfl");
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr569' width='100%'>
							<thead>
								<tr class='info'>
									<th><center> <%=resourceBundleHandler.gettingValueFromBundle("    ")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr569screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr569screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
									<%
										if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 60px; padding: 1px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd; z-index: 10; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft);"
										<%if ((sv.asterisk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="center" <%}%>><%=sv.asterisk.getFormData()%>&nbsp;



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 60px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd; z-index: 8; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft);">
										&nbsp;</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd; z-index: 8; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft);"
										align="center">
										<%--start ILIFE-1036 --%> <%--if(count>1){ --%> <%
 	if (!(sv.hcoverage.toString().trim().equals("")) && !(sv.hrider.toString().trim().equals(""))) {
 %>
										<%--end ILIFE-1036 --%> <!-- ILIFE-1051 start kpalani6 --> <%
 	if (!appVars.ind03.isOn()) {
 %>

										<input type="checkbox" value='<%=sv.select.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("sr569screensfl" + "." +
						 "select")'
										onKeyUp='return checkMaxLength(this)'
										name='sr569screensfl.select_R<%=count%>'
										id='sr569screensfl.select_R<%=count%>'
										onClick="selectedRow('sr569screensfl.select_R<%=count%>')"
										class="UICheck" /> <%
 	}
 %> <!-- ILIFE-1051 end kpalani6 -->
										<%
											}
										%>
									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd; z-index: 8; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft);">

									</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.cmpntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
										<%if ((sv.cmpntnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.cmpntnum.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;">

									</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.component).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
										<%if ((sv.component).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.component.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 80px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;">

									</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.deit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 200px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
										<%if ((sv.deit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.deit.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 200px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;">

									</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 100px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
										<%if ((sv.statcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.statcode.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 100px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;">

									</td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.pstatcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag"
										style="width: 100px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
										<%if ((sv.pstatcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.pstatcode.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag"
										style="width: 100px; border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;">

									</td>

									<%
										}
									%>

								</tr>

								<%
									count = count + 1;
										Sr569screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
        var showval= document.getElementById('show_lbl').value;
        var toval= document.getElementById('to_lbl').value;
        var ofval= document.getElementById('of_lbl').value;
        var entriesval= document.getElementById('entries_lbl').value;
        var nextval= document.getElementById('nxtbtn_lbl').value;
        var previousval= document.getElementById('prebtn_lbl').value;
        var dtmessage =  document.getElementById('msg_lbl').value;
		$('#dataTables-sr569').DataTable({
			ordering: false,
	    	searching:false,
	    	scrollX: true,
	    	scrollCollapse:true,
	    	scrollY: "580px",
	    	autoWidth: false,
	    	columnDefs: [
	    	  {	width:'40px', targets:[0,1]},
	    	  {	width:'100px', targets:[2,3,5,6]},
	    	],
	    	stateSave: true,
            language: {
                "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
                "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
                "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
                "sEmptyTable": dtmessage,
                "paginate": {
                    "next":       nextval,
                    "previous":   previousval
                }
            }
	   	});
		table.state.clear();
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label></label>
				<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

						if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Component Selection")%></label>
				<div style="width: 150px;">
					<%
						if (!((sv.textfield.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.textfield.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.textfield.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
	</div>
</div>