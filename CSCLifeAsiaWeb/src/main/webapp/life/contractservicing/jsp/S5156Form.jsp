

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5156";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5156ScreenVars sv = (S5156ScreenVars) fw.getVariables();%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Plan  Coverage Risk Status              Coverage Premium Status");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind04.isOn()) {
			sv.sfcdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.confirm.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>




<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>

<table><tr><td>




	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="min-width:2px">
	</td><td style="min-width:40px">





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:40px; max-width:50px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="min-width:2px">
	</td><td>





	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="cntdesc" style="max-width:160px;min-width:120px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
  
	</div></div>
	
	<div class="col-md-4"> 
				    		<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%}%>



<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div></div>
	

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">



<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>




	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>


<div class="input-group">

	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div>
</div></div>
</div>

<div class="row">
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
<table><tr><td>
	
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:70px;min-width:70px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	</td><td style="min-width:2px">
	</td><td style="min-width:120px">



	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:120px;min-width:120px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>


</div></div></div>
<div class="row">
			    	<div class="col-md-4"> 
				    		<div class="form-group">



<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
<table><tr><td>




	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:80px;min-width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	</td><td style="min-width:2px">
	</td><td style="min-width:120px">





	
  		
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:120px;min-width:120px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td></tr></table>

</div></div></div>



</td></tr></table><br/>

<!--<table><tr style= 'hieght:22px'><td width='600'>

<%if ((new Byte((sv.sfcdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.sfcdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sfcdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sfcdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			
				<div id='sfcdesc' class='label_txt'class='label_txt'  onHelp='return fieldHelp("sfcdesc")'><%=sv.sfcdesc.getFormData() %></div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	



<br/>


	

</td>



</tr></table>
-->
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5156' width='100%'>
							<thead>
								<tr class='info'>
									<th> <center>  <%=resourceBundleHandler.gettingValueFromBundle("Plan")%></center></th>
									<th> <center>  <%=resourceBundleHandler.gettingValueFromBundle("Coverage Risk Status")%></center></th>
									<th>  <center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage Premium Status")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
                                    
								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s5156screensfl");
									
									S5156screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S5156screensfl.hasMoreScreenRows(sfl)) {
								%>

									<tr id='<%="tablerow"+count%>' height='22px'>
						    										
													
					<div style='display:none; visiblity:hidden;'>
						 						 <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5156screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5156screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s5156screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%> px;"
						  >
					</div>
				    	<td align="left">														
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5156screensfl" + "." +
					      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.planSuffix.getFormData()%></span></a>							 						 		
						</td>
				    	<td align="left">									
						<%= sv.rstatcode.getFormData()%>
						</td>
				    	<td align="left">									
						<%= sv.rstatdesc.getFormData()%>
						</td>
				    	<td align="left">									
						<%= sv.pstatcode.getFormData()%>
						</td>
				    	<td align="left">									
						<%= sv.pstatdesc.getFormData()%>
						</td>
					
	</tr>

	<%
	count = count + 1;
	S5156screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


							</tbody>

						</table>
						
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
		$('#dataTables-s5156').DataTable({
			ordering : false,
			searching : false,
			paging: false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
		});
		$(".dataTables_info").hide();
	});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

