

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5541";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5541ScreenVars sv = (S5541ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loading Factor");%>

<%{
		if (appVars.ind21.isOn()) {
			sv.lfact01.setReverse(BaseScreenData.REVERSED);
			sv.lfact01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.lfact01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.lfact02.setReverse(BaseScreenData.REVERSED);
			sv.lfact02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.lfact02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.lfact03.setReverse(BaseScreenData.REVERSED);
			sv.lfact03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.lfact03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.lfact04.setReverse(BaseScreenData.REVERSED);
			sv.lfact04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.lfact04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.lfact05.setReverse(BaseScreenData.REVERSED);
			sv.lfact05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.lfact05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.lfact06.setReverse(BaseScreenData.REVERSED);
			sv.lfact06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.lfact06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.lfact07.setReverse(BaseScreenData.REVERSED);
			sv.lfact07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.lfact07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.lfact08.setReverse(BaseScreenData.REVERSED);
			sv.lfact08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.lfact08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.lfact09.setReverse(BaseScreenData.REVERSED);
			sv.lfact09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.lfact09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.lfact10.setReverse(BaseScreenData.REVERSED);
			sv.lfact10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.lfact10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.lfact11.setReverse(BaseScreenData.REVERSED);
			sv.lfact11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.lfact11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.lfact12.setReverse(BaseScreenData.REVERSED);
			sv.lfact12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.lfact12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.freqcy01.setReverse(BaseScreenData.REVERSED);
			sv.freqcy01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.freqcy01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.freqcy02.setReverse(BaseScreenData.REVERSED);
			sv.freqcy02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.freqcy02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.freqcy03.setReverse(BaseScreenData.REVERSED);
			sv.freqcy03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.freqcy03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.freqcy04.setReverse(BaseScreenData.REVERSED);
			sv.freqcy04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.freqcy04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.freqcy05.setReverse(BaseScreenData.REVERSED);
			sv.freqcy05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.freqcy05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.freqcy06.setReverse(BaseScreenData.REVERSED);
			sv.freqcy06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.freqcy06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.freqcy07.setReverse(BaseScreenData.REVERSED);
			sv.freqcy07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.freqcy07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.freqcy08.setReverse(BaseScreenData.REVERSED);
			sv.freqcy08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.freqcy08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.freqcy09.setReverse(BaseScreenData.REVERSED);
			sv.freqcy09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.freqcy09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.freqcy10.setReverse(BaseScreenData.REVERSED);
			sv.freqcy10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.freqcy10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.freqcy11.setReverse(BaseScreenData.REVERSED);
			sv.freqcy11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.freqcy11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.freqcy12.setReverse(BaseScreenData.REVERSED);
			sv.freqcy12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.freqcy12.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>




	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
<!-- <div class="col-md-3"></div> -->
<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>





	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
<!-- <div class="col-md-1"></div> -->
<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<!-- <div class="input-group"> -->
<table><tr><td>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
	

<!-- </div> --></div></div></div>
<div class="row">

<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
</div>
  


<div class="row">

<div class="col-md-4">


<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>

</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


<label><%=resourceBundleHandler.gettingValueFromBundle("Loading Factor")%></label>
</div>
<div class="col-md-4"> </div>
</div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy01");
	optionValue = makeDropDownList( mappedItems , sv.freqcy01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy01' type='list' style="width:140px;" id='freqcy01'
<% 
	if((new Byte((sv.freqcy01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact01' 
type='text'

<%if((sv.lfact01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact01) %>'
	 <%}%>

size='<%= sv.lfact01.getLength()%>'
maxLength='<%= sv.lfact01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact01).getColor()== null  ? 
			"input_cell" :  (sv.lfact01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div>
<div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy02");
	optionValue = makeDropDownList( mappedItems , sv.freqcy02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy02' type='list' style="width:140px;" id='freqcy02'
<% 
	if((new Byte((sv.freqcy02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">

	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact02' 
type='text'

<%if((sv.lfact02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact02) %>'
	 <%}%>

size='<%= sv.lfact02.getLength()%>'
maxLength='<%= sv.lfact02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact02).getColor()== null  ? 
			"input_cell" :  (sv.lfact02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div><div class="col-md-4"> </div></div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy03");
	optionValue = makeDropDownList( mappedItems , sv.freqcy03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy03' type='list' style="width:140px;" id='freqcy03'
<% 
	if((new Byte((sv.freqcy03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact03' 
type='text'

<%if((sv.lfact03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact03) %>'
	 <%}%>

size='<%= sv.lfact03.getLength()%>'
maxLength='<%= sv.lfact03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact03).getColor()== null  ? 
			"input_cell" :  (sv.lfact03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div><div class="col-md-4"> </div></div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy04");
	optionValue = makeDropDownList( mappedItems , sv.freqcy04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy04' type='list' style="width:140px;" id='freqcy04'
<% 
	if((new Byte((sv.freqcy04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact04' 
type='text'

<%if((sv.lfact04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact04) %>'
	 <%}%>

size='<%= sv.lfact04.getLength()%>'
maxLength='<%= sv.lfact04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact04).getColor()== null  ? 
			"input_cell" :  (sv.lfact04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div><div class="col-md-4"> </div></div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy05");
	optionValue = makeDropDownList( mappedItems , sv.freqcy05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy05' type='list' style="width:140px;" id='freqcy05'
<% 
	if((new Byte((sv.freqcy05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact05' 
type='text'

<%if((sv.lfact05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact05) %>'
	 <%}%>

size='<%= sv.lfact05.getLength()%>'
maxLength='<%= sv.lfact05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact05).getColor()== null  ? 
			"input_cell" :  (sv.lfact05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</div><div class="col-md-4"> </div></div>
<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy06");
	optionValue = makeDropDownList( mappedItems , sv.freqcy06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy06' type='list' style="width:140px;" id='freqcy06'
<% 
	if((new Byte((sv.freqcy06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">

	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact06' 
type='text'

<%if((sv.lfact06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact06) %>'
	 <%}%>

size='<%= sv.lfact06.getLength()%>'
maxLength='<%= sv.lfact06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact06).getColor()== null  ? 
			"input_cell" :  (sv.lfact06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div><div class="col-md-4"> </div></div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy07");
	optionValue = makeDropDownList( mappedItems , sv.freqcy07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy07' type='list' style="width:140px;" id='freqcy07'
<% 
	if((new Byte((sv.freqcy07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">

	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact07' 
type='text'

<%if((sv.lfact07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact07) %>'
	 <%}%>

size='<%= sv.lfact07.getLength()%>'
maxLength='<%= sv.lfact07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact07).getColor()== null  ? 
			"input_cell" :  (sv.lfact07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div><div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy08");
	optionValue = makeDropDownList( mappedItems , sv.freqcy08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy08' type='list' style="width:140px;" id='freqcy08'
<% 
	if((new Byte((sv.freqcy08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>


<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact08' 
type='text'

<%if((sv.lfact08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact08) %>'
	 <%}%>

size='<%= sv.lfact08.getLength()%>'
maxLength='<%= sv.lfact08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact08).getColor()== null  ? 
			"input_cell" :  (sv.lfact08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div><div class="col-md-4"> </div></div>

<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy09");
	optionValue = makeDropDownList( mappedItems , sv.freqcy09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy09' type='list' style="width:140px;" id='freqcy09'
<% 
	if((new Byte((sv.freqcy09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>

<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact09' 
type='text'

<%if((sv.lfact09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact09) %>'
	 <%}%>

size='<%= sv.lfact09.getLength()%>'
maxLength='<%= sv.lfact09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact09).getColor()== null  ? 
			"input_cell" :  (sv.lfact09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div><div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy10");
	optionValue = makeDropDownList( mappedItems , sv.freqcy10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy10' type='list' style="width:140px;" id='freqcy10'
<% 
	if((new Byte((sv.freqcy10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>


<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact10' 
type='text'

<%if((sv.lfact10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact10) %>'
	 <%}%>

size='<%= sv.lfact10.getLength()%>'
maxLength='<%= sv.lfact10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact10).getColor()== null  ? 
			"input_cell" :  (sv.lfact10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div><div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy11");
	optionValue = makeDropDownList( mappedItems , sv.freqcy11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy11' type='list' style="width:140px;" id='freqcy11'
<% 
	if((new Byte((sv.freqcy11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>


<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact11' 
type='text'

<%if((sv.lfact11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact11) %>'
	 <%}%>

size='<%= sv.lfact11.getLength()%>'
maxLength='<%= sv.lfact11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact11)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact11).getColor()== null  ? 
			"input_cell" :  (sv.lfact11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div><div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy12");
	optionValue = makeDropDownList( mappedItems , sv.freqcy12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy12' type='list' style="width:140px;" id='freqcy12'
<% 
	if((new Byte((sv.freqcy12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>


<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">


	<%	
			qpsf = fw.getFieldXMLDef((sv.lfact12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lfact12' 
type='text'

<%if((sv.lfact12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lfact12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lfact12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lfact12) %>'
	 <%}%>

size='<%= sv.lfact12.getLength()%>'
maxLength='<%= sv.lfact12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lfact12)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lfact12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lfact12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lfact12).getColor()== null  ? 
			"input_cell" :  (sv.lfact12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div><div class="col-md-4"> </div>
<!-- </tr></table><br/> -->
<script language="javascript">
$(document).ready(function(){
	createDropdownNotInTable("freqcy01",7);
	createDropdownNotInTable("freqcy02",7);
	createDropdownNotInTable("freqcy03",7);
	createDropdownNotInTable("freqcy04",7);
	createDropdownNotInTable("freqcy05",7);
	createDropdownNotInTable("freqcy06",7);
	createDropdownNotInTable("freqcy07",7);
	createDropdownNotInTable("freqcy08",7);
	createDropdownNotInTable("freqcy09",7);
	createDropdownNotInTable("freqcy10",7);
	createDropdownNotInTable("freqcy11",7);
	createDropdownNotInTable("freqcy12",7);
});
</script>
</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>

