
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR519";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%
	appVars.rollup();
	appVars.rolldown();
	
%>
<%Sr519ScreenVars sv = (Sr519ScreenVars) fw.getVariables();%>


	<%Sr519screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract number ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life no ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%sv.lifesel.setClassString("");%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex ");%>
	<%sv.sex.setClassString("");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date of birth ");%>
	<%sv.dobDisp.setClassString("");%>
	<%sv.dummy.setClassString("");%>
<%	sv.dummy.appendClassString("string_fld");
	sv.dummy.appendClassString("output_txt");
	sv.dummy.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age admitted");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age at RCD ");%>
	<%sv.anbage.setClassString("");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Medical evidence ");%>
	<%sv.selection.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%sv.smoking.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation ");%>
	<%sv.occup.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit code ");%>
	<%sv.pursuit01.setClassString("");%>
	<%sv.pursuit02.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Relationship ");%>
	<%sv.relation.setClassString("");%>
	<%sv.rollit.setClassString("");%>
<%	sv.rollit.appendClassString("string_fld");
	sv.rollit.appendClassString("output_txt");
	sv.rollit.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Use roll keys to display joint life details");%>
<%	generatedText15.appendClassString("label_txt");
	generatedText15.appendClassString("information_txt");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%if (sv.Sr519screenWritten.gt(0)) {
		if (appVars.ind51.isOn()) {
			generatedText4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind51.isOn()) {
			sv.jlife.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.lifesel.setReverse(BaseScreenData.REVERSED);
			sv.lifesel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.lifesel, appVars.ind50);
		if (!appVars.ind01.isOn()) {
			sv.lifesel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.sex.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.sex.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.sex.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.sex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.dobDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.dobDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.dobDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.dobDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.dummy.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.anbage.setReverse(BaseScreenData.REVERSED);
			sv.anbage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.anbage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.selection.setReverse(BaseScreenData.REVERSED);
			sv.selection.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.selection.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.smoking.setReverse(BaseScreenData.REVERSED);
			sv.smoking.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.smoking.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.occup.setReverse(BaseScreenData.REVERSED);
			sv.occup.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.occup.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.pursuit01.setReverse(BaseScreenData.REVERSED);
			sv.pursuit01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.pursuit01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.pursuit02.setReverse(BaseScreenData.REVERSED);
			sv.pursuit02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.pursuit02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.relation.setReverse(BaseScreenData.REVERSED);
			sv.relation.setColor(BaseScreenData.RED);
		}
		if (appVars.ind52.isOn()) {
			sv.relation.setInvisibility(BaseScreenData.INVISIBLE);
			sv.relation.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.relation.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.rollit.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind53.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
<!-- ILIFE-2730 Life Cross Browser -Coding and UT- Sprint 4 D1: Task 7 starts -->
<style>
@media \0screen\,screen\9
{.iconpos{margin-bottom:1px;}
 .blank_cell{margin-top:1px;}
}
</style>
<!-- ILIFE-2730 Life Cross Browser -Coding and UT- Sprint 4 D1: Task 7 starts -->





<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					    		     <div class="input-group">
						    		
<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
								<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						</div>
				   </div>		
			
			    	
		    </div>
				   
				
				   <div class="row">	
			    	<div class="col-md-6" >
				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					    		
					    		<table><tr><td> <div class="form-group" >
					    		  <div class="input-group"  style="width:120px;">
						    		<input name='lifesel' id='lifesel'
type='text' 
value='<%=sv.lifesel.getFormData()%>' 
maxLength='<%=sv.lifesel.getLength()%>' 
size='<%=sv.lifesel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lifesel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lifesel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){  
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lifesel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('lifesel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.lifesel).getColor()== null  ? 
"input_cell" :  (sv.lifesel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
  <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('lifesel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>


<%} %>


	</div>  </div> </td><td>
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell1" %>'style="min-width:100px;max-width:300px;">   <!--  ILIFE-2730 changed by pmujavadiya-->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      		</td></tr></table>	   
				    		</div>
					
				    		
				    		
				    
		    </div>
		
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
					    		     <div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sex"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sex");
	optionValue = makeDropDownList( mappedItems , sv.sex.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sex.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.sex).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.sex).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:142px;"> 
<%
} 
%>

<select name='sex' type='list' style="width:140px;"
<% 
	if((new Byte((sv.sex).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){  
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.sex).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.sex).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Date of birth")%></label>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="cltdobxDisp" data-link-format="dd/mm/yyyy" style="max-width:200px;">
			                    
								
			             	
							<input name='dobDisp' 
type='text' 
value='<%=sv.dobDisp.getFormData()%>' 
maxLength='<%=sv.dobDisp.getLength()%>' 
size='<%=sv.dobDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(dobDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.dobDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.dobDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('dobDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<%
	}else { 
%>

class = ' <%=(sv.dobDisp).getColor()== null  ? 
"input_cell" :  (sv.dobDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('dobDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<%} %>


	
						</div>
				   </div>	</div>	
			
			    	
		    </div>
		
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Medical evidence")%></label>
					    		     <div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"selection"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("selection");
	optionValue = makeDropDownList( mappedItems , sv.selection.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.selection.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.selection).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
 <% if("red".equals((sv.selection).getColor())){
%>
<div style="border:1px; border-color: #B55050;  width:202px;"> 
<%
} 
%>

<select name='selection' type='list' style="width:220px;"
<% 
	if((new Byte((sv.selection).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.selection).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.selection).getColor()== null  ? 
"input_cell" :  (sv.selection).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.selection).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Age at RCD")%></label>
							
									 <div class="input-group"  style="max-width:100px;">
							<%	
			qpsf = fw.getFieldXMLDef((sv.anbage).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='anbage' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.anbage) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.anbage);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.anbage) %>'
	 <%}%>

size='<%= sv.anbage.getLength()%>'
maxLength='<%= sv.anbage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(anbage)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.anbage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){  
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.anbage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.anbage).getColor()== null  ? 
			"input_cell" :  (sv.anbage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div></div>
				   </div>		
			
			    	
		    </div>
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
					    		     <div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"smoking"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("smoking");
	optionValue = makeDropDownList( mappedItems , sv.smoking.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.smoking.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.smoking).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.smoking).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:142px;"> 
<%
} 
%>

<select name='smoking' type='list' style="width:140px;"
<% 
	if((new Byte((sv.smoking).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.smoking).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.smoking).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
							<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"occup"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("occup");
	optionValue = makeDropDownList( mappedItems , sv.occup.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.occup.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.occup).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.occup).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='occup' type='list' style="width:240px;"
<% 
	if((new Byte((sv.occup).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.occup).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.occup).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
						</div>
				   </div>		
			
			    	
		    </div>
	
				   	<div class="row">	
			    	<div class="col-md-8"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit code")%></label>
					    		    <table><tr><td>
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"pursuit01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("pursuit01");
	optionValue = makeDropDownList( mappedItems , sv.pursuit01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.pursuit01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.pursuit01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.pursuit01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:172px;"> 
<%
} 
%>

<select name='pursuit01' type='list' style="width:175px;"
<% 
	if((new Byte((sv.pursuit01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.pursuit01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.pursuit01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td style="padding-left:1px;">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"pursuit02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("pursuit02");
	optionValue = makeDropDownList( mappedItems , sv.pursuit02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.pursuit02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.pursuit02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.pursuit02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:172px;"> 
<%
} 
%>

<select name='pursuit02' type='list' style="width:175px;"
<% 
	if((new Byte((sv.pursuit02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.pursuit02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.pursuit02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr></table>
				      			     
				    		</div>
					</div></div>
					
				

<%@ include file="/POLACommon2NEW.jsp"%>
