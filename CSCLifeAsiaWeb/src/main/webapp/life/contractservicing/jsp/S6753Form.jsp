<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6753";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6753ScreenVars sv = (S6753ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind10.isOn()) {
	sv.hflag01.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind11.isOn()) {
	sv.hflag02.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>




<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
    	 					<!-- <div class="input-group" style="max-width:100px;"> -->
    	 					<table><tr><td>

<%if ((new Byte((sv.chdrsel).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="max-width:80px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:400px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
<!-- </div> --></div></div>



<!-- <div class="col-md-1"> </div> -->

<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<div class="form-group" style="width:100px">
<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>


<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>

<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div>
</div>






 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 					
    	 					
    	 				</div></div>
    	 					
    	 	
    	 	<!-- <div class="col-md-1"> </div> -->
    	 	
    	 	
    	 	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>	
    	 					<div class="form-group" style="width:60px">			
    	 					<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>

<div class="col-md-4"></div>
    	 					
</div>   


<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
			    	     <!-- <div class="input-group"style="max-width:100px;"> -->
			    	     <table><tr><td>
    	 					
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:500px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.hflag02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.hflag02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.hflag02.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hflag02.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="max-width:30px;" >
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

 	 	</td></tr></table>				
<!-- </div> --></div></div><div class="col-md-3"></div><!-- </div>




<div class="row"> -->
<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
    	 					<!-- <div class="input-group"> -->
    	 					<table><tr><td>
<%if ((new Byte((sv.jownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:500px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
<!-- </div> --></div>

</div><div class="col-md-4"></div></div>




<div class="row">	
			    	<div class="col-md-12"> 
   <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-s6753'>
               <thead>
		
			        <tr class='info'>
			        
					<th>Sel</th>
                    <th>Seq.No</th>
                    <th>Date</th>
                    <th>Code</th>
                    <th>Description</th>
			     
		 	        
		 	        </tr>
			 </thead>
			 
			 
			 
		<%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=108;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=120;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=204;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=108;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=408;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("s6753screensfl");
GeneralTable sfl1 = fw.getTable("s6753screensfl");
S6753screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 270;
}	
%>
		
		
		
		
			 
			 
	<%
S6753screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean delVisibile = true;
boolean hyperLinkFlag;
while (S6753screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
{
if (appVars.ind02.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
	delVisibile = false;
}
if (appVars.ind04.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
}
}
%>
		



			 <tbody>


<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s6753screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s6753screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="s6753screensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >


<td >
	<%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
			 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s6753screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s6753screensfl.select_R<%=count%>'
						 id='s6753screensfl.select_R<%=count%>'
						 onClick="selectedRow('s6753screensfl.select_R<%=count%>')"
						 class="UICheck"
							 <% 
								if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							 %>
							disabled="disabled"
							<%}%>
							<%if(!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0") || (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
							checked="checked"
							<%}%>
					 />
					 					
					
	<%}%>
</td>




<td >									
	<%if((new Byte((sv.prcSeqNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.prcSeqNbr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
			<%if((new Byte((sv.prcSeqNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%if(hyperLinkFlag){%>
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6753screensfl" + "." +
					      "slt" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.prcSeqNbr.getFormData()%></span></a>							 						 		
						  <%}else{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.prcSeqNbr.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>



<td style="width:160px">				<%if((new Byte((sv.efdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	<%	longValue = sv.efdateDisp.getFormData();  	%>
					<% 
						if((new Byte((sv.efdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||
							(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div id="s6753screensfl.efdateDisp_R<%=count%>" name="s6753screensfl.efdateDisp_R<%=count%>" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "input_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>" data-link-format="dd/mm/yyyy">
					
					<input name='<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'
					id='<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.efdateDisp.getFormData() %>' 
					class = " <%=(sv.efdateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.efdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.efdateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(s6753screensfl.efdateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.efdateDisp.getLength()*12%> px;"
					>
					
					
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					<%-- <a href="javascript:;" 
					onClick="showCalendar(this, document.getElementById('<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
					 
					 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0">
					</a>--%>
					<%}%>  			
		<%}%>
</td>



<td >									
				<%if((new Byte((sv.trancd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.trancd.getFormData();
							%>
					 		<div id="s6753screensfl.trancd_R<%=count%>" name="s6753screensfl.trancd_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td >									
				<%if((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.descrip.getFormData();
							%>
					 		<div id="s6753screensfl.descrip_R<%=count%>" name="s6753screensfl.descrip_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



</tr>
<%	count = count + 1;
S6753screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>


</tbody></table></div></div>






</div>


<div class="row">
  
                  <div class="col-md-4">
        			<div class="form-group">
						<a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(9)"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
       
        			  
        			</div>
        		</div>

</div>


</div></div>



































































































<%-- 










<!-- ILIFE-2727 Life Cross Browser - Sprint 4 D1 : Task 5 -->
<style>
@media \0screen\,screen\9
{

.blank_cell{margin-left:1px}
.sectionbutton{margin-top:435px; margin-left:23px;}

}
@media all and (-ms-high-contrast:none) 
{
	.sectionbutton{margin-top:415px; margin-left:21px;}
		
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
	
	.sectionbutton{margin-top:440px; margin-left:25px;}	
}

</style>
<!-- ILIFE-2727 Life Cross Browser - Sprint 4 D1 : Task 5 -->
<div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData CHDRSEL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
<%=smartHF.getLit(0, 0, CHDRSEL_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</div>
<br/>
<%if ((new Byte((sv.chdrsel).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData CNTCURR_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
<%=smartHF.getLit(0, 0, CNTCURR_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
</div>
<br/>
<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData CHDRSTATUS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status");%>
<%=smartHF.getLit(0, 0, CHDRSTATUS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
</div>
<br/>
<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData PREMSTATUS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status");%>
<%=smartHF.getLit(0, 0, PREMSTATUS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
</div>
<br/>
<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData REGISTER_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register");%>
<%=smartHF.getLit(0, 0, REGISTER_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
</div>
<br/>
<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData LIFENUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured");%>
<%=smartHF.getLit(0, 0, LIFENUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
</div>
<br/>
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.hflag02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.hflag02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.hflag02.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hflag02.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData JOWNNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life");%>
<%=smartHF.getLit(0, 0, JOWNNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%>
</div>
<br/>
<%if ((new Byte((sv.jownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
</tr> </table>

<br/>


<%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=108;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=120;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=204;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=108;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=408;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("s6753screensfl");
GeneralTable sfl1 = fw.getTable("s6753screensfl");
S6753screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 270;
}	
%>
<!-- ILIFE-2727  Life Cross Browser -Coding and UT- Sprint 4 D1: Task 5 Start by snayeni --> 
<style type="text/css">
.fakeContainer {
	width:720px;	
	height:270px; 
	top:160px;
	left:4px;	
}
</style>
<script language="javascript">
		$(document).ready(function(){
			var rows = <%=sfl1.count()+1%>;
			var isPageDown = 1;
			var pageSize = 1;
			var headerRowCount=1;
			var fields = new Array();
			var colWidth = new Array();
			var j=0;
			<% for(int i=0;i<arraySize;i++){	%>
				colWidth[j]=<%=tblColumnWidth[i]%>;
				j=j+1;
			<%}%>
	<%if(false){%>	
		operateTableForSuperTable(rows,isPageDown,pageSize,fields,"s6753Table",null,headerRowCount);
	<%}%>
			new superTable("s6753Table", {
				fixedCols : 0,					
				colWidths : [50,150,170,133,200],
				<%if(false){%>	
					headerRows :headerRowCount,		
					addRemoveBtn:"Y",
					moreBtn: "Y",	/*ILIFE-2143*/
					moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				<%}%>
				hasHorizonScroll: "Y"
			
				
			});
		});
		
	</script>
	<div style="position:relative; top:5px; width:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%>">
	<div style="position:relative; left:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth - 87%>px;<%} else { %>643px;<%}%>">
		 <a id="more" style="margin-top:10px; margin-bottom:-3px; height:20px" name="more" href="javascript:;" onmouseout="changeMoreImageOut(this);" onmouseover="changeMoreImage(this);" onClick="pressMoreButton('PFKey90');">
			<img id="hasMore" name="hasMore" src="<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif" border="0" style="height:25px">
		 </a>	
	</div>	
<!-- <br/> -->

<!-- ILIFE-2727  Life Cross Browser -Coding and UT- Sprint 4 D1: Task 5 End by snayeni --> 
<div id="bottomCover" class="bottomCover">&nbsp;</div>
<div id="topCover" class="topCover">&nbsp;</div>
<div class="fakeContainer" id="container">
<table id="s6753Table" class="s6753Table">
 <tr height="30">
<th>Sel</th>
<th>Seq.No</th>
<th>Date</th>
<th>Code</th>
<th>Description</th>
</tr>
<% String backgroundcolor="#FFFFFF";%>	
<%
S6753screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean delVisibile = true;
boolean hyperLinkFlag;
while (S6753screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
{
if (appVars.ind02.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
	delVisibile = false;
}
if (appVars.ind04.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
}
}
%>
<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s6753screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s6753screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="s6753screensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >


<td style="width:<%=tblColumnWidth[0]%>px;" align="center">
	<%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
			 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s6753screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s6753screensfl.select_R<%=count%>'
						 id='s6753screensfl.select_R<%=count%>'
						 onClick="selectedRow('s6753screensfl.select_R<%=count%>')"
						 class="UICheck"
							 <% 
								if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							 %>
							disabled="disabled"
							<%}%>
							<%if(!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0") || (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
							checked="checked"
							<%}%>
					 />
					 					
					
	<%}%>
</td>




<td style="width:<%=tblColumnWidth[1]%>px;" 
	<%if(!(((BaseScreenData)sv.prcSeqNbr) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
	<%if((new Byte((sv.prcSeqNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.prcSeqNbr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
			<%if((new Byte((sv.prcSeqNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%if(hyperLinkFlag){%>
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6753screensfl" + "." +
					      "slt" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.prcSeqNbr.getFormData()%></span></a>							 						 		
						  <%}else{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.prcSeqNbr.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>



<td style="width:<%=tblColumnWidth[2]%>px;" 
	<%if(!(((BaseScreenData)sv.efdateDisp) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >				<%if((new Byte((sv.efdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	<%	longValue = sv.efdateDisp.getFormData();  	%>
					<% 
						if((new Byte((sv.efdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||
							(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div id="s6753screensfl.efdateDisp_R<%=count%>" name="s6753screensfl.efdateDisp_R<%=count%>" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "input_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'
					id='<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.efdateDisp.getFormData() %>' 
					class = " <%=(sv.efdateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.efdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.efdateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(s6753screensfl.efdateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.efdateDisp.getLength()*12%> px;"
					>
					
					<a href="javascript:;" 
					onClick="showCalendar(this, document.getElementById('<%="s6753screensfl" + "." +
					 "efdateDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
					 
					 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0">
					</a>
					<%}%>  			
		<%}%>
</td>



<td style="width:<%=tblColumnWidth[3]%>px;" 
	<%if(!(((BaseScreenData)sv.trancd) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.trancd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.trancd.getFormData();
							%>
					 		<div id="s6753screensfl.trancd_R<%=count%>" name="s6753screensfl.trancd_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[4]%>px;" 
	<%if(!(((BaseScreenData)sv.descrip) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.descrip.getFormData();
							%>
					 		<div id="s6753screensfl.descrip_R<%=count%>" name="s6753screensfl.descrip_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



</tr>
<%	count = count + 1;
S6753screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</table>
</div>
</div>
<br/>
<!-- smalchi2 for ILIFE-1049 STARTS -->
<%if(delVisibile == true){%>	
<div style="position:relative; width:730px;">
<table>
<tr style='height:22px;'>
<td>
<div class="sectionbutton">
      <p style="font-size: 12px; font-weight: bold;">
      <a href="JavaScript:;" onClick= "JavaScript:perFormOperation(9)">
        <%=resourceBundleHandler.gettingValueFromBundle("Delete")%>
      </a></p>
    </div>
			</td>	
		
</tr> </table>
</div>
<%}%>	
<!-- ENDS -->
<INPUT type="HIDDEN" name="screenIndicArea" id="screenIndicArea" value="<%=	(sv.screenIndicArea.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="subfilePosition" id="subfilePosition" value="<%=	(sv.subfilePosition.getFormData()).toString() %>" >

<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts-->
 --%>


<%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
<!-- ILIFE-2727  Life Cross Browser -Coding and UT- Sprint 4 D1: Task 5 Start by snayeni --> 
<%
if(!cobolAv3.isPagedownEnabled()){%>
	<script type="text/javascript">
		window.onload = function(){
			setDisabledMoreBtn();
		}
	</script>
	
<%}%> 
<!-- ILIFE-2727  Life Cross Browser -Coding and UT- Sprint 4 D1: Task 5 End by snayeni --> 