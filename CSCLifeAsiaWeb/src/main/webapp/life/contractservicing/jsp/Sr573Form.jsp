



<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR573";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr573ScreenVars sv = (Sr573ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
<div class="panel-body"> 


   
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
    	 					<div class="input-group">





<input name='chdrsel' id='chdrsel'
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>


<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>




<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%} %>



</div></div></div>






<div class="col-md-4"> 
 					
                </div>



    
    
    	 					
    	 					
    	 		<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	 					<div class="input-group">
	<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy">			


<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%} %>



	
    	 					
    	 					</div></div></div></div>
    	 					
    	 					
    	 			
    	 					

</div>

</div></div>



 <div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
         <div class="panel-body"> 



    	 			
    	 			
    	 			
    	 			
    	 			
    	 			
    	 			
    	 			
    	 			
   <div class="row">	
   <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Component Add Proposal")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Component Modify Proposal")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Component Add Approval")%></b>
					</label>
				</div>






</div>



<div class="row">
<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Component Modify Approval")%></b>
					</label>
				</div>
			<div class="col-md-5">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Reverse Component Add/Modify Proposal")%></b>
					</label>
				</div>











   
   </div> 	
   <div class="row">
   <div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Add Another Life")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "G")%><%=resourceBundleHandler.gettingValueFromBundle("Component Enquiry")%></b>
					</label>
				</div>
	   <%
		   if (sv.fuflag.compareTo("N") != 0) {
	   %>
				<div class="col-md-4">
					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "H")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Follow Ups")%></b>
					</label>
				</div>
	   <%
		   }
	   %>
   </div> 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>



<%-- <!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts -->
<style>
@media \0screen\,screen\9
{.iconpos{bottom:1px;}
}
</style>
<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts -->
<div class='outerDiv'>
<br/><br/><br/><br/>
<fieldset class="fieldsetUIG">
<legend class="legendUIG">
<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
</legend>
<table width='100%' align ='center' class ='tableFormat' cellspacing="5px">

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</div>



<br/>
 
<input name='chdrsel' 
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>


</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</div>



<br/>

<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%} %>
</td>
</tr></table></fieldset><br/><br/><br/><br/><br/>
<fieldset class="fieldsetUIG">
<legend class="legendUIG">
<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
</legend>
<table width='100%' align ='center' class ='tableFormat' cellspacing="5px">


<tr style='height:22px;'><td>

<div class="radioButtonSubmenuUIG">

<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Component Add Proposal")%></label> 
</div>

</td>


<td>

<div class="radioButtonSubmenuUIG">

<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
<%=resourceBundleHandler.gettingValueFromBundle("Component Modify Proposal")%></label> 
</div>

</td>


</tr>


<tr style='height:22px;'>
<td>

<div class="radioButtonSubmenuUIG">

<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%>
<%=resourceBundleHandler.gettingValueFromBundle("Component Add Approval")%></label> 
</div>

</td>
<td>

<div class="radioButtonSubmenuUIG">

<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%>
<%=resourceBundleHandler.gettingValueFromBundle("Component Modify Approval")%></label> 
</div>

</td>


</tr>


<tr style='height:22px;'>
<td>

<div class="radioButtonSubmenuUIG">

<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%>
<%=resourceBundleHandler.gettingValueFromBundle("Reverse Component Add/Modify Proposal")%></label> 
</div>

</td>


<td>

<div class="radioButtonSubmenuUIG">

<label for="F"><%= smartHF.buildRadioOption(sv.action, "action", "F")%>
<%=resourceBundleHandler.gettingValueFromBundle("Add Another Life")%></label> 
</div>

</td>

<td>

<div class="radioButtonSubmenuUIG">

<label for="G"><%= smartHF.buildRadioOption(sv.action, "action", "G")%>
<%=resourceBundleHandler.gettingValueFromBundle("Component Enquiry")%></label> 
</div>

</td>


<td>

<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
</td>
</tr></table></fieldset><br/></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>

<%@ include file="/POLACommon2NEW.jsp"%>

