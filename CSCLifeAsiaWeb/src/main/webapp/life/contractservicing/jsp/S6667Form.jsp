

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6667";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6667ScreenVars sv = (S6667ScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                     ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate ID");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                             ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                        ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"              ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"         ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                     ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mandref.setReverse(BaseScreenData.REVERSED);
			sv.mandref.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mandref.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.mrbnk.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
	}

	%>

<div class="panel panel-default">
<div class="panel-body"> 


   
<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Number")%></label>
    	 					<div class="input-group">
    	 					
    	 					<%
                                         if ((new Byte((sv.mandref).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 100px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.mandref)%>                                                      
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 100px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.mandref)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('mandref')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
    	 					</div>
    	 					
    	 					</div></div></div>
<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Sort Code")%></label>
    	 					<table>
    	 					<tr><td>
    	 					<%
                                         if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 130px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.bankkey)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 130px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankkey)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
    	 					
    	 					
    	 					</td><td style="min-width:100px;">
    	 					<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<!-- ILIFE 2726 starts -->			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<!-- ILIFE 2726 ends -->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 					</td>
    	 					<td style="padding-left:1px;min-width:100px;">
    	 					<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>	
					<!-- ILIFE 2726 starts -->		
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<!-- ILIFE 2726 ends -->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 					
    	 					</td>
    	 					</tr>
    	 					
    	 					</table>
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					<div class="col-md-4"> </div>
    	 					<% if ((new Byte((sv.mrbnk).getInvisible())).compareTo(new Byte (BaseScreenData.INVISIBLE)) != 0) { %>
    	 					<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Classification")%></label>
    	 					<div class="input-group">
    	 					<%
		fieldItem = appVars.loadF4FieldsLong(new String[] { "mrbnk" }, sv, "E", baseModel);
		mappedItems = (Map) fieldItem.get("mrbnk");
		longValue = (String) mappedItems.get((sv.mrbnk.getFormData()).toString().trim());
	%> 
	<%
	 	if (!((sv.mrbnk.getFormData()).toString()).trim().equalsIgnoreCase("")) {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	} else {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	}
	 %>
		<div
			class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:100px;">
			<%=XSSFilter.escapeHtml(formatValue)%>
		</div> 
	<%
	 	longValue = null;
	 	formatValue = null;
		
	 %>
    	 					</div>
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					<%   } %>
    	 					</div>    
    	 					
    	 					<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%></label>
    	 					<table>
    	 					<tr><td style="min-width:100px;">
    	 					<%					
		if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 					
    	 					</td>
    	 					<td style="padding-left:1px;min-width:100px;">
    	 					<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 					
    	 					</td>
    	 					</tr></table></div></div>
    	 					
    	 					
    	 					
    	 					</div>	 					
    	 					
    	 					
    	 					    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
</div></div>





















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Mandate ID")%>
</div>



<br/>
 
<input name='mandref' 
type='text' 
value='<%=sv.mandref.getFormData()%>' 
maxLength='<%=sv.mandref.getLength()%>' 
size='<%=sv.mandref.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mandref)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.mandref).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.mandref).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.mandref).getColor()== null  ? 
"input_cell" :  (sv.mandref).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>


</td>
<!-- ILIFE 2726 starts -->
<style>

/* for IE 8 */
@media \0screen\,screen\9
{
.blank_cell{margin-right:3px;}
}
</style>
<!-- ILIFE 2726 ends -->
<td width='251'></td><td width='251'></td></tr><tr style='height:24px;'><td width='753'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%>
</div>



<br/>

<input name='bankkey' 
type='text' 
value='<%=sv.bankkey.getFormData()%>' 
maxLength='<%=sv.bankkey.getLength()%>' 
size='<%=sv.bankkey.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.bankkey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.bankkey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('bankkey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.bankkey).getColor()== null  ? 
"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('bankkey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>






	
  		
		<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<!-- ILIFE 2726 starts -->			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<!-- ILIFE 2726 ends -->
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>	
					<!-- ILIFE 2726 starts -->		
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<!-- ILIFE 2726 ends -->
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<!-- ILIFE-2476 Modification Starts -->
<td width=251>
<% if ((new Byte((sv.mrbnk).getInvisible())).compareTo(new Byte (BaseScreenData.INVISIBLE)) != 0) {%>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Classification")%>
</div>
<br/>

	<%
		fieldItem = appVars.loadF4FieldsLong(new String[] { "mrbnk" }, sv, "E", baseModel);
		mappedItems = (Map) fieldItem.get("mrbnk");
		longValue = (String) mappedItems.get((sv.mrbnk.getFormData()).toString().trim());
	%> 
	<%
	 	if (!((sv.mrbnk.getFormData()).toString()).trim().equalsIgnoreCase("")) {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	} else {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	}
	 %>
		<div
			class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
			<%=formatValue%>
		</div> 
	<%
	 	longValue = null;
	 	formatValue = null;
		}
	 %>
</td>
<!-- ILIFE-2476 Modification End -->
</tr><tr style='height:42px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>


</tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='275'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>

</tr></table></div><br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>

