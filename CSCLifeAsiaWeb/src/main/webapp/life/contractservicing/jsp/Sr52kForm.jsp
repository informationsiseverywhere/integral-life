<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR52K";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sr52kScreenVars sv = (Sr52kScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind41.isOn()) {
			sv.ratebas.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
<tr>
<td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div style="margin-left: 2px; "
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;margin-left:2px; ">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
</tr>
					</table>
				</div>
			</div>

			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;margin-left: 2px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							 style="margin-left: 2px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Payer")%></label>
					<table>
<tr>
<td>
						<%
							if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.payorname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payorname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payorname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 2px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.agntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.agntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if ((new Byte((sv.agentname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.agentname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.agentname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 2px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
		</div>






		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Inception Date")%></label> --%>
					<!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Inception Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
					<div class="input-group" style="width: 65px;">

						<%=smartHF.getRichText(0, 0, fw, sv.occdateDisp, (sv.occdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute", "relative")%>



					</div>
				</div>


			</div>

		</div>


		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>
					<div class="input-group" style="width: 65px;">

						<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp, (sv.btdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute", "relative")%>

					</div>
				</div>
			</div>


			<div class="col-md-4"></div>


			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					<div class="input-group" style="width: 65px;">

						<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp, (sv.ptdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute", "relative")%>

					</div>
				</div>

			</div>
		</div>






		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing frequency")%></label>


					<%
						if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-4"></div>



			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>

					<%
						if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mop.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mop.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>



				</div>
			</div>

		</div>



		<div class="row">
			<div class="col-md-6">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Basis")%></label>
				<table>
				<tr>
				<td>
					<%=smartHF.getRichText(0, 0, fw, sv.ratebas, (sv.ratebas.getLength() + 1), null).replace("absolute",
					"relative")%>
					</td>
					<td>&nbsp;</td>
					<td>
				<%=resourceBundleHandler.gettingValueFromBundle(" (P - Refund Full Premium; U - Refund units)")%>
</td>
</tr>
</table>
			</div>

		</div>
		<br>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div class="input-group" style="width: 65px;">
						<%=smartHF.getRichText(0, 0, fw, sv.effdateDisp, (sv.effdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute", "relative")%>

					</div>
				</div>
			</div>


		</div>





	</div>
</div>
































































<%-- <div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</div>
<br/>
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
</div>
<br/>
<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
</div>
<br/>
<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>

<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Payer")%>
</div>
<br/>
<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.payorname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Agency")%>
</div>
<br/>
<%if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.agentname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
</td>
<!-- END TD FOR ROW 4,7 etc --> 
</tr>

 <tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Inception Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%>
</td>
</tr>
 <tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billing frequency")%>
</div>
<br/>
<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%>
</div>
<br/>
<%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.mop.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mop.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:42px;'>
<td width='271'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Refund Basis")%>
</div>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.ratebas,( sv.ratebas.getLength()+1),null).replace("absolute","relative")%>
<%=resourceBundleHandler.gettingValueFromBundle(" (P - Refund Full Premium; U - Refund units)")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
</td>
</tr>
<tr style='height:42px;'>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute","relative")%>
</td>
</tr>
 </table>
<br/>
</div> --%>
<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%@ include file="/POLACommon2NEW.jsp"%>