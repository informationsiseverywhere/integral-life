

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5077";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5077ScreenVars sv = (S5077ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Payor ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commence ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of Payment ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason ");%>

<%{
		if (appVars.ind05.isOn()) {
			generatedText4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payrnum.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.payorname.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	

        //ICIL-149 start
        if (appVars.ind38.isOn()) {
            sv.bnkout.setReverse(BaseScreenData.REVERSED);
            sv.bnkout.setColor(BaseScreenData.RED);
        }
        if (appVars.ind106.isOn()) {
            sv.bnkout.setEnabled(BaseScreenData.DISABLED);	
        }
        if (!appVars.ind38.isOn()) {
            sv.bnkout.setHighLight(BaseScreenData.BOLD);
        }	       
        if (appVars.ind78.isOn()) {
			sv.bnkout.setInvisibility(BaseScreenData.INVISIBLE);
		}
        
        if (appVars.ind33.isOn()) {
            sv.bnktel.setReverse(BaseScreenData.REVERSED);
            sv.bnktel.setColor(BaseScreenData.RED);
        }
        if (appVars.ind34.isOn()) {
            sv.bnktel.setEnabled(BaseScreenData.DISABLED);	
        }
        if (!appVars.ind33.isOn()) {
            sv.bnktel.setHighLight(BaseScreenData.BOLD);
        }	      
        if (appVars.ind79.isOn()) {
			sv.bnktel.setInvisibility(BaseScreenData.INVISIBLE);
		}
        
        //ICIL-149 end
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					
					<table><tr><td>
					<%
					if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.chdrnum.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					} else {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.chdrnum.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					}%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 65px">
							<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</td><td>
					<%
					if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.cnttype.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					} else {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.cnttype.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					}%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="width:50px;margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
					</td><td>
					<%
					if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					} else {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					}%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="max-width:350px;margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					<%					
							if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
						style="max-width: 170px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
							longValue = null;
							formatValue = null;
							%>

				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
							longValue = null;
							formatValue = null;
							%>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<!-- <div class="input-group" style="width: 100%;"> -->
					<table><tr><td>
					<%					
						if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %> ' style="width: 65px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%>
						</td><td>
						<%					
						if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %> ' style="width:100px; margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							longValue = null;
							formatValue = null;
							%>
						<!-- </div> -->
						</td></tr></table>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Payor")%></label>
					<!-- <div class="input-group" style="width: 100%"> -->
					<table><tr><td>
						<%
							if (!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell": "output_cell"%> ' style="min-width: 90px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>
						<%
							if (!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payorname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payorname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> '
							style="width:100px; margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					<!-- <div class="input-group" style="width: 100%"> -->
					<table><tr><td>
						<%
							if (!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="width: 65px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						
						</td><td>
						<%
							if (!((sv.oragntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.oragntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.oragntnam.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="width:100px;margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>

					<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("billfreq");
								optionValue = makeDropDownList( mappedItems , sv.billfreq,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
												
							if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
						style="width: 170px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
							longValue = null;
							formatValue = null;
							%>
				</div>
			</div>
			<!-- <div class="col-md-2">	
	                 </div>   -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
					<%
							fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("mop");
							optionValue = makeDropDownList( mappedItems , sv.mop,2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
												
						if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.mop.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.mop.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
						style="max-width: 170px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
						%>

				</div>
			</div>


		</div>

		<div class="row">
			<div class="col-md-4" style="min-width: 33%;">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New")%></label>
					
					 <table><tr><td> 

						
<%
                                         if ((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 120px;margin-right: 2px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.agntsel)%>
                                                          </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.agntsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                      style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 
						</td><td style="width:4px;"></td><td>
						
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "agentname" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("agentname");
						optionValue = makeDropDownList(mappedItems, sv.agentname, 2,
								resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.agentname.getFormData())
								.toString().trim());

						if (!((sv.agentname.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agentname.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agentname.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>' style="width:100px; margin-left:-2px;" ">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
					
					 <!-- </div> --> 
					 </td></tr></table> 
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>

					<table>
						<tr>
							<td style="min-width: 140px;">
								<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("reasoncd");
											optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
											longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
										%> <% 
											if((new Byte((sv.reasoncd).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
										%>
								<div
									class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
																	"blank_cell" : "output_cell" %>'>
									<%if(longValue != null){%>

									<%=longValue%>

									<%}%>
								</div> <%
										longValue = null;
										%> <% }else {%> <% if("red".equals((sv.reasoncd).getColor())){
										%>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 142px;">
									<%
										} 
										%>

									<select class='sel' name='reasoncd'
										type='list'
										<% 
											if((new Byte((sv.reasoncd).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>
										readonly="true" disabled
										class="output_cell"
										<%
											}else if((new Byte((sv.reasoncd).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>
										class="bold_cell"
										<%
											}else { 
										%>
										class='input_cell'
										<%
											} 
										%>
										onchange="change()">
										<%=optionValue%>
									</select>
									<% if("red".equals((sv.reasoncd).getColor())){
										%>
								</div> <%
										} 
										%> <%
										} 
										%>

							</td>
							<td><input style="width: 200px; !important;"
								"id='resn' name='resndesc' type='text'
								<%
										
												fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
												mappedItems = (Map) fieldItem.get("reasoncd");
												optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
												formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
												
										%>
								<%if(formatValue==null) {
											formatValue="";
										}
										 %>
								<% String str=(sv.resndesc.getFormData()).toString().trim(); %>
								<% if(str.equals("") || str==null) {
														str=formatValue;
													}
													
												%>
								value='<%=str%>'
								<%if(formatValue!=null && formatValue.trim().length()>0) {%>
								title='<%=str%>' <%}%> size='50'
								maxLength='50'
								<% 
											if((new Byte((sv.resndesc).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>
								readonly="true" class="output_cell"
								<%
											}else if((new Byte((sv.resndesc).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>
								class="bold_cell"
								<%
											}else { 
										%>
								class=' <%=(sv.resndesc).getColor()== null  ? 
													"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
								<%
											} 
										%>>

							</td>
						</tr>
					</table>


				</div>
			</div>

		</div>

<div class="row">
			<!--ICIL-149 start  -->
				<%
				if ((new Byte((sv.bnkout).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Outlet")%></label>
					<table>
						<tr>
							<td style="min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkout, true)%></td>
							<td style="padding-left: 1px; min-width: 50px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkoutname, true)%></td>
						</tr>
					</table>
				</div>
				
			</div>
			<%} %>
				
			</div>
			<div class="row">
			 
				<%
				if ((new Byte((sv.bnktel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label>
					<table>
						<tr>
							<td><input name='bnksm' id='bnksm' type='text'
								style="min-width: 91px;" value='<%=sv.bnksm.getFormData()%>'
								maxLength='<%=sv.bnksm.getLength()%>'
								size='<%=sv.bnksm.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(billcdDisp)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control"></td>
							<td>
							<td>
								<%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label> --%>
								<input name='bnksmname' id='bnksmname' style="margin-left: 1px;"
								type='text' value='<%=sv.bnksmname.getFormData()%>'
								maxLength='<%=sv.bnksmname.getLength()%>'
								size='<%=sv.bnksmname.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(billcdDisp)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control">
							</td>
						</tr>

					</table>
				</div>

			</div>
			<%} %>
                 </div>
                 <div class="row">
				<%
				if ((new Byte((sv.bnktel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Teller")%></label>
					<table>
						<tr>
							<td style="min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktel, true)%></td>
							<td style="padding-left: 1px; min-width: 50px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktelname, true)%></td>
						</tr>
					</table>
				</div>
			</div>
			<%} %>
				</div>
			<!--ICIL-149 end  -->
				</div>
	</div>
</div>
	      
	                    	


<%@ include file="/POLACommon2NEW.jsp"%>

