   <%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52L";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr52lScreenVars sv = (Sr52lScreenVars) fw.getVariables();%>
<%
if (appVars.ind52.isOn()) {
	sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind51.isOn()) {
	sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind06.isOn()) {
	sv.amnt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind08.isOn()) {
	sv.billcurr.setInvisibility(BaseScreenData.INVISIBLE);
}

if (appVars.ind50.isOn()) {
	sv.reasoncd.setReverse(BaseScreenData.REVERSED);
	sv.reasoncd.setColor(BaseScreenData.RED);
}
%>
<!-- <style>
.col-md-4>.form-group,.col-md-4>.form-group>.form-control.ellipsis,.col-md-4>.form-control.ellipsis, .col-md-4 > input{
	width: 150px;
	float: right;
}
</style> -->


<div class="panel panel-default">
	<div class="panel-body">
	
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
					<tr>
					<td>
						<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					<td>
					
					<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 200px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<%if (!appVars.ind51.isOn()) {%>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Reserve Date")%></label>
					<div class="input-group">
						<!-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
 -->
						<%=smartHF.getRichText(0, 0, fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						<%-- <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.reserveUnitsDateDisp).replace("absolute","relative")%> --%>
						<!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->



					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Med Fee")%></label>

					<%if ((new Byte((sv.amnt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.amnt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
			
						qpsf = fw.getFieldXMLDef((sv.amnt).getFieldName());
						
						/* ILIFE-2752   starts */
						
						qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						
						/* ILIFE-2752 ends */
						formatValue = smartHF.getPicFormatted(qpsf,sv.amnt);
						
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.amnt.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>

					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
	longValue = null;
	formatValue = null;
	%>
					<%}%>



				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bill Currency")%></label>

					<% if(!((sv.billcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
	longValue = null;
	formatValue = null;
	%>


				</div>
			</div>
		</div>
		<%}%>

		<br>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">


					<%if ((new Byte((sv.dtldesc01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.dtldesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.dtldesc01.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.dtldesc01.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
					<div id="dtldesc01" class='label_txt'>
						<% 
String agent = "AGENT";
try{
	agent = formatValue.trim().split(" ")[0];
	
}catch(Exception e){}%>
						<%=agent%>
					</div>
					<%
	longValue = null;
	formatValue = null;
	%>
					<%}%>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">

					<%if ((new Byte((sv.dtldesc01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.dtldesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.dtldesc01.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.dtldesc01.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
					<div id="dtldesc01" class='label_txt'>

						<% 
String agent = "AGENT";
try{
	agent = formatValue.trim().split(" ")[0];
	
}catch(Exception e){}
String t = formatValue.replaceFirst(agent, " ").trim(); %>
						<%=t%>
					</div>
					<%
	longValue = null;
	formatValue = null;
	%>
					<%}%>

				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<%if ((new Byte((sv.medfee01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.medfee01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
			
						qpsf = fw.getFieldXMLDef((sv.medfee01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.medfee01);
						
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.medfee01.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
					<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>

					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<% }%>


					<% if((browerVersion.equals(IE8))) {%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 100px;">

						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<% }%>
					<%
					longValue = null;
					formatValue = null;
					%>
					<%}%>


				</div>
			</div>

			<div class="col-md-2">


				<%=smartHF.getHTMLVarExt(fw, sv.medamt01)%>

			</div>

		</div>

		<div class="row">

			<div class="col-md-2"></div>
			<div class="col-md-2">


				<%if ((new Byte((sv.dtldesc02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dtldesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.dtldesc02.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div id="dtldesc02" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
			</div>

			<div class="col-md-4">
				<%if ((new Byte((sv.admfee01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% 
						qpsf = fw.getFieldXMLDef((sv.admfee01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.admfee01);
						if(!((sv.admfee01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.admfee01.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>

				<% if((browerVersion.equals(IE8))) {%>
				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="max-width: 100px">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>


				<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="max-width: 100px">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<%
							longValue = null;
							formatValue = null;
							%>
				<%}%>

			</div>

			<div class="col-md-2">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.admamt01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("absolute","relative")%>
				<%--   <%=smartHF.getHTMLVar(fw, sv.admamt01)%> --%>

			</div>

		</div>

		<br>
		<div class="row">

			<div class="col-md-2"></div>


			<div class="col-md-2">

				<%if ((new Byte((sv.dtldesc03).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.dtldesc03.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.dtldesc03.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
				<div id="dtldesc03" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
						longValue = null;
						formatValue = null;
						%>
				<%}%>
			</div>


			<div class="col-md-4">
				<div class="form-group" style="width: 200px">
					<table>
						<tr>
							<td><%=resourceBundleHandler.gettingValueFromBundle("A/C")%></td>

							<td>
								<%if ((new Byte((sv.sacscode01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.sacscode01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.sacscode01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.sacscode01.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
								%>
								<div id="sacstypw00"
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
									style="margin-left: 2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
										longValue = null;
										formatValue = null;
										%> <%}%>
							</td>

							<td>
								<div class="input-group">
									<% if ((new Byte((sv.sacstypw01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstypw01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("sacstypw01");
							optionValue = makeDropDownList( mappedItems , sv.sacstypw01.getFormData(),4);  
							longValue = (String) mappedItems.get((sv.sacstypw01.getFormData()).toString().trim());
							int colWidth = 0; 
							int dropDownDescWidth=0;
							dropDownDescWidth = ((sv.sacstypw01.getFormData()).length()*12)+88;	
						%>
									<% 
							if((new Byte((sv.sacstypw01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>
									<div
										class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%if(longValue != null){%>

										<%=longValue%>

										<%}%>
									</div>

									<%
						longValue = null;
						%>

									<% }else {%>

									<% if((browerVersion.equals(IE11))){%>

									<!-- <div style="z-index: 1; position: absolute; padding-left: 0px;  margin-top: 7px; width: 172px; height: 20px; top: 52px; padding-top: 0px; left: 297px;" sizcache="1" sizset="0">
							 
							 </div> -->
									<% }%>

									<% if((browerVersion.equals(IE8))){%>

									<!-- <div style="z-index: 1; position: absolute; padding-left: 0px;  margin-top: 0px; width: 172px; height: 20px; top: 52px; padding-top: 0px; left: 300px;" sizcache="1" sizset="0">
							 
							  </div> -->
									<% }%>


									<% if((browerVersion.equals(Chrome))) {%>

									<!-- <div style="z-index: 1; position: absolute; padding-left: 0px;  margin-top: 3px; width: 172px; height: 20px; top: 52px; padding-top: 0px; left: 297px;" sizcache="1" sizset="0">
						</div> -->
									<% }%>

									<input name="sacstypw01" id="sacstypw01"
										type='text'
										value='<%= sv.sacstypw01.getFormData() %>'
										class=" <%=(sv.sacstypw01).getColor()== null  ? 
						"input_cell" :  
						(sv.sacstypw01).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>"
										maxLength='<%=sv.sacstypw01.getLength()%>'
										onFocus='doFocus(this)'
										onBlur='tabFieldchanged(this)'
										onHelp='return fieldHelp(sr52lscreensfl.sacstypw01)'
										onKeyUp='return checkMaxLength(this)'
										style="left: 2px;float: right;width: 64px !important">

									<!-- ILIFE2844 starts -->
									<% if((browerVersion.equals(Chrome))||(browerVersion.equals(IE11))) {%>

									<!-- <a href="javascript:;" 
						onClick="doFocus(document.getElementById('sacstypw01')); changeF4Image(this); doAction('PFKEY04');" style="margin-left: 2px;">
						 -->

									<span class="input-group-btn">
										<button class="btn btn-info"
											style="margin-left: 1px;" type="button"
											onClick="doFocus(document.getElementById('sacstypw01')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search"
												aria-hidden="true"></i>
										</button>
									</span>



									<% }%>

									<% if((browerVersion.equals(IE8))) {%>

									<%-- <a href="javascript:;" 
						onClick="doFocus(document.getElementById('sacstypw01')); changeF4Image(this); doAction('PFKEY04');" style="margin-left: -2px;">
						
							 <% }%> 
							 	<!-- ILIFE2844 ends -->		
						
						
						<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0">
						</a> --%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="margin-left: 1px;" type="button"
											onClick="doFocus(document.getElementById('sacstypw01')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search"
												aria-hidden="true"></i>
										</button>
									</span>

									<% }%>


								</div> <% }} %>

							</td>
						</tr>
					</table>
				</div>
			</div>


			<div class="col-md-2">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.adjamt01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("absolute","relative")%>

				<%--   <%=smartHF.getHTMLVar(fw, sv.adjamt01)%> --%>

			</div>


		</div>

		<div class="row">
			<div class="col-md-2"></div>


			<div class="col-md-2">

				<%if ((new Byte((sv.dtldesc04).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dtldesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.dtldesc04.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div id="dtldesc04" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
			</div>


			<div class="col-md-4"></div>




			<div class="col-md-4">

				<%if ((new Byte((sv.tdeduct).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.tdeduct.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
						qpsf = fw.getFieldXMLDef((sv.tdeduct).getFieldName());
						
					/* 	ILIFE-2752--started by vjain60 */
						qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						/* 	ILIFE-2752--ended by vjain60 */
						formatValue = smartHF.getPicFormatted(qpsf,sv.tdeduct);
						
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( formatValue); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tdeduct.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>

				<% if((browerVersion.equals(IE11))){%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-right: 105px; max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<% if((browerVersion.equals(IE8))){%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-left: 32px; max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>

				<% if((browerVersion.equals(Chrome))){%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style=" max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>

			</div>
		</div>

		<br>
		<br>

		<div class="row">
			<div class="col-md-2">

				<%if ((new Byte((sv.dtldesc05).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.dtldesc05.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.dtldesc05.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
				<div id="dtldesc05" class='label_txt'>
					<% 
			String agent = "ASSURED";
			try{
				agent = formatValue.trim().split(" ")[0];
				
			}catch(Exception e){}%>
					<%=agent%>
				</div>
				<%
				longValue = null;
				formatValue = null;
				%>
				<%}%>


			</div>

			<div class="col-md-2">

				<%if ((new Byte((sv.dtldesc05).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.dtldesc05.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.dtldesc05.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
				<div id="dtldesc05" class='label_txt'>
					<% 
			String agent = "ASSURED";
			try{
				agent = formatValue.trim().split(" ")[0];
				
			}catch(Exception e){}%>
					<%=agent%>
				</div>
				<%
				longValue = null;
				formatValue = null;
				%>
				<%}%>
			</div>

			<div class="col-md-4"></div>


			<div class="col-md-4">
				<%if ((new Byte((sv.premi).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.premi.getFormData()).toString()).trim().equalsIgnoreCase("")) {
		
		qpsf = fw.getFieldXMLDef((sv.premi).getFieldName());
		qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
		formatValue = smartHF.getPicFormatted(qpsf,sv.premi);
		
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue(formatValue); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premi.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>

				<% if((browerVersion.equals(IE11)))  {%>
				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-right: 105px; max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<% if((browerVersion.equals(IE8))) {%>
				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-left: 32px; max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<% if((browerVersion.equals(Chrome))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style=" max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<%
	longValue = null;
	formatValue = null;
	%>
				<%}%>

			</div>

		</div>

		<br>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2">

				<%if ((new Byte((sv.dtldesc06).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.dtldesc06.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.dtldesc06.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
				<div id="dtldesc06" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
	longValue = null;
	formatValue = null;
	%>
				<%}%>
			</div>
			<div class="col-md-4">
			<div class="form-group">
				<%if ((new Byte((sv.medfee02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.medfee02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
		qpsf = fw.getFieldXMLDef((sv.medfee02).getFieldName());
		qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
		formatValue = smartHF.getPicFormatted(qpsf,sv.medfee02);
		
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue(formatValue); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.medfee02.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>

				<% if((browerVersion.equals(IE8))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-left: 32px;width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<%
	longValue = null;
	formatValue = null;
	%>
				<%}%>

			</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.medamt02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("absolute","relative")%>
				<%-- 	<%=smartHF.getHTMLVar( fw, sv.medamt02)%> --%>

			</div>
			</div>
		</div>

		<br>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2">
			<div class="form-group">
				<%if ((new Byte((sv.dtldesc07).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.dtldesc07.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.dtldesc07.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
				<div id="dtldesc07" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
	longValue = null;
	formatValue = null;
	%>
				<%}%>
				</div>
			</div>
			<div class="col-md-4">
			<div class="form-group">
				<%if ((new Byte((sv.admfee02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.admfee02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
					qpsf = fw.getFieldXMLDef((sv.admfee02).getFieldName());
					qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.admfee02);
					
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue(formatValue); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.admfee02.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>

				<% if((browerVersion.equals(IE8))) {%>
				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>

				<% }%>
				<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<%
				longValue = null;
				formatValue = null;
				%>
				<%}%>
			</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">
				<%-- <%=smartHF.getHTMLVar(fw, sv.admamt02)%> --%>

				<%=smartHF.getHTMLVar(0, 0, fw, sv.admamt02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("absolute","relative")%>
			</div>
			</div>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<%if ((new Byte((sv.dtldesc08).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.dtldesc08.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.dtldesc08.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
				<div id="dtldesc08" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
	longValue = null;
	formatValue = null;
	%>
				<%}%>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="width: 200px">

					<table>
						<tr>
							<td><%=resourceBundleHandler.gettingValueFromBundle("A/C")%>

							</td>
							<td>&nbsp;</td>



							<td>
								<!-- ILIFE-809 END -- Sr52l Freelook Refund Details numerical data UI alignment -->
								<%if ((new Byte((sv.sacscode02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.sacscode02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.sacscode02.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.sacscode02.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
								<div id="sacstypw22"
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
	longValue = null;
	formatValue = null;
	%> <%}%>
							</td>
							<td>
								<div class="input-group">
									<% if ((new Byte((sv.sacstypw02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstypw02"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("sacstypw02");
									optionValue = makeDropDownList( mappedItems , sv.sacstypw02.getFormData(),4);  
									longValue = (String) mappedItems.get((sv.sacstypw02.getFormData()).toString().trim());
									int colWidth = 0; 
									int dropDownDescWidth=0;
									dropDownDescWidth = ((sv.sacstypw02.getFormData()).length()*12)+88;	
								%>
																	<% 
									if((new Byte((sv.sacstypw02).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>
									<div
										class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%if(longValue != null){%>

										<%=longValue%>

										<%}%>
									</div>

									<%
									longValue = null;
									%>

									<% }else {%>


									<% if((browerVersion.equals(IE11))){%>

									<!-- <div style="z-index: 1; position: absolute; padding-left: 0px; margin-top:22px; width: 172px; height: 20px; top: 199px; padding-top: 0px; left: 295px;" sizcache="0" sizset="0">
	</div> -->
									<% }%>
									<% if((browerVersion.equals(IE8))) {%>
									<!-- <div style="z-index: 1; position: absolute; padding-right: 0px; margin-top:0px; width: 172px; height: 20px; top: 199px; padding-top: 0px; left: 296px;" sizcache="0" sizset="0">
	</div> -->
									<% }%>
									<% if((browerVersion.equals(Chrome))) {%>
									<!-- <div style="z-index: 1; position: absolute; padding-left: 0px; margin-top:9px; width: 172px; height: 20px; top: 199px; padding-top: 0px; left: 295px;" sizcache="0" sizset="0">
	</div> -->
									<% }%>
									<input name="sacstypw02" id="sacstypw02"
										type='text'
										value='<%= sv.sacstypw02.getFormData() %>'
										class=" <%=(sv.sacstypw02).getColor()== null  ? 
									"input_cell" :  
									(sv.sacstypw02).getColor().equals("red") ? 
									"input_cell red reverse" : 
									"input_cell" %>"
										maxLength='<%=sv.sacstypw02.getLength()%>'
										onFocus='doFocus(this)'
										onBlur='tabFieldchanged(this)'
										onHelp='return fieldHelp(sr52lscreensfl.sacstypw02)'
										onKeyUp='return checkMaxLength(this)'
										style="left: 2px;float: right;width: 64px !important">
									<!-- ILIFE2844 starts -->

									<% if((browerVersion.equals(Chrome))||(browerVersion.equals(IE11))) {%>

									
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="margin-left: 1px;" type="button"
											onClick="doFocus(document.getElementById('sacstypw02')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search"
												aria-hidden="true"></i>
										</button>
									</span>


									<% }%>

									<% if((browerVersion.equals(IE8))) {%>

									<%-- <a href="javascript:;" 
onClick="doFocus(document.getElementById('sacstypw02')); changeF4Image(this); doAction('PFKEY04');" style="margin-left: -2px;">

	 <% }%> 
	<!-- ILIFE2844 ends -->		

<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0">
</a> --%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 12px !important; left:5px" type="button"
											onClick="doFocus(document.getElementById('sacstypw02')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search"
												aria-hidden="true"></i>
										</button>
									</span>

									<% }%>

								</div> <% }} %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">
				<%-- <%=smartHF.getHTMLVar(fw, sv.adjamt02)%> --%>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.adjamt02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("absolute","relative")%>
			</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<%if ((new Byte((sv.dtldesc09).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.dtldesc09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.dtldesc09.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.dtldesc09.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
				<div id="dtldesc09" class='label_txt'>
					<%=formatValue%>
				</div>
				<%
				longValue = null;
				formatValue = null;
				%>
				<%}%>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<%if ((new Byte((sv.ztotamt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.ztotamt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
						
					qpsf = fw.getFieldXMLDef((sv.ztotamt).getFieldName());
					qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.ztotamt);
					
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue(formatValue); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ztotamt.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>

				<% if((browerVersion.equals(IE11))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>
				<% if((browerVersion.equals(IE8))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="margin-left: 30px;max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>


				<% if((browerVersion.equals(Chrome))) {%>

				<div
					class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					style="max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<% }%>

				<%
				longValue = null;
				formatValue = null;
				%>
				<%}%>


			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reason Details")%></label>
			</div>
		</div>

		<br>
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>

				<% if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("reasoncd");
	optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),3);  
	longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
	int colWidth = 0; 
	int dropDownDescWidth=0;
	dropDownDescWidth = ((sv.reasoncd.getFormData()).length()*12)+88;	
%>
				<% 
	if((new Byte((sv.reasoncd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>
				<div
					class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>
					<%if(longValue != null){%>

					<%=longValue%>

					<%}%>
				</div>

				<%
longValue = null;
%>

				<% }else {%>
				<select name='reasoncd' type='list'
					style="width: 250px;"
					<% 
		if((new Byte((sv.reasoncd).getEnabled()))
		.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
	%>
					readonly="true" disabled class="output_cell"
					<%
		}else if((new Byte((sv.reasoncd).getHighLight())).
			compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	%>
					class="bold_cell" <%
		}else { 
	%>
					class=' <%=(sv.reasoncd).getColor()== null  ? 
				"input_cell" :  (sv.reasoncd).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>'
					<%
		} 
	%>>
					<%=optionValue%>
				</select>
				<% }} %>



			</div>

		</div>






	</div>
</div>

<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%@ include file="/POLACommon2NEW.jsp"%>

<%/*bug #ILIFE-565 end*/%>