<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5083";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	S5083ScreenVars sv = (S5083ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Source ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prm Status ");
%>
<%
	 StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD "); 
%>	
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Instalment Prem ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Overdue ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency  ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Billing ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Renewal ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Payment");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Method ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Commission  ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid-to Date  ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bonuses ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to Date  ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"---------------- Details ---------------------------------------------------- ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Suppress Notices ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reason ");
%>

<%
	{
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cnttype.setReverse(BaseScreenData.REVERSED);
			sv.cnttype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cnttype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ctypdesc.setReverse(BaseScreenData.REVERSED);
			sv.ctypdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ctypdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.rstate.setReverse(BaseScreenData.REVERSED);
			sv.rstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.rstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pstate.setReverse(BaseScreenData.REVERSED);
			sv.pstate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pstate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cownnum.setReverse(BaseScreenData.REVERSED);
			sv.cownnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cownnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ownername.setReverse(BaseScreenData.REVERSED);
			sv.ownername.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ownername.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.notind.setReverse(BaseScreenData.REVERSED);
			sv.notind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.notind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.currfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.currfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.currfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.currtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.currtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.currtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
		
	}
%>



<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>


							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td>
								<%
									if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="cntdesc" style="margin-left:1px;max-width:155px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td>
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 200px;margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->
					<div class="input-group" style="max-width: 900px;">

						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prm Status")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>


						</div>
					</div>
				</div>


				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>

						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("register");
								optionValue = makeDropDownList(mappedItems, sv.register, 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());

								if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>


						</div>

					</div>
				</div>
			</div>
		


		<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Prem")%></label>
						<div class="input-group" style="max-width: 900px;">


							<%
								qpsf = fw.getFieldXMLDef((sv.instpramt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.instpramt,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

								if (!((sv.instpramt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
							%>
							<div class="output_cell">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								} else {
							%>

							<div class="blank_cell">&nbsp;</div>

							<%
								}
							%>
							<%
								longValue = null;
								formatValue = null;
							%>



						</div>
					</div>
				</div>


				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>



						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>
						<div class="input-group" style="max-width: 900px;">


							<%
								if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>


						</div>

					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>



						</div>
					</div>
				</div>


				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Payment")%></label>
						<div class="input-group" style="max-width: 900px;">
							<%
								fieldItem = appVars.loadF4FieldsLong(new String[]{"mop"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("mop");
								optionValue = makeDropDownList(mappedItems, sv.mop, 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());

								if (!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.mop.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.mop.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>


						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
						<div class="input-group" style="max-width: 900px;">

							<%
								fieldItem = appVars.loadF4FieldsLong(new String[]{"billfreq"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("billfreq");
								optionValue = makeDropDownList(mappedItems, sv.billfreq, 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());

								if (!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.billfreq.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.billfreq.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>



						</div>

					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Source")%></label>
						<div class="input-group" style="max-width: 900px;">
							<%
								fieldItem = appVars.loadF4FieldsLong(new String[]{"srcebus"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("srcebus");
								optionValue = makeDropDownList(mappedItems, sv.srcebus, 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());

								if (!((sv.srcebus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.srcebus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.srcebus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>



						</div>
					</div>
				</div>


				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>
						<div class="input-group" style="min-width: 120px;">

							<%
								fieldItem = appVars.loadF4FieldsLong(new String[]{"reptype"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("reptype");
								optionValue = makeDropDownList(mappedItems, sv.reptype, 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reptype.getFormData()).toString().trim());

								if (!((sv.reptype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.reptype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.reptype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>


						</div>
					</div>
				</div>

			</div>

			<br>









			<table>

				<tr style='height: 22px;'>
					<td width='351'></td>
					<td width='150'>

						<label>
							<%=resourceBundleHandler.gettingValueFromBundle("Indicator")%>
						</label>


					</td>
					<td width='150'>

						<label>
							<%=resourceBundleHandler.gettingValueFromBundle("From")%>
						</label>


					</td>
					<td width='125'>

						<label>
							<%=resourceBundleHandler.gettingValueFromBundle("To")%>
						</</label>
				</tr>




				<tr>
					<td>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="input-group">

										<label>
											<%=resourceBundleHandler.gettingValueFromBundle("Overdue")%>
										</label>
									</div>
								</div>
							</div>
						</div>


					</td>
					<td><input type='checkbox' readonly='readonly'
						disabled="disabled" name='lapind' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(lapind)'
						onKeyUp='return checkMaxLength(this)'
						<%if (!(sv.lapind).toString().trim().equalsIgnoreCase("X")) {%>
						<%}
			if ((sv.lapind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.lapind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.lapind).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled='disabled' <%}%> class='UICheck'
						onclick="handleCheckBox('lapind')" /> <input type='checkbox'
						readonly="readonly" name='lapind' value=' '
						<%if (!(sv.lapind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('lapind')" /></td>
					<td>
						<%
							if (!((sv.lapsfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lapsfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lapsfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 120px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>




					</td>





					<td>
						<%
							if (!((sv.lapstoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lapstoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lapstoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>

					</td>



				</tr>



				<tr>
					<td>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="input-group">
										<label>
											<%=resourceBundleHandler.gettingValueFromBundle("Billing")%>
										</label>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td><input type='checkbox' readonly='readonly'
						disabled="disabled" name='billind' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(billind)'
						onKeyUp='return checkMaxLength(this)'
						<%if (!(sv.billind).toString().trim().equalsIgnoreCase("X")) {%>
						<%}
			if ((sv.billind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.billind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.billind).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled='disabled' <%}%> class='UICheck'
						onclick="handleCheckBox('billind')" /> <input type='checkbox'
						readonly="readonly" name='billind' value=' '
						<%if (!(sv.billind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('billind')" /></td>
					<td>
						<%
							if (!((sv.billfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 120px;margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>




					</td>
					<td>
						<%
							if (!((sv.billtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					
				</tr>
				<tr>
					<td>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="input-group">
										<label>
											<%=resourceBundleHandler.gettingValueFromBundle("Renewal")%>
										</label>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td><input type='checkbox' readonly='readonly'
						disabled="disabled" name='renind' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(renind)'
						onKeyUp='return checkMaxLength(this)'
						<%if (!(sv.renind).toString().trim().equalsIgnoreCase("X")) {%>
						<%}
			if ((sv.renind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.renind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.renind).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled='disabled' <%}%> class='UICheck'
						onclick="handleCheckBox('renind')" /> <input type='checkbox'
						readonly="readonly" name='renind' value=' '
						<%if (!(sv.renind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('renind')" /></td>
					<td>
						<%
							if (!((sv.rnwlfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rnwlfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rnwlfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 120px;margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>





					</td>
					<td>
						<%
							if (!((sv.rnwltoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rnwltoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rnwltoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					
				</tr>
				<tr>
					<td>
						<%-- <label>
<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
</div> --%>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="input-group">
										<label>
											<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
										</label>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td><input type='checkbox' readonly='readonly'
						disabled="disabled" name='comind' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comind)'
						onKeyUp='return checkMaxLength(this)'
						<%if (!(sv.comind).toString().trim().equalsIgnoreCase("X")) {%>
						<%}
			if ((sv.comind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.comind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.comind).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled='disabled' <%}%> class='UICheck'
						onclick="handleCheckBox('comind')" /> <input type='checkbox'
						readonly="readonly" name='comind' value=' '
						<%if (!(sv.comind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('comind')" /></td>
					<td>
						<%
							if (!((sv.commfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.commfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.commfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 120px;margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>





					</td>
					<td>
						<%
							if (!((sv.commtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.commtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.commtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					
				</tr>
				<tr>
					<td>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="input-group">
										<label> <%=resourceBundleHandler.gettingValueFromBundle("Bonuses")%>
										</label>
									</div>
								</div>
							</div>
						</div>

					</td>
					<td><input type='checkbox' readonly='readonly'
						disabled="disabled" name='bnsind' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(bnsind)'
						onKeyUp='return checkMaxLength(this)'
						<%if (!(sv.bnsind).toString().trim().equalsIgnoreCase("X")) {%>
						<%}
			if ((sv.bnsind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.bnsind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.bnsind).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled='disabled' <%}%> class='UICheck'
						onclick="handleCheckBox('bnsind')" /> <input type='checkbox'
						readonly="readonly" name='bnsind' value=' '
						<%if (!(sv.bnsind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('bnsind')" /></td>
					<td>
						<%
							if (!((sv.bnsfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bnsfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bnsfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 120px;margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>





					</td>
					<td>
						<%
							if (!((sv.bnstoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bnstoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bnstoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="margin-top:7px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					
				</tr>
			</table>
			<br>
			<br>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle(
					"---------------- Details ----------------------------------------------------")%></label>

					</div>
				</div>
			</div>



			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Suppress Notices")%></label>

						<br /> <input type='checkbox' readonly='readonly'
							disabled="disabled" name='notind' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(notind)'
							onKeyUp='return checkMaxLength(this)'
							<%if (!(sv.notind).toString().trim().equalsIgnoreCase("X")) {%>
							<%}
			if ((sv.notind).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.notind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.notind).getEnabled() == BaseScreenData.DISABLED) {%>
							disabled='disabled' <%}%> class='UICheck'
							onclick="handleCheckBox('notind')" /> <input type='checkbox'
							readonly="readonly" name='notind' value=' '
							<%if (!(sv.notind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden;"
							onclick="handleCheckBox('notind')" />


					</div>
				</div>


				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("From")%></label>
						<div class="input-group date form_date col-md-8" data-date=""
							data-date-format="dd/MM/yyyy" data-link-field="currfromDisp"
							data-link-format="dd/mm/yyyy">

							<%
								longValue = sv.currfromDisp.getFormData();
							%>

							<%
								if ((new Byte((sv.currfromDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
							%>
							<div
								class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
								<%
									if (longValue != null) {
								%>

								<%=longValue%>

								<%
									}
								%>
							</div>

							<%
								longValue = null;
							%>
							<%
								} else {
							%>
							<input name='currfromDisp' type='text'
								value='<%=sv.currfromDisp.getFormData()%>'
								maxLength='<%=sv.currfromDisp.getLength()%>'
								size='<%=sv.currfromDisp.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(currfromDisp)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.currfromDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell">

							<%
								} else if ((new Byte((sv.currfromDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
							class="bold_cell" > <span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>

							<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('currfromDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>
 --%>
							<%
								} else {
							%>

							class = '
							<%=(sv.currfromDisp).getColor() == null
							? "input_cell"
							: (sv.currfromDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
								class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
							<%-- 
<a href="javascript:;" onClick="showCalendar(this, document.getElementById('currfromDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

							<%
								}
								}
							%>

						</div>
					</div>
				</div>



				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>
						<div class="input-group date form_date col-md-8" data-date=""
							data-date-format="dd/MM/yyyy" data-link-field="'currtoDisp'"
							data-link-format="dd/mm/yyyy">

							<%
								longValue = sv.currtoDisp.getFormData();
							%>

							<%
								if ((new Byte((sv.currtoDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
							%>
							<div
								class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
								<%
									if (longValue != null) {
								%>

								<%=longValue%>

								<%
									}
								%>
							</div>

							<%
								longValue = null;
							%>
							<%
								} else {
							%>
							<input name='currtoDisp' type='text'
								value='<%=sv.currtoDisp.getFormData()%>'
								maxLength='<%=sv.currtoDisp.getLength()%>'
								size='<%=sv.currtoDisp.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(currtoDisp)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.currtoDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell">

							<%
								} else if ((new Byte((sv.currtoDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
							class="bold_cell" > <span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>

							<%
								} else {
							%>

							class = '
							<%=(sv.currtoDisp).getColor() == null
							? "input_cell"
							: (sv.currtoDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
								class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
							<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('currtoDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

							<%
								}
								}
							%>

						</div>

					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>
						<div class="input-group">
							<table>
								<tr>
									<td>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[]{"reasoncd"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("reasoncd");
											optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 1, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
										%> <%
 	if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.reasoncd).getColor())) {
 %>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='reasoncd' type='list' style="width: 130px;"
												<%if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.reasoncd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell'
												<%}%> onchange="change()">
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.reasoncd).getColor())) {
											%>
										</div> <%
 	}
 %> <%
 	}
 %>

									</td>
									<td style="width: 150px;"><input name='resndesc'
										type='text'
										<%fieldItem = appVars.loadF4FieldsLong(new String[]{"reasoncd"}, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("reasoncd");
			optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 2, resourceBundleHandler);
			formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());%>
										<%if (formatValue == null) {
				formatValue = "";
			}%>
										<%String str = (sv.resndesc.getFormData()).toString().trim();%>
										<%if (str.equals("") || str == null) {
				str = formatValue;
			}%>
										value='<%=str%>'
										<%if (formatValue != null && formatValue.trim().length() > 0) {%>
										title='<%=str%>' <%}%> size='50' maxLength='50'
										onFocus='doFocus(this)' onHelp='return fieldHelp(resndesc)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.resndesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
										readonly="true" class="output_cell"
										<%} else if ((new Byte((sv.resndesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%>
										class=' <%=(sv.resndesc).getColor() == null
						? "input_cell"
						: (sv.resndesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										<%}%> style="margin-left:1px;">
									<script type="text/javascript">
										function change() {
											document
													.getElementsByName("resndesc")[0].value = "";
											doAction('PFKEY05');

										}
									</script></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  panel-->
	</div>
	<!--panel  -->
	<script>
$(document).ready(function() {
	if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','215px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','190px')
	} 
	
})
</script>

	
	<%@ include file="/POLACommon2NEW.jsp"%>