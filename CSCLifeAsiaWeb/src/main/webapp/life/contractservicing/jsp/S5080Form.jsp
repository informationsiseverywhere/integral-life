

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5080";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5080ScreenVars sv = (S5080ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Despatch Address ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason ");%>

<%{
		if (appVars.ind35.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.despsel.setReverse(BaseScreenData.REVERSED);
			sv.despsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.despsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default" >
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
                      	<div class="form-group">   	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
	                      <!-- <div class="input-group three-controller"> -->
	                      <table><tr><td>	
	                      <%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td><td style="padding-left:1px;">
					 	<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					</td><td style="padding-left:1px;">
							<%					
							if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 300px; min-width: 120px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td></tr></table>
					  
						  <!-- </div>	 -->
					  </div>	
				</div>
			</div>	
			
				<div class="row">
                      <div class="col-md-4">	
                      	<div class="form-group">                        
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
	                      <div class="input-group">      
	                      <%					
							if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </div>
	                      
	                    </div>
	                    </div>
	                    	
	                    <div class="col-md-4">	  
	                    	<div class="form-group">                    
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
	                      <div class="input-group">      
	                      <%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
	                      </div>
				       </div>
				   </div>
				 </div>  
				   <div class="row">
                      <div class="col-md-4">
                      <div class="form-group">                    	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
	                      
	                      <table><tr><td> 
	                      	<%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
								
						</td><td style="padding-left:1px;min-width:100px">
						<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 300px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td></tr></table>
	                      
	                      </div>
				    	</div>
				  </div>
				   
				                      
	                 <div class="row">
	                     <div class="col-md-4">
	                      <div class="form-group">   
	                      	  <label><%=resourceBundleHandler.gettingValueFromBundle("Despatch Address")%></label>
	                      	 <table><tr><td>
<%-- 	                      	 <div class="input-group">
	                     <%	
	longValue = sv.despsel.getFormData();  
%>

<% 
	if((new Byte((sv.despsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='despsel' 
type='text' id='despsel'
value='<%=sv.despsel.getFormData()%>' 
maxLength='<%=sv.despsel.getLength()%>' 
size='<%=sv.despsel.getLength()%>' style="width: 65px !important"
onFocus='doFocus(this)' onHelp='return fieldHelp(despsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.despsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 

<%
	}else if((new Byte((sv.despsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
	<span class="input-group-btn" style="right: 11px !important; paddin-left:15px">
										<button class="btn btn-info" style="font-size: 18px;" type="button" onClick="doFocus(document.getElementById('despsel')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
					</span>

<%
	}else { 
%>

class = ' <%=(sv.despsel).getColor()== null  ? 
"input_cell" :  (sv.despsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	<span class="input-group-btn" style="right: 11px !important; paddin-left:15px">
										<button class="btn btn-info" style="font-size: 18px;" type="button" onClick="doFocus(document.getElementById('despsel')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
	</span>

<%}} %>
 --%>

<!-- </div> -->

 <%
                                         if ((new Byte((sv.despsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.despsel)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.despsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('despsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 




	                    </td>
						<!-- </td><td style="min-width:100px"> -->

	<td>
  		
		<%		
					fieldItem=appVars.loadF4FieldsLong(new String[] {"despname"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("despname");
			optionValue = makeDropDownList( mappedItems , sv.despname,2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.despname.getFormData()).toString().trim());
						
		if(!((sv.despname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.despname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.despname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell1" %>' style="min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   
	                    </td></tr></table>
				  	</div>  
				  	</div> 
				  	
				  	
				  	<div class="col-md-4">
	                     <div class="form-group">   
	                     <label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>  
	                     <!-- <div class="input-group"> -->
	                     <table><tr><td style="min-width:140px;">                 
	                      <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:140px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:140px;"> 
					<%
					} 
					%>
					
					<select class='sel' name='reasoncd' type='list' 
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.reasoncd).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					onchange="change()"
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					} 
					%>
					</td><td style="
    padding-left: 1px;
">
					<input style="width:200px;" "id='resn' name='resndesc' 
							type='text'
							
							<%
							
									fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("reasoncd");
									optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
									formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
									
							%>
							<%if(formatValue==null) {
								formatValue="";
							}
							 %>
							 
							 <% String str=(sv.resndesc.getFormData()).toString().trim(); %>
										<% if(str.equals("") || str==null) {
											str=formatValue;
										}
										
									%>
							 
								value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
							
							size='50'
							maxLength='50' 
							 
							
							
							<% 
								if((new Byte((sv.resndesc).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.resndesc).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.resndesc).getColor()== null  ? 
										"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							</td></tr></table>
					<!-- </div> -->
				</div>
				</div>
					   
				  </div>
				   
		</div>	
</div>	
	




<%@ include file="/POLACommon2NEW.jsp"%>

