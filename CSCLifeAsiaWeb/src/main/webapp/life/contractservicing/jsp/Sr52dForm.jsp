<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR52D";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr52dScreenVars sv = (Sr52dScreenVars) fw.getVariables();%>




<div class="panel panel-default">

<div class="panel-body">  




			 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Company:")%></label>
                    
                         <div class="form-group" style="width:50px">
                         
                     
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
   </div></div></div>
   
   <div class="col-md-1"> </div>   
   
   <div class="col-md-3"> 
			    	     <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table:")%></label>
                       
   

<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>


   <div class="col-md-1"> </div>  

<div class="col-md-4"> 
			    	     <div class="form-group">
                        
                        
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item:")%></label>
                        
                 <table><tr><td>       
                        
                        
                        
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
</div></div>

</div>


<div class="row">

<div class="col-md-3"> 
			    	     <div class="form-group">
                        
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Tax Rate Code :")%></label>
                     <div class="input-group" style="mib-width:250px" >
                        

 <% if ((new Byte((sv.txcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"txcode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("txcode");
	optionValue = makeDropDownList( mappedItems , sv.txcode.getFormData(),4);  
	longValue = (String) mappedItems.get((sv.txcode.getFormData()).toString().trim());
	int colWidth = 0; 
	int dropDownDescWidth=0;
	dropDownDescWidth = ((sv.txcode.getFormData()).length()*12)+88;	
%>
<% 
	if((new Byte((sv.txcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>' >  
	<%if(longValue != null){%>

	<%=longValue%>

	<%}%>
</div>

<%
longValue = null;
%>

<% }else {%> 

			<%=smartHF.getDropDownExt(sv.txcode, fw, longValue, "txcode", optionValue) %>
			<%if(longValue == null || "".equalsIgnoreCase(longValue)){%>	
				<script>getDefaultTwo(document.getElementById('txcodediscr'),<%=dropDownDescWidth-20%>);</script>
				<!-- ILIFE 2732 ends -->
			<%}%>
		
<% }} %>
	
	</div></div></div> 
                        
                        
                        
                        <div class="col-md-1"> </div>   
                <div class="col-md-3"> 
			    	     <div class="form-group">
                        
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Calculation Program :")%></label>
                      
        <div class="input-group" style="min-width:71px;">
                       <%=smartHF.getRichText(0,0,fw,sv.txsubr,( sv.txsubr.getLength()+1),null).replace("absolute","relative")%>
                                
      </div>    </div>        </div>      
                        
                        
   
   </div>
   
   
   
   
   </div></div>



<%@ include file="/POLACommon2NEW.jsp"%>
