<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR574";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr574ScreenVars sv = (Sr574ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind03.isOn()) {
	sv.hflag.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind02.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
}

}%>

<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts -->
<style>
@media \0screen\,screen\9
{.blank_cell{margin-right:1px;}
}
</style>
<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends -->







<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract ")%></label>
    	 					<div class="input-group">

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>



<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency ")%></label>
    	 				

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>






<div class="col-md-4" > 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status ")%></label>
    	 					

<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>




</div>




			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status ")%></label>
    	 					

<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div>




			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Register ")%></label>

<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div>












<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured  ")%></label>
<div class="input-group">
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div></div>




</div>




	<div class="row">
<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life    ")%></label>
<div class="input-group">
<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.hflag).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.hflag.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div></div>


</div>







<br>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-s5011'>
               
               <thead>
		
			        <tr class='info'>
			      
			        <th ><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
		         	<th ><%=resourceBundleHandler.gettingValueFromBundle("CMPNTNUM")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Component")%></th>
					<th ><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
		         	<th ><%=resourceBundleHandler.gettingValueFromBundle("Risk Stat")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Prem Stat")%></th>		     
		 	        
		 	        </tr>
			 </thead>
			 
			
			
			<%
		GeneralTable sfl = fw.getTable("sr574screensfl");
		int height;
		if(sfl.count()*27 > 180) {
		height = 180 ;
		} else {
		height = sfl.count()*27;
		}	
		%>	
			
			
			 <tbody>
<%
	Sr574screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr574screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									
		    									
		    									
					<td>
																						
					 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr574screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr574screensfl.select_R<%=count%>'
						 id='sr574screensfl.select_R<%=count%>'
						 onClick="selectedRow('sr574screensfl.select_R<%=count%>')"
						 class="UICheck"
					 />
					 
					 					
					
											
					</td>
		<%}else{%>
					<td>
					</td>														
										
					<%}%>
								<%if((new Byte((sv.cmpntnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    			
		    		<td>									
								
											
									
											
						<%= sv.cmpntnum.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
					<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.component).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									
		          <td>									
								
											
									
											
						<%= sv.component.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
							<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.deit).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    			
		    			
		  				<td>									
								
											
									
											
						<%= sv.deit.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.statcode).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									
		    									
		    									
		    	<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.statcode).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.statcode);
							if(!(sv.statcode).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
						<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.pstatcode).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									
		    									
		    									
		    	
                    <td >									
								
											
									
											
						<%= sv.pstatcode.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
						<td>
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr574screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

</tbody></table></div></div></div></div>




</div></div>


































































































<%-- 
<div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='271'>
<!--
<%StringData CHDRNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract:");%>
<%=smartHF.getLit(0, 0, CHDRNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract ")%>
</div>
<br/>
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='151'>
<!--
<%StringData CNTCURR_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency:");%>
<%=smartHF.getLit(0, 0, CNTCURR_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Currency ")%>
</div>
<br/>
<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData CHDRSTATUS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status:");%>
<%=smartHF.getLit(0, 0, CHDRSTATUS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Status ")%>
</div>
<br/>
<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData PREMSTATUS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status:");%>
<%=smartHF.getLit(0, 0, PREMSTATUS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Premium Status ")%>
</div>
<br/>
<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='51'>
<!--
<%StringData REGISTER_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register:");%>
<%=smartHF.getLit(0, 0, REGISTER_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Register ")%>
</div>
<br/>
<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='471'>
<!--
<%StringData LIFENUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured  :");%>
<%=smartHF.getLit(0, 0, LIFENUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life Assured  ")%>
</div>
<br/>
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='750'>
<!--
<%StringData JLIFENUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life   :");%>
<%=smartHF.getLit(0, 0, JLIFENUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Joint Life    ")%>
</div>
<br/>
<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.hflag).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.hflag.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->

<td></td>
</tr> </table>
<br/>



<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Sel").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Sel").length())*12;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("CMPNTNUM").length() >= (sv.cmpntnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("CMPNTNUM").length())*15;								
			} else {		
				calculatedValue = (sv.cmpntnum.getFormData()).length()*15;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Component").length() >= (sv.component.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Component").length())*8;								
			} else {		
				calculatedValue = (sv.component.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Description").length() >= (sv.deit.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Description").length())*10;								
			} else {		
				calculatedValue = (sv.deit.getFormData()).length()*10;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Risk Stat").length() >= (sv.statcode.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Risk Stat").length())*8;								
			} else {		
				calculatedValue = (sv.statcode.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Prem Stat").length() >= (sv.pstatcode.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Prem Stat").length())*20;								
			} else {		
				calculatedValue = (sv.pstatcode.getFormData()).length()*20;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("sr574screensfl");
		int height;
		if(sfl.count()*27 > 180) {
		height = 180 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts -->

<div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" class ="tablePos" style='top:21px; width: <%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%> height: 205px; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 180 ) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		
		<DIV id="subf" style="POSITION: relative; WIDTH: <%=totalTblWidth%>px; HEIGHT: 205px;">
		<table style="width:<%=totalTblWidth%>px; margin-top:-2px; margin-left:-1px;" bgcolor="#dddddd" cellspacing="1px" class="tableTag" id="table">		
		
		<tr style="height: 25px;" class="tableRowHeader">
														
														
														
														
										<td  class="tableColHeaderFixed" style="width:<%=tblColumnWidth[0 ]%>px; margin-top:-2px;" align="left">
					<div class="tableDataHeader tableRowHeaderDivFixed" style="width:<%=tblColumnWidth[0 ] + 8 %>px; border-right: 1px solid #dddddd; "  align="center">
						<%=resourceBundleHandler.gettingValueFromBundle("Sel")%>
					</div>
				</td>				
														
														
														
														
														
														<td class="tableDataHeader " style="width:<%=tblColumnWidth[0 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></td>									
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[1 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("CMPNTNUM")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[2 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Component")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[3 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[4 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Risk Stat")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[5 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Prem Stat")%></td>
										
				
			
		</tr>
		
		<style type="text/css">
.fakeContainer {
	width:720px;		
	height:230px;	/*ILIFE-2679*/
	top: 190px;
	left:2px;		
		
}
.sSky th, .sSky td{
font-size:12px !important;
text-align:center;    /*ILIFE-2560*/
}
.sr574Table tr{height:35px}
</style>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr574Table", {
				fixedCols : 0,					
				colWidths : [50,80,140,300,140,140],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<div id="bottomCover" class="bottomCover">&nbsp;</div>

<div id="topCover" class="topCover">&nbsp;</div>

<div class="fakeContainer" id="container">
<table id="sr574Table" class="sr574Table">
  <tr height="35">
		
		            
		            <th ><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
		         	<th ><%=resourceBundleHandler.gettingValueFromBundle("CMPNTNUM")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Component")%></th>
					<th ><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
		         	<th ><%=resourceBundleHandler.gettingValueFromBundle("Risk Stat")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Prem Stat")%></th>
					
					
	</tr> 
		
		<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends -->
	<%
	Sr574screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr574screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
					<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
								
													
													
					
					 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr574screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr574screensfl.select_R<%=count%>'
						 id='sr574screensfl.select_R<%=count%>'
						 onClick="selectedRow('sr574screensfl.select_R<%=count%>')"
						 class="UICheck"
					 />
					 
					 					
					
											
									</td>
		<%}else{%>
												<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
					</td>														
										
					<%}%>
								<%if((new Byte((sv.cmpntnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" 
					<%if((sv.cmpntnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.cmpntnum.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.component).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
					<%if((sv.component).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.component.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.deit).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
					<%if((sv.deit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.deit.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px; text-align:right;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.statcode).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
					<%if((sv.statcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.statcode).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.statcode);
							if(!(sv.statcode).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.pstatcode).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
					<%if((sv.pstatcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.pstatcode.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr574screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
</DIV>
	
<br/>

<INPUT type="HIDDEN" name="hcoverage" id="hcoverage" value="<%=	(sv.hcoverage.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="hlifeno" id="hlifeno" value="<%=	(sv.hlifeno.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="hrider" id="hrider" value="<%=	(sv.hrider.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="hsuffix" id="hsuffix" value="<%=	(sv.hsuffix.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="screenIndicArea" id="screenIndicArea" value="<%=	(sv.screenIndicArea.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="subfilePosition" id="subfilePosition" value="<%=	(sv.subfilePosition.getFormData()).toString() %>" >

<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%@ include file="/POLACommon2NEW.jsp"%>
