

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5153";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5153ScreenVars sv = (S5153ScreenVars) fw.getVariables();%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%String color = sv.splitBcomm.getColor(); %>
<%{
		if (appVars.ind01.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.splitBcomm.setReverse(BaseScreenData.REVERSED);
			sv.splitBcomm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.splitBcomm.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Agency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage Split");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission");%>
<%		appVars.rollup(new int[] {93});
%>
<!-- ILIFE-2722 Life Cross Browser - Sprint 4 D5 : Task 2  starts-->

<script>
$(document).ready(function(){
	
	
	

})
</script>
<!-- ILIFE-2722 Life Cross Browser - Sprint 4 D5 : Task 2  ends-->
<div class="panel panel-default">
 <div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
					<tr>
					<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div id="chdrnum"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' 
									 >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	longValue = null;
								 	formatValue = null;
								 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div id="cnttype" class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
								: "output_cell"%>' style="margin-left:1px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	longValue = null;
								 	formatValue = null;
								 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div id="ctypedes"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
								: "output_cell"%> ' style="margin-left:1px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							</tr>
							</table>

				</div>
			</div>
			<div class="col-md-4"> </div>
			<div class="col-md-4"> </div>
		</div>

		<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
</label>

<table><tr><td>
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="cownnum" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>





	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="ownername" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table></div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>


 	<div class="row">	
	    	<div class="col-md-4"> 
		    		<div class="form-group">

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Servicing Agency")%>
</label>

<table><tr><td>
	
  		
		<%					
		if(!((sv.sellagent.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sellagent.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sellagent.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="sellagent" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="agentname" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td></tr></table>
</div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>
		
<br>		


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[3];
int totalTblWidth = 0;
int calculatedValue =0;

																			if((resourceBundleHandler.gettingValueFromBundle("Agency").length()*12) >= (sv.agntsel.getFormData()).length()*12+32+16 ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Agency").length())*12;								
						} else {		
							calculatedValue = ((sv.agntsel.getFormData()).length()*12)+32+16;								
						}		
									totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Percentage Split Commission").length()*24 >= (sv.splitBcomm.getFormData()).length()*24+32 ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Percentage Split Commission").length())*24;								
						} else {		
							calculatedValue = ((sv.splitBcomm.getFormData()).length()*24)+32;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
		
		tblColumnWidth[0] = 120; 
		tblColumnWidth[1] = 280; 
		tblColumnWidth[2] = 30;
		int iwidth = 45;
		totalTblWidth = tblColumnWidth[0]+tblColumnWidth[1]+tblColumnWidth[2]+10;
			%>
		<%
		GeneralTable sfl = fw.getTable("s5153screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
        <div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id='dataTables-s5153' width='100%'>
	                         	<thead>
	                            	<tr class='info'>	
		
					<th>  <center>  <%=resourceBundleHandler.gettingValueFromBundle("Select")%></center> </th>
		         								
					<th>  <center><%=resourceBundleHandler.gettingValueFromBundle("Agency")%>        </center> </th>
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Percentage Split Commission")%>     </center> </th>
				 </tr>
              </thead>
              <tbody>
<!-- ILIFE-2722 Life Cross Browser - Sprint 4 D5 : Task 2 fake container ends -->		
		
	<%
	S5153screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5153screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" style="<%if (count>1 && sv.agntsel.getFormData().trim().equals("")) {%>visibility: hidden; display: none<%} %>" id='<%="tablerow"+count%>' >
		<td class="tableDataTag " align="left">
		<div class="form-group">
				<INPUT type="checkbox" name="chk_R" id='<%="chk_R"+count %>' class="UICheck" />
				</div>
			</td>
									<td class="tableDataTag "
										style="width:400px;" align="left">
										<div class="form-group">

										<div class="input-group">
											<span class="input-group-btn"> <!-- ILIFE-680 START -- Screen S5153 - Fields should not be editable in enquiry mode -->
												<input
												name='<%="s5153screensfl" + "." +
						 "agntsel" + "_R" + count %>'
												id='<%="s5153screensfl" + "." +
						 "agntsel" + "_R" + count %>'
												type='text' value='<%= sv.agntsel.getFormData() %>'
												<%if(((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (sv.isScreenProtected())){%>
												readonly="true" class="output_cell"> <%
							}else {
						%> class = " <%=(sv.agntsel).getColor()== null  ? 
						"input_cell" :  
						(sv.agntsel).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" maxLength='<%=sv.agntsel.getLength()%>'
												onFocus='doFocus(this)' onHelp='return
												fieldHelp(s5153screensfl.agntsel)' onKeyUp='return
												checkMaxLength(this)' style = "width: 95px;" >

												<button class="btn btn-info" type="button"
													
													onClick="doFocus(document.getElementById('<%="s5153screensfl" + "." +
						 "agntsel" + "_R" + count %>'));doAction('PFKEY04');">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button> <% } %> <!-- ILIFE-680 END -- Screen S5153 - Fields should not be editable in enquiry mode -->
											</span>
										</div>
										</div>
									</td>
									<td class="tableDataTag" style="width:780px;" align="center" >									
																	
									<div class="form-group">				
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.splitBcomm).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.splitBcomm);
							
						%>					
						
						
						<!-- ILIFE-680 START -- Screen S5153 - Fields should not be editable in enquiry mode -->	
						<input type='text' 
						 maxLength='<%=sv.splitBcomm.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.splitBcomm.getLength()%>'
						 
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5153screensfl.splitBcomm)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5153screensfl" + "." +
						 "splitBcomm" + "_R" + count %>'
						 id='<%="s5153screensfl" + "." +
						 "splitBcomm" + "_R" + count %>' 
						 <%if(((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (sv.isScreenProtected())){%>
										readonly="true" class="output_cell">
						 <%
							}else {
						 %>	
						 class = ' <%=color== null  ? 
							"input_cell" :  color.equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'

						  style = "width: 100px;text-align: right" 
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							<%--onBlur='return doBlurNumber(event);' --%>
						  title='<%=formatValue %>'
						 >
						 <% } %>
						 <!-- ILIFE-680 END -- Screen S5153 - Fields should not be editable in enquiry mode -->			
					
									</div>		
									</td>
					
							</tr>
						
							<%
							count = count + 1;
							S5153screensfl
							.setNextScreenRow(sfl, appVars, sv);
							}
							%>
                            </tbody>
	                     </table>
	                 </div>
	             </div>
	         </div>
	     </div>


		
	<% if(!sv.isScreenProtected()){ %>
	
	
	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="btn-group">
						<div class="sectionbutton">
							<div style="font-size: 12px; font-weight: bold;">
								<a id="subfile_add" class="btn btn-success" href='javascript:;'>
								<%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<a id="subfile_remove" class="btn btn-danger" href='javascript:;' disabled>
								<%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<% } %>
	
	
</div></div>



<script>
$(document).ready(function() {
    $('#dataTables-s5153').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false
       
    } );
   
} );

</script>

<%@ include file="/POLACommon2NEW.jsp"%>


