
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH593";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sh593ScreenVars sv = (Sh593ScreenVars) fw.getVariables();%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date    ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Summary Totals");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life   ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loans          ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prin O/S ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm Date  ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-To-Date    ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date  ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"INDIVIDUAL LOAN DETAILS");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Start");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Curr");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Principal");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accrued");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pending");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duty");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No.Type");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Balance");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stamp");%>
<%		appVars.rollup(new int[] {93});
	 
%>
<%{
		if (appVars.ind01.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
	
	}

	%>
<div class="panel panel-default">
    	
    	<div class="panel-body">    
    	
    	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		     <table><tr><td>
						    		
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  
	</td><td>
	
  		
									<%					
									if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
  
	</td><td>

  		
								<%					
									if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width:200px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
						  
	</td></tr></table>
				    		</div>
					</div></div>
    	
    	
			<div class="row">	
			    	<div class="col-md-4" >
						<div class="form-group">	
							  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					    		    <table><tr><td>
	<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	</td></tr></table>

				      			     
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<div class="input-group">
						    			
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4" style="max-width:600px;">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group">
						    		
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("currcd");
	optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.currcd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='currcd' type='list' style="width:175px;"
<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.currcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.currcd).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				  
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					    		    <table><tr><td>
						    		
  		
								<%					
								if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'  style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
	</td><td>

								<%					
								if(!((sv.ownerdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownerdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownerdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
	
						    		
</td></tr></table>
				    		</div>
					</div>
				    		
				    		
				
		    </div>
			
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					    		     <table><tr><td>
						    			<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>
	
  		
		<%					
		if(!((sv.lifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>

				      			     
				    		</div>
					</div></div>
				    		
				   
				   <div class="row">
				    <div class="col-md-4"  style="max-width:200px;">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
   <table><tr><td>					    			
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>
  		
		<%					
		if(!((sv.jlifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px; min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		</td></tr></table>

				      			   
						</div>
				   </div></div>
					
			
			    	<div class="row"> 
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Comm Date")%></label>
							<div class="input-group">
						    				
		<%					
		if(!((sv.currfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
										
						</div>
				   </div>	
				   
			    	
			    	 <div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-To-Date")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
										
						</div>
				   </div>	
				   
			    	 <div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					    		     <div class="input-group">
						    		
		<%	
			qpsf = fw.getFieldXMLDef((sv.intanny).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.intanny);
			
			if(!((sv.intanny.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				   
			    	
		    </div>
		    
		    
	
		    
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#contact_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Loan Summary Totals")%></label></a>
						</li>
						<li><a href="#other_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Individual Loan Details")%></label></a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="contact_tab">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Loans")%></label>
												<div class="input-group" style="max-width:200px;min-width:100px"> 
												
		<%	
			qpsf = fw.getFieldXMLDef((sv.numberOfLoans).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.numberOfLoans);
			
			if(!((sv.numberOfLoans.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

										
									</div></div>
								</div>
								
								
								<div class="col-md-4">
							      <div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Prin O/S")%></label>
												<div class="input-group" style="max-width:200px;min-width:100px"> 
										<%	
			qpsf = fw.getFieldXMLDef((sv.hpleamt).getFieldName());
		//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.hpleamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.hpleamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
									</div>
								</div>
							</div></div>
							
							
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Interest")%></label>
												<div class="input-group" style="max-width:200px;min-width:100px"> 
										<%	
			qpsf = fw.getFieldXMLDef((sv.hpleint).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.hpleint,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.hpleint.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
									</div>
								</div></div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
												<div class="input-group" style="max-width:200px;min-width:100px"> 
										<%	
			qpsf = fw.getFieldXMLDef((sv.hpletot).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.hpletot,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.hpletot.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
 
	
									</div></div>
								</div></div>
								
								
								
							</div>
						
						
						
						<div class="tab-pane fade" id="other_tab"  class="table-responsive">
							<!-- <div class="row">
								<div class="col-md-12">
									<div class="form-group"> -->
										       <!-- <div id='content2' style='display:none;' class="table-responsive"> -->
	         <table class="table table-striped table-bordered table-hover "  width="100%"
							id='dataTables-sh593'>
               <thead>
		
			        <tr class="info">
			     
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Loan Number")%></th>
	    <th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Loan Type ")%></th>    								
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Start Date")%></th>
		<% if (sv.cslnd001Flag.compareTo("N") != 0) { %> 
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Next Int. Billing Dt")%></th>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Next Cap. Date")%></th>
		<% }%>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
	    <th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Principal Amount")%></th>    								
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Current Balance")%></th>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Accrued Interest")%></th>
	    <th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Pending Interest")%></th>    	
	    <%if (sv.actionflag.compareTo("N") != 0) {%>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Total Repaid Amt")%></th><%}%>							
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Total")%></th>
		<%if (sv.actionflag.compareTo("N") != 0) {%>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Repaid Status")%></th>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Repayment Date")%></th>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Repayment Method")%></th><%}%>
		<th style="text-align:center; min-width:120px;"><%=resourceBundleHandler.gettingValueFromBundle("Duty Stamp")%></th>
	    
		</tr>
		    
		    
			 </thead>
	
<tbody>		 


 <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[16];
int totalTblWidth = 0;
int calculatedValue =0;
			if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.loanNumber.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*10;								
		} else {		
			calculatedValue = (sv.loanNumber.getFormData()).length()*10;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[0]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.loanType.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
		} else {		
			calculatedValue = (sv.loanType.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[1]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.loanStartDateDisp.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
		} else {		
			calculatedValue = (sv.loanStartDateDisp.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[2]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.nxtintbdteDisp.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
		} else {		
			calculatedValue = (sv.nxtintbdte.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[3]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.nxtcapdateDisp.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
		} else {		
			calculatedValue = (sv.nxtcapdate.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[4]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.cntcurr.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*8;								
		} else {		
			calculatedValue = (sv.cntcurr.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[5]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.hprincipal.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
		} else {		
			calculatedValue = (sv.hprincipal.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[6]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.hcurbal.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*8;								
		} else {		
			calculatedValue = (sv.hcurbal.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[7]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.hacrint.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
		} else {		
			calculatedValue = (sv.hacrint.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[8]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header10").length() >= (sv.hpndint.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header10").length())*8;								
		} else {		
			calculatedValue = (sv.hpndint.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[9]= calculatedValue;
	if(resourceBundleHandler.gettingValueFromBundle("Header11").length() >= (sv.hpltot.getFormData()).length() ) {
		calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header11").length())*12;								
	} else {		
		calculatedValue = (sv.hpltot.getFormData()).length()*12;								
	}		
	totalTblWidth += calculatedValue;
tblColumnWidth[10]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header12").length() >= (sv.numcon.getFormData()).length() ) {
	calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header12").length())*12;								
} else {		
	calculatedValue = (sv.numcon.getFormData()).length()*12;								
}		
totalTblWidth += calculatedValue;
tblColumnWidth[11]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header13").length() >= (sv.totrepd.getFormData()).length() ) {
	calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header13").length())*12;								
} else {		
	calculatedValue = (sv.totrepd.getFormData()).length()*12;								
}		
totalTblWidth += calculatedValue;
tblColumnWidth[12]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header14").length() >= (sv.repdstat.getFormData()).length() ) {
	calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header14").length())*8;								
} else {		
	calculatedValue = (sv.repdstat.getFormData()).length()*8;								
}		
totalTblWidth += calculatedValue;
tblColumnWidth[13]= calculatedValue;
//CML075-Start
if(resourceBundleHandler.gettingValueFromBundle("Header15").length() >= (sv.repymop.getFormData()).length() ) {
	calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header15").length())*8;								
} else {		
	calculatedValue = (sv.repymop.getFormData()).length()*8;								
}		
totalTblWidth += calculatedValue;
tblColumnWidth[14]= calculatedValue;
if(resourceBundleHandler.gettingValueFromBundle("Header16").length() >= (sv.repdateDisp.getFormData()).length() ) {
	calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header16").length())*8;								
} else {		
	calculatedValue = (sv.repdateDisp.getFormData()).length()*8;								
}		
totalTblWidth += calculatedValue;
tblColumnWidth[15]= calculatedValue;
//CML075-End
	%>
	
<%
		GeneralTable sfl = fw.getTable("sh593screensfl");
		int height;
		if(sfl.count()*27 > 90) {
		height = 90 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>

		
<%

	
	Sh593screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sh593screensfl
	.hasMoreScreenRows(sfl)) {
	
%>

		





	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    									<td class="tableDataTag tableDataTagFixed" style="width:200px; font-weight: bold;" >
																	<%= formatValue( sv.loanNumber.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[1]%>px;font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.loanType.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[2]%>px;font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.loanStartDateDisp.getFormData() )%>	 
										
				 
				
					</td>	
					<!-- CML075 Start-->					
					<% if (sv.cslnd001Flag.compareTo("N") != 0) { %> 
													<td style="width:<%=tblColumnWidth[3]%>px;font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.nxtintbdteDisp.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[4]%>px;font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.nxtcapdateDisp.getFormData() )%>	 
										
				 
				
					</td>
					<%} %>	
					<!-- CML075 End-->
													<td style="width:<%=tblColumnWidth[5]%>px;font-weight: bold;" align="left">									
										
										&nbsp;	<%= formatValue( sv.cntcurr.getFormData() )%>	 
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[6]%>px;font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.hprincipal).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.hprincipal,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[7]%>px;font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.hcurbal).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.hcurbal,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[8]%>px;font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.hacrint).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.hacrint,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
										
				 
				
					</td>	
													<td style="width:<%=tblColumnWidth[9]%>px;font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.hpndint).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.hpndint,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
										
				 
				
					</td>	
						<%if (sv.actionflag.compareTo("N") != 0) {		
						%>
						<td style="width:<%=tblColumnWidth[10]%>px;font-weight: bold;" align="right">									
										
				 		<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.totrepd).getFieldName());						
									
						%>		
						<%= formatValue( smartHF.getPicFormatted(qpsf,sv.totrepd,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;		 		
			
					</td>
					<%} %>
						<td style="width:<%=tblColumnWidth[12]%>px;font-weight: bold;" align="right">									
										
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.hpltot).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);				
					%>						 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.hpltot,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
										
				 
				
					</td>	
					
	<%if (sv.actionflag.compareTo("N") != 0) {		
				%>
		
		<td style="width:<%=tblColumnWidth[13]%>px;font-weight: bold;" align="left">									
							 		
			 	  	&nbsp;	<%= formatValue( sv.repdstat.getFormData() )%>	 
	   </td>
	
	  <td style="width:<%=tblColumnWidth[15]%>px;font-weight: bold;" align="left">									
										
				&nbsp;	<%= formatValue( sv.repdateDisp.getFormData() )%>	 	
	 </td>
	    <td style="width:<%=tblColumnWidth[14]%>px;font-weight: bold;" align="left">									
										
							 		
			 	 	&nbsp;	<%= formatValue( sv.repymop.getFormData() )%>	 
	  </td>
		<%} %>
		
					<!-- ILIFE-2371 Starts  -->
					
					 
					<td style="width:<%=tblColumnWidth[11]%>px;font-weight: bold;" align="right">									
										
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.numcon).getFieldName());						
									
					%>						 		
			 	   <%= formatValue( smartHF.getPicFormatted(qpsf,sv.numcon,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) )%> &nbsp;
				   </td>	
				 
				 
		           <!-- ILIFE-2371 Ends  -->
	</tr>
<%	
	count = count + 1;
	Sh593screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%></tbody>
	</table>    
	
	</div>
													</div>
												</div>
												
				<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			 
												
											</div>
														
				
		</div>  
</div> 

<script>
	$(document).ready(function() {
		 var showval= document.getElementById('show_lbl').value;
		var toval= document.getElementById('to_lbl').value;
		var ofval= document.getElementById('of_lbl').value;
		var entriesval= document.getElementById('entries_lbl').value;	
		var nextval= document.getElementById('nxtbtn_lbl').value;
		var previousval= document.getElementById('prebtn_lbl').value; 
    	$('#dataTables-sh593').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
			scrollX: true,
			  language: {
		            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
		            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
		            "paginate": {                
		                "next":       nextval,
		                "previous":   previousval
		            }
		          }      
      	});
    });
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

