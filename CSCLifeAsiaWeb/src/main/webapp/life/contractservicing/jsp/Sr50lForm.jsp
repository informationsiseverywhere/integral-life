
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR50L";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr50lScreenVars sv = (Sr50lScreenVars) fw.getVariables();%>



	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1-Select");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm.Date");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date");%>
<%		appVars.rollup(new int[] {93});
%>



<div class="panel panel-default">
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4">
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		
					    		<table><tr><td>
					    		<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td><td style="padding-left:1px;">




<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td></tr></table>
					      	        
			                    					    		
				      		</div>
				    </div>
         </div>
         <br>
         <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
	         <table  id='dataTables-sr50l' class="table table-striped table-bordered table-hover"  width="100%">
               <thead>
		
			        <tr class="info">
			        <th><center><%=resourceBundleHandler.gettingValueFromBundle(" ")%></center></th>
			        <th><center><%=resourceBundleHandler.gettingValueFromBundle("Select")%></center></th>
	  <th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></center></th>
		  <th><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>
		  <th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Commence Date")%></center></th>
		  <th><center><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></center></th>
	
		 	        </tr>
			 </thead>
			  <tbody>
			 

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;


						if(resourceBundleHandler.gettingValueFromBundle("1-Select").length() >= (sv.ind.getFormData()).length() ) {
//ILIFE-1712 STARTS by smahalaxmi 
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("1-Select").length())*7;								
			} else {		
				calculatedValue = (sv.ind.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Select").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Select").length())*6;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*6;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Contract No").length() >= (sv.chdrnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Contract No").length())*12;								
			} else {		
				calculatedValue = (sv.chdrnum.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Status").length() >= (sv.status.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Status").length())*23;								
			} else {		
				calculatedValue = (sv.status.getFormData()).length()*23;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Risk Comm.Date").length() >= (sv.ccdateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Risk Comm.Date").length())*12;								
			} else {		
				calculatedValue = (sv.ccdateDisp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Paid To Date").length() >= (sv.ptdateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Paid To Date").length())*12;								
			} else {		
				calculatedValue = (sv.ptdateDisp.getFormData()).length()*12;								
			}
//ILIFE-1712 Ends
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("sr50lscreensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>

  <%
	Sr50lscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr50lscreensfl
	.hasMoreScreenRows(sfl)) {
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
%>

		







	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.ind).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
					<%if((sv.ind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
								
											
									
											
						<%= sv.ind.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
					</td>														
										
					<%}%>
								
								<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" align="center" width="<%=tblColumnWidth[1]%>px;">				
					
					
					
<input type='checkbox' name='sr50lscreensfl.select_R<%=count%>' value='<%=sv.select.getFormData()%>' onFocus='doFocus(this)'  onFocus='doFocus(this)' onHelp='return fieldHelp("sr50lscreensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						id='sr50lscreensfl.select_R<%=count%>' 

class ='UICheck' onclick="selectedRow('sr50lscreensfl.select_R<%=count%>')" 
<%if((sv.select).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
disabled="disabled" 
<%}
if((sv.select).getFormData().toString().trim().equalsIgnoreCase("1")){%>
checked<%}%>/>
				</td>
		<%}else{%>
		<td class="tableDataTag" align="center" width="<%=tblColumnWidth[1]%>px;"></td>
					<%}%>
								<%if((new Byte((sv.chdrnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;" 
					<%if((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.chdrnum.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.status).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
					<%if((sv.status).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.status.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.ccdateDisp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
					<%if((sv.ccdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.ccdateDisp.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.ptdateDisp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
					<%if((sv.ptdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.ptdateDisp.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr50lscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	
	</tbody>
		</table>
		</div>



</div></div>

<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">

</div>


</div></div> 
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;	
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	$('#dataTables-sr50l').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }
          }     
        
  	});
})
</script>


<%@ include file="/POLACommon2NEW.jsp"%>

