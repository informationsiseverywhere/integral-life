

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5081";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5081ScreenVars sv = (S5081ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Owner    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commence ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of Payment ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"--------------------------------------------------------------------------------");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New Owner ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New Joint Owner ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason ");%>


	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Relationship with Life Assured ");%>
	
<%{
		if (appVars.ind04.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
		
		
		 /* ILIFE-7277-start  */
		if (sv.multipleOwnerFlag.toString().trim().equals("Y")) {
			sv.clntwin2.setInvisibility(BaseScreenData.INVISIBLE);			
		}
		if (sv.multipleOwnerFlag.toString().trim().equals("Y")) {
			sv.clntwin3.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (sv.multipleOwnerFlag.toString().trim().equals("Y")) {
			sv.clntwin4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (sv.multipleOwnerFlag.toString().trim().equals("Y")) {
			sv.clntwin5.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 /* ILIFE-7277-end  */
		 
		 /* ILIFE-7690-starts  */
		if (sv.multipleOwnerFlag.toString().trim().equals("Y")) {
		sv.cownnum2.setInvisibility(BaseScreenData.INVISIBLE);
		sv.cownnum3.setInvisibility(BaseScreenData.INVISIBLE);
		sv.cownnum4.setInvisibility(BaseScreenData.INVISIBLE);
		sv.cownnum5.setInvisibility(BaseScreenData.INVISIBLE);
		sv.ownername2.setInvisibility(BaseScreenData.INVISIBLE);
		sv.ownername3.setInvisibility(BaseScreenData.INVISIBLE);
		sv.ownername4.setInvisibility(BaseScreenData.INVISIBLE);
		sv.ownername5.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 /* ILIFE-7690-ends  */
		 
		 if (appVars.ind30.isOn()) {
			sv.relationwithlife.setColor(BaseScreenData.RED);
			sv.relationwithlife.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.relationwithlife.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.relationwithlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.relationwithlife.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		
		 if (appVars.ind33.isOn()) {
				sv.relationwithlifejoint.setColor(BaseScreenData.RED);
				sv.relationwithlifejoint.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind34.isOn()) {
				sv.relationwithlifejoint.setEnabled(BaseScreenData.DISABLED);
			}
			if (!appVars.ind33.isOn()) {
				sv.relationwithlifejoint.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind35.isOn()) {
				sv.relationwithlifejoint.setInvisibility(BaseScreenData.INVISIBLE);
			}
		
		
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
                       <div class="form-group">    	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
	                      <table><tr><td>
	                      		<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
				  		</td><td style="padding-left:1px;">
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="max-width: 50px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
				  		</td><td style="padding-left:1px;">
								<%					
								if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %> ' style="max-width: 300px; min-width: 120px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  </td></tr></table>
	
	                     
	                  </div>
	              </div>
	             </div>
	              
	                <div class="row">
                      <div class="col-md-4">
                     	 <div class="form-group">    	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
	                      <%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width:100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
	                    </div>
	                   </div> 
	                    
	                    <div class="col-md-4">	  
	                     <div class="form-group">                    
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
	                        <div class="input-group">             
	                      <%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
	                    </div></div>
	              </div>
	            </div>
	              
	              
	               <div class="row">
                      <div class="col-md-4">
                       	<div class="form-group">  	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
	                      <table><tr><td>	 
	                      <%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> '>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td style="padding-left:1px;max-width:145px;">
							<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </td></tr></table>
	                   </div>
	                  </div> 
	                  
	                   <div class="col-md-4">	
	                   	<div class="form-group">                       
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Owner")%></label>
	                         <table><tr><td>	 
	                       <%					
								if(!((sv.jownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %> ' style="min-width:80px">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  </td><td style="padding-left:1px;">
							<%					
								if(!((sv.jownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %> ' style="min-width:80px;max-width:100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
	                      </td></tr></table>
	                   </div>
	                  </div>
	                  
	                   <div class="col-md-4">	
	                   	 	<div class="form-group">                      
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
	                          <table><tr><td>
	                      <%					
						if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %> '>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						 </td><td style="padding-left:1px;">
				 	<%					
						if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;max-width:145px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  						</td></tr></table>
	
	                   </div>
	              </div>
	             </div>
	               <!-- ILIFE-7277-start -->
	             <div class="row">
                     <div class="col-md-4">
                      	<div class="form-group">  	   
                      		<%
                      		if ((new Byte((sv.cownnum2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>                
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 2")%></label>
	                     
	                      
	                      <table><tr><td>	 
	                      <%					
							if(!((sv.cownnum2.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' style="min-width:80px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td style="padding-left:1px;max-width:145px;">
							<%					
							if(!((sv.ownername2.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' style="min-width:80px;max-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </td></tr></table>	
   							<%} %>
	                      </div> 
	                     </div>
	                     
                     <div class="col-md-4">
                      	<div class="form-group">  	   
                      		<%
                      		if ((new Byte((sv.cownnum3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>                
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 3")%></label>
	                     
	                      
	                      <table><tr><td>	 
	                      <%					
							if(!((sv.cownnum3.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> 'style="min-width:80px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td style="padding-left:1px;max-width:145px;">
							<%					
							if(!((sv.ownername3.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' style="min-width:80px;max-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </td></tr></table>
	                	<%} %>
	                      </div> 
	                     </div>
	     
                      <div class="col-md-4">
                      	<div class="form-group">  	   
                      		<%
                      		if ((new Byte((sv.cownnum4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>                
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 4")%></label>
	                     
	                      
	                      <table><tr><td>	 
	                      <%					
							if(!((sv.cownnum4.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> 'style="min-width:80px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td style="padding-left:1px;max-width:145px;">
							<%					
							if(!((sv.ownername4.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' style="min-width:80px;max-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </td></tr></table>
	                	
   							<%} %>
	                      </div> 
	                     </div>
	                  </div>
	             <div class="row">
                     <div class="col-md-4">
                      	<div class="form-group">  	   
                      		<%
                      		if ((new Byte((sv.cownnum5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>                
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Owner 5")%></label>
	                     
	                      
	                      <table><tr><td>	 
	                      <%					
							if(!((sv.cownnum5.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> 'style="min-width:80px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td style="padding-left:1px;max-width:145px;">
							<%					
							if(!((sv.ownername5.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> ' style="min-width:80px;max-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                      </td></tr></table>
	            
   							<%} %>
	                      </div> 
	                     </div>
	                    </div>
	                    <!-- ILIFE-7277-end -->
	               <div class="row">
                      <div class="col-md-4">
                      	<div class="form-group"> 
                      	<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->		                     
	                      <%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
	
	                    </div>
	                    </div> 
	                   
	                     <div class="col-md-4">	  
	                     	<div class="form-group">                    
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
	                      <%					
								if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
     
	                      </div>                      
	              </div>
	              </div> 
	              <div class="row">
                      <div class="col-md-4">
                     	 <div class="form-group">  	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
	                      	 <div class="input-group">  <%			
									fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("billfreq");
									optionValue = makeDropDownList( mappedItems , sv.billfreq,2,resourceBundleHandler);  
									longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
											
								if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
	                      </div> </div>
	                     </div>
	                     
	                    <div class="col-md-4">
	                    	<div class="form-group">  		                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
	                      	 <div class="input-group">  <%			
									fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("mop");
									optionValue = makeDropDownList( mappedItems , sv.mop,2,resourceBundleHandler);  
									longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
											
								if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.mop.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.mop.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
	                      
	                     </div>     </div>               
	              </div>
	           </div>    
	              
	               <div class="row">
                      <div class="col-md-4">
                      	<div class="form-group">  	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Owner")%></label>
	                       <table><tr><td>	
	                       <%
                                         if ((new Byte((sv.clntwin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.clntwin)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntwin)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntwin')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
	                     
					
					</td><td>
					  		
							<%		
												fieldItem=appVars.loadF4FieldsLong(new String[] {"clntname"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clntname");
								optionValue = makeDropDownList( mappedItems , sv.clntname,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clntname.getFormData()).toString().trim());
											
							if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="min-width:100px;max-width:145px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</td></tr></table>	
	                      </div> 
	                     </div>   
	                       
	                    <div class="col-md-4">	
	                    	<div class="form-group">                      
		                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Joint Owner")%></label>
		                       <table><tr><td>
		                       <%
                                         if ((new Byte((sv.clientnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.clientnum)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clientnum)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clientnum')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
	                   
					</td><td>
					
					
							<%		
							fieldItem=appVars.loadF4FieldsLong(new String[] {"clientname"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clientname");
								optionValue = makeDropDownList( mappedItems , sv.clientname,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clientname.getFormData()).toString().trim());
								
											
							if(!((sv.clientname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clientname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clientname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="max-width:145px;min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	                     </td></tr></table> 
	                                     
	              </div>
	              </div>
	             </div>
	             <!-- ILIFE-7277-start -->
	             <div class="row">
                     <div class="col-md-4">
                      	<div class="form-group">  
                      	 <%
                                         if ((new Byte((sv.clntwin2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>
                           	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Owner 2")%></label>
	                       <table><tr><td>	
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntwin2)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntwin2')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>     	            
					</td><td>

							<%		
												fieldItem=appVars.loadF4FieldsLong(new String[] {"clntname2"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clntname2");
								optionValue = makeDropDownList( mappedItems , sv.clntname2,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clntname2.getFormData()).toString().trim());
											
							if(!((sv.clntname2.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="min-width:100px;max-width:145px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</td></tr></table>	
   							<%} %>
	                      </div> 
	                     </div>
	                     
                     <div class="col-md-4">
                      	<div class="form-group">
                      	 <%
                                         if ((new Byte((sv.clntwin2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>  	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Owner 3")%></label>
	                       <table><tr><td>		       
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntwin3)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntwin3')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>

					</td><td>
					  		
							<%		
												fieldItem=appVars.loadF4FieldsLong(new String[] {"clntname3"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clntname3");
								optionValue = makeDropDownList( mappedItems , sv.clntname3,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clntname3.getFormData()).toString().trim());
											
							if(!((sv.clntname3.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname3.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="min-width:100px;max-width:145px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</td></tr></table>	
   							<%} %>
	                      </div> 
	                     </div>
	     
                     <div class="col-md-4">
                      	<div class="form-group"> 
                      	<%
                                         if ((new Byte((sv.clntwin2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>  	 	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Owner 4")%></label>
	                       <table><tr><td>	
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntwin4)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntwin4')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>

					</td><td>
					  		
							<%		
												fieldItem=appVars.loadF4FieldsLong(new String[] {"clntname4"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clntname4");
								optionValue = makeDropDownList( mappedItems , sv.clntname4,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clntname4.getFormData()).toString().trim());
											
							if(!((sv.clntname3.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname4.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="min-width:100px;max-width:145px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</td></tr></table>	
   							<%} %>
	                      </div> 
	                     </div>
	                  </div>
	             <div class="row">
                     <div class="col-md-4">
                      	<div class="form-group">  	  
                      	         <%
                                         if ((new Byte((sv.clntwin2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                  %>             
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("New Owner 5")%></label>
	                       <table><tr><td>	

                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntwin5)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntwin5')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>

					</td><td>
					  		
							<%		
												fieldItem=appVars.loadF4FieldsLong(new String[] {"clntname5"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("clntname5");
								optionValue = makeDropDownList( mappedItems , sv.clntname5,2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.clntname5.getFormData()).toString().trim());
											
							if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntname5.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %> col-md-8' style="min-width:100px;max-width:145px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
   							</td></tr></table>	
   							<%} %>
	                      </div> 
	                     </div>
	                    </div>
	                    <!-- ILIFE-7277-end -->
	                    
	                    
	                    
	                    
	                    <!-- S23 ICIL-1307 START-->

<div class="row">

<%
				if ((new Byte((sv.relationwithlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-4">
				<div class="form-group" style="width: 240px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Life Assured")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "relationwithlife" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("relationwithlife");
						optionValue = makeDropDownList(mappedItems, sv.relationwithlife.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.relationwithlife.getFormData()).toString().trim());
					%>

					<% 
						if((new Byte((sv.relationwithlife).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
						<div class='output_cell' style="width: 190px;">
							<%=longValue==null?"":longValue%>
						</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<select name='relationwithlife' type='list' id="relationwithlife"
								onFocus='doFocus(this)'
                                onHelp='return fieldHelp(relationwithlife)'
                                onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.relationwithlife).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="min-width:220px;" 
						<%}else if("red".equals((sv.relationwithlife).getColor())){%>
							class= "input_cell red reverse" 
						<%} else {%>
							class="input_cell"
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						optionValue=null;
						}
					%>
				</div>
			</div>
					<%
						}
					%>			
					
					
					<%
				if ((new Byte((sv.relationwithlifejoint).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group" style="width: 240px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Life Assured")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "relationwithlifejoint" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("relationwithlifejoint");
						optionValue = makeDropDownList(mappedItems, sv.relationwithlifejoint.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.relationwithlifejoint.getFormData()).toString().trim());
					%>

					<% 
						if((new Byte((sv.relationwithlifejoint).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
						<div class='output_cell' style="width: 190px;">
							<%=longValue==null?"":longValue%>
						</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<select name='relationwithlifejoint' type='list' id="relationwithlifejoint"
								onFocus='doFocus(this)'
                                onHelp='return fieldHelp(relationwithlifejoint)'
                                onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.relationwithlifejoint).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="min-width:220px;" 
						<%}else if("red".equals((sv.relationwithlifejoint).getColor())){%>
							class= "input_cell red reverse" 
						<%} else {%>
							class="input_cell"
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						optionValue=null;
						}
					%>
				</div>
			</div>
					<%
						}
					%>			
					
					
					
					</div>
					<!-- s23 ICIL-1307 END-->
	                    
	                    
	                    
	              <div class="row">
                   	<div class="col-md-3">
	                     <div class="form-group">   
	                     <label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>  
	                     <!-- <div class="input-group"> -->
	                     <table><tr><td style="min-width:140px;">                 
	                      <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:140px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:140px;"> 
					<%
					} 
					%>
					
					<select class='sel' name='reasoncd' type='list' 
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.reasoncd).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					onchange="change()"
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					} 
					%>
					</td><td style="
    padding-left: 1px;
">
					<input style="width:200px;" "id='resn' name='resndesc' 
							type='text'
							
							<%
							
									fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("reasoncd");
									optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
									formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
									
							%>
							<%if(formatValue==null) {
								formatValue="";
							}
							 %>
							 
							 <% String str=(sv.resndesc.getFormData()).toString().trim(); %>
										<% if(str.equals("") || str==null) {
											str=formatValue;
										}
										
									%>
							 
								value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
							
							size='50'
							maxLength='50' 
							 
							
							
							<% 
								if((new Byte((sv.resndesc).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.resndesc).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.resndesc).getColor()== null  ? 
										"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							</td></tr></table>
					<!-- </div> -->
				</div>
				</div> 
	              </div>
	      </div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

