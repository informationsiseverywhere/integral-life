<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5052";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5052ScreenVars sv = (S5052ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Jlife ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed to Date ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loans on Contract ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Surrender Value ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Cash Deposits ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current Loan Amount ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Amount Available ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Amount Required ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Duty Stamp ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net Loan Amount Paid ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Currency ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.loanvalue.setReverse(BaseScreenData.REVERSED);
			sv.loanvalue.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.loanvalue.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    		     <table><tr><td>
						    		
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  
	</td><td>
	
  		
									<%					
									if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
  
	</td><td>

  		
								<%					
									if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 350px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
								%>
						  
	</td></tr></table>

				      		
				    	</div>
					</div>
				    		
				      <div class="col-md-4"></div>
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group">
						    	<%		
											fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("cntcurr");
											longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
														
											if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  		
						</div>
					</div>
				  </div>		
			
			    
		    </div>
				   
				
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
					    		     <div class="input-group">
						    			
											<%					
											if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  	
							 </div>
				    	</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							
						    	
							<%					
								if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 110px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  	

				      		
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
							<div class="input-group">
						    		
							<%					
							if(!((sv.currfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  	

				      		</div>
										
						</div>
				   </div>	
		    </div>
				   
				
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					    		     <table><tr><td>
						    		
  		
								<%					
								if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'  style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
	</td><td>

								<%					
								if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
	
						    		
</td></tr></table>
				      	
				    </div>
				</div>
		    </div>
				   
				  
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					    		      <table><tr><td>
						    			<%					
									if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'  style="margin-left:1px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  </td><td>
	
								<%					
								if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left:1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  </td></tr></table>
				    	
				    </div>
				</div>
				    	 <div class="col-md-4"></div>	
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Jlife")%></label>
							<table><tr><td>
						    	<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" min-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



</td><td>

	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px; min-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		
</td></tr></table>
				      			     
						</div>
				   </div>		
			
			    	
		    </div>
				  <br>
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					    		     <div class="input-group">
						    			<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Billed to Date")%></label>
							<div class="input-group">
						    	<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Loans on Contract")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.numberOfLoans.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numberOfLoans.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numberOfLoans.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				  
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					    		     <div class="input-group">
						    			
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
				    		</div>
					</div>
				    		
	
		    </div>
				   <br>
				 
				   <div class="row">	
				   
				   
				   
				   			    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Surrender Value")%></label>
							<div class="input-group" style="width:71px;">
						    		<%	
			qpsf = fw.getFieldXMLDef((sv.surrval).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.surrval,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.surrval.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cash Deposits")%></label>
							<div class="input-group">
						    		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zrcshamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="min-width: 71px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 	

				      			     </div>
										
						</div>
				   </div>	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Current Loan Amount")%></label>
					    		     <div class="input-group">
						    			
		<%	
			qpsf = fw.getFieldXMLDef((sv.loansum).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.loansum,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.loansum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="min-width:71px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				  	
		    </div>
				  
				   <div class="row">	
				   
				   
				     <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Amount Available")%></label>
							<div class="input-group" style="width:71px;">
						    		<%	
			qpsf = fw.getFieldXMLDef((sv.loanallow).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.loanallow,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.loanallow.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Loan Amount Required")%></label>
							<div class="input-group" style="width:71px;">
						    
	<%	
			qpsf = fw.getFieldXMLDef((sv.loanvalue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.loanvalue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='loanvalue' 
type='text'

	value='<%=valueThis %>'
			 <%
	 //valueThis=smartHF.getPicFormatted(qpsf,sv.loanvalue);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.loanvalue.getLength(), sv.loanvalue.getScale(),3)%>'
maxLength='<%= sv.loanvalue.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(loanvalue)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.loanvalue).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.loanvalue).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.loanvalue).getColor()== null  ? 
			"input_cell" :  (sv.loanvalue).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>		

				      			     </div>
										
						</div>
				   </div>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Duty Stamp")%></label>
					    		     <div class="input-group">
						    			
		<%	
			qpsf = fw.getFieldXMLDef((sv.tplstmdty).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tplstmdty,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.tplstmdty.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="min-width: 71px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
			
		    </div>
		    
		    <div class="row">
		    
		    	    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Net Loan Amount Paid")%></label>
							<div class="input-group">
						    		
		<%	
			qpsf = fw.getFieldXMLDef((sv.netLceamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.netLceamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.netLceamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="min-width: 71px"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 	

				      			     </div>
						</div>
				   </div>		
			<div class="col-md-4"></div>
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Loan Currency")%></label>
							<div class="input-group">
						    	<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("currcd");
	optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.currcd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='currcd' type='list' style="width:180px;"
<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.currcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.currcd).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>	

				      			     </div>
										
						</div>
				   </div>	
		    
		    
		    </div>
				 
				   
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->


<%@ include file="/POLACommon2NEW.jsp"%>

