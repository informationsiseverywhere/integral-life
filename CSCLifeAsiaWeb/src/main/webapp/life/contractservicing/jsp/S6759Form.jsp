<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6759";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6759ScreenVars sv = (S6759ScreenVars) fw.getVariables();%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%{
if (appVars.ind10.isOn()) {
	sv.hflag01.setInvisibility(BaseScreenData.INVISIBLE);
}

if (appVars.ind11.isOn()) {
	sv.hflag02.setInvisibility(BaseScreenData.INVISIBLE);
}

if (appVars.ind12.isOn()) {
	sv.hflag03.setInvisibility(BaseScreenData.INVISIBLE);
}

 if (appVars.ind13.isOn()) {
	sv.confirm.setColor(BaseScreenData.RED);
} 
sv.confirm.setEnabled(BaseScreenData.ENABLED);

}%>

<div class="panel panel-default">
    	
    	<div class="panel-body"> 


<table width='100%'>
<tr style='height:22px;'>
<td width='100% '>



<%if ((new Byte((sv.hflag01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
<label style="padding-left:20px;">
<%=resourceBundleHandler.gettingValueFromBundle("You are about to submit the Windforward transaction.   ")%>
</label>
<br/>
<br/>
<br/>
<br/>
<br/>
<label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Press exit key to abandon the transaction.   ")%>
</label>
<br/>
<br/>
<label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Press Continue to continue with the transaction")%>
</label>
<%} %>

<!--  Ilife- Life Cross Browser - Sprint 4 D1 : Task 6 START-->
<!-- ILIFE-3374 starts -->
<%if ((new Byte((sv.hflag02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("You are about to delete all Windforward records for this contract.")%>
</label>
<br/>
<br/>
<br/>
<br/>
<br/>
<label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Press an exit key to abandon the transaction.   ")%>
</label>
<br/>
<br/>
<label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Press ENTER to continue with the transaction")%>
</label>
<%} %>
<!-- ILIFE-3374 ends -->
<%--<%if ((new Byte((sv.hflag03).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("An error has occurred during the transaction.   ")%>
</label>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Press an exit key to abandon the transaction.   ")%>
</label>
<%} %> --%>

<!-- ILIFE-1591 START by nnazeer -->
<br/>
<%-- <% if("red".equals((sv.confirm).getColor())){ %>
	<div style="border:1px; border-style: solid; border-color: #B55050;  width:30px;"> 
<% } %>
	<%= smartHF.getHTMLVarExt(fw, sv.confirm,0,30)%>
<% if("red".equals((sv.confirm).getColor())){ %>
</div>
<% }	%> --%>	


<!--  Ilife- Life Cross Browser - Sprint 4 D2 : Task 1 ends-->



<%
	longValue = null;
	formatValue = null;
%>
<!-- ILIFE-1591 END -->
</td>
</tr> </table>
<br/>
</div></div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
