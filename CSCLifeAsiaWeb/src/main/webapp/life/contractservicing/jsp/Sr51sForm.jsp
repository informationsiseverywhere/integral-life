
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR51S";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr51sScreenVars sv = (Sr51sScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    		<div class="col-md-4" >
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		<table><tr><td>
					    		
						    		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  >
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' id="cntdesc" style="max-width:160px; margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
				      			     
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							 <div class="input-group">
						    		<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						    		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							 <div class="input-group">
						    		<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				   	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					    		     <div class="input-group">
						    		<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							 <div class="input-group">
						    	<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						    		

				      			     </div>
						</div>
				   </div>		
			</div>
				
				   	<div class="row">	
			    <div class="col-md-4"">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					    		    <table><tr><td>
						    		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="max-width:180px;">
<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:100px; margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>

				      			   
				    		</div>
					</div>
				    			
				    	
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
							  <table><tr><td>
						    	<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="max-width:180px;">
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:100px; margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>	

				      	</td></tr></table>		    
						</div>
				   </div>		
			</div>
				
				   	
				   	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Inception Date")%></label> --%>
					    		<!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Inception Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
					    		     <div class="input-group">
						    		<%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
							 <div class="input-group">
						    		<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%>

				      			     </div>
						</div>
				   </div>	
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
							 <div class="input-group">
						    		
<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute","relative")%>
				      			     </div>
						</div>
				   </div>		
			</div>
				
				 	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					    		     <div class="input-group">
						    		
<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("billfreq");
		longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
	%>
	
    
	  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		%> 
				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Method")%></label>
							 <div class="input-group" >
						    		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("mop");
		longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());  
	%>
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:71px;">
<%=longValue%>
</div>	
	<%
	longValue = null;
	%>

						    		

				      			     </div>
						</div>
				   </div>		
			</div>
				
				   	<br>
				   	   <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-sr51s'>
							<thead>
								<tr class='info'>
			        <th><center>Proc</center></th>
<th style="min-width:70px;"><center>LF</center></th>
<th style="min-width:70px;"><center>JLF</center></th>
<th style="min-width:60px;"><center>Cov</center></th>
<th style="min-width:80px;"><center>Rdr</center></th>
<th style="min-width:100px;"><center>Coverage</center></th>
<th style="min-width:220px;"><center>Description</center></th>
<th style="min-width:100px;"><center>Risk Status</center></th>
<th style="min-width:100px;"><center>Premium Status</center></th>
<th><center>Error</center></th>
<th><center>Sum Assured</center></th>
<th><center>Premium</center></th>
			     	 
		 	        </tr>
			 </thead>
			 

 <%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;
 
calculatedValue=45;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=24;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=36;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=36;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=36;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

calculatedValue=70;
totalTblWidth += calculatedValue;
tblColumnWidth[5] = calculatedValue;

calculatedValue=300;
totalTblWidth += calculatedValue;
tblColumnWidth[6] = calculatedValue;

calculatedValue=50;
totalTblWidth += calculatedValue;
tblColumnWidth[7] = calculatedValue;

calculatedValue=70;
totalTblWidth += calculatedValue;
tblColumnWidth[8] = calculatedValue;

calculatedValue=60;
totalTblWidth += calculatedValue;
tblColumnWidth[9] = calculatedValue;

calculatedValue=220;
totalTblWidth += calculatedValue;
tblColumnWidth[10] = calculatedValue;

calculatedValue=220;
totalTblWidth += calculatedValue;
tblColumnWidth[11] = calculatedValue; 
//ILIFE-1534 ENDS 

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("sr51sscreensfl");
GeneralTable sfl1 = fw.getTable("sr51sscreensfl");
Sr51sscreensfl.set1stScreenRow(sfl, appVars, sv);
int height;
//ILIFE-1534 STARTS
if(sfl.count()*27 > 210) {
height = 350 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27+100;
} else {
height = 258;
}	
//ILIFE-1534 ENDS
%>


<tbody>
<%
Sr51sscreensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (Sr51sscreensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>


		





 <tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.cpstat01.getLength()%>'
 value='<%= sv.cpstat01.getFormData() %>' 
 size='<%=sv.cpstat01.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.cpstat01)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "cpstat01" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "cpstat01" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.cpstat04.getLength()%>'
 value='<%= sv.cpstat04.getFormData() %>' 
 size='<%=sv.cpstat04.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.cpstat04)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "cpstat04" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "cpstat04" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.crstat01.getLength()%>'
 value='<%= sv.crstat01.getFormData() %>' 
 size='<%=sv.crstat01.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.crstat01)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "crstat01" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "crstat01" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.premsubr01.getLength()%>'
 value='<%= sv.premsubr01.getFormData() %>' 
 size='<%=sv.premsubr01.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.premsubr01)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "premsubr01" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "premsubr01" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.crstat04.getLength()%>'
 value='<%= sv.crstat04.getFormData() %>' 
 size='<%=sv.crstat04.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.crstat04)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "crstat04" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "crstat04" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.nonForfeitMethod.getLength()%>'
 value='<%= sv.nonForfeitMethod.getFormData() %>' 
 size='<%=sv.nonForfeitMethod.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.nonForfeitMethod)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "nonForfeitMethod" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "nonForfeitMethod" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.premsubr04.getLength()%>'
 value='<%= sv.premsubr04.getFormData() %>' 
 size='<%=sv.premsubr04.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.premsubr04)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "premsubr04" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "premsubr04" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.planSuffix.getLength()%>'
 value='<%= sv.planSuffix.getFormData() %>' 
 size='<%=sv.planSuffix.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.planSuffix)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "planSuffix" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "planSuffix" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.pumeth.getLength()%>'
 value='<%= sv.pumeth.getFormData() %>' 
 size='<%=sv.pumeth.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.pumeth)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "pumeth" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "pumeth" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51sscreensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="sr51sscreensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >



<td style="width:<%=tblColumnWidth[0]%>px;" 
	<%if(!(((BaseScreenData)sv.sel) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
	<%if((new Byte((sv.sel).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.sel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
			<%if((new Byte((sv.sel).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%if(hyperLinkFlag){%>
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sr51sscreensfl" + "." +
					      "slt" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.sel.getFormData()%></span></a>							 						 		
						  <%}else{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.sel.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>



<td style="width:<%=tblColumnWidth[1]%>px;" 
	<%if(!(((BaseScreenData)sv.life) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.life.getFormData();
							%>
					 		<div id="sr51sscreensfl.life_R<%=count%>" name="sr51sscreensfl.life_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[2]%>px;" 
	<%if(!(((BaseScreenData)sv.jlife) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.jlife.getFormData();
							%>
					 		<div id="sr51sscreensfl.jlife_R<%=count%>" name="sr51sscreensfl.jlife_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[3]%>px;" 
	<%if(!(((BaseScreenData)sv.coverage) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.coverage.getFormData();
							%>
					 		<div id="sr51sscreensfl.coverage_R<%=count%>" name="sr51sscreensfl.coverage_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[4]%>px;" 
	<%if(!(((BaseScreenData)sv.rider) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.rider.getFormData();
							%>
					 		<div id="sr51sscreensfl.rider_R<%=count%>" name="sr51sscreensfl.rider_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[5]%>px;" 
	<%if(!(((BaseScreenData)sv.crtable) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.crtable.getFormData();
							%>
					 		<div id="sr51sscreensfl.crtable_R<%=count%>" name="sr51sscreensfl.crtable_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[6]%>px;" 
	<%if(!(((BaseScreenData)sv.crtabled) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.crtabled).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.crtabled.getFormData();
							%>
					 		<div id="sr51sscreensfl.crtabled_R<%=count%>" name="sr51sscreensfl.crtabled_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[7]%>px;" 
	<%if(!(((BaseScreenData)sv.statcde) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.statcde).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.statcde.getFormData();
							%>
					 		<div id="sr51sscreensfl.statcde_R<%=count%>" name="sr51sscreensfl.statcde_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[8]%>px;" 
	<%if(!(((BaseScreenData)sv.pstatcode) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.pstatcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.pstatcode.getFormData();
							%>
					 		<div id="sr51sscreensfl.pstatcode_R<%=count%>" name="sr51sscreensfl.pstatcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[9]%>px;" 
	<%if(!(((BaseScreenData)sv.eror) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.eror).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.eror.getFormData();
							%>
					 		<div id="sr51sscreensfl.eror_R<%=count%>" name="sr51sscreensfl.eror_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

<!-- ILIFE-1534 STARTS -->

<td style="padding: 5px; width:<%=tblColumnWidth[10]%>px;" 
	<%if(!(((BaseScreenData)sv.sumins) instanceof StringBase)) {%>align="center"<% }else {%> align="center" <%}%> >									
			<%if((new Byte((sv.sumins).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
						%>					
						<% 
					if((new Byte((sv.sumins).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<div id="sr51sscreensfl.sumins_R<%=count%>" name="sr51sscreensfl.sumins_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>  
	   										<%if(formatValue != null){%>
	   										
	   		 											<%=XSSFilter.escapeHtml(formatValue)%>
	   		 							
	   		
	   										<%}%>
	   				</div>

						
							<% }else {%>
<!-- 						MIBT-249 -->
						
						<input type='text'  readonly = "true"
						 value='<%=formatValue%>' 
						 <%if (qpsf.getDecimals() > 0) {%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins.getLength(), sv.sumins.getScale(),3)+1%>'
							maxLength='<%=sv.sumins.getLength()+1%>' 
						<%}else{%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins.getLength(), sv.sumins.getScale(),3)%>'
							maxLength='<%=sv.sumins.getLength()%>' 
						<%}%>
						 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.sumins)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr51sscreensfl" + "." +
						 "sumins" + "_R" + count %>'
						 id='<%="sr51sscreensfl" + "." +
						 "sumins" + "_R" + count %>'
						class = " <%=(sv.sumins).getColor()== null  ? 
						"input_cell" :  
						(sv.sumins).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						  style = "width: <%=sv.sumins.getLength()*12%> px;text-align='right';"
						   
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
					 <%}%>
										
					
											
									<%}%>
			 			</td>



<td style="padding: 5px;width:<%=tblColumnWidth[11]%>px;" 
	<%if(!(((BaseScreenData)sv.instprem) instanceof StringBase)) {%>align="center"<% }else {%> align="center" <%}%> >									
			<%if((new Byte((sv.instprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.instprem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
						%>					
						<% 
					if((new Byte((sv.instprem).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<div id="sr51sscreensfl.instprem_R<%=count%>" name="sr51sscreensfl.instprem_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>  
	   										<%if(formatValue != null){%>
	   										
	   		 											<%=XSSFilter.escapeHtml(formatValue)%>
	   		 							
	   		
	   										<%}%>
	   				</div>

						
							<% }else {%>
						
<!-- 						MIBT-249 -->
						<input type='text' readonly = "true"
						 value='<%=formatValue%>' 
						 <%if (qpsf.getDecimals() > 0) {%>
							size='<%=sv.instprem.getLength()+1%>'
							maxLength='<%=sv.instprem.getLength()+1%>' 
						<%}else{%>
							size='<%=sv.instprem.getLength()%>'
							maxLength='<%=sv.instprem.getLength()%>' 
						<%}%>
						 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51sscreensfl.instprem)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="sr51sscreensfl" + "." +
						 "instprem" + "_R" + count %>'
						 id='<%="sr51sscreensfl" + "." +
						 "instprem" + "_R" + count %>'
						class = " <%=(sv.instprem).getColor()== null  ? 
						"input_cell" :  
						(sv.instprem).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						  style = "width: <%=sv.instprem.getLength()*12%> px;text-align='right';"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true);  "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
					 <%}%>
										
					
											
									<%}%>
			 			</td>

<!-- ILIFE-1534 ENDS -->

</tr>

<%	count = count + 1;
Sr51sscreensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</tbody>
</table>
</div></div></div></div>

<INPUT type="HIDDEN" name="cpstat01" id="cpstat01" value="<%=	(sv.cpstat01.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="cpstat04" id="cpstat04" value="<%=	(sv.cpstat04.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="crstat01" id="crstat01" value="<%=	(sv.crstat01.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="crstat04" id="crstat04" value="<%=	(sv.crstat04.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="screenIndicArea" id="screenIndicArea" value="<%=	(sv.screenIndicArea.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="nonForfeitMethod" id="nonForfeitMethod" value="<%=	(sv.nonForfeitMethod.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="planSuffix" id="planSuffix" value="<%=	(sv.planSuffix.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="pumeth" id="pumeth" value="<%=	(sv.pumeth.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="premsubr01" id="premsubr01" value="<%=	(sv.premsubr01.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="premsubr04" id="premsubr04" value="<%=	(sv.premsubr04.getFormData()).toString() %>" >
			    	
		</div>  
</div>  

<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','225px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
    	$('#dataTables-sr51s').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
			scrollX: true,
			info: true
      	});
    });
</script>


<%@ include file="/POLACommon2NEW.jsp"%>
<%if(!cobolAv3.isPagedownEnabled()){%>
<script type="text/javascript">
window.onload = function()
{
setDisabledMoreBtn();
}
</script>
<%}%> 



