<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "ST528";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	St528ScreenVars sv = (St528ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Status ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prem Status ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Payor ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agency ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Commence ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billing Frequency ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid to Date ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Method of Payment ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Client Number ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client Role ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Surname ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Given Name ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Salutation ");
%>

<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Kanji Surname");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Kanji Given Name");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Kana Surname");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Kana Given Name");
%>
<%
	{
		if (appVars.ind04.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payrnum.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.payorname.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind40.isOn()) {
			sv.lsurname.setReverse(BaseScreenData.REVERSED);
			sv.lsurname.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.lsurname.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.lgivname.setReverse(BaseScreenData.REVERSED);
			sv.lgivname.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.lgivname.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.salutl.setReverse(BaseScreenData.REVERSED);
			sv.salutl.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.salutl.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.tsalutsd.setInvisibility(BaseScreenData.INVISIBLE);
		}
				//Japan Localization kana/kanji name indicators
				// Non display indicators
				if (appVars.ind72.isOn()) {
					sv.zkanasurname.setInvisibility(BaseScreenData.INVISIBLE);
					sv.zkanagivname.setInvisibility(BaseScreenData.INVISIBLE);
				}
				// Protect Kana/Kanji Name 
				if (appVars.ind43.isOn()) {
					sv.zkanasurname.setEnabled(BaseScreenData.DISABLED);
					sv.zkanagivname.setEnabled(BaseScreenData.DISABLED);
				}
				// Error indicators for Kana/Kanji 	
				if (appVars.ind75.isOn()) {
					sv.zkanasurname.setReverse(BaseScreenData.REVERSED);
					sv.zkanasurname.setColor(BaseScreenData.RED);
					sv.zkanasurname.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind76.isOn()) {
					sv.zkanagivname.setReverse(BaseScreenData.REVERSED);
					sv.zkanagivname.setColor(BaseScreenData.RED);
					sv.zkanagivname.setHighLight(BaseScreenData.BOLD);
				}	
				if (appVars.ind18.isOn()) {
					sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
				}	
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> 
							col-md-2'
									style="margin-left: 1px !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-7'
									style="margin-left: 1px !important;max-width:300px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
					<%
						if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem Status")%></label>
					<%
						if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-9'
									style="margin-left: 1px !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.agntnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.agntnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.oragntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.oragntnam.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.oragntnam.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-9'
									style="margin-left: 1px !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq");
						optionValue = makeDropDownList(mappedItems, sv.billfreq, 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());

						if (!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.billfreq.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.billfreq.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>

					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
					<div class="input-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop");
						optionValue = makeDropDownList(mappedItems, sv.mop, 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());

						if (!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.mop.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.mop.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
					</div>


				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></label>
					<div class="input-group">
					<%
						if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.clntnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.clntnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client Role")%></label>
					<div class="input-group">
					<%
						if ((new Byte((sv.clrole).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "clrole" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("clrole");
							longValue = (String) mappedItems.get((sv.clrole.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.clrole.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clrole.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clrole.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 70px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
					</div>
				</div>
			</div>
		</div>
			<%
			if ((new Byte((sv.zkanasurname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
			<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Kanji Surname")%></label>
					<div class="input-group">
					<input name='lsurname' type='text'
						<%formatValue = (sv.lsurname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=60%>'
						maxLength='<%=sv.lsurname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(lsurname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lsurname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lsurname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lsurname).getColor() == null ? "input_cell"
						: (sv.lsurname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Kana Surname")%></label>
					<div class="input-group">
					<input name='zkanasurname' type='text'
						<%formatValue = (sv.zkanasurname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=60%>'
						maxLength='<%=sv.zkanasurname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zkanasurname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zkanasurname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zkanasurname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zkanasurname).getColor() == null ? "input_cell"
						: (sv.zkanasurname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</div>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Kanji Given Name")%></label>
					<div class="input-group">
					<input name='lgivname' type='text'
						<%formatValue = (sv.lgivname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=60%>'
						maxLength='<%=sv.lgivname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zkanasurname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lgivname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lgivname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lgivname).getColor() == null ? "input_cell"
						: (sv.lgivname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Kana Given Name")%></label>
					<div class="input-group">
					<input name='zkanagivname' type='text'
						<%formatValue = (sv.zkanagivname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=60%>'
						maxLength='<%=sv.zkanagivname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zkanagivname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zkanagivname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zkanagivname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zkanagivname).getColor() == null ? "input_cell"
						: (sv.zkanagivname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>
						</div>
				</div>
			</div>
			<%
			} else {
		%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Surname")%></label>
					<div class="input-group">
					<input name='lsurname' type='text'
						<%formatValue = (sv.lsurname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=15%>'
						maxLength='<%=sv.lsurname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(lsurname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lsurname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lsurname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lsurname).getColor() == null ? "input_cell"
						: (sv.lsurname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Given Name")%></label> 
					<div class="input-group">
					<input name='lgivname'
						type='text'
						<%formatValue = (sv.lgivname.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=15%>'
						maxLength='<%=sv.lgivname.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(lgivname)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lgivname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lgivname).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lgivname).getColor() == null ? "input_cell"
						: (sv.lgivname).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Salutation")%></label>
					<table>
						<tr>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "salutl" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("salutl");
									optionValue = makeDropDownList(mappedItems, sv.salutl.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.salutl.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.salutl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.salutl).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
									<%
										}
									%>

									<select name='salutl' type='list' style="width: 95px;"
										<%if ((new Byte((sv.salutl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.salutl).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.salutl).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.tsalutsd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.tsalutsd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.tsalutsd.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.tsalutsd.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<%
			}
		%>
			<div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

