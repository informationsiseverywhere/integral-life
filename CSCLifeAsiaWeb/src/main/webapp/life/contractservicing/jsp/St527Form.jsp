<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "ST527";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	St527ScreenVars sv = (St527ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract no ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Status ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prem Status ");
%>
<%
	 StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD "); 
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to-date ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bill-to-date ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------------------------------------------------------------");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sel");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Name");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Role");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No ");
%>
<%
	appVars.rollup(new int[]{93});
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
				<table>
					<tr>
						<td>
							<%
								if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
						<td>
							<%
								if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'
								style="margin-left: 1px !important;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
						<td>
							<%
								if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-7' id="cntdesc"
								style="margin-left: 1px !important;max-width:155px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ownernum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownernum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownernum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-9'
									style="margin-left: 1px !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
					<%
						if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%
						if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed to Date")%></label>
					<%
						if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						} else {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 80px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-st527' width='100%'>
						<thead>
							<tr class='info'>
								<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center></th>

								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client Role")%></center></th>
							</tr>
						</thead>
						<%
							/* This block of jsp code is to calculate the variable width of the table at runtime.*/
							int[] tblColumnWidth = new int[5];
							int totalTblWidth = 0;
							int calculatedValue = 0;

							if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData())
									.length()) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
							} else {
								calculatedValue = (sv.select.getFormData()).length() * 12;
							}
							totalTblWidth += calculatedValue;
							tblColumnWidth[0] = calculatedValue;

							if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.clntnum.getFormData())
									.length()) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 8;
							} else {
								calculatedValue = (sv.clntnum.getFormData()).length() * 8;
							}
							totalTblWidth += calculatedValue;
							tblColumnWidth[1] = calculatedValue;

							if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.clntname.getFormData())
									.length()) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 8;
							} else {
								calculatedValue = (sv.clntname.getFormData()).length() * 8;
							}
							totalTblWidth += calculatedValue;
							tblColumnWidth[2] = calculatedValue;

							if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.clrrrole.getFormData())
									.length()) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 8;
							} else {
								calculatedValue = (sv.clrrrole.getFormData()).length() * 8;
							}
							totalTblWidth += calculatedValue;
							tblColumnWidth[3] = calculatedValue;

							if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.clrole.getFormData())
									.length()) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 8;
							} else {
								calculatedValue = (sv.clrole.getFormData()).length() * 8;
							}
							totalTblWidth += calculatedValue;
							tblColumnWidth[4] = calculatedValue;
						%>
						<%
							GeneralTable sfl = fw.getTable("st527screensfl");
							int height;
							if (sfl.count() * 27 > 150) {
								height = 150;
							} else {
								height = sfl.count() * 27;
							}
						%>

						<tbody>
							<%
								St527screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								Map<String, Map<String, String>> clrrRoleMap = appVars.loadF4FieldsLong(new String[]{"clrrrole"}, sv, "E",
										baseModel);

								while (St527screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr class="tableRowTag" id='<%="tablerow" + count%>'>
								<td class="tableDataTag tableDataTagFixed"
									style="width:5%;" align="left"><input
									type="checkbox" value='<%=sv.select.getFormData()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp("st527screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='st527screensfl.select_R<%=count%>'
									id='st527screensfl.select_R<%=count%>'
									onClick="selectedRow('st527screensfl.select_R<%=count%>')"
									class="UICheck" /></td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[1] + tblColumnWidth[2]%>px;"
									align="left"><%=sv.clntnum.getFormData()%>&nbsp;-&nbsp; <!-- 		</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;" align="left">									
																
									 --> <%=sv.clntname.getFormData()%></td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[3] + tblColumnWidth[4]%>px;"
									align="left"><%=sv.clrrrole.getFormData()%>&nbsp;-&nbsp;



									<!--  	</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4]%>px;" align="left">									
										--> <%
 	if ((new Byte((sv.clrole).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	mappedItems = (Map) clrrRoleMap.get("clrrrole");
 			longValue = (String) mappedItems.get((sv.clrrrole.getFormData()).toString().trim());
 			if (longValue == null || "".equals(longValue)) {
 				longValue = sv.clrrrole.getFormData();
 			}
 %> <%=longValue%> <%
 	longValue = null;
 			formatValue = null;
 %></td>
								<%
									} else {
								%>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[4]%>px;"></td>

								<%
									}
								%>


							</tr>
							<%
								count = count + 1;
									St527screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
		$('#dataTables-st527').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			info : false,
			paging : false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

