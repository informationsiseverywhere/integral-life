
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SD5H1";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sd5h1ScreenVars sv = (Sd5h1ScreenVars) fw.getVariables();
%>

<%
	appVars.rollup(new int[]{93});
%>
<%
	if (appVars.ind03.isOn()) {
		sv.repymop.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind02.isOn()) {
		sv.repymop.setColor(BaseScreenData.RED);
		sv.repymop.setReverse(BaseScreenData.REVERSED);
	}
	if (appVars.ind04.isOn()) {
		sv.repymop.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (!appVars.ind02.isOn()) {
		sv.repymop.setHighLight(BaseScreenData.BOLD);
	}
		
	if (appVars.ind06.isOn()) {
		sv.repaymentamt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind05.isOn()) {
		sv.repaymentamt.setColor(BaseScreenData.RED);
		sv.repaymentamt.setReverse(BaseScreenData.REVERSED);
	}
	if (appVars.ind07.isOn()) {
		sv.repaymentamt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (!appVars.ind05.isOn()) {
		sv.repaymentamt.setHighLight(BaseScreenData.BOLD);
	}
	
	
	if (appVars.ind09.isOn()) {
		sv.bankkey.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind08.isOn()) {
		sv.bankkey.setColor(BaseScreenData.RED);
		sv.bankkey.setReverse(BaseScreenData.REVERSED);
	}
	if (appVars.ind10.isOn()) {
		sv.bankkey.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (!appVars.ind08.isOn()) {
		sv.bankkey.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind11.isOn()) {
		sv.ddind.setEnabled(BaseScreenData.DISABLED);
		sv.crcind.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind12.isOn()) {
		sv.ddind.setColor(BaseScreenData.RED);
		sv.ddind.setReverse(BaseScreenData.REVERSED);
	}
	if (!appVars.ind12.isOn()) {
		sv.ddind.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind13.isOn()) {
		sv.crcind.setColor(BaseScreenData.RED);
		sv.crcind.setReverse(BaseScreenData.REVERSED);
	}
	if (!appVars.ind13.isOn()) {
		sv.crcind.setHighLight(BaseScreenData.BOLD);
	}
%>
<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								 <%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> 
								<%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px; max-width: 200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> 
								<%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>

					<%
						if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

					<%
						if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="margin-left: 1px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[]{"currcd"}, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("currcd");
						optionValue = makeDropDownList(mappedItems, sv.currcd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
					%>

					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					
				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						%>
						<div style="min-width: 100px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>						
						
					</div>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								 <%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
							<td>
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> 
								<%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								 <%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> 
								<%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
						</tr>
					</table>


				</div>
			</div>

		</div>

		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label>
					<div class="input-group">

						<%
							if (!((sv.currfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>




					</div>

				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					<div class="input-group">
						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>

				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
					<div class="input-group">
						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



					</div>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Repayment Effective Date")%></label>


					<%
						if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
		</div>
		<div class="row">
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Balance")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.susbalnce).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.susbalnce,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.susbalnce.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Repayment Currency")%></label>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[]{"currcd"}, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("currcd");
						optionValue = makeDropDownList(mappedItems, sv.currcd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
					%>

					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Repayment Amount")%></label>


					<%
						if ((new Byte((sv.repaymentamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						qpsf = fw.getFieldXMLDef((sv.repaymentamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.repaymentamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='repaymentamt' type='text'
						<%if ((sv.repaymentamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.repaymentamt)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.repaymentamt);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.repaymentamt)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.repaymentamt.getLength(),
						sv.repaymentamt.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.repaymentamt.getLength(),
						sv.repaymentamt.getScale(), 3) - 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(repaymentamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.repaymentamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.repaymentamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.repaymentamt).getColor() == null
							? "input_cell"
							: (sv.repaymentamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Repayment Method")%></label>
					<%
						if ((new Byte((sv.repymop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[]{"repymop"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("repymop");
							optionValue = makeDropDownList(mappedItems, sv.repymop.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.repymop.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.repymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div style="width: 135px;"
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
								/* Ticket #ILIFE-1802 start by akhan203  */
								formatValue = null;
								/*  Ticket #ILIFE-1802 ends  */
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.repymop).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #f51504 !important; width: 213px;">
						<%
							}
						%>

						<select name='repymop' type='list' style="width: 210px;"
							<%if ((new Byte((sv.repymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.repymop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.repymop).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						longValue = null;
								/* Ticket #ILIFE-1802 start by akhan203  */
								formatValue = null;
								/*  Ticket #ILIFE-1802 ends  */
					%>
					<%
						}
						}
					%>
				</div>
			</div>

		</div>
		<br>
		<div class="row">			
			<div class="col-md-4">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("List of Loans")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-Sd5h1' width='100%'>
						<thead>
							<tr class='info'>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Loan_Number")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Loan_Type")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Eff Date")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Curr")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Interest")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Principal Amt")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Current Amt")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Accrued Interest")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Pending Interest")%></th>
								<th style="text-align: center; min-width: 120px;"><%=resourceBundleHandler.gettingValueFromBundle("Total")%></th>								
							</tr>
						</thead>
						<%
							/* This block of jsp code is to calculate the variable width of the table at runtime.*/
							int[] tblColumnWidth = new int[10];
							int totalTblWidth = 0;
							int calculatedValue = 0;
						%>

						<%
							GeneralTable sfl = fw.getTable("sd5h1screensfl");
						
						%>


						<%
							Sd5h1screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (Sd5h1screensfl.hasMoreScreenRows(sfl)) {
						%>






						<tbody>


							<tr class="tableRowTag" id='<%="tablerow" + count%>'>
								<td class="tableDataTag tableDataTagFixed"
									style="width: 200px; font-weight: bold;">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.loanNumber).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> 
									<%=formatValue(smartHF.getPicFormatted(qpsf, sv.loanNumber,
											COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>



								</td>
								<td style="width:<%=tblColumnWidth[1]%>px;font-weight: bold;"
									align="left">&nbsp; <%=formatValue(sv.loanType.getFormData())%>



								</td>
								<td style="width:<%=tblColumnWidth[2]%>px;font-weight: bold;"
									align="left">&nbsp; <%=formatValue(sv.loanstdateDisp.getFormData())%>



								</td>
								<td style="width:<%=tblColumnWidth[3]%>px;font-weight: bold;"
									align="left">&nbsp; <%=formatValue(sv.cntcurr.getFormData())%>



								</td>
								
								<td style="width:<%=tblColumnWidth[3]%>px;font-weight: bold;"
									align="left">
									<%-- &nbsp; <%= formatValue( sv.intrstpercentage.getFormData() )%>	 --%>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.intrstpercentage).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.intrstpercentage,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%><span>%</span>
									
								</td>

								<td style="width:<%=tblColumnWidth[4]%>px;font-weight: bold;"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hprincipal).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.hprincipal,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>
									

								</td>
								<td style="width:<%=tblColumnWidth[5]%>px;font-weight: bold;"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hcurbal).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.hcurbal,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>
									

								</td>
								<td style="width:<%=tblColumnWidth[6]%>px;font-weight: bold;"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hacrint).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.hacrint,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>
									

								</td>
								<td style="width:<%=tblColumnWidth[7]%>px;font-weight: bold;"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hpndint).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.hpndint,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>
									



								</td>
								<td style="width:<%=tblColumnWidth[8]%>px;font-weight: bold;"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hpltot).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROVS2);
									%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.hpltot,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS))%>
									



								</td>



							</tr>
						</tbody>
						<%
							count = count + 1;
								Sd5h1screensfl.setNextScreenRow(sfl, appVars, sv);
							}
						%>

					</table>
				</div>
			</div>
			
			 <input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total PL Principal")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.totalplprncpl).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.totalplprncpl,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.totalplprncpl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total PL Interest")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.totalplint).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.totalplint,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.totalplint.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total APL Principal")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.totalaplprncpl).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.totalaplprncpl,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.totalaplprncpl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total APL Interest")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.totalaplint).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.totalaplint,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.totalaplint.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div >
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Remaining PL Principal")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.remngplprncpl).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.remngplprncpl,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.remngplprncpl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Remaining PL Interest")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.remngplint).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.remngplint,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.remngplint.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Remaining APL Principal")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.remngaplprncpl).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.remngaplprncpl,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.remngaplprncpl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Remaining APL Interest")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.remngaplint).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf, sv.remngaplint,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.remngaplint.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" id="amountfield">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" id="amountfield">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

	</div>
</div>


<div id="popups" class="panel panel-default pop" style="z-index: 1000;width: 710px;margin:50px 10%; display:none; position:absolute;display:none; top: 200px; border:1px solid lightgrey;">
	 <div class="panel-heading1">
	 <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loan / APL Repayment Reversal")%></label>
	 	</div>
	 	 <div class="panel-body"> 
	 		<div class="row">
	 		<div class="col-md-6">
	 		<label><%=resourceBundleHandler.gettingValueFromBundle("Confirm reversal of Loan Repayment?(Y/N)")%></label>
	 		</div></div>
	 			<div class="row">
	 			<div class="col-md-4">
	 	 		 <input type="text" name="ansrepy2" id='ans' maxlength="1">  
	 			</div></div>
	

	  <div style="float:right;margin-right:5px;">
	 <button id="closebutton" style="background:#EA4742;"><%=resourceBundleHandler.gettingValueFromBundle("Close")%></button>
	 <button id="closecontbutton"style="background:#5CB85C;"><%=resourceBundleHandler.gettingValueFromBundle("Continue")%></button>
	 </div> 
	 <br><br>
	 </div></div>
 <div id="popupx" class="panel panel-default pop" style="z-index: 1000;width: 710px;margin:50px 10%;  position:absolute;display:none; top: 200px; border:1px solid lightgrey;">
	 <div class="panel-heading1">
	 <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loan / APL Repayment Approval")%></label>
	 	</div>
	 	 <div class="panel-body"> 
	 		<div class="row">
	 		<div class="col-md-6">
	 		<label><%=resourceBundleHandler.gettingValueFromBundle("Confirm approval of Loan Repayment?(Y/N)")%></label>
	 		</div></div>
	 			<div class="row">
	 			<div class="col-md-4">
	 			<input type="text" name="ansrepy1" id='ans2'  maxlength="1">
	 			</div></div>
	

	
	 <div style="float:right;margin-right:5px;">
	 <button id="closebutton2" style="background:#EA4742;"><%=resourceBundleHandler.gettingValueFromBundle("Close")%></button>
	 <button id="closecontbutton2"style="background:#5CB85C;"><%=resourceBundleHandler.gettingValueFromBundle("Continue")%></button>
	 </div>
	 <br><br>
	 </div></div> 
		<input type="hidden" name="ansrepy" id="mainans" maxlength="1">
	 <style>
	 #ans{
	 height: 26px !important;
	 padding: 0 0 0 5px !important;
	 font-size: 13px !important;
	 border: 2px solid #ccc !important;
	 color: #000000 !important;
	 padding-right: 5px !important;
	 }
	 #closebutton,#closecontbutton, #closebutton2,#closecontbutton2{
	 color:white; padding:5px 10px; border:0px; border-radius:3px; font-weight:bold;
	 }
	 .panel-default > .panel-heading1 {
	     text-align: left;
	     font-weight: bold !important;
	  	 background-color: rgba(52, 77, 90, 0.78) !important;
	     color: #ffffff !important;
	     font-size: medium !important;
	     height:auto;
	 }
	 .panel-heading1 {
	     padding: 10px 15px;
	     border-bottom: 1px solid transparent;
	     border-top-left-radius: 3px;
	     border-top-right-radius: 3px;
	     
	 }
	 </style>

  <script type="text/javascript">
	 var flag= false;
	 var refcode="<%=sv.refcode%>";
	 var msg = "<%=resourceBundleHandler.gettingValueFromBundle("Enter only Y/N")%>";
	 
	 function replicatevalueans(value)
	 {
		 $("#mainans").val(value);
	 }
 	$(document).ready(function() { 
 	
		 if(!flag){ 
			
	 		if (parent.frames["mainForm"].document.form1.action_key.value.toUpperCase() == "PFKEY0"){
	 			if(!flag){
	 				if(refcode == "1")
	 					$('#popups').fadeIn(10);
	 				else if(refcode == "2")
	 					$('#popupx').fadeIn(10);	
	 			}
	 		};
				
	  	} 
	 	$('#closecontbutton,#closecontbutton2').on('click',function(){
	 		if(refcode == "1"){
	 			$('#popups').fadeOut(10);
	 			var here = $('#ans').val();
	 			if(here != "Y" && here !="N" && here!="")
	 				alert(msg);
	 			else{	
	 				if(here == "Y"){
	 					flag=true;
	 				 	document.getElementById('ans').value = 'Y';  
	 					document.getElementById('mainans').value = 'Y';  
	 				 	doAction("PFKEY0");
	 				}
	 				else if(here == "N"){
	 					document.getElementById('ans').value = 'N';  
	 					document.getElementById('mainans').value = 'N';  
	 					doAction("PFKEY0");
	 				}
	 				else  if(here == ""){
	 					document.getElementById('ans').value = 'X';  
	 					document.getElementById('mainans').value = 'X';  
	 					doAction("PFKEY0");
	 				}
	 			}
	 		/* 	$('#ans').val("") */
	 		}
	 		else if(refcode == "2"){
	 			$('#popupx').fadeOut(10);	
	 			var here = $('#ans2').val();

	 			if(here != "Y" && here !="N"  && here!="")
	 				alert(msg);
	 			else{	
	 				if(here == "Y"){
	 					flag=true;		
	 				 	document.getElementById('ans2').value = 'Y';  
						document.getElementById('mainans').value = 'Y';  
	 				 	doAction("PFKEY0");
	 				}
	 				else if(here == "N"){
	 					document.getElementById('ans2').value = 'N';  
						document.getElementById('mainans').value = 'N';;  
	 					doAction("PFKEY0");
	 				}
	 				else if(here == ""){
 						document.getElementById('ans2').value = 'X';  
							document.getElementById('mainans').value = 'X';  
 						doAction("PFKEY0");
 					}
	 			}
	 		/* 	$('#ans2').val("") */
	 		}
	 		
	 	});

	 	$('#closebutton,#closebutton2').on('click',function(){
	 		if(refcode == "1")
	 			$('#popups').fadeOut(10);	
			else if(refcode == "2")
				$('#popupx').fadeOut(10);	
	 	});
 		
	 });
	 </script>  

<script>
    $(document).ready(function () {
    	var showval= document.getElementById('show_lbl').value;
    	var toval= document.getElementById('to_lbl').value;
    	var ofval= document.getElementById('of_lbl').value;
    	var entriesval= document.getElementById('entries_lbl').value;	
    	var nextval= document.getElementById('nxtbtn_lbl').value;
    	var previousval= document.getElementById('prebtn_lbl').value;
        $('#dataTables-Sd5h1').DataTable({
            ordering: false,
            searching: false,
            scrollX: true,
            scrollY: "300px",
            scrollCollapse:true,
            language: {
                "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
                "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
                "paginate": {                
                    "next":       nextval,
                    "previous":   previousval
                }
              }     
        });
        
        /* $("div[style*='background-color: rgb(238, 238, 238)']").each(function(){
    		if($('#amountfield').text().replace(/(^\s+|\s+$)/g, "").length != 0){
    			$('#amountfield').css('width','225px');
    		}else if($('#amountfield').text().replace(/(^\s+|\s+$)/g, "").length == 0){
    			$('#amountfield').css('width','225px');
    		}
    		 
    	}); */
      //  $("#amountfield").width(225)
      
    });
</script>

<Div id='mainForm_OPTS' style='visibility:hidden;'>

<li>
									<input name='crcind' id='crcind' type='hidden' value="<%=sv.crcind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.crcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.crcind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payment")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("crcind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payment")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.crcind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.crcind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('crcind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='ddind' id='ddind' type='hidden' value="<%=sv.ddind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Debit")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Debit")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ddind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ddind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ddind'))"></i> 
			 						<%}%>
			 						</a>
								</li>

<%@ include file="/POLACommon2NEW.jsp"%>

