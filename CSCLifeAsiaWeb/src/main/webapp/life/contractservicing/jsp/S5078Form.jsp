<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5078";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5078ScreenVars sv = (S5078ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind66.isOn()) {
	sv.bnytype.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind64.isOn()) {
	sv.bnysel.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind61.isOn()) {
	sv.cltreln.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind62.isOn()) {
	sv.bnycd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind63.isOn()) {
	sv.bnypc.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind65.isOn()) {
	sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind52.isOn()) {
	sv.enddateDisp.setEnabled(BaseScreenData.DISABLED);
}
}%>

<%
String[][] items = {{"paymthbf"}, {}, {} };
Map longDesc = appVars.getLongDesc(items, "E", baseModel.getCompany().toString().trim(), baseModel, sv);
%>
<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		      <!-- <div class="input-group"> -->
								 <table><tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td><td>
						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
						 		} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td></tr></table>
					<!-- </div> -->
				    		</div>
					</div>
				</div>
				    		
				    <div class="row">		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
							
					<table><tr><td>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td>
						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td></tr></table>
					
						</div>
				   </div>		
			</div>
				    		
				    <div class="row">		
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
							
					<table><tr><td>
						<%
							if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td>
						<%
							if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
			</td></tr></table>
				
										
						</div>
				   </div>	
		    </div>
				   
				   
				   
			<%	appVars.rollup(new int[] {93}); %>
<% 


GeneralTable sfl = fw.getTable("s5078screensfl");
GeneralTable sfl1 = fw.getTable("s5078screensfl");
S5078screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 118;
}	
%>

<style type="text/css">
.fakeContainer {
	width:720px;		
	height:340px;	/*ILIFE-2143*/
	top: 150px;
	left:4px;			
}
.sSky th, .sSky td{
font-size:12px !important;
}
.s5078Table tr{height:25px}
</style>

    
<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " 
							id='dataTables-s5078' width='100%'>
							<thead>
							
								<tr class='info'>
				
		            <th style="min-width:180px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bnfcry Type")%></th>
		         								
					<th style="min-width:130px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary")%></th>
					
	<%if (sv.actionflag.compareTo("N") != 0) {%><th style="min-width:80px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Sequence")%></th><%} %>
	
					<th style="min-width:200px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
					
					<th style="min-width:150px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Relationship")%></th>				
					
					<th style="min-width:200px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Code")%></th>
					
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Rev Flg")%></th>	
					
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Rel To")%></th>	
					
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Share%")%></th>
					
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Effective From")%></th>
					
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("End Date")%></th>
					
<%if (sv.actionflag.compareTo("N") != 0) {%><th style="min-width:160px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Payout Method")%></th> 
					
<th style="min-width:160px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bank AcNo./CcNo.")%></th> 
					
<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></th>
					
<th style="min-width:160px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bank Name")%></th> <%}%>
					
					

	</tr> </thead><tbody>
<!-- ILIFE-2719 Life Cross Browser - Sprint 4 D2 : Task 2  ends-->		

				<%
				S5078screensfl.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (S5078screensfl.hasMoreScreenRows(sfl)) {
							
							//MIBT-70 START
							if (appVars.ind43.isOn()) {
								sv.bnytype.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind43.isOn()) {
								sv.bnytype.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind43.isOn()) {
								sv.bnytype.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind40.isOn()) {
								sv.bnysel.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind40.isOn()) {
								sv.bnysel.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind40.isOn()) {
								sv.bnysel.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind37.isOn()) {
								sv.cltreln.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind37.isOn()) {
								sv.cltreln.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind37.isOn()) {
								sv.cltreln.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind39.isOn()) {
								sv.bnypc.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind39.isOn()) {
								sv.bnypc.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind39.isOn()) {
								sv.bnypc.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind42.isOn()) {
								sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind42.isOn()) {
								sv.effdateDisp.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind42.isOn()) {
								sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
							}
							/*  ILIFE-3581 starts */
							if (appVars.ind51.isOn()) {
								sv.enddateDisp.setReverse(BaseScreenData.REVERSED);
								sv.enddateDisp.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind51.isOn()) {
								sv.enddateDisp.setHighLight(BaseScreenData.BOLD);
							}
							/*  ILIFE-3581 ends */
							//MIBT-70 END
							if (appVars.ind47.isOn()) {
								sv.sequence.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind47.isOn()) {
								sv.sequence.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind47.isOn()) {
								sv.sequence.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind53.isOn()) {
								sv.paymthbf.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind53.isOn()) {
								sv.paymthbf.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind53.isOn()) {
								sv.paymthbf.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind55.isOn()) {
								sv.bankkey.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind55.isOn()) {
								sv.bankkey.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind55.isOn()) {
								sv.bankkey.setHighLight(BaseScreenData.BOLD);
							}
							if (appVars.ind57.isOn()) {
								sv.bankacckey.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind57.isOn()) {
								sv.bankacckey.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind57.isOn()) {
								sv.bankacckey.setHighLight(BaseScreenData.BOLD);
							}
							
				%>

				<tr class="tableRowTag" id='<%="tablerow" + count%>'>
				
					<td class="tableDataTag " style="min-width:180px;"
						align="left">
						<div class="form-group">
						<div class="input-group">
						<!-- ILIFE-930-begins	 -->	
							<input name='<%="s5078screensfl" + "." +
						 "bnytype" + "_R" + count %>'
						id='<%="s5078screensfl" + "." +
						 "bnytype" + "_R" + count %>'
						type='text' 
						value='<%= sv.bnytype.getFormData() %>' 
						class = " <%=(sv.bnytype).getColor()== null  ? 
						"input_cell" :  
						(sv.bnytype).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.bnytype.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5078screensfl.bnytype)' onKeyUp='return checkMaxLength(this)' 
						 style = "width: 120px;"
					   
						 <%if(sv.revcflg.equals("N")) { %> 
						 disabled="disabled"
						 <%}%>
						>		
									
						<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('<%="s5078screensfl" + "." +
						 "bnytype" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
						<!-- ILIFE-930-ends	 -->		
						
						
<!-- may be check  sv.sel if it works wrong action, to check by comparing with this field in preview code --> 				
						
			</div>	</div>	</td>
					
					
					
					<td class="tableDataTag " style="min-width:130px;"
						align="left">
						<div class="form-group">
					<div class="input-group">
						<input name='<%="s5078screensfl" + "." +
						 "bnysel" + "_R" + count %>'
						id='<%="s5078screensfl" + "." +
						 "bnysel" + "_R" + count %>'
						type='text' 
						value='<%= sv.bnysel.getFormData() %>' 
						class = " <%=(sv.bnysel).getColor()== null  ? 
						"input_cell" :  
						(sv.bnysel).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.bnysel.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5078screensfl.bnysel)' onKeyUp='return checkMaxLength(this)'	 style = "width: 80px;"					 
						  <%if(sv.revcflg.equals("N")) { %> 
						 disabled="disabled"
						 <%} %>
						>			
								<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('<%="s5078screensfl" + "." +
						 "bnysel" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>	
							
				</div>	</div></td>				
					
					<!-- ICIL-11 -->
						<%if (sv.actionflag.compareTo("N") != 0) {%>
				<td class="tableDataTag" style="min-width:60px;text-align:center; align:center;">
				  <div class="form-group">	
						<div class="input-group">	
						<input name = '<%="s5078screensfl" + "." +"sequence" + "_R" + count %>' id ='<%="s5078screensfl" + "." +"sequence" + "_R" + count %>' type='text' 
						class = "<%=(sv.sequence).getColor()== null  ? "input_cell" :  (sv.sequence).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>" 
						value='<%=sv.sequence.getFormData().trim() %>' oninput="this.value=this.value.replace(/\D/g,'')" maxlength="<%=sv.sequence.length() %>" 
						size="<%=sv.sequence.length() %>" onFocus='doFocus(this)' onHelp='return fieldHelp("sequence")'>
						</div>
				  </div>
				</td>
				<%} %>
					
					<td class="tableDataTag"
						style="min-width:230px;text-align:left"
						align="center">
						<div class="form-group">
						<a href="javascript:;" class = 'tableLink'><span><%=sv.clntsname.getFormData()%></span></a>		
					
					</td>
					
					
					<td class="tableDataTag " style="min-width:150px;"
						align="left">
						<div class="form-group">
					<div class="input-group">
						<input name='<%="s5078screensfl" + "." +
						 "cltreln" + "_R" + count %>'
						id='<%="s5078screensfl" + "." +
						 "cltreln" + "_R" + count %>'
						type='text' 
						value='<%= sv.cltreln.getFormData() %>' 
						class = " <%=(sv.cltreln).getColor()== null  ? 
						"input_cell" :  
						(sv.cltreln).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.cltreln.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5078screensfl.cltreln)' onKeyUp='return checkMaxLength(this)' 
						 style = "width: 120px;"						 
						  <%if(sv.revcflg.equals("N")) { %> 
						 disabled="disabled"
						 <%} %>
						>		
						<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('<%="s5078screensfl" + "." +
						 "cltreln" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>	
							
						
					<!-- ILIFE-930-ends	 -->						
				</div></div>	</td>
						
                     <td class="tableDataTag"
						style="min-width:120px;text-align:left"
						align="left">	
						<div class="form-group">	
						<div class="input-group">			
					<input name='<%="s5078screensfl" + "." +
						 "bnycd" + "_R" + count %>'
						id='<%="s5078screensfl" + "." +
						 "bnycd" + "_R" + count %>'
						type='text' 
						value='<%= sv.bnycd.getFormData() %>' 
						class = " <%=(sv.bnycd).getColor()== null  ? 
						"input_cell" :  
						(sv.bnycd).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.bnycd.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5078screensfl.bnycd)' onKeyUp='return checkMaxLength(this)' 
						 style = "width: 120px;"
						 
						  <%if(sv.revcflg.equals("N")) { %> 
						 disabled="disabled"
						 <%} %>
						>	
						
						
						
							<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('<%="s5078screensfl" + "." +
						 "bnycd" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>	 						 
						 	
									
						
						<!-- ILIFE-930-ends	 -->				
			</div>	</div>	</td>
					                                       
					
					
					
					
					
					<td class="tableDataTag" style="min-width:100px;" align="left">
					<div class="form-group">
							<%if((new Byte((sv.revcflg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.revcflg.getFormData();
							%>
					 		<div id="s5078screensfl.revcflg_R<%=count%>" name="s5078screensfl.revcflg_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
						</div>
					</td>	
					
									
					<td class="tableDataTag" style="min-width:100px;" align="left">
					<div class="form-group">
					
						<%if((new Byte((sv.relto).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.relto.getFormData();
							%>
					 		<div id="s5078screensfl.relto_R<%=count%>" name="s5078screensfl.relto_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
					</div>
					</td>	
									
						<td class="tableDataTag" style="min-width:100px;" align="left">
					<div class="form-group">
						<%if((new Byte((sv.bnypc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
													
				
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.bnypc).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.bnypc);
										
									%>					
									<% 
								if((new Byte((sv.bnypc).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%> 
								<div id="s5078screensfl.bnypc_R<%=count%>" name="s5078screensfl.bnypc_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
				   										<%if(formatValue != null){%>
				   										
				   		 											<%=XSSFilter.escapeHtml(formatValue)%>
				   		 							
				   		
				   										<%}%>
				   				</div>
			
									
										<% }else {%>
									
									<!-- ILIFE-930-begins	 -->
									<input type='text' 
									 value='<%=formatValue%>' 
									 <%if (qpsf.getDecimals() > 0) {%>
										size='<%=sv.bnypc.getLength()+1%>'
										maxLength='<%=sv.bnypc.getLength()+1%>' 
									<%}else{%>
										size='<%=sv.bnypc.getLength()%>'
										maxLength='<%=sv.bnypc.getLength()%>' 
									<%}%>
									 onFocus='doFocus(this)' onHelp='return fieldHelp(s5078screensfl.bnypc)' onKeyUp='return checkMaxLength(this)' 
									 name='<%="s5078screensfl" + "." +
									 "bnypc" + "_R" + count %>'
									 id='<%="s5078screensfl" + "." +
									 "bnypc" + "_R" + count %>'
									class = " <%=(sv.bnypc).getColor()== null  ? 
									"input_cell" :  
									(sv.bnypc).getColor().equals("red") ? 
									"input_cell red reverse" : 
									"input_cell" %>" 
									  style = "width: <%=sv.bnypc.getLength()*12%> px;"
									  
									  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>' 
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);'
									  title='<%=formatValue %>'
									    <%if(sv.revcflg.equals("N")) { %>
						  					disabled="disabled"
						  				<%} %>
									 >
								 <%}%>
								<!-- ILIFE-930-ends	 -->										
						<%}%>					
					</div>
					</td>
					
					
						<td style="min-width:100px">
						<div class="form-group">
							<div class="input-group">





							<%
								if((new Byte((sv.effdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
								<span class="input-group-addon"  style="margin-left:1px;"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
							
				   </div>
		</div></td>
					<!-- ILIFE-3581 starts-->
					
					
					
					
<td style="min-width:100px">
<div class="form-group">
	<div class="input-group">





							<%
								if((new Byte((sv.enddateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.enddateDisp,(sv.enddateDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="enddateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.enddateDisp,(sv.enddateDisp.getLength()))%>
								<span class="input-group-addon" style="margin-left:1px;"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
							
				   </div>
		
</div></td>
<!-- ILIFE-3581 ends-->

		<%if (sv.actionflag.compareTo("N") != 0) {%>			
<td style="min-width:160px">
<div class="form-group">
	<div class="input-group">


                  <%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"paymthbf"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("paymthbf");
							optionValue = makeDropDownList( mappedItems , sv.paymthbf.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.paymthbf.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
						%>
						
						<% 
							if((new Byte((sv.paymthbf).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=longValue%>
						</div>
						
						<%
						longValue = null;
						%>`
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.paymthbf, fw, longValue, "paymthbf", optionValue, 0) %>
						<%
							} 
						%>  
							
				   </div>
		
</div></td>
<%} %>
	<%if (sv.actionflag.compareTo("N") != 0) {%>
<td>
					<div class="form-group">
					 <div class="input-group" style="width: 150px;">
							<input name='s5078screensfl.bankacckey_R<%=count %>' id='s5078screensfl.bankacckey_R<%=count %>' style="min-width: 150px;max-width: 150px;"
				type='text'
				value='<%=sv.bankacckey.getFormData()%>'
				maxLength='<%=sv.bankacckey.getLength()%>'
				size='<%=sv.bankacckey.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(bankacckey)' onKeyUp='return checkMaxLength(this)'
				
				<%
					if((new Byte((sv.bankacckey).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
				%>
				readonly="true"
				class="output_cell"
				
				<%
					}else if((new Byte((sv.bankacckey).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
				%>
					class="bold_cell" >
				 <!-- IFSU-414 Modified the class of the img by xma3 -->
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('bankacckey')); changeF4Image(this); doAction('PFKEY04')" class="divClass">
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0">
				</a> --%>
				
				<span class="input-group-btn">
					<button class="btn btn-info" type="button"
						onClick="doBank('<%=count %>')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</span>
				
				<%
					}else {
				%>
				
				class = ' <%=(sv.bankacckey).getColor()== null  ?
				"input_cell" :  (sv.bankacckey).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
					<button class="btn btn-info" type="button"
						onClick="doBank('<%=count %>')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</span>
				
				<%} %>
				 <div style="display:none;">
				  <input name='s5078screensfl.bkrelackey_R<%=count %>' id='s5078screensfl.bkrelackey_R<%=count %>' value='<%=sv.bkrelackey.getFormData()%>'>
				 </div>
				</div>
				</div>
					</td>
					<%} %>
	<%if (sv.actionflag.compareTo("N") != 0) {%>
<td
						style="color:#434343; padding-left: 5px;font-weight: bold;border-right: 1px solid #dddddd;"
						align="center">
						<div class="form-group">
						<%
							fieldItem=appVars.loadF4FieldsShort(new String[] {"bankkey"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("bankkey");
							optionValue = makeDropDownList( mappedItems , sv.bankkey.getFormData(),2,resourceBundleHandler);
				// 	<!-- MIBT-194 START -->
				if(mappedItems.containsKey((sv.bankkey.getFormData()).toString().trim())){
						    longValue = (String) mappedItems.get((sv.bankkey.getFormData()).toString().trim());
					}
				else{
			    	longValue=(sv.bankkey.getFormData()).toString().trim();
			    }
				// 			<!--    MIBT-194 END -->
						    if(longValue==null)
						    {
						      longValue="";
						    }
						%>
						<%if((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
						<div class='output_cell' style="width: 60px;">
							<%=longValue%>
						</div>
							<%longValue = null;%>
						<%
						}else {
						%>					
								
						<div class="input-group" style="width: 150px;">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankkey)%>
				           
				        </div>
						
						<%
					 	 }
					 	%>
					 </div>
					</td>
					<%} %>
					<%if (sv.actionflag.compareTo("N") != 0) {%>
					<td style="min-width:160px">
                      <div class="form-group">
	                    <div class="input-group">
	                    <div name='s5078screensfl.bnkcdedsc_R<%=count %>' id='s5078screensfl.bnkcdedsc_R<%=count %>'><%=sv.bnkcdedsc.getFormData()%></div>			
	                    </div>
                      </div>
                   </td>
					<%} %>

					
				</tr>

				<%
					count = count + 1;
					S5078screensfl.setNextScreenRow(sfl, appVars, sv);
					}
				%>
</tbody>
</table>
</div>

</DIV>

</div></div>

</div></div>

<script>
function doBank(count){

	var hasred = false;
	var hasnull = false;
	var haseror = false;
	
	
	$('#tablerow'+count).find("input[class*='red']").each(function(){
		if(this.id.indexOf("bankacckey") == -1){
			hasred = true;
		}
		
	});
	
	
	

	$('#tablerow'+count).find("input[id^='s5078screensfl.bnysel_R"+count+"']").each(function(){
		if($.trim($(this).val()) == ''){
			hasnull = true;
		}
	});
	
	$('#tablerow'+count).find("input[id^='s5078screensfl.sequence_R"+count+"']").each(function(){
		if($.trim($(this).val()) == ''){
			hasnull = true;
		}
	});
	
	
	
	$('#tablerow'+count).find("input[id^='s5078screensfl.effdate_R"+count+"']").each(function(){
		if($.trim($(this).val()) == '' || $.trim($(this).val()) == '0' || $.trim($(this).val()) == '99999999'){
			hasnull = true;
		}
	});
	
	$('#tablerow'+count).find("input[id^='s5078screensfl.cltreln_R"+count+"']").each(function(){
		if($.trim($(this).val()) == ''){
			hasnull = true;
		}
	});
	
	$('#tablerow'+count).find("select[id^='s5078screensfl.paymthbf_R"+count+"']").each(function(){
		if( $.trim($(this).val()) == ''){
			hasnull = true;
			haseror = true;
		}
	});
	
	
	
	
	if(hasred || hasnull){
		if(haseror){
	 	 $('#tablerow'+count).find("input[id^='s5078screensfl.bankacckey_R"+count+"']").each(function(){
	 		$(this).css("cssText","color: #FFFFFF !important; min-width: 150px;");
		 	$(this).val('EROR');
		 });
     	}
		
		doAction('PFKEY05');
	}else{
		doFocus(document.getElementById('s5078screensfl.bankacckey_R'+count));
		doAction('PFKEY04');
	}
		
	
} 
</script>

<script>

$(document).ready(function() {
	$('#dataTables-s5078').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:false,
        info:false
  	});
	
	$("input[id^='s5078screensfl.bankkey_R']").removeAttr("class");
	$("input[id^='s5078screensfl.bankkey_R']").css("border", "none").css("backgroundColor","transparent");


});




</script>



<%@ include file="/POLACommon2NEW.jsp"%>
