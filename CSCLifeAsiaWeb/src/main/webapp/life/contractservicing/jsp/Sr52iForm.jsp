<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR52I";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sr52iScreenVars sv = (Sr52iScreenVars) fw.getVariables();
%>
<%
	{
	}
%>
<style>
.col-md-3>.form-group>.input-group>.form-control.ellipsis, .col-md-3>.form-group>.form-control.ellipsis
	{
	padding-right: 5px !important;
}
</style>



<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Taxable Amount")%></label>
				</div>
			</div>

			<div class="col-md-3"></div>


			<div class="col-md-3">
				<div class="form-group">
					<div class="input-group" style="width: 205px; text-align: right;">
						<%
							if (((BaseScreenData) sv.aprem) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.aprem, (sv.aprem.getLength() + 1), null)
						.replace("absolute", "relative").replace("size='50'", "size='30'")%>
						<%
							} else if (((BaseScreenData) sv.aprem) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.aprem, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%
							if ((new Byte((sv.descript01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.descript01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.descript01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.descript01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3"></div>


			<div class="col-md-3">
				<div class="form-group">
				
					<%=smartHF.getHTMLVarExt(fw, sv.taxamt01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS,
					0, 205)%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div class="input-group" style="width: 150px;">
						<%
							if ((new Byte((sv.descript02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.descript02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.descript02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.descript02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3"></div>


			<div class="col-md-3">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.taxamt02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS,
					0, 205)%>
				</div>
			</div>
		</div>





		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Tax")%></label>
				</div>
			</div>

			<div class="col-md-3"></div>


			<div class="col-md-3">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.gndtotal, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS,
					0, 205)%>
				</div>
			</div>
		</div>


	</div>
</div>
<script>
       $(document).ready(function() {
    	   $('#gndtotal').width(109);
    	   $('#aprem').width(109);
    	   $('#taxamt01').width(109);
    	   $('#taxamt02').width(109); 
       });
       </script> 


<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
