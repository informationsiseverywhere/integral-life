<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6004";%>
<%@ include file="/POLACommon1.jsp"%>
<!---Ticket ILIFE-758--><%@ page import="com.csc.life.contractservicing.screens.*" %><!---Ticket ILIFE-758 ends-->
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S6004ScreenVars sv = (S6004ScreenVars) fw.getVariables();%>
<%{
}%>

<div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData CHDRNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number");%>
<%=smartHF.getLit(0, 0, CHDRNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</div>
<br/>
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->

<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>
<td width='251'>
<!--
<%StringData RSTATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status");%>
<%=smartHF.getLit(0, 0, RSTATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%>
</div>
<br/>
<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData PSTATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status");%>
<%=smartHF.getLit(0, 0, PSTATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
</div>
<br/>
<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData COWNNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner");%>
<%=smartHF.getLit(0, 0, COWNNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
</div>
<br/>
<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData PAYRNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Payer");%>
<%=smartHF.getLit(0, 0, PAYRNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Payer")%>
</div>
<br/>
<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.payorname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData AGNTNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency");%>
<%=smartHF.getLit(0, 0, AGNTNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Agency")%>
</div>
<br/>
<%if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.agentname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData OCCDATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commencement");%>
<%=smartHF.getLit(0, 0, OCCDATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Commencement")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData PTDATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date");%>
<%=smartHF.getLit(0, 0, PTDATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData CNTCURR_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency");%>
<%=smartHF.getLit(0, 0, CNTCURR_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%>
</div>
<br/>
<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData BTDATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date");%>
<%=smartHF.getLit(0, 0, BTDATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData BATCTRCDE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Significant Transaction");%>
<%=smartHF.getLit(0, 0, BATCTRCDE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Last Significant Transaction")%>
</div>
<br/>
<%if ((new Byte((sv.batctrcde).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.batctrcde.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.batctrcde.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.batctrcde.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
<td></td>
</tr> </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 --><%@ include file="/POLACommon2.jsp"%><!---Ticket ILIFE-758 ends-->
