<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR576";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr576ScreenVars sv = (Sr576ScreenVars) fw.getVariables();%>
<%
	{
if (appVars.ind02.isOn()) {
	sv.sumin.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
	sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind14.isOn()) {
	sv.sumin.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind14.isOn()) {
	sv.sumin.setColor(BaseScreenData.RED);
}
if (appVars.ind01.isOn()) {
	sv.frqdesc.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind03.isOn()) {
	sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind04.isOn()) {
	sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind24.isOn()) {
	sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind25.isOn()) {
	sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind24.isOn()) {
	sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind25.isOn()) {
	sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind06.isOn()) {
	sv.mortcls.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind05.isOn()) {
	sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind39.isOn()) {
	sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind41.isOn()) {
	sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind08.isOn()) {
	sv.liencd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind07.isOn()) {
	sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind31.isOn()) {
	sv.linstamt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind32.isOn()) {
	sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind42.isOn()) {
	sv.taxamt01.setInvisibility(BaseScreenData.INVISIBLE);
	sv.taxamt01.setEnabled(BaseScreenData.DISABLED);
}

if (appVars.ind75.isOn()) {
	sv.taxamt01.setEnabled(BaseScreenData.DISABLED);
}

if (appVars.ind35.isOn()) {
	sv.anntind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind33.isOn()) {
	sv.anntind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind42.isOn()) {
	sv.taxind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind42.isOn()) {
	sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind37.isOn()) {
	sv.pbind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind37.isOn()) {
	sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind26.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind27.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
}

if (appVars.ind31.isOn()) {
	sv.instPrem.setEnabled(BaseScreenData.DISABLED);
}

if (appVars.ind42.isOn()) {
	sv.taxamt02.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind54.isOn()) {
	sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
	}
if (appVars.ind55.isOn()) {
	sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind61.isOn()) {
	sv.waitperiod.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind62.isOn()) {
	sv.waitperiod.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind61.isOn()) {
	sv.waitperiod.setColor(BaseScreenData.RED);
}
if (!appVars.ind61.isOn()) {
	sv.waitperiod.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind56.isOn()) {
	sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind63.isOn()) {
	sv.bentrm.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind64.isOn()) {
	sv.bentrm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind63.isOn()) {
	sv.bentrm.setColor(BaseScreenData.RED);
}
if (!appVars.ind63.isOn()) {
	sv.bentrm.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind57.isOn()) {
	sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind65.isOn()) {
	sv.poltyp.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind66.isOn()) {
	sv.poltyp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind65.isOn()) {
	sv.poltyp.setColor(BaseScreenData.RED);
}
if (!appVars.ind65.isOn()) {
	sv.poltyp.setHighLight(BaseScreenData.BOLD);
}

if (appVars.ind58.isOn()) {
	sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind67.isOn()) {
	sv.prmbasis.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind68.isOn()) {
	sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind67.isOn()) {
	sv.prmbasis.setColor(BaseScreenData.RED);
}
if (!appVars.ind67.isOn()) {
	sv.prmbasis.setHighLight(BaseScreenData.BOLD);
}
/*BRD-306 starts */
if (appVars.ind59.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind52.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind60.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
/*BRD-306 ends */
//BRD-009-STARTS
if (appVars.ind70.isOn()) {
	sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind69.isOn()) {
	sv.statcode.setEnabled(BaseScreenData.DISABLED);
}
//BRD-009-ENDS
//BRD-NBP-011 starts
if (appVars.ind72.isOn()) {
	sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
	sv.dialdownoption.setColor(BaseScreenData.RED);
}
if (!appVars.ind72.isOn()) {
	sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind73.isOn()) {
	sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind71.isOn()) {
	sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
}
//ICIL-560 FWANG3
if (appVars.ind77.isOn()) {
	sv.cashvalarer.setInvisibility(BaseScreenData.INVISIBLE);
}	
//BRD-NBP-011 ends

if (appVars.ind91.isOn()) {
    sv.fuind.setEnabled(BaseScreenData.DISABLED);
    sv.fuind.setInvisibility(BaseScreenData.INVISIBLE);
}

//ILJ-46
if (appVars.ind123.isOn()) {
	sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
}
//end
//IBPLIFE-2135 Start
if (sv.nbprp126lag.compareTo("N") != 0){
	if (appVars.ind80.isOn()) {
		sv.validflag.setReverse(BaseScreenData.REVERSED);
		sv.validflag.setColor(BaseScreenData.RED);
	}
	 if (appVars.ind83.isOn()) {
		sv.validflag.setEnabled(BaseScreenData.DISABLED);
	} 
	if (!appVars.ind80.isOn()) {
		sv.validflag.setHighLight(BaseScreenData.BOLD);
	}
	
	if (appVars.ind82.isOn()) {
		sv.covrprpse.setReverse(BaseScreenData.REVERSED);
		sv.covrprpse.setColor(BaseScreenData.RED);
	}
	 if (appVars.ind85.isOn()) {
		sv.covrprpse.setEnabled(BaseScreenData.DISABLED);
	} 
	if (!appVars.ind82.isOn()) {
		sv.covrprpse.setHighLight(BaseScreenData.BOLD);
	}
	}
//IBPLIFE-2135 End
	}
%>
 <!--  ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts-->
<style>
@media \0screen\,screen\9
{
.iconPos{margin-bottom:1px}
}
#effdateDisp{width:140px}
</style>
<!--  ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends-->











<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>
    	 					
    	 					<table>
    	 					<tr>
    	 					<td>
    	 					
    	 					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
	</td>
	<td>
	
<%  	if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  		
  		</td>
  		<td>
<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
    	 					
    	 			</td>
    	 			</tr>
    	 			</table>
    	 					
    	 					</div></div>
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 	<div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>

							<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							</td>
							<td>

						<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
								"blank_cell" : "output_cell" %>' style="max-width:250px;margin-left: 1px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						</td>
						</tr>
						</table>
					</div></div>
    	 					
    	 					
    	 					
    	 					 
    	 					<div class="col-md-2"></div>
    	 					
    	 					
    	 					
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	 <label  style="white-space: nowrap;">    <%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
    	 					
  <%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
 
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

 
<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
<%}%>
	
    	 					
    	 					</div></div>
    	 					
    	 				
    	 				
    	 				
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     <%if ((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>	
    	 					
    	 					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statcode");
	optionValue = makeDropDownList( mappedItems , sv.statcode.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.statcode.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.statcode).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:190px;"> 
<%
} 
%>

<select name='statcode' type='list' style="width:190px;"
<% 
	if((new Byte((sv.statcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statcode).getColor())){
%>
</div>
<%
} 
%>

<%
}}  
%>
<% longValue = null;%>
</div></div>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>

							<%					
								if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:71px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
						</td>
						<td style="padding-left: 1px;">	
  		
						<%					
						if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' 
										style='min-width:71px;margin-left:1px;" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
						</table>
    	 					
    	 				</div></div>
    	 					
    	 					
    	 					
    	 					 
			    	     
			    	     
    	 					<div class="col-md-2"></div> 
    	 					<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


			    	     
			    	     
			    	     </div></div>
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     
			    	     <div class="col-md-2"> 
			    	     <div class="form-group">
    	 					
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
    	 					

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

		<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
  
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>

<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 			
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>
<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="width:95px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 				</div></div>
    	 				
    	 				</div>
    	 					
<!-- IBPLIFE-2135 Start -->	
	<% if (sv.nbprp126lag.compareTo("Y") != 0){%>
		<hr>
		<%} else{%>
			<div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assure / Premium")%></a>
                        </li>
                       
                        <li>
                        	<a href="#prem_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></a>
                        </li>
                    </ul>
                    </div>
                  </div>
             <div class="tab-content">
               	<div class="tab-pane fade in active" id="basic_tab">
               	<%} %>
               	<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
<div class="input-group" style="width:82px;">
  		  <%
						if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;" : "width:82px;"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
    	 		</div>		
    	 				
    	 				</div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 				 	<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit amount")%></label>	
 
	
	<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='sumin' 
type='text'
<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width:82px; "<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
 
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    
    <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>	


	<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mortcls");
						optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:120px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mortcls).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 120px;">
						<%
							}
						%>

						<select name='mortcls' type='list' style="width: 210px;"
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mortcls).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
 
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 	<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>				
    	 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>	
    	 					

<%	
	if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("prmbasis");
	optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.prmbasis).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='prmbasis' type='list' style="width:145px;"
<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.prmbasis).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.prmbasis).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
    	 						<%
	} 
%> 				
    	 				
    	 				</div>	
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		   
			    	     	<!--  ILJ-46 -->
							<%
								if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%></label>
							<% } else{%>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
							<% } %>
							<!--  END  --> 	     
    	 					
<!-- <div class="input-group"> -->

 							<table>
 							<tr>
 							<td>
						
							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>


						
						</td>
						
						<td>
						



							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;margin-left: 1px;"" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						
						</td>
						</tr>
						</table>
						
						</div>
					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	
			    	     	<!--  ILJ-46 -->
							<%
								if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
							<% } else{%>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
							<% } %>
							<!--  END  -->     
    	 					
 <div class="input-group">
<%	
	longValue = sv.riskCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>


<% }else {%> 


 
 <input name='riskCessDateDisp' 
type='text' 
value='<%=sv.riskCessDateDisp.getFormData()%>' 
maxLength='<%=sv.riskCessDateDisp.getLength()%>' 
size='<%=sv.riskCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	style="width:80px;"

<%
	}else if((new Byte((sv.riskCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:80px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('riskCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	} else { 
%>

class = ' <%=(sv.riskCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.riskCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:120px;">

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('riskCessDateDisp'), '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} longValue = null;}%>
    	 					
    	 				</div>	
    	 					</div></div> 
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
    	 					
<div class="input-group">
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
    	 					
    	 					
    	 					
    	 				</div>	</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 			<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>		
    	 		 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>

<%	
	if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"waitperiod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("waitperiod");
	optionValue = makeDropDownList( mappedItems , sv.waitperiod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.waitperiod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>
							
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.waitperiod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='waitperiod' type='list' style="width:145px;"
<% 
	if((new Byte((sv.waitperiod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.waitperiod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.waitperiod).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>
    	 					
    	 					
    	 					</div></div>  
    	 					
    	 					<%
	} 
%>
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 				 	 					  	 					
    	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>
						
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
							if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>



						
						</td>
						
						<td>
						


							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						
						</td>
						</tr>
						</table>
				


					</div></div>



   
    	 				
    	 				
    	 				
    	 					
    	 					
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	
			    	     <div class="form-group">     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
 <div class="input-group">
<%	
	longValue = sv.premCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 



<input name='premCessDateDisp' 
type='text' 
value='<%=sv.premCessDateDisp.getFormData()%>' 
maxLength='<%=sv.premCessDateDisp.getLength()%>' 
size='<%=sv.premCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	style="width:80px;"

<%
	}else if((new Byte((sv.premCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:80px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.premCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.premCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:120px;">

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('premCessDateDisp'), '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} longValue = null;}%>
 
 
    	 				</div></div></div></div> 
    	 				
    	 				
    	 				
    	 				
    	 				
    	 				 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.liencd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					<select name='liencd' type='list' style="width:145px;"
					<% 
				if((new Byte((sv.liencd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.liencd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.liencd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	


    	 				
    	 				</div></div>
    	 				
    	 				
    	 				<%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {%>
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>

	<%	
	if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bentrm"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bentrm");
	optionValue = makeDropDownList( mappedItems , sv.bentrm.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bentrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.bentrm).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='bentrm' type='list' style="width:145px;"
<% 
	if((new Byte((sv.bentrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.bentrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.bentrm).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>
    	 				
    	 				</div></div>	 	
    	 					
    	 					<%
					} 
					%>
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 		 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age / Term")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>
 
					

							<%
								qpsf = fw.getFieldXMLDef((sv.benCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessAge' type='text'
								<%if ((sv.benCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>' <%}%>
								size='<%=sv.benCessAge.getLength()%>'
								maxLength='<%=sv.benCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessAge).getColor() == null ? "input_cell"
						: (sv.benCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>

						
						</td>
						<td>

						


							<%
								qpsf = fw.getFieldXMLDef((sv.benCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessTerm' type='text'
								<%if ((sv.benCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width:50px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>' <%}%>
								size='<%=sv.benCessTerm.getLength()%>'
								maxLength='<%=sv.benCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessTerm).getColor() == null ? "input_cell"
						: (sv.benCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>

						
						</td>
						</tr>
						</table>
					

</div></div>



<div class="col-md-3"> 
 <div class="form-group">	
            <label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess date")%></label>
	 <div class="input-group">
	<%	
	longValue = sv.benCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.benCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 


<input name='benCessDateDisp' 
type='text' 
value='<%=sv.benCessDateDisp.getFormData()%>' 
maxLength='<%=sv.benCessDateDisp.getLength()%>' 
size='<%=sv.benCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(benCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.benCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	style="width:80px;"

<%
	}else if((new Byte((sv.benCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:80px;">
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('benCessDateDisp'),   '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>


<%
	}else { 
%>

class = ' <%=(sv.benCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.benCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:120px;">

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('benCessDateDisp'), '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>


<%} longValue = null;}%>
    	 				
    	 			</div></div>	</div> 
    	 				
    	 				
    	 				
    	 				
    	 					
    	 					
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	
			    	     <div class="form-group">     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>
<div class="input-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bappmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bappmeth");
	optionValue = makeDropDownList( mappedItems , sv.bappmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.bappmeth).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					<select name='bappmeth' type='list' style="width:145px;"
					<% 
				if((new Byte((sv.bappmeth).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.bappmeth).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.bappmeth).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
 
 
    	 				</div></div></div></div>
    	 				
    	 				
    	 				
    	 				
    	 				<%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
    	 				 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
<%	
	if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"poltyp"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("poltyp");
	optionValue = makeDropDownList( mappedItems , sv.poltyp.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.poltyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.poltyp).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:147px;"> 
<%
} 
%>

<select name='poltyp' type='list' style="width:145px;"
<% 
	if((new Byte((sv.poltyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.poltyp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.poltyp).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	


    	 				
    	 				</div></div>
    	 					<%
					} 
					%>
    	 				</div> 
    	 				
    	 				
    	 				
    	 				
    	 				<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
    	 				 <div class="row">
    	 				 
    	 				 <div class="col-md-9"></div>
    	 				 
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
    	 					
    	 					<%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.dialdownoption).getColor())){
%>
<div style="border:2px; border-style: solid; border-color: #B55050;  width:175px;"> 
<%
} 
%>

<select name='dialdownoption' type='list' style="width:175px;"
<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dialdownoption).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dialdownoption).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	



</div></div></div>
<%}%>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
 
	
	<hr>	
    	 					
    	 	<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>				
    	 <div class="row">
    	 					<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%><div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
    	 					
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div><%}%>
                        
                        <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%}%>
                        
                        
                        <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>

<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%}%>
                        
                        
                        <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>

<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%}%>
                        
                        
                        
                        </div>   <%}%>	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					 <div class="row">
    	 					 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 				<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

    	 					
    	 					</div></div><%}%><%}%>
    	 					
    	 					
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <!-- ILIFE-8323 Starts -->
 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>			    	     		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>

<%}	
 if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0){ %>			    	     		    	     
	<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
				<!-- ILIFE-8323 ends -->
<%}	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width: 145px !important;text-align: right;padding-right:5px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width: 145px !important;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
    	 					
    	 					</div></div>
    	 					
    	 					<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%}}%>
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>

	<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
<%if(sv.zstpduty01.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:  145px !important;  text-align: right;\' ")%>	
<%} %>
<%}else {%>
<%}%>
    	 					</div></div><%}%>
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 		 <div class="row">
    	 					    	 					 
    	 					    	 					 
    	 			 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>

	<%if ((new Byte((sv.linstamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
	<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='linstamt' 
type='text'
<%if((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px !important;"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.linstamt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.linstamt) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)-3%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.linstamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" 
<%
	}else if((new Byte((sv.linstamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  

<%
	}else { 
%>

	class = ' <%=(sv.linstamt).getColor()== null  ? 
			"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

<%} %>
<%
longValue = null;
formatValue = null;
%>	
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					<%if ((new Byte((sv.taxamt01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Ttl Prem w/Tax")%></label>

<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt01).getFieldName());


			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='taxamt01' 	
type='text'
<%if((sv.taxamt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width: 145px !important; "<% }%>

	value='<%=valueThis %>'
			 <%

	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt01.getLength(), sv.taxamt01.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt01.getLength(), sv.taxamt01.getScale(),3)-3%>'
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt01)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxamt01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt01).getColor()== null  ? 
			"input_cell" :  (sv.taxamt01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
 
<%
longValue = null;
formatValue = null;
%>		
    	 					</div></div><%} %>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%></label>

	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
			
	%>

<%if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
<input name='instPrem' 
type='text'
<%if((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px !important;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)%>'
maxLength='<%= sv.instPrem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>				
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
				<%
					if ((new Byte((sv.cashvalarer).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
    	 		 <div class="col-md-3">
							<div class="form-group">
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Value in Arrears")%></label>
								<%
									qpsf = fw.getFieldXMLDef((sv.cashvalarer).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.cashvalarer,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
										if (!((sv.cashvalarer.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}
			
										if (!formatValue.trim().equalsIgnoreCase("")) {
								%>
								<div class="output_cell"
									style="width: 175px !important; text-align: right;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
									} else {
								%>
			
								<div class="blank_cell" style="width: 175px !important;">
									&nbsp;</div>
			
								<%
									}
								%>
								<%
									longValue = null;
										formatValue = null;
								%>
							</div>
				</div>

				<%
					}
				%>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					 
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="row">
    	 					
 <%if(!(sv.anntind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.anntind
.getEnabled()==BaseScreenData.DISABLED)){
%>  	
 <div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Details")%></label>
 <div class="form-group" style="width:60px">
<%if(((BaseScreenData)sv.anntind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.anntind,( sv.anntind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.anntind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.anntind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
 
    	 					
    	 	</div>				</div>
	
	</div><%}%>
	
	
	<div class="col-md-1"></div>
	<%if(!(sv.pbind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.pbind
.getEnabled()==BaseScreenData.DISABLED)){
%> 	
	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></label>
 <div class="form-group" style="width:60px">
<%if(((BaseScreenData)sv.pbind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pbind,( sv.pbind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pbind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pbind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
 
    	 					
    	 					</div>
	
	</div></div>
	<%}%>
	
	</div>
	
	<% if (sv.nbprp126lag.compareTo("N") != 0){%>
	</div>
	<div class="tab-pane fade" id="prem_tab">
           		<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
								<table>
								<tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trancd, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trandesc, true)%></td>
							</tr>
							</table>
						</div>
				  </div>
				  
				  <div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
								<%
						if ((new Byte((sv.effdatexDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.effdatexDisp.getFormData()%>' maxLength='<%=sv.effdatexDisp.getLength()%>' 
										size='<%=sv.effdatexDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>								
					
					</div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdatexDisp"
						data-link-format="dd/mm/yyyy" >
						<%=smartHF.getRichTextDateInput(fw, sv.effdatexDisp, (sv.effdatexDisp.getLength()))%>
						<span class="input-group-addon" style="visibility: hidden;"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<%
						}
					%>
  </div> 
  </div>
             </div>
             
             <div class="row">
             	<div class="col-md-9">
             	<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose")%></label>

					<table><tr>
							<td>
							
							<textarea name='covrprpse' style='width:400px;height:70px;resize:none;border: 2px solid !important;'
							type='text' 
							
							<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
							
							<%
							
									formatValue = (sv.covrprpse.getFormData()).toString();
							
							%>
							 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>
							
							size='<%= sv.covrprpse.getLength() %>'
							maxLength='<%= sv.covrprpse.getLength() %>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.covrprpse).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.covrprpse).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.covrprpse).getColor()== null  ? 
										"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							><%=XSSFilter.escapeHtml(formatValue)%></textarea>
							
							</td></tr>
							
							</table>
					
              </div>
             </div>
             </div>
             
             
             </div>
             </div>
             <%} %>
 <!-- IBPLIFE-2135 End -->              	
         	

<Div id='mainForm_OPTS' style='visibility:hidden;'>
<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
 
<%=smartHF.getMenuLink(sv.comind, resourceBundleHandler.gettingValueFromBundle("Agent Commission Split"))%>

<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"))%>

<%=smartHF.getMenuLink(sv.exclind, resourceBundleHandler.gettingValueFromBundle("Exclusions"))%>

<%=smartHF.getMenuLink(sv.fuind, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>

</div>





 <div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
</tr></table></div>

</div></div>




 
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
