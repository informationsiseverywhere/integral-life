<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5697";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5697ScreenVars sv = (S5697ScreenVars) fw.getVariables();%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Plan  Coverage Risk Status              Coverage Premium Status");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind04.isOn()) {
			sv.sfcdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.confirm.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>







<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>

<!-- <div class="input-group"> -->
<table><tr><td>



	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



</td><td>

	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
	</div></div>


<div class="col-md-4"></div>
<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>





	
  		
		<%			
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("register");
			longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div></div></div>



<div class="row">
<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>





	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> 'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div>

                       <!--  <div class="col-md-2"></div>  -->
				    		<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>





	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
<!-- <div class="col-md-2"></div> -->
				    		
<div class="col-md-4"> 
				    		<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%}%>




<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div></div>



<div class="row">
<div class="col-md-4"> 
				    		<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
<!-- <div class="input-group"> -->

<table><tr><td>
	
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
	<!-- </div> --></div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>


<div class="row">
<div class="col-md-4"> 
				    		<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
<!-- <div class="input-group"> -->

<table><tr><td>


	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

  		</td><td>
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:450px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr></table>
<!-- </div> --></div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>



<div class="row">
<div class="col-md-12"> 
				    		<div class="form-group">

<%if ((new Byte((sv.sfcdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.sfcdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sfcdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sfcdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
<div id='sfcdesc' class='label_txt'class='label_txt'  onHelp='return fieldHelp("sfcdesc")'><%=sv.sfcdesc.getFormData() %></div>
		
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	


<!-- ILIFE-895 Start  -->


<!-- ILIFE-895 End  -->
</div></div></div>
<br>

		<%
		GeneralTable sfl = fw.getTable("s5697screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script language="javascript">
				        $(document).ready(function(){
							var rows = <%=sfl.count()+1%>;
							var isPageDown = 1;
							var pageSize = 1;
							var fields = new Array("zintcd","sbusin","tsumins");
							operateTableForSuperTable(rows,isPageDown,pageSize,fields,"demoTable",null,1);

							function doSuperTable(){
								new superTable("demoTable", {
											fixedCols : 1,
											headerRows : 1,
											colWidths : [90,250,250],
											isReadOnlyFlag: true,
											hasHorizonScroll:"N",
											addRemoveBtn: "N",
											moreBtn: "Y"
								});
							}

						    //var dis = $("#content2").css("display");
							//$("#content2").css("display", "block");
							doSuperTable();
							//$("#content2").css("display", dis);
				        });
			    	</script>
				<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5697' width="100%">
					  <thead>
					  <tr class='info'>
					    	<th><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
					    	<th><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
					    	<th><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
					    	</tr>
						</thead>
	                  		
	<%
	S5697screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5697screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr id='<%="tablerow"+count%>' height='22px'>
						    										
													
					
					 						 
						 
						 						 	<div style='display:none; visiblity:hidden;'>
						 						 <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5697screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5697screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s5697screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%> px;"
						  
						  >
						</div>
				    	<td align="left">														
																
									
												
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5697screensfl" + "." +
					      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.planSuffix.getFormData()%></span></a>							 						 		
						
						
														 
				
									</td>
				    	<td align="left">									
																
									
											
						<%= sv.statcode.getFormData()%>&nbsp;&nbsp;
						<%= sv.rstatdesc.getFormData()%>
						</td>
				    	<td align="left">	
						<%= sv.pstatcode.getFormData()%>&nbsp;&nbsp;
						<%= sv.pstatdesc.getFormData()%>
					</td>
					
	</tr>

	<%
	count = count + 1;
	S5697screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div></div></div></div>



<script>
$(document).ready(function() {
    $('#dataTables-s5697').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false
       
    } );
   
} );

</script>

</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

