
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR51T";%>
<%@ include file="/POLACommon1NEW.jsp"%>	
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr51tScreenVars sv = (Sr51tScreenVars) fw.getVariables();%>
<%{
}%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-8" > 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    		    	 <table><tr><td>
						    		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td>
<%if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:100px; margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
				      			    
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						</div>
				   </div>		
			
			    	
		    </div>
				   
			
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					    		     <div class="input-group">
						    		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:80px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
							
										
						</div>
				   </div>	
		    </div>
		
				<br>
				   	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-sr51t'>
							<thead>
								<tr class='info'>
			
 
<th><center>TrnNo</center></th>
<th style="min-width:200px;"><center>Premium Holiday Start Date</center></th>
<th style="min-width:200px;"><center>Premium Holiday End Date</center></th>
<th><center>ReqType</center></th>
<th><center>BackPay</center></th>
<th><center>Stat</center></th>
<th><center>TransDate</center></th>
<th><center>User ID</center></th>
<th><center>VFlag</center></th>

			      
			        
		 	        
		 	        </tr>
			 </thead>
			 <%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=60;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=192;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=180;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=84;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=84;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

calculatedValue=60;
totalTblWidth += calculatedValue;
tblColumnWidth[5] = calculatedValue;

calculatedValue=180;
totalTblWidth += calculatedValue;
tblColumnWidth[6] = calculatedValue;

calculatedValue=156;
totalTblWidth += calculatedValue;
tblColumnWidth[7] = calculatedValue;

calculatedValue=72;
totalTblWidth += calculatedValue;
tblColumnWidth[8] = calculatedValue;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("sr51tscreensfl");
GeneralTable sfl1 = fw.getTable("sr51tscreensfl");
Sr51tscreensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 118;
}	
%>

	
	

<%
Sr51tscreensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (Sr51tscreensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>
<%{
	
	//start ILIFE-776 date fields should be disabled on enquiry mode		
		sv.frmdateDisp.setEnabled(BaseScreenData.DISABLED);
		
		sv.todateDisp.setEnabled(BaseScreenData.DISABLED);
		
		sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
	//end ILIFE-776 date fields should be disabled on enquiry mode

}%>
			
			<tbody>
			
			
			
			<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sr51tscreensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sr51tscreensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="sr51tscreensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >



<td style="width:<%=tblColumnWidth[0]%>px;" 
	<%if(!(((BaseScreenData)sv.seqnbr) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
	<%if((new Byte((sv.seqnbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.seqnbr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
			<%if((new Byte((sv.seqnbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%if(hyperLinkFlag){%>
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sr51tscreensfl" + "." +
					      "slt" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.seqnbr.getFormData()%></span></a>							 						 		
						  <%}else{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.seqnbr.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>



<td style="width:300px;" 
	<%if(!(((BaseScreenData)sv.frmdateDisp) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >				
	<%if((new Byte((sv.frmdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<%	longValue = sv.frmdateDisp.getFormData();  	%>
		<% if((new Byte((sv.frmdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
			<% //ILIFE-776 date fields should be disabled on enquiry mode %>  
			<div id="sr51tscreensfl.frmdateDisp_R<%=count%>" name="sr51tscreensfl.frmdateDisp_R<%=count%>" style= "width: <%=sv.frmdateDisp.getLength()*15%> px; margin-left:10px">

						   		
						   		<%=longValue%>
						   		
						   		
						   </div>
					
					<%
					longValue = null;
					%>
		<% }else {%> 
					<input name='<%="sr51tscreensfl" + "." +
					 "frmdateDisp" + "_R" + count %>'
					id='<%="sr51tscreensfl" + "." +
					 "frmdateDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.frmdateDisp.getFormData() %>' 
					class = " <%=(sv.frmdateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.frmdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.frmdateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr51tscreensfl.frmdateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.frmdateDisp.getLength()*12%> px; margin-left:10px"
					>
					
					<a href="javascript:;"
					onClick="showCalendar(this, document.getElementById('<%="sr51tscreensfl" + "." +
					 "frmdateDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
					 
					 <img style="width:17px" src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0">
					</a>
					<%}%>  			
		<%}%>
</td>



<td style="width:300px;" 
	<%if(!(((BaseScreenData)sv.todateDisp) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >				<%if((new Byte((sv.todateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	<%	longValue = sv.todateDisp.getFormData();  	%>
					<% 
						if((new Byte((sv.todateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<% //ILIFE-776 date fields should be disabled on enquiry mode %> 					
					<div id="sr51tscreensfl.todateDisp_R<%=count%>" name="sr51tscreensfl.todateDisp_R<%=count%>" style= "width: <%=sv.todateDisp.getLength()*15%> px; margin-left:10px">
					
						   		
						   		<%=longValue%>
						   		
					
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='<%="sr51tscreensfl" + "." +
					 "todateDisp" + "_R" + count %>'
					id='<%="sr51tscreensfl" + "." +
					 "todateDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.todateDisp.getFormData() %>' 
					class = " <%=(sv.todateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.todateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.todateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr51tscreensfl.todateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.todateDisp.getLength()*12%> px;"
					>
					
					<a href="javascript:;" 
					onClick="showCalendar(this, document.getElementById('<%="sr51tscreensfl" + "." +
					 "todateDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
					 
					 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0">
					</a>
					<%}%>  			
		<%}%>
</td>



<td style="width:<%=tblColumnWidth[3]%>px;" 
	<%if(!(((BaseScreenData)sv.logtype) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.logtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.logtype.getFormData();
							%>
					 		<div id="sr51tscreensfl.logtype_R<%=count%>" name="sr51tscreensfl.logtype_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[4]%>px;" 
	<%if(!(((BaseScreenData)sv.apind) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.apind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.apind.getFormData();
							%>
					 		<div id="sr51tscreensfl.apind_R<%=count%>" name="sr51tscreensfl.apind_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[5]%>px;" 
	<%if(!(((BaseScreenData)sv.activeInd) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.activeInd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.activeInd.getFormData();
							%>
					 		<div id="sr51tscreensfl.activeInd_R<%=count%>" name="sr51tscreensfl.activeInd_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[6]%>px;" 
	<%if(!(((BaseScreenData)sv.effdateDisp) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >				<%if((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	<%	longValue = sv.effdateDisp.getFormData();  	%>
					<% 
						if((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<% //ILIFE-776 date fields should be disabled on enquiry mode %>
					<div id="sr51tscreensfl.effdateDisp_R<%=count%>" name="sr51tscreensfl.effdateDisp_R<%=count%>" style= "width: <%=sv.effdateDisp.getLength()*15%> px; margin-left:10px" >

						   		
						   		<%=longValue%>
						   		
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='<%="sr51tscreensfl" + "." +
					 "effdateDisp" + "_R" + count %>'
					id='<%="sr51tscreensfl" + "." +
					 "effdateDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.effdateDisp.getFormData() %>' 
					class = " <%=(sv.effdateDisp).getColor()== null  ? 
					"input_cell" :
					(sv.effdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.effdateDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr51tscreensfl.effdateDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style = "width: <%=sv.effdateDisp.getLength()*12%> px;"
					disabled>
					
					<a href="javascript:;" 
					onClick="showCalendar(this, document.getElementById('<%="sr51tscreensfl" + "." +
					 "effdateDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
					 
					 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0">
					</a>
					<%}%>  			
		<%}%>
</td>



<td style="width:<%=tblColumnWidth[7]%>px;" 
	<%if(!(((BaseScreenData)sv.crtuser) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.crtuser).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.crtuser.getFormData();
							%>
					 		<div id="sr51tscreensfl.crtuser_R<%=count%>" name="sr51tscreensfl.crtuser_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td style="width:<%=tblColumnWidth[8]%>px;" 
	<%if(!(((BaseScreenData)sv.validflag) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.validflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.validflag.getFormData();
							%>
					 		<div id="sr51tscreensfl.validflag_R<%=count%>" name="sr51tscreensfl.validflag_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



</tr>
<%	count = count + 1;
Sr51tscreensfl.setNextScreenRow(sfl, appVars, sv);
}
%>

			</tbody></table>
			<INPUT type="HIDDEN" name="screenIndicArea" id="screenIndicArea" value="<%=	(sv.screenIndicArea.getFormData()).toString() %>" >
			</div></div></div></div>
			
			
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->



<script>
	$(document).ready(function() {
    	$('#dataTables-sr51t').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
			scrollX: true,
			info: true
      	});
    });
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE 2732 starts -->
<%if(!cobolAv3.isPagedownEnabled()){%>
<script type="text/javascript">
window.onload = function()
{
setDisabledMoreBtn();
}
</script>
<%}%>
<!-- ILIFE 2732 ends -->
