<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>


<%
	String screenName = "SR580";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%
	Sr580ScreenVars sv = (Sr580ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract no ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life no ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage no ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider no ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life Assured ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Stat. fund ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub-sect ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Joint life ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Plan Policies ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Policy Num ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to 1");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit amount ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<% 	
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk cess Age / Term ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk cess date ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Prem cess Age / Term ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Prem cess date ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bene cess Age / Term ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bene cess date ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Mortality class ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Lien code ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bonus Appl Method ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Loaded Premium ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Special terms ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Annuity details ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Agent Commission Split ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Breakdown ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life (J/L) ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium with Tax ");
%>
<%
	StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Stamp Duty");
%>
<%
	{
		appVars.rolldown(new int[] { 10 });
		appVars.rollup(new int[] { 10 });
		if (appVars.ind43.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind05.isOn()) {
			sv.planSuffix.setColor(BaseScreenData.WHITE);
		}
		if (appVars.ind50.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind50.isOn()) {
			generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.zrsumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.zrsumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.zrsumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.zrsumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
			sv.zrsumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.frqdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind38.isOn()) {
			sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind41.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind38.isOn()) {
			sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind33.isOn()) {
			sv.anntind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.anntind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.anntind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind36.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}

		if (appVars.ind84.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind59.isOn()) {
			sv.linstamt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.linstamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind61.isOn()) {
			sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.linstamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.linstamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3364-STARTS
		if (appVars.ind55.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3364--ENDS
		/*BRD-306 starts */
		if (appVars.ind62.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind63.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 ends */
		if (appVars.ind64.isOn()) {
			sv.waitperiod.setReverse(BaseScreenData.REVERSED);
			sv.waitperiod.setColor(BaseScreenData.RED);
		}
		if (appVars.ind66.isOn()) {
			sv.bentrm.setReverse(BaseScreenData.REVERSED);
			sv.bentrm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind68.isOn()) {
			sv.poltyp.setReverse(BaseScreenData.REVERSED);
			sv.poltyp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
			sv.prmbasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.waitperiod.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind66.isOn()) {
			sv.bentrm.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind68.isOn()) {
			sv.poltyp.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind70.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.waitperiod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) {
			sv.bentrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind69.isOn()) {
			sv.poltyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-NBP-011 starts
		if (appVars.ind82.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind80.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind81.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		if (appVars.ind91.isOn()) {
			sv.fuind.setEnabled(BaseScreenData.DISABLED);
			sv.fuind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		//ILJ-46
		if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//end
	}
%>




<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>

					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 300px; margin-left: 1px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>

				</div>
			</div>

		</div>





		<div class="row">
			<div class="col-md-7">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px; max-width: 300px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
						</tr>
					</table>
				</div>
			</div>



			<div class="col-md-1"></div>




			<div class="col-md-3">
				<div class="form-group">




					<%
						if (!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.zagelit.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.zagelit.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div id='zagelit' class='label_txt' class='label_txt'
						onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>
					<%
						longValue = null;
						formatValue = null;
					%>



					<%
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

						if (!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 50px;">&nbsp;</div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>







		</div>







		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 71px;">

									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>




							</td>
							<td>
								<%
									if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlinsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlinsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px; max-width: 300px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
						</tr>
					</table>

				</div>
			</div>



			<div class="col-md-4"></div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Policies")%></label>

					<%
						if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.numpols.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.numpols.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>




				</div>
			</div>






			<div class="col-md-2">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

					<%
						if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.planSuffix.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.planSuffix.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>






		</div>




		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>


					<%
						if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

					<%
						if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>



			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

					<%
						if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>


			<%
				if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>


					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statFund");
							longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>


			<%
				if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>


					<%
						if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>


			<%
				if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>


					<%
						if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>

		</div>

		<br>


		<hr>
		<br>


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>


					<%
						if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;" : "width:100px;"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>




			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Ammount")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.zrsumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zrsumin' type='text'
						<%if ((sv.zrsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zrsumin.getLength(), sv.zrsumin.getScale(), 3)%>'
						maxLength='<%=sv.zrsumin.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zrsumin)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.zrsumin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zrsumin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zrsumin).getColor() == null ? "input_cell"
						: (sv.zrsumin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>




			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mortcls");
						optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:120px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mortcls).getColor())) {
					%>
					<div
						style="border: 1px; border-color: #B55050; width: 150px;">
						<%
							}
						%>

						<select name='mortcls' type='list' style="width: 210px;"
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mortcls).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>





				</div>
			</div>






			<%
				if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>



					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("currcd");
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;" : "width:100px;"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>






				</div>
			</div>


		</div>






		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
							<!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
								<% } %>
							<!--  END  -->	
					
					<!-- <div class="input-group"> -->

					<div class="row">
						<div class="col-md-3" style="left-padding: 0">
							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>


						</div>
						<div class="col-md-3" style="margin-left: -3px">



							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" " <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						</div>
					</div>
				</div>
			</div>





			<div class="col-md-3">
				<div class="form-group">
					    	     	 <!--  ILJ-46 -->
								<%
									if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
								<% } else{%>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
								<% } %>
								<!--  END  -->	
					
					<div class="input-group">
						<%
							longValue = sv.riskCessDateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="width: 80px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>

						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
							data-link-format="dd/mm/yyyy" style="width: 150px;">
							<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>


							<span class="input-group-addon"> <span
								class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>


						<%
							}
						%>

						<!-- </div> -->
					</div>
				</div>
			</div>





			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("liencd");
						optionValue = makeDropDownList(mappedItems, sv.liencd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:100px;" : "width:100px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 203px;">
						<%
							}
						%>

						<select name='liencd' type='list' style="width: 200px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.liencd).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>



				</div>
			</div>





			<%
				if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>

					<%
						if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "waitperiod" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("waitperiod");
								optionValue = makeDropDownList(mappedItems, sv.waitperiod.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.waitperiod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.waitperiod).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='waitperiod' type='list' style="width: 145px;"
							<%if ((new Byte((sv.waitperiod).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.waitperiod).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.waitperiod).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>
				</div>
			</div>
			<%
				}
			%>



		</div>




		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
					<div class="row">
						<div class="col-md-3">
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>



						</div>
						<div class="col-md-3" style="margin-left: -3px">


							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						</div>
					</div>

				</div>
			</div>






			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
					<div class="input-group">
						<%
							longValue = sv.premCessDateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:100px;" : "width:100px;"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
							data-link-format="dd/mm/yyyy" style="width: 150px;">
							<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
							<span class="input-group-addon"> <span
								class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>

						<%
							}
						%>
					</div>
				</div>
			</div>




			<%
				if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>

					<%
						if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "prmbasis" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("prmbasis");
								optionValue = makeDropDownList(mappedItems, sv.prmbasis.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.prmbasis).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 1203px;">
						<%
							}
						%>

						<select name='prmbasis' type='list' style="width: 200px;"
							<%if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.prmbasis).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.prmbasis).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>

					<%
						if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "bentrm" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("bentrm");
								optionValue = makeDropDownList(mappedItems, sv.bentrm.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.bentrm).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='bentrm' type='list' style="width: 145px;"
							<%if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else
							if ((new Byte((sv.bentrm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.bentrm).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>
				</div>
			</div>
			<%
				}
			%>


		</div>




		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age / Term")%></label>


					<div class="row">
						<div class="col-md-3">

							<%
								qpsf = fw.getFieldXMLDef((sv.benCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessAge' type='text'
								<%if ((sv.benCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>' <%}%>
								size='<%=sv.benCessAge.getLength()%>'
								maxLength='<%=sv.benCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessAge).getColor() == null ? "input_cell"
						: (sv.benCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>

						</div>

						<div class="col-md-3" style="margin-left: -3px">


							<%
								qpsf = fw.getFieldXMLDef((sv.benCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessTerm' type='text'
								<%if ((sv.benCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 50px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>' <%}%>
								size='<%=sv.benCessTerm.getLength()%>'
								maxLength='<%=sv.benCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessTerm).getColor() == null ? "input_cell"
						: (sv.benCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>

						</div>
					</div>

				</div>
			</div>



			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess date")%></label>
					<div class="input-group">
						<%
							longValue = sv.benCessDateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.benCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:100px;" : "width:100px;"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>

						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="benCessDateDisp"
							data-link-format="dd/mm/yyyy" style="width: 150px;">
							<%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp, (sv.benCessDateDisp.getLength()))%>
							<span class="input-group-addon"> <span
								class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>

						<%
							}
						%>


					</div>
				</div>
			</div>




			<%
				if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
					<%
						if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dialdownoption" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dialdownoption");
								optionValue = makeDropDownList(mappedItems, sv.dialdownoption.getFormData(), 2,
										resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.dialdownoption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.dialdownoption).getColor())) {
					%>
					<div
						style="border: 1px; border-color: #ec7572; width: 203px;">
						<%
							}
						%>

						<select name='dialdownoption' type='list' style="width: 155px;"
							<%if ((new Byte((sv.dialdownoption).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.dialdownoption).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.dialdownoption).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>





			<%
				if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
					<%
						if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "poltyp" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("poltyp");
								optionValue = makeDropDownList(mappedItems, sv.poltyp.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;"
								: "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.poltyp).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='poltyp' type='list' style="width: 145px;"
							<%if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else
							if ((new Byte((sv.poltyp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.poltyp).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
							}
					%>
					<%
						longValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>






		</div>
		<br>










		<hr>
		<br>

		<%
			if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="row">
			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>

					<%
						if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
										.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
					%>
					<%
						if (sv.adjustageamt.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>

				</div>
			</div>
			<%
				}
			%>

			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>

					<%
						if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
					%>
					<%
						if (sv.rateadj.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>

					<%
						if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
					%>
					<%
						if (sv.fltmort.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>

			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>

					<%
						if (((BaseScreenData) sv.loadper) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
					%>
					<%
						if (sv.loadper.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>



		</div>
		<%
			}
		%>



		<%
			if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>


		<div class="row">



			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
					<%
						if (((BaseScreenData) sv.premadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.premadj, (sv.premadj.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
					%>
					<%
						if (sv.premadj.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>



					<%
						longValue = null;
								formatValue = null;
					%>


				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>



					<%
						qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zlinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

								if (!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell"
						style="width: 145px !important; text-align: right;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 145px !important;"></div>

					<%
						}
					%>



				</div>
			</div>
			<%
				}
			%>

			<%
				if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<%
				if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

					<%
						if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem, (sv.zbinstprem.getLength() + 1), null)
											.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
					%>
					<%
						if (sv.zbinstprem.equals(0)) {
					%>
					<%=smartHF
										.getHTMLVar(0, 0, fw, sv.zbinstprem,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'blank_cell\' ",
												"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
										.getHTMLVar(0, 0, fw, sv.zbinstprem,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'output_cell \' ",
												"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>

				</div>
			</div>
			<%
				}
					}
			%>






			<%
				if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>

					<%
						if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
					%>
					<%
						if (sv.zstpduty01.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.zstpduty01,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.zstpduty01,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>




		</div>
		<%
			}
		%>

<!-- ILIFE-8323 starts -->
	<%
			if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0) {
		%>


		<div class="row">



		<%
				if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>



					<%
						qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.zlinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

								if (!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell"
						style="width: 145px !important; text-align: right;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 145px !important;"></div>

					<%
						}
					%>



				</div>
			</div>
			<%
				}
			%>
			</div>
			<%
				}
			%>
<!-- ILIFE-8323 ends -->



		<div class="row">



			<%
				if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>



					<%
						qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='instPrem' type='text'
						<%if ((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.instPrem.getLength(), sv.instPrem.getScale(), 3)%>'
						maxLength='<%=sv.instPrem.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(instPrem)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.instPrem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.instPrem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.instPrem).getColor() == null ? "input_cell"
							: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>




				</div>
			</div>
			<%
				}
			%>





			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
						formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='taxamt' type='text'
						<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3) - 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(taxamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
						: (sv.taxamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
					<%
						longValue = null;
						formatValue = null;
					%>



				</div>
			</div>




			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.linstamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
					%>

					<input name='linstamt' type='text'
						<%if ((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px !important;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.linstamt.getLength(), sv.linstamt.getScale(), 3)%>'
						maxLength='<%=sv.linstamt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(linstamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.linstamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.linstamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.linstamt).getColor() == null ? "input_cell"
						: (sv.linstamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>







		</div>





	</div>
</div>


<BODY>
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='comind' id='comind' type='hidden'
									value="<%=sv.comind.getFormData()%>"> <%
 	if (sv.comind.getFormData().equals("+")) {
 %> <img
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
									border="0" align="right" style="margin-right: -3px;"> <%
 	} else {

 		if (sv.comind.getFormData().equals("X")) {
 %>
									<div></div> <%
 	} else {
 %>
									<div style="width: 15px"></div> <%
 	}

 	}

 	if (sv.comind.getFormData().equals("X")) {
 %> <img
									onclick="removeXfield(parent.frames['mainForm'].document.getElementById('comind'))"
									style="cursor: pointer; margin-right: -3px;"
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
									border="0" align="right"> <%
 	}
 %></li>







								<li>
									<%
										if (sv.comind.getInvisible() != BaseScreenData.INVISIBLE
												|| sv.comind.getEnabled() != BaseScreenData.DISABLED) {
									%>



									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%></a>




									</div> <%
 	}
 %>




								</li>
								<li><input name='optextind' id='optextind' type='hidden'
									value="<%=sv.optextind.getFormData()%>"> <%
 	if (sv.optextind.getFormData().equals("+")) {
 %> <img
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
									border="0" align="right" style="margin-right: -3px;"> <%
 	} else {

 		if (sv.optextind.getFormData().equals("X")) {
 %>
									<div></div> <%
 	} else {
 %>


									<div style="width: 15px"></div> <%
 	}

 	}

 	if (sv.optextind.getFormData().equals("X")) {
 %> <img
									onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optextind'))"
									style="cursor: pointer; margin-right: -3px;"
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
									border="0" align="right"> <%
 	}
 %></li>
								<li>
									<%
										if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
												|| sv.optextind.getEnabled() != BaseScreenData.DISABLED) {
									%>


									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></a>
									</div> <%
 	}
 %>
								</li>


								<li><input name='taxind' id='taxind' type='hidden'
									value="<%=sv.taxind.getFormData()%>"> <%
 	if (sv.taxind.getFormData().equals("+")) {
 %> <img
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
									border="0" align="right" style="margin-right: -3px;"> <%
 	} else {

 		if (sv.taxind.getFormData().equals("X")) {
 %>


									<div></div> <%
 	} else {
 %>
									<div style="width: 15px"></div> <%
 	}
 	}

 	if (sv.taxind.getFormData().equals("X")) {
 %> <img
									onclick="removeXfield(parent.frames['mainForm'].document.getElementById('taxind'))"
									style="cursor: pointer; margin-right: -3px;"
									src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
									border="0" align="right"> <%
 	}
 %></li>




								<li>
									<%
										if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE

										|| sv.taxind.getEnabled() != BaseScreenData.DISABLED) {
									%>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%></a>
									</div> <%
 	}
 %>
								</li>














								<li>
									<%
										if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> <input name='exclind' id='exclind' type='hidden'
									value="<%=sv.exclind.getFormData()%>" /> <!-- text --> <%
 	if (sv.exclind.getInvisible() == BaseScreenData.INVISIBLE

 		|| sv.exclind.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.exclind.toString())%>



										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.exclind.toString())%>






											<%
												}
											%> <!-- icon --> <%
 	if (sv.exclind.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}

 		if (sv.exclind.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>








									</a> <%
 	}
 %>
								</li>

																<li><input name='fuind' id='fuind' type='hidden'
                                                                           value="<%=sv.fuind.getFormData()%>"> <%
                                                                    if (sv.fuind.getFormData().equals("+")) {
                                                                %> <img
                                                                        src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
                                                                        border="0" align="right" style="margin-right: 14px;margin-top: -29px;width: 13px;"> <%
                                                                } else {

                                                                    if (sv.fuind.getFormData().equals("X")) {
                                                                %>


									<div></div> <%
                                                                    } else {
                                                                    %>
									<div style="width: 15px"></div> <%
                                                                            }
                                                                        }

                                                                        if (sv.fuind.getFormData().equals("X")) {
                                                                    %> <img
                                                                            onclick="removeXfield(parent.frames['mainForm'].document.getElementById('fuind'))"
                                                                            style="cursor: pointer; margin-right: 14px;margin-top: -29px;width: 13px;"
                                                                            src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
                                                                            border="0" align="right"> <%
                                                                        }
                                                                    %></li>




								<li>
									<%
                                        if (sv.fuind.getInvisible() != BaseScreenData.INVISIBLE

                                                || sv.fuind.getEnabled() != BaseScreenData.DISABLED) {
                                    %>
									<div style="height: 15 px">
										<a href="javascript:;"
                                           onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fuind"))'
                                           class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%></a>
									</div> <%
                                    }
                                %>
								</li>







							</ul>
					</span></li>
				</ul>



			</div>




		</div>



	</div>
</BODY>

<%@ include file="/POLACommon2NEW.jsp"%>






