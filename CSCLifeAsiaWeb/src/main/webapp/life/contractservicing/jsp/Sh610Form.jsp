
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH610";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sh610ScreenVars sv = (Sh610ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind05.isOn()) {
	sv.payrnum.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind05.isOn()) {
	sv.payorname.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>
<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		    	 <table><tr><td>
						    		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' id="ctypdesc" style="max-width:150px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>

				      			   
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4" 4>
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
							<table><tr><td>
							<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:150px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>

						
				   </div>		</div>
			    	<div class="col-md-4" >
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
							<table><tr><td>
									<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:150px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>	
</td></tr></table>
					</div>
				   </div>	
		    </div>
				   
				   <div class="row">	
			    	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Payor")%></label>
					    		    <table><tr><td>
						    		<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;min-width:71px"">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.payorname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:150px;min-width:71px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
				      			    
				    		</div>
					</div>
					
					<div class="col-md-4" >
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
							 <table><tr><td>
							<%if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:100px;min-width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.oragntnam).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.oragntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.oragntnam.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.oragntnam.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:150px;min-width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table>
						
				   </div>		</div>
			
			    	
			    	<div class="col-md-4">
						<div class="form-group">	
						    <%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label> --%>
						    <!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Commence"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
						     <div class="input-group"  style="max-width:300px;">
							<%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%>
										
						</div></div>
				   </div>	
		    </div>
			
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					    		     <div class="input-group">
						    		<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
								 <div class="input-group"  style="max-width:100px;">
							<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						</div></div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
						    	 <div class="input-group"  style="max-width:100px;">
							<%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.mop.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mop.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
										
						</div></div>
				   </div>	
		    </div>
			
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dividend Option")%></label>
					    		     <div class="input-group">
						    		<% if ((new Byte((sv.zdivopt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"zdivopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("zdivopt");
	optionValue = makeDropDownList( mappedItems , sv.zdivopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.zdivopt.getFormData()).toString().trim());
%>
	<%=smartHF.getDropDownExt(sv.zdivopt, fw, longValue, "zdivopt", optionValue) %>
	<%}%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>
							  <div class="input-group"><% if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("reasoncd");
	optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
%>
	<%=smartHF.getDropDownExt(sv.reasoncd, fw, longValue, "reasoncd", optionValue) %>
<%}%>
						</div>
				   </div>		</div>
			
			    	
		    </div>
			
				
				   	
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->



<script>
$(document).ready(function() {
	if (screen.height == 900) {
		
		$('#ctypdesc').css('max-width','215px')
	} 
if (screen.height == 768) {
		
		$('#ctypdesc').css('max-width','190px')
	} 
	
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
