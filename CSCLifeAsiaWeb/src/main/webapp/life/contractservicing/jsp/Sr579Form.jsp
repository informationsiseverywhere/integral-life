<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR579";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr579ScreenVars sv = (Sr579ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind29.isOn()) {
	sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind29.isOn()) {
	sv.effdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind01.isOn()) {
	sv.sumin.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind02.isOn()) {
	sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind13.isOn()) {
	sv.matage.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind15.isOn()) {
	sv.mattrm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind18.isOn()) {
	sv.mattcessDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind06.isOn()) {
	sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind08.isOn()) {
	sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind11.isOn()) {
	sv.premcessDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind23.isOn()) {
	sv.liencd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind43.isOn()) {
	sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind25.isOn()) {
	sv.singlePremium.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind36.isOn()) {
	sv.mortcls.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind35.isOn()) {
	sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind46.isOn()) {
	sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind46.isOn()) {
	sv.taxind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind46.isOn()) {
	sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind44.isOn()) {
	sv.comind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind44.isOn()) {
	sv.comind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind31.isOn()) {
	sv.optextind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind31.isOn()) {
	sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>







<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>
    	 					
    	 					
                     <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>

<div class="col-md-1"> </div>

<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
    	 					
    	 					

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>


<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
    	 					
    	 					
<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>


</div>




<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>




<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
    	 					
    	 					<div class="input-group">
    	 					<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:400px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
</div></div></div>

<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
    	 					
    	 					

<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.anbAtCcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.anbAtCcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					
    	 					
    	 					
    	 					
    	 					</div></div>
</div>




<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat fund")%></label>
<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statFund.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statFund.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statFund.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>



<div class="col-md-3"> 
			    	     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>



<div class="col-md-3"> 
			    	     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Sub-Section")%></label>
<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:60px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>


</div>



<div class="row">	
			    	<div class="col-md-7"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
    	 					
    	 					<div class="input-group">
<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %> 'style="max-width:400px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div></div>



<div class="col-md-2"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	 					
    	 					<div class="input-group">
    	 					
    	 <%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>

<!-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy"> -->

<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute","relative")%>
<!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->

</div></div></div><!-- </div> -->


<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total in Suspense")%></label>
    	 					
    	 					<%if ((new Byte((sv.susamt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.susamt.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.susamt.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
    	 					</div></div>

    	 					
    	 					
    	 					
</div>




<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total policies in plan")%></label>

<%if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:80px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>



<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy number")%></label>

<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:80px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>




<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum assured")%></label>


<%if(((BaseScreenData)sv.sumin) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.sumin,( COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.sumin) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

</div></div>



</div>






<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:80px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>

</div>




<div class="row">
	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity age/term")%></label>
    	 					
    	 					
    	 	<div class="row">
	<div class="col-md-6"> 				
    	 					<%if(((BaseScreenData)sv.matage) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.matage,( sv.matage.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.matage) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.matage, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%></div>
<div class="col-md-6"> 
<%if(((BaseScreenData)sv.mattrm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.mattrm,( sv.mattrm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.mattrm) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.mattrm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%></div></div>
    	 					
    	 				</div></div>



 

<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity age/term")%></label>
<div class="input-group">

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="mattcessDisp" data-link-format="dd/mm/yyyy">
<%=smartHF.getRichText(0, 0, fw, sv.mattcessDisp,(sv.mattcessDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>

<%-- <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.mattcessDisp).replace("absolute","relative")%> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div></div></div></div>


</div>



<div class="row">
	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess age/term")%></label>
    	 					 
    	 					
    	 					<div class="row">
	<div class="col-md-6"> 	
    	 					<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%></div>
<div class="col-md-6"> 
<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%></div></div>
    	 					
    	 					
    	 					 </div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 			 		
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium date")%></label>
    	 					<div class="input-group">
    	 					

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="premcessDisp" data-link-format="dd/mm/yyyy">
<%=smartHF.getRichText(0, 0, fw, sv.premcessDisp,(sv.premcessDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%-- <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.premcessDisp).replace("absolute","relative")%> --%>
    	 					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    	 					
    	 					</div></div></div></div>

    	 					
    	 					</div>




<div class="row">
	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%></label>
    	 					
    	 					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.liencd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='liencd' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.liencd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.liencd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.liencd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
    	 					
   
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
    	 					
    	 					<%if(((BaseScreenData)sv.singlePremium) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.singlePremium,( COBOLHTMLFormatter.getLengthWithCommas( sv.singlePremium.getLength(), sv.singlePremium.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.singlePremium) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.singlePremium, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

</div></div>		
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
    	 					
    	 					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.mortcls).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='mortcls' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.mortcls).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.mortcls).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.mortcls).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	
	</div></div>
    	 					
    	 					</div>



<div class="row">
	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
    	 					
    	 					<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.zlinstprem.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zlinstprem.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>




<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Ttl Prem w/Tax")%></label>
    	 					<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.taxamt.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.taxamt.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div></div>



<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%></label>
  	 					<div class="form-group" style="width:50px">
<%if(((BaseScreenData)sv.taxind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind,( sv.taxind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

</div></div></div>




</div>





<div class="row">
	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%></label>
      	 					<div class="form-group" style="width:50px">	 					
    	 					<%if(((BaseScreenData)sv.comind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.comind,( sv.comind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.comind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.comind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

</div></div></div>



	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></label>
    	 					<div class="form-group" style="width:50px">
<%if(((BaseScreenData)sv.optextind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.optextind,( sv.optextind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.optextind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.optextind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div></div>

</div>
    	 					
    	 					
    	 					






</div></div>



























































































































































<%--  <!--  ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts-->
<style>
@media \0screen\,screen\9
{
img{margin-top:1px}
}
</style>
<!--  ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends-->

<div class='outerDiv' style='width:750;height:500;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData CHDRNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no");%>
<%=smartHF.getLit(0, 0, CHDRNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract no")%>
</div>
<br/>
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData LIFE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no");%>
<%=smartHF.getLit(0, 0, LIFE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life no")%>
</div>
<br/>
<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData COVERAGE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no");%>
<%=smartHF.getLit(0, 0, COVERAGE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%>
</div>
<br/>
<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData RIDER_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no");%>
<%=smartHF.getLit(0, 0, RIDER_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rider no")%>
</div>
<br/>
<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData LIFENUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured");%>
<%=smartHF.getLit(0, 0, LIFENUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life assured")%>
</div>
<br/>
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
	<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 starts -->
 <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
<div id='zagelit' class='label_txt'class='label_txt'  
				onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>
<!-- ILIFE-2735 Life Cross Browser -Coding and UT- Sprint 4 D2: Task 7 ends -->
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<br/>
<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.anbAtCcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.anbAtCcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData STFUND_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat fund");%>
<%=smartHF.getLit(0, 0, STFUND_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Stat fund")%>
</div>
<br/>
<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statFund.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statFund.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statFund.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData STSECT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section");%>
<%=smartHF.getLit(0, 0, STSECT_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Section")%>
</div>
<br/>
<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData STSSECT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Section");%>
<%=smartHF.getLit(0, 0, STSSECT_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Sub-Section")%>
</div>
<br/>
<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData JLIFCNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life");%>
<%=smartHF.getLit(0, 0, JLIFCNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%>
</div>
<br/>
<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData EFFDATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
<%=smartHF.getLit(0, 0, EFFDATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SUSAMT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total in Suspense");%>
<%=smartHF.getLit(0, 0, SUSAMT_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Total in Suspense")%>
</div>
<br/>
<%if ((new Byte((sv.susamt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.susamt.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.susamt.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData POLINC_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total policies in plan");%>
<%=smartHF.getLit(0, 0, POLINC_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Total policies in plan")%>
</div>
<br/>
<%if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.polinc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData PLNSFX_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy number");%>
<%=smartHF.getLit(0, 0, PLNSFX_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Policy number")%>
</div>
<br/>
<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData SUMIN_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum assured");%>
<%=smartHF.getLit(0, 0, SUMIN_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Sum assured")%>
</div>
<br/>
<%if(((BaseScreenData)sv.sumin) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.sumin,( COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.sumin) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData CURRCD_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
<%=smartHF.getLit(0, 0, CURRCD_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
</div>
<br/>
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData MATAGE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity age/term");%>
<%=smartHF.getLit(0, 0, MATAGE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maturity age/term")%>
</div>
<br/>
<%if(((BaseScreenData)sv.matage) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.matage,( sv.matage.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.matage) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.matage, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
<%if(((BaseScreenData)sv.mattrm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.mattrm,( sv.mattrm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.mattrm) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.mattrm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData MATTCESS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity date");%>
<%=smartHF.getLit(0, 0, MATTCESS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maturity date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.mattcessDisp,(sv.mattcessDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.mattcessDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData PCESSAGE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess age/term");%>
<%=smartHF.getLit(0, 0, PCESSAGE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Prem cess age/term")%>
</div>
<br/>
<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData PREMCESS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium date");%>
<%=smartHF.getLit(0, 0, PREMCESS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Premium date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.premcessDisp,(sv.premcessDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.premcessDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'><br>
<!--
<%StringData LIENCD_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien Code");%>
<%=smartHF.getLit(0, 0, LIENCD_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%>
</div>
<br/>
<!-- ILIFE-856 START -- Screen Sr579 - Corrected Missing Drop down -->
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.liencd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='liencd' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.liencd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.liencd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.liencd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
</td>
<td width='251'><br>
<!--
<%StringData SINGPRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium");%>
<%=smartHF.getLit(0, 0, SINGPRM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%>
</div>
<br/>
<%if(((BaseScreenData)sv.singlePremium) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.singlePremium,( COBOLHTMLFormatter.getLengthWithCommas( sv.singlePremium.getLength(), sv.singlePremium.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.singlePremium) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.singlePremium, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'><br>
<!--
<%StringData MORTCLS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class");%>
<%=smartHF.getLit(0, 0, MORTCLS_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%>
</div>
<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.mortcls).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='mortcls' type='list' style="width:177px;"
					<% 
				if((new Byte((sv.mortcls).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.mortcls).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.mortcls).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
</td>
<!-- ILIFE-856 END -- Screen Sr579 - Corrected Missing Drop down -->
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData ZLINSTPREM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium");%>
<%=smartHF.getLit(0, 0, ZLINSTPREM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%>
</div>
<br/>
<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.zlinstprem.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.zlinstprem.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TAXAMT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ttl Prem w/Tax");%>
<%=smartHF.getLit(0, 0, TAXAMT_LBL).replace("absolute","relative").replace("size='62'","size='30'")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Ttl Prem w/Tax")%>
</div>
<br/>
<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.taxamt.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.taxamt.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TAXIND_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Detail");%>
<%=smartHF.getLit(0, 0, TAXIND_LBL).replace("absolute","relative").replace("size='77'","size='30'")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>
</div>
<br/>
<%if(((BaseScreenData)sv.taxind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.taxind,( sv.taxind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.taxind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.taxind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData COMIND_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Commission Split");%>
<%=smartHF.getLit(0, 0, COMIND_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%>
</div>
<br/>
<%if(((BaseScreenData)sv.comind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.comind,( sv.comind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.comind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.comind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData OPTEXTIND_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms");%>
<%=smartHF.getLit(0, 0, OPTEXTIND_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
</div>
<br/>
<%if(((BaseScreenData)sv.optextind) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.optextind,( sv.optextind.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.optextind) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.optextind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
<td></td>
</tr> </table>

<INPUT type="HIDDEN" name="zbinstprem" id="zbinstprem" value="<%=	(sv.zbinstprem.getFormData()).toString() %>" >

<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
