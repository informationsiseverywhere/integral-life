<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5079";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5079ScreenVars sv = (S5079ScreenVars) fw.getVariables();%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
  



<%{
		if (appVars.ind36.isOn()) {
			sv.asgnsel.setReverse(BaseScreenData.REVERSED);
			sv.asgnsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.asgnsel.setHighLight(BaseScreenData.BOLD);
			
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.commfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.commfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.commfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.commtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.commtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.commtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner    ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Payor    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency            ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commence ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of Payment  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"<-------Assignment------->");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number    ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assignee Name");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From      ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To        ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind05.isOn()) {
			generatedText5.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payrnum.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.payorname.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			generatedText12.setColor(BaseScreenData.BLUE);
		}
	}

	%>
<!-- ILIFE-2719 Life Cross Browser - Sprint 4 D2 : Task 2  -->


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		     <!-- <div class="input-group"> -->
					    		     <table><tr><td>
						    			<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="padding-left:1px;">





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="padding-left:1px;max-width:300px;">



	
  		
		<%					
		if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 180px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
						    		

				      			    <!--  </div> -->
				    		</div>
					</div>
				    			
						</div>
				 
				 
				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					    		 <div class="input-group"> 
		<%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
  
				    		</div></div>
					
					             
				    			<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					    		 <div class="input-group" style="min-width: 70px;"> 
					    		    
						    		<%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</div>
				      			     
				    		</div>
					</div>
						</div>
						
						 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					    		    <table><tr><td>
						    		
		
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="padding-left:1px;max-width:145px;">





	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
				      			     
				    		</div>
					</div>
				    			
				    			
				    			<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Payor")%></label>
					    		   <table><tr><td>
						    			<%					
		if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="padding-left:1px;">





	
  		
		<%					
		if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>

				      			     
				    		</div>
					</div>
						
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					    		    
						    		<table><tr><td>	
		<%					
		if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="padding-left:1px;">



	
  		
		<%					
		if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>

				      			    
				    		</div>
					</div></div>
					
					
					<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	
					    		   <div class="input-group" style="width: 72px;">
						    			<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 72px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
						    		

				      			  
				    		</div>
					</div>
				    			
				    			<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					    		     <div class="input-group" style="width: 72px;"> 
						    			
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div>
				      			     </div>
				    		
					</div>
						</div>
						
						
						<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
					    		        <div class="input-group" >
						    			<%	
			fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("billfreq");
			optionValue = makeDropDownList( mappedItems , sv.billfreq,2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
							
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    </div>		

				      			    
				    		
					</div></div>
				    				
				    			<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
					    	  <div class="input-group">	    
						    				
		<%
		
					fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("mop");
			optionValue = makeDropDownList( mappedItems , sv.mop,2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
							
		if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</div>
				      			 
				    		</div>
					</div>
						</div>
					
					<br><br>
					
  <div class="row">
			<div class="col-md-12">
				<div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5079' width='100%'>
							<thead>
								<tr class='info'>
			   	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Select")%></center></th>
		         								
					<th style="width:150px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
					
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
					
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
					
					
	     
		 	        
		 	        </tr>
			 </thead>
			 <%GeneralTable sfl = fw.getTable("s5079screensfl");%>
				<script language="javascript">
					        $(document).ready(function(){
					        	var rows = <%=sfl.count() + 1%>;
								var isPageDown = 1;
								var pageSize = 1;
								<%if (!appVars.ind54.isOn()) {%> /*BSIBF-119 START*/
								var headerRowCount= 1;
								<%} else {%>
								var headerRowCount= rows;
								 <%}%>					 	/*BSIBF-119 END*/
								 //IFSU-1092 Start
								var fields = new Array("asgnsel");
								//IFSU-1092 End
					        	operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"dataTables-s5079",null,headerRowCount);
					        });
					    </script>
			
			 <tbody>
			 
							<%

	S5079screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 6;
	Map<String,Map<String,String>> reasonCdMap = appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);

	while (S5079screensfl.hasMoreScreenRows(sfl)) {	
%> 
		<%
{
		if (appVars.ind36.isOn()) {
			sv.asgnsel.setReverse(BaseScreenData.REVERSED);
			sv.asgnsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.asgnsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.commfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.commfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.commfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.commtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.commtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.commtoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
        <%--   <tr class="tableRowTag" style="<%if (count>1 && sv.asgnsel.getFormData().trim().equals("")) {%>visibility: hidden; display: none<%} %>" id='<%="tablerow"+count%>' > --%>
		<td class="tableDataTag tableDataTagFixed" style="width:40px; padding:15px;" align="center">
		<div class="form-group">
				<INPUT type="checkbox" name="chk_R" id='<%="chk_R"+count %>' class="UICheck" />
				</div>
			</td>
		
		
		<td class="tableDataTag" style="min-width:120px;" align="center">														
																	
						<div class="form-group">						
				      			<div class="input-group">	
						<input name='<%="s5079screensfl" + "." +
						 "asgnsel" + "_R" + count %>'
						id='<%="s5079screensfl" + "." +
						 "asgnsel" + "_R" + count %>'
						type='text' 
						value='<%= sv.asgnsel.getFormData() %>' 
						class = " <%=(sv.asgnsel).getColor()== null  ? 
						"input_cell" :  
						(sv.asgnsel).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.asgnsel.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5079screensfl.asgnsel)' onKeyUp='return checkMaxLength(this)' 
						 style = "width:200px;"
						>		
				<!-- ILIFE-594 starts -->					
						<span class="input-group-btn">
										<button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
											onclick="doFocus(document.getElementById('<%="s5079screensfl" + "." +
											"asgnsel" + "_R" + count %>')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span> 
					<!-- ILIFE-594 ends -->
					</div>	
					</div>	 </td>
		
		
		
		
		

				    	<td class="tableDataTag" style="min-width:210px;"  align="left">
				    	<div class="form-group">	 
								 		
						<%= sv.assigneeName.getFormData()%>
										</div> 
			
									</td>
				    									
				    									
				    									<td class="tableDataTag" style="width:180px;padding-top:13px" 
					<%if((sv.reasoncd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
					<div class="form-group">	
												
										<%if((new Byte((sv.reasoncd).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
												
				      				     <%	
						mappedItems = (Map) reasonCdMap.get("reasoncd");
						optionValue = makeDropDownList1( mappedItems , sv.reasoncd,2,resourceBundleHandler);						
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());						
												 
					%>
					<% if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					<div class='output_cell'> 
					   <%=longValue%>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:215px;"> 
					<%
					} 
					%>
					<select name='<%="s5079screensfl.reasoncd_R"+count%>' id='<%="s5079screensfl.reasoncd_R"+count%>' type='list' style="width:210px;"
					
					class = 'input_cell'
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>
				    
											
									<%}%>
									</div>
		</td>
				   
				   
				   

				   
				   
				   
				   
				    									
		<td class="tableDataTag" style="width:120px;" align="left">	
		<div class="form-group">
		
			<div class="input-group">





							<%
								if((new Byte((sv.commfromDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.commfromDisp,(sv.commfromDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="commfromDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.commfromDisp,(sv.commfromDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
							
				   </div>
		</div> </td>
				    <!-- ILIFE-594 ends -->	
				    <td class="tableDataTag" style="width:120px;" align="left">		
				    <div class="form-group">	
				    <div class="input-group">





							<%
								if((new Byte((sv.commtoDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.commtoDisp,(sv.commtoDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="commfromDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.commtoDisp,(sv.commtoDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
							
				   </div>						
		          </div> </td>
					<!-- ILIFE-594 ends -->	
	</tr>
		
     <%
	sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
	count = count + 1;
	S5079screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
          


</tbody>
</table>
</div>

		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="btn-group" style="margin-left:0px;">
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a id="subfile_add" class="btn btn-success" href='javascript:;' onClick="JavaScript:addRow(1,1);"><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<a id="subfile_remove" class="btn btn-danger" href='javascript:;'disabled onClick="JavaScript:deleteRow(chk_R);"><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div></div>
	
	 
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->

<script>
	$(document).ready(function() {
		$('#dataTables-s5079').DataTable({
			ordering : false,
			searching : false,
			scrollY : "350px",
			scrollCollapse : true,
			scrollX : true,
			info: false,
			paging:false
		});
	});
</script>

<script>
 $(document).ready(function(){

    $("#subfile_remove").click(function () {
    	
		$('input:checkbox[type=checkbox]').prop('checked',false);
	    	
	   	$("#subfile_remove").attr('disabled', 'disabled'); 

    });  
    
    $('input[type="checkbox"]'). click(function(){
    	if($(this). prop("checked") == true){
    		 $('#subfile_remove').removeAttr('disabled');
    	}else{
    		 $('#subfile_remove').attr("disabled","disabled");   
    	}
    
    });
    
  });	 
</script>


<%@ include file="/POLACommon2NEW.jsp"%>
