<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sd5kl";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sd5klScreenVars sv = (Sd5klScreenVars) fw.getVariables();%>

<%
	if (appVars.ind03.isOn()) {
		sv.taxFrmYear.setReverse(BaseScreenData.REVERSED);
		sv.taxFrmYear.setColor(BaseScreenData.RED);
	}
	if (appVars.ind04.isOn()) {
		sv.taxToYear.setReverse(BaseScreenData.REVERSED);
		sv.taxToYear.setColor(BaseScreenData.RED);
	}
%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
		        		<table>
			        		<tr>
				        		<td>
					        		<%					
										if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}							
										}
										else  {		
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
										}
									%>
									<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
								<td>
					        		<%					
										if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}							
										}
										else  {		
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
										}
									%>
									<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
							</tr>
						</table>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
		        		<table>
			        		<tr>
				        		<td>
					        		<%					
										if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}							
										}
										else  {		
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
										}
									%>
									<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
								<td>
					        		<%					
										if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}							
										}
										else  {		
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
										}
									%>
									<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
							</tr>
						</table>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
	        		<%					
						if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
		</div>
		<div class="row">
			<div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
	        		<%					
						if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("bcompany");
						longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
					%>
					<div class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%if(longValue != null){%>
	   					<%=longValue%>
	   					<%}%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
	        		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("bbranch");
						longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
					%>
					<div class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%if(longValue != null){%>
	   					<%=longValue%>
	   					<%}%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
		</div>
		<div class="row" style="margin-top: 30px;">
			<div class="col-md-4">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Period")%></label>
		        <table>
			        <tr>
				      	<td>
					       	<div class="form-group">
					       		<input name="taxFrmYear" type="text"
					       		<%formatValue = (sv.taxFrmYear.getFormData()).toString(); %>
					       		value="<%= XSSFilter.escapeHtml(formatValue)%>" <%if(formatValue!=null && formatValue.trim().length()>0) {%> title="<%=formatValue%>" <%}%>
					       		size="<%= sv.taxFrmYear.getLength()%>" maxLength="<%= sv.taxFrmYear.getLength()%>" onFocus="doFocus(this)" onHelp="return fieldHelp(taxFrmYear)" onKeyUp="return checkMaxLength(this)"
					       		<% if((new Byte((sv.taxFrmYear).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>
					       				readonly class="output_cell"
					       		<% } else if((new Byte((sv.taxFrmYear).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>	
					       				class="bold_cell" 
					       		<% }else { %> 
					       				class = " <%=(sv.taxFrmYear).getColor()== null  ? "input_cell" :  (sv.taxFrmYear).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>"
					      		<%}	%> />
					    	</div>
						</td>
						<td>
							<label><b><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;/&nbsp;&nbsp;")%></b></label>
						</td>
						<td>
							<div class="form-group">
					      		<input name="taxToYear" type="text"
					       		<%formatValue = (sv.taxToYear.getFormData()).toString(); %>
					       		value="<%= XSSFilter.escapeHtml(formatValue)%>" <%if(formatValue!=null && formatValue.trim().length()>0) {%> title="<%=formatValue%>" <%}%>
					       		size="<%= sv.taxToYear.getLength()%>" maxLength="<%= sv.taxToYear.getLength()%>" onFocus="doFocus(this)" onHelp="return fieldHelp(taxToYear)" onKeyUp="return checkMaxLength(this)"
					       		<% if((new Byte((sv.taxToYear).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>
					       				readonly class="output_cell"
					       		<% } else if((new Byte((sv.taxToYear).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>	
					       				class="bold_cell" 
					   			<% }else { %> 
									class = " <%=(sv.taxToYear).getColor()== null  ? "input_cell" :  (sv.taxToYear).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>"
					   			<%}	%> />
				       		</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Extract All Contracts From")%></label>
	        		<table>
		        		<tr>
			        		<td>
			        			<input name="chdrnum" type="text"
			        			<%formatValue = (sv.chdrnum.getFormData()).toString(); %>
			        			value="<%= XSSFilter.escapeHtml(formatValue)%>" <%if(formatValue!=null && formatValue.trim().length()>0) {%> title="<%=formatValue%>" <%}%>
			        			size="<%= sv.chdrnum.getLength()%>" maxLength="<%= sv.chdrnum.getLength()%>" onFocus="doFocus(this)" onHelp="return fieldHelp(chdrnum)" onKeyUp="return checkMaxLength(this)"
			        			<% if((new Byte((sv.chdrnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>
			        					readonly class="output_cell"
			        			<% } else if((new Byte((sv.chdrnum).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>	
			        					class="bold_cell" 
			        			<% }else { %> 
			        					class = " <%=(sv.chdrnum).getColor()== null  ? "input_cell" :  (sv.chdrnum).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>"
			        			<%}	%> />
							</td>
							<td>
								<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;to&nbsp;&nbsp;")%></label>
							</td>
							<td>
			        			<input name="chdrnum1" type="text"
			        			<%formatValue = (sv.chdrnum1.getFormData()).toString(); %>
			        			value="<%= XSSFilter.escapeHtml(formatValue)%>" <%if(formatValue!=null && formatValue.trim().length()>0) {%> title="<%=formatValue%>" <%}%>
			        			size="<%= sv.chdrnum1.getLength()%>" maxLength="<%= sv.chdrnum1.getLength()%>" onFocus="doFocus(this)" onHelp="return fieldHelp(chdrnum)" onKeyUp="return checkMaxLength(this)"
			        			<% if((new Byte((sv.chdrnum1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>
			        					readonly class="output_cell"
			        			<% } else if((new Byte((sv.chdrnum1).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>	
			        					class="bold_cell" 
			        			<% }else { %> 
			        					class = " <%=(sv.chdrnum1).getColor()== null  ? "input_cell" :  (sv.chdrnum1).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>"
			        			<%}	%> />
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>