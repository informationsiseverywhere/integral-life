<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5117";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S5117ScreenVars sv = (S5117ScreenVars) fw.getVariables();%>

<%if (sv.S5117screenWritten.gt(0)) {%>
	<%S5117screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("A - Billing Reversal");%>
	<%StringData generatedText3 = new StringData("B - Full Contract Reversal");%>
	<%StringData generatedText4 = new StringData("C - Lapse Reversal");%>
	<%StringData generatedText5 = new StringData("D - Paid-up Reversal");%>
	<%StringData generatedText6 = new StringData("E - Death Claims Reversal");%>
	<%StringData generatedText7 = new StringData("F - Full Surrender Reversal");%>
	<%StringData generatedText8 = new StringData("G - Maturity Reversal");%>
	<%StringData generatedText9 = new StringData("H - Expiry Reversal");%>
	<%StringData generatedText10 = new StringData("I - Manual Non-Forfeiture Surrender Reversal");%>
	<%StringData generatedText11 = new StringData("J - Regular Payment Reversal");%>
	<%StringData generatedText12 = new StringData("K - Vesting Registration Reversal");%>
	<%StringData generatedText13 = new StringData("L - First Death Registration Reversal");%>
	<%StringData generatedText14 = new StringData("M - Cash Call Reversal");%>
	<%StringData generatedText15 = new StringData("N - AutoLapse Reversal");%>
	<%StringData generatedText16 = new StringData("O - Reversal of FAILED Part Surrender");%>
	<%StringData generatedText17 = new StringData("P - Reversal of FAILED Fund Switch");%>
	<%StringData generatedText20 = new StringData("Q - Pre-Registration Reversal");%>
	<%StringData generatedText18 = new StringData("Contract Number ");%>
	<%sv.chdrsel.setClassString("");%>
	<%StringData generatedText19 = new StringData("Action ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					    		
				    		</div>
			    	</div>
			      
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			

	
		<div class="row">	
			    <div class="col-md-3"> 
			    
			<div class="radioButtonSubmenuUIG">			 
				<label for="A">
					<%= smartHF.buildRadioOption(sv.action, "action", "A")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Billing Reversal")%>
				</label>					
			</div>		 
		</div>
				<div class="col-md-4">		 
			<div class="radioButtonSubmenuUIG">		 
				<label for="B">
					<%= smartHF.buildRadioOption(sv.action, "action", "B")%>
					<%-- MIBT-95 --%>
					<%=resourceBundleHandler.gettingValueFromBundle("Full Contract Reversal")%>
				</label> 
			</div>		 
		</div>
				<div class="col-md-4">			 
			<div class="radioButtonSubmenuUIG">		 
				<label for="C">
					<%= smartHF.buildRadioOption(sv.action, "action", "C")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Lapse Reversal")%>
				</label> 
			</div>		 
		</div></div>
				<div class="row">
					<div class="col-md-3">	
				 
			<div class="radioButtonSubmenuUIG">			 
				<label for="D">
					<%= smartHF.buildRadioOption(sv.action, "action", "D")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Paid-up Reversal")%>
				</label>					
			</div>		 
		</div>
				<div class="col-md-4">		 
			<div class="radioButtonSubmenuUIG">		 
				<label for="E">
					<%= smartHF.buildRadioOption(sv.action, "action", "E")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Death Claims Reversal")%>
				</label> 
			</div>		 
		</div>
				<div class="col-md-4">		 
			<div class="radioButtonSubmenuUIG">		 
				<label for="F">
					<%= smartHF.buildRadioOption(sv.action, "action", "F")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Full Surrender Reversal")%>
				</label> 
			</div>		 
</div></div>
				<div class="row">	
				<div class="col-md-3">	
	 
			<div class="radioButtonSubmenuUIG">			 
				<label for="G">
					<%= smartHF.buildRadioOption(sv.action, "action", "G")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Maturity Reversal")%>
				</label>					
			</div>		 
	</div>
				<div class="col-md-4">				 
			<div class="radioButtonSubmenuUIG">		 
				<label for="H">
					<%= smartHF.buildRadioOption(sv.action, "action", "H")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Expiry Reversal")%>
				</label> 
			</div>		 
		</div>
				<div class="col-md-5" >				 
			<div class="radioButtonSubmenuUIG">		 
				<label for="I">
					<%= smartHF.buildRadioOption(sv.action, "action", "I")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Manual Non-Forfeiture Surrender Reversal")%>
				</label> 
			</div>		</div></div>
			
			
				<div class="row">
				<div class="col-md-3">	
			 
			 
			<div class="radioButtonSubmenuUIG">			 
				<label for="J">
					<%= smartHF.buildRadioOption(sv.action, "action", "J")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Regular Payment Reversal")%>
				</label>					
			</div>	 
		</div>
				<div class="col-md-4" >	
				 
			<div class="radioButtonSubmenuUIG">		 
				<label for="K">
					<%-- ILIFE-616- Corrected action mapping --%>
					<%= smartHF.buildRadioOption(sv.action, "action", "K")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Vesting Registration Reversal")%>
				</label> 
			</div>		 
		</div>
				<div class="col-md-4">				 
			<div class="radioButtonSubmenuUIG">		 
				<label for="L">
					<%-- ILIFE-616- Corrected action mapping --%>
					<%= smartHF.buildRadioOption(sv.action, "action", "L")%>
					<%=resourceBundleHandler.gettingValueFromBundle("First Death Registration Reversal")%>
				</label> 
			</div>		 
		</div></div>
			<div class="row">
				 
			<div class="col-md-3">			 
			<div class="radioButtonSubmenuUIG">			 
				<label for="M">
					<%= smartHF.buildRadioOption(sv.action, "action", "M")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Cash Call Reversal")%>
				</label>					
			</div>		 
		</div>
				<div class="col-md-4">			 
			<div class="radioButtonSubmenuUIG">		 
				<label for="N">
					<%= smartHF.buildRadioOption(sv.action, "action", "N")%>
					<%-- MIBT-97 --%>
					<%=resourceBundleHandler.gettingValueFromBundle("AutoLapse Reversal")%>
				</label> 
			</div>		 
		</div>
				<div class="col-md-4">				 
			<div class="radioButtonSubmenuUIG">		 
				<label for="0">
					<%= smartHF.buildRadioOption(sv.action, "action", "O")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Reversal of FAILED Part Surrender")%>
				</label> 
			</div>		 
		</div></div>
				<div class="row">
				<div class="col-md-3">		
		
		 
			<div class="radioButtonSubmenuUIG">			 
				<label for="P">
					<%= smartHF.buildRadioOption(sv.action, "action", "P")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Reversal of FAILED Fund Switch")%>
				</label>					
			</div>	
			  </div>
			  <div class="col-md-4">			 
			  <div class="radioButtonSubmenuUIG">		 
			  <label for="Q">
					<%= smartHF.buildRadioOption(sv.action, "action", "Q")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Pre-Registration Reversal")%>
				</label> 
			</div>		 
		</div>
	</div>						 
		</td>		
		</tr>							
		</table>
										
		</div>
	</div>	


<%}%>

<%if (sv.S5117protectWritten.gt(0)) {%>
	<%S5117protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>


<%@ include file="/POLACommon2NEW.jsp"%>
