<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5224";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S5224ScreenVars sv = (S5224ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status     ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency   ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured      ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life        ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reverse Trans No  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Press ENTER to confirm ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Vesting Amount");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Vesting Lump Sum");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind04.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.trandes.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
<!-- <div class="input-group"> -->

<table><tr><td>


	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



</td><td>

	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
	<!-- </div> -->
	</div></div></div>

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">



<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>



<div class="input-group">
	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
	

</div></div>

	<div class="col-md-2"></div>
			    	<div class="col-md-4"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>


<div class="input-group">

	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
	

</div></div>


			    	<div class="col-md-4"> 
				    		<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%}%>




<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div></div>

<div class="row">
			    	<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>

	<table><tr><td>
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>



</div></div></div>
<div class="row">
			    	<div class="col-md-4"> 
				    		<div class="form-group">



<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
<table><tr><td style="width:100px;">




	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td style="width:100px;">


	
  		
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:450px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td></tr></table>
</div></div></div>


<div class="row">
			    	<div class="col-md-4"> 
				    		<div class="form-group">

<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label><%=resourceBundleHandler.gettingValueFromBundle("Reverse Trans No")%></label>

<%}%>

<div class="input-group">



<%if ((new Byte((sv.trandes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.trandes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </div>

</div></div></div>

	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5224' width='100%'>
							<thead>
								<tr class='info'>
									<th> <center>  <%=resourceBundleHandler.gettingValueFromBundle("Plan")%></center>   </th>
									<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Life")%></center>  </th>
									<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Coverage")%> </center> </th>
									<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></center> </th>
									<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Vesting Amount")%> </center></th>
                                    <th><center> <%=resourceBundleHandler.gettingValueFromBundle("Vesting Lump Scan")%></center></th>
								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s5224screensfl");
									
									S5224screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S5224screensfl.hasMoreScreenRows(sfl)) {
								%>

								<tr height="27" class="tableRowTag" id='<%="tablerow"+count%>' >
		<%-- <%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	 	<div style='display:none; visiblity:hidden;'>
		 	<input type='text' maxLength='<%=sv.select.getLength()%>' value='<%= sv.select.getFormData() %>' size='<%=sv.select.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(s0026screensfl.select)' onKeyUp='return checkMaxLength(this)' name='<%="s0026screensfl" + "." +
			"select" + "_R" + count %>' id='<%="s0026screensfl" + "." + "select" + "_R" + count %>' class = "input_cell"
			style = "width: <%=sv.select.getLength()*12%> px;" >
	 	</div>
						 						 
<%}%> --%>
		<td class="tableDataTag " 
		<%if((sv.plansfx).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >
			<%if((new Byte((sv.plansfx).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>			
				<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5224screensfl" + "." +
				"select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.plansfx.getFormData()%></span></a>					
			<%}%>
		</td>
		<td class="tableDataTag " 
		<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
			<%if((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>			
				<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5224screensfl" + "." +
				"select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.life.getFormData()%></span></a>					
			<%}%>
		</td>
		<td class="tableDataTag"  
		<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
				<%= sv.coverage.getFormData()%>
			<%}%>
		</td>
		<td class="tableDataTag " 
		<%if((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
				<%-- <input type='checkbox' name='validflagDisp' value='1' 
				
						<%
						if((sv.validflag).toString().trim().equalsIgnoreCase("1")){
							%>checked
				
				       <%}%>
						disabled="disabled" class ='UICheck'/> --%>
				<%= sv.effdateDisp.getFormData()%>
			<%}%>
		</td>
		<td class="tableDataTag" 
		<%if((sv.vstpay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.vstpay).getInvisible()).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0)){%>	
				<%= sv.vstpay.getFormData()%>
			<%}%>
		</td>		
		<td class="tableDataTag" 
		<%if((sv.vstlump).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.vstlump).getInvisible()).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0)){%>	
				<%= sv.vstlump.getFormData()%>
			<%}%>
		</td>		
	</tr>

	<%
	count = count + 1;
	S5224screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


							</tbody>

						</table>
						
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTables-s5224').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

