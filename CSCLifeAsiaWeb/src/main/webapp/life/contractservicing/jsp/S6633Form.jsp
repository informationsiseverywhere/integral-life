<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6633";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S6633ScreenVars sv = (S6633ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed or Variable ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(F/V)");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Guaranteed Fixed Period ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(months)");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Capitalisation ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Fixed Date ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(DD)");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Fixed Date ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(DD)");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Frequency ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Frequency ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Policy Anniv ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Policy Anniv ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Loan Anniv ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"- Loan Anniv ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.intRate.setReverse(BaseScreenData.REVERSED);
			sv.intRate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.intRate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.inttype.setReverse(BaseScreenData.REVERSED);
			sv.inttype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.inttype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mperiod.setReverse(BaseScreenData.REVERSED);
			sv.mperiod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mperiod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.day.setReverse(BaseScreenData.REVERSED);
			sv.day.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.day.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.compfreq.setReverse(BaseScreenData.REVERSED);
			sv.compfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.compfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.annpoly.setReverse(BaseScreenData.REVERSED);
			sv.annpoly.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.annpoly.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.annloan.setReverse(BaseScreenData.REVERSED);
			sv.annloan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.annloan.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.policyAnnivInterest.setReverse(BaseScreenData.REVERSED);
			sv.policyAnnivInterest.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.policyAnnivInterest.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.loanAnnivInterest.setReverse(BaseScreenData.REVERSED);
			sv.loanAnnivInterest.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.loanAnnivInterest.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.interestFrequency.setReverse(BaseScreenData.REVERSED);
			sv.interestFrequency.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.interestFrequency.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.interestDay.setReverse(BaseScreenData.REVERSED);
			sv.interestDay.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.interestDay.setHighLight(BaseScreenData.BOLD);
		}
		 if (appVars.ind14.isOn()) {
				sv.nofDay.setInvisibility(BaseScreenData.INVISIBLE);
				sv.nofDay.setEnabled(BaseScreenData.DISABLED);
		}
	}

	%>




<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		
					    		<div class="input-group" style="max-width:100px;">  
					    		 	
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
				    		</div>
					</div></div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							
							<div class="input-group" style="max-width:200px;">  
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
						</div>
				   </div>		</div>
			
			    	<div class="col-md-4">
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						  
							 <!-- <div class="input-group"  style="max-width:100px;"> -->
							 <table><tr><td>
								<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		</td></tr></table>
		
						<!-- </div> -->
						</div>
				   </div>	
		    </div>
				   
				 
				   <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label> 
				    		<table>
			    	   <tr>
			    	       
			    	     <td>
				 
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  
		 </td>


                      <td>  &nbsp; </td>
                        <td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
                        <td>  &nbsp; </td>


                      
                        <td> 

		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
   </td>
                      </tr>
                   </table>		
	

				      		 </div></div><div class="col-md-4"></div><div class="col-md-4"></div>
				      </div>
				      
				      <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					    		     <div class="input-group">
						    		
	<%	
			qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS5);
			
	%>

<input name='intRate' 
type='text' 
<%if((sv.intRate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intRate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
	 <%}%>

size='<%= sv.intRate.getLength()%>'
maxLength='<%= sv.intRate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intRate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intRate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intRate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intRate).getColor()== null  ? 
			"input_cell" :  (sv.intRate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						    		

				      			     </div>
				    		</div>
					</div><div class="col-md-4"></div><div class="col-md-4"></div>

<!-- ICIL-549 start -->
<%if((new Byte((sv.nofDay).getInvisible()))
				.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> 					
<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Interest Deferred Days")%></label>
					    		  <table>
					    		  <tr><td>
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.nofDay).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name=nofDay 
type='text'
<%if((sv.nofDay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.nofDay) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.nofDay);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.nofDay) %>'
	 <%}%>

size='<%= sv.nofDay.getLength()%>'
maxLength='<%= sv.nofDay.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(nofDay)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.nofDay).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.nofDay).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.nofDay).getColor()== null  ? 
			"input_cell" :  (sv.nofDay).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
<td> &nbsp;&nbsp;&nbsp;</td>
</tr></table>
					    		    
					</div>
				    		</div>
<%}%>		
<!-- ICIL-549 end -->
				    		</div>
				    		
				    		 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Fixed or Variable")%></label>
					    		     <div class="input-group">
						    		

<%	
	if((new Byte((sv.inttype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= (((sv.inttype.getFormData()).toString().trim() == null)||("".equals((sv.inttype.getFormData()).toString().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if((sv.inttype.getFormData()).toString().trim() != null){
	   			if((sv.inttype.getFormData()).toString().trim().equalsIgnoreCase("F"))
	   			{ %>
	   				<%=resourceBundleHandler.gettingValueFromBundle("Fixed")%>
	   			<%} if((sv.inttype.getFormData()).toString().trim().equalsIgnoreCase("V")){ %>
	   				<%=resourceBundleHandler.gettingValueFromBundle("Variable")%>
   				
   			<%} %>
	   		<%} %>
	   </div>

<%
longValue = null;
%>
	<% }else { %>
	

<select value='<%=sv.inttype.getFormData()%>'
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(inttype)'
	onKeyUp='return checkMaxLength(this)' name='inttype'
	onchange="changeTextBoxVal()";
	class="input_cell">
<option value="">---------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>---------</option>
<option value="F"<% if(sv.inttype.getFormData().equalsIgnoreCase("F")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Fixed")%></option>
<option value="V"<% if(sv.inttype.getFormData().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Variable")%></option>
		
					<%
						if ((new Byte((sv.inttype).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0) {
					%>
					readonly="true" class="output_cell"
					<%
						} else if ((new Byte((sv.inttype).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell"
		
					<%
						} else {
					%>
		
					class = '
					<%=(sv.inttype).getColor() == null ? "input_cell"
										: (sv.inttype).getColor().equals("red") ? "input_cell red reverse"
												: "input_cell"%>'
		
					<%
						}
					%>
				</select>
<%} %>

				      			     </div>
				    		</div>
					</div><div class="col-md-4"></div><div class="col-md-4"></div>
				    		</div>
				    		
				    		
				    		 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Guaranteed Fixed Period")%></label>
					    		  <table>
					    		  <tr><td>
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.mperiod).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mperiod' 
type='text'
<%if((sv.mperiod).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mperiod) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mperiod);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mperiod) %>'
	 <%}%>

size='<%= sv.mperiod.getLength()%>'
maxLength='<%= sv.mperiod.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mperiod)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mperiod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mperiod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mperiod).getColor()== null  ? 
			"input_cell" :  (sv.mperiod).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
<td> &nbsp;&nbsp;&nbsp;</td>
<td>
						    		

				      			 
				    		<label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("(months)")%></label>
				    		</td></tr></table>
					    		    
					</div>
				    		</div><div class="col-md-4"></div><div class="col-md-4"></div></div>
				    		
				    		
				    		 <div class="row">	
			    	<div class="col-md-4"> 
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Capitalisation")%></label>
					    		    
					</div>
					
						<div class="col-md-4">				    		    
					</div>
					
						<div class="col-md-4"> 
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Interest")%></label>
					    		    
					</div>
				    		</div>
				    		
				    		<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("- Fixed Date")%></label>
					    		    <table>
					    		    <tr><td>
						    		
<%	
			qpsf = fw.getFieldXMLDef((sv.day).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='day' 
type='text'
<%if((sv.day).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.day) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.day);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.day) %>'
	 <%}%>

size='<%= sv.day.getLength()%>'
maxLength='<%= sv.day.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(day)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.day).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.day).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.day).getColor()== null  ? 
			"input_cell" :  (sv.day).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
<td> &nbsp;&nbsp;&nbsp;</td>
<td>
		


						    <label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("(DD)")%></label> </td></tr></table>
						
				   </div>	</div>
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("- Fixed Date")%></label>
							<table>
							<tr><td>
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.interestDay).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='interestDay' 
type='text'
<%if((sv.interestDay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.interestDay) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.interestDay);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.interestDay) %>'
	 <%}%>

size='<%= sv.interestDay.getLength()%>'
maxLength='<%= sv.interestDay.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(interestDay)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.interestDay).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{  
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.interestDay).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.interestDay).getColor()== null  ? 
			"input_cell" :  (sv.interestDay).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
<td> &nbsp;&nbsp;&nbsp;</td>
<td>
	
						    		

				      			    
					    		<label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("(DD)")%></label></td></tr></table>
					    		</div></div>

		    </div>
				   
				   
				   	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("- Frequency")%></label>
					    		     <div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"compfreq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("compfreq");
	optionValue = makeDropDownList( mappedItems , sv.compfreq.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.compfreq.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.compfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.compfreq).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:252px;"> 
<%
} 
%>

<select name='compfreq' type='list' style="width:250px;"
<% 
	if((new Byte((sv.compfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.compfreq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.compfreq).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("- Frequency")%></label>
							
								<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"interestFrequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("interestFrequency");
	optionValue = makeDropDownList( mappedItems , sv.interestFrequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.interestFrequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.interestFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:170px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.interestFrequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:252px;"> 
<%
} 
%>

<select name='interestFrequency' type='list' style="width:250px;"
<% 
	if((new Byte((sv.interestFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.interestFrequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.interestFrequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>		
						</div>
				   </div>	
		    </div>
				   
				   
				   
				   	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("- Policy Anniv")%></label>
					    		
						    		

<input type='checkbox' name='annpoly' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(annpoly)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.annpoly).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.annpoly).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.annpoly).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('annpoly')"/>

<input type='checkbox' name='annpoly' value='N' 

<% if(!(sv.annpoly).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('annpoly')"/>
						    		

				      			   
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("- Policy Anniv")%></label>
							
							
<input type='checkbox' name='policyAnnivInterest' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(policyAnnivInterest)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.policyAnnivInterest).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.policyAnnivInterest).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.policyAnnivInterest).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('policyAnnivInterest')"/>

<input type='checkbox' name='policyAnnivInterest' value='N' 

<% if(!(sv.policyAnnivInterest).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('policyAnnivInterest')"/>

										
						</div>
				   </div>	
		    </div>
				   
				   
				   
				   	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("- Loan Anniv")%></label>
					    		   
						    		


<input type='checkbox' name='annloan' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(annloan)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.annloan).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.annloan).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.annloan).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('annloan')"/>

<input type='checkbox' name='annloan' value='N' 

<% if(!(sv.annloan).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('annloan')"/>






				      			    
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("- Loan Anniv")%></label>
							

<input type='checkbox' name='loanAnnivInterest' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(loanAnnivInterest)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.loanAnnivInterest).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.loanAnnivInterest).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.loanAnnivInterest).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('loanAnnivInterest')"/>

<input type='checkbox' name='loanAnnivInterest' value='N' 

<% if(!(sv.loanAnnivInterest).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('loanAnnivInterest')"/>




							
										
						</div>
				   </div>	
		    </div>
				   
				   
				
			        
				   </div></div>


<%@ include file="/POLACommon2NEW.jsp"%>

