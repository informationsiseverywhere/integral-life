
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6757";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6757ScreenVars sv = (S6757ScreenVars) fw.getVariables();%>
<%if (sv.S6757screenWritten.gt(0)) {%>
	<%S6757screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
<!-- ILIFE-2727  Life Cross Browser -Coding and UT- Sprint 4 D1: Task 5  by snayeni --> 
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Windforward Schedules ");%>
	<%sv.jobnameJob01.setClassString("");%>
	<%sv.jobnameJob02.setClassString("");%>
	<%sv.jobnameJob03.setClassString("");%>
	<%sv.jobnameJob04.setClassString("");%>
	<%sv.jobnameJob05.setClassString("");%>
	<%sv.jobnameJob06.setClassString("");%>
	<%sv.jobnameJob07.setClassString("");%>
	<%sv.jobnameJob08.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Enter, from left to right, the sequence of Schedules to be submitted to Windforward a Contract under this Transaction Code");%>
	<%-- <%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Windforward a Contract under this Transaction Code");%> --%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (!appVars.ind01.isOn()) {
			sv.jobnameJob01.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind02.isOn()) {
			sv.jobnameJob02.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind03.isOn()) {
			sv.jobnameJob03.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind04.isOn()) {
			sv.jobnameJob04.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind05.isOn()) {
			sv.jobnameJob05.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind06.isOn()) {
			sv.jobnameJob06.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind07.isOn()) {
			sv.jobnameJob07.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind08.isOn()) {
			sv.jobnameJob08.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	
<style type="text/css">
.fix-row > .label_txt{
	position: relative !important;
	top: 0 !important;
    left: 0 !important;
}
</style>

<div class="panel panel-default">    	
    	<div class="panel-body">  
		
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		
					    		<div class="input-group" style="max-width:100px;">  
					    		 <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
				    		</div>
					</div></div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							
							<div class="input-group" >  
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
						</div>
				   </div>		</div>
			
			    	<div class="col-md-4">
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						  
							
							 <table><tr><td>
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td>

<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
		<!-- 	</div>	 -->
		</td></tr></table>
		
		
						</div>
						</div>
				  </div>	
		     <!-- </div> -->
				   
				 
				   <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label> 
				    		<table>
			    	   <tr>
			    	       
			    	     <td>
				   	<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		 </td>


                      <td>  &nbsp; </td>
                        <td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
                        <td>  &nbsp; </td>


                      
                        <td> 

<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </td>
                      </tr>
                   </table>		
	

				      		 </div></div><div class="col-md-4"> </div><div class="col-md-4"> </div>
				      </div>
			        
				   
				   
				   
	<div class="row">
	
	
	<div class="col-md-4">
<label><%= generatedText10%></label>

</div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>

<div class="row">
	
	<div class="col-md-2">
<div class="list-group" >			   
				   
	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob01)%>
	
	</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >	
		<%=smartHF.getHTMLVarExt(fw, sv.jobnameJob02)%>
	</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >

	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob03)%>
	</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >

	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob04)%>
	</div></div></div>
	
	<div class="row">
	<div class="col-md-2">
	<div class="list-group" >

	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob05)%>
</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >

	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob06)%>
</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >
	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob07)%>
</div></div>
	
	<div class="col-md-2">
	<div class="list-group" >	
	<%=smartHF.getHTMLVarExt( fw, sv.jobnameJob08)%>
</div></div></div>

<div class="row">
	<div class="col-md-12 fix-row">
	<%=smartHF.getLit(15, 3, generatedText11)%>
	</div>
</div>
	
	

</div>
</div>

<%}%>

<%if (sv.S6757protectWritten.gt(0)) {%>
	<%S6757protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>




<script>
$(document).ready(function() {
	
	$('#jobnameJob01').width('103px');
	$('#jobnameJob02').width('103px');
	$('#jobnameJob03').width('103px');
	$('#jobnameJob04').width('103px');
	$('#jobnameJob05').width('103px');
	$('#jobnameJob06').width('103px');
	$('#jobnameJob07').width('103px');
	$('#jobnameJob08').width('103px');

	
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
