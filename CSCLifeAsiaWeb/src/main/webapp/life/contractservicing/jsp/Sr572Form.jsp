

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR572";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr572ScreenVars sv = (Sr572ScreenVars) fw.getVariables();%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		//TSD-266 start
		if (appVars.ind13.isOn()) {
			sv.lifcnum.setReverse(BaseScreenData.REVERSED);
			sv.lifcnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.lifcnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.jlifcnum.setReverse(BaseScreenData.REVERSED);
			sv.jlifcnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.jlifcnum.setHighLight(BaseScreenData.BOLD);
		}
		//TSD-266 end
	}

	%>



	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life   ");%>
	<!-- ILIFE-1403 START by nnazeer -->
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan  ");%>
	<!-- ILIFE-1403 END -->
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"U/W Date     ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select?");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandatory");%>
<%		appVars.rollup(new int[] {93});
%>

<% if(1 == sv.uwFlag){ %>
<script type="text/javascript">
	var uwResultMap = new Map();
<%
	Map<String, Integer> map = sv.uwResultMap;
	if (map != null && !map.isEmpty()) {
		for (String comType : map.keySet()) {
%>
	uwResultMap.set('<%=comType%>','<%=map.get(comType)%>');
	
<% 		} 
	} %>
</script>

<% } %>



<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
    	 					<table>
    	 					<tr>
    	 					<td>
    	 					
						<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
						</td>
						<td>




			
				
			  		
					<%					
					if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  				
  				</td>
  				<td>
	
  		
				<%					
				if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="max-width:300px;margin-left: 1px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		
		</td>
		</tr>
		</table>
		
		</div></div>



<div class="col-md-4"></div>




 	              <div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					    		

		<%			
				fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("register");
			longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>



</div></div>


</div>

	<div class="row">	
			    	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					    		<%
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}


					} else  {

					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

					}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
		</div></div>
				
		<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					    		<div class="input-group">
					 
	<%
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}


					} else  {

					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

					}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
						"blank_cell" : "output_cell" %>' style="min-width: 100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
					    		</div>
					    		
					    		
					    		</div></div>
		
<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
					    		
					    		
					    		
					    		
					    		</div></div>
					    		
					    		
					    		
					 </div>   	
					 
					 	<div class="row">	
			    	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>	
<table>
<tr>
<td>
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  <td>	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:80px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 145px;;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

</td>
</tr>
</table>

</div></div>

<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>	
<table>
<tr>
<td>

		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td>
<td>	
  		
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:71px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>


</td>
</tr>
</table>
</div></div>

			    	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>	
<table>
<tr>
<td>
  		
		<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>	
  		
		<%					
		if(!((sv.entity.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.entity.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.entity.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:71px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>	
  		</td>
  		<td>
		<%					
		if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

</td>
</tr>
</table>

</div></div>
	
	</div>
	<div class="row">
	
	
	<div class="col-md-4">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("U/W Date")%></label>	
<div class="input-group">


<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="fupremdtDisp" data-link-format="dd/mm/yyyy">
<input name='huwdcdteDisp' 
type='text' 
value='<%=sv.huwdcdteDisp.getFormData()%>' 
maxLength='<%=sv.huwdcdteDisp.getLength()%>' 
size='<%=sv.huwdcdteDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(huwdcdteDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.huwdcdteDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.huwdcdteDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('huwdcdteDisp'),  '<%= av.getAppConfig ().getDateFormat () %>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.huwdcdteDisp).getColor()== null  ? 
"input_cell" :  (sv.huwdcdteDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('huwdcdteDisp'), '<%= av.getAppConfig ().getDateFormat () %>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%} %>




</div></div></div>
</div>

	
	</div>
	
	<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-sr572' width="100%">
               
               <thead>
		
			        <tr class='info'>
			      
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
					
		 	        </tr>
			 </thead>
			  <tbody>
			<% GeneralTable sfl = fw.getTable("sr572screensfl");  %>
			 
			 	<%
	Sr572screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr572screensfl
	.hasMoreScreenRows(sfl)) {	
%>
			
			






  
	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
								
									
											
						<%= sv.indic.getFormData()%>
						<%

						
						%>
						
														 
				
									</td>
									
									
									
									
		    								<td class="tableDataTag tableDataTagFixed" style="width:80px;" >
					<%if(("+".equalsIgnoreCase(sv.select.getFormData()))||("X".equalsIgnoreCase(sv.select.getFormData())))
							{ %>									
								
													
													
					
					 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr572screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr572screensfl.select_R<%=count%>'
						 id='sr572screensfl.select_R<%=count%>'
						 <%-- MIBT-94 --%>
						 onClick="selectedRowIdValue('sr572screensfl.select_R<%=count%>','<%= sv.select.getFormData()%>')" 
						 class="UICheck" disabled
					 />
					 
					 					
					<%}else{ %>
											 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr572screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr572screensfl.select_R<%=count%>'
						 id='sr572screensfl.select_R<%=count%>'
						<%--  onClick="selectedRow('sr572screensfl.select_R<%=count%>')" --%>
						<%-- MIBT-94 --%>
						<% if(1 == sv.uwFlag){ %>
						onClick="checkComUWResult('<%=count%>')"
						<% } else { %>
						 onClick="selectedRowChecIdValue('sr572screensfl.select_R<%=count%>', 'sr572screensfl.select_R<%=count%>', '<%=count%>','X')"
						 <% } %>
						 class="UICheck" 
					 />
					 	<%} %>				
									</td>
		
					
					<%-- 
				    									<!--<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																	
													
					<%if(("+".equalsIgnoreCase(sv.select.getFormData()))||("X".equalsIgnoreCase(sv.select.getFormData())))
							{%>
					
					<input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' 
						 onHelp='return fieldHelp("sr572screensfl" + "." +"select")' 
						 onKeyUp='return checkMaxLength(this)' 
						 name='sr572screensfl.select_R<%=count%>'
						 id='sr572screensfl.select_R<%=count%>'
						 onClick="selectedRow('sr572screensfl.select_R<%=count%>')"
						 class="radio"
						 disabled
					 />
						<%	}
						else {
						%>
					 					 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr572screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr572screensfl.select_R<%=count%>'
						 id='sr572screensfl.select_R<%=count%>'
						 onClick="selectedRow('sr572screensfl.select_R<%=count%>')"
						 class="radio"
						
					 />
					 
					<%} %>
					
											
									</td> --%>
				    									<td class="tableDataTag" style="width:180px;" align="left">									
																
									
											
						<%= sv.crcode.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:180px;" align="left">									
																
									
											
						<%= sv.rtable.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:300px;" align="left">									
																
									
											
						<%= sv.longdesc.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:150px;" align="left">									
																
									
											
						<%= sv.hrequired.getFormData()%>
						
														 
				
									</td>
	
	
			<%
	count = count + 1;
	Sr572screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	
	</tr>
	
	
	
	
	</tbody>
	
	</table>
	
		
	
		
		</div></div></div>
	
		
		
		
		
		</div>
<div class="row">
  
                  <div class="col-md-4">
        			 <div class="btn-group">
                    <div class="sectionbutton">
        			
						<a class="btn btn-info" href= "#" onClick=" JavaScript:perFormOperation('X')">
						<%=resourceBundleHandler.gettingValueFromBundle("Select")%></a>


    </div>                    
</div></div></div>

	<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
	<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
		</div></div>

<script language='javaScript'> 
function perFormOperation1(act){
if(selectedRow1!=null)
document.getElementById(selectedRow1).value=act;
}

	function perFormOperationForContinue(act) {
		if (selectedRow1 != null && selectedRow1 != "") {
			document.getElementById(selectedRow1).value = act;
		} else {
			var elementSelected = false;
			for ( var index = 0; index < idRowArray.length; ++index) {
				var item = idRowArray[index];
				if (item != null && item != "") {
					document.getElementById(item).value = act;
					elementSelected = true;
				}
			}
		}
	}
	
	//MIBT-94 STARTS
	function selectedRowChecIdValue(idt1,idt2,count,idValue)
	{
		count=count-1;
		if(document.getElementById(idt2).checked){
			document.getElementById(idt1).value=idValue;
			idRowArray[count]=idt1;
		}else{
			document.getElementById(idt1).value=' ';
			idRowArray[count]="";
		}
	}

	function selectedRowIdValue(idt,idValue){

		//document.getElementById(idt).value='1';
		document.getElementById(idt).selected=true;
		document.getElementById(idt).value=idValue;
		firstTimeOver=true;
		selectedRow1=idt;
	}
	//MIBT-94 ENDS
</script>


<script>
$(document).ready(function() {
    var showval= document.getElementById('show_lbl').value;
    var toval= document.getElementById('to_lbl').value;
    var ofval= document.getElementById('of_lbl').value;
    var entriesval= document.getElementById('entries_lbl').value;
    var nextval= document.getElementById('nxtbtn_lbl').value;
    var previousval= document.getElementById('prebtn_lbl').value;
    var dtmessage =  document.getElementById('msg_lbl').value;
	$('#dataTables-sr572').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        paging:   true,		
        info:     true,       
        orderable: false,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {
                "next":       nextval,
                "previous":   previousval
            }
        }
    });
	
	<%-- if(1 == sv.uwFlag){ %> 
		<% if(-1 != sv.mainCovrDisFlag) {%>
		popupWarning(<%=sv.mainCovrDisPos%>,<%=sv.mainCovrDisFlag%>);
		<% } %>
		<% if(-1 != sv.tableCovrDisPos) {%>
		popupWarning(<%=sv.tableCovrDisPos%>,<%=sv.tableCovrDisFlag%>);
		<% } %>
	<% } --%>
})
</script>
<% if(1 == sv.uwFlag){ %>
<style type="text/css">
<!--
.popup-panel{
    display: block;
	z-index: 99;
    width: 45% !important;
    height: 25%;
    position: absolute !important;
    left: 28%;
    top: 30% !important;
    margin-bottom: 0 !important;
    border-color: #bce8f1 !important;
    background-color: #fff;
    border: 1px solid transparent;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
}
.popup-heading{
    font-weight: bolder;
    text-align: center;
    padding-right: 40px;
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1 ;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
}
.popup-body{
	padding: 0 !important;
    padding-left: 15px !important;
    height: auto !important;
    line-height: 1.42857143;
    color: #333;
    clear: both;
	display: table;
	margin-top:30px;
	width:100%;
}
-->
</style>

<script type='text/javascript'>
	function checkComUWResult(obj){
		$("#popup-select").val("N");
		var idt = "sr572screensfl.select_R" + obj;
		var checkboxValue = document.getElementById(idt).checked;
        if(checkboxValue==true){
			var comType = getComType(obj);
			if(uwResultMap.has(comType)){
				uwResult = uwResultMap.get(comType);
				if(uwResult == 2 || uwResult == 1){
					popupWarning(obj, uwResult);
				}
			}
        }
		selectedRowChecIdValue(idt, idt, obj,'X');
	}
	function getComType(obj){//todo
		var idt = "input[name='sr572screensfl.select_R" + obj + "']";
		var crtable = $($(idt).parent().parent().children()[1]).text().trim();
		if(crtable.isBlank() || /^\d+$/.test(crtable)){
			return $($(idt).parent().parent().children()[2]).text().trim();
		} else {
			return crtable;
		}
	}
	function saveUWReason(obj){
		var flag = $("#popup-select").val();
		if("Y" != flag){
			var idt = "sr572screensfl.select_R" + obj;
			document.getElementById(idt).checked = false;
		} 
		closeWarning();
	}
	
	function popupWarning(obj, type){
   		$(".container").css("opacity","0.5");
   		$(".container").css("pointer-events","none");
   		$("#navigateButton").css("opacity","0.5");
   		$("#navigateButton").css("pointer-events","none");
   		document.getElementById("popup-button").onclick = function(){saveUWReason(obj);};
   		$(".popup-panel").css("display","block");
   		var uwResultText = "Conditionally accepted";
   		if(type == 2){
   			uwResultText = "Decline";
   		}
   		$("#uwResultText").text(uwResultText);
   		disableF5();
	}
	function closeWarning(){
   		$(".container").css("opacity","1");
   		$(".container").css("pointer-events","");
   		$("#navigateButton").css("opacity","1");
   		$("#navigateButton").css("pointer-events","");
   		$("#popup-reason").val("");
   		$(".popup-panel").css("display","none");
   		$("#popup-reason").css("cssText", "border-color:#ccc !important");
   		enableF5();
	}
	String.prototype.isBlank = function(){
	    var s = $.trim(this);
	    if(s == "undefined" || s == null || s == "" || s.length == 0){
	        return true;
	    }
	    return false;
	};
　　    String.prototype.trim=function(){
　　    	return this.replace(/(^\s*)|(\s*$)/g, "");
　　    }
	function disableF5(){
		document.body.onkeydown = function(){ 
			if ( event.keyCode==116) 
			{ 
				event.keyCode = 0; 
				event.cancelBubble = true; 
				return false; 
			} 
		};
	}
	function enableF5(){
		document.body.onkeydown = checkAllKeys;
	}
</script>
<% } %>

<%@ include file="/POLACommon2NEW.jsp"%>
 <div id="popup-panel" class="popup-panel" style="display: none;width: 60% !important;height: 40%;">
   	<div class="popup-heading" style="font-weight: bolder; text-align: left;padding-right: 40px;">
       	Warning
    </div>
    	<div class="popup-body">     
			<div class="row" style="padding-left: 15px !important;width: 100%;margin-top:2px">	
				 <div>
				 	<label style="font-size: 15px !important;width: 100%margin-top:2px">
				 	The selected benefit has higher risk do you still want to accept and proceed?
				 	</label>
				 	<select id="popup-select" name="" type="list" class="" style="width:60px;float:right;margin-top:12px">
				 		<option value="N" title="No">No</option>
				 		<option value="Y" title="Yes">Yes</option>
				 	</select>
				 </div>
				 <div style="margin-top:17px">
				 	<label style="font-size: 15px !important;margin-top:17px">Reason: </label>
				 	<input name="" id="popup-reason" type="text" size="100" style="font-size: 16px !important;overflow: hidden; white-space:
				 	 nowrap; position: relative; top: 13px; left: 0px; text-align: left; min-width: 80% !important;" 
				 	 value="" maxlength="100" class="form-control"/>
				 </div>
				 <div style="float:right;margin-top:15px;">
				 	<input type="button" id="popup-button" style="width:70px" class="popup-button" value=' OK '/>
				 </div>
			</div>
		</div>
</div>
