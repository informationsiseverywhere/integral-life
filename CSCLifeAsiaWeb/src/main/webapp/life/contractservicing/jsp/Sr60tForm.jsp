

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR60T";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>

<%Sr60tScreenVars sv = (Sr60tScreenVars) fw.getVariables();%>
	
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
<table><tr><td>
<%					
		if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

	
</td><td>
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:140px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td></tr></table></div></div>

<div class="col-md-4"> 
				    		<div class="form-group">  	
<label><%=resourceBundleHandler.gettingValueFromBundle("Scheme")%></label> 
<table><tr><td>

<%					
		if(!((sv.schmno.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>

</td><td>
		<%					
		if(!((sv.schmnme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmnme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmnme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:140px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 
</td></tr></table></div></div></div>
<br><br>

	<%
	GeneralTable sfl = fw.getTable("sr60tscreensfl");
	int height;
	if(sfl.count()*27 > 320) {
	height = 320 ;
	} else {
	height = sfl.count()*27;
	}	
	%>

<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
	
<style type="text/css">
	.fakeContainer {
		width:720px;		
		height:380px;	/*ILIFE-2143*/
		top: 80px;
		left:4px;			
	}
	.sSky th, .sSky td{
	font-size:12px !important;
	}
	.sr60tTable tr{height:35px}
</style>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr60tTable", {
				fixedCols : 0,					
				colWidths : [50,110,110,110,100,113,110],
				hasHorizonScroll :"Y",	
				isReadOnlyFlag: true				
				
			});

        });
    </script>
   
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-sr60t'>
							<thead>
								<tr class='info'>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>							         								
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Scheme Number")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Product Type")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Tax Period From")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Tax Period To")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></th>
</tr></thead>
		 
	<%
	Sr60tscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr60tscreensfl
	.hasMoreScreenRows(sfl)) {	
		
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		
%>

			<tr class="tableRowTag" id='<%="tablerow"+count%>'>
				<td>
				<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {%>
				<input type="checkbox" value='<%=sv.select.getFormData()%>'
					onFocus='doFocus(this)'
					onHelp='return fieldHelp("sr60tscreensfl" + "." +
						 "select")'
					onKeyUp='return checkMaxLength(this)'
					name='sr60tscreensfl.select_R<%=count%>'
					id='sr60tscreensfl.select_R<%=count%>'
					onClick="selectedRow('sr60tscreensfl.select_R<%=count%>')"
					class="UICheck" 
					<% if(((sv.taxPeriodFromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {%> 
						disabled="disabled"
					<% }%> />
					<%} %>
					</td>
				<td class="tableDataTag" align="left"><%= sv.chdrnum.getFormData()%></td>
				<td class="tableDataTag tableDataTagFixed" align="left"><%= sv.clntnumSfl.getFormData()%></td>
				<td class="tableDataTag" align="left"><%= sv.schmnoSfl.getFormData()%></td>
				<td class="tableDataTag" align="left"><%= sv.cnttype.getFormData()%></td>
				
				<td class="tableDataTag" align="left">
					<div>
						<%
						if(!((sv.taxPeriodFromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.taxPeriodFromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}			
			
						} else  {
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.taxPeriodFromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
						}
						%>
						<%=formatValue%>
					</div>
				
				</td>				
				<td class="tableDataTag" align="left">
					<div>
						<%
						if(!((sv.taxPeriodToDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.taxPeriodToDisp.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}			
			
						} else  {
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.taxPeriodToDisp.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
						}
						%>
						<%=formatValue%>
					</div>
				</td>		
				<!-- ILIFE-8031 : Starts-->		
				<td class="tableDataTag" align="left">
					<div>
						<%
						if((sv.totamnt).getClass().getSimpleName().equals("ZonedDecimalData")) {			
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.totamnt).getFieldName());									
							valueThis=smartHF.getPicFormatted(qpsf,sv.totamnt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);			
						}
						%>
						<%=valueThis%>
					</div>
				</td>
			<!-- ILIFE-8031 : Ends -->
				<td class="tableDataTag" align="left"><input
					name='ukPensionInd' id='ukPensionInd' type='hidden'
					value="<%=sv.statcode.getFormData()%>"></td>
				
			</tr>

			<%
	count = count + 1;	
	Sr60tscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
<!-- </DIV> -->
	
<br/></div>

</div></div></div></div>


<script>
	$(document).ready(function() {
		$('#dataTables-sr60t').DataTable({ /* ILIFE-8031 */
			ordering : false,
			searching : false,
			scrollY : "350px",
			scrollCollapse : true,
			scrollX : true,
			
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
