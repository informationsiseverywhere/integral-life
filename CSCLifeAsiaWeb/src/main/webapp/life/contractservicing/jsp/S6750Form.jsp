<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6750";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6750ScreenVars sv = (S6750ScreenVars) fw.getVariables();%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 = Select");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
	
	
	
	
	<div class="panel panel-default">
<div class="panel-body">     

<div class="row">

<div class="col-md-12"> 
	
 <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-s6750' width='100%'>
               <thead>
		
			        <tr class='info'>
			        
				      <th> <center>  <%=resourceBundleHandler.gettingValueFromBundle("Select")%></center></th>
		              <th><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction Code")%></center></th>
		              <th><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction Description")%></center></th>
			     	 	        
		 	        </tr>
			 </thead>
			 
			 
			 <%
		GeneralTable sfl = fw.getTable("s6750screensfl");
		/* int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	 */
		%>
		
		
			<%
	S6750screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6750screensfl
	.hasMoreScreenRows(sfl)) {	
%>
		
			 <tbody>
	
	
	      <tr>
					<td>
					<div class="form-group">
																			
										<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
													
					
					 					 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s6750screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s6750screensfl.select_R<%=count%>'
						 id='s6750screensfl.select_R<%=count%>'
						 onClick="selectedRow('s6750screensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
									<%}%>
									</div>
			</td>
				    									
			

				  	<td >		<div class="form-group">							
										<%if((new Byte((sv.transCode).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.transCode.getFormData()%>
						
														 
				
									<%}%>
		</div>	</td>
				    		<td>									
										<%if((new Byte((sv.descr).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.descr.getFormData()%>
						
														 
				
									<%}%>
			</td>
					
	</tr>

	<%
	count = count + 1;
	S6750screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	
	</tbody>
	
	
	
	</table>
	
	</div>
	
	
	</div></div></div></div>
	
	<script>
       $(document).ready(function() {
              $('#dataTables-s6750').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script>
	
<%@ include file="/POLACommon2NEW.jsp"%>

