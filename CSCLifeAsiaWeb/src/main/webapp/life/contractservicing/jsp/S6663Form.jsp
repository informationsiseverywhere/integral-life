<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6663";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%S6663ScreenVars sv = (S6663ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = new StringData("                                                     ");%>
	<%StringData generatedText7 = new StringData(" ");%>
	<%StringData generatedText8 = new StringData("                                               ");%>
	<%StringData generatedText9 = new StringData(" ");%>
	<%StringData generatedText10 = new StringData(" ");%>
	<%StringData generatedText11 = new StringData("Payor No ");%>
	<%StringData generatedText12 = new StringData("                          ");%>
	<%StringData generatedText13 = new StringData(" ");%>
	<%StringData generatedText14 = new StringData(" ");%>
	<%StringData generatedText15 = new StringData("               ");%>
	<%StringData generatedText16 = new StringData(" ");%>
	<%StringData generatedText17 = new StringData(" ");%>
	<%StringData generatedText18 = new StringData("               ");%>
	<%StringData generatedText19 = new StringData(" ");%>
	<%StringData generatedText20 = new StringData(" ");%>
	<%StringData generatedText21 = new StringData("               ");%>
	<%StringData generatedText22 = new StringData(" ");%>
	<%StringData generatedText23 = new StringData(" ");%>
	<%StringData generatedText24 = new StringData("               ");%>
	<%StringData generatedText25 = new StringData(" ");%>
	<%StringData generatedText26 = new StringData(" ");%>
	<%StringData generatedText27 = new StringData("               ");%>
	<%StringData generatedText28 = new StringData(" ");%>
	<%StringData generatedText29 = new StringData(" ");%>
	<%StringData generatedText36 = new StringData("    ");%>
	<%StringData generatedText30 = new StringData(" ");%>
	<%StringData generatedText31 = new StringData(" ");%>
	<%StringData generatedText32 = new StringData("Income Source  ");%>
	<%StringData generatedText33 = new StringData("                            ");%>
	<%StringData generatedText34 = new StringData(" ");%>
	<%StringData generatedText35 = new StringData("                                                     ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.payrnum.setReverse(BaseScreenData.REVERSED);
			sv.payrnum.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.payrnum, appVars.ind20);
		if (!appVars.ind01.isOn()) {
			sv.payrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.incomeSeqNo.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind10.isOn()) {
			sv.incomeSeqNo.setInvisibility(BaseScreenData.INVISIBLE);
			sv.incomeSeqNo.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.incomeSeqNo.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.incomeSeqNo.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-6">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payor No")%>
					<%}%></label>
		       		<table><tr><td class="input-group">
						<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						<%	
							
							longValue = sv.payrnum.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.payrnum).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						<input name='payrnum' id='payrnum'
						type='text' 
						value='<%=sv.payrnum.getFormData()%>' 
						maxLength='<%=sv.payrnum.getLength()%>' 
						size='<%=sv.payrnum.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(payrnum)' onKeyUp='return checkMaxLength(this)'  
						
						<% 
							if((new Byte((sv.payrnum).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.payrnum).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						class="bold_cell" >
						 
						 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payrnum')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('payrnum')); changeF4Image(this); doAction('PFKEY04')"> 
						<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
						</a> --%>
						
						<%
							}else { 
						%>
						
						class = ' <%=(sv.payrnum).getColor()== null  ? 
						"input_cell" :  (sv.payrnum).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payrnum')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('payrnum')); changeF4Image(this); doAction('PFKEY04')"> 
						<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
						</a> --%>
						
						<%}longValue = null;}} %>
						
						</td>
						<td>  &nbsp;&nbsp;  &nbsp; </td>
						
						<td>
						
						
						<%if ((new Byte((sv.payrname).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						
						<input name='payrname' 
						type='text'
						
						<%if((sv.payrname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.payrname.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.payrname.getLength()%>'
						maxLength='<%= sv.payrname.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(payrname)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.payrname).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.payrname).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.payrname).getColor()== null  ? 
									"input_cell" :  (sv.payrname).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	<div class="col-md-4"></div>
		       	<div class="col-md-2"></div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%StringData newLabelgeneratedText2=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Address");%>
					<%if ((new Byte((newLabelgeneratedText2).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Address")%>
					<%}%></label>
		       		
		       		<table><tr><td>
		       		<div class="input-group" style="padding-bottom: 4px;">
					<%if ((new Byte((sv.payraddr1).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.payraddr1.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.payraddr1.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.payraddr1.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' ><%--ILIFE-3222 style removed --%>
									<%=XSSFilter.escapeHtml(formatValue)%>   <!-- ILIFE-2725 Life Cross Browser - Sprint 4 D1 : Task 1 changed width  --> 
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					  </div>
					  </td></tr>
					  <tr><td>
					  <div class="input-group" style="padding-bottom: 4px;">
					  <%if ((new Byte((sv.payraddr2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.payraddr2.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr2.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr2.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:80px" ><%--ILIFE-3222 style removed --%>
										<%=XSSFilter.escapeHtml(formatValue)%>   <!-- ILIFE-2725 Life Cross Browser - Sprint 4 D1 : Task 1 changed width  --> 
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						  </div>
					   </td></tr>
					  <tr style='height:22px;'>
						<td>
						<div class="input-group" style="padding-bottom: 4px;">
						
						<%if ((new Byte((sv.payraddr3).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.payraddr3.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr3.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr3.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px"><%--ILIFE-3222 style removed --%>
										<%=XSSFilter.escapeHtml(formatValue)%>    <!-- ILIFE-2725 Life Cross Browser - Sprint 4 D1 : Task 1 changed width  --> 
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							</div>
						
						</td></tr>
						
						<tr style='height:22px;'>
						<td>
						
						<div class="input-group" style="padding-bottom: 4px;">
						
						<%if ((new Byte((sv.payraddr4).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.payraddr4.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr4.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr4.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px"><%--ILIFE-3222 style removed --%>
										<%=XSSFilter.escapeHtml(formatValue)%>    <!-- ILIFE-2725 Life Cross Browser - Sprint 4 D1 : Task 1 changed width  --> 
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							
						</div>
						</td></tr>
						
						<tr style='height:22px;'>
						<td>
						<div class="input-group" style="padding-bottom: 4px;">
						
						
						<%if ((new Byte((sv.payraddr5).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.payraddr5.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr5.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payraddr5.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:80px"><%--ILIFE-3222 style removed --%>
										<%=XSSFilter.escapeHtml(formatValue)%>    <!-- ILIFE-2725 Life Cross Browser - Sprint 4 D1 : Task 1 changed width  --> 
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							</div>
						
						</td></tr>
					  </table>
		       		
		       		</div>
		       	</div>
		    </div>
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
		       		<div class="input-group" style="min-width:80px">
					<%if ((new Byte((sv.cltpcode).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cltpcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
		       		</div>
		       		</div>
		       	</div>
		       	<div class="col-md-4"></div>
		       	<div class="col-md-4"></div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Income Source")%>
					<%}%>
					</label>
		       		<table>
					<tr style='height:22px;'><tr style='height:22px;'><td width='251'>
					<%if ((new Byte((sv.incomeSeqNo).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
					
					
						<%	
								qpsf = fw.getFieldXMLDef((sv.incomeSeqNo).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								
						%>
					
					<input name='incomeSeqNo' 
					type='text'
					
					<%if((sv.incomeSeqNo).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
						value='<%=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo) %>'
								 <%
						 valueThis=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo);
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo) %>'
						 <%}%>
					
					size='<%= sv.incomeSeqNo.getLength()%>'
					maxLength='<%= sv.incomeSeqNo.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(incomeSeqNo)' onKeyUp='return checkMaxLength(this)'  
					
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
					
					<% 
						if((new Byte((sv.incomeSeqNo).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.incomeSeqNo).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.incomeSeqNo).getColor()== null  ? 
								"input_cell" :  (sv.incomeSeqNo).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					<%}%>
					</td></tr></table>
		       		</div>
		       	</div><div class="col-md-4"></div><div class="col-md-4"></div>
		    </div>
	 </div>
</div>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText0).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText36).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText34).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>

</tr></table></div>

<%@ include file="/POLACommon2NEW.jsp"%>

