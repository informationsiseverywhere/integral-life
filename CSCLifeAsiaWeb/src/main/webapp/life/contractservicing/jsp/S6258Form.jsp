

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6258";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	S6258ScreenVars sv = (S6258ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number   ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status  ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner    ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Payor    ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Agency            ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Commence  ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid-to Date       ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Currency           ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to Date     ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Nxt Incr. Due Date ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payment Frequency  ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billing Currency   ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Regular Premium    ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Current Suspense   ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Deposit    ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"--------------------------------------------------------------------------------");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Advance Payment To          ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date              ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Ttl Prem w/Tax     ");
%>

<%
	{
		if (appVars.ind07.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payToDateAdvanceDisp.setReverse(BaseScreenData.REVERSED);
			sv.payToDateAdvanceDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.payToDateAdvanceDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind41.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}
%>



<div class="panel panel-default">
	<div class="panel-body">
		<!-- <div class='outerDiv'>
<table> -->

		<div class="row">
			<div class="col-md-4"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>

<table><tr><td>




	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="min-width:2px">
	</td><td style="min-width:40px">





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:40px; max-width:50px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="min-width:2px">
	</td><td>





	
  		
		<%					
		if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="cntdesc" style="max-width:160px;min-width:120px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
  
	</div></div>

			<div class="col-md-4">
				<div class="form-group">
					<!-- <td width='251'> -->

					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>

					<table><tr><td>
						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</td><td style="padding-left:1px;max-width:190px;">

						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.pstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.pstate.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>

					<table><tr><td>

						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:145px;">
						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="cntown">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Payor")%></label>
					<table><tr><td>

						<%
							if (!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:145px;">
						<%
							if (!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payorname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payorname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>

					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>

					<table><tr><td>

						<%
							if (!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="padding-left:1px;max-width:185px;">
						<%
							if (!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agentname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agentname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->

					<div>

						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>


			<!-- </td> -->

			<div class="col-md-2">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>
					<div>

						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>

				</div>
			</div>

			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>

					<div>

						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">

					<%
						if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

					<%
						}
					%>
					<div>

						<%
							if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 65px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Currency")%></label>
					<%
						}
					%>

					<div>

						<%
							if ((new Byte((sv.billcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.billcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.billcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.billcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 65px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Nxt Incr. Due Date")%></label>
					<div>
						<%
							if (!((sv.cpiDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cpiDateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cpiDateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group" style="width: 145px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Deposit")%></label>
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.prmdepst).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf, sv.prmdepst,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.prmdepst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-5"></div>

			<div class="col-md-3">
				<div class="form-group" style="width: 145px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Frequency")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("billfreq");
							longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());

							if (!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width: 145px">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Current Suspense")%></label>
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.sacscurbal).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.sacscurbal,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.sacscurbal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-5"></div>

			<div class="col-md-3">
				<div class="form-group" style="width: 145px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Regular Premium")%></label>

					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.cntinst).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.cntinst,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.cntinst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <!-- ILIFE-1711 START by nnazeer --> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%>
						<!-- ILIFE-1711 END -->
					</label>
					<div>
						<%
							if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
								// qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);   //ILIFE-1711 START by nnazeer
								formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS); //ILIFE-1711 END

								if (!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div style="width: 100px;" class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Advance Payment To")%></label>

					<%
						if ((new Byte((sv.payToDateAdvanceDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<input name='payToDateAdvanceDisp' type='text'
						value='<%=sv.payToDateAdvanceDisp.getFormData()%>'
						maxLength='<%=sv.payToDateAdvanceDisp.getLength()%>'
						size='<%=sv.payToDateAdvanceDisp.getLength()%>'
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(payToDateAdvanceDisp)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">

					<%
						} else if ((new Byte((sv.payToDateAdvanceDisp).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					<!-- class="bold_cell" > -->
					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy"
						data-link-field="payToDateAdvanceDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.payToDateAdvanceDisp,
						(sv.payToDateAdvanceDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('payToDateAdvanceDisp'), '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
				</a> --%>

					<%
						} else {
					%>

					<%-- class = ' <%=(sv.payToDateAdvanceDisp).getColor()== null  ? 
				"input_cell" :  (sv.payToDateAdvanceDisp).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' > --%>

					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy"
						data-link-field="payToDateAdvanceDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.payToDateAdvanceDisp,
						(sv.payToDateAdvanceDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('payToDateAdvanceDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
				</a> --%>

					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-5"></div>
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<input name='effdateDisp' type='text'
						value='<%=sv.effdateDisp.getFormData()%>'
						maxLength='<%=sv.effdateDisp.getLength()%>'
						size='<%=sv.effdateDisp.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(effdateDisp)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">

					<%
						} else if ((new Byte((sv.effdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					<!-- class="bold_cell" > -->
					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
				</a> --%>

					<%
						} else {
					%>

					<%-- class = ' <%=(sv.effdateDisp).getColor()== null  ? 
				"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' > --%>

					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
				</a> --%>

					<%
						}
					%>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	
	if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','215px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','190px')
	} 
	
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

