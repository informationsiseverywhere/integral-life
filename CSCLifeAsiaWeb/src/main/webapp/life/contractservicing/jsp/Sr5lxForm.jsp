<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5lx";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%Sr5lxScreenVars sv = (Sr5lxScreenVars) fw.getVariables();%>

<%
	if (appVars.ind01.isOn()) {
		sv.taxPeriodFrm.setReverse(BaseScreenData.REVERSED);
		sv.taxPeriodFrm.setColor(BaseScreenData.RED);
	}
	if (appVars.ind02.isOn()) {
		sv.taxPeriodTo.setReverse(BaseScreenData.REVERSED);
		sv.taxPeriodTo.setColor(BaseScreenData.RED);
	}
%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table>
		        		<tr>
		        			<td>
				        		<%					
									if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<%					
									if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
	        	</div>
	        </div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%					
									if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<label>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp;</label>
							</td>
							<td>
								<%					
									if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 30px;">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Period")%></label>
				<table>
					<tr>
						<td>
							<div class="form-group">
								<div class="input-group"">
									<input name='taxPeriodFrm' id="taxPeriodFrm" type='text' value='<%=sv.taxPeriodFrm.getFormData()%>' maxLength='<%=sv.taxPeriodFrm.getLength()%>' 
										size='<%=sv.taxPeriodFrm.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(taxPeriodFrm)' onKeyUp='return checkMaxLength(this)'  
										<% 
											if((new Byte((sv.taxPeriodFrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
										%>	readonly="true"	class="output_cell"	
										<%
											}else if((new Byte((sv.taxPeriodFrm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	class="bold_cell" 
										<%
											}else{
										%>
											class = '<%=(sv.taxPeriodFrm).getColor() == null ? "input_cell" : (sv.taxPeriodFrm).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
										<%	
											}
										%>
									/>
								</div>
							</div>
						</td>
						<td>
							<label>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("/")%>&nbsp;</label>
						</td>
						<td>
							<div class="form-group">
								<div class="input-group">
									<input name='taxPeriodTo' id="taxPeriodTo" type='text' value='<%=sv.taxPeriodTo.getFormData()%>' maxLength='<%=sv.taxPeriodTo.getLength()%>' 
										size='<%=sv.taxPeriodTo.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(taxPeriodTo)' onKeyUp='return checkMaxLength(this)'  
										<% 
												if((new Byte((sv.taxPeriodTo).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
											%>	readonly="true"	class="output_cell"	
											<%
												}else if((new Byte((sv.taxPeriodTo).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
											%>	class="bold_cell" 
											<%
												}else{
											%>
												class = '<%=(sv.taxPeriodTo).getColor() == null ? "input_cell" : (sv.taxPeriodTo).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
											<%	
												}
											%>
										/>
									</div>
								</div>
							</td>
						</tr>
					</table>
				
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>