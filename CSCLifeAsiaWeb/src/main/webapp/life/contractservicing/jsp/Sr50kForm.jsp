
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%Sr50kScreenVars sv = (Sr50kScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Old");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"/");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Salutation");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"/");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"/");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date of Birth");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation Class");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.salutl02.setReverse(BaseScreenData.REVERSED);
			sv.salutl02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.salutl02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.salutl02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.cltsex02.setReverse(BaseScreenData.REVERSED);
			sv.cltsex02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind13.isOn()) {
			sv.cltsex02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.cltsex02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.cltdob02Disp.setReverse(BaseScreenData.REVERSED);
			sv.cltdob02Disp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind14.isOn()) {
			sv.cltdob02Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.cltdob02Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.occpcode02.setReverse(BaseScreenData.REVERSED);
			sv.occpcode02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind15.isOn()) {
			sv.occpcode02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.occpcode02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.statcode02.setReverse(BaseScreenData.REVERSED);
			sv.statcode02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind16.isOn()) {
			sv.statcode02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.statcode02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.smoking02.setReverse(BaseScreenData.REVERSED);
			sv.smoking02.setColor(BaseScreenData.RED);
		}
	
		if (!appVars.ind06.isOn()) {
			sv.smoking02.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4" >
				    		<div class="form-group"> 
				    		<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> 	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		<%}%>	
					    		   
						    <table><tr><td>		
<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td><td style="padding-left:1px;">




<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:250px;min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		</td></tr></table>		      			     
				    		</div>
					</div>
					</div>
					<BR>
					
<table>



<tr>


<td>
<div class="row">
<div class="col-md-12">
</div></div>
</td>


	 

<td>
<div class="row">
<div class="col-md-10 col-md-offset-9">
<div class="form-group" style="align="center"> 

<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Old")%>
</label>
<%}%>
</div></div></div>

</td><td></td>

<td>
<div class="row">
<div class="col-md-10 col-md-offset-9">
<div class="form-group" style="align="center"> 
<div class="input-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("New")%>
</label>
<%}%>
</div></div></div></div>
</td>
</tr>




<tr>
<td>

<div class="row">
<div class="col-md-3">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Sex")%>
</label>
<%}%>
</div>
<div class="col-md-1" style="max-width:1px ">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("/")%>
</label>
<%}%>
</div>
<div class="col-md-2" style="padding-left:0px">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Salutation")%>
</label>
<%}%>
</div>
</div>
</td>


<td><div class="row">
<div class="col-md-2">
</div> <div class="col-md-2">
</div><div class="col-md-2">
</div> <div class="col-md-2">
</div> </div></td>


<td>
<div class="row">
<div class="col-md-3">
<div class="form-group" >
<div class="input-group">
<%if ((new Byte((sv.cltsex01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cltsex01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:36px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
		
</div></div></div>



<div class="col-md-1" style="padding-left:30px">

<div class="form-group">

<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							
<label style="padding-top:5px;">
<%=resourceBundleHandler.gettingValueFromBundle("/")%>

</label>
<%}%>
</div></div>


<div class="col-md-2" style="padding-left:5px">
<div class="form-group">
<div class="input-group">

<%if ((new Byte((sv.salutl01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.salutl01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.salutl01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.salutl01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div></div></div>



</td>   
        
<td><div class="row">
<div class="col-md-2">
</div> <div class="col-md-2">
</div><div class="col-md-2">
</div>  <div class="col-md-2">
</div></div></td>




<td>
<div class="row">
<div class="col-md-4">
<div class="form-group">
<div class="input-group">
<%if ((new Byte((sv.cltsex02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.cltsex02.getFormData();  
%>

<% 
	if((new Byte((sv.cltsex02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='cltsex02' id='cltsex02'
type='text' 
value='<%=sv.cltsex02.getFormData()%>' 
maxLength='<%=sv.cltsex02.getLength()%>' 
size='<%=sv.cltsex02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(cltsex02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.cltsex02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.cltsex02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('cltsex02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.cltsex02).getColor()== null  ? 
"input_cell" :  (sv.cltsex02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('cltsex02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;}} %>

</div></div></div>

<div class="col-md-1"  style="padding-left:5px">
<div class="form-group">
<div class="input-group">

 
 		

<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="padding-top:5px;">
<%=resourceBundleHandler.gettingValueFromBundle("/")%>
</label>
<%}%>

</div></div></div>

<div class="col-md-6" style="padding-left:5px">
<div class="form-group">
<div class="input-group">

<%if ((new Byte((sv.salutl02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.salutl02.getFormData();  
%>

<% 
	if((new Byte((sv.salutl02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='salutl02' id='salutl02'
type='text' 
value='<%=sv.salutl02.getFormData()%>' 
maxLength='<%=sv.salutl02.getLength()%>' 
size='<%=sv.salutl02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(salutl02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.salutl02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.salutl02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('salutl02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.salutl02).getColor()== null  ? 
"input_cell" :  (sv.salutl02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('salutl02')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;}} %>


</div></div></div></div>


</td>

</tr>

<tr style='height:22px;'><td>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Date of Birth")%>
</label>
<%}%>


</td>

<td></td>

<td>
<div class="row">
<div class="col-md-4">
<div class="form-group" style="width:100px;">
<div >
<%if ((new Byte((sv.cltdob01Disp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cltdob01Disp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdob01Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdob01Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div></div></div>


</td>


<td><div class="row">
<div class="col-md-2">
</div></div></td>



<td>
<div class="row">
<div class="col-md-7">
<%	
	if ((new Byte((sv.cltdob02Disp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.cltdob02Disp.getFormData();  
%>

<% 
	if((new Byte((sv.cltdob02Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  <div class="form-group">
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%></div>
<% }else {%> 
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="cltdob02Disp" data-link-format="dd/mm/yyyy">

<input name='cltdob02Disp' 
type='text' 
value='<%=sv.cltdob02Disp.getFormData()%>' 
maxLength='<%=sv.cltdob02Disp.getLength()%>' 
size='<%=sv.cltdob02Disp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(cltdob02Disp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.cltdob02Disp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.cltdob02Disp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
			                  
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.cltdob02Disp).getColor()== null  ? 
"input_cell" :  (sv.cltdob02Disp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%}%>
 </div>

<%}}%>

</div></div>	</td>
 
</tr>

<tr style='height:22px;'><td>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Occupation")%>
</label>
<%}%>


</td>
<td>
<div class="row">
<div class="col-md-2">
</div></div></td>


<td>
<div class="row">
<div class="col-md-4">
<div class="form-group" style="width:120px">
<div class="">
<%if ((new Byte((sv.occpcode01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"occpcode01"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("occpcode01");
		longValue = (String) mappedItems.get((sv.occpcode01.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.occpcode01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occpcode01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occpcode01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div></div></div>



</td><td><div class="row">
<div class="col-md-2">
</div></div></td>

<!-- ILIFE-2982 Life Assured Details Screen_SR50K code modification started -->
<td>
<div class="row">
<div class="col-md-4">
<div class="form-group" >
<div >
<%	
	if ((new Byte((sv.occpcode02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"occpcode02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("occpcode02");
	optionValue = makeDropDownList( mappedItems , sv.occpcode02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.occpcode02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.occpcode02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:242px;"> 
<%
} 
%>

<select name='occpcode02' type='list' style="width:205px;"
<% 
	if((new Byte((sv.occpcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.occpcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.occpcode02).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>

<!-- ILIFE-2982 Life Assured Details Screen_SR50K code modification ended -->
</div></div></div></div></td>

</tr>

<tr><td>

<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt" style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%>
</div>
<%}%>
 

</td>
<td><div class="row">
<div class="col-md-2">
</div></div></td>

<td>
<div class="row">
<div class="col-md-4">
<div class="form-group" style="width:120px">
<div >
<%if ((new Byte((sv.statcode01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode01"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("statcode01");
		longValue = (String) mappedItems.get((sv.statcode01.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.statcode01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statcode01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statcode01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div></div></div>



</td>

<td><div class="row">
<div class="col-md-2">
</div></div></td>

<td>
<div class="row">
<div class="col-md-4">
<div class="form-group">
<div class="">
<%	
	if ((new Byte((sv.statcode02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statcode02");
	optionValue = makeDropDownList( mappedItems , sv.statcode02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statcode02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statcode02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:242px;"> 
<%
} 
%>

<select name='statcode02' type='list' style="width:205px;"
<% 
	if((new Byte((sv.statcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statcode02).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>
</div></div></div></div></td>

</tr><tr style='height:22px;'><td>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="font-size:14px">
<%=resourceBundleHandler.gettingValueFromBundle("Smoking")%>
</label>
<%}%>


</td>
<td><div class="row">
<div class="col-md-2">
</div></div></td>


<td>
<div class="row">
<div class="col-md-4">
<div class="form-group" style="width: 120px;">
<div >
<%if ((new Byte((sv.smoking01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"smoking01"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("smoking01");
		longValue = (String) mappedItems.get((sv.smoking01.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.smoking01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div></div></div>


</td>

<td><div class="row">
<div class="col-md-2">
</div></div></td>

<td>	

<div class="row">
<div class="col-md-4">
<div class="form-group">
<div class="">
<%	
	if ((new Byte((sv.smoking02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"smoking02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("smoking02");
	optionValue = makeDropDownList( mappedItems , sv.smoking02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.smoking02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.smoking02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.smoking02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:242px;"> 
<%
} 
%>

<select name='smoking02' type='list' style="width:205px;"
<% 
	if((new Byte((sv.smoking02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.smoking02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.smoking02).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>
</div></div></div></div></td>
</tr></table> 
					</div></div>  <!--panel  -->
	
	
	
	

<!-- ILIFE-2730 Life Cross Browser -Coding and UT- Sprint 4 D1: Task 7 starts -->
<style>
@media \0screen\,screen\9
{.iconpos{margin-bottom:1px;}
}
</style>
<!-- ILIFE-2730 Life Cross Browser -Coding and UT- Sprint 4 D1: Task 7 starts -->


<%@ include file="/POLACommon2NEW.jsp"%>
