

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6756";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%S6756ScreenVars sv = (S6756ScreenVars) fw.getVariables();%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Type ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to Date ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to Date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.efdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.efdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.efdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.tranCode.setReverse(BaseScreenData.REVERSED);
			sv.tranCode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.tranCode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		     <div class="input-group">
						    		
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						    		

				      			     </div>
				    		</div>
					</div> <%} %>
				    		
				    <%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Type")%></label>
							<!-- <div class="input-group" style="max-width:120px"> -->
							<table><tr><td>
						    	
<%if ((new Byte((sv.contractType).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.contractType.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.contractType.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.contractType.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	


</td><td style="padding-left:1px;">


<%if ((new Byte((sv.shortds02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.shortds02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:150px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		</td></tr></table>

				      			     <!-- </div> -->
						</div>
				   </div><%} %>		
			
			<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group">
						    		

<%if ((new Byte((sv.cnstcur).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cnstcur"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cnstcur");
		longValue = (String) mappedItems.get((sv.cnstcur.getFormData()).toString().trim());  
		
	%>
	
  		
		<%					
		if(!((sv.cnstcur.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnstcur.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnstcur.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
										
						</div>
				   </div>	
		    </div><%} %>	
				   
				   <div class="row">	
				   <%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					    		     <div class="input-group">
						    		
<%if ((new Byte((sv.shortds01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.shortds01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
				    		</div>
					</div><%} %>
				    		
				    	<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
							<div class="input-group">
						    		
<%if ((new Byte((sv.shortds03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.shortds03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortds03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
						</div>
				   </div>		<%} %>
				   <div class="col-md-4"> </div>
			</div>
			
			<div class="row">	
			<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					    		     <div class="input-group">
						    		
<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
				    		</div>
					</div>	<%} %>
				    		
	
			<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to Date")%></label>
					    		     <div class="input-group">
						    		
<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
				    		</div>
					</div>  <%}%>
					<div class="col-md-4"> </div>
					 </div>
				    		
				    		<div class="row">	
				    	<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<div class="input-group date form_date col-md-7" data-date="" data-date-format="dd/MM/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">

						    	
<%	
	if ((new Byte((sv.efdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.efdateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.efdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='efdateDisp' 
type='text' 
value='<%=sv.efdateDisp.getFormData()%>' 
maxLength='<%=sv.efdateDisp.getLength()%>' 
size='<%=sv.efdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(efdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.efdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.efdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.efdateDisp).getColor()== null  ? 
"input_cell" :  (sv.efdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} }}%>	

				      			     </div>
						</div>
				   </div><%}%>	
				   
			<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								
								
				<div class="col-md-8"> 
				   
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
				    	
				    	<table><tr><td>
				    	 <div class="form-group">
				    	<%
                                         if ((new Byte((sv.tranCode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.tranCode)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.tranCode)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('tranCode')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
                                  </div>
                              
					</td><td style="max-width:250px;min-width:100px;">		
	                            
	                              
<%if ((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
					</td></tr></table>	
						
						
				    </div>
				    	
				    	<%}%>	
				  
				    		</div>
								
			
			
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->

<%@ include file="/POLACommon2NEW.jsp"%>

