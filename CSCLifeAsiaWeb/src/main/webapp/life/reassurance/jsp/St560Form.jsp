<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "ST560";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%St560ScreenVars sv = (St560ScreenVars) fw.getVariables();%>

<%if (sv.St560screenWritten.gt(0)) {%>
	<%St560screen.clearClassString(sv);%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%sv.scheduleName.setClassString("");%>
<%	sv.scheduleName.appendClassString("string_fld");
	sv.scheduleName.appendClassString("output_txt");
	sv.scheduleName.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%sv.acctmonth.setClassString("");%>
<%	sv.acctmonth.appendClassString("num_fld");
	sv.acctmonth.appendClassString("output_txt");
	sv.acctmonth.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%sv.scheduleNumber.setClassString("");%>
<%	sv.scheduleNumber.appendClassString("num_fld");
	sv.scheduleNumber.appendClassString("output_txt");
	sv.scheduleNumber.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%sv.acctyear.setClassString("");%>
<%	sv.acctyear.appendClassString("num_fld");
	sv.acctyear.appendClassString("output_txt");
	sv.acctyear.appendClassString("highlight");
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%sv.effdateDisp.setClassString("");%>
<%	sv.effdateDisp.appendClassString("string_fld");
	sv.effdateDisp.appendClassString("output_txt");
	sv.effdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%sv.bcompany.setClassString("");%>
<%	sv.bcompany.appendClassString("string_fld");
	sv.bcompany.appendClassString("output_txt");
	sv.bcompany.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%sv.jobq.setClassString("");%>
<%	sv.jobq.appendClassString("string_fld");
	sv.jobq.appendClassString("output_txt");
	sv.jobq.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%sv.bbranch.setClassString("");%>
<%	sv.bbranch.appendClassString("string_fld");
	sv.bbranch.appendClassString("output_txt");
	sv.bbranch.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Original Commencement Date ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From Date ");%>
	<%sv.fromdateDisp.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  To Date ");%>
	<%sv.todateDisp.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind13.isOn()) {
			sv.todateDisp.setReverse(BaseScreenData.REVERSED);
			sv.todateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.todateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.fromdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.fromdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.fromdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 7, generatedText5)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.scheduleName)%>

	<%=smartHF.getLit(3, 49, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 73, fw, sv.acctmonth, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(4, 16, generatedText6)%>

	<%=smartHF.getHTMLVar(4, 31, fw, sv.scheduleNumber, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(4, 60, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 73, fw, sv.acctyear, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(5, 7, generatedText1)%>

	<%=smartHF.getHTMLSpaceVar(5, 31, fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 31, fw, sv.effdateDisp)%>

	<%=smartHF.getLit(5, 49, generatedText8)%>

	<%=smartHF.getHTMLVar(5, 73, fw, sv.bcompany)%>

	<%=smartHF.getLit(6, 7, generatedText3)%>

	<%=smartHF.getHTMLVar(6, 31, fw, sv.jobq)%>

	<%=smartHF.getLit(6, 49, generatedText7)%>

	<%=smartHF.getHTMLVar(6, 73, fw, sv.bbranch)%>

	<%=smartHF.getLit(10, 18, generatedText12)%>

	<%=smartHF.getLit(12, 26, generatedText13)%>

	<%=smartHF.getHTMLSpaceVar(12, 38, fw, sv.fromdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(12, 38, fw, sv.fromdateDisp)%>

	<%=smartHF.getLit(14, 26, generatedText14)%>

	<%=smartHF.getHTMLSpaceVar(14, 38, fw, sv.todateDisp)%>
	<%=smartHF.getHTMLCalNSVar(14, 38, fw, sv.todateDisp)%>




<%}%>

<%if (sv.St560protectWritten.gt(0)) {%>
	<%St560protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
