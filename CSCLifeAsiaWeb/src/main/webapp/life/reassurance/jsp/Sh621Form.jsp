

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH621";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%Sh621ScreenVars sv = (Sh621ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Class  ");%>

<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                     <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
								<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  						</div>	                   
	               </div>
	               <!-- <div class="col-md-1">
	                </div> -->
 					<div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:60px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  						</div>	                   
	               </div>	
	               <!-- <div class="col-md-2">
	               </div> -->	
	                <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						<!-- <div class="input-group"> -->
						<table><tr><td>
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  						<!-- </div>	  -->
  						</td></tr></table> 
  						</div>	                 
	               </div>	               	                           
	   			</div>
	   			
	   			 <div class="row">
                     <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Class")%></label>
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rprmcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rprmcls");
	optionValue = makeDropDownList( mappedItems , sv.rprmcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rprmcls.getFormData()).toString().trim());  
%>
 <% 
	if((new Byte((sv.rprmcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rprmcls).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:165px;"> 
<%
} 
%>

<select name='rprmcls' type='list' style="width:240px;"
<% 
	if((new Byte((sv.rprmcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rprmcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rprmcls).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%> 
<!-- ILIFE-2658 Life Cross Browser - Coding and UT - Sprint 3 D2 Start Modified By snayeni -->
<%-- <%=smartHF.getDropDownExt(sv.rprmcls, fw, longValue, "rprmcls", optionValue,0,300) %> --%> 
	                        </div>
	                  </div>
	                  <div class="col-md-4"></div><div class="col-md-4"></div>
	              </div>
		</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

