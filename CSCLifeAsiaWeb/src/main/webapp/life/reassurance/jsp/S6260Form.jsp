

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6260";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S6260ScreenVars sv = (S6260ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Create Reassurer");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Modify Reassurer");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Enquire on Reassurer");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurer Number ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.rasnum.setReverse(BaseScreenData.REVERSED);
			sv.rasnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.rasnum.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
<div class="panel-heading">
<%=resourceBundleHandler.gettingValueFromBundle("Input]")%>
</div>
<div class="panel-body">
				<div class="row">
						<div class="col-md-3"> 
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Reassurer Number")%>
</label>
<div class="input-group">
<input name='rasnum' id='rasnum'
type='text' 
value='<%=sv.rasnum.getFormData()%>' 
maxLength='<%=sv.rasnum.getLength()%>' 
size='<%=sv.rasnum.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rasnum)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rasnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rasnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
					        <button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('rasnum')); doAction('PFKEY04');">
					        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        </button>
						</span>
<%
	}else { 
%>

class = ' <%=(sv.rasnum).getColor()== null  ? 
"input_cell" :  (sv.rasnum).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('rasnum')); doAction('PFKEY04');">
		        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		        			</button>
		      			</span>

<%} %>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
							<div class="panel-heading">
							<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
							</div>
							<div class="panel-body">
<div class="row">
								<div class="col-md-4"> 
									<div class="radioButtonSubmenuUIG">
									
									<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Create Reassurer")%></label> 
</div>
</div>
<div class="col-md-4">
<div class="radioButtonSubmenuUIG">

<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
<%=resourceBundleHandler.gettingValueFromBundle("Modify Reassurer")%></label> 
</div>
</div>
<div class="col-md-4">
<div class="radioButtonSubmenuUIG">

<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%>
<%=resourceBundleHandler.gettingValueFromBundle("Enquire on Reassurer")%></label> 
</div>
</div>


<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >

</div>
</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>

