<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5465";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5465ScreenVars sv = (S5465ScreenVars) fw.getVariables();%>
<%
{
		if (appVars.ind20.isOn()) {
			sv.callamt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind30.isOn()) {
			sv.callamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.callamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.callamt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
<div class="input-group" style="min-width:410px;">
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
							%>
								<input class="form-control" type="text" placeholder='<%=formatValue%>' disabled style="max-width: 160px;">	
							<%
							longValue = null;
							formatValue = null;
							%>				
							
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
								%>			
									<div class="input-group-addon"><%=formatValue%></div>		
								<%
								longValue = null;
								formatValue = null;
								%>
								
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
									%>	
													<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width:400px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>			
							</div>
							</div>
							</div>
							<div class="col-md-3"></div>
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Component")%></label>
<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:80px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Premium Status")%></label>
<div class="input-group">
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:190px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.tabledesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabledesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabledesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabledesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>							
</div>
 <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
                              <div class="input-group">
           <%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
                              <div class="input-group">
                              <%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
                              <div class="input-group">
                              <%if ((new Byte((sv.currcode).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcode.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcode.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:40px;" >
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.currds).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currds.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currds.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currds.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
</div>
 <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                             <%--  <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label> --%>
                             	<!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
                              <div class="input-group">
                              <%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%>                                               
							</div>			
</div>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Billed to Date")%></label>
                              <div class="input-group">
                              <%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute","relative")%>                                               
							</div>			
</div>
</div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
                              <div class="input-group">
                              <%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%>                                              
							</div>			
</div>
</div>
</div>
 <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Reassurer")%></label>
                              <div class="input-group">
                              <%if ((new Byte((sv.rasnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rasnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.clntname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
	<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement")%></label>
                              <div class="input-group">
	<%if ((new Byte((sv.rngmnt).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rngmnt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rngmnt.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rngmnt.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.rngmntdsc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rngmntdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rngmntdsc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rngmntdsc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:400px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Reassurance Type")%></label>
                              <div class="input-group">
				<%if ((new Byte((sv.retype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.retype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.retype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.retype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.retypedesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.retypedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.retypedesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.retypedesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:400px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Cession Commencement")%></label>
                               <div class="form-group" style="max-width:120px;">
                             <%=smartHF.getRichText(0, 0, fw, sv.cmdateDisp,(sv.cmdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.cmdateDisp).replace("absolute","relative")%></td>
							</div>
							</div>
<div class="col-md-3">
	                       
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Next Review Date")%></label>
                               <div class="form-group" style="max-width:120px;">
                         <%=smartHF.getRichText(0, 0, fw, sv.rrevdtDisp,(sv.rrevdtDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.rrevdtDisp).replace("absolute","relative")%>
							</div>
							</div>
<div class="col-md-3">
	                       
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Costed to Date")%></label>
                               <div class="form-group" style="max-width:120px;">
                         <%=smartHF.getRichText(0, 0, fw, sv.ctdateDisp,(sv.ctdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ctdateDisp).replace("absolute","relative")%>
							</div>
							</div>
							</div>	
<div class="row">
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Component Sum Assured")%></label>	
   <div class="form-group" style="max-width:120px;">                            <%	
			qpsf = fw.getFieldXMLDef((sv.sumins).getFieldName());
			//ILIFE 1487 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			 valueThis=smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
				%>

<input name='sumins' 
type='text'
<%if((sv.sumins).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=valueThis%>'			 <%
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis 
	//ILIFE 1487 ENDS	
	 %>'
	 <%}%>  
	size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins.getLength(), sv.sumins.getScale(),3)%>'
		maxLength='<%= sv.sumins.getLength()%>' 
		onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumins)' onKeyUp='return checkMaxLength(this)'  



	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumins).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumins).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumins).getColor()== null  ? 
			"input_cell" :  (sv.sumins).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
</div>												
</div>
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Sums Reassured")%></label>	
   <div class="form-group" style="max-width:120px;">  
   <%	
			qpsf = fw.getFieldXMLDef((sv.raAmount).getFieldName());
		//ILIFE 1487 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			 valueThis=smartHF.getPicFormatted(qpsf,sv.raAmount,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='raAmount' 
type='text'
<%if((sv.raAmount).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=valueThis %>'
	<% if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis
		//ILIFE 1487 ENDS
	 %>'
	 <%}%>

	size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.raAmount.getLength(), sv.raAmount.getScale(),3)%>'
		maxLength='<%= sv.raAmount.getLength()%>' 
		onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(raAmount)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.raAmount).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.raAmount).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.raAmount).getColor()== null  ? 
			"input_cell" :  (sv.raAmount).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
</div>
</div>
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage Reassured")%></label>	
   <div class="form-group" style="max-width:120px;"> 
   
	<%	
			qpsf = fw.getFieldXMLDef((sv.reasper).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);

	%>

<input name='reasper' 
type='text'
<%if((sv.reasper).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.reasper) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.reasper);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.reasper) %>'
	 <%}%>

size='<%= sv.reasper.getLength()%>'
maxLength='<%= sv.reasper.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(reasper)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.reasper).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.reasper).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.reasper).getColor()== null  ? 
			"input_cell" :  (sv.reasper).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Recovery Amount")%></label>	
   <div class="form-group" style="max-width:120px;">
   <%	
			qpsf = fw.getFieldXMLDef((sv.recovamt).getFieldName());
			//ILIFE 1487 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.recovamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='recovamt' 
type='text'
<%if((sv.recovamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=valueThis %>'
	<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis 
	 //ILIFE 1487 ENDS
	 %>'
	 <%}%>

	size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.recovamt.getLength(), sv.recovamt.getScale(),3)%>'
		maxLength='<%= sv.recovamt.getLength()%>' 
		onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(recovamt)' onKeyUp='return checkMaxLength(this)'  



	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.recovamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.recovamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.recovamt).getColor()== null  ? 
			"input_cell" :  (sv.recovamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
</div>
</div>
<div class="col-md-3">
 <div class="form-group">	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>	
   <div class="input-group">
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.currdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Latest Cash Call Amt")%></label>	
  <div class="form-group" style="max-width:180px;">                            <%if(((BaseScreenData)sv.callamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.callamt,( sv.callamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.callamt) instanceof DecimalData){
		
	qpsf = fw.getFieldXMLDef((sv.callamt).getFieldName());	
	valueThis=smartHF.getPicFormatted(qpsf,sv.callamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
%>

<input name='callamt' 
type='text'
<%if((sv.callamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=valueThis%>'			 <%
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis 
	//ILIFE 1487 ENDS	
	 %>'
	 <%}%>  
	size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.callamt.getLength(), sv.callamt.getScale(),3)%>'
		maxLength='<%= sv.callamt.getLength()%>' 
		onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(callamt)' onKeyUp='return checkMaxLength(this)'  



	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.callamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.callamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.callamt).getColor()== null  ? 
			"input_cell" :  (sv.callamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>

<!--<%=
	//ILIFE 1487 STARTS
	smartHF.getHTMLVar(0, 0, fw, sv.callamt ,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)
	//ILIFE 1487 ENDS
%>
-->
<%}else {%>
hello
<%}%>
</div>
</div>
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Sum Received to date")%></label>	
   <div class="form-group" style="max-width:180px;">   
	<%	
			qpsf = fw.getFieldXMLDef((sv.callrecd).getFieldName());
			//ILIFE 1487 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.callrecd,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='callrecd' 
type='text'
<%if((sv.callrecd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=valueThis %>'
	<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis 
	 //ILIFE 1487 ENDS
	 %>'
	 <%}%>

	size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.callrecd.getLength(), sv.callrecd.getScale(),3)%>'
		maxLength='<%= sv.callrecd.getLength()%>' 
		onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(callrecd)' onKeyUp='return checkMaxLength(this)' 

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.callrecd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.callrecd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.callrecd).getColor()== null  ? 
			"input_cell" :  (sv.callrecd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>                         
</div>
</div>
<div class="col-md-3">
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Cash Call Date")%></label>	
   <div class="form-group" style="max-width:120px;"> 
   <%=smartHF.getRichText(0, 0, fw, sv.calldtDisp,(sv.calldtDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.calldtDisp).replace("absolute","relative")%>
</div>
</div>
</div>
						</div>															
						</div>
<%@ include file="/POLACommon2NEW.jsp"%>
