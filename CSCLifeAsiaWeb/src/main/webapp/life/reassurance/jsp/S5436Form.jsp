<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5436";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5436ScreenVars sv = (S5436ScreenVars) fw.getVariables();%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurer     ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client        ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex ");%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"DOB ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arrangement");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Curr ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Reassured");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Reassurer")%></label>
					    		     <table><tr><td>
						    			<%					
		if(!((sv.rasnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
		<td>  		
		<%					
		if(!((sv.reasname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:200px; margin-left: 2px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		</tr>
  		</table>
				    		</div>
					</div>
				    		
				    		
				   
		    </div>
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		     <table><tr><td>
						    		
	<%					
		if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
		<td>		 		
		<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:200px; margin-left: 2px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	  </td></tr></table>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
							<div class="input-group">
						    	<%					
		if(!((sv.cltsex.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("DOB")%></label>
							<div class="input-group">
						    		
	<%					
		if(!((sv.cltdobDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 71px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
										
						</div>
				   </div>	
		    </div>
				
			    <div class="row">
			<div class="col-md-12" style="width:100%">
				<div class="form-group">
					
					<div class="table-responsive">
	         <table  id='dataTables-s5436' class="table table-striped table-bordered table-hover"  >
               <thead>
		
			        <tr class="info">
			        
			        <th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Arrangement")%></th>
		         								
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
					
					
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
					<th style="min-width:150px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Reassured")%></th>
					
			       
			     	 
		 	        </tr>
			 </thead>
			 

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[5];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Arrangement").length() >= (sv.rngmnt.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Arrangement").length())*12;								
			} else {		
				calculatedValue = (sv.rngmnt.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Contract").length() >= (sv.chdrnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Contract").length())*12;								
			} else {		
				calculatedValue = (sv.chdrnum.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Type").length() >= (sv.retype.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Type").length())*12;								
			} else {		
				calculatedValue = (sv.retype.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Currency").length() >= (sv.currency.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Currency").length())*12;								
			} else {		
				calculatedValue = (sv.currency.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Sum Reassured").length() >= (sv.raAmount.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Sum Reassured").length())*12;								
			} else {		
				calculatedValue = (sv.raAmount.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s5436screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
<%
	S5436screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5436screensfl
	.hasMoreScreenRows(sfl)) {	
%>
		
<!-- <script>
	$(document).ready(function() {
    	$('#dataTables-s5436').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    	

    });
</script> -->



<tbody>

<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																
									
											
						<%= sv.rngmnt.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																
									
											
						<%= sv.chdrnum.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="left">									
																
									
											
						<%= sv.retype.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="left">									
																
									
											
						<%= sv.currency.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.raAmount).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.raAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!sv.
							raAmount
							.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5436screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

		</table>
		</div></div></div></div>

				<script>
	$(document).ready(function() {
    	$('#dataTables-s5436').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
        	info: false,
    		paging: false,
      	});

    });
</script>			
			    	
			    	
			    	
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->

<%@ include file="/POLACommon2NEW.jsp"%>

