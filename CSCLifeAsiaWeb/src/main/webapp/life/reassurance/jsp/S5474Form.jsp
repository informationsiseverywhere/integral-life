

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5474";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5474ScreenVars sv = (S5474ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Cession Maintenance Transactions");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No     ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
<div class="panel-heading">
<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
</div>
<div class="panel-body">
				<div class="row">
						<div class="col-md-3"> 
						    		<div class="form-group">  	  
							    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
						<div class="input-group">
						<%if ((new Byte((sv.chdrsel).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						<%
						
							longValue = sv.chdrsel.getFormData();
						%>
						
						<%
							if((new Byte((sv.chdrsel).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
						%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
													"blank_cell" : "output_cell" %>'>
							   		<%if(longValue != null){%>
						
							   		<%=XSSFilter.escapeHtml(longValue)%>
						
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%>
						<input name='chdrsel' id='chdrsel'
						type='text'
						value='<%=sv.chdrsel.getFormData()%>'
						maxLength='<%=sv.chdrsel.getLength()%>'
						size='<%=sv.chdrsel.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'
						
						<%
							if((new Byte((sv.chdrsel).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
						%>
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.chdrsel).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						
						<span class="input-group-btn">
		        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
		        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		        			</button>
		      			</span>
						
						
						
						<%
							}else {
						%>
						
						class = ' <%=(sv.chdrsel).getColor()== null  ?
						"input_cell" :  (sv.chdrsel).getColor().equals("red") ?
						"input_cell red reverse" : "input_cell" %>' >
						<!-- ILIFE2448 starts -->
						<span class="input-group-btn">
					        <button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        </button>
						</span>
						<!-- ILIFE2448 ends -->
						<%}longValue = null;}} %>
						
						</div>
						</div>
				</div>
				<div class="col-md-4"></div>
	<div class="col-md-3"> 
						    		<div class="form-group">  	  
							    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>


<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datefrmDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

</div></div>
</div></div></div>
<div class="panel panel-default">
							<div class="panel-heading">
							<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
							</div>
							<div class="panel-body">
								<div class="row">
								<div class="col-md-4"> 
									<div class="radioButtonSubmenuUIG">
									
									<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
									<%=resourceBundleHandler.gettingValueFromBundle("Cession Maintenance Transactions")%></label>
									</div>
								</div>
						</div>
					</div>
			</div>
			

<%@ include file="/POLACommon2NEW.jsp"%>

