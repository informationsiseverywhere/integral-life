
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH618";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%Sh618ScreenVars sv = (Sh618ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurance Risk Class  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2 ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3 ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4 ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"5 ");%>
<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                     <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
								<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						 </div>
	                  </div>
	                    <!-- <div class="col-md-1">
	                    </div> -->
	                  <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
								<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						 </div>
	                  </div>
	                   <!--  <div class="col-md-1">
	                    </div> -->
	                  <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                         <!--  <div class="input-group"> -->
	                         <table><tr><td>
							
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  						<!-- </div> -->
  						</td></tr></table>
						 </div>
	                  </div>
	               </div>
	               
	                <div class="row">
                     <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Reassurance Risk Class")%></label>
	                        </div>
	                  </div><div class="col-md-4"></div><div class="col-md-4"></div>
	               </div>
	                       
	                       <div class="row">
                     		<div class="col-md-2">
	                        <div class="form-group">
	                       <div class="input-group" style="min-width:71px;">
	                       <%	
	longValue = sv.lrkcls01.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls01' 
id='lrkcls01' 
type='text' 
value='<%=sv.lrkcls01.getFormData()%>' 
maxLength='<%=sv.lrkcls01.getLength()%>' 
size='<%=sv.lrkcls01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls01)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
 <!-- ILIFE-904 Start -- Screen Sh618 has UI issues - Field Alignment issue -->
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.lrkcls01).getColor()== null  ? 
"input_cell" :  (sv.lrkcls01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       
	                       </div>
	                  </div>
	               </div>
	               
	               <div class="col-md-2">
	                     <div class="form-group">
	                       <div class="input-group" style="min-width:71px;">
	                       <%	
	longValue = sv.lrkcls02.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls02' 
id='lrkcls02'
type='text' 
value='<%=sv.lrkcls02.getFormData()%>' 
maxLength='<%=sv.lrkcls02.getLength()%>' 
size='<%=sv.lrkcls02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.lrkcls02).getColor()== null  ? 
"input_cell" :  (sv.lrkcls02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       
	                        </div>
	                  </div>
	               </div>
	                 <div class="col-md-2">
	                     <div class="form-group">
	                       <div class="input-group" style="min-width:71px;">
	                       <%	
	longValue = sv.lrkcls03.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls03' 
id='lrkcls03' 
type='text' 
value='<%=sv.lrkcls03.getFormData()%>' 
maxLength='<%=sv.lrkcls03.getLength()%>' 
size='<%=sv.lrkcls03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls03)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.lrkcls03).getColor()== null  ? 
"input_cell" :  (sv.lrkcls03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       
	                        </div>
	                  </div>
	               </div>
	                 <div class="col-md-2">
	                     <div class="form-group">
	                       <div class="input-group" style="min-width:71px;">
	                       <%	
	longValue = sv.lrkcls04.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls04' 
 id='lrkcls04' 
type='text' 
value='<%=sv.lrkcls04.getFormData()%>' 
maxLength='<%=sv.lrkcls04.getLength()%>' 
size='<%=sv.lrkcls04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls04)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.lrkcls04).getColor()== null  ? 
"input_cell" :  (sv.lrkcls04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       
	                        </div>
	                  </div>
	               </div>
	               
	                 <div class="col-md-2">
	                     <div class="form-group">
	                       <div class="input-group" style="min-width:71px;">
	                       <%	
	longValue = sv.lrkcls05.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls05' 
id='lrkcls05' 
type='text' 
value='<%=sv.lrkcls05.getFormData()%>' 
maxLength='<%=sv.lrkcls05.getLength()%>' 
size='<%=sv.lrkcls05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls05)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.lrkcls05).getColor()== null  ? 
"input_cell" :  (sv.lrkcls05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<!-- ILIFE-904 End -- Screen Sh618 has UI issues - Field Alignment issue -->

<%}longValue = null;} %>
	                       
	                        </div>
	                  </div>
	               </div>
	<div class="col-md-2"></div>      </div>
</div>
	             
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

