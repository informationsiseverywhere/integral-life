

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5434";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5434ScreenVars sv = (S5434ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Company Exposure");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Reassurer Exposure");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action               ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client No            ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurer            ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Action B only)");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Class           ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Action A only)");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arrangement          ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Action B only)");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.clttwo.setReverse(BaseScreenData.REVERSED);
			sv.clttwo.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.clttwo.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.rasnum.setReverse(BaseScreenData.REVERSED);
			sv.rasnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.rasnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.lrkcls.setReverse(BaseScreenData.REVERSED);
			sv.lrkcls.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.lrkcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rngmnt.setReverse(BaseScreenData.REVERSED);
			sv.rngmnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rngmnt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
					    		<div class="input-group">
						    		<input name='clttwo' id='clttwo'
type='text' 
value='<%=sv.clttwo.getFormData()%>' 
maxLength='<%=sv.clttwo.getLength()%>' 
size='<%=sv.clttwo.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(clttwo)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.clttwo).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.clttwo).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
 

<%
	}else { 
%>

class = ' <%=(sv.clttwo).getColor()== null  ? 
"input_cell" :  (sv.clttwo).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
<%} %>

						    		
				      			</div>
				    		</div>
			    	</div>
			    	
			    	
			    	 <div class="col-md-3">
						<div class="form-group">	</div></div>
			      
			       <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Reassurer")%></label>
							<div class="input-group">
						    		<input name='rasnum' id='rasnum'
type='text' 
value='<%=sv.rasnum.getFormData()%>' 
maxLength='<%=sv.rasnum.getLength()%>' 
size='<%=sv.rasnum.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rasnum)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rasnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rasnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('rasnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
 

<%
	}else { 
%>

class = ' <%=(sv.rasnum).getColor()== null  ? 
"input_cell" :  (sv.rasnum).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('rasnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
 

<%} %>

						    		

				      			     </div>
						</div>
				   </div>	
			      </div>
			      
			      <div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%></label>
					    		     <div class="input-group">
						    		
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"lrkcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("lrkcls");
	optionValue = makeDropDownList( mappedItems , sv.lrkcls.getFormData(),2,resourceBundleHandler);  
%>
<select name='lrkcls' type='list' style="width:240px;"
<% 
	if((new Byte((sv.lrkcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.lrkcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.lrkcls).getColor()== null  ? 
			"input_cell" :  (sv.lrkcls).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%>
</select>
				      			     </div>
				    		</div>
					</div>
				    		<div class="col-md-3">
						<div class="form-group">	</div></div>
				    		
				    <div class="col-md-5">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement")%></label>
							<div class="input-group">
						    	<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rngmnt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rngmnt");
	optionValue = makeDropDownList( mappedItems , sv.rngmnt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rngmnt.getFormData()).toString().trim()); 
%>
<!-- ILIFE-2646 Life Cross Browser - Sprint 3 D1 : Task 1  -->
 <%=smartHF.getDropDownExt(sv.rngmnt, fw, longValue, "rngmnt", optionValue) %>
 <%-- 
 <select name='rngmnt' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rngmnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rngmnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.rngmnt).getColor()== null  ? 
			"input_cell" :  (sv.rngmnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%> 

 </select> --%> 
<script>
$(document).ready(function(){
	createDropdownNotInTable("rngmnt",5);
	</script> 
	<!-- ILIFE-2646 Life Cross Browser - Sprint 3 D1 : Task 1  -->
						    		

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
			      
			</div>
		</div>
	
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-3">
					<div class="radioButtonSubmenuUIG">

<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Company Exposure")%></label> 
</div>
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<div class="radioButtonSubmenuUIG">

<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
<%=resourceBundleHandler.gettingValueFromBundle("Reassurer Exposure")%></label> 
</div>
				</div>
				
				
			</div>		
							
							
							
										
		</div>
	</div>	





<%@ include file="/POLACommon2NEW.jsp"%>



