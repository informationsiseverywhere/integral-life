<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5443";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5443ScreenVars sv = (S5443ScreenVars) fw.getVariables();%>

<%if (sv.S5443screenWritten.gt(0)) {%>
	<%S5443screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Premium");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bands");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"GST");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate");%>
	<%sv.reprmfrm01.setClassString("");%>
	<%sv.reprmto01.setClassString("");%>
	<%sv.gstrate01.setClassString("");%>
	<%sv.reprmfrm02.setClassString("");%>
	<%sv.reprmto02.setClassString("");%>
	<%sv.gstrate02.setClassString("");%>
	<%sv.reprmfrm03.setClassString("");%>
	<%sv.reprmto03.setClassString("");%>
	<%sv.gstrate03.setClassString("");%>
	<%sv.reprmfrm04.setClassString("");%>
	<%sv.reprmto04.setClassString("");%>
	<%sv.gstrate04.setClassString("");%>
	<%sv.reprmfrm05.setClassString("");%>
	<%sv.reprmto05.setClassString("");%>
	<%sv.gstrate05.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.reprmfrm02.setReverse(BaseScreenData.REVERSED);
			sv.reprmfrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reprmfrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.reprmto02.setReverse(BaseScreenData.REVERSED);
			sv.reprmto02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.reprmto02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.gstrate02.setReverse(BaseScreenData.REVERSED);
			sv.gstrate02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.gstrate02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.reprmfrm01.setReverse(BaseScreenData.REVERSED);
			sv.reprmfrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.reprmfrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.reprmto01.setReverse(BaseScreenData.REVERSED);
			sv.reprmto01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.reprmto01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.gstrate01.setReverse(BaseScreenData.REVERSED);
			sv.gstrate01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.gstrate01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.reprmfrm03.setReverse(BaseScreenData.REVERSED);
			sv.reprmfrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.reprmfrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.reprmto03.setReverse(BaseScreenData.REVERSED);
			sv.reprmto03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.reprmto03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.gstrate03.setReverse(BaseScreenData.REVERSED);
			sv.gstrate03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.gstrate03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.reprmfrm04.setReverse(BaseScreenData.REVERSED);
			sv.reprmfrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.reprmfrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.reprmto04.setReverse(BaseScreenData.REVERSED);
			sv.reprmto04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.reprmto04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.gstrate04.setReverse(BaseScreenData.REVERSED);
			sv.gstrate04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.gstrate04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.reprmfrm05.setReverse(BaseScreenData.REVERSED);
			sv.reprmfrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reprmfrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.reprmto05.setReverse(BaseScreenData.REVERSED);
			sv.reprmto05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.reprmto05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.gstrate05.setReverse(BaseScreenData.REVERSED);
			sv.gstrate05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.gstrate05.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	
	
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    			<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group"style="max-width:100px;">
						    		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
						    			<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				    		</div>
				    		
				    		
				    		<BR>	<BR>
				    		
				 <div class='label_txt'class='label_txt' >Annual&nbsp;&nbsp;Premium&nbsp;&nbsp;Bands</div>
<br>
	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<div class='label_txt'class='label_txt' >From</div>
				    		</div>
					</div>
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<div class='label_txt'class='label_txt' >To</div>
				    		</div>
					</div>
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<div class='label_txt'class='label_txt' >GST Rate</div>
				    		</div>
					</div>
					</div>
				    		
	


	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt( fw, sv.reprmfrm01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
				    		</div>
					</div>
					
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt(fw, sv.reprmto01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
				    		</div>
					</div>
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  
				    		<div class="input-group">  	  
					    	<%=smartHF.getHTMLVarExt( fw, sv.gstrate01, COBOLHTMLFormatter.S2VS2)%>	
				    		</div></div>
					</div>
	</div>
				    		

	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    	<%=smartHF.getHTMLVarExt( fw, sv.reprmfrm02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>	
				    		</div>
					</div>
					
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt( fw, sv.reprmto02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
					    		
				    		</div>
					</div>
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  
				    		<div class="input-group">  	  
					    <%=smartHF.getHTMLVarExt( fw, sv.gstrate02, COBOLHTMLFormatter.S2VS2)%>
				    		</div></div>
					</div>
					
	</div>
				    		
	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt( fw, sv.reprmfrm03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
				    		</div>
					</div>
					
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt( fw, sv.reprmto03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
					    		
				    		</div>
					</div>
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  
				    		<div class="input-group">  	  
					    <%=smartHF.getHTMLVarExt( fw, sv.gstrate03, COBOLHTMLFormatter.S2VS2)%>
				    		</div></div>
					</div>
					
	</div>
	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<%=smartHF.getHTMLVarExt( fw, sv.reprmfrm04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
					    		
				    		</div>
					</div>
					
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<%=smartHF.getHTMLVarExt( fw, sv.reprmto04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS) %>
					    		
				    		</div>
					</div>
					
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  
				    		<div class="input-group">  	  
					    <%=smartHF.getHTMLVarExt( fw, sv.gstrate04, COBOLHTMLFormatter.S2VS2)%>
				    		</div></div>
					</div>
	</div>



				    		

	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<%=smartHF.getHTMLVarExt( fw, sv.reprmfrm05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					    		
				    		</div>
					</div>
					
					<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    			<%=smartHF.getHTMLVarExt( fw, sv.reprmto05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					    		
				    		</div>
					</div>
					
					<div class="col-md-1"> </div>
					<div class="col-md-3"> 
				    		<div class="form-group">  
				    		<div class="input-group">  	  
					    <%=smartHF.getHTMLVarExt( fw, sv.gstrate05, COBOLHTMLFormatter.S2VS2)%>
				    		</div></div>
					</div>
	</div>
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->

<%}%>

<%if (sv.S5443protectWritten.gt(0)) {%>
	<%S5443protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
  <style>
div[id*='reprmt0']{padding-right:2px !important} 
div[id*='reprmfrm']{padding-right:2px !important}
div[id*='gstrate']{padding-right:2px !important}
</style>  
  

<%@ include file="/POLACommon2NEW.jsp"%>