<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5595";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*"%>

<%
	S5595ScreenVars sv = (S5595ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5595screenWritten.gt(0)) {
%>
<%
	S5595screen.clearClassString(sv);
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		}
%>

<%=smartHF.getLit(23, 4, generatedText15)%>


<%
	}
%>

<%-- <%
	if (sv.S5595screensflWritten.gt(0)) {
%> --%>
<%
	GeneralTable sfl = fw.getTable("s5595screensfl");
		savedInds = appVars.saveAllInds();
		S5595screensfl.set1stScreenRow(sfl, appVars, sv);
		double sflLine = 0.0;
		int doingLine = 0;
		int sflcols = 1;
		int linesPerCol = 14;
		String height = smartHF.fmty(14);
		smartHF.setSflLineOffset(7);
		
		int count=1;  //ILIFE-6026
%>
<%-- <div
	style='position: absolute; left: 0%; top: <%=smartHF.fmty(7)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%
		while (S5595screensfl.hasMoreScreenRows(sfl)) {
	%>
	<%
		sv.slt.setClassString("");
	%>
	<%
		sv.slt.appendClassString("string_fld");
				sv.slt.appendClassString("input_txt");
				sv.slt.appendClassString("highlight");
	%>
	<%
		sv.rasnum.setClassString("");
	%>
	<%
		sv.rasnum.appendClassString("string_fld");
				sv.rasnum.appendClassString("output_txt");
				sv.rasnum.appendClassString("highlight");
	%>
	<%
		sv.validflag.setClassString("");
	%>
	<%
		sv.validflag.appendClassString("string_fld");
				sv.validflag.appendClassString("output_txt");
				sv.validflag.appendClassString("highlight");
	%>
	<%
		sv.rapaymth.setClassString("");
	%>
	<%
		sv.rapaymth.appendClassString("string_fld");
				sv.rapaymth.appendClassString("output_txt");
				sv.rapaymth.appendClassString("highlight");
	%>
	<%
		sv.rapayfrq.setClassString("");
	%>
	<%
		sv.rapayfrq.appendClassString("string_fld");
				sv.rapayfrq.appendClassString("output_txt");
				sv.rapayfrq.appendClassString("highlight");
	%>
	<%
		sv.currcode.setClassString("");
	%>
	<%
		sv.currcode.appendClassString("string_fld");
				sv.currcode.appendClassString("output_txt");
				sv.currcode.appendClassString("highlight");
	%>
	<%
		sv.agntnum.setClassString("");
	%>
	<%
		sv.agntnum.appendClassString("string_fld");
				sv.agntnum.appendClassString("output_txt");
				sv.agntnum.appendClassString("highlight");
	%>
	<%
		sv.minsta.setClassString("");
	%>
	<%
		sv.minsta.appendClassString("num_fld");
				sv.minsta.appendClassString("output_txt");
				sv.minsta.appendClassString("highlight");
	%>
	<%
		sv.screenIndicArea.setClassString("");
	%>

	<%
		{
				}
	%>

	<%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.slt)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 9, sfl, sv.rasnum)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 9, sfl, sv.rasnum)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 23, sfl, sv.validflag)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 30, sfl, sv.rapaymth)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 30, sfl, sv.rapaymth)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 36, sfl, sv.rapayfrq)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 36, sfl, sv.rapayfrq)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 43, sfl, sv.currcode)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 43, sfl, sv.currcode)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 50, sfl, sv.agntnum)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 62, sfl, sv.minsta,
							COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%
		sflLine += 1;
				doingLine++;
				if (doingLine % linesPerCol == 0 && sflcols > 1) {
					sflLine = 0.0;
				}
				S5595screensfl.setNextScreenRow(sfl, appVars, sv);
			}
	%>
</div> --%>
<%
	appVars.restoreAllInds(savedInds);
%>

<%-- 
<%
	}
%> --%>

<%
	if (sv.S5595protectWritten.gt(0)) {
%>
<%
	S5595protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>

<%
	if (sv.S5595screenctlWritten.gt(0)) {
%>
<%
	S5595screenctl.clearClassString(sv);
%>
<%-- <%
	GeneralTable sfl = fw.getTable("s5595screensfl");
%> --%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client ");
%>
<%
	sv.clntnum.setClassString("");
%>
<%
	sv.clntnum.appendClassString("string_fld");
		sv.clntnum.appendClassString("output_txt");
		sv.clntnum.appendClassString("highlight");
%>
<%
	sv.cname.setClassString("");
%>
<%
	sv.cname.appendClassString("string_fld");
		sv.cname.appendClassString("output_txt");
		sv.cname.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reassurer");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Payment");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Minimum");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sel");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Number");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Flag");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Meth");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Freq");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Curr");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Statement Amnt");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>
<%
	sv.subfilePosition.setClassString("");
%>

<%
	{
			appVars.rollup(new int[] { 93 });
		}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label></td>
							<td>&nbsp;</td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.clntnum)%>
							</td>
							<td>
							<div style="margin-left: 1px !important;max-width: 330px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cname)%></div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5679' width='100%'>
						<thead>
							<tr class='info'>
								<th></th>
								<th><%=generatedText13%></th>
								<th><%=generatedText4%></th>
								<th colspan="4"><%=generatedText6%></th>
								<th><%=generatedText11%></th>
							</tr>
							<tr class='info'>
								<th><%=generatedText2%></th>
								<th><%=generatedText3%></th>
								<th><%=generatedText5%></th>
								<th><%=generatedText7%></th>
								<th><%=generatedText8%></th>
								<th><%=generatedText9%></th>
								<th><%=generatedText10%></th>
								<th><%=generatedText12%></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		
		</div>
		
	<%
	}
%>	
		
		
	<%
		if (sv.S5595screensflWritten.gt(0)) {
	%>	
		<div>
		
		
		
	<%
		while (S5595screensfl.hasMoreScreenRows(sfl)) {
	%>
	<%
		sv.slt.setClassString("");
	%>
	<%
		sv.slt.appendClassString("string_fld");
				sv.slt.appendClassString("input_txt");
				sv.slt.appendClassString("highlight");
	%>
	<%
		sv.rasnum.setClassString("");
	%>
	<%
		sv.rasnum.appendClassString("string_fld");
				sv.rasnum.appendClassString("output_txt");
				sv.rasnum.appendClassString("highlight");
	%>
	<%
		sv.validflag.setClassString("");
	%>
	<%
		sv.validflag.appendClassString("string_fld");
				sv.validflag.appendClassString("output_txt");
				sv.validflag.appendClassString("highlight");
	%>
	<%
		sv.rapaymth.setClassString("");
	%>
	<%
		sv.rapaymth.appendClassString("string_fld");
				sv.rapaymth.appendClassString("output_txt");
				sv.rapaymth.appendClassString("highlight");
	%>
	<%
		sv.rapayfrq.setClassString("");
	%>
	<%
		sv.rapayfrq.appendClassString("string_fld");
				sv.rapayfrq.appendClassString("output_txt");
				sv.rapayfrq.appendClassString("highlight");
	%>
	<%
		sv.currcode.setClassString("");
	%>
	<%
		sv.currcode.appendClassString("string_fld");
				sv.currcode.appendClassString("output_txt");
				sv.currcode.appendClassString("highlight");
	%>
	<%
		sv.agntnum.setClassString("");
	%>
	<%
		sv.agntnum.appendClassString("string_fld");
				sv.agntnum.appendClassString("output_txt");
				sv.agntnum.appendClassString("highlight");
	%>
	<%
		sv.minsta.setClassString("");
	%>
	<%
		sv.minsta.appendClassString("num_fld");
				sv.minsta.appendClassString("output_txt");
				sv.minsta.appendClassString("highlight");
	%>
	<%
		sv.screenIndicArea.setClassString("");
	%>

	<%
		{
				}
	%>

	<%-- <%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.slt)%> --%>
	<%-- <%=smartHF.getHTMLSFSpaceVar(sflLine, 9, sfl, sv.rasnum)%> --%>
	<%-- <%=smartHF.getHTMLF4SSVar(sflLine, 9, sfl, sv.rasnum)%> --%>
	<%-- <%=smartHF.getTableHTMLVarQual(sflLine, 23, sfl, sv.validflag)%> --%>
	<%-- <%=smartHF.getHTMLSFSpaceVar(sflLine, 30, sfl, sv.rapaymth)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 30, sfl, sv.rapaymth)%> --%>
	<%-- <%=smartHF.getHTMLSFSpaceVar(sflLine, 36, sfl, sv.rapayfrq)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 36, sfl, sv.rapayfrq)%> --%>
	<%-- <%=smartHF.getHTMLSFSpaceVar(sflLine, 43, sfl, sv.currcode)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 43, sfl, sv.currcode)%> --%>
<%-- 	<%=smartHF.getTableHTMLVarQual(sflLine, 50, sfl, sv.agntnum)%> --%>
	<%-- <%=smartHF.getTableHTMLVarQual(sflLine, 62, sfl, sv.minsta,
							COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%> --%>
							
							
							
							
		<div class="row">
		
			<div class="col-md-1">
				<div class="form-group">
				<%-- 	<div style="width: 50px !important;"><%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.slt)%></div> --%>
					<%-- <%=smartHF.getRichText(0,0,fw,sv.slt,( sv.slt.getLength()+1),null).replace("absolute","relative")%> --%>
					
					<%=smartHF.getHTMLCheckBoxSFL(sv.slt, "slt", "s5595screensfl", count)%> <!-- ILIFE-6026 -->
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%=smartHF.getHTMLSFSpaceVar(sflLine, 9, sfl, sv.rasnum)%>
					<%=smartHF.getHTMLF4SSVar(sflLine, 9, sfl, sv.rasnum)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getTableHTMLVarQual(sflLine, 23, sfl, sv.validflag)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLSFSpaceVar(sflLine, 30, sfl, sv.rapaymth)%>
					<%=smartHF.getHTMLF4SSVar(sflLine, 30, sfl, sv.rapaymth)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLSFSpaceVar(sflLine, 36, sfl, sv.rapayfrq)%>
					<%=smartHF.getHTMLF4SSVar(sflLine, 36, sfl, sv.rapayfrq)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLSFSpaceVar(sflLine, 43, sfl, sv.currcode)%>
					<%=smartHF.getHTMLF4SSVar(sflLine, 43, sfl, sv.currcode)%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%=smartHF.getTableHTMLVarQual(sflLine, 50, sfl, sv.agntnum)%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%=smartHF.getTableHTMLVarQual(sflLine, 62, sfl, sv.minsta,
							COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				</div>
			</div>
		
		</div>

	<%
		sflLine += 1;
				doingLine++;
				if (doingLine % linesPerCol == 0 && sflcols > 1) {
					sflLine = 0.0;
				}
				S5595screensfl.setNextScreenRow(sfl, appVars, sv);
			}
		count++;  //ILIFE-6026
	%>
</div>
		<%
			}
		%>	
		

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->




<%@ include file="/POLACommon2NEW.jsp"%>
