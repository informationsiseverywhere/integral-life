<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5476";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5476ScreenVars sv = (S5476ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind04.isOn()) {
	sv.trandes.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>
<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
							<table>
							<tr>
							<td>
								<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							</td>
							<td>

								<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
									<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}						
										}else{
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											}else{
												formatValue = formatValue( longValue);
											}
										}
									%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
									style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
								<%}%>
							</td>
							<td>

							<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
							style="margin-left: 1px;max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>	
							</td>
							</tr>
							</table>																		
							
					</div>
				</div>
			<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>							
				 </div>
<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>

<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
	<table>
	<tr>
	<td>
	
	 <%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
style="margin-left: 1px;max-width: 200px;min-width: 100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
</tr>
</table>
</div>
</div>						
<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>	
							<table>
							<tr>
							<td>
							
							
							<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
style="min-width:65px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
style="min-width:100px;margin-left: 1px;max-width: 200px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	</td>
	</tr>
	</table>
	
	</div></div>												
</div>
<div class="row">	
<div class="col-md-2">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Reverse Trans No")%></label>
							<%if ((new Byte((sv.trandes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.trandes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
</div>
<%	appVars.rollup(new int[] {93}); %>
<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table style="white-space: nowrap;"  class="table table-striped table-bordered table-hover" id='dataTables-s5476' width='100%'>
                         	<thead>
                            	<tr class='info'>
                            	<th><%=resourceBundleHandler.gettingValueFromBundle("Plan")%></th>
                            	<th><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Cov")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Rid")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Eff. Date")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Curr")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Cash Call Amount")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Received to Date ")%></th>
		         				</tr></thead>
		         				<tbody>
		         				<%
			GeneralTable sfl = fw.getTable("s5476screensfl");		
	S5476screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5476screensfl
	.hasMoreScreenRows(sfl)) {	
%>	
<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5476screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5476screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="s5476screensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >



<td>
<%--ILIFE-1520 ENDS --%>									
	<%if((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.planSuffix).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0)%>
			<%if((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.planSuffix.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>



<td>									
				<%if((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.life.getFormData();
							%>
					 		<div id="s5476screensfl.life_R<%=count%>" name="s5476screensfl.life_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>									
				<%if((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.coverage.getFormData();
							%>
					 		<div id="s5476screensfl.coverage_R<%=count%>" name="s5476screensfl.coverage_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>									
				<%if((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.rider.getFormData();
							%>
					 		<div id="s5476screensfl.rider_R<%=count%>" name="s5476screensfl.rider_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>				<%if((new Byte((sv.calldtDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
	<%	longValue = sv.calldtDisp.getFormData();  	%>
					<% 
						if((new Byte((sv.calldtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div id="s5476screensfl.calldtDisp_R<%=count%>" name="s5476screensfl.calldtDisp_R<%=count%>" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "input_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='<%="s5476screensfl" + "." +
					 "calldtDisp" + "_R" + count %>'
					id='<%="s5476screensfl" + "." +
					 "calldtDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.calldtDisp.getFormData() %>' 
					class = " <%=(sv.calldtDisp).getColor()== null  ? 
					"input_cell" :
					(sv.calldtDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.calldtDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(s5476screensfl.calldtDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					<%-- ILIFE-1520 STARTS by smahalaxmi --%>
					style = "width: <%=sv.calldtDisp.getLength()*7%> px;"	
					>
					<a href="javascript:;" 
					onClick="showCalendar(this, document.getElementById('<%="s5476screensfl" + "." +
					 "calldtDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
					 <!-- ILIFE-1268 by smalchi2 STARTS-->
					<img style="margin-left: 0px; margin-top: 0px" src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
					<!-- ENDS -->
					<%-- ILIFE-1520 ENDS --%>
					</a>
					<%}%>  			
		<%}%>
</td>



<td>									
				<%if((new Byte((sv.currcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.currcode.getFormData();
							%>
					 		<div id="s5476screensfl.currcode_R<%=count%>" name="s5476screensfl.currcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

<!-- ILIFE-1268 smalchi2 added the padding property for the input tag. -->
<td>									
			<%if((new Byte((sv.callamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
	<!-- ILIFE-1520 ENDS -->
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.callamt).getFieldName());							
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.callamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);							
						%>					
						<% 
					if((new Byte((sv.callamt).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<div id="s5476screensfl.callamt_R<%=count%>" name="s5476screensfl.callamt_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>  
	   										<%if(formatValue != null){%>
	   										
	   		 											<%=XSSFilter.escapeHtml(formatValue)%>
	   		 							
	   		
	   										<%}%>
	   				</div>

						
							<% }else {%>
						
						
						<input type='text' 
						 value='<%=formatValue%>' 
						 <%if (qpsf.getDecimals() > 0) {%>
							size='<%=sv.callamt.getLength()+1%>'
							maxLength='<%=sv.callamt.getLength()+1%>' 
						<%}else{%>
							size='<%=sv.callamt.getLength()%>'
							maxLength='<%=sv.callamt.getLength()%>' 
						<%}%>
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5476screensfl.callamt)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5476screensfl" + "." +
						 "callamt" + "_R" + count %>'
						 id='<%="s5476screensfl" + "." +
						 "callamt" + "_R" + count %>'
						class = " <%=(sv.callamt).getColor()== null  ? 
						"input_cell" :  
						(sv.callamt).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						  style = "width: <%=sv.callamt.getLength()*12%>px; text-align='right';" 
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
					 <%}%>
										
					
											
									<%}%>
			 			</td>


<td>									
			<%if((new Byte((sv.callrecd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
<%--ILIFE-1520 ENDS --%>	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.callrecd).getFieldName());
							//ILIFE 1487 STARTS
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.callrecd,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							//ILIFE 1487 ENDS							
						%>					
						<% 
					if((new Byte((sv.callrecd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<div id="s5476screensfl.callrecd_R<%=count%>" name="s5476screensfl.callrecd_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>  
	   										<%if(formatValue != null){%>
	   										
	   		 											<%=XSSFilter.escapeHtml(formatValue)%>
	   		 							
	   		
	   										<%}%>
	   				</div>

						
							<% }else {%>
						
						
						<input type='text' 
						 value='<%=formatValue%>' 
						 <%if (qpsf.getDecimals() > 0) {%>
							size='<%=sv.callrecd.getLength()+1%>'
							maxLength='<%=sv.callrecd.getLength()+1%>' 
						<%}else{%>
							size='<%=sv.callrecd.getLength()%>'
							maxLength='<%=sv.callrecd.getLength()%>' 
						<%}%>
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5476screensfl.callrecd)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5476screensfl" + "." +
						 "callrecd" + "_R" + count %>'
						 id='<%="s5476screensfl" + "." +
						 "callrecd" + "_R" + count %>'
						class = " <%=(sv.callrecd).getColor()== null  ? 
						"input_cell" :  
						(sv.callrecd).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						  style = "width: <%=sv.callrecd.getLength()*12%>px; text-align='right';"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
					 <%}%>
										
					
											
									<%}%>
			 			</td>
<!-- ENDS -->


</tr>
<%	count = count + 1;
S5476screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</tbody>
</table>
</div>
</div>
</div>														 
 </div>                    
 </div>	


<INPUT type="HIDDEN" name="screenIndicArea" id="screenIndicArea" value="<%=	(sv.screenIndicArea.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="subfilePosition" id="subfilePosition" value="<%=	(sv.subfilePosition.getFormData()).toString() %>" >

<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<script>
$(document).ready(function(){
	$('#dataTables-s5476').DataTable({
		ordering: false,
		searching: false,
		scrollX:true,
		scrollY: "390px",
		scrollCollapse:true,
		
	});
	
});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
