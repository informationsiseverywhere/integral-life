
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5459";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5459ScreenVars sv = (S5459ScreenVars) fw.getVariables();%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component Sum Assured ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Reassured ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comm. Date");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurer");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arr. Code");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Reassured");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"O/R");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind02.isOn()) {
			sv.sumins02.setReverse(BaseScreenData.REVERSED);
			sv.sumins02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.sumins02.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-6">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<div class="input-group">
								<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
								%>			
									<input class="form-control" type="text" placeholder='<%=formatValue%>' disabled>
								<%
								longValue = null;
								formatValue = null;
								%>							
							
							
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
								%>			
									<div class="input-group-addon"><%=formatValue%></div>
								<%
								longValue = null;
								formatValue = null;
								%>	
									
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
								%>			
									<div class="input-group-addon" style="max-width:400px;"><%=formatValue%></div>
								<%
								longValue = null;
								formatValue = null;
								%>																					
							</div>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label> 
                              <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%> 
  </div>
  </div>
  <div class="col-md-2"></div>
  <div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
                              <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%> 
  </div>
  </div>
</div>
<div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></label>
                              <%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</div>
	</div>
	<div class="col-md-6"></div>
<div class="col-md-2">
	                   <div class="form-group" style="max-width:100px;">
                              <label style="min-width:220px;"><%=resourceBundleHandler.gettingValueFromBundle("Component Sum Assured")%></label>
            	
		
<%if ((new Byte((sv.sumins01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.sumins01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumins01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.sumins01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	</div>
	</div>
<div class="col-md-1"></div>	
<div class="col-md-2">
	                   <div class="form-group" style="max-width:120px;">
                              <label style="min-width:220px;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Reassured")%></label>
          <%if ((new Byte((sv.sumins02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.sumins02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumins02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.sumins02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 </div>
 </div>                    
 </div>	
 <div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
                         	<thead>
                            	<tr class='info'>
                            	<th><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
		         								
					<th><%=resourceBundleHandler.gettingValueFromBundle("Commencement Date")%></th>
	
					<th><%=resourceBundleHandler.gettingValueFromBundle("Reassurer")%></th>
					
					<th><%=resourceBundleHandler.gettingValueFromBundle("Arrangement Code")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Sum Reassured")%></th>
						<th><%=resourceBundleHandler.gettingValueFromBundle("Override Indicator")%></th>
						</tr>
						</thead>
			<%
			GeneralTable sfl = fw.getTable("s5459screensfl");		
	S5459screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5459screensfl
	.hasMoreScreenRows(sfl)) {	
%>	
<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>
										 
					<input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5459screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5459screensfl.select_R<%=count%>'
						 id='s5459screensfl.select_R<%=count%>'
						 onClick="selectedRow('s5459screensfl.select_R<%=count%>')"
						 class="UICheck"
					 />
					 				
									</td>
		<%}else{%>
												<td>
					</td>														
										
					<%}%>
								<%if((new Byte((sv.cmdateDisp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td><%= sv.cmdateDisp.getFormData()%></td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.rasnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
							
											
						<%= sv.rasnum.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.rngmnt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
								
											
						<%= sv.rngmnt.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.raAmount).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.raAmount).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.raAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.raAmount).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.ovrdind).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
								
									
											
						<%= sv.ovrdind.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	S5459screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	
	</table>
	</div>
	</div>
	</div>	
	<!-- ILIFE-8217 -->
	<div class="row">        
				<div class="col-md-12">
					<div class="form-group">
					<a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
						
					<a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(3)"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>

					 </div>
					</div>
				</div>
			
</div>
</div>
<script>
	$(document).ready(function() {
    	$('#s5466Table').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    	$('#load-more').appendTo($('.col-sm-6:eq(-1)'));

    });
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

