

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5441";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5441ScreenVars sv = (S5441ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction Date Range  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date From)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date To)");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
					    		  <%} %>   <div class="input-group"style="max-width:100px;">
						    		
<%if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
<%if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	




	
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
							 <%} %><div class="input-group"style="max-width:100px;">
						    		
<%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	





<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"style="max-width:100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
						 <%} %>	<div class="input-group">
						    		
<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
				    		<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
					    		 <%} %>     <div class="input-group">
						    		
<%if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						 	<div class="input-group">
						    		
<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
				      			     </div>
						</div>
				   </div>		
			<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
						 <div class="input-group">
						    		<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
						    		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				  <div class="row">	
			    	<div class="col-md-8"> 
				    		<div class="form-group">  	
				    		<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction Date Range")%></label>
					    	 <%} %>	     
						    		<TABLE><TR><TD>
						    		<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="datefrmDisp" data-link-format="dd/mm/yyyy">
						    		
						    		<%	
	if ((new Byte((sv.datefrmDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datefrmDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datefrmDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datefrmDisp' 
type='text' 
value='<%=sv.datefrmDisp.getFormData()%>' 
maxLength='<%=sv.datefrmDisp.getLength()%>' 
size='<%=sv.datefrmDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datefrmDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datefrmDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datefrmDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

			                 
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			               	
							
							
 


<%
	}else { 
%>

class = ' <%=(sv.datefrmDisp).getColor()== null  ? 
"input_cell" :  (sv.datefrmDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%} }}%>
						    	</div>	</TD>
						    		<td>  &nbsp; </td>
						    		<TD>  <%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Date To)")%>
</div>
<%}%>
								</TD><td>  &nbsp; </td>
						    		<td>   
			<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy">
						 
<%	
	if ((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datetoDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='datetoDisp' 
type='text' 
value='<%=sv.datetoDisp.getFormData()%>' 
maxLength='<%=sv.datetoDisp.getLength()%>' 
size='<%=sv.datetoDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datetoDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.datetoDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.datetoDisp).getColor()== null  ? 
"input_cell" :  (sv.datetoDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} }}%></td>
						    		
						    		</TR></TABLE>

				      			     </div>
				    		</div>
					</div>
				    		
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->

<%@ include file="/POLACommon2NEW.jsp"%>
