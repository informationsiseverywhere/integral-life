<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5462";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5462ScreenVars sv = (S5462ScreenVars) fw.getVariables();%>

	
<%if (sv.S5462screenWritten.gt(0)) {%>
	<%S5462screen.clearClassString(sv);%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText5)%>


<%}%>

<%if (sv.S5462screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s5462screensfl");
	savedInds = appVars.saveAllInds();
	S5462screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 13;
	String height = smartHF.fmty(13);
	smartHF.setSflLineOffset(9);
	%>
	<div >
	<%while (S5462screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.action.setClassString("");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%sv.shortdesc.setClassString("");%>
<%	sv.shortdesc.appendClassString("string_fld");
	sv.shortdesc.appendClassString("output_txt");
	sv.shortdesc.appendClassString("highlight");
%>
	<%sv.elemdesc.setClassString("");%>
<%	sv.elemdesc.appendClassString("string_fld");
	sv.elemdesc.appendClassString("output_txt");
	sv.elemdesc.appendClassString("highlight");
%>
	<%sv.hrrn01.setClassString("");%>
	<%sv.hrrn02.setClassString("");%>
	<%sv.hrrn03.setClassString("");%>
	<%sv.hcrtable.setClassString("");%>
	<%sv.hcallind.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
 

		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S5462screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.S5462protectWritten.gt(0)) {%>
	<%S5462protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S5462screenctlWritten.gt(0)) {%>
	<%S5462screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5462screensfl");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No   ");%>
	<%sv.chdrsel.setClassString("");%>
<%	sv.chdrsel.appendClassString("string_fld");
	sv.chdrsel.appendClassString("output_txt");
	sv.chdrsel.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1-Create 2-Post Receipts 3-Enquire");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act Life Cov Rider  Component Details");%>
<%	generatedText2.appendClassString("subfile_hdg");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>
<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
<table>
<tr>
<td>
					<%					
		if(!((sv.chdrsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>						</td>
		</tr>
		</table>		
							</div>
							</div>
							</div>
<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-s5462">
                         	<thead>
                            <tr class='info'>
                            	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Component Details")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("    ")%></th>
							</tr>
							</thead>
<%	
	S5462screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5462screensfl
	.hasMoreScreenRows(sfl)) {	
%>	
<tr class="tableRowTag" id='<%="tablerow"+count%>' >
		<td>
		<%if (null != sv.coverage && !sv.coverage.equals("")) {%>														
						 <input type="radio" 
						 value='<%= sv.action.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5462screensfl" + "." +
						 "action")' onKeyUp='return checkMaxLength(this)' 
						 name='s5462screensfl.action_R<%=count%>'
						 id='s5462screensfl.action_R<%=count%>'
						 onClick="selectedRow('s5462screensfl.action_R<%=count%>')"				 
						 class="radio"/>
<!-- ILIFE-740 End	 -->
		<%} %>
		</td>
		<td>									
			<%if (null == sv.coverage || sv.coverage.equals("")) {%>											
				<%= sv.life%>
			<%} %>
		</td>
		<td>									
			<%= sv.coverage%>
		</td>
		<td>									
			<%if (null != sv.rider && !sv.rider.equals("00")) {%>											
				<%= sv.rider%>
			<%} %>							
		</td>
		<td>									
			<%= sv.shortdesc%>
		</td>
		<td>									
			<%= sv.elemdesc%>
		</td>									
	</tr>
	<%
		
		count = count + 1;
		S5462screensfl.setNextScreenRow(sfl, appVars, sv);
	}
	}%>
	</table>
	</div>
	</div>
	</div>
	<div class="row">        
				<div class="col-md-6">
					
						<button type='button' class='btn btn-info' onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Create")%></button>
						
						<button type='button' class='btn btn-info' onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Post Receipts")%></button>
						
						<button type='button' class='btn btn-info' onClick="JavaScript:perFormOperation(3)"><%=resourceBundleHandler.gettingValueFromBundle("Enquire")%></button>
					
				</div>
				</div>
	</div>
	</div>
	<script>
       $(document).ready(function() {
       $('#dataTables-s5462').DataTable({
             ordering: false,
             searching:false,
             scrollY: "385px",
             scrollCollapse:true,
             paging:false,
             info:false
      });
 
    });
</script> 
	
<%@ include file="/POLACommon2NEW.jsp"%>
