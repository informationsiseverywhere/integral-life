

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5451";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5451ScreenVars sv = (S5451ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Ratings ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age Addition ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Load %       ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate Adjust  ");%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                              <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-3">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>

  </div>
<div class="row">
	<div class="col-md-3">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
</div>
<div class="panel panel-default">
<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Maximum Ratings")%></div>

    	<div class="panel-body">
    	<div class="row">
	<div class="col-md-2">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Age Addition")%></label>
              <%	
			qpsf = fw.getFieldXMLDef((sv.agerate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='agerate' 
type='text'

<%if((sv.agerate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.agerate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.agerate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.agerate) %>'
	 <%}%>

size='<%= sv.agerate.getLength()%>'
maxLength='<%= sv.agerate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(agerate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.agerate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.agerate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.agerate).getColor()== null  ? 
			"input_cell" :  (sv.agerate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Load %")%></label>
              <%	
			qpsf = fw.getFieldXMLDef((sv.oppc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='oppc' 
type='text'

<%if((sv.oppc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.oppc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.oppc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.oppc) %>'
	 <%}%>

size='<%= sv.oppc.getLength()%>'
maxLength='<%= sv.oppc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(oppc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.oppc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.oppc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.oppc).getColor()== null  ? 
			"input_cell" :  (sv.oppc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjust")%></label>
              <%	
			qpsf = fw.getFieldXMLDef((sv.insprm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='insprm' 
type='text'

<%if((sv.insprm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.insprm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.insprm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.insprm) %>'
	 <%}%>

size='<%= sv.insprm.getLength()%>'
maxLength='<%= sv.insprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(insprm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.insprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.insprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.insprm).getColor()== null  ? 
			"input_cell" :  (sv.insprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
  </div>
  </div>
</div>
  </div>
<%@ include file="/POLACommon2NEW.jsp"%>

