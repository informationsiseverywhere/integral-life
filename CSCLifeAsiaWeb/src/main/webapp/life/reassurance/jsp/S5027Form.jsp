

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5027";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5027ScreenVars sv = (S5027ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                         ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"          ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"          ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"               ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"          ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
					    		
					    		 <table><tr>
					    		 
					    		 <td style="min-width:140px;">
					    		 <div class="input-group">
						    		<input name='bankkey' id='bankkey'
									type='text' 
									value='<%=sv.bankkey.getFormData()%>' 
									maxLength='<%=sv.bankkey.getLength()%>' 
									size='<%=sv.bankkey.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  
									
									
									<% 
									
									if((new Byte((sv.bankkey).getEnabled()))
									
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
									%>  
									readonly="true"
									class="output_cell"	 >
									
									<%
										}else if((new Byte((sv.bankkey).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										
									%>	
									class="bold_cell" >
									 
									<span class="input-group-btn">
																<button class="btn btn-info" style="font-size: 19px;"
																	type="button"
																	onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
																	<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
																</button>
															</span>
									<%
										}else { 
									%>
									
									class = ' <%=(sv.bankkey).getColor()== null  ? 
									"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>' >
									
									
									<span class="input-group-btn">
																<button class="btn btn-info" style="font-size: 19px;"
																	type="button"
																	onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
																	<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
																</button>
															</span>
									
									
									<%} %>

							</div>
								</td>



							<td style="min-width: 71px; padding-right: 2px; padding-left: 5px;">
						  		
								<%					
								if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %> '>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							</td>

							<td style="min-width: 71px;">
									<%					
									if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %> ' >
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  

				      			   </td></tr></table>
				    		</div>
					</div>
				</DIV>
				    		
				    		
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Account")%></label>
					    			 <table><tr><td>
					    			 <div class="form-group">
					    			 <%
                                         if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 200px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.bankacckey)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 200px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankacckey)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
					    			 
					    			 
					    			 
					    		</div> 
					    			 
					    			 
					    			 
					    			 
					    			 
					    			 
					    			 
					    			 
					    			 
						    		 <%-- <input name='bankacckey' 
type='text' 
value='<%=sv.bankacckey.getFormData()%>' 
maxLength='<%=sv.bankacckey.getLength()%>' 
size='<%=sv.bankacckey.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(bankacckey)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.bankacckey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.bankacckey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('bankacckey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.bankacckey).getColor()== null  ? 
"input_cell" :  (sv.bankacckey).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('bankacckey')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %> --%>


</td><td style="
    padding-left: 5px;
    min-width: 71px;
">

<div class="form-group">

	
  		
		<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> '>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
		</div>

				      			   </td></tr></table>
				    		</div>
					</div>
				</DIV>
				   
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->




<%@ include file="/POLACommon2NEW.jsp"%>

