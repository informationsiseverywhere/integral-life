

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5473";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5473ScreenVars sv = (S5473ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction Date Range  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date From)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date To)");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- ILIFE-2650 Life Cross Browser -Coding and UT- Sprint 3 D1: Task5   starts -->
<style>
@media \0screen\,screen\9
{
.iconPos{margin-bottom:1px}
}
</style>
</style>
<!-- ILIFE-2650 Life Cross Browser -Coding and UT- Sprint 3 D1: Task5  ends -->


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
  					<div class="input-group">
  <%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	<%					
		if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
 </div>       
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
        <div class="input-group" style="max-width:60px;">
         <%					
		if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	<%					
		if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
</div>		
</div>	
</div>   
<div class="col-md-1"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label> 
       <div class="form-group" style="max-width:100px;">    <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>                                       
</div>
</div>
<div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
  <div class="form-group" style="max-width:100px;">                               <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>' style="max-width:120px;">
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
        </div>
        </div>
        </div>
        <%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>
<div class="col-md-1"></div>	
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label> 
        <div style="position: relative; margin-left:1px;max-width:100px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div>
	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
	<div class="col-md-2"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label> 
             <div style="position: relative; margin-left:1px;max-width:120px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>                 	
	</div>                             
</div>
</div>
 <div class="row">
 <div class="col-md-4">
	                   <div class="form-group" >
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Transaction Date Range")%></label><br/>
                              <table>
<tr><td>



<%	
	if ((new Byte((sv.datefrmDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datefrmDisp.getFormData();
	
%>

<% 
	if((new Byte((sv.datefrmDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	  </div>

<%
longValue = null;
%>
<% }else {%> 

<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datefrmDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>
</td>
<td>

<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt" >
&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("(Date To)")%>&nbsp;&nbsp;
</div>
<%}%>
</td>

<td>
<%	
	if ((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datetoDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	 

<%
longValue = null;
%>
<% }else {%>
 <div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div></div>


<%} }%>
</td></tr></table>
</div>
</div>
</div>   
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

