<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5446";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5446ScreenVars sv = (S5446ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Retention Currency ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Retention ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Discretionary Retention ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Aggregate to            ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Limit           ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"x");%>

<%!private static class ScreenDataHelper {

		VarModel screen;
		BaseScreenData data;

		ScreenDataHelper(VarModel screen, BaseScreenData data) {
			super();
			if (screen == null)
				throw new AssertionError("Null VarModel");
			if (data == null)
				throw new AssertionError("Null BaseScreenData");
			this.screen = screen;
			this.data = data;
		}

		String value() {
			return data.getFormData().trim();
		}

		boolean isReadonly() {
			return (isProtected() || isDisabled());
		}

		boolean isProtected() {
			return screen.isScreenProtected();
		}

		boolean isDisabled() {
			return data.getEnabled() == BaseScreenData.DISABLED;
		}

		String readonly() {
			if (isProtected())
				return "readonly";
			else
				return "";
		}

		String visibility() {
			if (data.getInvisible() == BaseScreenData.INVISIBLE)
				return "hidden";
			else
				return "visible";
		}

		int length() {
			return value().length();
		}

		String clazz() {
			String clazz = "input_cell";
			if (isProtected())
				clazz = "blank_cell";
			else if (isDisabled())
				clazz = "output_cell";
			if (data.getHighLight() == BaseScreenData.BOLD)
				clazz += " bold_cell";
			if (BaseScreenData.RED.equals(data.getColor()))
				clazz += " red reverse";
			return clazz;
		}

		String selected(String value) {
			if (value().equalsIgnoreCase(value))
				return "selected='selected'";
			else
				return "";
		}

	}%>
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    				
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<!-- <div class="input-group"style="max-width:100px;"> -->
							<table><tr><td>
						    		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td>

<td>
	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  </td></tr></table>
	

				      			    <!--  </div> -->
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style=width:90px;>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <!-- <div class="label_txt"> -->
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
			

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				    <div class="col-md-4"></div><div class="col-md-4"></div>		</div>
				    		
				    		
				    		
				    			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Retention Currency")%></label>
					    		     <div class="input-group"style="max-width:120px">

<%	
	longValue = sv.currcode.getFormData();  
%>

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='currcode' id='currcode'
type='text' 
value='<%=sv.currcode.getFormData()%>' 
maxLength='<%=sv.currcode.getLength()%>' 
size='<%=sv.currcode.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(currcode)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.currcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
				 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	

<%
	}else { 
%>

class = ' <%=(sv.currcode).getColor()== null  ? 
"input_cell" :  (sv.currcode).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>


				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
				    <div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Retention")%></label>
					    		     <div class="input-group">
					    		     
	<%	
			qpsf = fw.getFieldXMLDef((sv.retn).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.retn,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='retn' 
type='text'

<%if((sv.retn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.retn.getLength(), sv.retn.getScale(),3)%>'
maxLength='<%= sv.retn.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(retn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.retn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.retn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.retn).getColor()== null  ? 
			"input_cell" :  (sv.retn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					    		     </div></div>
								   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						  <div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Discretionary Retention")%></label>
					    		     <div class="input-group">
					
	<%	
			qpsf = fw.getFieldXMLDef((sv.discretn).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.discretn,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='discretn' 
type='text'

<%if((sv.discretn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.discretn.getLength(), sv.discretn.getScale(),3)%>'
maxLength='<%= sv.discretn.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(discretn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.discretn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.discretn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.discretn).getColor()== null  ? 
			"input_cell" :  (sv.discretn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
				   </div>	
		    </div></div></div>


	<div class="row">	
			    	
				    <div class="col-md-4">
				    <div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Aggregate to")%></label>
							<div class="input-group"style="max-width:120px">
							
<%	
	longValue = sv.lrkcls.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="min-width:80px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls' id='lrkcls'
type='text' 
value='<%=sv.lrkcls.getFormData()%>' 
maxLength='<%=sv.lrkcls.getLength()%>' 
size='<%=sv.lrkcls.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.lrkcls).getColor()== null  ? 
"input_cell" :  (sv.lrkcls).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>

							
								   </div>	</div></div>	
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Limit")%></label>
							
						   
	<Table><Tr><Td>
	<%	
			qpsf = fw.getFieldXMLDef((sv.maxAmount).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.maxAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='maxAmount' 
type='text'

<%if((sv.maxAmount).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.maxAmount.getLength(), sv.maxAmount.getScale(),3)%>'
maxLength='<%= sv.maxAmount.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(maxAmount)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.maxAmount).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.maxAmount).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.maxAmount).getColor()== null  ? 
			"input_cell" :  (sv.maxAmount).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</td> 


<Td> &nbsp; &nbsp; &nbsp; &nbsp; </Td>

<Td>
<label><%=resourceBundleHandler.gettingValueFromBundle("x")%></label>
</Td>

<Td> &nbsp; &nbsp; &nbsp; &nbsp; </Td>

<td>





	<%	
			qpsf = fw.getFieldXMLDef((sv.fieldseq).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='fieldseq' 
type='text'

<%if((sv.fieldseq).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.fieldseq) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.fieldseq);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.fieldseq) %>'
	 <%}%>

size='<%= sv.fieldseq.getLength()%>'
maxLength='<%= sv.fieldseq.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(fieldseq)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.fieldseq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.fieldseq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.fieldseq).getColor()== null  ? 
			"input_cell" :  (sv.fieldseq).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table>
				      			   
						</div>
				   </div></div>
				   	
				   <div class="row" >
			<%
				ScreenDataHelper duplicateAllowed = new ScreenDataHelper(fw.getVariables(), sv.rskcldisp);
			%>
			<div class="col-md-4"
				style="visibility:<%=duplicateAllowed.visibility()%>">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Display on screen (Y/N)")%></label>
					<div style="width: 70px;">
						<%
							if (duplicateAllowed.isReadonly()) {
						%>
						<input name='rskcldisp' type='text'
							value='<%=duplicateAllowed.value()%>'
							class="<%=duplicateAllowed.clazz()%>" readonly
							onFocus='doFocus(this)' onHelp='return fieldHelp(rskcldisp)'
							onKeyUp='return checkMaxLength(this)'>
						<%
							} else {
						%>
						<select name="rskcldisp" type="list"
							class="<%=duplicateAllowed.clazz()%>">
							<option <%=duplicateAllowed.selected("Y")%> value="Y">Y</option>
							<option <%=duplicateAllowed.selected("N")%> value="N">N</option>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>	   
			</div>
		    </div>
		    
		    </div>

<%@ include file="/POLACommon2NEW.jsp"%>

