<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5449";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5449ScreenVars sv = (S5449ScreenVars) fw.getVariables();%>

<%if (sv.S5449screenWritten.gt(0)) {%>
	<%S5449screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arrangement Type ");%>
	<%sv.retype.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Class ");%>
	<%sv.lrkcls.setClassString("");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arrangement Cession Type ");%>
	<%sv.rtytyp.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Standard Lives Allowed? ");%>
	<%sv.subliv.setClassString("");%>
<%	sv.subliv.appendClassString("string_fld");
	sv.subliv.appendClassString("input_txt");
	sv.subliv.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Standard Limits  ");%>
	<%sv.sslivb.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Refund Basis ");%>
	<%sv.prefbas.setClassString("");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.currcode.setClassString("");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company % Share if Quota Based ");%>
	<%sv.qcoshr.setClassString("");%>
<%	sv.qcoshr.appendClassString("num_fld");
	sv.qcoshr.appendClassString("input_txt");
	sv.qcoshr.appendClassString("highlight");
%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Facultative Schedule Required? ");%>
	<%sv.facsh.setClassString("");%>
<%	sv.facsh.appendClassString("string_fld");
	sv.facsh.appendClassString("input_txt");
	sv.facsh.appendClassString("highlight");
%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Facultative Schedule Letter ");%>
	<%sv.letterType.setClassString("");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reasurer                 Quota %          Retention            Premium Basis          ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1  ");%>
	<%sv.rasnum01.setClassString("");%>
<%	sv.rasnum01.appendClassString("string_fld");
	sv.rasnum01.appendClassString("input_txt");
	sv.rasnum01.appendClassString("highlight");
%>
	<%sv.qreshr01.setClassString("");%>
<%	sv.qreshr01.appendClassString("num_fld");
	sv.qreshr01.appendClassString("input_txt");
	sv.qreshr01.appendClassString("highlight");
%>
	<%sv.relimit01.setClassString("");%>
<%	sv.relimit01.appendClassString("num_fld");
	sv.relimit01.appendClassString("input_txt");
	sv.relimit01.appendClassString("highlight");
%>
	<%sv.rprmmth01.setClassString("");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2  ");%>
	<%sv.rasnum02.setClassString("");%>
<%	sv.rasnum02.appendClassString("string_fld");
	sv.rasnum02.appendClassString("input_txt");
	sv.rasnum02.appendClassString("highlight");
%>
	<%sv.qreshr02.setClassString("");%>
<%	sv.qreshr02.appendClassString("num_fld");
	sv.qreshr02.appendClassString("input_txt");
	sv.qreshr02.appendClassString("highlight");
%>
	<%sv.relimit02.setClassString("");%>
<%	sv.relimit02.appendClassString("num_fld");
	sv.relimit02.appendClassString("input_txt");
	sv.relimit02.appendClassString("highlight");
%>
	<%sv.rprmmth02.setClassString("");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3  ");%>
	<%sv.rasnum03.setClassString("");%>
<%	sv.rasnum03.appendClassString("string_fld");
	sv.rasnum03.appendClassString("input_txt");
	sv.rasnum03.appendClassString("highlight");
%>
	<%sv.qreshr03.setClassString("");%>
<%	sv.qreshr03.appendClassString("num_fld");
	sv.qreshr03.appendClassString("input_txt");
	sv.qreshr03.appendClassString("highlight");
%>
	<%sv.relimit03.setClassString("");%>
<%	sv.relimit03.appendClassString("num_fld");
	sv.relimit03.appendClassString("input_txt");
	sv.relimit03.appendClassString("highlight");
%>
	<%sv.rprmmth03.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4  ");%>
	<%sv.rasnum04.setClassString("");%>
<%	sv.rasnum04.appendClassString("string_fld");
	sv.rasnum04.appendClassString("input_txt");
	sv.rasnum04.appendClassString("highlight");
%>
	<%sv.qreshr04.setClassString("");%>
<%	sv.qreshr04.appendClassString("num_fld");
	sv.qreshr04.appendClassString("input_txt");
	sv.qreshr04.appendClassString("highlight");
%>
	<%sv.relimit04.setClassString("");%>
<%	sv.relimit04.appendClassString("num_fld");
	sv.relimit04.appendClassString("input_txt");
	sv.relimit04.appendClassString("highlight");
%>
	<%sv.rprmmth04.setClassString("");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"5  ");%>
	<%sv.rasnum05.setClassString("");%>
<%	sv.rasnum05.appendClassString("string_fld");
	sv.rasnum05.appendClassString("input_txt");
	sv.rasnum05.appendClassString("highlight");
%>
	<%sv.qreshr05.setClassString("");%>
<%	sv.qreshr05.appendClassString("num_fld");
	sv.qreshr05.appendClassString("input_txt");
	sv.qreshr05.appendClassString("highlight");
%>
	<%sv.relimit05.setClassString("");%>
<%	sv.relimit05.appendClassString("num_fld");
	sv.relimit05.appendClassString("input_txt");
	sv.relimit05.appendClassString("highlight");
%>
	<%sv.rprmmth05.setClassString("");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"6  ");%>
	<%sv.rasnum06.setClassString("");%>
<%	sv.rasnum06.appendClassString("string_fld");
	sv.rasnum06.appendClassString("input_txt");
	sv.rasnum06.appendClassString("highlight");
%>
	<%sv.qreshr06.setClassString("");%>
<%	sv.qreshr06.appendClassString("num_fld");
	sv.qreshr06.appendClassString("input_txt");
	sv.qreshr06.appendClassString("highlight");
%>
	<%sv.relimit06.setClassString("");%>
<%	sv.relimit06.appendClassString("num_fld");
	sv.relimit06.appendClassString("input_txt");
	sv.relimit06.appendClassString("highlight");
%>
	<%sv.rprmmth06.setClassString("");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"7  ");%>
	<%sv.rasnum07.setClassString("");%>
<%	sv.rasnum07.appendClassString("string_fld");
	sv.rasnum07.appendClassString("input_txt");
	sv.rasnum07.appendClassString("highlight");
%>
	<%sv.qreshr07.setClassString("");%>
<%	sv.qreshr07.appendClassString("num_fld");
	sv.qreshr07.appendClassString("input_txt");
	sv.qreshr07.appendClassString("highlight");
%>
	<%sv.relimit07.setClassString("");%>
<%	sv.relimit07.appendClassString("num_fld");
	sv.relimit07.appendClassString("input_txt");
	sv.relimit07.appendClassString("highlight");
%>
	<%sv.rprmmth07.setClassString("");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"8  ");%>
	<%sv.rasnum08.setClassString("");%>
<%	sv.rasnum08.appendClassString("string_fld");
	sv.rasnum08.appendClassString("input_txt");
	sv.rasnum08.appendClassString("highlight");
%>
	<%sv.qreshr08.setClassString("");%>
<%	sv.qreshr08.appendClassString("num_fld");
	sv.qreshr08.appendClassString("input_txt");
	sv.qreshr08.appendClassString("highlight");
%>
	<%sv.relimit08.setClassString("");%>
<%	sv.relimit08.appendClassString("num_fld");
	sv.relimit08.appendClassString("input_txt");
	sv.relimit08.appendClassString("highlight");
%>
	<%sv.rprmmth08.setClassString("");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"9  ");%>
	<%sv.rasnum09.setClassString("");%>
<%	sv.rasnum09.appendClassString("string_fld");
	sv.rasnum09.appendClassString("input_txt");
	sv.rasnum09.appendClassString("highlight");
%>
	<%sv.qreshr09.setClassString("");%>
<%	sv.qreshr09.appendClassString("num_fld");
	sv.qreshr09.appendClassString("input_txt");
	sv.qreshr09.appendClassString("highlight");
%>
	<%sv.relimit09.setClassString("");%>
<%	sv.relimit09.appendClassString("num_fld");
	sv.relimit09.appendClassString("input_txt");
	sv.relimit09.appendClassString("highlight");
%>
	<%sv.rprmmth09.setClassString("");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"10 ");%>
	<%sv.rasnum10.setClassString("");%>
<%	sv.rasnum10.appendClassString("string_fld");
	sv.rasnum10.appendClassString("input_txt");
	sv.rasnum10.appendClassString("highlight");
%>
	<%sv.qreshr10.setClassString("");%>
<%	sv.qreshr10.appendClassString("num_fld");
	sv.qreshr10.appendClassString("input_txt");
	sv.qreshr10.appendClassString("highlight");
%>
	<%sv.relimit10.setClassString("");%>
<%	sv.relimit10.appendClassString("num_fld");
	sv.relimit10.appendClassString("input_txt");
	sv.relimit10.appendClassString("highlight");
%>
	<%sv.rprmmth10.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.retype.setReverse(BaseScreenData.REVERSED);
			sv.retype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.retype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.lrkcls.setReverse(BaseScreenData.REVERSED);
			sv.lrkcls.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.lrkcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.sslivb.setReverse(BaseScreenData.REVERSED);
			sv.sslivb.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.sslivb.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.rprmmth01.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.rprmmth01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.rprmmth03.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.rprmmth03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.rprmmth02.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rprmmth02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.rprmmth04.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.rprmmth04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.rprmmth05.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.rprmmth05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.rprmmth06.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.rprmmth06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.rprmmth07.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.rprmmth07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.rprmmth08.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.rprmmth08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.rprmmth09.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.rprmmth09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.rprmmth10.setReverse(BaseScreenData.REVERSED);
			sv.rprmmth10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.rprmmth10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.prefbas.setReverse(BaseScreenData.REVERSED);
			sv.prefbas.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.prefbas.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.rtytyp.setReverse(BaseScreenData.REVERSED);
			sv.rtytyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.rtytyp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.currcode.setReverse(BaseScreenData.REVERSED);
			sv.currcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.currcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.letterType.setReverse(BaseScreenData.REVERSED);
			sv.letterType.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.letterType.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
	
	
	
	
		<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    				<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  
	

				      			     </div>
				    		</div>
					</div>
				    		 <!-- <div class="col-md-1"></div> -->
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    	
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		

				      			     </div>
						</div>
				   </div>		
			<!-- <div class="col-md-1"></div> -->
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<!-- <div class="input-group"style="max-width:100px;"> -->
							<table><tr><td>
						    		
			
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

	</td></tr></table>

				      			    <!--  </div> -->
										
						</div>
				   </div>	
		    </div>
		    
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
						    			<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style=width:90px;>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD><!--  <div class="label_txt"> -->
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				
  

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				   <div class="col-md-4"></div><div class="col-md-4"></div> 		</div>
				    		
				    		
		
				    		
				    		
				  <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement Type")%></label>
					    		     <div class="input-group" style="max-width:120px">
						    				<%	
	longValue = sv.retype.getFormData();  
%>

<% 
	if((new Byte((sv.retype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='retype' id='retype'
type='text' 
value='<%=sv.retype.getFormData()%>' 
maxLength='<%=sv.retype.getLength()%>' 
size='<%=sv.retype.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(retype)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.retype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.retype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

					 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";" type="button" onClick="doFocus(document.getElementById('retype')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
 

<%
	}else { 
%>

class = ' <%=(sv.retype).getColor()== null  ? 
"input_cell" :  (sv.retype).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

					 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('retype')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%}longValue = null;} %>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		<!--  <div class="col-md-1"></div>	 -->
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%></label>
							<div class="input-group"  style="max-width:120px">
						    	
		
<%	
	longValue = sv.lrkcls.getFormData();  
%>

<% 
	if((new Byte((sv.lrkcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='lrkcls' id='lrkcls'
type='text' 
value='<%=sv.lrkcls.getFormData()%>' 
maxLength='<%=sv.lrkcls.getLength()%>' 
size='<%=sv.lrkcls.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(lrkcls)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.lrkcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.lrkcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
			 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.lrkcls).getColor()== null  ? 
"input_cell" :  (sv.lrkcls).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('lrkcls')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>

				      			     </div>
						</div>
				   </div>		
			<!-- <div class="col-md-1"></div> -->
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement Cession Type")%></label>
							<div class="input-group"  style="max-width:120px">
						    		
			
	<%	
	longValue = sv.rtytyp.getFormData();  
%>

<% 
	if((new Byte((sv.rtytyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rtytyp' id='rtytyp'
type='text' 
value='<%=sv.rtytyp.getFormData()%>' 
maxLength='<%=sv.rtytyp.getLength()%>' 
size='<%=sv.rtytyp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rtytyp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rtytyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rtytyp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rtytyp')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.rtytyp).getColor()== null  ? 
"input_cell" :  (sv.rtytyp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rtytyp')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>

	

	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		      		
				  <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    	
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Standard Lives Allowed?")%></label>
					    		 
						    			<input type='checkbox' name='subliv' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(subliv)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.subliv).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.subliv).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.subliv).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('subliv')"/>

<input type='checkbox' name='subliv' value=' ' 

<% if(!(sv.subliv).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('subliv')"/>

				      			
				    		</div>
					</div>
				    		
				    		  <div class="col-md-4"></div>
				    		  <!-- <div class="col-md-1"></div> -->
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Standard Limits")%></label>
							<div class="input-group"  style="max-width:120px">
						    	
	<%	
	longValue = sv.sslivb.getFormData();  
%>

<% 
	if((new Byte((sv.sslivb).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='sslivb' id='sslivb'
type='text' 
value='<%=sv.sslivb.getFormData()%>' 
maxLength='<%=sv.sslivb.getLength()%>' 
size='<%=sv.sslivb.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(sslivb)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.sslivb).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.sslivb).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('sslivb')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.sslivb).getColor()== null  ? 
"input_cell" :  (sv.sslivb).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('sslivb')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
		      		
				    		
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Refund Basis")%></label>
					    		     <div class="input-group"  style="max-width:120px">
						    			<%	
	longValue = sv.prefbas.getFormData();  
%>

<% 
	if((new Byte((sv.prefbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='prefbas' id='prefbas'
type='text' 
value='<%=sv.prefbas.getFormData()%>' 
maxLength='<%=sv.prefbas.getLength()%>' 
size='<%=sv.prefbas.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prefbas)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.prefbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.prefbas).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('prefbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.prefbas).getColor()== null  ? 
"input_cell" :  (sv.prefbas).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
<button class="btn btn-info"style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('prefbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>


  
	

				      			     </div>
				    		</div>
					</div>
				    			<!--  <div class="col-md-1"></div> -->
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group"  style="max-width:120px">
						    	
	<%	
	longValue = sv.currcode.getFormData();  
%>

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='currcode' id='currcode'
type='text' 
value='<%=sv.currcode.getFormData()%>' 
maxLength='<%=sv.currcode.getLength()%>' 
size='<%=sv.currcode.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(currcode)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.currcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.currcode).getColor()== null  ? 
"input_cell" :  (sv.currcode).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>
	
		

				      			     </div>
						</div>
				   </div>		
			<!-- <div class="col-md-1"></div> -->
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Company % Share if Quota Based")%></label>
							<div class="input-group">
						    		
			
		<%	
			qpsf = fw.getFieldXMLDef((sv.qcoshr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='qcoshr' 
type='text'

<%if((sv.qcoshr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.qcoshr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.qcoshr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.qcoshr) %>'
	 <%}%>

size='<%= sv.qcoshr.getLength()%>'
maxLength='<%= sv.qcoshr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(qcoshr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.qcoshr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.qcoshr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.qcoshr).getColor()== null  ? 
			"input_cell" :  (sv.qcoshr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Facultative Schedule Required?")%></label>
				    			<!-- <table><tr><td>  
					    		
					    		  </td>  <td> &nbsp; </td> <td>  	  
					    		     -->
						    		
<input type='checkbox' name='facsh' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(facsh)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.facsh).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.facsh).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.facsh).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('facsh')"/>

<input type='checkbox' name='facsh' value=' ' 

<% if(!(sv.facsh).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('facsh')"/>				      			 
<!-- </td></tr></table> -->
				    		</div>
					</div>
				    		<div class="col-md-4"></div>
				    	 <!-- <div class="col-md-2"></div> -->	
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Facultative Schedule Letter")%></label>
							<div class="input-group"  style="max-width:180px">
	
<%	
	longValue = sv.letterType.getFormData();  
%>

<% 
	if((new Byte((sv.letterType).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='letterType' id='letterType'
type='text' 
value='<%=sv.letterType.getFormData()%>' 
maxLength='<%=sv.letterType.getLength()%>' 
size='<%=sv.letterType.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(letterType)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.letterType).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.letterType).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
					 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('letterType')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
<%
	}else { 
%>

class = ' <%=(sv.letterType).getColor()== null  ? 
"input_cell" :  (sv.letterType).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

					 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('letterType')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	

<%}longValue = null;} %>

	
		

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
		    <br>
		    <div class="row">	
			    	<div class="col-md-2"> 
			    	<label><%=resourceBundleHandler.gettingValueFromBundle("Reasurer ")%></label>     
			    	</div>
			    	
			    	<div class="col-md-2"> 
			    <label><%=resourceBundleHandler.gettingValueFromBundle("Quota %")%> </label>      
			    	</div>
			    	
			    	<div class="col-md-3"> 
			    	<label><%=resourceBundleHandler.gettingValueFromBundle("Retention")%></label>     
			    	</div>
			    	
			    	<div class="col-md-2"> 
			    	<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label> 
			    	</div>
			    	<div class="col-md-3"></div> 
			    	</div>
			    	
		        		   <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum01)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum01)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr01, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth01)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth01)%>
		    </div></div>
		    <div class="col-md-3"> </div>
		    
		    </div>
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px">  
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum02)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum02)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr02, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    		<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth02)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth02)%>
		    </div></div>
		    
		    <div class="col-md-3"></div>
		    </div>
		    
		    
		    
		    
		       <br>
		    
		     <div class="row">	
			    	<div class="col-md-2"> 
			    		<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum03)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum03)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr03, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    		<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth03)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth03)%>
		    </div></div>
		    
		    <div class="col-md-3"></div>
		    </div>
		    
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum04)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum04)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr04, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth04)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth04)%>
		    </div></div>
		    
		   <div class="col-md-3"></div> 
		    </div>
		    
		       <br>
		    
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum05)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum05)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr05, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth05)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth05)%>
		    </div></div>
		    <div class="col-md-3"></div>
		    
		    </div>
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum06)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum06)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr06, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth06)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth06)%>
		    </div></div>
		    
		    <div class="col-md-3"></div>
		    </div>
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum07)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum07)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr07, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth07)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth07)%>
		    </div></div>
		    
		    <div class="col-md-3"></div>
		    </div>
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum08)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum08)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr08, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth08)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth08)%>
		    </div></div>
		    
		   <div class="col-md-3"></div> 
		    </div>
		       <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum09)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum09)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr09, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth09)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth09)%>
		    </div></div>
		    
		   <div class="col-md-3"></div> 
		    </div>
		    
		    <br>
		     <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		    <%=smartHF.getHTMLSpaceVar( fw, sv.rasnum10)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rasnum10)%>
		    </div></div>
		    
		    <div class="col-md-2"> 
			    
		   <%=smartHF.getHTMLVar(fw, sv.qreshr10, COBOLHTMLFormatter.S3VS2)%>
		   </div>
		    
		    <div class="col-md-3"> 
			    	
		 <%=smartHF.getHTMLVar(fw, sv.relimit10, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		   </div>
		    
		    <div class="col-md-2"> 
			    	<div class="input-group" style="min-width:100px"> 
		   <%=smartHF.getHTMLSpaceVar( fw, sv.rprmmth10)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmmth10)%>
		    </div></div>
		    
		    <div class="col-md-3"></div>
		    </div>
		    </div></div>
	
	

<%}%>

<%if (sv.S5449protectWritten.gt(0)) {%>
	<%S5449protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
	<style>
		
		
		div[id*='relimit']{padding-right:2px !important} 
		div[id*='qreshr']{padding-right:2px !important} 
		div[id*='rasnum']{padding-right:2px !important} 
		div[id*='rprmmth']{padding-right:3px !important} 
				
	</style> 
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
