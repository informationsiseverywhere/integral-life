<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5431";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5431ScreenVars sv = (S5431ScreenVars) fw.getVariables();%>
	<%StringData generatedText12 = new StringData(" ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Select");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Component   Description                            Risk Stat  Prem Stat");%>
<%		appVars.rollup(new int[] {93});
%>





<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-5"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		     <table><tr><td>
						    			
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:2px">
</td><td>



	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:2px">
</td><td>






	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </td></tr></table>
				    		</div>
					</div>
				    		<div class="col-md-1"> </div>
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<table><tr><td>
						    		
<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
	%>
	
    
	   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:80px;"> 
   <%=	(sv.cntcurr.getFormData()).toString()%>
   </div>
   
   
   
</td><td style="min-width:2px">
</td><td>

   
   
   
	   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:150px;">  
	   		<%if(longValue != null){%>
  
   <%=XSSFilter.escapeHtml(longValue)%>
  
	   		<%}%>
   </div> 		
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </td></tr></table>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					    		     <div class="input-group" >
						    		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				  	
				<div class="col-md-2"></div>
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<table><tr><td>
						    			<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("register");
		longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
	%>
	
    
	   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:80px;"> 
   <%=	(sv.register.getFormData()).toString()%>
  
   </div>
   
   
   
</td><td style="min-width:2px">
</td><td>

   
   
	   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:100px;">  
	   		<%if(longValue != null){%>
  
   <%=XSSFilter.escapeHtml(longValue)%>
  
	   		<%}%>
   </div>
  		
		<%
		longValue = null;
		formatValue = null;
		%>
		

				      			     </td></tr></table>
										
						</div>
				   </div>	
		
			    	<div class="col-md-2" style="min-width:200px"> 
				    		<div class="form-group"style="min-width:200px">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
					    		     <div class="input-group">
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
			
			if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

				      			     </div>
				    		</div>
					</div></DIV>	
				    		
				      <div class="row">		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
							<table><tr><td>
						    		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



</td><td style="min-width:2px">
</td><td>




	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			    </td></tr></table>
						</div>
				   </div>		
			
			  <div class="col-md-2"></div>
			 <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
							<table><tr><td>
						    		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td style="min-width:2px">
</td><td>





	
  		
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </td></tr></table>
						</div>
				   </div>		
			    	
		    </div>
		    
		    
		    
			
			 <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<%-- <div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div> --%>
					<div class="table-responsive">
	         <table  id='dataTables-s5431' class="table table-striped table-bordered table-hover"  >
               <thead>
		
			        <tr class="info">
			       <th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Component Number")%></th>
	<th style="text-align:center; padding-bottom:15px"><%=resourceBundleHandler.gettingValueFromBundle("Component ")%></th>
		<th style="text-align:center; padding-bottom:15px"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
		<th style="text-align:center; padding-bottom:15px"><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></th>
		<th style="text-align:center; padding-bottom:15px"><%=resourceBundleHandler.gettingValueFromBundle("Premuim Status")%></th>
	
			     	 
		 	        </tr>
			 </thead>
			 

 
		<%
		GeneralTable sfl = fw.getTable("s5431screensfl");
	
		%>
		
<%
	S5431screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5431screensfl
	.hasMoreScreenRows(sfl)) {	
%>
		




<tbody>


	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    								
																	
													
					
					 					 
						 						 	<div style='display:none; visiblity:hidden; '>
						 						 <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5431screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5431screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s5431screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%>px;"
						  
						  >
						  
						  						 	</div>
				    									<td class="tableDataTag tableDataTagFixed" style="width:80px; " align="left">													
																
																
									
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5431screensfl" + "." +
					      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.cmpntnum.getFormData()%></span></a>							 						 		
											
											
														 
				
									</td>
				    									<td class="tableDataTag" style="width:150px;" align="left">									
																
									
											
						<%= sv.component.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:400px;" align="left">							
																
									
											
						<%= sv.deit.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:150px;" align="left">								
																
									
											
						<%= sv.statcode.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:100px;" align="left">										
																
									
											
						<%= sv.pstatcode.getFormData()%>
						
														 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5431screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

	

		</table>
		</div></div></div></div>
<script>
	$(document).ready(function() {
    	$('#dataTables-s5431').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    	$('#load-more').appendTo($('.col-sm-6:eq(-1)'));

    });
</script>



			
			
			
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->




<%@ include file="/POLACommon2NEW.jsp"%>


