<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SH619";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%Sh619ScreenVars sv = (Sh619ScreenVars) fw.getVariables();%>

<%if (sv.Sh619screenWritten.gt(0)) {%>
	<%Sh619screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Class");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Arrangement");%>
	<%sv.lrkcls01.setClassString("");%>
<%	sv.lrkcls01.appendClassString("string_fld");
	sv.lrkcls01.appendClassString("input_txt");
	sv.lrkcls01.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.rngmnt01.setClassString("");%>
<%	sv.rngmnt01.appendClassString("string_fld");
	sv.rngmnt01.appendClassString("input_txt");
	sv.rngmnt01.appendClassString("highlight");
%>
	<%sv.lrkcls02.setClassString("");%>
<%	sv.lrkcls02.appendClassString("string_fld");
	sv.lrkcls02.appendClassString("input_txt");
	sv.lrkcls02.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.rngmnt02.setClassString("");%>
<%	sv.rngmnt02.appendClassString("string_fld");
	sv.rngmnt02.appendClassString("input_txt");
	sv.rngmnt02.appendClassString("highlight");
%>
	<%sv.lrkcls03.setClassString("");%>
<%	sv.lrkcls03.appendClassString("string_fld");
	sv.lrkcls03.appendClassString("input_txt");
	sv.lrkcls03.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.rngmnt03.setClassString("");%>
<%	sv.rngmnt03.appendClassString("string_fld");
	sv.rngmnt03.appendClassString("input_txt");
	sv.rngmnt03.appendClassString("highlight");
%>
	<%sv.lrkcls04.setClassString("");%>
<%	sv.lrkcls04.appendClassString("string_fld");
	sv.lrkcls04.appendClassString("input_txt");
	sv.lrkcls04.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.rngmnt04.setClassString("");%>
<%	sv.rngmnt04.appendClassString("string_fld");
	sv.rngmnt04.appendClassString("input_txt");
	sv.rngmnt04.appendClassString("highlight");
%>
	<%sv.lrkcls05.setClassString("");%>
<%	sv.lrkcls05.appendClassString("string_fld");
	sv.lrkcls05.appendClassString("input_txt");
	sv.lrkcls05.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.rngmnt05.setClassString("");%>
<%	sv.rngmnt05.appendClassString("string_fld");
	sv.rngmnt05.appendClassString("input_txt");
	sv.rngmnt05.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                     <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%					
							if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.company.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.company.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="max-width:50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	
  						 </div>
	                  </div>
	                    <!-- <div class="col-md-2">
	                      </div> -->
	                    <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  						 </div>
	                  </div>
	                   <!--  <div class="col-md-1">
	                     </div> -->
	                    <div class="col-md-4">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<!-- <div class="input-group"> --><table><tr><td>
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
							<!-- </div> -->
  						 </div>
	                  </div>
	             </div>
	               <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%></label>
	                        </div>
	                  </div>
	                    <div class="col-md-1">
	                     
	                  </div> 
	                   <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement")%></label>
	                        </div>
	                  </div>
	                   <div class="col-md-5"></div>
	               </div>
	               
	                 <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.lrkcls01)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.lrkcls01)%>
	                        </div>
	                  		</div>
	                   </div>
	                   <div class="col-md-1">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("--")%></label>
	                    </div>
	                   <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.rngmnt01)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.rngmnt01)%>
	                        </div>
	                  		</div>
	                   </div>
	                    <div class="col-md-5"></div>
	               </div>
	               <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.lrkcls02)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.lrkcls02)%>
	                        </div>
	                  		</div>
	                   </div>
	                   <div class="col-md-1">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("--")%></label>
	                    </div>
	                   <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.rngmnt02)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.rngmnt02)%>
	                        </div>
	                  		</div>
	                   </div>
	                    <div class="col-md-5"></div>
	               </div>
	                <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.lrkcls03)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.lrkcls03)%>
	                        </div>
	                  		</div>
	                   </div>
	                   <div class="col-md-1">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("--")%></label>
	                    </div>
	                   <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.rngmnt03)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.rngmnt03)%>
	                        </div>
	                  		</div>
	                   </div>
	                    <div class="col-md-5"></div>
	               </div>
	               
	               <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.lrkcls04)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.lrkcls04)%>
	                        </div>
	                  		</div>
	                   </div>
	                   <div class="col-md-1">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("--")%></label>
	                    </div>
	                   <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.rngmnt04)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.rngmnt04)%>
	                        </div>
	                  		</div>
	                   </div>
	                    <div class="col-md-5"></div>
	               </div>
	               
	             <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.lrkcls05)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.lrkcls05)%>
	                        </div>
	                  		</div>
	                   </div>
	                   <div class="col-md-1">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("--")%></label>
	                    </div>
	                   <div class="col-md-3">
	                     <div class="form-group">
	                     <div class="input-group" style="max-width:130px;min-width:71px;">
	                    	   <%=smartHF.getHTMLSpaceVar( fw, sv.rngmnt05)%>
								<%=smartHF.getHTMLF4NSVarExt( fw, sv.rngmnt05)%>
	                        </div>
	                  		</div>
	                   </div>
	                    <div class="col-md-5"></div>
	               </div>
	               
	               
	             
	    </div>
</div>

<%}%>

<%if (sv.Sh619protectWritten.gt(0)) {%>
	<%Sh619protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<%@ include file="/POLACommon2NEW.jsp"%>
