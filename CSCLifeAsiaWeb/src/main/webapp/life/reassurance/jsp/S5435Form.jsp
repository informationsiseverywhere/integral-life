

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5435";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5435ScreenVars sv = (S5435ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client        ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Address       ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"DOB      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex      ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Postcode ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Country  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk  Contract Life Cov. Rid.  Curr        Sum Retained     Treaty Reassured /");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Class                                                    Facultative Reassured ");%>
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		     <div class="input-group">
						    		
	<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></label>
							<div class="input-group">
						    		
		<%					
		if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
				   
				   
				   
				   <div class="row">	
				
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Address")%></label>
					    		     <div class="input-group">
						    			<%					
		if(!((sv.cltaddr01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:310px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("DOB")%></label>
							<div class="input-group">
						    		
<%					
		if(!((sv.cltdobDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltdobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
							<div class="input-group">
						    	
		<%					
		if(!((sv.cltsex.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltsex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>		

				      			     </div>
						</div>
				   </div>	
		    </div>
				   
				   
				   
				   <div class="row">	
				  
				   
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		
					    		     <div class="input-group">
					    		   
						    			<%					
		if(!((sv.cltaddr02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>	<% if((browerVersion.equals(Chrome)) ) {%>		
				<div style="width:310px;margin-top: 3px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
			 <% }%> 
			 <% if((browerVersion.equals(IE11)) ) {%>		
				<div style="width:310px;margin-top: 3px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
			 <% }%> 
			 
			 <% if((browerVersion.equals(IE8)) ) {%>		
				<div style="width:310px;margin-top: 3px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
			 <% }%> 
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Postcode")%></label>
							<div class="input-group">
						    		
<%					
		if(!((sv.cltpcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
						</div>
				   </div>		
			 		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Country")%></label>
							<div class="input-group">
						    		
<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"ctrycode"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("ctrycode");
		longValue = (String) mappedItems.get((sv.ctrycode.getFormData()).toString().trim());  
	%>
	
    
   
   <div class='output_cell'> 
  
   <%=XSSFilter.escapeHtml(longValue)%>
  
   </div>
  		
		<%
		longValue = null;
		formatValue = null;
		%>
				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		
					    		     <div class="input-group">
		    	<%					
		if(!((sv.cltaddr03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:310px; margin-top: -20px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		      </div>
				    		</div>
					</div></div>
		    
			<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		
					    		     <div class="input-group">
						    			<%					
		if(!((sv.cltaddr04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:310px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
				    		</div>
					</div></div>
					
					<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		
					    		     <div class="input-group">
						    		<%					
		if(!((sv.cltaddr05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:310px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
				    		</div>
					</div></div>	   
				
				
				
				
				    <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
	         <table  id='dataTables-s5435' class="table table-striped table-bordered table-hover" width='100%' >
               <thead>
		
			        <tr class="info">
			         	<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%></th>
		         								
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
					<th style=" text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Retained")%></th>
					<th style=" text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Treaty Reassured")%></th>
					
					<th style=" text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Facultative Reassured")%></th>
					
	
			     	 
		 	        </tr>
			 </thead>
			 

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[9];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.lrkcls.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
			} else {		
				calculatedValue = (sv.lrkcls.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.chdrnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
			} else {		
				calculatedValue = (sv.chdrnum.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.life.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
			} else {		
				calculatedValue = (sv.life.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.coverage.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
			} else {		
				calculatedValue = (sv.coverage.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.rider.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.rider.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.currency.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
			} else {		
				calculatedValue = (sv.currency.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.ssretn.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
			} else {		
				calculatedValue = (sv.ssretn.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.ssreast.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
			} else {		
				calculatedValue = (sv.ssreast.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[7]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.ssreasf.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
			} else {		
				calculatedValue = (sv.ssreasf.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[8]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s5435screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		

<%
	S5435screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5435screensfl
	.hasMoreScreenRows(sfl)) {	
%>
		
<!-- <script>
	$(document).ready(function() {
    	$('#dataTables-s5435').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    	

    });
</script> -->



<tbody>


<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																
									
											
						<%= sv.lrkcls.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																
									
											
						<%= sv.chdrnum.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="left">									
																
									
											
						<%= sv.life.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="left">									
																
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" align="left">									
																
									
											
						<%= sv.currency.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" align="left">									
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.ssretn).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.ssretn,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!sv.
							ssretn
							.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[7 ]%>px;" align="left">									
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.ssreast).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.ssreast,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!sv.
							ssreast
							.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[8 ]%>px;" align="left">									
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.ssreasf).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.ssreasf,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!sv.
							ssreasf
							.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5435screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

		</table>
		</div></div></div></div>

				
		<script>
	$(document).ready(function() {
    	$('#dataTables-s5435').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    	$('#load-more').appendTo($('.col-sm-6:eq(-1)'));

    });
</script>
				
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->


<%@ include file="/POLACommon2NEW.jsp"%>

