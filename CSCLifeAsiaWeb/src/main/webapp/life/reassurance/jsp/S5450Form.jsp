
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5450";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5450ScreenVars sv = (S5450ScreenVars) fw.getVariables();%>

<%if (sv.S5450screenWritten.gt(0)) {%>
	<%S5450screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Class");%>
	<%sv.rprmcls01.setClassString("");%>
<%	sv.rprmcls01.appendClassString("string_fld");
	sv.rprmcls01.appendClassString("input_txt");
	sv.rprmcls01.appendClassString("highlight");
%>
	<%sv.rprmcls02.setClassString("");%>
<%	sv.rprmcls02.appendClassString("string_fld");
	sv.rprmcls02.appendClassString("input_txt");
	sv.rprmcls02.appendClassString("highlight");
%>
	<%sv.rprmcls03.setClassString("");%>
<%	sv.rprmcls03.appendClassString("string_fld");
	sv.rprmcls03.appendClassString("input_txt");
	sv.rprmcls03.appendClassString("highlight");
%>
	<%sv.rprmcls04.setClassString("");%>
<%	sv.rprmcls04.appendClassString("string_fld");
	sv.rprmcls04.appendClassString("input_txt");
	sv.rprmcls04.appendClassString("highlight");
%>
	<%sv.rprmcls05.setClassString("");%>
<%	sv.rprmcls05.appendClassString("string_fld");
	sv.rprmcls05.appendClassString("input_txt");
	sv.rprmcls05.appendClassString("highlight");
%>
	<!--<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-----------------------------------------------------------------------------------------------------------------------------------------------------");%>
	--><%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Calculation ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Life");%>
	<%sv.premsubr01.setClassString("");%>
	<%sv.premsubr02.setClassString("");%>
<%	sv.premsubr02.appendClassString("string_fld");
	sv.premsubr02.appendClassString("input_txt");
	sv.premsubr02.appendClassString("highlight");
%>
	<%sv.premsubr03.setClassString("");%>
<%	sv.premsubr03.appendClassString("string_fld");
	sv.premsubr03.appendClassString("input_txt");
	sv.premsubr03.appendClassString("highlight");
%>
	<%sv.premsubr04.setClassString("");%>
<%	sv.premsubr04.appendClassString("string_fld");
	sv.premsubr04.appendClassString("input_txt");
	sv.premsubr04.appendClassString("highlight");
%>
	<%sv.premsubr05.setClassString("");%>
<%	sv.premsubr05.appendClassString("string_fld");
	sv.premsubr05.appendClassString("input_txt");
	sv.premsubr05.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life");%>
	<%sv.rjlprem01.setClassString("");%>
	<%sv.rjlprem02.setClassString("");%>
<%	sv.rjlprem02.appendClassString("string_fld");
	sv.rjlprem02.appendClassString("input_txt");
	sv.rjlprem02.appendClassString("highlight");
%>
	<%sv.rjlprem03.setClassString("");%>
<%	sv.rjlprem03.appendClassString("string_fld");
	sv.rjlprem03.appendClassString("input_txt");
	sv.rjlprem03.appendClassString("highlight");
%>
	<%sv.rjlprem04.setClassString("");%>
<%	sv.rjlprem04.appendClassString("string_fld");
	sv.rjlprem04.appendClassString("input_txt");
	sv.rjlprem04.appendClassString("highlight");
%>
	<%sv.rjlprem05.setClassString("");%>
<%	sv.rjlprem05.appendClassString("string_fld");
	sv.rjlprem05.appendClassString("input_txt");
	sv.rjlprem05.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Rates  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select");%>
	<%sv.slctrate01.setClassString("");%>
	<%sv.slctrate02.setClassString("");%>
<%	sv.slctrate02.appendClassString("string_fld");
	sv.slctrate02.appendClassString("input_txt");
	sv.slctrate02.appendClassString("highlight");
%>
	<%sv.slctrate03.setClassString("");%>
<%	sv.slctrate03.appendClassString("string_fld");
	sv.slctrate03.appendClassString("input_txt");
	sv.slctrate03.appendClassString("highlight");
%>
	<%sv.slctrate04.setClassString("");%>
<%	sv.slctrate04.appendClassString("string_fld");
	sv.slctrate04.appendClassString("input_txt");
	sv.slctrate04.appendClassString("highlight");
%>
	<%sv.slctrate05.setClassString("");%>
<%	sv.slctrate05.appendClassString("string_fld");
	sv.slctrate05.appendClassString("input_txt");
	sv.slctrate05.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ultimate");%>
	<%sv.ultmrate01.setClassString("");%>
	<%sv.ultmrate02.setClassString("");%>
<%	sv.ultmrate02.appendClassString("string_fld");
	sv.ultmrate02.appendClassString("input_txt");
	sv.ultmrate02.appendClassString("highlight");
%>
	<%sv.ultmrate03.setClassString("");%>
<%	sv.ultmrate03.appendClassString("string_fld");
	sv.ultmrate03.appendClassString("input_txt");
	sv.ultmrate03.appendClassString("highlight");
%>
	<%sv.ultmrate04.setClassString("");%>
<%	sv.ultmrate04.appendClassString("string_fld");
	sv.ultmrate04.appendClassString("input_txt");
	sv.ultmrate04.appendClassString("highlight");
%>
	<%sv.ultmrate05.setClassString("");%>
<%	sv.ultmrate05.appendClassString("string_fld");
	sv.ultmrate05.appendClassString("input_txt");
	sv.ultmrate05.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial Discount/Commission Period");%>
	<%sv.inlprd01.setClassString("");%>
	<%sv.inlprd02.setClassString("");%>
<%	sv.inlprd02.appendClassString("num_fld");
	sv.inlprd02.appendClassString("input_txt");
	sv.inlprd02.appendClassString("highlight");
%>
	<%sv.inlprd03.setClassString("");%>
<%	sv.inlprd03.appendClassString("num_fld");
	sv.inlprd03.appendClassString("input_txt");
	sv.inlprd03.appendClassString("highlight");
%>
	<%sv.inlprd04.setClassString("");%>
<%	sv.inlprd04.appendClassString("num_fld");
	sv.inlprd04.appendClassString("input_txt");
	sv.inlprd04.appendClassString("highlight");
%>
	<%sv.inlprd05.setClassString("");%>
<%	sv.inlprd05.appendClassString("num_fld");
	sv.inlprd05.appendClassString("input_txt");
	sv.inlprd05.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Basis");%>
	<%sv.basicCommMeth01.setClassString("");%>
	<%sv.basicCommMeth02.setClassString("");%>
<%	sv.basicCommMeth02.appendClassString("string_fld");
	sv.basicCommMeth02.appendClassString("input_txt");
	sv.basicCommMeth02.appendClassString("highlight");
%>
	<%sv.basicCommMeth03.setClassString("");%>
<%	sv.basicCommMeth03.appendClassString("string_fld");
	sv.basicCommMeth03.appendClassString("input_txt");
	sv.basicCommMeth03.appendClassString("highlight");
%>
	<%sv.basicCommMeth04.setClassString("");%>
<%	sv.basicCommMeth04.appendClassString("string_fld");
	sv.basicCommMeth04.appendClassString("input_txt");
	sv.basicCommMeth04.appendClassString("highlight");
%>
	<%sv.basicCommMeth05.setClassString("");%>
<%	sv.basicCommMeth05.appendClassString("string_fld");
	sv.basicCommMeth05.appendClassString("input_txt");
	sv.basicCommMeth05.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Rates");%>
	<%sv.commrate01.setClassString("");%>
	<%sv.commrate02.setClassString("");%>
<%	sv.commrate02.appendClassString("string_fld");
	sv.commrate02.appendClassString("input_txt");
	sv.commrate02.appendClassString("highlight");
%>
	<%sv.commrate03.setClassString("");%>
<%	sv.commrate03.appendClassString("string_fld");
	sv.commrate03.appendClassString("input_txt");
	sv.commrate03.appendClassString("highlight");
%>
	<%sv.commrate04.setClassString("");%>
<%	sv.commrate04.appendClassString("string_fld");
	sv.commrate04.appendClassString("input_txt");
	sv.commrate04.appendClassString("highlight");
%>
	<%sv.commrate05.setClassString("");%>
<%	sv.commrate05.appendClassString("string_fld");
	sv.commrate05.appendClassString("input_txt");
	sv.commrate05.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate Factor");%>
	<%sv.rratfac01.setClassString("");%>
	<%sv.rratfac02.setClassString("");%>
<%	sv.rratfac02.appendClassString("num_fld");
	sv.rratfac02.appendClassString("input_txt");
	sv.rratfac02.appendClassString("highlight");
%>
	<%sv.rratfac03.setClassString("");%>
<%	sv.rratfac03.appendClassString("num_fld");
	sv.rratfac03.appendClassString("input_txt");
	sv.rratfac03.appendClassString("highlight");
%>
	<%sv.rratfac04.setClassString("");%>
<%	sv.rratfac04.appendClassString("num_fld");
	sv.rratfac04.appendClassString("input_txt");
	sv.rratfac04.appendClassString("highlight");
%>
	<%sv.rratfac05.setClassString("");%>
<%	sv.rratfac05.appendClassString("num_fld");
	sv.rratfac05.appendClassString("input_txt");
	sv.rratfac05.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.premsubr01.setReverse(BaseScreenData.REVERSED);
			sv.premsubr01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.premsubr01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.rjlprem01.setReverse(BaseScreenData.REVERSED);
			sv.rjlprem01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.rjlprem01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.slctrate01.setReverse(BaseScreenData.REVERSED);
			sv.slctrate01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.slctrate01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ultmrate01.setReverse(BaseScreenData.REVERSED);
			sv.ultmrate01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ultmrate01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.basicCommMeth01.setReverse(BaseScreenData.REVERSED);
			sv.basicCommMeth01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.basicCommMeth01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.inlprd01.setReverse(BaseScreenData.REVERSED);
			sv.inlprd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.inlprd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.rratfac01.setReverse(BaseScreenData.REVERSED);
			sv.rratfac01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rratfac01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.commrate01.setReverse(BaseScreenData.REVERSED);
			sv.commrate01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.commrate01.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    			<%					
						    							
						    			if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						    						
						    								if(longValue == null || longValue.equalsIgnoreCase("")) {
						    									formatValue = formatValue( (sv.company.getFormData()).toString()); 
						    								} else {
						    									formatValue = formatValue( longValue);
						    								}
						    								
						    								
						    						} else  {
						    									
						    						if(longValue == null || longValue.equalsIgnoreCase("")) {
						    									formatValue = formatValue( (sv.company.getFormData()).toString()); 
						    								} else {
						    									formatValue = formatValue( longValue);
						    								}
						    						
						    						}
						    						%>			
						    					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						    							"blank_cell" : "output_cell" %>'>
						    					<%=XSSFilter.escapeHtml(formatValue)%>
						    				</div>	
						    			<%
						    			longValue = null;
						    			formatValue = null;
						    			%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group">
						    		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


	<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 140px;text-align: left;border:1px solid #ccc !important;"
>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	

  
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
			
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <label><font size='2'>
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</font></label>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    	
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				    		</div>
				    		
				    		<br>
				    		
				    		<div class="row">	
			    	<div class="col-md-2"> 
			    	<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Class")%></label>
			    	</div>
			    	
			    		
			    	
			    		<div class="col-md-2"style="min-width:175px; padding-top:5px"> 	
			    		<div class="input-group">
			  
	
	<%=smartHF.getHTMLSpaceVar(fw, sv.rprmcls01)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.rprmcls01)%>
			    	
			    	</div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;"> 
			    	 <div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    		<%=smartHF.getHTMLSpaceVar( fw, sv.rprmcls02)%>
			    </td><td>
	                    <%=smartHF.getHTMLF4NSVar(fw, sv.rprmcls02)%>
			    	</td></tr></table>
			    	 </div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;"> 
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">		    	 
			    	<%=smartHF.getHTMLSpaceVar( fw, sv.rprmcls03)%>
			    	</td><td>
	<%=smartHF.getHTMLF4NSVar(fw, sv.rprmcls03)%>
	</td></tr></table>
			    	</div> </div>
			    	
			    		<div class="col-md-2" style="max-width:117px;"> 
			    	 <div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px"> 
			    	 
			    	<%=smartHF.getHTMLSpaceVar( fw, sv.rprmcls04)%></td><td>
	<%=smartHF.getHTMLF4NSVar(fw, sv.rprmcls04)%>
			    	</td></tr></table>
			    	</div> </div>
			    	
			    		<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    	 
			    	
	<%=smartHF.getHTMLSpaceVar( fw, sv.rprmcls05)%>
	</td><td>
	<%=smartHF.getHTMLF4NSVar(fw, sv.rprmcls05)%></td></tr></table>
			    	</div></div>
			    	
			    	
			    	

	
	
</div>

<br>

		<div class="row">
			<div class="col-md-2" style="max-width: 150px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Calculation")%></label>
			</div>
<div class="col-md-1" style="max-width: 80px;">
				
			</div>

			<div class="col-md-2" style="max-width: 117px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Single")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
			</div>

			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.premsubr01)%>

			</div>

			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.premsubr02)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.premsubr03)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.premsubr04)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.premsubr05)%>

			</div>





		</div>

<br>

<div class="row">
			<div class="col-md-2" style="max-width: 150px;">
				
			</div>
<div class="col-md-1" style="max-width: 80px;">
				
			</div>

			<div class="col-md-2" style="max-width: 117px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Joint")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
			</div>

			<div class="col-md-2" style="max-width: 117px;">



			<%=smartHF.getHTMLVar(fw, sv.rjlprem01)%>

			</div>

			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.rjlprem02)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.rjlprem03)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



			<%=smartHF.getHTMLVar(fw, sv.rjlprem04)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar(fw, sv.rjlprem05)%>
			</div>





		</div>

<br>

<div class="row">
			<div class="col-md-2" style="max-width: 150px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Rates")%></label>
			</div>
<div class="col-md-1" style="max-width: 80px;">
				
			</div>

			<div class="col-md-2" style="max-width: 117px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Select")%></label>
			</div>

			<div class="col-md-2" style="max-width: 117px;">



			<%=smartHF.getHTMLVar( fw, sv.slctrate01)%>

			</div>

			<div class="col-md-2" style="max-width: 117px;">


	<%=smartHF.getHTMLVar( fw, sv.slctrate02)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">


	<%=smartHF.getHTMLVar( fw, sv.slctrate03)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar( fw, sv.slctrate04)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar( fw, sv.slctrate05)%>

			</div>





		</div>

<br>

<div class="row">
			<div class="col-md-2" style="max-width: 150px;">
				
			</div>
<div class="col-md-1" style="max-width: 80px;">
				
			</div>

			<div class="col-md-2" style="max-width: 117px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Ultimatee")%></label>
			</div>

			<div class="col-md-2" style="max-width: 117px;">



		<%=smartHF.getHTMLVar( fw, sv.ultmrate01)%>

			</div>

			<div class="col-md-2" style="max-width: 117px;">



				<%=smartHF.getHTMLVar( fw, sv.ultmrate02)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar( fw, sv.ultmrate03)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar( fw, sv.ultmrate04)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar( fw, sv.ultmrate05)%>
			</div>





		</div>


<br>

		<div class="row">
			<div class="col-md-6" style="max-width: 348px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Initial")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Discount/Commission")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Period")%></label>
			</div>
<!-- <div class="col-md-1" style="max-width: 80px;">
				
			</div> -->

			<div class="col-md-2" style="max-width: 117px;">
			<%=smartHF.getHTMLVar( fw, sv.inlprd01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>  
			</div>

			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar( fw, sv.inlprd02)%>

			</div>

			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar( fw, sv.inlprd03)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



			<%=smartHF.getHTMLVar(fw, sv.inlprd04)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



			<%=smartHF.getHTMLVar( fw, sv.inlprd05)%>

			</div>
			





		</div>



<br>

<div class="row">	
			    	<div class="col-md-2"> 
			    			<label><%=resourceBundleHandler.gettingValueFromBundle("Commission")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Basis")%></label>
			    	</div>
			    	
			    		
			    	
			    	<div class="col-md-2"style="min-width:175px; padding-top:5px"> 	
			    		<div class="input-group">
	
		<%=smartHF.getHTMLSpaceVar(fw, sv.basicCommMeth01)%> 
	<%=smartHF.getHTMLF4NSVar( fw, sv.basicCommMeth01)%>
			    	 </div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    			<%=smartHF.getHTMLSpaceVar(fw, sv.basicCommMeth02)%></td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.basicCommMeth02)%>
	</td></tr></table>
			    	</div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    		<%=smartHF.getHTMLSpaceVar(fw, sv.basicCommMeth03)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.basicCommMeth03)%>
	</td></tr></table>
			    	</div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    		<%=smartHF.getHTMLSpaceVar(fw, sv.basicCommMeth04)%></td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.basicCommMeth04)%>
			    	</td></tr></table>
			    	</div></div>
			    	
			    		<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    	
	<%=smartHF.getHTMLSpaceVar(fw, sv.basicCommMeth05)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.basicCommMeth05)%>
	</td></tr></table>
			    	</div></div>
			    	
			    	
			    	

	
	
</div>

<br>

<div class="row">	
			    	<div class="col-md-2"> 
			    			<label><%=resourceBundleHandler.gettingValueFromBundle("Commission")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Rates")%></label>
			    	</div>
			    	
			    		
			    	
			    	<div class="col-md-2"style="min-width:175px; padding-top:5px"> 	
			    		<div class="input-group">
			  
	
		<%=smartHF.getHTMLSpaceVar(fw, sv.commrate01)%>
	<%=smartHF.getHTMLF4NSVar( fw, sv.commrate01)%>
			    	</div></div>
			    	
			    			<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    			<%=smartHF.getHTMLSpaceVar(fw, sv.commrate02)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.commrate02)%>
			    	</td></tr></table>
			    	</div></div>
			    	
			    			<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    		<%=smartHF.getHTMLSpaceVar(fw, sv.commrate03)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.commrate03)%>
			    	</td></tr></table></div></div>
			    	
			    			<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    		<%=smartHF.getHTMLSpaceVar(fw, sv.commrate04)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.commrate04)%>
			    	</td></tr></table>
			    	</div></div>
			    	
			    			<div class="col-md-2" style="max-width:117px;">  
			    	<div class="form-group" style="max-width:87px;">
			    	 <table><tr><td style="min-width:50px">
			    	
	<%=smartHF.getHTMLSpaceVar(fw, sv.commrate05)%> </td><td>
	<%=smartHF.getHTMLF4NSVar( fw, sv.commrate05)%>
			    	</td></tr></table>
			    	</div></div>
			    	
			    	
			    	

	
	
</div>

<br>

		<div class="row">
			<div class="col-md-6" style="max-width: 348px;">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Rate")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>
			</div>
<!-- <div class="col-md-1" style="max-width: 80px;">
				
			</div> -->

			<div class="col-md-2" style="max-width: 117px;">
<%=smartHF.getHTMLVar( fw, sv.rratfac01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>

			<div class="col-md-2" style="max-width: 117px;">


	<%=smartHF.getHTMLVar(fw, sv.rratfac02, COBOLHTMLFormatter.S1VS2)%>


			</div>

			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar(fw, sv.rratfac03, COBOLHTMLFormatter.S1VS2)%>
			</div>
			<div class="col-md-2" style="max-width: 117px;">


<%=smartHF.getHTMLVar(fw, sv.rratfac04, COBOLHTMLFormatter.S1VS2)%>

			</div>
			<div class="col-md-2" style="max-width: 117px;">



		<%=smartHF.getHTMLVar(fw, sv.rratfac05, COBOLHTMLFormatter.S1VS2)%>

			</div>
			





		</div>


	</div></div> 

<%}%>

<%if (sv.S5450protectWritten.gt(0)) {%>
	<%S5450protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>

<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
<!-- 	<style>
		
		
		div[id*='rprmcls']{padding-right:2px !important} 
		div[id*='premsubr']{padding-right:2px !important} 
		div[id*='rjlprem']{padding-right:2px !important} 
		div[id*='slctrate']{padding-right:3px !important} 
		div[id*='ultmrate']{padding-right:3px !important}
		div[id*='inlprd']{padding-right:3px !important}		
		div[id*='basicCommMeth']{padding-right:3px !important}	
		div[id*='commrate']{padding-right:3px !important}	
		div[id*='rratfac']{padding-right:3px !important}
			
	</style>  -->
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->