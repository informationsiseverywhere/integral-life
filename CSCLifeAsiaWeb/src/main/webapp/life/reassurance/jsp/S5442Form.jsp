

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5442";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5442ScreenVars sv = (S5442ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No   ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 1-Create 2-Modify 3-Enquire 4-Delete");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act Life Cov Rider  Component Details");%>
<%		appVars.rollup(new int[] {93});
%>



<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  
				    		<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    		    <%} %> <div class="input-group"style="max-width:100px;">
						    		

<%if ((new Byte((sv.chdrsel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				  
		    </div>
				   
			 <div class="table-responsive">
	         <table id="dataTables-s5442" class="table table-striped table-bordered table-hover" >
               <thead>
		
			        <tr class="info">
			        

				<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Action")%></th>
		     							
				<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
				<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
						<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
						<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Components")%></th>
						<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Details")%></th>
			
			        
			      
		 	        </tr>
			 </thead>
<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;
								//ILIFE-1539 STARTS
														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.action.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*10;								
						} else {		
							calculatedValue = (sv.action.getFormData()).length()*10;								
						}	
								//ILIFE-1539 ENDS
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.life.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*13;								
			} else {		
				calculatedValue = (sv.life.getFormData()).length()*13;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.coverage.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*13;								
			} else {		
				calculatedValue = (sv.coverage.getFormData()).length()*13;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.rider.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*13;								
			} else {		
				calculatedValue = (sv.rider.getFormData()).length()*13;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.shortdesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*13;								
			} else {		
				calculatedValue = (sv.shortdesc.getFormData()).length()*13;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
		//ILIFE-1539 STARTS
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.elemdesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*7;								
			} else {		
				calculatedValue = (sv.elemdesc.getFormData()).length()*7;								
			}	
		//ILIFE-1539 ENDS
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			%>
			
	<!-- ILIFE-1482 END -->		
			
		<%
		GeneralTable sfl = fw.getTable("s5442screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
		<tbody>
	<%
	S5442screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5442screensfl
	.hasMoreScreenRows(sfl)) {	
%>
<%
{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.action).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
					<%if((sv.action).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
										 					 
					 <input type="radio" 
						 value='<%= (sv.action).getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5442screensfl" + "." +
						 "action")' onKeyUp='return checkMaxLength(this)' 
						 name='s5442screensfl.action_R<%=count%>'
						 id='s5442screensfl.action_R<%=count%>'
						 onClick="selectedRow('s5442screensfl.action_R<%=count%>');toggleButton('s5442screensfl.action_R<%=count%>',<%=count%>);"
						 class="radio"
					 />
					
		
											
									</td>
		<%}else{%>
												<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
					</td>														
										
					<%}%>
								<%if((new Byte((sv.life).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" 
					<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.life.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.rider).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
					<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.shortdesc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
					<%if((sv.shortdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.shortdesc.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.elemdesc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
					<%if((sv.elemdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.elemdesc.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
														
				    </td>
										
					<%}%>
		<input type='text' style='display:none;' id='s5442screensfl.bascovr_R<%=count%>' name='bascovr' onFocus='doFocus(this)' value='<%=sv.bascovr%>' onKeyUp='return checkMaxLength(this)' />					
	</tr>

	<%
	count = count + 1;
	S5442screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>

	<input type='text' style='display:none;' id='indxflgid' name='indxflg' onFocus='doFocus(this)' value='N' onKeyUp='return checkMaxLength(this)' />
	<div class="row">
			<div class="col-md-12">
			                
					<table><tr>
		<td>
		
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperationS5442(1)" class="btn btn-info" id='test1'><%=resourceBundleHandler.gettingValueFromBundle("Create")%></a>
			</div>
		</td>
		<td>&nbsp;</td>
		<td>
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(2)" class="btn btn-info" id='test2'><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
			</div>
		</td>
		<td>&nbsp;</td>
		<td>
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(4)" class="btn btn-info" id='test3'><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
			</div>
		</td>
		<td>&nbsp;</td> 
		<td>
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(3)" class="btn btn-info" id='test4'><%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></a>
			</div>
		</td>
		
		</tr></table>
		</div>
		</div>    	
	</div>  <!--  panel-->
	</div>  <!--panel  -->


<script>
 function perFormOperationS5442(act) {
	$("input:radio").each(function(){
	    var $this = $(this);

	    if($this.is(":checked")){
	    	document.getElementById($this.attr('id')).value = act; 
	    }
	});
/* 	document.getElementById('indxflgid').value = 'Y';  */ 
	
	doAction('PFKEY0'); 
} 
 
function toggleButton(actionId, count){
	var elementid= 's5442screensfl.bascovr_R'+count;
	console.log('Check this value'+document.getElementById(elementid).value);
	if(document.getElementById(elementid).value=='1'){
		
		document.getElementById('indxflgid').value = 'P';
		
	}
	if(document.getElementById(elementid).value=='2'){
		
		document.getElementById('indxflgid').value = 'Q';
	
	}
	if(document.getElementById(elementid).value=='3'){
		
		document.getElementById('indxflgid').value = 'R';
		
	}
}
	$(document).ready(function() {
		$('#dataTables-s5442').DataTable({
			ordering : false,
			searching : false,
			info:false,
			paging:false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
		});


	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
