<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5438";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5438ScreenVars sv = (S5438ScreenVars) fw.getVariables();%>
<%{
}%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-5"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Reassurer Number ")%></label>
					    		     <div class="input-group">
					    		     
					    		     <!-- ILIFE-6026 cltnme is changed to clntname, refer DDFieldWindowing of rasnum -->
					    		     
						    		<%=smartHF.getRichTextExt(fw, sv.rasnum,(sv.rasnum.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.rasnum).replace("absolute","relative")%>
<%if ((new Byte((sv.clntname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="min-width:200px;"><!-- ILIFE-6026 -->
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		<div class="col-md-1"> </div>
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Commencement Date ")%></label>
							<div class="input-group" style="min-width:80px;"><!-- ILIFE-6026 -->
						    	<%=smartHF.getRichText(0, 0, fw, sv.cmdateDisp,(sv.cmdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.cmdateDisp).replace("absolute","relative")%>	

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
				 
				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement   ")%></label>
					    		     <div class="input-group">
						    		<% if("red".equals((sv.rngmnt).getColor())){ %>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:40px;">
<%}%>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rngmnt"},sv,"E",baseModel); 
	mappedItems = (Map) fieldItem.get("rngmnt");
	optionValue = makeDropDownList( mappedItems , sv.rngmnt.getFormData(),2,resourceBundleHandler);   
%>
<select name='rngmnt' type='list' style="width:250px;"
<% 
if((new Byte((sv.rngmnt).getEnabled()))
		.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rngmnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell'
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rngmnt).getColor())){%>
</div>
<%}%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		<div class="col-md-2"> </div>
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Costed To Date ")%></label>
							<div class="input-group" style="min-width:80px;"><!-- ILIFE-6026 -->
						    	<%=smartHF.getRichText(0, 0, fw, sv.ctdateDisp,(sv.ctdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ctdateDisp).replace("absolute","relative")%>	

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
				   
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Component Sum Assured ")%></label>
					    		     <div class="input-group" style="min-width:100px;"><!-- ILIFE-6026 -->
						    		<%if(((BaseScreenData)sv.sumins) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.sumins,( sv.sumins.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.sumins) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.sumins, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

				      			     </div>
				    		</div>
					</div>
				    		
				    		<div class="col-md-2"> </div>
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Reassured ")%></label>
							<div class="input-group">
						    		<%if(((BaseScreenData)sv.raAmount) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.raAmount,( sv.raAmount.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.raAmount) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.raAmount,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>

				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
				     
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<%@ include file="/POLACommon2NEW.jsp"%>
