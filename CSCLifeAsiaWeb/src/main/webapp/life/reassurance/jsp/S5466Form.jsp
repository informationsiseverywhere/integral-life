




<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5466";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>
<%S5466ScreenVars sv = (S5466ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind03.isOn()) {
	sv.hflag.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind02.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
}
}%>
<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table>
							<tr>
							<td>
							<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							</td>
							<td>

							<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
							style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							</td>
							<td>

							<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
							style="margin-left: 1px;max-width: 170px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>																			
							</td>
							</tr>
							</table>
					</div>
				</div>
				
			<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
                             <%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>

  <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
                              <%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>
			</div>
<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>

<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.register.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>

<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label><br/>
	<table>
	<tr>
	<td>
					<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 170px;min-width: 100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
</tr>
</table>

</div></div>	</div>
<div class="row">        
					<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
							<table>
							<tr>
							<td>
					<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
style="margin-left: 1px;min-width:100px; ">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.hflag).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.hflag.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hflag.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
 style="margin-left: 1px;max-width: 170px;min-width: 100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
</tr>
</table>
</div></div></div>
<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="s5466Table">
                            <thead>

                            <tr class='info'>
                            <th><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
		         								
					<th><%=resourceBundleHandler.gettingValueFromBundle("CMPNTNUM")%></th>
	
					<th><%=resourceBundleHandler.gettingValueFromBundle("Component")%></th>
					
					<th><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
					<th><%=resourceBundleHandler.gettingValueFromBundle("Risk Stat")%></th>
						<th><%=resourceBundleHandler.gettingValueFromBundle("Prem Stat")%></th>
						</tr>
						</thead>
						<%
			GeneralTable sfl = fw.getTable("s5466screensfl");		
	S5466screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5466screensfl
	.hasMoreScreenRows(sfl)) {	
%>	
<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5466screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5466screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="s5466screensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.hsuffix.getLength()%>'
 value='<%= sv.hsuffix.getFormData() %>' 
 size='<%=sv.hsuffix.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5466screensfl.hsuffix)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5466screensfl" + "." + "hsuffix" + "_R" + count %>'
 id='<%="s5466screensfl" + "." + "hsuffix" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.hrider.getLength()%>'
 value='<%= sv.hrider.getFormData() %>' 
 size='<%=sv.hrider.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5466screensfl.hrider)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5466screensfl" + "." + "hrider" + "_R" + count %>'
 id='<%="s5466screensfl" + "." + "hrider" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.hlifeno.getLength()%>'
 value='<%= sv.hlifeno.getFormData() %>' 
 size='<%=sv.hlifeno.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5466screensfl.hlifeno)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5466screensfl" + "." + "hlifeno" + "_R" + count %>'
 id='<%="s5466screensfl" + "." + "hlifeno" + "_R" + count %>'		  >


 <input type='hidden' maxLength='<%=sv.hcoverage.getLength()%>'
 value='<%= sv.hcoverage.getFormData() %>' 
 size='<%=sv.hcoverage.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5466screensfl.hcoverage)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5466screensfl" + "." + "hcoverage" + "_R" + count %>'
 id='<%="s5466screensfl" + "." + "hcoverage" + "_R" + count %>'		  >
 
<!--  ILIFE-993 -->

<td>
								 <input type='checkbox' 
								onclick="handleCheckBox('s5466screensfl.select_R<%=count%>')"
								value='1' name='s5466screensfl.select_R<%=count%>' id='s5466screensfl.select_R<%=count%>'
								onFocus="doFocus('s5466screensfl.select_R<%=count%>')" 
								onHelp='return fieldHelp("s5466screensfl" + "." +"select")' class="UICheck"
								style="margin-top:0px;"
								 <%if(((sv.select.getFormData()).toString().trim().equalsIgnoreCase("Y"))) {%>
									    checked="checked"
									<%} if((sv.select).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
						   			disabled
								<% } %>
								>
								<input type='checkbox' style="visibility: hidden" value=' ' 
										 onFocus='doFocus(this)' onHelp='return fieldHelp("s5466screensfl" + "." +
										 "select")' onKeyUp='return checkMaxLength(this)' 
										 name='s5466screensfl.select_R<%=count%>' id='s5466screensfl.select_R<%=count%>'
										 <%if(!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("Y"))) {%>
									    checked="checked"
									<%}%>
									onclick="handleCheckBox('s5466screensfl.select_R<%=count%>')"
								/>
													
								</td>
								
<!-- ILIFE-993 -->
<td>									
	<%if((new Byte((sv.cmpntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.cmpntnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0)%>
			<%if((new Byte((sv.cmpntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
				
						<%=sv.cmpntnum.getFormData()%>							 						 		
					
														 
				
									<%}%>
			 			</td>



<td>									
				<%if((new Byte((sv.component).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.component.getFormData();
							%>
					 		<div id="s5466screensfl.component_R<%=count%>" name="s5466screensfl.component_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>									
				<%if((new Byte((sv.deit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.deit.getFormData();
							%>
					 		<div id="s5466screensfl.deit_R<%=count%>" name="s5466screensfl.deit_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>									
				<%if((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.statcode.getFormData();
							%>
					 		<div id="s5466screensfl.statcode_R<%=count%>" name="s5466screensfl.statcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



<td>									
				<%if((new Byte((sv.pstatcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.pstatcode.getFormData();
							%>
					 		<div  id="s5466screensfl.pstatcode_R<%=count%>" name="s5466screensfl.pstatcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>



</tr>

<%	count = count + 1;
S5466screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</table>
</div>
</div>
</div>							
		</div>
		</div>
<script>
	$(document).ready(function() {
    	$('#s5466Table').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
        	paging:false,
        	info:false
      	});
    	

    });
</script>	
<%@ include file="/POLACommon2NEW.jsp"%>

