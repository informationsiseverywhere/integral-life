<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5448";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5448ScreenVars sv = (S5448ScreenVars) fw.getVariables();%>

<%if (sv.S5448screenWritten.gt(0)) {%>
	<%S5448screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Type ");%>
	<%sv.rrsktyp.setClassString("");%>
	<%sv.lrkclsdsc.setClassString("");%>
<%	sv.lrkclsdsc.appendClassString("string_fld");
	sv.lrkclsdsc.appendClassString("output_txt");
	sv.lrkclsdsc.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Costing Frequency ");%>
	<%sv.rcstfrq.setClassString("");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Priority ");%>
	<%sv.rprior.setClassString("");%>
<%	sv.rprior.appendClassString("num_fld");
	sv.rprior.appendClassString("input_txt");
	sv.rprior.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Review Frequency ");%>
	<%sv.rrevfrq.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ceding Based on Sum At Risk ");%>
	<%sv.rcedsar.setClassString("");%>
<%	sv.rcedsar.appendClassString("string_fld");
	sv.rcedsar.appendClassString("input_txt");
	sv.rcedsar.appendClassString("highlight");
%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Costing Based on Sum Reassured At Risk ");%>
	<%sv.rcstsar.setClassString("");%>
<%	sv.rcstsar.appendClassString("string_fld");
	sv.rcstsar.appendClassString("input_txt");
	sv.rcstsar.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Taxation Basis ");%>
	<%sv.rtaxbas.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reserve Calculation for Sum At Risk ");%>
	<%sv.rresmeth.setClassString("");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Basis ");%>
	<%sv.rclmbas.setClassString("");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Proportionate Processing Applicable ");%>
	<%sv.proppr.setClassString("");%>
<%	sv.proppr.appendClassString("string_fld");
	sv.proppr.appendClassString("input_txt");
	sv.proppr.appendClassString("highlight");
%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Argument Type Hierarchy");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 ");%>
	<%sv.rrngmnt01.setClassString("");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 2 ");%>
	<%sv.rrngmnt02.setClassString("");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 3 ");%>
	<%sv.rrngmnt03.setClassString("");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 4 ");%>
	<%sv.rrngmnt04.setClassString("");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 5 ");%>
	<%sv.rrngmnt05.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 6 ");%>
	<%sv.rrngmnt06.setClassString("");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 7 ");%>
	<%sv.rrngmnt07.setClassString("");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 8 ");%>
	<%sv.rrngmnt08.setClassString("");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class," 9 ");%>
	<%sv.rrngmnt09.setClassString("");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"10 ");%>
	<%sv.rrngmnt10.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.rrsktyp.setReverse(BaseScreenData.REVERSED);
			sv.rrsktyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.rrsktyp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.rresmeth.setReverse(BaseScreenData.REVERSED);
			sv.rresmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.rresmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.rcstfrq.setReverse(BaseScreenData.REVERSED);
			sv.rcstfrq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.rcstfrq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rrevfrq.setReverse(BaseScreenData.REVERSED);
			sv.rrevfrq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rrevfrq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.rtaxbas.setReverse(BaseScreenData.REVERSED);
			sv.rtaxbas.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.rtaxbas.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.rrngmnt01.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.rrngmnt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.rrngmnt02.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.rrngmnt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.rrngmnt03.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rrngmnt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.rrngmnt04.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.rrngmnt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.rrngmnt05.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.rrngmnt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.rrngmnt06.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.rrngmnt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.rrngmnt07.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.rrngmnt07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.rrngmnt08.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.rrngmnt08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.rrngmnt09.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.rrngmnt09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.rrngmnt10.setReverse(BaseScreenData.REVERSED);
			sv.rrngmnt10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.rrngmnt10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.rclmbas.setReverse(BaseScreenData.REVERSED);
			sv.rclmbas.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.rclmbas.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		
					    		<div class="input-group" >  
					    			
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

  
	
				    		</div>
					</div></div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							
							<div class="input-group" >  
							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

	
						</div>
				   </div>		</div>
			
			    	<div class="col-md-4">
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						  
							<!--  <div class="input-group"  style="max-width:100px;"> -->
							<table><tr><td>
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
		
						<!-- </div> -->
						</div>
				   </div>	
		    </div>
				   
				 
				   <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label> 
				    		<table>
			    	   <tr>
			    	       
			    	     <td>
				  	<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style=width:90px; >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



		 </td>


                      <td>  &nbsp; </td>
                        <td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
                        <td>  &nbsp; </td>


                      
                        <td> 

  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
   </td>
                      </tr>
                   </table>		
	

				      		 </div></div>
				<div class="col-md-4"> </div><div class="col-md-4"> </div>      </div>
			        
				   
				   
				   
	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Type")%></label>
					    		     <div class="input-group"'style="min-width:100px;">
						    		
<%	
	longValue = sv.rrsktyp.getFormData();  
%>

<% 
	if((new Byte((sv.rrsktyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rrsktyp' id='rrsktyp' style=width:50px;
type='text' 
value='<%=sv.rrsktyp.getFormData()%>' 
maxLength='<%=sv.rrsktyp.getLength()%>' 
size='<%=sv.rrsktyp.getLength()+1%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rrsktyp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rrsktyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rrsktyp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rrsktyp')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.rrsktyp).getColor()== null  ? 
"input_cell" :  (sv.rrsktyp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rrsktyp')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>






	
  		
		<%					
		if(!((sv.lrkclsdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lrkclsdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lrkclsdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
		<div class="col-md-4"> </div><div class="col-md-4"> </div>		    		</div>
				    		
				    		<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Costing Frequency")%></label>
					    		     <div class="input-group" style="max-width:120px">
						    		
<%	
	longValue = sv.rcstfrq.getFormData();  
%>

<% 
	if((new Byte((sv.rcstfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rcstfrq' id='rcstfrq'
type='text' 
value='<%=sv.rcstfrq.getFormData()%>' 
maxLength='<%=sv.rcstfrq.getLength()%>' 
size='<%=sv.rcstfrq.getLength()+3%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rcstfrq)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rcstfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rcstfrq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rcstfrq')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>


<%
	}else { 
%>

class = ' <%=(sv.rcstfrq).getColor()== null  ? 
"input_cell" :  (sv.rcstfrq).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rcstfrq')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>

						    		

				      			     </div>
				    		</div>
					</div>
				    		<!-- <div class="col-md-1"> </div> -->
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Priority")%></label>
							<div class="input-group">
						    		<%	
			qpsf = fw.getFieldXMLDef((sv.rprior).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='rprior' 
type='text'

<%if((sv.rprior).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.rprior) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.rprior);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.rprior) %>'
	 <%}%>

size='<%= sv.rprior.getLength()%>'
maxLength='<%= sv.rprior.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rprior)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.rprior).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.rprior).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.rprior).getColor()== null  ? 
			"input_cell" :  (sv.rprior).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Review Frequency")%></label>
							<div class="input-group"style="max-width:120px">
						    <%	
	longValue = sv.rrevfrq.getFormData();  
%>

<% 
	if((new Byte((sv.rrevfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rrevfrq' id='rrevfrq'
type='text' 
value='<%=sv.rrevfrq.getFormData()%>' 
maxLength='<%=sv.rrevfrq.getLength()%>' 
size='<%=sv.rrevfrq.getLength()+2%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rrevfrq)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rrevfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rrevfrq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rrevfrq')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.rrevfrq).getColor()== null  ? 
"input_cell" :  (sv.rrevfrq).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rrevfrq')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%}longValue = null;} %>

						    		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Ceding Based on Sum At Risk")%></label>
					    		     <div class="input-group">
						    		
<input name='rcedsar' 
type='text'

<%if((sv.rcedsar).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.rcedsar.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.rcedsar.getLength()+4%>'
maxLength='<%= sv.rcedsar.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rcedsar)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.rcedsar).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.rcedsar).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.rcedsar).getColor()== null  ? 
			"input_cell" :  (sv.rcedsar).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Costing Based on Sum Reassured At Risk")%></label>
							<div class="input-group">
						    	
<input name='rcstsar' 
type='text'

<%if((sv.rcstsar).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.rcstsar.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.rcstsar.getLength()+3%>'
maxLength='<%= sv.rcstsar.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rcstsar)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.rcstsar).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.rcstsar).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.rcstsar).getColor()== null  ? 
			"input_cell" :  (sv.rcstsar).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
	
	   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Taxation Basis")%></label>
					    		     <div class="input-group"style="max-width:120px">
						    		
<%	
	longValue = sv.rtaxbas.getFormData();  
%>

<% 
	if((new Byte((sv.rtaxbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rtaxbas' id='rtaxbas'
type='text' 
value='<%=sv.rtaxbas.getFormData()%>' 
maxLength='<%=sv.rtaxbas.getLength()%>' 
size='<%=sv.rtaxbas.getLength()+1%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rtaxbas)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rtaxbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rtaxbas).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rtaxbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.rtaxbas).getColor()== null  ? 
"input_cell" :  (sv.rtaxbas).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rtaxbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%}longValue = null;} %>
						    		

				      			     </div>
				    		</div>
					</div>
				    		<div class="col-md-4"> </div>
				    		
				   <!--  <div class="col-md-3"> -->
						
				 <!--   </div> -->		
			
			    	<div class="col-md-4">
						<div class="form-group" >	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Reserve Calculation for Sum At Risk")%></label>
							<div class="input-group" style="max-width:120px">
						    <%	
	longValue = sv.rresmeth.getFormData();  
%>

<% 
	if((new Byte((sv.rresmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rresmeth' id='rresmeth'
type='text' 
value='<%=sv.rresmeth.getFormData()%>' 
maxLength='<%=sv.rresmeth.getLength()%>' 
size='<%=sv.rresmeth.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rresmeth)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rresmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rresmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rresmeth')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.rresmeth).getColor()== null  ? 
"input_cell" :  (sv.rresmeth).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rresmeth')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%}longValue = null;} %>

				      			     </div>
										
						</div>
				   </div>	
		    </div>
	
	   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Basis")%></label>
					    		     <div class="input-group"style="max-width:120px">
						    		
<%	
	longValue = sv.rclmbas.getFormData();  
%>

<% 
	if((new Byte((sv.rclmbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='rclmbas' id='rclmbas'
type='text' 
value='<%=sv.rclmbas.getFormData()%>' 
maxLength='<%=sv.rclmbas.getLength()%>' 
size='<%=sv.rclmbas.getLength()+1%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(rclmbas)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.rclmbas).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.rclmbas).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
<button class="btn btn-info"style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rclmbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.rclmbas).getColor()== null  ? 
"input_cell" :  (sv.rclmbas).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('rclmbas')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>


<%}longValue = null;} %>

						    		

				      			     </div>
				    		</div>
					</div>
				    		<div class="col-md-4"> </div>
				    		
				  <!--   <div class="col-md-3"> -->
						
				   <!-- </div>	 -->	
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Proportionate Processing Applicable")%></label>
							<div class="input-group">
						    	
<input name='proppr' 
type='text'

<%if((sv.proppr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.proppr.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.proppr.getLength()+3%>'
maxLength='<%= sv.proppr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(proppr)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.proppr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.proppr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.proppr).getColor()== null  ? 
			"input_cell" :  (sv.proppr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
	
<div class="row">	
			    	<div class="col-md-6"> 
			    			<label><%=resourceBundleHandler.gettingValueFromBundle("Arrangement Type Hierarchy")%></label>
			    	</div></div>
			    	
			    		
			    	<div class="row">	
			    		<div class="col-md-2"style="min-width:150px;"> 	
			    		<div class="input-group">
			  
	
		<%=smartHF.getHTMLSpaceVar(fw, sv.rrngmnt01).replaceAll("width:100%","width:125%")%>   
	<%=smartHF.getHTMLF4NSVar(fw, sv.rrngmnt01)%>   
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    			 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt02).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt02)%>   
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    		 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt03).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt03)%>
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt04).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt04)%>
			    	
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt05).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt05)%>
			    	</div></div></div>
			    	
			    	
			    	 <BR>
			    	<div class="row">
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt06).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt06)%>
			    	</div></div>
			   
			   
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt07).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt07)%>
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt08).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt08)%>
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt09).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt09)%>
			    	</div></div>
			    	
			    		<div class="col-md-2"style="min-width:150px;"> 
			    	<div class="input-group">
			    	
	 	<%=smartHF.getHTMLSpaceVar( fw, sv.rrngmnt10).replaceAll("width:100%","width:125%")%>   
   	<%=smartHF.getHTMLF4NSVar( fw, sv.rrngmnt10)%>
			    	</div></div>
			    	
			    	

	
	
</div>
</div>
</div>
<%}%>	
                                                           
<%@ include file="/POLACommon2NEW.jsp"%>


<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
	<style>
		
		
		div[id*='rrngmnt']{padding-right:2px !important} 
		
				
	</style> 
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
