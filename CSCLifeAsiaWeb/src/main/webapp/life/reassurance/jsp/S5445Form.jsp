

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5445";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S5445ScreenVars sv = (S5445ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Refund Frequency ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rounding  (Y/N)");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flat Refund Fee     Currency ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount   ");%>
<!-- ILIFE-2649 Life Cross Browser - Sprint 3 D1 : Task 4  -->
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    			<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<!-- <div class="input-group"style="max-width:100px;"> -->
						    		<table><tr><td>
			<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
	</td></tr></table>

				      			   <!--   </div> -->
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
						    		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style=width:90px;>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <!-- <div class="label_txt"/> -->
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    			<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  id="to_cell" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				 <div class="col-md-4"> </div><div class="col-md-4"> </div>   		</div>
				    		
				    		
				    		
				    			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Frequency")%></label>
					    		     <div class="input-group"style="max-width:120px">
						    		
<%	
	longValue = sv.frequency.getFormData();  
%>

<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='frequency' id='frequency'
type='text' 
value='<%=sv.frequency.getFormData()%>' 
maxLength='<%=sv.frequency.getLength()%>' 
size='<%=sv.frequency.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(frequency)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"  >

<%
	}else if((new Byte((sv.frequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('frequency')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.frequency).getColor()== null  ? 
"input_cell" :  (sv.frequency).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('frequency')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
<%}longValue = null;} %>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
								   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						   <%
ScreenDataHelper rounding = new ScreenDataHelper(fw.getVariables(), sv.rndgrqd);
%>
<td style="visibility:<%=rounding.visibility()%>" width='251'>
<LABEL>
<%=resourceBundleHandler.gettingValueFromBundle("Rounding")%>
</LABEL>
<br/>
<%if (rounding.isReadonly()) {%>
<input style="width:60px;" name='rndgrqd' type='text' value='<%=rounding.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)' onHelp='return fieldHelp(rndgrqd)'
       onKeyUp='return checkMaxLength(this)'>
<%} else {%>

<select name="rndgrqd" type="list" class="<%=rounding.clazz()%>" style="max-width:60px">
  <option <%=rounding.selected("Y")%> value="Y">Y</option>
  <option <%=rounding.selected("N")%> value="N">N</option>
</select>
<%}%>
						</div>   </td> 
				   </div>	
		    </div>


	<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Refund Fee     Currency")%></label>
					    		     <div class="input-group"style="max-width:120px">
						    		
<%	
	longValue = sv.currcode.getFormData();  
%>

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='currcode' id='currcode'
type='text' 
value='<%=sv.currcode.getFormData()%>' 
maxLength='<%=sv.currcode.getLength()%>' 
size='<%=sv.currcode.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(currcode)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.currcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
				 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";  type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	

<%
	}else { 
%>

class = ' <%=(sv.currcode).getColor()== null  ? 
"input_cell" :  (sv.currcode).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

		 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	

<%}longValue = null;} %>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
								   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></label>
							<div class="input-group" style="max-width:100px">
						    	<%	
			qpsf = fw.getFieldXMLDef((sv.refundfe).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.refundfe,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input style="width:155px;" name='refundfe' 
type='text'

<%if((sv.refundfe).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.refundfe.getLength(), sv.refundfe.getScale(),3)%>'
maxLength='<%= sv.refundfe.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(refundfe)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.refundfe).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.refundfe).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.refundfe).getColor()== null  ? 
			"input_cell" :  (sv.refundfe).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
		    
		    </div></div>
<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>
