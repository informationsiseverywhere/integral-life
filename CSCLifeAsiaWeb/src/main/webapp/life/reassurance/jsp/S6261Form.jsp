<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6261";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.reassurance.screens.*" %>

<%S6261ScreenVars sv = (S6261ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurance No ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commencement Date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Termination Date ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay Method ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay Frequency ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Statement ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Statement ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Payee ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factoring House ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company Agent No ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Statement Amount ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client Details  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank account details  ");%>

<%{
		if (appVars.ind21.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
		}
		appVars.setFieldChange(sv.clntsel, appVars.ind22);
		if (appVars.ind01.isOn()) {
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.rapaymth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.rapaymth.setReverse(BaseScreenData.REVERSED);
		}
		appVars.setFieldChange(sv.rapaymth, appVars.ind27);
		if (appVars.ind09.isOn()) {
			sv.rapaymth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.rapaymth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.rapayfrq.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.rapayfrq.setReverse(BaseScreenData.REVERSED);
		}
		appVars.setFieldChange(sv.rapayfrq, appVars.ind28);
		if (appVars.ind10.isOn()) {
			sv.rapayfrq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.rapayfrq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.rapayee.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.rapayee.setReverse(BaseScreenData.REVERSED);
		}
		appVars.setFieldChange(sv.rapayee, appVars.ind26);
		if (appVars.ind08.isOn()) {
			sv.rapayee.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rapayee.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.currcode.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.currcode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.currcode.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.currcode, appVars.ind25);
		if (!appVars.ind30.isOn()) {
			sv.currcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.agntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.minsta.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.minsta.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.minsta.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.minsta.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.clientind.setReverse(BaseScreenData.REVERSED);
			sv.clientind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.clientind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.reasst.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.reasst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.reasst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.reasst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.termdtDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind05.isOn()) {
			sv.termdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.termdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.termdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.commdtDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.commdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.commdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.commdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.nxtpayDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind07.isOn()) {
			sv.nxtpayDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.nxtpayDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.nxtpayDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
<div class="row">
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
<table><tr><td>
 <div class="form-group">
  <%
                                         if ((new Byte((sv.clntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 150px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.clntsel)%>
                                                        
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 19px; border-bottom-width: 2px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
</div>
	</td><td>
  		
		<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos' style="width:100px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>



</div>
</div>
<div class="row">
<div class="col-md-3">
<div class="form-group">	
<label><%=resourceBundleHandler.gettingValueFromBundle("Reassurance No")%></label>

		<%					
		if(!((sv.rasnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rasnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Commencement Date")%></label>
<div class="input-group" style="width:70px;">

 
 <% if ((new Byte((sv.commdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.commdtDisp)%>
                                       
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="commdtDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.commdtDisp, (sv.commdtDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>




</div>
</div></div>
<div class="col-md-3"></div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Termination Date")%></label>
<div class="input-group" style="width:100px;">
<% if ((new Byte((sv.termdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.termdtDisp)%>
                                       
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="termdtDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.termdtDisp, (sv.termdtDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
</div>
</div>
</div></div>
<div class="row">
<div class="col-md-5">

<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
<table><tr><td style="min-width:100px;">
<div class="form-group">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.reasst)%>
 </td><td>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.reasst).replace("<img","<img id='searchImg' ")%>
	</td><td>
  		</div>
		<%					
		if(!((sv.reastdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reastdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reastdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos' style="min-width:100px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
	</td></tr></table>
	</div>
	<div class="col-md-1"></div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
<table><tr><td>
<%					
		if(!((sv.branch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.branchnm.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchnm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchnm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
  </div>
  </div>
	
	</div>
<div class="row">
<div class="col-md-5">

<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Method")%></label>
<table><tr><td style="min-width:100px;">
<div class="form-group">
  <%=smartHF.getRichTextInputFieldLookup(fw, sv.rapaymth)%>
  </td><td>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.rapaymth).replace("<img","<img id='searchImg' ")%>
</td><td>
</div>
		<%					
		if(!((sv.paymthd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paymthd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paymthd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos' style="min-width:120px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 </td></tr></table>
  </div>
<div class="col-md-1"></div>
<div class="col-md-5">

<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Frequency")%></label>
<table><tr><td style="min-width:100px;">
<div class="form-group">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.rapayfrq)%>
 </td><td>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.rapayfrq).replace("<img","<img id='searchImg' ")%>
  		</td><td>
</div>
		<%					
		if(!((sv.rapayfrqd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rapayfrqd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rapayfrqd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos' style="min-width:120px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </td></tr></table>
  </div>
	
	</div>
	
 
<div class="row">
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Last Statement")%></label>
	<div class="input-group" style="width:100px;">
		<%					
		if(!((sv.stmtdtDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.stmtdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.stmtdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
  </div></div>
  <div class="col-md-3"></div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Next Statement")%></label> 
<div class="input-group" style="width:100px;">
<% if ((new Byte((sv.nxtpayDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.nxtpayDisp)%>
                                       
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="nxtpayDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.nxtpayDisp, (sv.nxtpayDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
</div>
</div></div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
<table><tr><td>
<%					
		if(!((sv.bankkey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankkey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankkey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
 </div>
 </div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Account")%></label> 
<table><tr><td>
		<%					
		if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td>



	
  		
		<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
		</div>
		</div>
 </div> 
<div class="row">
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Account Payee")%></label>
<div class="input-group">


<%=smartHF.getRichTextInputFieldLookup(fw, sv.rapayee)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.rapayee).replace("<img","<img id='searchImg' ")%><!-- IFSU-414 -->








	
  		
		<%					
		if(!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos'style="min-width:100px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
  </div>
  </div>
  </div>
 <div class="row">
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Factoring House")%></label> 

		<%					
		if(!((sv.facthous.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
  </div> 
  <div class="col-md-3"></div>
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<table><tr><td style="min-width:100px;">
<div class="form-group">
  



<%=smartHF.getRichTextInputFieldLookup(fw, sv.currcode)%>
</td><td>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.currcode).replace("<img","<img id='searchImg' ")%><!-- IFSU-414 -->
</td><td>
</div>
		<%					
		if(!((sv.currdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos'style="min-width:110px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table></div>
  </div>
 <div class="row">
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Company Agent No")%></label> 
<table><tr><td>
<div class="form-group">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.agntsel)%>
 </td><td>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.agntsel).replace("<img","<img id='searchImg' ")%><!-- IFSU-414 -->
</td><td>
</div>
<%					
		if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> iconPos' style="min-width:100px; margin-left:5px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr></table>
</div>

<div class="col-md-2"></div>
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Statement Amount")%></label>
<div class="input-group" style="width:160px;">
<%	
			qpsf = fw.getFieldXMLDef((sv.minsta).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.minsta,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='minsta' 
type='text'

	
	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.minsta.getLength(), sv.minsta.getScale(),3)%>'
maxLength='<%= sv.minsta.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(minsta)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.minsta).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.minsta).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.minsta).getColor()== null  ? 
			"input_cell" :  (sv.minsta).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%> 
</div>
</div>
 </div>
</div>
<Div id='mainForm_OPTS' style='visibility:hidden'>
<%=smartHF.getMenuLink(sv.clientind, resourceBundleHandler.gettingValueFromBundle("Client Details"))%>
<%=smartHF.getMenuLink(sv.ddind, resourceBundleHandler.gettingValueFromBundle("Bank account details"))%>

</div>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

