

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6704";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6704ScreenVars sv = (S6704ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  Agency/Cat/Year/Area/Br /BA /BS /BP /BT /Type /CRT/Overid/Pstat/Curr");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Journal for account month ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account year ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account year");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Journal");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Count   ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage count   ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Premium   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium   ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission       ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Time ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.acctyr.setReverse(BaseScreenData.REVERSED);
			sv.acctyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.acctyr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.acyr.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.acmn.setReverse(BaseScreenData.REVERSED);
			sv.acmn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.acmn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind17.isOn()) {
			generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind17.isOn()) {
			sv.descrip.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-1"> 
				    		<div class="form-group">


<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>





	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div></div>
<br/>




<div class="row">	
			    	<div class="col-md-2" style="min-width:145px;!important"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.agntsel)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 


 <div class="col-md-1"></div> 

<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Category")%></label>
					    		 <div class="input-group"> 
						    		<%=smartHF.getHTMLVarExt(fw, sv.statcat)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('statcat')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div> 
				    		</div>
			    	</div> 
<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.acctyr)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('acctyr')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Area")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.aracde)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
</div>
<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.cntbranch)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('cntbranch')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 


<div class="col-md-1"></div> 
<div class="col-md-2"> 
				    		<div class="form-group">  	  


<label><%=resourceBundleHandler.gettingValueFromBundle("BA")%></label>







<input name='bandage' 
type='text'

<%

		formatValue = (sv.bandage.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandage.getLength()%>'
maxLength='<%= sv.bandage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandage)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandage).getColor()== null  ? 
			"input_cell" :  (sv.bandage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div>

<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">  	  
<label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>







<input name='bandsa' 
type='text'

<%

		formatValue = (sv.bandsa.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandsa.getLength()%>'
maxLength='<%= sv.bandsa.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandsa)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandsa).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandsa).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandsa).getColor()== null  ? 
			"input_cell" :  (sv.bandsa).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>







<input name='bandprm' 
type='text'

<%

		formatValue = (sv.bandprm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandprm.getLength()%>'
maxLength='<%= sv.bandprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandprm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandprm).getColor()== null  ? 
			"input_cell" :  (sv.bandprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></div>

	<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>







<input name='bandtrm' 
type='text'

<%

		formatValue = (sv.bandtrm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandtrm.getLength()%>'
maxLength='<%= sv.bandtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandtrm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandtrm).getColor()== null  ? 
			"input_cell" :  (sv.bandtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
<div class="col-md-1"></div> 
<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.chdrtype)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrtype')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 

<div class="col-md-1"></div>
<div class="col-md-2"> 
<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("CRT")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.crtable)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('crtable')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
<div class="col-md-1"></div>
<div class="col-md-2"> 
<div class="form-group">  
<label><%=resourceBundleHandler.gettingValueFromBundle("Over Id")%></label>


<input name='ovrdcat' 
type='text'

<%

		formatValue = (sv.ovrdcat.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ovrdcat.getLength()%>'
maxLength='<%= sv.ovrdcat.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ovrdcat)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ovrdcat).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ovrdcat).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ovrdcat).getColor()== null  ? 
			"input_cell" :  (sv.ovrdcat).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></div>
<div class="row">
<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Pstat")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.pstatcd)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('pstatcd')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.cntcurr)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('cntcurr')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> </div>
<br/>
<div class="row">
<div class="col-md-2"> 
				    		<div class="form-group">  
<label><%=resourceBundleHandler.gettingValueFromBundle("Journal for account month")%></label>
<div class="input-group">






<%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acmn);
			
			if(!((sv.acmn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
  




	
  		
		<%					
		if(!((sv.mthldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>
<div class="col-md-1"></div>
<div class="col-md-2"> 
				    		<div class="form-group">  
<label><%=resourceBundleHandler.gettingValueFromBundle("Account year")%></label>





	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acyr);
			
			if(!((sv.acyr.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>
<div class="col-md-1"></div>
<div class="col-md-2" style="min-width:200px;"> 
				    		<div class="form-group">  
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>





	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
<br /><br />
</div></div></div>



<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account year")%>
</div>


<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Journal")%>
</div>

<br>
<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Count")%>
</div>

<td width='251'>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stcmth);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stcmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stcmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:71px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:71px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

<br>

<td width='251'></td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Coverage count")%>
</div>


<td width='251'>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stvmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stvmth);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stvmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stvmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:71px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:71px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <br>
	



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Annual Premium")%>
</div>


<td width='251'>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	<br>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%>
</div>


<td width='251'>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stsmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	<br>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
</div>


<td width='251'>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stmmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stmmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stmmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	<br>



<!-- <td width='251'></td></tr><tr style='height:22px;'><td width='251'> -->

<%-- <div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Date")%>
</div> --%>


<!-- <td width='251'> -->

	
  		
		<%-- <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			 --%>
				<%-- <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	 --%>
		<%-- <%
		longValue = null;
		formatValue = null;
		%>
   --%>
	
<!-- <br> -->


<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Time")%>
</div>


<td width='251'>

	
  		
		<%					
		if(!((sv.transactionTime.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.transactionTime.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.transactionTime.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	<br>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("User")%>
</div>


<td width='251'>

	
  		
		<%					
		if(!((sv.user.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.user.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.user.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account  Agency/Cat/Year/Area/Br /BA /BS /BP /BT /Type /CRT/Overid/Pstat/Curr")%>
</div>

</tr>
</table></div><br/>

</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>
 
