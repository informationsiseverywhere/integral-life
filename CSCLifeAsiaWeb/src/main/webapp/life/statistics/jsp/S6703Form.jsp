

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6703";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6703ScreenVars sv = (S6703ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  Agency/Cat/Year/Area/Br / BA/BS /BP /BT /Type /CRT/Overid/Pstat/Curr");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Balance for account month ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.statcat.setReverse(BaseScreenData.REVERSED);
			sv.statcat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.statcat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.acctyr.setReverse(BaseScreenData.REVERSED);
			sv.acctyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.acctyr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.aracde.setReverse(BaseScreenData.REVERSED);
			sv.aracde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.aracde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.cntbranch.setReverse(BaseScreenData.REVERSED);
			sv.cntbranch.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.cntbranch.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.bandage.setReverse(BaseScreenData.REVERSED);
			sv.bandage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.bandage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.bandsa.setReverse(BaseScreenData.REVERSED);
			sv.bandsa.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.bandsa.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.bandprm.setReverse(BaseScreenData.REVERSED);
			sv.bandprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.bandprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.bandtrm.setReverse(BaseScreenData.REVERSED);
			sv.bandtrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.bandtrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.chdrtype.setReverse(BaseScreenData.REVERSED);
			sv.chdrtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.chdrtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.crtable.setReverse(BaseScreenData.REVERSED);
			sv.crtable.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.cntcurr.setReverse(BaseScreenData.REVERSED);
			sv.cntcurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.cntcurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.acmn.setReverse(BaseScreenData.REVERSED);
			sv.acmn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.acmn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.pstatcd.setReverse(BaseScreenData.REVERSED);
			sv.pstatcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.pstatcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.ovrdcat.setReverse(BaseScreenData.REVERSED);
			sv.ovrdcat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.ovrdcat.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company  ")%></label>
	                       <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	                      </div>
				    </div>
				</div>
				 <div class="row">
                     <div class="col-md-2" style="min-width:143px;">
	                     <div class="form-group" >
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
	                        <div class="input-group"style="max-width:150px;">
	                       <input name='agntsel' 
	                       id='agntsel' 
type='text' 
value='<%=sv.agntsel.getFormData()%>' 
maxLength='<%=sv.agntsel.getLength()%>' 
size='<%=sv.agntsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(agntsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.agntsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.agntsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.agntsel).getColor()== null  ? 
"input_cell" :  (sv.agntsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %> 						</div>
	                      </div>
				    </div>
				    <div class="col-md-1"></div>
				    
				    <div class="col-md-2">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Category")%></label>
	                        <div class="input-group" style="max-width:130px;">
	                       <input name='statcat' 
	                       id='statcat' 
type='text' 
value='<%=sv.statcat.getFormData()%>' 
maxLength='<%=sv.statcat.getLength()%>' 
size='<%=sv.statcat.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(statcat)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.statcat).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.statcat).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('statcat')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.statcat).getColor()== null  ? 
"input_cell" :  (sv.statcat).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('statcat')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>

	                       
	                      </div>
				    </div>
				    </div>
				   <div class="col-md-1"></div>
				     <div class="col-md-2">
	                     <div class="form-group" style="max-width:70px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Year")%></label>
	                         
	                       	<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acctyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
				    </div>
				    </div>
				    <div class="col-md-1"></div>
				    <div class="col-md-2">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Area")%></label>
	                       <div class="input-group"style="max-width:130px;">
	                       <input name='aracde' 
	                        id='aracde' 
type='text' 
value='<%=sv.aracde.getFormData()%>' 
maxLength='<%=sv.aracde.getLength()%>' 
size='<%=sv.aracde.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.aracde).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.aracde).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.aracde).getColor()== null  ? 
"input_cell" :  (sv.aracde).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>
	                      </div>
				    </div>
				</div>
				</div>
				<div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
	                       <div class="input-group"style="max-width:130px;">
	                       <input name='cntbranch' 
	                       id='cntbranch' 
type='text' 
value='<%=sv.cntbranch.getFormData()%>' 
maxLength='<%=sv.cntbranch.getLength()%>' 
size='<%=sv.cntbranch.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(cntbranch)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.cntbranch).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.cntbranch).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('cntbranch')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.cntbranch).getColor()== null  ? 
"input_cell" :  (sv.cntbranch).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('cntbranch')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%} %>
	                      </div>
				    </div>
				     </div>
				    <div class="col-md-3">
	                     <div class="form-group" style="max-width:100px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("BA")%></label>
	                       <input name='bandage' 
type='text'

<%

		formatValue = (sv.bandage.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandage.getLength()%>'
maxLength='<%= sv.bandage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandage)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandage).getColor()== null  ? 
			"input_cell" :  (sv.bandage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div>
				    </div>
				     <div class="col-md-3">
	                     <div class="form-group"style="max-width:150px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>
	                       <input name='bandsa' 
	                       id='bandsa' 
type='text'

<%

		formatValue = (sv.bandsa.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandsa.getLength()%>'
maxLength='<%= sv.bandsa.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandsa)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandsa).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandsa).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandsa).getColor()== null  ? 
			"input_cell" :  (sv.bandsa).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div>
				    </div>
				    <div class="col-md-3">
	                     <div class="form-group" style="max-width:100px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>
	                       <input name='bandprm' 
type='text'

<%

		formatValue = (sv.bandprm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandprm.getLength()%>'
maxLength='<%= sv.bandprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandprm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandprm).getColor()== null  ? 
			"input_cell" :  (sv.bandprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div>
				    </div>
				</div>
				
				<div class="row">
                     <div class="col-md-3">
	                     <div class="form-group"style="max-width:100px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>
	                       <input name='bandtrm' 
type='text'

<%

		formatValue = (sv.bandtrm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandtrm.getLength()%>'
maxLength='<%= sv.bandtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandtrm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandtrm).getColor()== null  ? 
			"input_cell" :  (sv.bandtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div>
				    </div>
				    <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>
	                       <div class="input-group"style="max-width:130px;">
	                       <input name='chdrtype' id='chdrtype'
type='text' 
value='<%=sv.chdrtype.getFormData()%>' 
maxLength='<%=sv.chdrtype.getLength()%>' 
size='<%=sv.chdrtype.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrtype)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrtype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrtype')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.chdrtype).getColor()== null  ? 
"input_cell" :  (sv.chdrtype).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrtype')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>

	                         </div>
	                      </div>
				    </div>
				     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("CRT")%></label>
	                      <div class="input-group" style="max-width:140px;">
	                       <input name='crtable' id='crtable'
type='text' 
value='<%=sv.crtable.getFormData()%>' 
maxLength='<%=sv.crtable.getLength()%>' 
size='<%=sv.crtable.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(crtable)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.crtable).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.crtable).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('crtable')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.crtable).getColor()== null  ? 
"input_cell" :  (sv.crtable).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('crtable')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>
	                      </div>
				   		 </div>
				      </div>
				    <div class="col-md-3">
	                     <div class="form-group"style="max-width:100px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Over Id")%></label>
	                       <input name='ovrdcat' 
type='text'

<%

		formatValue = (sv.ovrdcat.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ovrdcat.getLength()%>'
maxLength='<%= sv.ovrdcat.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ovrdcat)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ovrdcat).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ovrdcat).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ovrdcat).getColor()== null  ? 
			"input_cell" :  (sv.ovrdcat).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div>
				    </div>
				</div>
				
				<div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Pstat")%></label>
	                         <div class="input-group" style="max-width:130px;">
	                       <input name='pstatcd' 
	                       id='pstatcd' 
type='text' 
value='<%=sv.pstatcd.getFormData()%>' 
maxLength='<%=sv.pstatcd.getLength()%>' 
size='<%=sv.pstatcd.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(pstatcd)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.pstatcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.pstatcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('pstatcd')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.pstatcd).getColor()== null  ? 
"input_cell" :  (sv.pstatcd).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('pstatcd')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>
	                      </div>
				    </div>
				     </div>
				    <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                        <div class="input-group" style="max-width:130px;">
	                       <input name='cntcurr' id='cntcurr'
type='text' 
value='<%=sv.cntcurr.getFormData()%>' 
maxLength='<%=sv.cntcurr.getLength()%>' 
size='<%=sv.cntcurr.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(cntcurr)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.cntcurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.cntcurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('cntcurr')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.cntcurr).getColor()== null  ? 
"input_cell" :  (sv.cntcurr).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('cntcurr')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} %>
	                      </div>
				    </div>			    
				 </div>
				 </div>
				
				<div class="row">
                     <div class="col-md-3">
	                     <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Balance for account month")%></label>
	                        <div class="input-group" style="min-width:150px"> 
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acmn' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acmn) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acmn);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acmn) %>'
	 <%}%>

size='<%= sv.acmn.getLength()%>'
maxLength='<%= sv.acmn.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acmn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acmn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acmn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acmn).getColor()== null  ? 
			"input_cell" :  (sv.acmn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                      </div></div>
				    </div>
				  
				</div>
		</div>
</div>
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account  Agency/Cat/Year/Area/Br / BA/BS /BP /BT /Type /CRT/Overid/Pstat/Curr")%>
</div>

</tr></table></div><br/>


<%@ include file="/POLACommon2NEW.jsp"%>

