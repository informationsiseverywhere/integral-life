

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SJ517";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>
<%Sj517ScreenVars sv = (Sj517ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Year   ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month  ");%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
  					<div class="input-group">
  <%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	<%					
		if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
 </div>       
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label> 
       <div class="form-group" style="max-width:100px;">    <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>                                       
</div>
<div class="col-md-1"></div>
<div class="col-md-2">
	                   <div class="form-group" style="min-width:200px;">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
       <div class="input-group" style="max-width:100px;">
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="max-width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
</div>	
</div>		
</div>	
</div>   

<div class="row">
 <%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>
	
<div class="col-md-2">
	                 
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label> 
     <div class="form-group" style="max-width:50px;">     <div style="position: relative; margin-left:1px;max-width:100px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div>
	      

	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
	<div class="col-md-2"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label> 
             <div style="position: relative; margin-left:1px;max-width:120px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>                 	
	</div>                             
</div>
<div class="col-md-2"></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
  <div class="form-group" style="max-width:100px;">                               <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>' style="max-width:120px;">
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
        </div>
        </div>
        </div>
        </div>
        <div class="row">
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Year")%></label> 
<table><tr><td>
<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='acctyr' 
type='text'

<%if((sv.acctyr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td>

	<%	
			qpsf = fw.getFieldXMLDef((sv.acctmnth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='acctmnth' 
type='text'

<%if((sv.acctmnth).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctmnth) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctmnth);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctmnth) %>'
	 <%}%>

size='<%= sv.acctmnth.getLength()%>'
maxLength='<%= sv.acctmnth.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctmnth)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctmnth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctmnth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctmnth).getColor()== null  ? 
			"input_cell" :  (sv.acctmnth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table>
<div style='visibility:hidden;'>

<table>
<tr style='height:22px;'><td width='251'>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Number")%>
</label>


<br/>


	

</td>

<td width='251'></td><td width='251'>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Year")%>
</label>


	
  		
		
	

</td></tr>

<tr style='height:22px;'><td width='251'>


</td>

<td width='251'></td><td width='251'>
</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%>
</label>
  
</div>
</div>
</td>
</tr>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

