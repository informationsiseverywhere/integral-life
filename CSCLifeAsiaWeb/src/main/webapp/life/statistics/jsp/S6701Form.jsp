

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6701";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6701ScreenVars sv = (S6701ScreenVars) fw.getVariables();%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Journal Create");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Journal Inquiry");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F - Account Balance Inquiry");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-3">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Journal Create")%>
					</b></label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Journal Inquiry")%>
					</b></label>			
			    </div>		
			     <div class="col-md-5">
					  <label class="radio-inline">
					  <b> <%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Account Balance Inquiry")%>
					</b>   </label>
				</div>        
			</div>
		
			
		</div>
	</div>	


<%@ include file="/POLACommon2NEW.jsp"%>

