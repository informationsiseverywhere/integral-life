<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6713";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<!-- ILIFE 2784 starts/ends  -->
<%@ page import="com.csc.life.statistics.screens.*" %>
<%S6713ScreenVars sv = (S6713ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account   Cat /Year/Fund /Sect/Subsect/Reg /Br /BA /BS /BP /BT /ComYr/Pst/Cur");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Journal for account month ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Year ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current account");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjustment");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjusted Account");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Count ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Premium ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Insured    ");%>

<%{
	/*bug #ILIFE-1070 start*/
		if (appVars.ind01.isOn()) {
			sv.stcmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stpmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stsmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stimthAdj.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.stcmthAdj.setColor(BaseScreenData.RED);
			sv.stpmthAdj.setColor(BaseScreenData.RED);
			sv.stsmthAdj.setColor(BaseScreenData.RED);
			sv.stimthAdj.setColor(BaseScreenData.RED);
		}
		/*bug #ILIFE-1070 end*/
	}%>

<!-- ILIFE-2664 Life Cross Browser -Coding and UT- Sprint 3 D3: Task 5  starts -->
<style>
.input_cell {width:118px;}
</style>
<!-- ILIFE-2664 Life Cross Browser -Coding and UT- Sprint 3 D3: Task 5  ends -->

<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-1">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
  		<div class="form-group" style="max-width:50px;">
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
		<div class="form-group">				 
						<label><%=resourceBundleHandler.gettingValueFromBundle("SCategory")%></label>
  		<div class="input-group" style="max-width:120px;">

  <%=smartHF.getRichTextInputFieldLookup(fw, sv.statcat)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statcat).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
				<div class="form-group">		 
						<label><%=resourceBundleHandler.gettingValueFromBundle("AcctYr")%></label>
  		<div class="form-group" style="max-width:100px;">

	<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acctyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("SttrFund")%></label>
  		<div class="input-group" style="max-width:100px;">

 <%=smartHF.getRichTextInputFieldLookup(fw, sv.statFund)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statFund).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
				<div class="form-group">	 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Sect")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.statSect)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statSect).replace("<img","<img id='searchImg' ")%>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Statutory Subsection")%></label>
  		<div class="input-group" style="max-width:120px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.stsubsect)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.stsubsect).replace("<img","<img id='searchImg' ")%>
</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group" style="max-width:100px;">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.register)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.register).replace("<img","<img id='searchImg' ")%>
 

</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Br")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.cntbranch)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntbranch).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BA")%></label>
  		<div class="form-group" style="max-width:100px;">


<input name='bandage' 
type='text'

<%

		formatValue = (sv.bandage.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandage.getLength()%>'
maxLength='<%= sv.bandage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandage)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandage).getColor()== null  ? 
			"input_cell" :  (sv.bandage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>
  		<div class="form-group" style="max-width:60px;">


<input name='bandsa' 
type='text'

<%

		formatValue = (sv.bandsa.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandsa.getLength()%>'
maxLength='<%= sv.bandsa.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandsa)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandsa).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandsa).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandsa).getColor()== null  ? 
			"input_cell" :  (sv.bandsa).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>
	<div class="form-group" style="max-width:60px;">

<input name='bandprm' 
type='text'

<%

		formatValue = (sv.bandprm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandprm.getLength()%>'
maxLength='<%= sv.bandprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandprm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandprm).getColor()== null  ? 
			"input_cell" :  (sv.bandprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>
	<div class="form-group" style="max-width:60px;">

<input name='bandtrm' 
type='text'

<%

		formatValue = (sv.bandtrm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandtrm.getLength()%>'
maxLength='<%= sv.bandtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandtrm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandtrm).getColor()== null  ? 
			"input_cell" :  (sv.bandtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-3">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("ComYr")%></label>
	<div class="form-group" style="max-width:120px;">



	<%	
			qpsf = fw.getFieldXMLDef((sv.commyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='commyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.commyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
	 <%}%>

size='<%= sv.commyr.getLength()%>'
maxLength='<%= sv.commyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(commyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.commyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.commyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.commyr).getColor()== null  ? 
			"input_cell" :  (sv.commyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
</div>

<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Pst")%></label>
  		<div class="input-group" style="max-width:100px;">
  <%=smartHF.getRichTextInputFieldLookup(fw, sv.pstatcd)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.pstatcd).replace("<img","<img id='searchImg' ")%>


</div></div></div>
<div class="col-md-2">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
  		<div class="input-group" style="max-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.cntcurr)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntcurr).replace("<img","<img id='searchImg' ")%>
</div></div>
</div></div>

<div class="row">
<div class="col-md-3">
					<div class="form-group">	 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Journal for account month")%></label>
  		<table><tr><td>
		<%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acmn);
			
			if(!((sv.acmn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
</td><td>




	
  		
		<%					
		if(!((sv.mthldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
</div>
</div>

<div class="col-md-3">
					 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Account Year")%></label>

	<div class="form-group" style="max-width:100px;">	
  		
		<%					
		if(!((sv.acyr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
</div>
</div>

<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-2">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Current Account")%></label>
	</div>
	<div class="col-md-1"></div>
	<div class="col-md-2">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%></label>
	</div>
	<div class="col-md-1"></div>
	<div class="col-md-2">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Adjusted Account")%></label>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Contract Count")%>
		</label>
	</div>
	<div class="col-md-3">
	
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stcmth);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stcmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stcmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
		<!-- snayeni for ILIFE-448 STARTS -->
				<div class="output_cell integer_data"  style="width:140px;">
					<%= XSSFilter.escapeHtml(formatValue)%>

				</div>
<!-- ENDS-->
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"></div><!-- padding Added by pmujavadiya -->
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	<div class="col-md-3">
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stcmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
	%>

	<input name='stcmthAdj' 
	type='text'
<%if((sv.stcmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="width:140px;text-align: right"<% }%>
	value='<%=valueThis%>'
			 <%
	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stcmthAdj.getLength(), sv.stcmthAdj.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stcmthAdj.getLength(), sv.stcmthAdj.getScale(),3)-2%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stcmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
		<% 
			if((new Byte((sv.stcmthAdj).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
			readonly="true"
			class="output_cell"
		<%
			}else if((new Byte((sv.stcmthAdj).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		%>	
				class="bold_cell" 
		
		<%
			}else { 
		%>
		
			class = ' <%=(sv.stcmthAdj).getColor()== null  ? 
					"input_cell integer_data":  (sv.stcmthAdj).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell integer_data"%>'
		 
		<%
			} 
		%>
		>
		
	</div>
	<div class="col-md-3">
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stcmtho);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stcmtho,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stcmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"  style="width:140px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Annual Premium")%>
		</label>
	</div>
	<div class="col-md-3">
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"  style="width:140px;margin-left:0px;margin-right:3px;">	<!-- margin Added by pmujavadiya -->
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"> </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	<div class="col-md-3">
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stpmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stpmthAdj' 
type='text'
<%if((sv.stpmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="width:140px;text-align: right"<% }%>
	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stpmthAdj.getLength(), sv.stpmthAdj.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stpmthAdj.getLength(), sv.stpmthAdj.getScale(),3)-4%>'
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stpmthAdj)' onKeyUp='return checkMaxLength(this)'

onKeyPress="return digitsOnly(this,<%=qpsf.getDecimals()%> ); "
decimal='<%=qpsf.getDecimals()%>'
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'

		
		<% 
			if((new Byte((sv.stpmthAdj).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
			readonly="true"
			class="output_cell"
		<%
			}else if((new Byte((sv.stpmthAdj).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		%>	
				class="bold_cell" 
		
		<%
			}else { 
		%>
		
			class = ' <%=(sv.stpmthAdj).getColor()== null  ? 
					"input_cell integer_data" :  (sv.stpmthAdj).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell integer_data" %>'
		 
		<%
			} 
		%>
		>
	</div>
	<div class="col-md-3">
				<%	
			qpsf = fw.getFieldXMLDef((sv.stpmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmtho,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stpmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"  style="width:140px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:.5px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%>
		</label>
	</div>
	<div class="col-md-3">
				<%	
			qpsf = fw.getFieldXMLDef((sv.stsmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stsmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data" style="width:140px;">
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	<div class="col-md-3">
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stsmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
	%>

	<input name='stsmthAdj' 
	type='text'
<%if((sv.stsmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="width:140px;text-align: right"<% }%>
	value='<%=valueThis%>'
			 <%
	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stsmthAdj.getLength(), sv.stsmthAdj.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stsmthAdj.getLength(), sv.stsmthAdj.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stsmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
		
		<% 
			if((new Byte((sv.stsmthAdj).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
			readonly="true"
			class="output_cell"
		<%
			}else if((new Byte((sv.stsmthAdj).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		%>	
				class="bold_cell" 
		
		<%
			}else { 
		%>
		
			class = ' <%=(sv.stsmthAdj).getColor()== null  ? 
					"input_cell integer_data":  (sv.stsmthAdj).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell integer_data"%>'
		 
		<%
			} 
		%>
		>
		
	</div>
	<div class="col-md-3">
				<%	
			qpsf = fw.getFieldXMLDef((sv.stsmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmtho,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stsmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"  style="width:140px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Sum Insured")%>
		</label>
	</div>
	<div class="col-md-3">
				<%	
			qpsf = fw.getFieldXMLDef((sv.stimth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stimth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stimth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"  style="width:140px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;">  </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	<div class="col-md-3">
			<%	
			qpsf = fw.getFieldXMLDef((sv.stimthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stimthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stimthAdj' 
type='text'
<%if((sv.stimthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="width:140px;text-align: right"<% }%>
	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stimthAdj.getLength(), sv.stimthAdj.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stimthAdj.getLength(), sv.stimthAdj.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stimthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.stimthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stimthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stimthAdj).getColor()== null  ? 
			"input_cell integer_data" :  (sv.stimthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell integer_data" %>'
 
<%
	} 
%>
>
	</div>
	
	<div class="col-md-3">
				<%	
			qpsf = fw.getFieldXMLDef((sv.stimtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stimtho,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stimtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell integer_data"   style="width:140px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;padding-right:1px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
</div>
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account   Cat /Year/Fund /Sect/Subsect/Reg /Br /BA /BS /BP /BT /ComYr/Pst/Cur")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Current account")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjusted Account")%>
</div>

</tr></table></div><br/></div></div>


<%@ include file="/POLACommon2NEW.jsp"%>

