<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6628";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>
<%S6628ScreenVars sv = (S6628ScreenVars) fw.getVariables();%>

<%if (sv.S6628screenWritten.gt(0)) {%>
	<%S6628screen.clearClassString(sv);%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	//sv.company.appendClassString("underline");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	//sv.tabl.appendClassString("underline");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	//sv.item.appendClassString("underline");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	//sv.longdesc.appendClassString("underline");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Details");%>
<%	generatedText3.appendClassString("label_txt");
	//generatedText3.appendClassString("underline");
	generatedText3.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statistical Categories");%>
<%	generatedText4.appendClassString("label_txt");
	//generatedText4.appendClassString("underline");
	generatedText4.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Previous");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subtract");%>
	<%sv.statcatg01.setClassString("");%>
<%	sv.statcatg01.appendClassString("string_fld");
	sv.statcatg01.appendClassString("input_txt");
	sv.statcatg01.appendClassString("highlight");
%>
	<%sv.statcatg02.setClassString("");%>
<%	sv.statcatg02.appendClassString("string_fld");
	sv.statcatg02.appendClassString("input_txt");
	sv.statcatg02.appendClassString("highlight");
%>
	<%sv.statcatg03.setClassString("");%>
<%	sv.statcatg03.appendClassString("string_fld");
	sv.statcatg03.appendClassString("input_txt");
	sv.statcatg03.appendClassString("highlight");
%>
	<%sv.statcatg04.setClassString("");%>
<%	sv.statcatg04.appendClassString("string_fld");
	sv.statcatg04.appendClassString("input_txt");
	sv.statcatg04.appendClassString("highlight");
%>
	<%sv.statcatg05.setClassString("");%>
<%	sv.statcatg05.appendClassString("string_fld");
	sv.statcatg05.appendClassString("input_txt");
	sv.statcatg05.appendClassString("highlight");
%>
	<%sv.statcatg06.setClassString("");%>
<%	sv.statcatg06.appendClassString("string_fld");
	sv.statcatg06.appendClassString("input_txt");
	sv.statcatg06.appendClassString("highlight");
%>
	<%sv.statcatg07.setClassString("");%>
<%	sv.statcatg07.appendClassString("string_fld");
	sv.statcatg07.appendClassString("input_txt");
	sv.statcatg07.appendClassString("highlight");
%>
	<%sv.statcatg08.setClassString("");%>
<%	sv.statcatg08.appendClassString("string_fld");
	sv.statcatg08.appendClassString("input_txt");
	sv.statcatg08.appendClassString("highlight");
%>
	<%sv.statcatg09.setClassString("");%>
<%	sv.statcatg09.appendClassString("string_fld");
	sv.statcatg09.appendClassString("input_txt");
	sv.statcatg09.appendClassString("highlight");
%>
	<%sv.statcatg10.setClassString("");%>
<%	sv.statcatg10.appendClassString("string_fld");
	sv.statcatg10.appendClassString("input_txt");
	sv.statcatg10.appendClassString("highlight");
%>
	<%sv.statcatg11.setClassString("");%>
<%	sv.statcatg11.appendClassString("string_fld");
	sv.statcatg11.appendClassString("input_txt");
	sv.statcatg11.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Add");%>
	<%sv.statcatg12.setClassString("");%>
<%	sv.statcatg12.appendClassString("string_fld");
	sv.statcatg12.appendClassString("input_txt");
	sv.statcatg12.appendClassString("highlight");
%>
	<%sv.statcatg13.setClassString("");%>
<%	sv.statcatg13.appendClassString("string_fld");
	sv.statcatg13.appendClassString("input_txt");
	sv.statcatg13.appendClassString("highlight");
%>
	<%sv.statcatg14.setClassString("");%>
<%	sv.statcatg14.appendClassString("string_fld");
	sv.statcatg14.appendClassString("input_txt");
	sv.statcatg14.appendClassString("highlight");
%>
	<%sv.statcatg15.setClassString("");%>
<%	sv.statcatg15.appendClassString("string_fld");
	sv.statcatg15.appendClassString("input_txt");
	sv.statcatg15.appendClassString("highlight");
%>
	<%sv.statcatg16.setClassString("");%>
<%	sv.statcatg16.appendClassString("string_fld");
	sv.statcatg16.appendClassString("input_txt");
	sv.statcatg16.appendClassString("highlight");
%>
	<%sv.statcatg17.setClassString("");%>
<%	sv.statcatg17.appendClassString("string_fld");
	sv.statcatg17.appendClassString("input_txt");
	sv.statcatg17.appendClassString("highlight");
%>
	<%sv.statcatg18.setClassString("");%>
<%	sv.statcatg18.appendClassString("string_fld");
	sv.statcatg18.appendClassString("input_txt");
	sv.statcatg18.appendClassString("highlight");
%>
	<%sv.statcatg19.setClassString("");%>
<%	sv.statcatg19.appendClassString("string_fld");
	sv.statcatg19.appendClassString("input_txt");
	sv.statcatg19.appendClassString("highlight");
%>
	<%sv.statcatg20.setClassString("");%>
<%	sv.statcatg20.appendClassString("string_fld");
	sv.statcatg20.appendClassString("input_txt");
	sv.statcatg20.appendClassString("highlight");
%>
	<%sv.statcatg21.setClassString("");%>
<%	sv.statcatg21.appendClassString("string_fld");
	sv.statcatg21.appendClassString("input_txt");
	sv.statcatg21.appendClassString("highlight");
%>
	<%sv.statcatg22.setClassString("");%>
<%	sv.statcatg22.appendClassString("string_fld");
	sv.statcatg22.appendClassString("input_txt");
	sv.statcatg22.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subtract");%>
	<%sv.statcatg23.setClassString("");%>
<%	sv.statcatg23.appendClassString("string_fld");
	sv.statcatg23.appendClassString("input_txt");
	sv.statcatg23.appendClassString("highlight");
%>
	<%sv.statcatg24.setClassString("");%>
<%	sv.statcatg24.appendClassString("string_fld");
	sv.statcatg24.appendClassString("input_txt");
	sv.statcatg24.appendClassString("highlight");
%>
	<%sv.statcatg25.setClassString("");%>
<%	sv.statcatg25.appendClassString("string_fld");
	sv.statcatg25.appendClassString("input_txt");
	sv.statcatg25.appendClassString("highlight");
%>
	<%sv.statcatg26.setClassString("");%>
<%	sv.statcatg26.appendClassString("string_fld");
	sv.statcatg26.appendClassString("input_txt");
	sv.statcatg26.appendClassString("highlight");
%>
	<%sv.statcatg27.setClassString("");%>
<%	sv.statcatg27.appendClassString("string_fld");
	sv.statcatg27.appendClassString("input_txt");
	sv.statcatg27.appendClassString("highlight");
%>
	<%sv.statcatg28.setClassString("");%>
<%	sv.statcatg28.appendClassString("string_fld");
	sv.statcatg28.appendClassString("input_txt");
	sv.statcatg28.appendClassString("highlight");
%>
	<%sv.statcatg29.setClassString("");%>
<%	sv.statcatg29.appendClassString("string_fld");
	sv.statcatg29.appendClassString("input_txt");
	sv.statcatg29.appendClassString("highlight");
%>
	<%sv.statcatg30.setClassString("");%>
<%	sv.statcatg30.appendClassString("string_fld");
	sv.statcatg30.appendClassString("input_txt");
	sv.statcatg30.appendClassString("highlight");
%>
	<%sv.statcatg31.setClassString("");%>
<%	sv.statcatg31.appendClassString("string_fld");
	sv.statcatg31.appendClassString("input_txt");
	sv.statcatg31.appendClassString("highlight");
%>
	<%sv.statcatg32.setClassString("");%>
<%	sv.statcatg32.appendClassString("string_fld");
	sv.statcatg32.appendClassString("input_txt");
	sv.statcatg32.appendClassString("highlight");
%>
	<%sv.statcatg33.setClassString("");%>
<%	sv.statcatg33.appendClassString("string_fld");
	sv.statcatg33.appendClassString("input_txt");
	sv.statcatg33.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Add");%>
	<%sv.statcatg34.setClassString("");%>
<%	sv.statcatg34.appendClassString("string_fld");
	sv.statcatg34.appendClassString("input_txt");
	sv.statcatg34.appendClassString("highlight");
%>
	<%sv.statcatg35.setClassString("");%>
<%	sv.statcatg35.appendClassString("string_fld");
	sv.statcatg35.appendClassString("input_txt");
	sv.statcatg35.appendClassString("highlight");
%>
	<%sv.statcatg36.setClassString("");%>
<%	sv.statcatg36.appendClassString("string_fld");
	sv.statcatg36.appendClassString("input_txt");
	sv.statcatg36.appendClassString("highlight");
%>
	<%sv.statcatg37.setClassString("");%>
<%	sv.statcatg37.appendClassString("string_fld");
	sv.statcatg37.appendClassString("input_txt");
	sv.statcatg37.appendClassString("highlight");
%>
	<%sv.statcatg38.setClassString("");%>
<%	sv.statcatg38.appendClassString("string_fld");
	sv.statcatg38.appendClassString("input_txt");
	sv.statcatg38.appendClassString("highlight");
%>
	<%sv.statcatg39.setClassString("");%>
<%	sv.statcatg39.appendClassString("string_fld");
	sv.statcatg39.appendClassString("input_txt");
	sv.statcatg39.appendClassString("highlight");
%>
	<%sv.statcatg40.setClassString("");%>
<%	sv.statcatg40.appendClassString("string_fld");
	sv.statcatg40.appendClassString("input_txt");
	sv.statcatg40.appendClassString("highlight");
%>
	<%sv.statcatg41.setClassString("");%>
<%	sv.statcatg41.appendClassString("string_fld");
	sv.statcatg41.appendClassString("input_txt");
	sv.statcatg41.appendClassString("highlight");
%>
	<%sv.statcatg42.setClassString("");%>
<%	sv.statcatg42.appendClassString("string_fld");
	sv.statcatg42.appendClassString("input_txt");
	sv.statcatg42.appendClassString("highlight");
%>
	<%sv.statcatg43.setClassString("");%>
<%	sv.statcatg43.appendClassString("string_fld");
	sv.statcatg43.appendClassString("input_txt");
	sv.statcatg43.appendClassString("highlight");
%>
	<%sv.statcatg44.setClassString("");%>
<%	sv.statcatg44.appendClassString("string_fld");
	sv.statcatg44.appendClassString("input_txt");
	sv.statcatg44.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Difference");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subtract");%>
	<%sv.statcatg45.setClassString("");%>
<%	sv.statcatg45.appendClassString("string_fld");
	sv.statcatg45.appendClassString("input_txt");
	sv.statcatg45.appendClassString("highlight");
%>
	<%sv.statcatg46.setClassString("");%>
<%	sv.statcatg46.appendClassString("string_fld");
	sv.statcatg46.appendClassString("input_txt");
	sv.statcatg46.appendClassString("highlight");
%>
	<%sv.statcatg47.setClassString("");%>
<%	sv.statcatg47.appendClassString("string_fld");
	sv.statcatg47.appendClassString("input_txt");
	sv.statcatg47.appendClassString("highlight");
%>
	<%sv.statcatg48.setClassString("");%>
<%	sv.statcatg48.appendClassString("string_fld");
	sv.statcatg48.appendClassString("input_txt");
	sv.statcatg48.appendClassString("highlight");
%>
	<%sv.statcatg49.setClassString("");%>
<%	sv.statcatg49.appendClassString("string_fld");
	sv.statcatg49.appendClassString("input_txt");
	sv.statcatg49.appendClassString("highlight");
%>
	<%sv.statcatg50.setClassString("");%>
<%	sv.statcatg50.appendClassString("string_fld");
	sv.statcatg50.appendClassString("input_txt");
	sv.statcatg50.appendClassString("highlight");
%>
	<%sv.statcatg51.setClassString("");%>
<%	sv.statcatg51.appendClassString("string_fld");
	sv.statcatg51.appendClassString("input_txt");
	sv.statcatg51.appendClassString("highlight");
%>
	<%sv.statcatg52.setClassString("");%>
<%	sv.statcatg52.appendClassString("string_fld");
	sv.statcatg52.appendClassString("input_txt");
	sv.statcatg52.appendClassString("highlight");
%>
	<%sv.statcatg53.setClassString("");%>
<%	sv.statcatg53.appendClassString("string_fld");
	sv.statcatg53.appendClassString("input_txt");
	sv.statcatg53.appendClassString("highlight");
%>
	<%sv.statcatg54.setClassString("");%>
<%	sv.statcatg54.appendClassString("string_fld");
	sv.statcatg54.appendClassString("input_txt");
	sv.statcatg54.appendClassString("highlight");
%>
	<%sv.statcatg55.setClassString("");%>
<%	sv.statcatg55.appendClassString("string_fld");
	sv.statcatg55.appendClassString("input_txt");
	sv.statcatg55.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Add");%>
	<%sv.statcatg56.setClassString("");%>
<%	sv.statcatg56.appendClassString("string_fld");
	sv.statcatg56.appendClassString("input_txt");
	sv.statcatg56.appendClassString("highlight");
%>
	<%sv.statcatg57.setClassString("");%>
<%	sv.statcatg57.appendClassString("string_fld");
	sv.statcatg57.appendClassString("input_txt");
	sv.statcatg57.appendClassString("highlight");
%>
	<%sv.statcatg58.setClassString("");%>
<%	sv.statcatg58.appendClassString("string_fld");
	sv.statcatg58.appendClassString("input_txt");
	sv.statcatg58.appendClassString("highlight");
%>
	<%sv.statcatg59.setClassString("");%>
<%	sv.statcatg59.appendClassString("string_fld");
	sv.statcatg59.appendClassString("input_txt");
	sv.statcatg59.appendClassString("highlight");
%>
	<%sv.statcatg60.setClassString("");%>
<%	sv.statcatg60.appendClassString("string_fld");
	sv.statcatg60.appendClassString("input_txt");
	sv.statcatg60.appendClassString("highlight");
%>
	<%sv.statcatg61.setClassString("");%>
<%	sv.statcatg61.appendClassString("string_fld");
	sv.statcatg61.appendClassString("input_txt");
	sv.statcatg61.appendClassString("highlight");
%>
	<%sv.statcatg62.setClassString("");%>
<%	sv.statcatg62.appendClassString("string_fld");
	sv.statcatg62.appendClassString("input_txt");
	sv.statcatg62.appendClassString("highlight");
%>
	<%sv.statcatg63.setClassString("");%>
<%	sv.statcatg63.appendClassString("string_fld");
	sv.statcatg63.appendClassString("input_txt");
	sv.statcatg63.appendClassString("highlight");
%>
	<%sv.statcatg64.setClassString("");%>
<%	sv.statcatg64.appendClassString("string_fld");
	sv.statcatg64.appendClassString("input_txt");
	sv.statcatg64.appendClassString("highlight");
%>
	<%sv.statcatg65.setClassString("");%>
<%	sv.statcatg65.appendClassString("string_fld");
	sv.statcatg65.appendClassString("input_txt");
	sv.statcatg65.appendClassString("highlight");
%>
	<%sv.statcatg66.setClassString("");%>
<%	sv.statcatg66.appendClassString("string_fld");
	sv.statcatg66.appendClassString("input_txt");
	sv.statcatg66.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Difference  decrease");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subtract");%>
	<%sv.statcatg67.setClassString("");%>
<%	sv.statcatg67.appendClassString("string_fld");
	sv.statcatg67.appendClassString("input_txt");
	sv.statcatg67.appendClassString("highlight");
%>
	<%sv.statcatg68.setClassString("");%>
<%	sv.statcatg68.appendClassString("string_fld");
	sv.statcatg68.appendClassString("input_txt");
	sv.statcatg68.appendClassString("highlight");
%>
	<%sv.statcatg69.setClassString("");%>
<%	sv.statcatg69.appendClassString("string_fld");
	sv.statcatg69.appendClassString("input_txt");
	sv.statcatg69.appendClassString("highlight");
%>
	<%sv.statcatg70.setClassString("");%>
<%	sv.statcatg70.appendClassString("string_fld");
	sv.statcatg70.appendClassString("input_txt");
	sv.statcatg70.appendClassString("highlight");
%>
	<%sv.statcatg71.setClassString("");%>
<%	sv.statcatg71.appendClassString("string_fld");
	sv.statcatg71.appendClassString("input_txt");
	sv.statcatg71.appendClassString("highlight");
%>
	<%sv.statcatg72.setClassString("");%>
<%	sv.statcatg72.appendClassString("string_fld");
	sv.statcatg72.appendClassString("input_txt");
	sv.statcatg72.appendClassString("highlight");
%>
	<%sv.statcatg73.setClassString("");%>
<%	sv.statcatg73.appendClassString("string_fld");
	sv.statcatg73.appendClassString("input_txt");
	sv.statcatg73.appendClassString("highlight");
%>
	<%sv.statcatg74.setClassString("");%>
<%	sv.statcatg74.appendClassString("string_fld");
	sv.statcatg74.appendClassString("input_txt");
	sv.statcatg74.appendClassString("highlight");
%>
	<%sv.statcatg75.setClassString("");%>
<%	sv.statcatg75.appendClassString("string_fld");
	sv.statcatg75.appendClassString("input_txt");
	sv.statcatg75.appendClassString("highlight");
%>
	<%sv.statcatg76.setClassString("");%>
<%	sv.statcatg76.appendClassString("string_fld");
	sv.statcatg76.appendClassString("input_txt");
	sv.statcatg76.appendClassString("highlight");
%>
	<%sv.statcatg77.setClassString("");%>
<%	sv.statcatg77.appendClassString("string_fld");
	sv.statcatg77.appendClassString("input_txt");
	sv.statcatg77.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Add");%>
	<%sv.statcatg78.setClassString("");%>
<%	sv.statcatg78.appendClassString("string_fld");
	sv.statcatg78.appendClassString("input_txt");
	sv.statcatg78.appendClassString("highlight");
%>
	<%sv.statcatg79.setClassString("");%>
<%	sv.statcatg79.appendClassString("string_fld");
	sv.statcatg79.appendClassString("input_txt");
	sv.statcatg79.appendClassString("highlight");
%>
	<%sv.statcatg80.setClassString("");%>
<%	sv.statcatg80.appendClassString("string_fld");
	sv.statcatg80.appendClassString("input_txt");
	sv.statcatg80.appendClassString("highlight");
%>
	<%sv.statcatg81.setClassString("");%>
<%	sv.statcatg81.appendClassString("string_fld");
	sv.statcatg81.appendClassString("input_txt");
	sv.statcatg81.appendClassString("highlight");
%>
	<%sv.statcatg82.setClassString("");%>
<%	sv.statcatg82.appendClassString("string_fld");
	sv.statcatg82.appendClassString("input_txt");
	sv.statcatg82.appendClassString("highlight");
%>
	<%sv.statcatg83.setClassString("");%>
<%	sv.statcatg83.appendClassString("string_fld");
	sv.statcatg83.appendClassString("input_txt");
	sv.statcatg83.appendClassString("highlight");
%>
	<%sv.statcatg84.setClassString("");%>
<%	sv.statcatg84.appendClassString("string_fld");
	sv.statcatg84.appendClassString("input_txt");
	sv.statcatg84.appendClassString("highlight");
%>
	<%sv.statcatg85.setClassString("");%>
<%	sv.statcatg85.appendClassString("string_fld");
	sv.statcatg85.appendClassString("input_txt");
	sv.statcatg85.appendClassString("highlight");
%>
	<%sv.statcatg86.setClassString("");%>
<%	sv.statcatg86.appendClassString("string_fld");
	sv.statcatg86.appendClassString("input_txt");
	sv.statcatg86.appendClassString("highlight");
%>
	<%sv.statcatg87.setClassString("");%>
<%	sv.statcatg87.appendClassString("string_fld");
	sv.statcatg87.appendClassString("input_txt");
	sv.statcatg87.appendClassString("highlight");
%>
	<%sv.statcatg88.setClassString("");%>
<%	sv.statcatg88.appendClassString("string_fld");
	sv.statcatg88.appendClassString("input_txt");
	sv.statcatg88.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

	
<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
				<div class="col-md-2">
					 <div class="form-group"style="max-width:50px;"> 
								<label><%=generatedText5%></label>
								<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						</div>
					</div>
						<div class="col-md-1">
						</div>
					<div class="col-md-2">
					 <div class="form-group"style="max-width:90px;"> 
								<label><%=generatedText6%></label>
									
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						</div>
					</div>
					
					<div class="col-md-2">
					 <div class="form-group"> 
								<label><%=generatedText2%></label>
								<div class="input-group"> 
								<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
						</div>
					</div>
					</div>
			</div>
			
			</br>
			
				<div class="row">        
					<div class="col-md-2">
						<div class="form-group"> 
								
								<label><%=generatedText3%></label>
						</div>
					</div>
					<div class="col-md-2">
						
					</div>
					
					<div class="col-md-3">
						<div class="form-group"> 
									<label><%=generatedText4%></label>
								
						</div>
					</div>
			</div>
			
			<div class="row">        
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText7%></label>
								
						</div>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText11%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg01)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg02)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg03)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg04)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg05)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg06)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg07)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg08)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg09)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg10)%>
								
						</div>
					</div>
						<div class="col-md-1" style="max-width:50px;"> 
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg11)%>
								
						</div>
					</div>
					
			</div>
			
			
			<div class="row">        
					<div class="col-md-1">
						
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText12%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg12)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg13)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg14)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg15)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg16)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg17)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg18)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg19)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg20)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg21)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg22)%>
								
						</div>
					</div>
					
			</div>
			
			
			<div class="row">    
			    <div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText8%></label>
						</div>
						</div>
					<div class="col-md-2">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText13%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg23)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg24)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg25)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg26)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg27)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg28)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg29)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg30)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg31)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg32)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg33)%>
								
						</div>
					</div>
					
			</div>
			
			<div class="row">        
					<div class="col-md-3">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText14%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg23)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg24)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg25)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg26)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg27)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg28)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg29)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg30)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg31)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;"> 
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg32)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg33)%>
								
						</div>
					</div>
					
			</div>
			
			<div class="row">  
			      <div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText9%> Increase</label>
						</div>
						</div>
					<div class="col-md-2">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText13%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg34)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg35)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg36)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg37)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg38)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg39)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg40)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg41)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg42)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg43)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg44)%>
								
						</div>
					</div>
					
			</div>
			
				<div class="row">        
					<div class="col-md-3">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText14%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg34)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg35)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg36)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg37)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg38)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;"> 
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg39)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg40)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg41)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg42)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg43)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg44)%>
								
						</div>
					</div>
					
			</div>
			
			
			<div class="row">      
			
			<div class="col-md-1">
						 
								
								<label><%=generatedText9%></label>
						
						</div> 
					<div class="col-md-2">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText15%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg45)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg46)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"style="max-width:50px;"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg47)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg48)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg49)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg50)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg51)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg52)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg53)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg54)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg55)%>
								
						</div>
					</div>
					
			</div>
			
			
			<div class="row">        
					<div class="col-md-3">
						
					</div>
					
					<div class="col-md-1">
						<div class="form-group"> 
								
								<label><%=generatedText16%></label>
						</div>
					</div>
					<div class="col-md-1" style="max-width:70px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg56)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg57)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg58)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg59)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg60)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg61)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg62)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg63)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar( fw, sv.statcatg64)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg65)%>
								
						</div>
					</div>
					<div class="col-md-1" style="max-width:50px;">
						<div class="form-group"> 
									<%=smartHF.getHTMLVar(fw, sv.statcatg66)%>
								
						</div>
					</div>
					
			</div>
			
		</div>
</div>
					
	




<%}%>

<%if (sv.S6628protectWritten.gt(0)) {%>
	<%S6628protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<style>
div[id*='statcatg']{padding-right:2px !important} 
</style> 

<%@ include file="/POLACommon2NEW.jsp"%>

