

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SJ675";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>
<%Sj675ScreenVars sv = (Sj675ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Business Category to print ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.statSect01.setReverse(BaseScreenData.REVERSED);
			sv.statSect01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.statSect01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.statSect02.setReverse(BaseScreenData.REVERSED);
			sv.statSect02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.statSect02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.statSect03.setReverse(BaseScreenData.REVERSED);
			sv.statSect03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.statSect03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.statSect04.setReverse(BaseScreenData.REVERSED);
			sv.statSect04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.statSect04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.statSect05.setReverse(BaseScreenData.REVERSED);
			sv.statSect05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.statSect05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.statSect06.setReverse(BaseScreenData.REVERSED);
			sv.statSect06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.statSect06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.statSect07.setReverse(BaseScreenData.REVERSED);
			sv.statSect07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.statSect07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.statSect08.setReverse(BaseScreenData.REVERSED);
			sv.statSect08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.statSect08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.statSect09.setReverse(BaseScreenData.REVERSED);
			sv.statSect09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.statSect09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.statSect10.setReverse(BaseScreenData.REVERSED);
			sv.statSect10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.statSect10.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                 
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
   <div class="form-group" style="max-width:80px;">                             <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>
 </div>

 <div class="row">
<div class="col-md-12">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Business Category to print")%></label>
                              <table>

<tr><td>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect01");
	optionValue = makeDropDownList( mappedItems , sv.statSect01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td><td>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect02");
	optionValue = makeDropDownList( mappedItems , sv.statSect02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect03");
	optionValue = makeDropDownList( mappedItems , sv.statSect03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td><td>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect04");
	optionValue = makeDropDownList( mappedItems , sv.statSect04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect05");
	optionValue = makeDropDownList( mappedItems , sv.statSect05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr><tr><td>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect06");
	optionValue = makeDropDownList( mappedItems , sv.statSect06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</td><td>


<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect07");
	optionValue = makeDropDownList( mappedItems , sv.statSect07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td><td>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect08");
	optionValue = makeDropDownList( mappedItems , sv.statSect08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</td><td>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect09");
	optionValue = makeDropDownList( mappedItems , sv.statSect09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td><td>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statSect10");
	optionValue = makeDropDownList( mappedItems , sv.statSect10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.statSect10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statSect10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.statSect10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='statSect10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.statSect10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statSect10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statSect10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td></tr></table>
</div></div>
</div></div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

