<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6711";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>
<%S6711ScreenVars sv = (S6711ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  Cat/Year/Fund /Sect /Subsect /Reg /Br /BA /BS /BP /BT /ComYr/Pst/Cur");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Journal for account month ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Year ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract count ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual premium ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single premium ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum insured    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Time ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.acctyr.setReverse(BaseScreenData.REVERSED);
			sv.acctyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.acctyr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.acmn.setReverse(BaseScreenData.REVERSED);
			sv.acmn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.acmn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.commyr.setReverse(BaseScreenData.REVERSED);
			sv.commyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.commyr.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-1">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
  		<div class="form-group" style="max-width:50px;">
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
		<div class="form-group">				 
						<label><%=resourceBundleHandler.gettingValueFromBundle("SCategory")%></label>
  		<div class="input-group" style="max-width:120px;">

  <%=smartHF.getRichTextInputFieldLookup(fw, sv.statcat)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statcat).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
				<div class="form-group">		 
						<label><%=resourceBundleHandler.gettingValueFromBundle("AcctYr")%></label>
  		<div class="form-group" style="max-width:100px;">

	<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acctyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("SttrFund")%></label>
  		<div class="input-group" style="max-width:100px;">

 <%=smartHF.getRichTextInputFieldLookup(fw, sv.statFund)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statFund).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
				<div class="form-group">	 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Sect")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.statSect)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.statSect).replace("<img","<img id='searchImg' ")%>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Statutory Subsection")%></label>
  		<div class="input-group" style="max-width:120px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.stsubsect)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.stsubsect).replace("<img","<img id='searchImg' ")%>
</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group" style="max-width:100px;">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.register)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.register).replace("<img","<img id='searchImg' ")%>
 

</div>
</div>
</div>
<div class="col-md-2">
			<div class="form-group">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Br")%></label>
  		<div class="input-group" style="max-width:100px;">
 <%=smartHF.getRichTextInputFieldLookup(fw, sv.cntbranch)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntbranch).replace("<img","<img id='searchImg' ")%>


</div>
</div>
</div>
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BA")%></label>
  		<div class="form-group" style="max-width:100px;">


<input name='bandage' 
type='text'

<%

		formatValue = (sv.bandage.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandage.getLength()%>'
maxLength='<%= sv.bandage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandage)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandage).getColor()== null  ? 
			"input_cell" :  (sv.bandage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>
  		<div class="form-group" style="max-width:100px;">


<input name='bandsa' 
type='text'

<%

		formatValue = (sv.bandsa.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandsa.getLength()%>'
maxLength='<%= sv.bandsa.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandsa)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandsa).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandsa).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandsa).getColor()== null  ? 
			"input_cell" :  (sv.bandsa).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>

<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>
	<div class="form-group" style="max-width:100px;">

<input name='bandprm' 
type='text'

<%

		formatValue = (sv.bandprm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandprm.getLength()%>'
maxLength='<%= sv.bandprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandprm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandprm).getColor()== null  ? 
			"input_cell" :  (sv.bandprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>
	<div class="form-group" style="max-width:100px;">

<input name='bandtrm' 
type='text'

<%

		formatValue = (sv.bandtrm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandtrm.getLength()%>'
maxLength='<%= sv.bandtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandtrm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandtrm).getColor()== null  ? 
			"input_cell" :  (sv.bandtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-3">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("ComYr")%></label>
	<div class="form-group" style="max-width:120px;">



	<%	
			qpsf = fw.getFieldXMLDef((sv.commyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='commyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.commyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
	 <%}%>

size='<%= sv.commyr.getLength()%>'
maxLength='<%= sv.commyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(commyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.commyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.commyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.commyr).getColor()== null  ? 
			"input_cell" :  (sv.commyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
</div>

<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Pst")%></label>
  		<div class="input-group" style="max-width:100px;">
  <%=smartHF.getRichTextInputFieldLookup(fw, sv.pstatcd)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.pstatcd).replace("<img","<img id='searchImg' ")%>


</div></div></div>
<div class="col-md-2">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
  		<div class="input-group" style="max-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.cntcurr)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntcurr).replace("<img","<img id='searchImg' ")%>
</div></div>
</div></div>

<div class="row">
<div class="col-md-3">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Journal for account month")%></label>
  		<div class="form-group">
  		<table><tr><td>


	<%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acmn' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acmn) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acmn);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acmn) %>'
	 <%}%>

size='<%= sv.acmn.getLength()%>'
maxLength='<%= sv.acmn.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acmn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acmn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acmn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acmn).getColor()== null  ? 
			"input_cell" :  (sv.acmn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td><td>

	
  		
		<%					
		if(!((sv.mthldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td></tr></table>	

</div>
</div>

<div class="col-md-3">
					 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Account Year")%></label>

	<div class="form-group" style="max-width:100px;">	
  		
		<%					
		if(!((sv.acyr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
</div>
</div>
<div class="row">
<div class="col-md-3">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
</div>


<div class="col-md-2">
			<div class="form-group">			 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Count")%></label>

	<%	
			qpsf = fw.getFieldXMLDef((sv.stcmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stcmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='stcmth' 
type='text'

<%if((sv.stcmth).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stcmth.getLength(), sv.stcmth.getScale(),3)%>'
maxLength='<%= sv.stcmth.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(volbanfr01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stcmth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stcmth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stcmth).getColor()== null  ? 
			"input_cell" :  (sv.stcmth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>

</div></div></div>
<div class="row">
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Annual Premium")%></label>
	<div class="form-group" style="max-width:100px;">
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%></label>
	<div class="form-group" style="max-width:100px;">
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stsmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div>
</div>
</div>

<div class="row">
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Insured")%></label>
	<div class="form-group" style="max-width:100px;">
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stimth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stimth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stimth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("User")%></label>
	<div class="form-group" style="max-width:100px;">
	
  		
		<%					
		if(!((sv.user.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.user.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.user.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Date")%></label>
	<div class="form-group">
	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
						 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Time")%></label>
	<div class="form-group" style="max-width:100px;">
	
  		
		<%					
		if(!((sv.transactionTime.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.transactionTime.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.transactionTime.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</div></div>
</div>	

</div>
</div>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account  Cat/Year/Fund /Sect /Subsect /Reg /Br /BA /BS /BP /BT /ComYr/Pst/Cur")%>
</div>

</tr></table></div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

