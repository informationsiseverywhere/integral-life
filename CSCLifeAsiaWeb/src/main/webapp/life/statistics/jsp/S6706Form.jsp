<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6706";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6706ScreenVars sv = (S6706ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  Agency/Cat/Year/Area/Br /BA /BS /BP /BT /Type /CRT/Overid/Pstat/Curr");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Balance for account month ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Year ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current account");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjustment");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjusted Account");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Count ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage count ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Premium ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission     ");%>

<%{
	/*bug #ILIFE-1070 start*/
		if (appVars.ind01.isOn()) {
			sv.stcmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stvmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stpmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stsmthAdj.setEnabled(BaseScreenData.DISABLED);
			sv.stmmthAdj.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.stcmthAdj.setColor(BaseScreenData.RED);
			sv.stvmthAdj.setColor(BaseScreenData.RED);
			sv.stpmthAdj.setColor(BaseScreenData.RED);
			sv.stsmthAdj.setColor(BaseScreenData.RED);
			sv.stmmthAdj.setColor(BaseScreenData.RED);
		}
		/*bug #ILIFE-1070 end*/
	}

	%>


<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-1"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>




	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>





	
  		
		<%					
		if(!((sv.agntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Category")%></label>

	
  		
		<%					
		if(!((sv.statcat.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statcat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statcat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%></label>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyr);
			
			if(!((sv.acctyr.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>

<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Area")%></label>

	
  		
		<%					
		if(!((sv.aracde.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.aracde.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.aracde.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>


	
  		
		<%					
		if(!((sv.cntbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>


	<div class="col-md-1"></div> 
			    	<div class="col-md-2"> 
				    		<div class="form-group">
				 <label><%=resourceBundleHandler.gettingValueFromBundle("BA")%></label>

	
  		
		<%					
		if(!((sv.bandage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>

	
  		
		<%					
		if(!((sv.bandsa.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandsa.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandsa.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>
<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>

	
  		
		<%					
		if(!((sv.bandprm.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandprm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandprm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>



  		
		<%					
		if(!((sv.bandtrm.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandtrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bandtrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>



<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>



	
  		
		<%					
		if(!((sv.chdrtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div>

	<div class="col-md-1"></div>
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Table")%></label>





	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>

	<div class="col-md-1"></div> 
			    	<div class="col-md-2"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Over Id")%></label>

	
  		
		<%					
		if(!((sv.ovrdcat.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ovrdcat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ovrdcat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Pstat")%></label>


	
  		
		<%					
		if(!((sv.pstatcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstatcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstatcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div>




			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>



<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Balance for account month")%></label>
<div class="input-group">

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acmn);
			
			if(!((sv.acmn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	





	
  		
		<%					
		if(!((sv.mthldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>
<!-- ILIFE-2663  Life Cross Browser -Coding and UT- Sprint 3 D3: Task 4 Start by snayeni --> 
<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
}
</style>
<!-- ILIFE-2663  Life Cross Browser -Coding and UT- Sprint 3 D3: Task 4 End by snayeni --> 

			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Account Year")%></label>

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acyr);
			
			if(!((sv.acyr.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>
	<div class="col-md-1"></div>
			    	<div class="col-md-3"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div><br/><br/><br/><br/>
<table>
<div class="col-md-2"></div>
<div class="row"><div class="col-md-2">

<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Current account")%>
</div>
<%}%>


</div><div class="col-md-2">
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%>
</div>
<%}%>


</div><div class="col-md-2">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjusted Account")%>
</div>
<%}%>


</div>
</div>
<div class="row">
<div class="col-md-2">
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Count")%>
</div>
<%}%>


</div>
<div class="col-md-2">

<%if ((new Byte((sv.stcmth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stcmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stcmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" align="right">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	




</div><div class="col-md-2">

<div class="form-group">
<%if ((new Byte((sv.stcmthAdj).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.stcmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			 valueThis=smartHF.getPicFormatted(qpsf,sv.stcmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stcmthAdj' 
type='text'

<%if((sv.stcmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis %>'
			 <%
	
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stcmthAdj.getLength(), sv.stcmthAdj.getScale(),3)%>'
maxLength='<%= sv.stcmthAdj.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(stcmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stcmthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stcmthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stcmthAdj).getColor()== null  ? 
			"input_cell" :  (sv.stcmthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
width="251px"
>
<%}%>


</div></div><div class="col-md-2">

<%if ((new Byte((sv.stcmtho).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stcmtho,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stcmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" align="right">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	



</div></div>
<div class="row"><div class="col-md-2">

<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Coverage count")%>
</div>
<%}%>


</div><div class="col-md-2">

<%if ((new Byte((sv.stvmth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stvmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stvmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stvmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  align="right">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	




</div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((sv.stvmthAdj).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.stvmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			 valueThis=smartHF.getPicFormatted(qpsf,sv.stvmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stvmthAdj' 
type='text'

<%if((sv.stvmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis %>'
			 <%
	
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.stvmthAdj.getLength(), sv.stvmthAdj.getScale(),3)%>'
maxLength='<%= sv.stvmthAdj.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(stvmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stvmthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stvmthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>

	class = ' <%=(sv.stvmthAdj).getColor()== null  ? 
			"input_cell" :  (sv.stvmthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>


</div></div>
<div class="col-md-2">

<%if ((new Byte((sv.stvmtho).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stvmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stvmtho,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stvmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" align="right">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	


</div></div>
<!-- ILIFE-1514 STARTS -->


<div class="row"><div class="col-md-2">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Annual Premium")%>
</div>
<%}%>


</div><div class="col-md-2">

<%if ((new Byte((sv.stpmth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:126px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
				
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	



</div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((sv.stpmthAdj).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.stpmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stpmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stpmthAdj' 
type='text'

<%if((sv.stpmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

			<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
			} %>

	value='<%=formatValue %>'
			 <%
	 valueThis=formatValue;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=formatValue%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stpmthAdj.getLength(), sv.stpmthAdj.getScale(),3)%>'
maxLength='<%= sv.stpmthAdj.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stpmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stpmthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stpmthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stpmthAdj).getColor()== null  ? 
			"input_cell" :  (sv.stpmthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>


</div></div>
<div class="col-md-2">

<%if ((new Byte((sv.stpmtho).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmtho,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:127px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	



</div></div>
<div class="row"><div class="col-md-2">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%>
</div>
<%}%>


</div><div class="col-md-2">

<%if ((new Byte((sv.stsmth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stsmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:127px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
				
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	




</div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((sv.stsmthAdj).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.stsmthAdj).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stsmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stsmthAdj' 
type='text'

<%if((sv.stsmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%if(valueThis.charAt(0)=='.'){
	valueThis = "0"+valueThis;
					} %>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stsmthAdj.getLength(), sv.stsmthAdj.getScale(),3)%>'
maxLength='<%= sv.stsmthAdj.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stsmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stsmthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stsmthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stsmthAdj).getColor()== null  ? 
			"input_cell" :  (sv.stsmthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>


</div></div>
<div class="col-md-2">

<%if ((new Byte((sv.stsmtho).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmtho,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stsmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:127px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
				
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	



</div></div>
<div class="row"><div class="col-md-2">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
</div>
<%}%>


</div><div class="col-md-2">

<%if ((new Byte((sv.stmmth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stmmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stmmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stmmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:127px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
				
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	




</div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((sv.stmmthAdj).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.stmmthAdj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stmmth);
			valueThis=smartHF.getPicFormatted(qpsf,sv.stmmthAdj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
	%>

<input name='stmmthAdj' 
type='text'

<%if((sv.stmmthAdj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%if(valueThis.charAt(0)=='.'){
	valueThis = "0"+valueThis;
					} %>


	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.stmmthAdj.getLength(), sv.stmmthAdj.getScale(),3)%>'
maxLength='<%= sv.stmmthAdj.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(stmmthAdj)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.stmmthAdj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.stmmthAdj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.stmmthAdj).getColor()== null  ? 
			"input_cell" :  (sv.stmmthAdj).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

<%}%>


</div></div>
<div class="col-md-2">

<%if ((new Byte((sv.stmmtho).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stmmtho).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stmmtho,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stmmtho.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:127px;" align="right">	
				
				<%if(formatValue.charAt(0)=='.'){
					formatValue = "0"+formatValue;
					} %>
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
				<!-- ILIFE-1514 ENDS -->
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</div></div>
</table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Account  Agency/Cat/Year/Area/Br /BA /BS /BP /BT /Type /CRT/Overid/Pstat/Curr")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Count")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Current account")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%>
</div>

</tr></table></div><br/></div></div>


<%@ include file="/POLACommon2NEW.jsp"%>

