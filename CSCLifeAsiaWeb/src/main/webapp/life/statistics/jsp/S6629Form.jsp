

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6629";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6629ScreenVars sv = (S6629ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accumulate Agent Statistics");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age band item");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum insured band item");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk term band item");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium band item");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accumulate Government Statistics");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age band item");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum insured band item");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk term band item");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium band item");%>
<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-3">
						<div class="form-group"style="max-width:50px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
								<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						</div>					
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-3">
						<div class="form-group"style="max-width:100px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
								<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						</div>					
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group"style="max-width:100px;">
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
							</div>
						</div>					
				</div>
		</div>
		
		<div class="row">        
					<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Accumulate Agent Statistics")%></label>
							<input type='checkbox' name='agntstat' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(agntstat)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.agntstat).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.agntstat).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.agntstat).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('agntstat')"/>

<input type='checkbox' name='agntstat' value=' ' 

<% if(!(sv.agntstat).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('agntstat')"/>
						</div>					
				</div>				
		</div>
		
		<div class="row">        
					<div class="col-md-3">
						<div class="form-group" style="max-width:150px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Age band item")%></label>
							<input name='ageband01' 
type='text'

<%if((sv.ageband01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.ageband01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ageband01.getLength()%>'
maxLength='<%= sv.ageband01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageband01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ageband01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageband01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageband01).getColor()== null  ? 
			"input_cell" :  (sv.ageband01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
					</div>	
				<div class="col-md-5">
									
				</div>			
				<div class="col-md-3">
						<div class="form-group" style="max-width:170px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sum insured band item")%></label>
							<input name='sumband01' 
type='text'

<%if((sv.sumband01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.sumband01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.sumband01.getLength()%>'
maxLength='<%= sv.sumband01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(sumband01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.sumband01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumband01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumband01).getColor()== null  ? 
			"input_cell" :  (sv.sumband01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
				</div>					
		</div>
		
			<div class="row">        
					<div class="col-md-3">
						<div class="form-group" style="max-width:150px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk term band item")%></label>
							<input name='rskband01' 
type='text'

<%if((sv.rskband01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.rskband01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.rskband01.getLength()%>'
maxLength='<%= sv.rskband01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rskband01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.rskband01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.rskband01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.rskband01).getColor()== null  ? 
			"input_cell" :  (sv.rskband01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
					</div>	
				<div class="col-md-5">
									
				</div>			
				<div class="col-md-3">
						<div class="form-group"style="max-width:170px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium band item")%></label>
							<input name='prmband01' 
type='text'

<%if((sv.prmband01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.prmband01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.prmband01.getLength()%>'
maxLength='<%= sv.prmband01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(prmband01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.prmband01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.prmband01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.prmband01).getColor()== null  ? 
			"input_cell" :  (sv.prmband01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
				</div>					
		</div>
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"style="max-width:250px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Accumulate Government Statistics")%></label>
							</br>
							<input type='checkbox' name='govtstat' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(govtstat)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.govtstat).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.govtstat).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.govtstat).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('govtstat')"/>

<input type='checkbox' name='govtstat' value=' ' 

<% if(!(sv.govtstat).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('govtstat')"/>
							
						</div>					
					</div>	
						
		</div>
		
			<div class="row">        
					<div class="col-md-3">
						<div class="form-group"style="max-width:150px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Age band item")%></label>
							<input name='ageband02' 
type='text'

<%if((sv.ageband02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.ageband02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ageband02.getLength()%>'
maxLength='<%= sv.ageband02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageband02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ageband02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageband02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageband02).getColor()== null  ? 
			"input_cell" :  (sv.ageband02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
					</div>	
				<div class="col-md-5">
									
				</div>			
				<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Sum insured band item")%></label>
							<input name='sumband02' 
type='text'

<%if((sv.sumband02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.sumband02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.sumband02.getLength()%>'
maxLength='<%= sv.sumband02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(sumband02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.sumband02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumband02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumband02).getColor()== null  ? 
			"input_cell" :  (sv.sumband02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
				</div>					
		</div>
		
			<div class="row">        
					<div class="col-md-3">
						<div class="form-group"style="max-width:150px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk term band item")%></label>
							<input name='rskband02' 
type='text'

<%if((sv.rskband02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.rskband02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.rskband02.getLength()%>'
maxLength='<%= sv.rskband02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rskband02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.rskband02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.rskband02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.rskband02).getColor()== null  ? 
			"input_cell" :  (sv.rskband02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
					</div>	
				<div class="col-md-5">
									
				</div>			
				<div class="col-md-3">
						<div class="form-group"style="max-width:150px;"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium band item")%></label>
							<input name='prmband02' 
type='text'

<%if((sv.prmband02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.prmband02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.prmband02.getLength()%>'
maxLength='<%= sv.prmband02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(prmband02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.prmband02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.prmband02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.prmband02).getColor()== null  ? 
			"input_cell" :  (sv.prmband02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
						</div>					
				</div>					
		</div>
	</div>					
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
