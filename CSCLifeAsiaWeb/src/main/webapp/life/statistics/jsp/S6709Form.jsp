<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6709";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>

<%S6709ScreenVars sv = (S6709ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  Cat/Year/Fund /Sect /Subsect /Reg /Br /BA /BS /BP /BT /ComYr/Pst/Cur");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Balance for account month ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Year ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract count ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual premium ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single premium ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum insured    ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.acctyr.setReverse(BaseScreenData.REVERSED);
			sv.acctyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.acctyr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.acmn.setReverse(BaseScreenData.REVERSED);
			sv.acmn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.acmn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.commyr.setReverse(BaseScreenData.REVERSED);
			sv.commyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.commyr.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-1"> 
				    		<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>





	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>


			<div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("SCategory")%></label>

<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.statcat)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('statcat')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 


<div class="col-md-2"></div>
<div class="col-md-2"> 
				    		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("AcctYr")%></label>







	<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='acctyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("SttrFund")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.statFund)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('statFund')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 

<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Sect")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.statSect)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('statSect')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
</div>

<div class="row">
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Subsect")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.stsubsect)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('stsubsect')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Reg")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.register)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('register')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
			    	<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Br")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.cntbranch)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('cntbranch')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("BA ")%></label>







<input name='bandage' 
type='text'

<%

		formatValue = (sv.bandage.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandage.getLength()%>'
maxLength='<%= sv.bandage.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandage)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandage).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandage).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandage).getColor()== null  ? 
			"input_cell" :  (sv.bandage).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></div>

<div class="row">
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("BS")%></label>







<input name='bandsa' 
type='text'

<%

		formatValue = (sv.bandsa.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandsa.getLength()%>'
maxLength='<%= sv.bandsa.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandsa)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandsa).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandsa).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandsa).getColor()== null  ? 
			"input_cell" :  (sv.bandsa).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("BP")%></label>




<input name='bandprm' 
type='text'

<%

		formatValue = (sv.bandprm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandprm.getLength()%>'
maxLength='<%= sv.bandprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandprm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandprm).getColor()== null  ? 
			"input_cell" :  (sv.bandprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("BT")%></label>







<input name='bandtrm' 
type='text'

<%

		formatValue = (sv.bandtrm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bandtrm.getLength()%>'
maxLength='<%= sv.bandtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bandtrm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bandtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bandtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bandtrm).getColor()== null  ? 
			"input_cell" :  (sv.bandtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>

<div class="col-md-1"></div>
<div class="col-md-2" style="min-width:150px;">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("ComYr")%></label>






	<%	
			qpsf = fw.getFieldXMLDef((sv.commyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='commyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.commyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.commyr) %>'
	 <%}%>

size='<%= sv.commyr.getLength()%>'
maxLength='<%= sv.commyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(commyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.commyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.commyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.commyr).getColor()== null  ? 
			"input_cell" :  (sv.commyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div></div>

<div class="row">
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Pst")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.pstatcd)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('pstatcd')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div>
			 
<div class="col-md-2"></div>			    	 
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.cntcurr)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('cntcurr')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div> 
			    	</div>
			    	<br/>

<div class="row">
<div class="col-md-2">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Balance for account month")%></label>
<div class="input-group">




<%	
			qpsf = fw.getFieldXMLDef((sv.acmn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acmn);
			
			if(!((sv.acmn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
  



	
  		
		<%					
		if(!((sv.mthldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mthldesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:330px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Account Year")%></label>





	
  		
		<%					
		if(!((sv.acyr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acyr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div></div>

<div class="row">
<div class="col-md-2" style="min-width:180px;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>




  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div></div></div>

<div class="row">
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Contract count")%></label>



	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stcmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			//formatValue = smartHF.getPicFormatted(qpsf,sv.stcmth);
			formatValue=smartHF.getPicFormatted(qpsf,sv.stcmth,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.stcmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>

<div class="col-md-3"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Annual premium")%></label>





	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stpmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stpmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stpmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div></div>

<div class="row">
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Single premium")%></label>





	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stsmth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stsmth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stsmth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div>

<div class="col-md-3"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Sum insured")%></label>


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.stimth).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.stimth,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.stimth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</div></div></div></div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

