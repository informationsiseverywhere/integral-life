

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SJ688";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.statistics.screens.*" %>
<%Sj688ScreenVars sv = (Sj688ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversal Transaction Code");%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                 
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
   <div class="form-group" style="max-width:80px;">                             <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>
 </div>

<div class="row">
<div class="col-md-12">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Reversal Transaction Code")%></label>
                       </div>
                       </div>
                       </div>



<table>
<tr><td>
 
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode01)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode01).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode02)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode02).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode03)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode03).replace("<img","<img id='searchImg' ")%>
</div></td><td>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode04)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode04).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode05)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode05).replace("<img","<img id='searchImg' ")%>
</div>

<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode06)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode06).replace("<img","<img id='searchImg' ")%>
</div></td><td>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode07)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode07).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode08)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode08).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode09)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode09).replace("<img","<img id='searchImg' ")%>
</div>
</td><td>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode10)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode10).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode11)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode11).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode12)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode12).replace("<img","<img id='searchImg' ")%>
</div></td><td>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode13)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode13).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode14)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode14).replace("<img","<img id='searchImg' ")%>
</div>
<div class="input-group" style="min-width:120px;">
<%=smartHF.getRichTextInputFieldLookup(fw, sv.trcode15)%>
<%=smartHF.getHTMLF4NSVarExt(fw, sv.trcode15).replace("<img","<img id='searchImg' ")%>
</div>


</td></tr></table><br/></div>

</div>
<%@ include file="/POLACommon2NEW.jsp"%>

