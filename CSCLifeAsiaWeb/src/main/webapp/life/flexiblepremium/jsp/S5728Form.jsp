<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5728";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.flexiblepremium.screens.*" %>

<%S5728ScreenVars sv = (S5728ScreenVars) fw.getVariables();%>

<%if (sv.S5728screenWritten.gt(0)) {%>
	<%S5728screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Payment");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"As Earned Commission ?");%>
	<%sv.commAsEarned.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Indemnity Commission Payment Pattern  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% Target Received");%>
	<%sv.tgtRcvdPay01.setClassString("");%>
	<%sv.tgtRcvdPay02.setClassString("");%>
	<%sv.tgtRcvdPay03.setClassString("");%>
	<%sv.tgtRcvdPay04.setClassString("");%>
	<%sv.tgtRcvdPay05.setClassString("");%>
	<%sv.tgtRcvdPay06.setClassString("");%>
	<%sv.tgtRcvdPay07.setClassString("");%>
	<%sv.tgtRcvdPay08.setClassString("");%>
	<%sv.tgtRcvdPay09.setClassString("");%>
	<%sv.tgtRcvdPay10.setClassString("");%>
	<%sv.tgtRcvdPay11.setClassString("");%>
	<%sv.tgtRcvdPay12.setClassString("");%>
	<%sv.tgtRcvdPay13.setClassString("");%>
	<%sv.tgtRcvdPay14.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"%");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Released");%>
	<%sv.commRel01.setClassString("");%>
	<%sv.commRel02.setClassString("");%>
	<%sv.commRel03.setClassString("");%>
	<%sv.commRel04.setClassString("");%>
	<%sv.commRel05.setClassString("");%>
	<%sv.commRel06.setClassString("");%>
	<%sv.commRel07.setClassString("");%>
	<%sv.commRel08.setClassString("");%>
	<%sv.commRel09.setClassString("");%>
	<%sv.commRel10.setClassString("");%>
	<%sv.commRel11.setClassString("");%>
	<%sv.commRel12.setClassString("");%>
	<%sv.commRel13.setClassString("");%>
	<%sv.commRel14.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Earned");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% Target Received (up to)");%>
	<%sv.tgtRcvdEarn01.setClassString("");%>
	<%sv.tgtRcvdEarn02.setClassString("");%>
	<%sv.tgtRcvdEarn03.setClassString("");%>
	<%sv.tgtRcvdEarn04.setClassString("");%>
	<%sv.tgtRcvdEarn05.setClassString("");%>
	<%sv.tgtRcvdEarn06.setClassString("");%>
	<%sv.tgtRcvdEarn07.setClassString("");%>
	<%sv.tgtRcvdEarn08.setClassString("");%>
	<%sv.tgtRcvdEarn09.setClassString("");%>
	<%sv.tgtRcvdEarn10.setClassString("");%>
	<%sv.tgtRcvdEarn11.setClassString("");%>
	<%sv.tgtRcvdEarn12.setClassString("");%>
	<%sv.tgtRcvdEarn13.setClassString("");%>
	<%sv.tgtRcvdEarn14.setClassString("");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"%");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Earned");%>
	<%sv.commEarn01.setClassString("");%>
	<%sv.commEarn02.setClassString("");%>
	<%sv.commEarn03.setClassString("");%>
	<%sv.commEarn04.setClassString("");%>
	<%sv.commEarn05.setClassString("");%>
	<%sv.commEarn06.setClassString("");%>
	<%sv.commEarn07.setClassString("");%>
	<%sv.commEarn08.setClassString("");%>
	<%sv.commEarn09.setClassString("");%>
	<%sv.commEarn10.setClassString("");%>
	<%sv.commEarn11.setClassString("");%>
	<%sv.commEarn12.setClassString("");%>
	<%sv.commEarn13.setClassString("");%>
	<%sv.commEarn14.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.commAsEarned.setReverse(BaseScreenData.REVERSED);
			sv.commAsEarned.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.commAsEarned.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.tgtRcvdPay01.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.tgtRcvdPay01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.tgtRcvdPay02.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.tgtRcvdPay02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.tgtRcvdPay03.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.tgtRcvdPay03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.tgtRcvdPay04.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.tgtRcvdPay04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.tgtRcvdPay05.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.tgtRcvdPay05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.tgtRcvdPay06.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.tgtRcvdPay06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.tgtRcvdPay07.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.tgtRcvdPay07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.tgtRcvdPay08.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.tgtRcvdPay08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.tgtRcvdPay09.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.tgtRcvdPay09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.tgtRcvdPay10.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.tgtRcvdPay10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.tgtRcvdPay11.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.tgtRcvdPay11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.tgtRcvdPay12.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.tgtRcvdPay12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.tgtRcvdPay13.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.tgtRcvdPay13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.tgtRcvdPay14.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdPay14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.tgtRcvdPay14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.commRel01.setReverse(BaseScreenData.REVERSED);
			sv.commRel01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.commRel01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.commRel02.setReverse(BaseScreenData.REVERSED);
			sv.commRel02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.commRel02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.commRel03.setReverse(BaseScreenData.REVERSED);
			sv.commRel03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.commRel03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.commRel04.setReverse(BaseScreenData.REVERSED);
			sv.commRel04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.commRel04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.commRel05.setReverse(BaseScreenData.REVERSED);
			sv.commRel05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.commRel05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.commRel06.setReverse(BaseScreenData.REVERSED);
			sv.commRel06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.commRel06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.commRel07.setReverse(BaseScreenData.REVERSED);
			sv.commRel07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.commRel07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.commRel08.setReverse(BaseScreenData.REVERSED);
			sv.commRel08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.commRel08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.commRel09.setReverse(BaseScreenData.REVERSED);
			sv.commRel09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.commRel09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.commRel10.setReverse(BaseScreenData.REVERSED);
			sv.commRel10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.commRel10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.commRel11.setReverse(BaseScreenData.REVERSED);
			sv.commRel11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.commRel11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.commRel12.setReverse(BaseScreenData.REVERSED);
			sv.commRel12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.commRel12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.commRel13.setReverse(BaseScreenData.REVERSED);
			sv.commRel13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.commRel13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.commRel14.setReverse(BaseScreenData.REVERSED);
			sv.commRel14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.commRel14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.tgtRcvdEarn01.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.tgtRcvdEarn01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.tgtRcvdEarn02.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.tgtRcvdEarn02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.tgtRcvdEarn03.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.tgtRcvdEarn03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.tgtRcvdEarn04.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.tgtRcvdEarn04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.tgtRcvdEarn05.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.tgtRcvdEarn05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.tgtRcvdEarn06.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.tgtRcvdEarn06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.tgtRcvdEarn07.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.tgtRcvdEarn07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.tgtRcvdEarn08.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.tgtRcvdEarn08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.tgtRcvdEarn09.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.tgtRcvdEarn09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.tgtRcvdEarn10.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.tgtRcvdEarn10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.tgtRcvdEarn11.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.tgtRcvdEarn11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.tgtRcvdEarn12.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.tgtRcvdEarn12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.tgtRcvdEarn13.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.tgtRcvdEarn13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.tgtRcvdEarn14.setReverse(BaseScreenData.REVERSED);
			sv.tgtRcvdEarn14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.tgtRcvdEarn14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.commEarn01.setReverse(BaseScreenData.REVERSED);
			sv.commEarn01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.commEarn01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.commEarn02.setReverse(BaseScreenData.REVERSED);
			sv.commEarn02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.commEarn02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.commEarn03.setReverse(BaseScreenData.REVERSED);
			sv.commEarn03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.commEarn03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.commEarn04.setReverse(BaseScreenData.REVERSED);
			sv.commEarn04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.commEarn04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.commEarn05.setReverse(BaseScreenData.REVERSED);
			sv.commEarn05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.commEarn05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.commEarn06.setReverse(BaseScreenData.REVERSED);
			sv.commEarn06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.commEarn06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.commEarn07.setReverse(BaseScreenData.REVERSED);
			sv.commEarn07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.commEarn07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.commEarn08.setReverse(BaseScreenData.REVERSED);
			sv.commEarn08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.commEarn08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.commEarn09.setReverse(BaseScreenData.REVERSED);
			sv.commEarn09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.commEarn09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.commEarn10.setReverse(BaseScreenData.REVERSED);
			sv.commEarn10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.commEarn10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.commEarn11.setReverse(BaseScreenData.REVERSED);
			sv.commEarn11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.commEarn11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.commEarn12.setReverse(BaseScreenData.REVERSED);
			sv.commEarn12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.commEarn12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.commEarn13.setReverse(BaseScreenData.REVERSED);
			sv.commEarn13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.commEarn13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.commEarn14.setReverse(BaseScreenData.REVERSED);
			sv.commEarn14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.commEarn14.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company "))%>
					</label>
					<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table "))%>
					</label>
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item "))%>
					</label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:85px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
						</td><td style="padding-left:1px;">
							<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="itemdesc" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 185px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective "))%>
					</label>
					<table><tr><td>
					<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:85px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td style="padding-left:7px;">
		
		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%>
					</label>
					</td><td style="padding-left:7px;">
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:85px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
				</td></tr></table>	
					
					</div></div></div>
					<br>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Commission Payment")%>
					</label>
					
					
					</div></div></div>
					<br>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("As Earned Commission ?")%>
					</label>
					<table>
					<tr><td>
					<input type='checkbox' name='commAsEarned' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(commAsEarned)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.commAsEarned).getColor()!=null){
			 %>
		<%}
		if((sv.commAsEarned).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.commAsEarned).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('commAsEarned')"/>

<input type='checkbox' name='commAsEarned' value=' ' 

<% if(!(sv.commAsEarned).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('commAsEarned')"/>
					</td></tr>
					</table>
					
					</div></div></div>
					
					<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Indemnity Commission Payment Pattern")%>
					</label>
					
					
					</div></div></div>
					<br>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("% Target Received")%>
					</label>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<table><tr><td>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdPay14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td></tr>
					</table>
			</div></div></div>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("%  Released")%>
					</label>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<table><tr><td>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commRel14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td></tr>
					</table>
			</div></div></div>	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Commission Earned")%>
					</label>
					
					
					</div></div></div>
					<br>
					<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("% Target Received (up to)")%>
					</label>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<table><tr><td>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.tgtRcvdEarn14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td></tr>
					</table>
			</div></div></div>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("% Earned")%>
					</label>
					
					</div></div></div>
						<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<table><tr><td>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td><td style="padding-left: 1px;min-width: 46px;">
					<%=smartHF.getHTMLVar(0, 0, fw, sv.commEarn14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</td></tr>
					</table>
			</div></div></div>	
		</div></div>







<%}%>

<%if (sv.S5728protectWritten.gt(0)) {%>
	<%S5728protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<script>
$(document).ready(function() {
	if (screen.height == 900) {
		
		$('#itemdesc').css('max-width','230px')
	} 
if (screen.height == 768) {
		
		$('#itemdesc').css('max-width','210px')
	} 
	
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
