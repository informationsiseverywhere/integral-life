

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5140";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.flexiblepremium.screens.*" %>
<%S5140ScreenVars sv = (S5140ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Target Premium  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Premium     ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency      ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date         ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Details of Premium Received");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Overdue Minimum Required");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Required");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Billed");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Received");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed/Received Variance          ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Variance                  ");%>

<div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("register");
		longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Annual Target Premium")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.anntarprm).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anntarprm).getFieldName());
		//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
				formatValue = smartHF.getPicFormatted(qpsf,sv.anntarprm,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.anntarprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.instalment).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.instalment).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf,sv.instalment,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.instalment.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr></table><br/><br/>
<fieldset class="fieldsetUIG" align='left'>
<legend class="legendUIG">
<%=resourceBundleHandler.gettingValueFromBundle("Details of Premium Received")%>
</legend>
<table width='100%' align ='left' class ='tableFormat' cellspacing="5px">


<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Overdue")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.minPrmReqd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.minPrmReqd).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf,sv.minPrmReqd,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.minPrmReqd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Total Billed")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.totalBilled).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totalBilled).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalBilled,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.totalBilled.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Total Received")%>
</div>
<%}%>


<br/>
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>




<%if ((new Byte((sv.totalRecd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totalRecd).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
				formatValue = smartHF.getPicFormatted(qpsf,sv.totalRecd,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.totalRecd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billed/Received")%>
</div>
<%}%>



<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Variance")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.varnce).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.varnce).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
						formatValue = smartHF.getPicFormatted(qpsf,sv.varnce,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.varnce.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td></tr></table></fieldset><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Details of Premium Received")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Minimum")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Required")%>
</div>
<%}%>

</tr></table></div><br/></div>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>




