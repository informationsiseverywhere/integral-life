

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR627";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr627ScreenVars sv = (Sr627ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Self-Sufficient Age ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary or Exact ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(A/E)");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.zsufcage.setReverse(BaseScreenData.REVERSED);
			sv.zsufcage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.zsufcage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.eaage.setReverse(BaseScreenData.REVERSED);
			sv.eaage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.eaage.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<script>
$(document).ready(function(){
	
	$("#item").attr("class","input-group-addon");
	$("#longdesc").attr("class","form-control");
})
</script>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group" style="max-width:40px"><%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:20px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div></div>
			</div>
			<!-- ILIFE-2422 Coding and Unit testing - Life Cross Browser - Sprint 1 D3: Task 5 starts-->
			<style>

/* for IE 8 */
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>

<div class="col-md-2"></div>
			<!-- ILIFE-2422 Coding and Unit testing - Life Cross Browser - Sprint 1 D3: Task 5 ends-->
			<div class="col-md-2" >
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4" >
				<div class="form-group">

					<label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div 
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:50px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>
							<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div 
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 170px;text-align: left;border:1px solid #ccc !important;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Self-Sufficient Age")%></label>
					<div class="input-group">

						<%	
						qpsf = fw.getFieldXMLDef((sv.zsufcage).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
					%>

						<input name='zsufcage' type='text'
							<%if((sv.zsufcage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							value='<%=smartHF.getPicFormatted(qpsf,sv.zsufcage) %>'
							<%
			 valueThis=smartHF.getPicFormatted(qpsf,sv.zsufcage);
			 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.zsufcage) %>' <%}%>
							size='<%= sv.zsufcage.getLength()%>'
							maxLength='<%= sv.zsufcage.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zsufcage)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<% 
				if((new Byte((sv.zsufcage).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>
							readonly="true" class="output_cell"
							<%
				}else if((new Byte((sv.zsufcage).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
							class="bold_cell" <%
				}else { 
			%>
							class=' <%=(sv.zsufcage).getColor()== null ? 
						"input_cell" :  (sv.zsufcage).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Anniversary or Exact")%></label>
					<% 
			if((new Byte((sv.eaage).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>
					<div
						class='<%= (((sv.eaage.getFormData()).toString().trim() == null)||("".equals((sv.eaage.getFormData()).toString().trim()))) ? 
			"blank_cell" : "output_cell" %>'>
						<%if((sv.eaage.getFormData()).toString().trim() != null){
			if((sv.eaage.getFormData()).toString().trim().equalsIgnoreCase("A"))
			{ %>
						<%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%>
						<%} if((sv.eaage.getFormData()).toString().trim().equalsIgnoreCase("E")){ %>
						<%=resourceBundleHandler.gettingValueFromBundle("Exact")%>

						<%} %>
						<%} %>
					</div>
					<%
			longValue = null;
			%>
					<% }else { %>
					<select value='<%=sv.eaage.getFormData()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(eaage)'
						onKeyUp='return checkMaxLength(this)' name='eaage'
						class="input_cell">

						<%
										
			 %>

						<option value="">...</option>
						<option value="A"
							<% if(sv.eaage.getFormData().equalsIgnoreCase("A")) {%> Selected
							<% }%>><%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%></option>
						<option value="E"
							<% if(sv.eaage.getFormData().equalsIgnoreCase("E")) {%> Selected
							<% }%>><%=resourceBundleHandler.gettingValueFromBundle("Exact")%></option>

						<%
							
								if ((new Byte((sv.eaage).getEnabled())).compareTo(new Byte(
										BaseScreenData.DISABLED)) == 0)  {
							%> readonly="true" class="output_cell"
						<%
								} else if ((new Byte((sv.eaage).getHighLight()))
										.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%> class="bold_cell"

						<%
								} else {
							%> class ='
						<%=(sv.eaage).getColor() == null ? "input_cell"
												: (sv.eaage).getColor().equals("red") ? "input_cell red reverse"
														: "input_cell"%>'

						<%
								}
							%>
					</select>
					<%} %>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>