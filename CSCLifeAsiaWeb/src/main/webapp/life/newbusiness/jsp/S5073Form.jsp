

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5073";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.fsuframework.core.FeaConfg" %>

<%S5073ScreenVars sv = (S5073ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
    	<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group" style="min-width:120px">
				<input name='chdrsel' 
type='text'  id="chdrsel" 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')" >
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>

<%} %>			    		
				      			</div>
				    		</div>
				       </div>
		     </div>
	      </div>
	</div>      

	<div class="panel panel-default">
    	
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	
     <div class="panel-body">     
			<div class="row">	
			
			    <div class="col-md-3">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Issue Contract")%>
					</b></label>
				</div>
				
				<div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Withdraw Proposal")%>
					</b></label>			
			    </div>		
			    
			    <div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Decline Proposal")%>
					</b></label>			
			    </div>    
			        
			
			    <div class="col-md-3">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Postpone Proposal")%>
					</b></label>
				</div></div>
				<div class="row">
				<%if (!FeaConfg.isFeatureExist("2", "NBPRP125", appVars,"IT")) { %>
				<div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Not taken Up Proposal")%>
					</b></label>			
			    </div>	
			    <%} %>
			    <div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Proposal Enquiry")%>
					</b></label>			
			    </div>        
			    
			
			
			
			    <div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "G")%><%=resourceBundleHandler.gettingValueFromBundle("Reverse Decline/Withdraw/Postpone/NTU")%>
					</b></label>
				</div>
         </div>
  </div>
<br>
 </div>
<%@ include file="/POLACommon2NEW.jsp"%>
