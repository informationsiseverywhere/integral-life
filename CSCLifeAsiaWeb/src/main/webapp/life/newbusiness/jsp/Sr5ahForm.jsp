<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR5AH";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<script type="text/javascript">
	function perFormAdd(act,enqy) {
		if(enqy=="0"){	
		var inputs = document.getElementsByTagName('input');
		inputIdValue = '';
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].type.toLowerCase() == 'hidden') {
				inputIdValue = inputs[i].id;
				if (inputIdValue.substring(0, 21) == 'sr5ahscreensfl.select') {
					break;
				}
			}
		}
		document.getElementById(inputIdValue).value = act;

		doAction('PFKEY0');
}
	}

	function selectedRowRadio(idt) {
		document.getElementById(idt).selected = true;
		selectedRow1 = idt;
	}
	function perFormThisOperation(act) {
		if (selectedRow1 != null && selectedRow1 != "") {
			document.getElementById(selectedRow1).value = act;

		} else {
			var elementSelected = false;
			for (var index = 0; index < idRowArray.length; ++index) {
				var item = idRowArray[index];
				if (item != null && item != "") {
					document.getElementById(item).value = act;
					elementSelected = true;
				}
			}

		}
		doAction('PFKEY0');
	}
</script>
<%
	Sr5ahScreenVars sv = (Sr5ahScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind08.isOn()) {
			sv.optdsc01.setEnabled(BaseScreenData.DISABLED);
		} else {
			sv.optdsc01.setEnabled(BaseScreenData.ENABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.optdsc02.setEnabled(BaseScreenData.DISABLED);
		} else {
			sv.optdsc02.setEnabled(BaseScreenData.ENABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.optdsc03.setEnabled(BaseScreenData.DISABLED);
		} else {
			sv.optdsc03.setEnabled(BaseScreenData.ENABLED);
		}
	}
%>
<style>
.panel{
height: 300px !important;
}

</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>


					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Component")%></label>
					<%
						}
					%>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtable.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtable.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<%
			GeneralTable sfl = fw.getTable("sr5ahscreensfl");
		%>


		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr5ah' width='100%'>
						<thead>
							<tr class='info'>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
							</tr>
						</thead>

						<tbody>
							

							<%
								Sr5ahscreensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								boolean isRowAvailable = false;
								while (Sr5ahscreensfl.hasMoreScreenRows(sfl) && !sv.noRecord.toString().equals("Y")) {
									hyperLinkFlag = true;
									long uniqueNumber = sv.uniqueNum.toLong();
									if (uniqueNumber > 0) {
										isRowAvailable = true;
							%>

							<%
								{
											if (sv.noRecord.toString().equals("Y")) {
												sv.select.setInvisibility(BaseScreenData.INVISIBLE);
												sv.excda.setInvisibility(BaseScreenData.INVISIBLE);
												sv.longdesc.setInvisibility(BaseScreenData.INVISIBLE);
												sv.prntstat.setInvisibility(BaseScreenData.INVISIBLE);
											} else {
												sv.select.setInvisibility(BaseScreenData.VISIBLE);
												sv.excda.setInvisibility(BaseScreenData.VISIBLE);
												sv.longdesc.setInvisibility(BaseScreenData.VISIBLE);
												sv.prntstat.setInvisibility(BaseScreenData.VISIBLE);
											}

										}
							%>
							<tr id='tr<%=count%>' height="30">

								<%
									if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>

								<td align="center"><input type="hidden"
									value='<%=sv.select.getFormData()%>'
									name='sr5ahscreensfl.select_R<%=count%>'
									id='sr5ahscreensfl.select_R<%=count%>' /> <input type="radio"
									value='<%=sv.select.getFormData()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp("sr5ahscreensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='sr5ahscreensfl.select_R'
									id='sr5ahscreensfl.select_R<%=count%>'
									onClick="selectedRowRadio('sr5ahscreensfl.select_R<%=count%>')"
									class="radio"
									<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
									disabled="disabled" <%}%> /></td>
								<%
									}
								%>

								<td
									style=<%if (!(((BaseScreenData) sv.excda) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.excda).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										longValue = sv.excda.getFormData();
									%>
									<div id="sr5ahscreensfl.excda_R<%=count%>"
										name="sr5ahscreensfl.excda_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td
									style=<%if (!(((BaseScreenData) sv.longdesc) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										longValue = sv.longdesc.getFormData();
									%>
									<div id="sr5ahscreensfl.longdesc_R<%=count%>"
										name="sr5ahscreensfl.longdesc_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td
									style=<%if (!(((BaseScreenData) sv.prntstat) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.prntstat).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										longValue = sv.prntstat.getFormData();
									%>
									<div id="sr5ahscreensfl.select_R<%=count%>prnt"
										name="sr5ahscreensfl.prntstat_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

							</tr>

							<%
								}
									count = count + 1;
									Sr5ahscreensfl.setNextScreenRow(sfl, appVars, sv);
								}
								if (!isRowAvailable) {
							%>
							<tr id='tr1' height="30">
								<td style="width: 60px; display: none;" align="center"><input
									type="hidden" value='<%="0"%>'
									name='sr5ahscreensfl.select_R1' id='sr5ahscreensfl.select_R1' />
									<input type="radio" value='<%="0"%>' name='select_R1'
									id='select_R1'
									onClick="selectedRow('sr5ahscreensfl.select_R1')"
									class="UICheck" 
									disabled="disabled" /></td>
								<td style="width: 150px; display: none;"></td>
								<td style="width: 200px; display: none;"></td>
								<td style="width: 174px; display: none;"></td>
							</tr>
							<%} %>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="btn-group">
					<div class="sectionbutton">
						<p style="font-size: 12px; font-weight: bold;">
							<%
								if ((sv.optdsc01) != null) {
									String token = ((sv.optdsc01.toString()).indexOf('=') >= 0) ? "=" : "-";
									String[] strControls = (sv.optdsc01.toString()).split(token);
									if (strControls != null && strControls.length > 1) {
										String strKey = strControls[0];
										String strValue = strControls[1];
							%>

							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<a class="btn btn-info btn-xs" href="#"
							<% if((new Byte((sv.optdsc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
                            	disabled
                            <% } %>
								onClick="JavaScript:perFormAdd(<%=strKey%>,<%=sv.isEnquiryMode%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<%
								}
								}
							%>
							<%
								if ((sv.optdsc02) != null) {
									String token = ((sv.optdsc02.toString()).indexOf('=') >= 0) ? "=" : "-";
									String[] strControls = (sv.optdsc02.toString()).split(token);
									if (strControls != null && strControls.length > 1) {
										String strKey = strControls[0];
										String strValue = strControls[1];
							%>

							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<a class="btn btn-info" style="height: 26px;" href="#"
							<% if((new Byte((sv.optdsc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
                            	disabled
                         	<% } %>
                     		 onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%>
							</a>
							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<%
								}
								}
							%>
							<%
								if ((sv.optdsc03) != null) {
									String token = ((sv.optdsc03.toString()).indexOf('=') >= 0) ? "=" : "-";
									String[] strControls = (sv.optdsc03.toString()).split(token);
									if (strControls != null && strControls.length > 1) {
										String strKey = strControls[0];
										String strValue = strControls[1];
							%>
							
							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<a class="btn btn-info btn-xs" style="margin-left: -2px !important;" href="#"
							<% if((new Byte((sv.optdsc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
                            	disabled
                         	<% } %>
								onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							<!-- ILIFE-6853 Checking to disable or enable buttons -->
							<%
								}
								}
							%>
							<%
								if ((sv.optdsc04) != null) {
									String token = ((sv.optdsc04.toString()).indexOf('=') >= 0) ? "=" : "-";
									String[] strControls = (sv.optdsc04.toString()).split(token);
									if (strControls != null && strControls.length > 1) {
										String strKey = strControls[0];
										String strValue = strControls[1];
							%>

							<a class="btn btn-info btn-xs" href="#"
								onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>

							<%
								}
								}
							%>


						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {
		$('#dataTables-sr5ah').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			info : false,
			paging : false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script type="text/javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>

<%
	}
	if ((sv.optdsc04) != null) {
		String token = ((sv.optdsc04.toString()).indexOf('=') >= 0) ? "=" : "-";
		String[] strControls = (sv.optdsc04.toString()).split(token);
		if (strControls != null && strControls.length > 1) {
			String strKey = strControls[0];
			String strValue = strControls[1];
%>



<%
	}
	}
%>



