<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52N";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr52nScreenVars sv = (Sr52nScreenVars) fw.getVariables();%>

<%if (sv.Sr52nscreenWritten.gt(0)) {%>
	<%Sr52nscreen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("A - Update Policy Acknowledgement");%>
	<%StringData generatedText3 = new StringData("B - Policy Despatch Enquiry");%>
	<%StringData generatedText4 = new StringData("Contract Number ");%>
	<%sv.chdrsel.setClassString("");%>
	<%StringData generatedText5 = new StringData("Action          ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind04.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<div class="input-group" style="min-width:110px">
						<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
						<span class="input-group-btn">
			        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
			        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-5">

					<label class="radio-inline">
					<b>	<%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Update Policy Acknowledgement")%></b>
					</label>
				</div>
			<div class="col-md-1">	</div>
				<div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Policy Despatch Enquiry")%></b>
					</label>			
			    </div>	
			    <div class="col-md-3">	</div>	        
			</div>
		</div>
		</div>
<%}%>
<%@ include file="/POLACommon2NEW.jsp"%>