

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR589";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr589ScreenVars sv = (Sr589ScreenVars) fw.getVariables();%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind39.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind39.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Doctor ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Exclusion clause, 2 - Follow up extended text");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act?");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cat ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/L");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reminder Date");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Created Date");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran.No ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Received Date");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Expiry Date");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Remarks");%>
<script>
$(document).ready(function(){
	$("#zdocname").attr("class","form-control");
	$("#ownername").attr("class","form-control");
	
})
</script>
	<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
							<table>
							<tr>
							<td>
									<%					
					if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  
	
				</td>
				<td>



					<%					
					if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  			</td>
  			<td>
	
  		
					<%	
					
					if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'style="margin-left:1px;max-width: 165px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
			%>
  			</td></tr>
  			</table>
																							
													
						</div>
					</div>
				
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
							<table><tr><td>
							
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td>





	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td></tr></table>
	
					
							</div>						
						</div>
						
			     
				<div class="col-md-4">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Doctor")%></label>
						
							<table><tr><td>
		<%					
		if(!((sv.zdoctor.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zdoctor.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zdoctor.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



</td><td>

	
  		
		<%					
		if(!((sv.zdocname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zdocname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zdocname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
											
					</div>
				</div>
			</div>
									
			<div class="row">        
				<div class="col-md-12">
                   <!--  <div class="panel panel-default">
                        <div class="panel-body"> -->				
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-sr589" width="100%">
		                         	<thead>
		                            	<tr class='info'>
											<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Act?") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Cat ") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Life") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("J/L") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Status") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Reminder Date") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Created Date") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction No.") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Received Date") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Expiry Date") %></center></th>
											<th><center><%=resourceBundleHandler.gettingValueFromBundle("Remarks") %></center></th>                                  
		                               </tr>
		                            </thead>
		                            <tbody>
									<%
									GeneralTable sfl = fw.getTable("sr589screensfl");
									Sr589screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr589screensfl.hasMoreScreenRows(sfl)) {
									%>	   
									<tr>                         
										<td>
											<input type="radio" 
												value='<%= sv.select.getFormData() %>' 
								 			 	onFocus='doFocus(this)' onHelp='return fieldHelp("sr589screensfl" + "." +"select")' onKeyUp='return checkMaxLength(this)' 
								 			 	name='sr589screensfl.select_R<%=count%>'
								 				id='sr589screensfl.select_R<%=count%>'
								 				onClick="selectedRow('sr589screensfl.select_R<%=count%>')"
							 				/>
										</td>	
										<td><%= sv.fuptype.getFormData()%></td>								
										<td>
											<%	
												fieldItem=appVars.loadF4FieldsLong(new String[] {"fupcode"},sv,"E",baseModel);
												mappedItems = (Map) fieldItem.get("fupcode");
												longValue = (String) mappedItems.get((sv.fupcode.getFormData()).toString().trim());  
												if(longValue==null ||"".equals(longValue)){
												longValue=sv.fupcode.getFormData();
												}
											%>
											
											<%=longValue%>
											<%
												longValue = null;
												formatValue = null;
											%>								
										</td>   
										<td>
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.lifeno).getFieldName());						
												qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
											%>
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.lifeno);
												if(!sv.
												lifeno
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>								
										</td>
										
										<td><%= sv.jlife.getFormData()%></td>
										
										<td>
											<%	
												fieldItem=appVars.loadF4FieldsLong(new String[] {"fupstat"},sv,"E",baseModel);
												mappedItems = (Map) fieldItem.get("fupstat");
												longValue = (String) mappedItems.get((sv.fupstat.getFormData()).toString().trim());  
												if(longValue==null ||"".equals(longValue)){
												longValue=sv.fupstat.getFormData();
												}
											%>
											
											<%=longValue%>
											<%
												longValue = null;
												formatValue = null;
											%>								
										</td>
										
										<td><%= sv.date_var.getFormData()%></td>
										<td><%= sv.crtdateDisp.getFormData()%></td>
										<td>
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.tranno).getFieldName());						
												qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
											%>
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.tranno);
												if(!sv.tranno.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
											%>
											<%= formatValue%>
											<%
												longValue = null;
												formatValue = null;
											%>								
										</td>
										<td><%= sv.fuprcvdDisp.getFormData()%></td>
										<td><%= sv.exprdateDisp.getFormData()%></td>
										<td><%= sv.fupremk.getFormData()%></td>								
									</tr> 
									<%
									count = count + 1;
									Sr589screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>	                           
		                            </tbody>	                            	
		                         </table>
	                         <!-- </div>
	                    </div>
	                     -->
	                   
	                    
	                  <%-- <div class="sectionbutton">
							<div class='btn-group'>
							
							
								<button type='button' class='btn btn-info' onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Exclusion clause")%></button>
								<button type='button' class='btn btn-info' onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Follow up extended text")%></button>
							</div>
                        </div> --%>
	                    
	                </div>
	             </div>
	             <input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	        </div>
	       
			 <div class="sectionbutton" >
	                    <table><tr><td> 
			<a href="#" onClick="JavaScript:perFormOperation('1')" class='btn btn-info'><%=resourceBundleHandler.gettingValueFromBundle("Exclusion clause")%></a>
			</td> <td> &nbsp;&nbsp;</td> <td>
			<a href="#" onClick="JavaScript:perFormOperation('2')" class='btn btn-info'><%=resourceBundleHandler.gettingValueFromBundle("Follow up extended text")%></a>
			</td></tr></table>
				</div>
		</div>
		
	</div>	

<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;	
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
    var dtmessage =  document.getElementById('msg_lbl').value;
    $('#dataTables-sr589').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: "301px",
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }
          }     
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

