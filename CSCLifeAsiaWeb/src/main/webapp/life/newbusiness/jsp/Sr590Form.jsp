
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR590";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	Sr590ScreenVars sv = (Sr590ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind30.isOn()) {
			sv.fupcdes.setReverse(BaseScreenData.REVERSED);
			sv.fupcdes.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupcdes.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.fupcdes.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.lifeno.setReverse(BaseScreenData.REVERSED);
			sv.lifeno.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.lifeno.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind31.isOn()) {
			sv.lifeno.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.jlife.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind36.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.fupstat.setReverse(BaseScreenData.REVERSED);
			sv.fupstat.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupstat.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind32.isOn()) {
			sv.fupstat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.fupremdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.fupremdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupremdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind33.isOn()) {
			sv.fupremdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.crtdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.fupremk.setReverse(BaseScreenData.REVERSED);
			sv.fupremk.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupremk.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind34.isOn()) {
			sv.fupremk.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind39.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.fuptype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind41.isOn()) {
			sv.fuprcvdDisp.setReverse(BaseScreenData.REVERSED);
			sv.fuprcvdDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fuprcvdDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind41.isOn()) {
			sv.fuprcvdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.exprdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.exprdateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.exprdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind42.isOn()) {
			sv.exprdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Doctor ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"1 - Exclusion clause, 2 - Follow up extended text");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Act?");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/L");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Status");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reminder Date");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Created Date");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cat ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Received Date");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Expiry Date");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Remarks");
%>

<%
	{
		if (appVars.ind35.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
			sv.zdoctor.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind35.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- <script>
$(document).ready(function(){
	
	$("#chdrnum").attr("class","input-group-addon");
	$("#cnttype").attr("class","input-group-addon");
	$("#ctypedes").attr("class","form-control").css("max-width","200px");
	$("#ownername").attr("class","form-control").css("width","200px");
	$("#cownnum").attr("class","input-group-addon");
	//$("#zdoctor").css("max-width","80px");
	
	
})
</script> -->
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
					</label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left: 1px;">
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left: 1px; min-width: 120px;">
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>


				</div>
			</div>
			<!--  Ilife- Life Cross Browser - Sprint 1 D3 : Task 4 Start-->

			<!--  Ilife- Life Cross Browser - Sprint 1 D3 : Task 4 end-->
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
					</label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left: 1px;">
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>



				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Doctor")%>
					</label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.zdoctor).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| fw.getVariables().isScreenProtected()) {
								%>

								<div class="input-group" style="width: 130px;">
									<%=smartHF.getHTMLVarExt(fw, sv.zdoctor)%>
									<span class="input-group-addon"><span
										style="font-size: 19px;"><span
											class="glyphicon glyphicon-search"></span></span></span>

								</div> <%
 	} else {
 %>
								<div class="input-group" style="width: 130px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.zdoctor)%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
											type="button"
											onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> <%
 	}
 %>
							</td>
							<td style="padding-left: 0px;min-width: 100px;max-width: 120px;">
								<%
									if (!((sv.zdocname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zdocname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zdocname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="overflow: hidden; white-space: nowrap; text-overflow: hidden;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>

						</tr>


					</table>









				</div>
			</div>
		</div>




		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[11];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Act?").length() >= (sv.select.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Act?").length()) * 10;
			} else {
				calculatedValue = (sv.select.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Type").length()
					* 12) >= (sv.fupcdes.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Type").length()) * 9;
			} else {
				calculatedValue = ((sv.fupcdes.getFormData()).length() * 9) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Life").length() >= (sv.lifeno.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Life").length()) * 12;
			} else {
				calculatedValue = (sv.lifeno.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("J/L").length() >= (sv.jlife.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("J/L").length()) * 12;
			} else {
				calculatedValue = (sv.jlife.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Status").length()
					* 12) >= (sv.fupstat.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Status").length()) * 12;
			} else {
				calculatedValue = ((sv.fupstat.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Reminder Date").length()
					* 12) >= (sv.fupremdtDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Reminder Date").length()) * 12;
			} else {
				calculatedValue = ((sv.fupremdtDisp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Created Date").length() >= (sv.crtdateDisp.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Created Date").length()) * 10;
			} else {
				calculatedValue = (sv.crtdateDisp.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Cat ").length() >= (sv.fuptype.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Cat ").length()) * 10;
			} else {
				calculatedValue = (sv.fuptype.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Received Date").length()
					* 12) >= (sv.fuprcvdDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Received Date").length()) * 12;
			} else {
				calculatedValue = ((sv.fuprcvdDisp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[8] = calculatedValue;

			if ((resourceBundleHandler.gettingValueFromBundle("Expiry Date").length()
					* 12) >= (sv.exprdateDisp.getFormData()).length() * 12 + 32 + 16) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Expiry Date").length()) * 12;
			} else {
				calculatedValue = ((sv.exprdateDisp.getFormData()).length() * 12) + 32 + 16;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[9] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Remarks").length() >= (sv.fupremk.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Remarks").length()) * 12;
			} else {
				calculatedValue = (sv.fupremk.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[10] = calculatedValue;

			tblColumnWidth[1] = 140;
			tblColumnWidth[4] = 140;
			totalTblWidth += 100;
		%>
		<%
			GeneralTable sfl = fw.getTable("sr590screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr590' width='100%'>
							<thead>
								<tr class='info'>
									<th><center>
											<%=resourceBundleHandler.gettingValueFromBundle("Act?")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Life")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("J/L")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Reminder Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Created Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Cat ")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Received Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Expiry Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Remarks")%></center></th>
								</tr>
							</thead>
							<tbody>

								<%
									Sr590screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									Map<String, Map<String, String>> fupCdesMap = appVars.loadF4FieldsLong(new String[]{"fupcdes"}, sv, "E",
											baseModel);
									Map<String, Map<String, String>> fupStatMap = appVars.loadF4FieldsLong(new String[]{"fupstat"}, sv, "E",
											baseModel);

									while (Sr590screensfl.hasMoreScreenRows(sfl)) {
								%>

								<tr>
										<td align="left"><input type="radio"
										value='<%=sv.select.getFormData()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp("sr590screensfl" + "." +
					 "select")'
										onKeyUp='return checkMaxLength(this)'
										name='sr590screensfl.select_R<%=count%>'
										id='sr590screensfl.select_R<%=count%>'
										onClick="selectedRow('sr590screensfl.select_R<%=count%>')"
										class="radio" /></td>
									<td>
										<%
											if ((new Byte((sv.fupcdes).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| fw.getVariables().isScreenProtected()) {
										%>

										<div class="input-group" style="width: 100px;">
											<%=smartHF.getHTMLVarExt(fw, sv.fupcdes)%>
											<!--  <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> -->

										</div> <%
 	} else {
 %>
										<div class="input-group" style="width: 100px;">
											<%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes)%>
											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
													type="button"
													 
													onClick="doFocus(document.getElementById('<%="sr590screensfl.fupcdes" + "_R" +count %>')); changeF4Image(this); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div> <%
 	}
 %> <%-- <%	
						mappedItems = (Map) fupCdesMap.get("fupcdes");
						optionValue = makeDropDownList( mappedItems , sv.fupcdes,1,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.fupcdes.getFormData()).toString().trim());  
					%>
					<% if((new Byte((sv.fupcdes).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
					%>  
					<div class='output_cell'> 
					   <%=longValue%>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.fupcdes).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='<%="sr590screensfl.fupcdes_R"+count%>' id='<%="sr590screensfl.fupcdes_R"+count%>' type='list' style="width:<%=tblColumnWidth[1 ]%>px;" onclick="showSelect('<%="sr590screensfl.fupcdeslist_R"+count%>','<%="sr590screensfl.fupcdes_R"+count%>')"
					class = 'input_cell'
					>
					<% if((sv.fupcdes).toString().trim().equalsIgnoreCase("")){%>
					   <%=optionValue.replace("---------Select---------","-----Select-----")%>
                    <% }else{%>
		              <option value='<%=(sv.fupcdes.getFormData()).toString().trim()%>' title='<%=longValue%>' SELECTED><%=sv.fupcdes.getFormData()%></option>
                    <% }%>
					</select>
					<select id="<%="sr590screensfl.fupcdeslist_R"+count%>" style="display:none; width:138px;">
	                   <%=optionValue%>
                    </select>
					<% if("red".equals((sv.fupcdes).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>				 --%>
									</td>
							
 <%-- <%	
						mappedItems = (Map) fupCdesMap.get("fupcdes");
						optionValue = makeDropDownList( mappedItems , sv.fupcdes,1,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.fupcdes.getFormData()).toString().trim());  
					%>
					<% if((new Byte((sv.fupcdes).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
					%>  
					<div class='output_cell'> 
					   <%=longValue%>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.fupcdes).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='<%="sr590screensfl.fupcdes_R"+count%>' id='<%="sr590screensfl.fupcdes_R"+count%>' type='list' style="width:<%=tblColumnWidth[1 ]%>px;" onclick="showSelect('<%="sr590screensfl.fupcdeslist_R"+count%>','<%="sr590screensfl.fupcdes_R"+count%>')"
					class = 'input_cell'
					>
					<% if((sv.fupcdes).toString().trim().equalsIgnoreCase("")){%>
					   <%=optionValue.replace("---------Select---------","-----Select-----")%>
                    <% }else{%>
		              <option value='<%=(sv.fupcdes.getFormData()).toString().trim()%>' title='<%=longValue%>' SELECTED><%=sv.fupcdes.getFormData()%></option>
                    <% }%>
					</select>
					<select id="<%="sr590screensfl.fupcdeslist_R"+count%>" style="display:none; width:138px;">
	                   <%=optionValue%>
                    </select>
					<% if("red".equals((sv.fupcdes).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>				 --%>
							<td align="left">
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.lifeno).getFieldName());
												qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.lifeno);
										%> <input type='text' maxLength='<%=sv.lifeno.getLength()%>'
										<%if ((sv.lifeno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right; width: 90px;" <%}%>
										value='<%=formatValue%>' size='<%=sv.lifeno.getLength()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr590screensfl.lifeno)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr590screensfl" + "." + "lifeno" + "_R" + count%>'
										id='<%="sr590screensfl" + "." + "lifeno" + "_R" + count%>'
										class="input_cell"
										style="width:<%=sv.lifeno.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>
									</td>
									<td align="left"><input type='text'
										maxLength='<%=sv.jlife.getLength()%>'
										value='<%=sv.jlife.getFormData()%>'
										size='<%=sv.jlife.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr590screensfl.jlife)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr590screensfl" + "." + "jlife" + "_R" + count%>'
										id='<%="sr590screensfl" + "." + "jlife" + "_R" + count%>'
										class="input_cell" style="width: 48px;"></td>
									<td align="left">
										<%
											mappedItems = (Map) fupStatMap.get("fupstat");
												optionValue = makeDropDownList(mappedItems, sv.fupstat, 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.fupstat.getFormData()).toString().trim());
										%> <%
 	if ((new Byte((sv.fupstat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| fw.getVariables().isScreenProtected()) {
 %>
										<div class='output_cell'>
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.fupstat).getColor())) {
 %>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>
											<select name='<%="sr590screensfl.fupstat_R" + count%>'
												id='<%="sr590screensfl.fupstat_R" + count%>' type='list'
												style="width: 175px;" class='input_cell'>
												<%=optionValue.replace("---------Select---------", "-----Select-----")%>
											</select>
											<%
												if ("red".equals((sv.fupstat).getColor())) {
											%>
										</div> <%
 	}
 %> <%
 	}
 %>
									</td>
									<td align="left">
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field=''
											<%="sr590screensfl" + "." + "fupremdtDisp" + "_R" + count%>' data-link-format="dd/mm/yyyy"
											style="min-width: 150px">
											<input
												name='<%="sr590screensfl" + "." + "fupremdtDisp" + "_R" + count%>'
												id='<%="sr590screensfl" + "." + "fupremdtDisp" + "_R" + count%>'
												type='text' value='<%=sv.fupremdtDisp.getFormData()%>'
												class=" <%=(sv.fupremdtDisp).getColor() == null
						? "input_cell"
						: (sv.fupremdtDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.fupremdtDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr590screensfl.fupremdtDisp)'
												onKeyUp='return checkMaxLength(this)' class="form-controll">
											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</td>
									<td align="left">
										<div style="width: 100px">
											<%
												if ((sv.fupcdes.getFormData().toString().trim()) != null
															&& !(sv.fupcdes.getFormData().toString().trim().equals(""))) {
											%>
											<%=sv.crtdateDisp.getFormData()%>
											<%
												}
											%>
										</div>
									</td>
									<td align="left">
										<%
											if ((sv.fupcdes.getFormData().toString().trim()) != null
														&& !(sv.fupcdes.getFormData().toString().trim().equals(""))) {
										%> <%=sv.fuptype.getFormData()%> <%
 	}
 %>

									</td>
									<td align="left">
										<div class="input-group date form_date col-md-2" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field=''
											<%="sr590screensfl" + "." + "fuprcvdDisp" + "_R" + count%>' data-link-format="dd/mm/yyyy"
											style="min-width: 150px">
											<input
												name='<%="sr590screensfl" + "." + "fuprcvdDisp" + "_R" + count%>'
												id='<%="sr590screensfl" + "." + "fuprcvdDisp" + "_R" + count%>'
												type='text' value='<%=sv.fuprcvdDisp.getFormData()%>'
												class=" <%=(sv.fuprcvdDisp).getColor() == null
						? "input_cell"
						: (sv.fuprcvdDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.fuprcvdDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr590screensfl.fuprcvdDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell"
												style="width:<%=sv.fuprcvdDisp.getLength() * 10%>px;">
											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</td>
									<td align="left">
										<div class="input-group date form_date col-md-2" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field=''
											<%="sr590screensfl" + "." + "exprdateDisp" + "_R" + count%>' data-link-format="dd/mm/yyyy"
											style="min-width: 150px">
											<input
												name='<%="sr590screensfl" + "." + "exprdateDisp" + "_R" + count%>'
												id='<%="sr590screensfl" + "." + "exprdateDisp" + "_R" + count%>'
												type='text' value='<%=sv.exprdateDisp.getFormData()%>'
												class=" <%=(sv.exprdateDisp).getColor() == null
						? "input_cell"
						: (sv.exprdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.exprdateDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sr590screensfl.exprdateDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell"
												style="width:<%=sv.exprdateDisp.getLength() * 10%>px;">
											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</td>
									<td align="left"><input type='text'
										maxLength='<%=sv.fupremk.getLength()%>'
										value='<%=sv.fupremk.getFormData()%>'
										size='<%=sv.fupremk.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(sr590screensfl.fupremk)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sr590screensfl" + "." + "fupremk" + "_R" + count%>'
										id='<%="sr590screensfl" + "." + "fupremk" + "_R" + count%>'
										class="input_cell"
										style="width:<%=sv.fupremk.getLength() * 12%>px;">
									</td>

								</tr>

								<%
									count = count + 1;
										Sr590screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			
		</div>
		<!-- table row -->


		<table>
			<tr>
				<td>

					<div class="btn-group">
						<div class="sectionbutton">


							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Clause")%></a>

							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(1)" style="margin-left:-8px;"><%=resourceBundleHandler.gettingValueFromBundle("Follow Up Extended Text")%></a>


						</div>
					</div>
				</td>
			</tr>

		</table>
		<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;	
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	$('#dataTables-sr590').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }
          }     
  	});
})
</script>

		<%-- <div class="row">
			<div class="col-md-10">
				<div class="btn-group">
					<button class="btn btn-info" onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Clause")%></button>
					<button style="left:10px" class="btn btn-info" onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Follow Up Extended Text")%></button>
				</div>
			</div>
			<div class="col-md-2 text-right">
             <div class="form-controll">
                 <button style="height:30px" class="btn btn-info"  onClick="pressMoreButton('PFKey90');">More</button>
             </div>
            </div>
		</div> --%>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

