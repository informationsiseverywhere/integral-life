
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH565";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh565ScreenVars sv = (Sh565ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Base Currency ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction Codes ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1st Quarter");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2nd Quarter");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3rd Quarter");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4th Quarter");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Period From ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			    </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>  
      <div class="col-md-3">
          <div class="form-group">
            <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>    
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
   </div>
 </div>

<div class="col-md-3">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <table><tr>
              <td>	
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </td>
    </tr></table>
  </div>
 </div>
  </div>  
    
    	<div class="row">	
			<div class="col-md-3"> 
				  <div class="form-group">  	  
					   <label><%=resourceBundleHandler.gettingValueFromBundle("Base Currency")%></label>
					     <div class="input-group" >
<%	
	longValue = sv.currcode.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='currcode' 
id='currcode'
type='text' 
value='<%=sv.currcode.getFormData()%>' 
maxLength='<%=sv.currcode.getLength()%>' 
size='<%=sv.currcode.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(currcode)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.currcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.currcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.currcode).getColor()== null  ? 
"input_cell" :  (sv.currcode).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('currcode')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>

				      			</div>
				    		</div>
			    	</div>
                </div>
            
	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction Codes")%></label>
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
			<%	
	longValue = sv.ztrcde01.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde01' 
id='ztrcde01'
type='text' 
value='<%=sv.ztrcde01.getFormData()%>' 
maxLength='<%=sv.ztrcde01.getLength()%>' 
size='<%=sv.ztrcde01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde01)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde01).getColor()== null  ? 
"input_cell" :  (sv.ztrcde01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
</div>			
</div>

<div class="col-md-2">
			<%					
		if(!((sv.trandesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			</div>
		</div>
<br/>
<div class="row">
			<div class="col-md-3">
				<div class="form-group"></div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group" >
			<%	
	longValue = sv.ztrcde02.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde02' 
id='ztrcde02'
type='text' 
value='<%=sv.ztrcde02.getFormData()%>' 
maxLength='<%=sv.ztrcde02.getLength()%>' 
size='<%=sv.ztrcde02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde02).getColor()== null  ? 
"input_cell" :  (sv.ztrcde02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>		
</div>			
</div>

<div class="col-md-2">
			<%					
		if(!((sv.trandesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			</div>
		</div>
<br/>	

	
<div class="row">
			<div class="col-md-3">
				<div class="form-group"></div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group" >
			<%	
	longValue = sv.ztrcde03.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde03' 
id='ztrcde03'
type='text' 
value='<%=sv.ztrcde03.getFormData()%>' 
maxLength='<%=sv.ztrcde03.getLength()%>' 
size='<%=sv.ztrcde03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde03)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde03).getColor()== null  ? 
"input_cell" :  (sv.ztrcde03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
            </div>			
</div>

<div class="col-md-2">
			<%					
		if(!((sv.trandesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			</div>
		</div>	
<br/>		
<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group" >
			<%	
	longValue = sv.ztrcde04.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde04' 
type='text' 
value='<%=sv.ztrcde04.getFormData()%>' 
maxLength='<%=sv.ztrcde04.getLength()%>' 
size='<%=sv.ztrcde04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde04)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde04).getColor()== null  ? 
"input_cell" :  (sv.ztrcde04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
			
			</div>
		</div>
		
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			</div>
		</div>		
		<br/>	
		
				
<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group" >
			<%	
	longValue = sv.ztrcde05.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde05' 
id='ztrcde05'
type='text' 
value='<%=sv.ztrcde05.getFormData()%>' 
maxLength='<%=sv.ztrcde05.getLength()%>' 
size='<%=sv.ztrcde05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde05)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde05).getColor()== null  ? 
"input_cell" :  (sv.ztrcde05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
			
			
			 </div>
			</div>
			
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
			</div>
		</div>		
		
	<br/>	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
			<%	
	longValue = sv.ztrcde06.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde06' 
id='ztrcde06'
type='text' 
value='<%=sv.ztrcde06.getFormData()%>' 
maxLength='<%=sv.ztrcde06.getLength()%>' 
size='<%=sv.ztrcde06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde06)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde06).getColor()== null  ? 
"input_cell" :  (sv.ztrcde06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
			</div>
			</div>
			
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			</div>
		</div>	
	<br/>	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
			<%	
	longValue = sv.ztrcde07.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde07' 
id='ztrcde07'
type='text' 
value='<%=sv.ztrcde07.getFormData()%>' 
maxLength='<%=sv.ztrcde07.getLength()%>' 
size='<%=sv.ztrcde07.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde07)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde07).getColor()== null  ? 
"input_cell" :  (sv.ztrcde07).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>			

              </div>		
			</div>
			
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			</div>
		</div>	
	<br/>	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
			<%	
	longValue = sv.ztrcde08.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'  style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde08' 
type='text' 
value='<%=sv.ztrcde08.getFormData()%>' 
maxLength='<%=sv.ztrcde08.getLength()%>' 
size='<%=sv.ztrcde08.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde08)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde08).getColor()== null  ? 
"input_cell" :  (sv.ztrcde08).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
			</div>
			</div>
			
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			</div>
		</div>	
		<br/>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
			<%	
	longValue = sv.ztrcde09.getFormData().toString().trim();  
%>

<% 
	if((new Byte((sv.ztrcde09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell' style="width:50px";> 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %>
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde09' 
id='ztrcde09'
type='text' 
value='<%=sv.ztrcde09.getFormData()%>' 
maxLength='<%=sv.ztrcde09.getLength()%>' 
size='<%=sv.ztrcde09.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde09)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.ztrcde09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde09).getColor()== null  ? 
"input_cell" :  (sv.ztrcde09).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>
			</div>
			</div>
			<div class="col-md-2">
		<%					
		if(!((sv.trandesc09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
			</div>
		</div>	
	<br/>	
		
				<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
				</div>
			</div>
			
			<div class="col-md-3" >
   	             <div class="input-group">
<%	
	longValue = sv.ztrcde10.getFormData().toString().trim(); 
%>

<% 
	if((new Byte((sv.ztrcde10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div  class='output_cell'   style="width:50px";  > 
<%if(longValue!=null){%>
   <%=longValue%>
<%} %> 
</div>

<%
longValue = null;
%>

	<% }else {%> 
<input name='ztrcde10' 
type='text' 
value='<%=sv.ztrcde10.getFormData()%>' 
maxLength='<%=sv.ztrcde10.getLength()%>' 
size='<%=sv.ztrcde10.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(ztrcde10)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.ztrcde10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 

<%
	}else if((new Byte((sv.ztrcde10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.ztrcde10).getColor()== null  ? 
"input_cell" :  (sv.ztrcde10).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ztrcde10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%} }%>

			</div>
			</div>
			
			<div class="col-md-2">
		
		<%					
		if(!((sv.trandesc10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			</div>
		</div>
		<br/>
       <div class="row">
                      <div class="col-md-2">
				          <div class="form-group">				
				          </div>
			          </div>
					  <div class="col-md-2">
	                       <div class="form-group">
	                             <label><%=resourceBundleHandler.gettingValueFromBundle("1st Quarter")%> </label>
	                        </div>
		               </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                             <label><%=resourceBundleHandler.gettingValueFromBundle("2nd Quarter")%> </label>
	                        </div>
		               </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                             <label><%=resourceBundleHandler.gettingValueFromBundle("3rd Quarter")%> </label>
	                        </div>
		               </div>
		               <div class="col-md-2">
	                       <div class="form-group">
	                             <label><%=resourceBundleHandler.gettingValueFromBundle("4th Quarter")%> </label>
	                        </div>
		               </div>
	</div>
<div class="row">
          <div class="col-md-2">
				          <div class="form-group">	 
                              <label> <%=resourceBundleHandler.gettingValueFromBundle("Period From")%></label>		
				          </div>
			</div>
					  
			<div class="col-md-2">
	                       <div class="form-group">
	                             <%	
			qpsf = fw.getFieldXMLDef((sv.zmthfrm01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthfrm01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthfrm01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm01) %>'
	 <%}%>

size='<%= sv.zmthfrm01.getLength()%>'
maxLength='<%= sv.zmthfrm01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthfrm01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthfrm01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthfrm01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthfrm01).getColor()== null  ? 
			"input_cell" :  (sv.zmthfrm01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	   </div>
 </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                            <%	
			qpsf = fw.getFieldXMLDef((sv.zmthfrm02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthfrm02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthfrm02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm02) %>'
	 <%}%>

size='<%= sv.zmthfrm02.getLength()%>'
maxLength='<%= sv.zmthfrm02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthfrm02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthfrm02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthfrm02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthfrm02).getColor()== null  ? 
			"input_cell" :  (sv.zmthfrm02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                            
	<%	
			qpsf = fw.getFieldXMLDef((sv.zmthfrm03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthfrm03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthfrm03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthfrm03) %>'
	 <%}%>

size='<%= sv.zmthfrm03.getLength()%>'
maxLength='<%= sv.zmthfrm03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthfrm03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthfrm03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthfrm03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthfrm03).getColor()== null  ? 
			"input_cell" :  (sv.zmthfrm03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
		               <div class="col-md-2">
	                       <div class="form-group">
	                             <%	
			qpsf = fw.getFieldXMLDef((sv.zmthto04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthto04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthto04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthto04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthto04) %>'
	 <%}%>

size='<%= sv.zmthto04.getLength()%>'
maxLength='<%= sv.zmthto04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthto04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthto04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthto04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthto04).getColor()== null  ? 
			"input_cell" :  (sv.zmthto04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
	</div>			
<div class="row">
          <div class="col-md-2">
				          <div class="form-group">	 
                              <label> <%=resourceBundleHandler.gettingValueFromBundle("to")%></label>		
				          </div>
			</div>
					  
			<div class="col-md-2">
	                       <div class="form-group">
	                             <%	
			qpsf = fw.getFieldXMLDef((sv.zmthto01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthto01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthto01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthto01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthto01) %>'
	 <%}%>

size='<%= sv.zmthto01.getLength()%>'
maxLength='<%= sv.zmthto01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthto01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthto01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthto01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthto01).getColor()== null  ? 
			"input_cell" :  (sv.zmthto01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	   </div>
 </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                            <%	
			qpsf = fw.getFieldXMLDef((sv.zmthto02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthto02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthto02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthto02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthto02) %>'
	 <%}%>

size='<%= sv.zmthto02.getLength()%>'
maxLength='<%= sv.zmthto02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthto02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthto02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthto02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthto02).getColor()== null  ? 
			"input_cell" :  (sv.zmthto02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
		                <div class="col-md-2">
	                       <div class="form-group">
	                            
	<%	
			qpsf = fw.getFieldXMLDef((sv.zmthto03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthto03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthto03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthto03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthto03) %>'
	 <%}%>

size='<%= sv.zmthto03.getLength()%>'
maxLength='<%= sv.zmthto03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthto03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthto03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthto03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthto03).getColor()== null  ? 
			"input_cell" :  (sv.zmthto03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
		               <div class="col-md-2">
	                       <div class="form-group">
	                            <%	
			qpsf = fw.getFieldXMLDef((sv.zmthto04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='zmthto04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zmthto04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zmthto04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zmthto04) %>'
	 <%}%>

size='<%= sv.zmthto04.getLength()%>'
maxLength='<%= sv.zmthto04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zmthto04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zmthto04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zmthto04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zmthto04).getColor()== null  ? 
			"input_cell" :  (sv.zmthto04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                        </div>
		               </div>
	</div>	
	</div>	
	</div>					
<%@ include file="/POLACommon2NEW.jsp"%>


