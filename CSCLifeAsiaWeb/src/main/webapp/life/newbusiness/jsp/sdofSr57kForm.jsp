﻿
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import='com.quipoz.framework.util.*'%>
<%@ page import='com.quipoz.framework.screenmodel.*'%>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel'%>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@ page import="com.csc.life.newbusiness.screens.Sr57kScreenVars"%>
<%@ page import="com.quipoz.framework.datatype.BaseScreenData"%>
<%@ page import="com.quipoz.framework.datatype.StringBase"%>
<%@ page import="com.quipoz.framework.error.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.properties.PropertyLoader" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<%
	BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
	ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
	COBOLAppVars cobolAv3 = (COBOLAppVars) bm3.getApplicationVariables();
	response.addHeader("Cache-Control", "no-cache");
	response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");

	String lang1 = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();
	String imageFolder = PropertyLoader.getFolderName(request.getLocale().toString());
	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw3.getScreenName(),request.getLocale());
	
	AppConfig appCfg = AppConfig.getInstance();
//  Ticket #IFSU-285 both polisy and Life use this page, so the appVars need to dynamic load by xma3
//	LifeAsiaAppVars appVars = (LifeAsiaAppVars)bm3.getApplicationVariables();
	Object appVars  = null;
	try{
		Class asiaAppVars = Class.forName("com.csc.lifeasia.runtime.variables.LifeAsiaAppVars").getClass();
		appVars = asiaAppVars.cast(bm3.getApplicationVariables());
	}catch (Exception e1){
		try{
			Class asiaAppVars = Class.forName("com.csc.polisyasiaframework.variables.PolisyAsiaAppVars").getClass();
			appVars = asiaAppVars.cast(bm3.getApplicationVariables());
		}catch(Exception e2){
			
		}
	}
	
%>

<%
	
				Sr57kScreenVars sv = (Sr57kScreenVars) fw3.getVariables();
%>

<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--  LINK REL="StyleSheet" HREF="theme/<%=lang1.toLowerCase() %>/style.css" TYPE="text/css" -->
<!-- bootstrap -->
<link href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
<link href="../../../bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
<script src='../../../bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='../../../bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src='../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='../../../bootstrap/sb/dist/js/sb-admin-2.js'></script>
<script src="../../../bootstrap/integral/integral-admin-sidebar.js"></script>

<link
	href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- bootstrap end -->

<script language='javaScript'>

function doAction(act) {
	  /*   parent.frames[1].document.form1.action_key.value = act;
	    parent.frames[1].doSub(); */
	    parent.frames["mainForm"].document.form1.action_key.value = act;
	    parent.frames["mainForm"].doSub();
		pageloading();
	  }

	<%char[] c = cobolAv3.additionalValidKeys.toCharArray();
			String validKeys = "[";
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '1') {
					validKeys += i + ",";
				}
			}
			if (validKeys.endsWith("]")) {
				validKeys += "";//empty array
			} else {
				validKeys = validKeys.substring(0, validKeys.length() - 1)
						+ "]";
			}%>
	var validKeyArr = <%=validKeys%>;
	var isSupported = false;
	var lang1 = "<%=lang1%>";
	function isSupportKey(validKeyArr,action){
		if (validKeyArr.length > 0){
			for (var i=0;i<validKeyArr.length;i++){
				if (validKeyArr[i] == action){
					isSupported = true;
					break;
				}
			}
			if (isSupported){
				clearFField(); // fix bug46
				doAction('PFKEY0'+action);
			}else{
			/*<% if ("CHI".equals(lang1)) { %>
				alert("功能键" + action + "不能在当前屏幕使用");
			<% } else {%>
				alert("Function key " + action + " is not active on this screen at this time.");
			<%}%>*/
			callCommonAlert(lang1,"No0002",action);
			}
		}else{
			//alert("no keys are supported");
			callCommonAlert(lang1,"No00020");
		}
	}
	    
  
</script>

<%@ page session="false"%>
<title>Generic SideBar</title>
<%-- <LINK REL="StyleSheet"
	HREF="../../../theme/<%=lang1.toLowerCase()%>/style.css"
	TYPE="text/css">
 --%>

<%
	HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel != null) {
		baseModel.getApplicationVariables();
		ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
		SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter) AppVars.hf;
%>


<script type="text/javascript">
	function hyperLinkTo(nextField) {
		var fieldName = nextField.name;
		var doc = parent.frames["mainForm"].document;
		doc.getElementById(fieldName).value = "X";
		doAction('PFKEY0');
	}
</script>

</HEAD>
<BODY id="sideBar" style="background-color: #ffffff;font-size: 13px !important; border: 1px solid gainsboro !important;">
	<style>
@media screen and (-webkit-min-device-pixel-ratio:0) {
	#bgImg {
		margin-top: -5px;
	}
}

@media all and (min-width:0) {
	#bgImg {
		margin-top: -5px;
	}
}
</style>
	<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) Begin-->
	<%
		String msgs = resourceBundleHandler.gettingValueFromBundle("No Message");
			String fontStyle = " font-family:Arial; font-weight:bold; font-size:12px;";
			String lang = request.getParameter("lang");
			/**if ("CHI".equalsIgnoreCase(lang)) {
				msgs = "没有信息";
				fontStyle = " font-type:宋体; font-weight:normal; font-size:12px;";
			}*/
			try {
				sess = request.getSession();
				BaseModel bm = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE);
				AppVars avs = bm.getApplicationVariables();
				lang = avs.getUserLanguage().toString().trim();
				if (!avs.mainFrameLoaded) {
					for (int i = 0; i < 40; i++) {
						avs.waitabit(250);
						if (avs.mainFrameLoaded) {
							break;
						}
					}
				}
				if (!avs.mainFrameLoaded) {
					msgs = resourceBundleHandler.gettingValueFromBundle("Messages failed to load after 10 seconds.")
							+ "<br>"
							+ resourceBundleHandler.gettingValueFromBundle(
									"This is usually caused by slow response on the main form; errors may be incorrect.")
							+ "<br>"
							+ resourceBundleHandler.gettingValueFromBundle(
									"Press the refresh button here when the main form loads.")
							+ "<button onClick='document.location.reload(false)'>"
							+ resourceBundleHandler.gettingValueFromBundle("Refresh errors") + "</Button>";
				} else {
					String ctx = request.getContextPath() + "/";
					MessageList list = avs.getMessages();
					Iterator i = list.iterator();
					StringBuffer sb = new StringBuffer();
					while (i.hasNext()) {
						String str = i.next().toString();
						HTMLFormatter formatter = new HTMLFormatter();

						/* Remove any trailing ? which is historic and means "stop underlining" */
						str = QPUtilities.removeTrailing(str.trim(), "?");
						//added by wayne to parse errorno
						String[] arr = str.split("ErrorMessage");
						if (arr.length < 2)
							sb.append(str + "<br>");
						else {
							sb.append(arr[0]);
								for (int j = 1; j < arr.length - 1; j++) {
								sb.append("<a href='#'>")
										.append(formatter
												.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?")))
										.append("</a><br/>");
							}
							sb.append("<a href='#'>")
									
									.append(formatter.HTMLIfy(
											QPUtilities.removeTrailing(arr[arr.length - 1].substring(4), "?")))
									.append("</a>");
							sb.append("<br>");
						}
					}

					msgs = sb.toString();

					msgs = msgs.replaceFirst("Message:", "");
				}
			} catch (Exception e) {
			}
	%>

	<script language="javascript">
	//boostrap
		$(document).ready(function() {
	if(<%=msgs.length()%> == 0)	$("#msg-panel").css("display","none");
		calculateMenuHeight();
		});
	function calculateMenuHeight(){
		var screenBodyHeight = screen.height;
		var sidebarBodyHeight = $("#sideBar").height();
   		//var logoHeight = $("#topPanel").outerHeight(true);
		// ILIFE-8864 STARTS
		var logoHeight = 0; 
		if($("#sidebar-hide").length > 0){
			logoHeight = 20;
		}
		// ILIFE-8864 ENDS
   		var sidebar = $(".sidearea");
   		var message = $(".err-msg");
   		var messPane = $("#msg-panel");
   		if(screenBodyHeight > 1000) {
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = sidebarBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = sidebarBodyHeight - logoHeight;
   			}
   		}else{  
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = screenBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = screenBodyHeight - logoHeight;
   			}
   		}
   		sidebar.css('height', sidebarHeight + "px");
   	}
	</script>
<style>
.row.message {
	padding-left: 15px;
}
#msg-panel> .panel-heading{
	 font-weight: bolder !important;
	 text-align: center !important;
	 padding-right: 40px !important;
}
/* #msg-panel{
     top: 305px !important;
} */
</style>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
//public static final boolean ieEmulationOn = true;	//IGROUP-1211 make emulation to be configurable
public static String emulationVer="off";			//IGROUP-1211
%>
<%	
	// IGroup-1086 add the brower version in common jsp
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	emulationVer = "off";	//IGROUP-1211
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10; //IGROUP-1211
		}
	} else if (userAgent.contains("Trident/7.0")) {
			browerVersion = IE11;
		    if(AppConfig.ieEmulationEnable) emulationVer =  IE11;	//IGROUP-1211
	}		
%>
	<%-- <div id="topPanel" class="row" style="border:0px;margin:0 0 0 -1;background-color:#2c2cce;height: 50px;width: 300px;">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; margin-top: 2px;background-color: #2c2cce;">
				<% if ( browerVersion.equals(Chrome)) {%>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;">				
				<%}else{ %>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;height: 59px">	
				<%} %>
					<li><a><img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/dxc_symbol_wht_rgb_150.png" width="55px" border="0"/></a></li>
					<li><a style="left: -15px;top: -4px;"><div style="font-size:15px;font-weight: bold;color: white !important;">INTEGRAL Admin</div>
						<table style="font-size: 10px !important;font-weight: bold;width:91%;">
				         <tr>
					         	<td style="color: #ffffff !important;"><%=AppConfig.getInstance().getVersion() %> </td>
					         	<td style="text-align: right; color: #fff !important;"><%=fw.getScreenName()%></td>
					         </tr>
				         </table>
				         </a>
					</li>
				</ul>
			</nav>
	 </div> --%>
	<div class="sidearea" style="overflow: auto;">
<%
	// To decide whether menus are displayed or not
		LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel
				.getApplicationVariables();
		boolean isShow = av.isMenuDisplayed();
		if (!isShow) {
%>
<!-- ILIFE-8864 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<div id="sidebar-hide" style="padding-left: 93%;">
	<span id="sidebar-pin" class="glyphicon glyphicon-pushpin" style="line-height: 20px; cursor:pointer"></span>
	<span id="sidebar-unpin" class="glyphicon glyphicon-chevron-left" style="line-height: 20px; cursor:pointer"></span>
</div>

<script>
	$(document).ready(function(){
		var mainDoc=parent.frames["mainForm"];
		var msgDiv = document.getElementById("msg-panel");
		if(msgDiv.style.display !=="none"){
			mainDoc.document.getElementById("msg-panel").innerHTML += msgDiv.innerHTML;
			mainDoc.document.getElementById("msg-panel").style.display="block";
		}	
	})
	if(sessionStorage.getItem("sidebar") !== "pined"){
		$("#sidebar-pin").css("display", "block");
		$("#sidebar-unpin").css("display", "none");
	}else{
		$("#sidebar-pin").css("display", "none");
		$("#sidebar-unpin").css("display", "block");
		window.top.document.getElementsByName("realContent")[0].cols = "275,*";
	}
	$("#sidebar-hide>span").click(function(){
		if(sessionStorage.getItem("sidebar") === "pined"){
			$("#sidebar-pin").css("display", "block");
			$("#sidebar-unpin").css("display", "none");
			sessionStorage.setItem("sidebar", "unpined");
			window.top.document.getElementsByName("realContent")[0].cols = "0,*"
		}else{
			$("#sidebar-pin").css("display", "none");
			$("#sidebar-unpin").css("display", "block");
			sessionStorage.setItem("sidebar", "pined");
		}
	});
</script>
<%}%>
<!-- ILIFE-8864 ENDS-->

		<!-- <div class="logoarea" style="height: 50px;">
			<a class="navbar-brand" href="#">Integral Admin</a>
		</div> -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="side-menu">
				<li class="active">
                       
							<a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Extra_Info")%></span></a>
														
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
								<%
										if (sv.ddind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='ddind' id='ddind' type='hidden'  value="<%=sv.ddind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Direct Credit")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Direct Credit")%>
                                    <%}%>
                                    
                                    <!-- icon -->
                                    <%
                                    if (sv.ddind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.ddind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
                                <li>
                                <%
										if (sv.crcind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
                                    <input name='crcind' id='crcind' type='hidden'  value="<%=sv.crcind.getFormData()%>">
                                    <!-- text -->
                                    <%
                                    if((sv.crcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.crcind
                                            .getEnabled()==BaseScreenData.DISABLED)){
                                    %> 
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payout ")%>
                                    <%
                                    } else {
                                    %>
                                    <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("crcind"))' class="hyperLink"> 
                                        <%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payout ")%>
                                    <%}%>
                                    
                                    <!-- icon -->
                                    <%
                                    if (sv.crcind.getFormData().equals("+")) {
                                    %> 
                                    <i class="fa fa-tasks fa-fw sidebar-icon"></i>
                                    <%}
                                    if (sv.crcind.getFormData().equals("X")) {
                                    %>
                                    <i class="fa fa-warning fa-fw sidebar-icon"></i> 
                                    <%}%>
                                    </a>
                                    <%} %>
                                </li>
                               
							</ul>
							</span>
						</li>
					    <li class="active"><a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Functions")%></a> 
						<ul class="nav nav-second-level" aria-expanded="true">
							
						</ul></li>

				</ul>
			</div>
		</div>
<%} %>

		<div class="panel-group" id="accordion">
			<div class="panel panel-info" id='msg-panel'>
			    <div class="panel-heading" style=" text-align: center;padding-right: 40px;">
			        <h4 class="panel-title" style="font-size: 13px !important;font-weight: bolder;">
			            <%=resourceBundleHandler.gettingValueFromBundle("Messages")%>
			        </h4>
			    </div>
			    <div class="panel-body" style="font-size: 13px !important; colour:red;">
					<div class="row" style="padding-left: 15px !important;">	
							<%=msgs%>
					</div>
			    </div>
			</div>	
		</div>

	<%
		}
		
	%>
	</div>


</BODY>
</HTML>
