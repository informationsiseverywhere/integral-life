

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH575";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh575ScreenVars sv = (Sh575ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Headings on ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule 1");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule 2");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"SI Notation ");%>
<style>
.input-group-addon{
 width:100%;
}
</style>
<div class="panel panel-default">

    	<div class="panel-body">
    	
    	  <div class="row"> 
    	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
    	        <input class="form-control" type="text" style="width: 50px" placeholder=<%=sv.company.getFormData().toString()%> disabled>
    	      </div>
    	    </div>
    	    
    	     <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
    	        <input class="form-control" type="text" style="width: 60px" placeholder=<%=sv.tabl.getFormData().toString()%> disabled>
    	      </div>
    	     </div>
    	     
    	     <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
    	        
    	        <table>
	    	       <tr>
		    	       <td>
		    	       	 <%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
		    	       </td>
		    	       <td>
		    	       	  <%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc) %> 
		    	       </td>
	    	       </tr>
    	       </table>
    	       </div>
    	     </div>
    	     
    	   </div>
    	   
    	   <div class="row">
    	   
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule 1")%></label>
    	        <input name='scrtitle01' type='text' style="width: 270px;"

               <%

						formatValue = (sv.scrtitle01.getFormData()).toString();
				
				%>
					value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.scrtitle01.getLength()%>'
				maxLength='<%= sv.scrtitle01.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(scrtitle01)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.scrtitle01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.scrtitle01).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.scrtitle01).getColor()== null  ? 
							"input_cell" :  (sv.scrtitle01).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
    	      </div>
    	    </div>
    	    
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule 2")%></label>
    	        <input name='scrtitle02' type='text' style="width: 270px;"

				<%
				
						formatValue = (sv.scrtitle02.getFormData()).toString();
				
				%>
					value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.scrtitle02.getLength()%>'
				maxLength='<%= sv.scrtitle02.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(scrtitle02)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.scrtitle02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.scrtitle02).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.scrtitle02).getColor()== null  ? 
							"input_cell" :  (sv.scrtitle02).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
            </div>
           </div>
           
      	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("SI Notation")%></label>         
    	        <input name='ttdesc' type='text' style="width: 270px;"

			<%
			
					formatValue = (sv.ttdesc.getFormData()).toString();
			
			%>
				value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
			
			size='<%= sv.ttdesc.getLength()%>'
			maxLength='<%= sv.ttdesc.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(ttdesc)' onKeyUp='return checkMaxLength(this)'  
			
			
			<% 
				if((new Byte((sv.ttdesc).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
				readonly="true"
				class="output_cell"
			<%
				}else if((new Byte((sv.ttdesc).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			
			<%
				}else { 
			%>
			
				class = ' <%=(sv.ttdesc).getColor()== null  ? 
						"input_cell" :  (sv.ttdesc).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
			 
			<%
				} 
			%>
			>
     </div>
   </div>
   
  </div>
    	    



<div style='visibility:hidden;display:none'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Headings on")%>
</div>

</div>
<%//=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

