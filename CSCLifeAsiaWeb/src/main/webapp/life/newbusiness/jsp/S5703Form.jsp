<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5703";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%S5703ScreenVars sv = (S5703ScreenVars) fw.getVariables();%>

<%if (sv.S5703screenWritten.gt(0)) {%>
	<%S5703screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Age ");%>
	<%sv.riskCessAge.setClassString("");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Term ");%>
	<%sv.riskCessTerm.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Age ");%>
	<%sv.premCessAge.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Term ");%>
	<%sv.premCessTerm.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Date ");%>
	<%sv.riskCessDateDisp.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Date ");%>
	<%sv.premCessDateDisp.setClassString("");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund Split Plan ");%>
	<%sv.virtFundSplitMethod.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Relief Indicator ");%>
	<%sv.taxind.setClassString("");%>
<%	sv.taxind.appendClassString("string_fld");
	sv.taxind.appendClassString("input_txt");
	sv.taxind.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.virtFundSplitMethod.setReverse(BaseScreenData.REVERSED);
			sv.virtFundSplitMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.virtFundSplitMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
	<div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</div>
		       		</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       	</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			       		<!-- <div class="input-group"> -->
			       		<table><tr><td>
			       		<%					
							if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.item.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.item.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="max-width: 900px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  		</td><td>
							<%					
							if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="max-width: 900px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td></tr></table>
			       		<!-- </div> -->
		       		</div>
		       	</div>
		    </div> 
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
		       			<table>
				    		<tr>
				    			<td>
				    				<%if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    			<td style="padding-left: 10px;padding-right: 10px;"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
				    			<td>
				    				<%if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {			
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    		</tr>
				    	</table>
		       		</div> 
		     	</div>
		     </div>
		     
		     
	
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
					<%=generatedText6%>
					<%}%></label>
					<div class="input-group">
					<%if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) {%>
						
						
						<%	
						qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						%>
						
						<input name='riskCessAge' 
						type='text'
						
						<%if((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
						<%
						valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessAge);
						if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
						<%}%>
						
						size='<%= sv.riskCessAge.getLength()%>'
						maxLength='<%= sv.riskCessAge.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)' onKeyUp='return checkMaxLength(this)'  
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						
						<% 
						if((new Byte((sv.riskCessAge).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						readonly="true"
						class="output_cell"
						<%
						}else if((new Byte((sv.riskCessAge).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
						class="bold_cell" 
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.riskCessAge).getColor()== null  ? 
						"input_cell" :  (sv.riskCessAge).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
						
						<%
						} 
						%>
						>
						<%}%>
					
		       		</div>
		       	</div>
		       	</div>
		       		
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
					<%=generatedText7%>
					<%}%></label>
					<div class="input-group">
					<%if ((new Byte((sv.riskCessTerm).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) {%>
					
					
					<%	
					qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
					qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					
					%>
					
					<input name='riskCessTerm' 
					type='text'
					
					<%if((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
					value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessTerm) %>'
					<%
					valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessTerm);
					if(valueThis!=null&& valueThis.trim().length()>0) {%>
					title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessTerm) %>'
					<%}%>
					
					size='<%= sv.riskCessTerm.getLength()%>'
					maxLength='<%= sv.riskCessTerm.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					
					<% 
					if((new Byte((sv.riskCessTerm).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"
					<%
					}else if((new Byte((sv.riskCessTerm).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
					class="bold_cell" 
					
					<%
					}else { 
					%>
					
					class = ' <%=(sv.riskCessTerm).getColor()== null  ? 
					"input_cell" :  (sv.riskCessTerm).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>'
					
					<%
					} 
					%>
					>
					<%}%>
					</div>
		       		</div>
		       	</div>
		    </div>
		    
		  		     
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
					<%=generatedText8%>
					<%}%></label>
					<div class="input-group">
		     		<%if ((new Byte((sv.premCessAge).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) {%>
						
						
						<%	
						qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						%>
						
						<input name='premCessAge' 
						type='text' 
						
						<%if((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						value='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
						<%
						valueThis=smartHF.getPicFormatted(qpsf,sv.premCessAge);
						if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
						<%}%>
						
						size='<%= sv.premCessAge.getLength()%>'
						maxLength='<%= sv.premCessAge.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)' onKeyUp='return checkMaxLength(this)'  
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						
						<% 
						if((new Byte((sv.premCessAge).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						readonly="true"
						class="output_cell"
						<%
						}else if((new Byte((sv.premCessAge).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
						class="bold_cell" 
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.premCessAge).getColor()== null  ? 
						"input_cell" :  (sv.premCessAge).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' 
						
						<%
						} 
						%>
						>
						<%}%>
		       		</div>
		       	</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
					<%=generatedText9%>
					<%}%>
					</label>
					<div class="input-group">
		       		<%if ((new Byte((sv.premCessTerm).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) {%>
						
						
						<%	
						qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						%>
						
						<input name='premCessTerm' 
						type='text' 
						
						<%if((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						value='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
						<%
						valueThis=smartHF.getPicFormatted(qpsf,sv.premCessTerm);
						if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
						<%}%>
						
						size='<%= sv.premCessTerm.getLength()%>'
						maxLength='<%= sv.premCessTerm.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)' onKeyUp='return checkMaxLength(this)'  
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						
						<% 
						if((new Byte((sv.premCessTerm).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						readonly="true"
						class="output_cell"
						<%
						}else if((new Byte((sv.premCessTerm).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
						class="bold_cell" 
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.premCessTerm).getColor()== null  ? 
						"input_cell" :  (sv.premCessTerm).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' 
						
						<%
						} 
						%>
						>
						<%}%>
					</div>
		       		</div>
		       	</div>
		    </div>
		    
		   
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
					<%=generatedText13%>
					<%}%></label>
					<div class="input-group" style="min-width:100px;">
					<%	
						if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
							longValue = sv.riskCessDateDisp.getFormData();  
						%>
						
						<% 
						if((new Byte((sv.riskCessDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>  
							<%if(longValue != null){%>
							
							<%=longValue%>
							
							<%}%>
						</div>
						
						<%
						longValue = null;
						%>
						<% }else {%>
						 
						
						
						<% 
						if((new Byte((sv.riskCessDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						
						
						
						<%
						}else if((new Byte((sv.riskCessDateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>	
					
						 
						 <div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy">
									<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div> 
								
											
						<%
						}else { 
						%>
						
						class = ' <%=(sv.riskCessDateDisp).getColor()== null  ? 
						"input_cell" :  (sv.riskCessDateDisp).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						 <div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy">
									<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div> 
						
						
						
						<%} }}%>
		       		</div>
		       	</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0) { %>
						<%=generatedText14%>
						<%}%>
					</label>
					<div class="input-group"style="min-width:100px;">
					<%	
						if ((new Byte((sv.premCessDateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.premCessDateDisp.getFormData();  
						%>
						
						<% 
						if((new Byte((sv.premCessDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>  
						<%if(longValue != null){%>
						
						<%=longValue%>
						
						<%}%>
						</div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						
						
						<% 
						if((new Byte((sv.premCessDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						
						<%
						}else if((new Byte((sv.premCessDateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>	
						
						
						 <div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy">
									<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div> 
								
						
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.premCessDateDisp).getColor()== null  ? 
						"input_cell" :  (sv.premCessDateDisp).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy">
									<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div> 
								
											
						<%} }}%>
		       		</div>
		       	</div>
		    </div>
		    </div>
		    
		  
		     
		    <div class="row">
	        	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label>	<%=generatedText10%></label>
		       		<div class="input-group" style="max-width:140px;">
		       		<input name='virtFundSplitMethod' id='virtFundSplitMethod'
						type='text' 
						value='<%=sv.virtFundSplitMethod.getFormData()%>' 
						maxLength='<%=sv.virtFundSplitMethod.getLength()%>' 
						size='<%=sv.virtFundSplitMethod.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(virtFundSplitMethod)' onKeyUp='return checkMaxLength(this)'  
						
						<% 
						if((new Byte((sv.virtFundSplitMethod).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						readonly="true"
						class="output_cell"	 >
						
						<%
						}else if((new Byte((sv.virtFundSplitMethod).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>	
						class="bold_cell" >
						 
												
						<span class="input-group-btn">
									<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('virtFundSplitMethod')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.virtFundSplitMethod).getColor()== null  ? 
						"input_cell" :  (sv.virtFundSplitMethod).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						
						<span class="input-group-btn">
									<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('virtFundSplitMethod')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
						
						<%} %>
						</div>
		       		</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label><%=generatedText11%></label>
		       		<div class="input-group" style="min-width:65px;">
		       		<input name='taxind' 
						type='text'
						
						<%
						
						formatValue = (sv.taxind.getFormData()).toString();
						
						%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.taxind.getLength()%>'
						maxLength='<%= sv.taxind.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(taxind)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
						if((new Byte((sv.taxind).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						readonly="true"
						class="output_cell"
						<%
						}else if((new Byte((sv.taxind).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
						class="bold_cell" 
						
						<%
						}else { 
						%>
						
						class = ' <%=(sv.taxind).getColor()== null  ? 
						"input_cell" :  (sv.taxind).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
						
						<%
						} 
						%>
						>
		       		</div>
		       	</div>
		    </div>
		    </div>	
	</div>
</div>
<%}%>

<%if (sv.S5703protectWritten.gt(0)) {%>
	<%S5703protect.clearClassString(sv);%>

	<%
{
	}

	%>
<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>
