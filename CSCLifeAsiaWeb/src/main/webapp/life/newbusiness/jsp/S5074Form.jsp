<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5074";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5074ScreenVars sv = (S5074ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prm Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm. Date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Concurrent Proposal ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commence ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No of policies ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Currency ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Register ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Source");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Business ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cross Ref.Type ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Campaign ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statistical");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Collection Currency ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract suspense ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract fee ");%>
	<%StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Deposit   ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tolerance ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total required ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Available ");%>
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ready to issue - Press ENTER to put in force");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.hcurrcd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.totlprm.setReverse(BaseScreenData.REVERSED);
			sv.totlprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.totlprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.totamnt.setReverse(BaseScreenData.REVERSED);
			sv.totamnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.totamnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()
		|| appVars.ind02.isOn()) {
			generatedText39.setInvisibility(BaseScreenData.INVISIBLE); 
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
		//ILJ-44
		if (appVars.ind121.isOn()) {
    			sv.premRcptDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
    		}
		if (appVars.ind122.isOn()) {
			sv.concommflg.setInvisibility(BaseScreenData.INVISIBLE);
		}
        if (appVars.ind123.isOn()) {
			sv.rskcommdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
        if (appVars.ind124.isOn()) {
			sv.decldateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
        if (appVars.ind125.isOn()) {
			sv.rskcommdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
//end	
	}

	%>



<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4" >
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		<!-- <div class="input-group"> -->
					    		<table><tr><td>
					    		<div class='<%= (sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style="min-width:20px;">
						    		<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td><td>
	<%					
					if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px !important;
    min-width: 170px;
   ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
					</td></tr></table>
				      			<!-- </div> -->
				    		</div>
				    		</div>
				    	
				    
				    		<div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prm Status")%></label>
							
							<!-- <div class="input-group"  > -->
							<table><tr><td>
							<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statdsc"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statdsc");
	longValue = (String) mappedItems.get((sv.statdsc.getFormData()).toString());  
%>


	<div 	
	class='<%= (sv.statdsc.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'style="max-width:300px;">
<%					
if(!((sv.statdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>									
				</td><td>			
			
			    		
						<span class="input-group"><%=resourceBundleHandler.gettingValueFromBundle("/")%></span>
						</td><td>
							<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"premStatDesc"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("premStatDesc");
	longValue = (String) mappedItems.get((sv.premStatDesc.getFormData()).toString());  
%>


	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px ; min-width: 70px;">
<%					
if(!((sv.premStatDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue(longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
							</td></tr></table>			
						<!-- </div> --></div>
				   </div>	</div>
				   
				   
				   <div class="row">
			    	<div class="col-md-4">
						<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
						<table><tr><td>
						<!--  <div class="input-group"> -->	
							<div class='<%= (sv.ownersel.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.ownersel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownersel.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownersel.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td><td>
	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px ; min-width: 100px;">
    
    
<%					
if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	

										</td></tr></table>
						<!-- </div> --></div></div>
				 
				   
				   
				 
				   
				   
				 
				 
				 
				   <div class="col-md-4">
						<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Concurrent Proposal")%></label>
					    		<%					
if(!((sv.conprosal.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.conprosal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.conprosal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		
		
<input type='checkbox' name='conprosal' value='Y' readonly="readonly"

disabled
<%
		if(formatValue.trim().equalsIgnoreCase("Y")){
			%>checked
		<%}%>
class ='UICheck'/>
</div>	
<%
longValue = null;
formatValue = null;
%>
					    		</div>
					    		
					    		 <!-- ILJ-44 : Start -->
			 <% if ((new Byte((sv.concommflg).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commencement Flag")%></label>
					<%
						if (!((sv.concommflg.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.concommflg.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.concommflg.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>


					<input type='checkbox' name='concommflg' value='Y'
						readonly="readonly" disabled
						<%if (formatValue.trim().equalsIgnoreCase("Y")) {%> checked
						<%}%> class='UICheck' />
				</div>
				<%
					longValue = null;
						formatValue = null;
				%>
			</div>
			<%
				}
			%>
			<!-- ILJ-44 : End -->
					    	</div> 
				    	 
							
				 <%-- <table>
				 <tr>
				 <td style="padding-top:37px; width:154px">
				 
				 <label style="font-size: 90%;"><%=resourceBundleHandler.gettingValueFromBundle("Concurrent Proposal")%></label>
				 
				 </td>
				 
				 <td>
							<%					
if(!((sv.conprosal.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.conprosal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.conprosal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		
		<class ='UICheck' onclick="handleCheckBox('conprosal')"/>
							<input type='checkbox' name='exact' value=' ' 
							<% if(!(sv.exact).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<% } %>
							style="visibility: hidden" onclick="handleCheckBox('exact')" class ='UICheck'/>
		<div class="checkbox">
<input type='checkbox' name='conprosal' value='Y' readonly="readonly"

disabled
<%
		if(formatValue.trim().equalsIgnoreCase("Y")){
			%>checked
		<%}%>
class ='UICheck'/>
	
<%
longValue = null;
formatValue = null;
%>		

</td></tr></table> --%>
						

		<!-- </div> -->					   
				     
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
				    		<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Comm. Date")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	  	  
					    	
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div class='<%= (sv.occdateDisp.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
					    		
								
								
								</div>
							</div></div>
					
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					    			<div class="input-group"  style="max-width:300px;">	
					    		<div class='<%= (sv.lassured.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'  style="max-width:200px;">
<%					
if(!((sv.lassured.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lassured.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lassured.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

							</div></div>
					</div>
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("No of policies")%></label>
					    		<div class="input-group"  style="max-width:300px;">	
					    		<%	
			qpsf = fw.getFieldXMLDef((sv.polinc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.polinc);
			
			if(!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" />
		
		<% 
			} 
		%>
		
	
							</div></div>
					</div>
				</div>
				
 <!-- ILJ-44 start  --> 

	<% if ((new Byte((sv.rskcommdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label>

					<div class="input-group" style="max-width: 400px;">
						<div
							class='<%=(sv.rskcommdateDisp.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.rskcommdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.rskcommdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.rskcommdateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>



					</div>
				</div>
			</div>
<% } %>
<% if ((new Byte((sv.decldateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>		 			
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Declaration Date")%></label>

					<div class="input-group" style="max-width: 400px;">
						<div
							class='<%=(sv.decldateDisp.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.decldateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.decldateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.decldateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>



					</div>
				</div>
			</div>
<% } %>
<% if ((new Byte((sv.premRcptDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>		 
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("First Premium Receipt Date")%></label>

					<div class="input-group" style="max-width: 400px;">
						<div
							class='<%=(sv.premRcptDateDisp.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
							<%
								if (!((sv.premRcptDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.premRcptDateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.premRcptDateDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>



					</div>
				</div>
			</div>
			</div>
		<%
			}
		%>

<!-- ILJ-44 end  --> 

				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.billfreq.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%	
//Amit hack
	fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("billfreq");
	longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString()); 
					
if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
							</div></div>
					</div>
					
				
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Method")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.mop.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%	


	//Amit hack
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mop");
	longValue = (String) mappedItems.get((sv.mop.getFormData()).toString());  

				
if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mop.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mop.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

							</div></div>
					</div>
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Commence")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.billcdDisp.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.billcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billcdDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billcdDisp.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

							</div>
					</div>
				</div></div>
				
				
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.cntcurr.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
							</div></div>
					</div>
					
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Currency")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.billcurr.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.billcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	

					</div></div>
			</div>
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Register")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		
	<div 	
	class='<%= (sv.register.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.register.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
							</div>
					</div></div>
				
				</div>
				
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Cross Ref.Type")%></label>
					    		<div class="input-group"  style="min-width:100px;">	
	<div 	
	class='<%= (sv.reptype.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.reptype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.reptype.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.reptype.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

							</div></div>
					</div>
					
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Number")%></label>
					    		<div class="input-group"  style="min-width:100px;">	
					    		<div 	
	class='<%= (sv.lrepnum.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.lrepnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lrepnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lrepnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	
							</div>
					</div></div>
					
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Source")%></label>
					    		<div class="input-group"  style="max-width:400px;">	
					    		<div 	
	class='<%= (sv.srcebus.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%		
    fieldItem=appVars.loadF4FieldsLong(new String[] {"srcebus"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("srcebus");
	longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString());  			
if(!((sv.srcebus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.srcebus.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.srcebus.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

							</div>
					</div>
					</div>
				</div>
				
				
				
				<div class="row">	
			    	<div class="col-md-4" style="max-width:600px;">
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing")%></label>
					    		 <!-- <div class="input-group"  > -->
					    		 <table><tr><td>
					    		<div class='<%= (sv.cntbranch.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="max-width:100px;">
<%					
if(!((sv.cntbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntbranch.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntbranch.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td><td>
	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px ; min-width: 90px;">
<%					
if(!((sv.sbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.sbrdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.sbrdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	
								
								
								<!-- </div> -->
								</td></tr></table>
							</div>
					</div>
				
				
				
					
					<div class="col-md-4" >
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					    	
					    	<table><tr><td>
					    		<div 	
								class='<%= (sv.agntsel.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>'  style="max-width:100px;">
							<%					
							if(!((sv.agntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.agntsel.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.agntsel.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td>
						<td>
						
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'  style="margin-left: 2px;  min-width: 90px; ">
						<%					
						if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>

	
								
								
							
								</td></tr></table>
							</div>
					</div>
				</div>
			
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Campaign")%></label>
					    		<div class="input-group"  style="min-width:80px;">	
					    		<div 	
	class='<%= (sv.campaign.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.campaign.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.campaign.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.campaign.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	
							</div></DIV>
					</div>
					
				
					<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Collection Currency")%></label>
					    			<div class="input-group"  style="min-width:80px;">	
					    		<div 	
	class='<%= (sv.hcurrcd.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.hcurrcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hcurrcd.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.hcurrcd.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	
							</div></div>
					</div> 
					
					
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Statistical")%></label>
				    	
				    	
				    	<table>
				    	<tr>
				    	
				    	<td>
				    		<% 
if(!((sv.stcal.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcal.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		
	
	<div 	
	class='<%= (sv.stcal.getFormData()).trim().length() == 0 ? 
					"output_cell" : "output_cell" %>' 
						<%if("".equals(formatValue.trim())) {%>
		style="width: 50px;"
		<%} %>
					  >

		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
				    	
				    	</td>
				    	
				    	<td>
				    	
				    		<%					
if(!((sv.stcbl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcbl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcbl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>


	<div 	
	class='<%= (sv.stcbl.getFormData()).trim().length() == 0 ? 
					"output_cell" : "output_cell" %>'
					<%if("".equals(formatValue.trim())) {%>
		style="width: 50px;margin-left: 1px;"
		<%} %>
					>

		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
				    	</td>
				    	
				    	
				    	
				    	
				    	<td>
				    	
				    	<%					
if(!((sv.stccl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stccl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stccl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>

	<div 	
	class='<%= (sv.stccl.getFormData()).trim().length() == 0 ? 
					"output_cell" : "output_cell" %>'
					<%if("".equals(formatValue.trim())) {%>
		style="width: 50px;margin-left: 1px;"
		<%} %>
					
					>

		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
				    	
				    	</td>
				    	
				    	
				    	<td><%					
if(!((sv.stcdl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcdl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.stcdl.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>





	<div 	
	class='<%= (sv.stcdl.getFormData()).trim().length() == 0 ? 
					"output_cell" : "output_cell" %>'
					<%if("".equals(formatValue.trim())) {%>
	style="width: 50px;margin-left: 1px;"
		<%} %>
					
					>

		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
								</td>
								</tr></table>
								
				
					
					
					</div></div></div>
					
			
					<table style='width:680px; '>
	<tr>
		<td width='251px'>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Premium")%>
				</div>
		</td><td width='351px' >
		
						<%	
							qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
														formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" align="right" id="instPrem">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="output_cell"  id="instPrem"></div>
						
						<% 
							} 
						%>
					
		</td>	
		
	
		
		
		<td width='500px' >
	
					<div class="label_txt">
						<%=resourceBundleHandler.gettingValueFromBundle("Contract suspense")%>
					</div>
		</td>
	
		<td width='251px' >
		
							<%	
							qpsf = fw.getFieldXMLDef((sv.cntsusp).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
														formatValue = smartHF.getPicFormatted(qpsf,sv.cntsusp,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.cntsusp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell"  align="right" id="cntsusp">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="output_cell"  id="cntsusp"></div>
						
						<% 
							} 
						%>
				
		
		</td></tr>
		
	
	<tr>
		<td width="125px">
			<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Contract fee")%>
			</div>
		</td>
		<td width='301px' align="left">
							<%	
								qpsf = fw.getFieldXMLDef((sv.cntfee).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.cntfee,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								
								if(!((sv.cntfee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell"  style="margin-top: 2px;" align=right id="cntfee">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="output_cell" style="margin-top: 2px;" align=left id="cntfee"></div>
							
							<% 
								} 
							%>
							
		</td>	
		
		
		<td width="125px">
		
			<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Premium Deposit")%>
			</div>
		</td>
		
		<td width='251px' style="padding-top: 2px;">

				<%	
					qpsf = fw.getFieldXMLDef((sv.prmdepst).getFieldName());
				//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
									formatValue = smartHF.getPicFormatted(qpsf,sv.prmdepst,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					
					if(!((sv.prmdepst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if(longValue == null || longValue.equalsIgnoreCase("")) { 			
							formatValue = formatValue( formatValue );
							} else {
							formatValue = formatValue( longValue );
							}
					}
			
					if(!formatValue.trim().equalsIgnoreCase("")) {
				%>
						<div class="output_cell"   align=right id="prmdepst">	
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
				<%
					} else {
				%>
				
						<div class="output_cell"  id="prmdepst"></div>
				
				<% 
					} 
				%>
		</td>
	</tr>
	
	<tr>
			<%
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			//ILIFE-1477 Start
              formatValue =  smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
            //ILIFE-1477 Ends
			if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
				} else {
					formatValue = formatValue( longValue );
				}
			}
			if(formatValue.trim().equalsIgnoreCase("")) { %>
			<td width="251px"></td><td width="251px"></td>
			<% } else { %>
			<td width='251px'>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Taxes")%>
				</div>
			</td>
			<td width='251px'>
							<%	
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell"  style="margin-top: 1px;" align="right" id="taxamt">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>							
								<div class="output_cell" style="margin-top: 1px;" id="taxamt">	</div>						
							<% 
								} 
							%>
							
		</td> <% } %>
		<td width='251px'>
			<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Tolerance")%>
			</div>
		</td>
		<td width='251px' style="padding-top: 2px;">
							<%	
								qpsf = fw.getFieldXMLDef((sv.tolerance).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.tolerance);
								
								if(!((sv.tolerance.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="margin-top: 1px;" align="right" id="tolerance">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="output_cell" style="margin-top: 1px;" id="tolerance"></div>
							
							<% 
								} 
							%>
							
		</td>
	</tr>
	<tr>
		<td width="125px">
			<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Total required")%>
			</div>
		</td>
		<td >				<%	
							qpsf = fw.getFieldXMLDef((sv.totlprm).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.totlprm,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							

							
							
							if(!((sv.totlprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell"   align="right" id="totlprm">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="output_cell"  id="totlprm"></div>
						
						<% 
							} 
						%>
						
		</td>
		<td width="125px">
			<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Total Available")%>
			</div>
		</td>
		<td width='251px' style="padding-top: 2px;">
						<%	
								qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.totamnt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								
								
								if(!((sv.totamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell"  align="right" id="totamnt">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
									<!-- ILIFE-1499 ENDS  -->
							<%
								} else {
							%>
							
									<div class="output_cell"  id="totamnt"></div>
							
							<% 
								} 
							%>
							
		</td>
	</tr>
</table>

	</div></div>
	
<script type="text/javascript">


$(document).ready(function(){
	
	$("#totamnt").width(150)
	$("#totlprm").width(150)
	$("#tolerance").width(150)
	$("#taxamt").width(150)
	$("#prmdepst").width(150)
	$("#cntfee").width(150)	
	$("#cntsusp").width(150)
	$("#instPrem").width(150)
	
	
});



</script>
	
    
<%@ include file="/POLACommon2NEW.jsp"%>
