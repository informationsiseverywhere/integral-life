

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6276";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6276ScreenVars sv = (S6276ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"            ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ref ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Key");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
			<div class="panel-body">
			<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover"  id='dataTables-s6276'
					width="100%">
					<thead>
						<tr class='info'>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
							
						</tr>
					</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s6276screensfl");
									S6276screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S6276screensfl.hasMoreScreenRows(sfl)) {
								%>

								<tr class="tableRowTag" id='<%="tablerow" + count%>' >
									<%
										if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>





									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.select.getLength()%>'
											value='<%=sv.select.getFormData()%>'
											size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6276screensfl.select)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="s6276screensfl" + "." + "select" + "_R" + count%>'
											id='<%="s6276screensfl" + "." + "select" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.select.getLength() * 12%> px;">

									</div>




									</td>
									<%
										} else {
									%>
									<%
										}
									%>
									<%
										if ((new Byte((sv.mandref).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag" style="width: 100px;"
										<%if ((sv.mandref).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><a
										href="javascript:;" class='tableLink'
										onClick='document.getElementById("<%="s6276screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.mandref.getFormData()%></span></a>




									</td>
									<%
										} else {
									%>
									<td class="tableDataTag" style="width: 100px;"></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag" style="width: 100px;"
										<%if ((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.effdateDisp.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag" style="width: 100px;"></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.bankkey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag" style="width: 120px;"
										<%if ((sv.bankkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.bankkey.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag" style="width: 120px;"></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag" style="width: 200px;"
										<%if ((sv.bankacckey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.bankacckey.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag" style="width: 200px;"></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.mandstat).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td class="tableDataTag" style="width: 200px;"
										<%if ((sv.mandstat).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.mandstat.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td class="tableDataTag" style="width: 200px;"></td>

									<%
										}
									%>

								</tr>

								<%
									count = count + 1;
										S6276screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>


							</tbody>

						</table>
			</div>
		</div>
	</div>
</div>
</div>
</div>


<script>
$(document).ready(function() {
	$('#dataTables-s6276').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '450px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

