

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM510";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sm510ScreenVars sv = (Sm510ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month     ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date From   ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to    ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"   ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mnth.setReverse(BaseScreenData.REVERSED);
			sv.mnth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mnth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.year.setReverse(BaseScreenData.REVERSED);
			sv.year.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.year.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<style>
#acctmonth{
 width:100px;
}
#acctyear{
width:50%;
}
</style>
<div class="panel panel-default">

<div class="panel-body">
    	
    	  <div class="row">  	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Number / Name")%></label>
    	     
    	             <table><tr><td>
    	          <%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="max-width:30px">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
	</td>
	<td>
  		
		<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;" id="scheduleName">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	          
            </td>
            </tr>
            </table>
    	          </div>
    	         </div>   
    	          <div class="col-md-4"></div>
    	   <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
    	          	
				<%					
				if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
    	     </div> 	
    	     </div>   
    	    </div>
    	    
    	
    	    
    	  <div class="row">  	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	         		
					<%					
					if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  
    	      </div>
    	    </div>
    	    <div class="col-md-4"></div> 
    	   <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
    	        <table><tr>
    	        	<td><%=smartHF.getHTMLVarReadOnly(fw, sv.acctmonth)%></td>
    	          <td style="padding-left: 1px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.acctyear) %></td>
    	         </tr></table> 
                
    	      </div>
    	    </div>
        </div>
        
        <div class="row">  	  
    	   <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
    	       	
					<%					
					if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="max-width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
    	      
    	      </div>
    	    </div>
    	     <div class="col-md-4"></div>
    	   <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
    	      		
					<%					
					if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  
    	      
    	      </div>
    	    </div>
        </div>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mnth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);			
	%>
        <div class="row">  	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
    	          <div class="row">  	  
    	           <div class="col-md-2" style="padding-right:0px;">
    	              <div class="form-group">
<input name='mnth' 
type='text'

<%if((sv.mnth).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth) %>'
	 <%}%>

size='<%= sv.mnth.getLength()%>'
maxLength='<%= sv.mnth.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth).getColor()== null  ? 
			"input_cell" :  (sv.mnth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
                 </div>
                </div>
                 <div class="col-md-3"  style="padding-left:0px;">
    	              <div class="form-group">
	<%	
			qpsf = fw.getFieldXMLDef((sv.year).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='year' 
type='text'

<%if((sv.year).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.year) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.year);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.year) %>'
	 <%}%>

size='<%= sv.year.getLength()%>'
maxLength='<%= sv.year.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(year)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.year).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.year).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.year).getColor()== null  ? 
			"input_cell" :  (sv.year).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
                     </div>
                    </div>
                   </div>
                  </div>
                 </div>
                  <div class="col-md-4"></div>
                 <div class="col-md-4">
    	          
    	            
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date From")%></label>
				    	<table>
				    		<tr>
				    			<td>
				    				<%					
		if(!((sv.datefrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.datefrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.datefrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td><td> &nbsp; </td>
				    			<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td><td> &nbsp; </td>
				    			<td>
				    				<%if(!((sv.datetoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {			
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.datetoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.datetoDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    		</tr>
				    	</table>
				    </div>
    	            	              
    	          
                 
                 
                </div>

</div>
</div> 
</div>
            
<%@ include file="/POLACommon2NEW.jsp"%>
