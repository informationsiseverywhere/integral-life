<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5002";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%--<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S5002ScreenVars sv = (S5002ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Create New Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Modify Unissued Contract");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - U/W Approval");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Follow-up Status Update");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Contract Proposal Enquiry");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F - Contract Enquiry");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"G - U/W Approval Reversal");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"H - Fast Track Issue");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"I - Change Proposal Contract Type");%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"J - Print U/W Worksheet");
%>
<%-- ILIFE-3139 --%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind31.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.chdrtype.setReverse(BaseScreenData.REVERSED);
			sv.chdrtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.chdrtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	   <div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					    		<div class="input-group" style="min-width:110px">
						    		<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div>
			    	<div class="col-md-3">
			    	</div>
			        <div class="col-md-3">
			        	<div class="form-group">
			        	  	<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Type"))%></label>
							<% if ((new Byte((sv.chdrtype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
							fieldItem=appVars.loadF4FieldsLong(new String[] {"chdrtype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("chdrtype");
							optionValue = makeDropDownList( mappedItems , sv.chdrtype.getFormData(),2,resourceBundleHandler); 
							longValue = (String) mappedItems.get((sv.chdrtype.getFormData()).toString().trim());	
							%>			        	  	
								<%=smartHF.getDropDownExt(sv.chdrtype, fw, longValue, "chdrtype", optionValue) %>
							<%} %>			        	  	
			       		</div>
			        </div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
    	
    <!-- 	<div class=" panel-default">
    	  	<div class="panel-body">   -->  
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Create New Contract")%></b>
					</label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
					<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Modify Unissued Contract")%></b>
					</label>			
			    </div>		        
				
			    <div class="col-md-4">
					  <label class="radio-inline">
					  <b> <%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("U/W Approval")%></b>
					  </label>
				</div>
				      
			</div>		
			<div class="row">	
			<div class="col-md-4">	  			
					  <label class="radio-inline">
					<b>   <%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Follow-up Status Update")%></b>
					 </label>			
			    </div>		  
			    <div class="col-md-4">
					  <label class="radio-inline">
					<b>    <%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Proposal Enquiry")%></b>
					  </label>
				</div>	  	
				<div class="col-md-4">	  		
					  <label class="radio-inline">
					<b>   <%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%></b>
					 </label>			
			    </div>		        
			
	
			           
			</div>	
			<div class="row">	
			<div class="col-md-4">
					  <label class="radio-inline">
					<b>  <%=smartHF.buildRadioOption(sv.action, "action", "G")%><%=resourceBundleHandler.gettingValueFromBundle("U/W Approval Reversal")%></b>
					</label>	
				</div>	  
				<div class="col-md-4">		
					  <label class="radio-inline">
					<b>  <%=smartHF.buildRadioOption(sv.action, "action", "H")%><%=resourceBundleHandler.gettingValueFromBundle("Fast Track Issue")%></b>
					  </label>			
			    </div>		 
			    <div class="col-md-4">
					  <label class="radio-inline">
					<b>    <%=smartHF.buildRadioOption(sv.action, "action", "I")%><%=resourceBundleHandler.gettingValueFromBundle("Change Proposal Contract Type")%></b>
					  </label>	
				</div>
			</div>	
			<div class="row">	
			<div class="col-md-4">	
					  <label class="radio-inline">
					<b>   <%=smartHF.buildRadioOption(sv.action, "action", "J")%><%=resourceBundleHandler.gettingValueFromBundle("Print UW WorkSheet")%></b>
					 </label>
				</div>
				<%
						if ((new Byte((sv.action).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>	
				<div class="col-md-4">	
					  <label class="radio-inline">
					<b>   <%=smartHF.buildRadioOption(sv.action, "action", "K")%><%=resourceBundleHandler.gettingValueFromBundle("Tentative Approval")%></b>
					 </label>
				</div>
				<div class="col-md-4">	
					  <label class="radio-inline">
					<b>   <%=smartHF.buildRadioOption(sv.action, "action", "L")%><%=resourceBundleHandler.gettingValueFromBundle("Tentative Approval Reversal")%></b> <!-- IBPLIFE-2472 -->
					 </label>
				</div>
				<%} %>	
				<div class="col-md-4"></div>  					        
			</div>										
		</div></div>
		<!-- </div>
	</div> -->

<%@ include file="/POLACommon2NEW.jsp"%>
