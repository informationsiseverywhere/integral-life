

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6278";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S6278ScreenVars sv = (S6278ScreenVars) fw.getVariables();%>

	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor Company        ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor Client Number  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Reference    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Status Code");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Times to Use         ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date       ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Sort Code       ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account Number  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factoring House      ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Amount       ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Currency     ");%>

<%{
		if (appVars.ind04.isOn()) {
			sv.mandAmt.setReverse(BaseScreenData.REVERSED);
			sv.mandAmt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mandAmt.setInvisibility(BaseScreenData.INVISIBLE);
			sv.mandAmt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.mandAmt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.timesUse.setReverse(BaseScreenData.REVERSED);
			sv.timesUse.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.timesUse.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText3.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.facthous.setReverse(BaseScreenData.REVERSED);
			sv.facthous.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.facthous.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Payor Company")%></label>
					<table><tr><td>
						<%
							if (!((sv.payrcoy.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrcoy.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrcoy.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>


						<%
							if(!((sv.compdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.compdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.compdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:300px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



					</td>
					</tr>
					</table>
				</div>
			</div></div>
			
			
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Payor Client Number")%></label>
					<table><tr><td>
						<%
							if (!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.payrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>

						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:300px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		
		<hr>
		
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Reference")%></label>
					<div class="input-group">
					<%
						if (!((sv.mandref.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.mandref.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.mandref.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div></div>
			</div>
				<div class="col-md-2"></div>
			<!-- <div class="col-md-4"></div> -->
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Status Code")%></label>
					<table><tr><td>
						<%
							if (!((sv.mandstat.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mandstat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mandstat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td>


						<%
							if (!((sv.statdets.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statdets.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statdets.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:300px;margin-left: 1px;min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</td>
					</tr>
					</table>

				</div>
			</div>
		</div>
		<%--row 3 --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Times to Use")%></label> 
					<div class="input-group">
						<input name='timesUse' type='text'
						<%formatValue = (sv.timesUse.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.timesUse.getLength()%>'
						maxLength='<%=sv.timesUse.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(timesUse)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.timesUse).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell1"
						<%} else if ((new Byte((sv.timesUse).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.timesUse).getColor() == null ? "input_cell"
						: (sv.timesUse).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div></div>
			</div>
				<div class="col-md-2"></div>
			<!-- <div class="col-md-4"></div> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
						<%
								if((new Byte((sv.effdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-8" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dobDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>

				</div>
			</div>
		</div>
		<%-- row 4 --%>
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Sort Code")%></label>
					<table><tr><td class="input-group">

						<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
						<input name='bankkey' id='bankkey' 
		type='text' 
		value='<%=sv.bankkey.getFormData()%>' 
		maxLength='<%=sv.bankkey.getLength()%>' 
		size='<%=sv.bankkey.getLength()%>'
		onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  
		
		<% 
			if((new Byte((sv.bankkey).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
		%>  
		readonly="true"
		class="output_cell1"	 >
		
		<%
			}else if((new Byte((sv.bankkey).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
		%>	
		class="bold_cell" >
									 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
						<%
			}else { 
		%>
		
		class = ' <%=(sv.bankkey).getColor()== null  ? 
		"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
		"input_cell red reverse" : "input_cell" %>' >
		
							 <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
			

					<%} %>
		</td>
		<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
<%
		String style = "";
		String style2 = "";
		if (browerVersion.equals("IE11")) {
			style = "margin-left:1px;font-size:14px;";
			style2 = "font-size:14px";
		}
		/* ILIFE2418 starts */
		if (browerVersion.equals("Chrome")) {
			style = "margin-left:1px;font-size:14px;";
			style2 = "font-size:14px;";
		}
		if (browerVersion.equals("IE8")) {
			style = "margin-left:1px;font-size:14px;";
			style2 = "font-size:14px";
		}
		/* ILIFE2418 ends */
%>
		
		
	
	<td>	
		
		
  		
		<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</td>
		<td>
		
  		
		<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>'style="max-width:300px;margin-left: 1px;" ><!-- payment screen for ie11 by liwei 2016.3.3  -->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		


				</td>
				</tr>
				</table>
				</div>
			</div>
		
		
		
		
			<div class="col-md-7">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Account Number")%></label>
					<div class="input-group">

						<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
						<input name='bankacckey' id='bankacckey' type='text'
							value='<%=sv.bankacckey.getFormData()%>'
							maxLength='<%=sv.bankacckey.getLength()%>'
							size='<%=sv.bankacckey.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(bankacckey)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell1">

						<%
							} else if ((new Byte((sv.bankacckey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > 
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.bankacckey).getColor() == null ? "input_cell"
						: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > 
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>



						<%
							if (!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell1" : "output_cell1"%>'
							style=<%=style2%>>
							<!-- payment screen for ie11 by liwei 2016.3.3  -->
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>




					</div>
				</div>
			</div>
		</div>
		<%-- row 5 --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Factoring House")%></label>

					<%
						if ((new Byte((sv.facthous).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[] { "facthous" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("facthous");
							optionValue = makeDropDownList(mappedItems, sv.facthous.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.facthous.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.facthous).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell1" : "output_cell1"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.facthous).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
						<%
							}
						%>
						<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
						<select name='facthous' type='list' id='facthous'
							<%if ((new Byte((sv.facthous).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell1"
							<%} else if ((new Byte((sv.facthous).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.facthous).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
						}
					%>

				</div>
			</div>
				<div class="col-md-2"></div>
			<!-- <div class="col-md-4"></div> -->
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Amount")%></label>
	<div class="input-group">
					<%
						if ((new Byte((sv.mandAmt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<%
						qpsf = fw.getFieldXMLDef((sv.mandAmt).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.mandAmt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='mandAmt' type='text'
						<%if ((sv.mandAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.mandAmt.getLength(), sv.mandAmt.getScale(), 3)%>'
						maxLength='<%=sv.mandAmt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(mandAmt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.mandAmt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.mandAmt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.mandAmt).getColor() == null ? "input_cell"
							: (sv.mandAmt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

					<%--ILIFE-1536 START by dpuhawan --%>
					<%
						}
					%>
					<%--ILIFE-1536 END --%>

				</div></div>
			</div>
		</div>
		<%--row 6 --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Currency")%></label>
					<table><tr><td>

						<%
							if (!((sv.currcode.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcode.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcode.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td>


						<%
							if(!((sv.curdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.curdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.curdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:300px;min-width: 100px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td></tr></table>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

