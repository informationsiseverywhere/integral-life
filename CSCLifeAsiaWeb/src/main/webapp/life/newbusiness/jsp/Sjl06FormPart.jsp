<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sjl06FormPart";%>

<%@page import="com.csc.life.newbusiness.screens.Sjl06ScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.quipoz.framework.util.AppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>



<%-- Start preparing data --%>
<%
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;
String formatValue = null;
QPScreenField qpsf = null;
String valueThis = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();

LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

LifeAsiaAppVars appVars = av;

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), request.getLocale());

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%Sjl06ScreenVars sv = (Sjl06ScreenVars) fw.getVariables();%>

<%!
public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}

public String formatValue(String aValue) {
	return aValue;
}
%>

		<div class="row">
			<div class="col-md-3" >
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
					
				</div>
			</div>
		</div>
			
			<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
			
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben11)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben11)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben11')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>

		
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben12)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben12)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben12')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben13)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben13)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben13')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben14)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben14)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben14')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben15)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben15)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben15')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben16)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben16)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben16')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben17)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben17)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben17')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben18)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben18)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben18')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
		
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben19)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben19)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben19')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben20)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben20)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben20')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben21).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben21)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben21)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben21')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
		
		
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben22).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben22)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben22)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben22')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben23).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben23)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben23)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben23')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.hosben24).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.hosben24)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.hosben24)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('hosben24')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				</div>
	
		
<br> 

	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("---------------------------------------------------------------  For Sum Insured  ----------------------------------------------------------------------------------------")%></label>
					
				</div>
			</div>
	</div> 
		<div class="row">
			<div class="col-md-3">
			<div class="form-group">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Minimum Sum Insured")%> </label>
				<div class="input-group" style="min-width:120px;">

							<%	
							qpsf = fw.getFieldXMLDef((sv.minsumin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.minsumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
				
				<input name='minsumin' 
				type='text'
				<%if((sv.minsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
					value='<%=valueThis %>'
							 <%
					 valueThis=valueThis;
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis %>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.minsumin.getLength(), sv.minsumin.getScale(),3)%>'
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.minsumin.getLength(), sv.minsumin.getScale(),3)-4%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(minsumin)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.minsumin).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.minsumin).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.minsumin).getColor()== null  ? 
							"input_cell" :  (sv.minsumin).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' 
				 
				<%
					} 
				%>
				>
				<%
				longValue = null;
				formatValue = null;
				%>
				</div>
				</div>
			</div>
			
			<div class="col-md-1"></div>
				<div class="col-md-5">
			<div class="form-group">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Minimum Sum Assured for Juvenile")%></label>
				<div class="input-group" style="min-width:120px;">

							<%	
							qpsf = fw.getFieldXMLDef((sv.minsum).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.minsum,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
				
				<input name='minsum' 
				type='text'
				<%if((sv.minsum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
					value='<%=valueThis %>'
							 <%
					 valueThis=valueThis;
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis %>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.minsum.getLength(), sv.minsum.getScale(),3)%>'
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.minsum.getLength(), sv.minsum.getScale(),3)-4%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(minsum)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.minsum).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.minsum).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.minsum).getColor()== null  ? 
							"input_cell" :  (sv.minsum).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' 
				 
				<%
					} 
				%>
				>
				<%
				longValue = null;
				formatValue = null;
				%>
				</div>
				</div>
			</div>
			
			
			</div>
			
			
			
		<div class="row">
			<div class="col-md-2" style="min-width: 105px;">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
					
				</div>
			</div>
		</div>
			
			
			
				<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable06)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable06)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable06')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable07)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable07)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable07')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable08)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable08)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable08')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable09)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable09)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable09')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
			
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable10)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable10)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable10')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable11)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable11)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable11')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>

		
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable12)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable12)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable12')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable13)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable13)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable13')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable14)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable14)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable14')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable15)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable15)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable15')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable16).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable16)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable16)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable16')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable17).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable17)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable17)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable17')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable18).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable18)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable18)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable18')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
		
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable19).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable19)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable19)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable19')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable20).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable20)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable20)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable20')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable21).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable21)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable21)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable21')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
		
		
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable22).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable22)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable22)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable22')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable23).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable23)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable23)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable23')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable24).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable24)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable24)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable24')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable25).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable25)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable25)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable25')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable26).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable26)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable26)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable26')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable27).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable27)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable27)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable27')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable28).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable28)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable28)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable28')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable29).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable29)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable29)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable29')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable30).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable30)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable30)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable30')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable31).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable31)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable31)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable31')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
		<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable32).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable32)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable32)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable32')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			</div>
		
					<div class="row">
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable33).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable33)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable33)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable33')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable34).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable34)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable34)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable34')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable35).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable35)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable35)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable35')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			
				<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable36).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable36)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable36)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable36')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable37).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable37)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable37)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable37')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable38).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable38)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable38)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable38')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
						
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable39).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable39)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable39)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable39')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.ctable40).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 75px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.ctable40)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                               <div class="input-group" style="min-width: 95px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.ctable40)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('ctable40')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
				</div>
				</br>
		<div class="row">
			<div class="col-md-2" style="max-width: 105px !important;">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Follow-Up")%></label>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
						<% 
							if((new Byte((sv.fupcdes01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>		
								<div class="input-group" style="width: 75px;">
                            		<%=smartHF.getHTMLVarExt(fw, sv.fupcdes01)%>
                            	</div>
						<%
							}else{
						%>
								<div class="input-group" style="min-width: 95px;">
                                	<%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes01)%>
                                    <span class="input-group-btn">
                                    	<button class="btn btn-info"
                                        	style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                            type="button"
                                            onClick="doFocus(document.getElementById('fupcdes01')); doAction('PFKEY04')">
                                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    	</button>
                                    </span>
                                </div>
						<%
							}
						%>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
						<% 
							if((new Byte((sv.fupcdes02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>		
								<div class="input-group" style="width: 75px;">
                            		<%=smartHF.getHTMLVarExt(fw, sv.fupcdes02)%>
                            	</div>
						<%
							}else{
						%>
								<div class="input-group" style="min-width: 95px;">
                                	<%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes02)%>
                                    <span class="input-group-btn">
                                    	<button class="btn btn-info"
                                        	style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                            type="button"
                                            onClick="doFocus(document.getElementById('fupcdes02')); doAction('PFKEY04')">
                                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    	</button>
                                    </span>
                                </div>
						<%
							}
						%>
				</div>
			</div>
			<div class="col-md-1" style="min-width: 105px;">
				<div class="form-group">
						<% 
							if((new Byte((sv.fupcdes03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>		
								<div class="input-group" style="width: 75px;">
                            		<%=smartHF.getHTMLVarExt(fw, sv.fupcdes03)%>
                            	</div>
						<%
							}else{
						%>
								<div class="input-group" style="min-width: 95px;">
                                	<%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes03)%>
                                    <span class="input-group-btn">
                                    	<button class="btn btn-info"
                                        	style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                            type="button"
                                            onClick="doFocus(document.getElementById('fupcdes03')); doAction('PFKEY04')">
                                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    	</button>
                                    </span>
                                </div>
						<%
							}
						%>
				</div>
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Duration from Contract Date")%> </label>
					<%
						qpsf = fw.getFieldXMLDef((sv.cdateduration).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
					%>
					<input name='cdateduration' type='text' style="width:120px;"
						value='<%=smartHF.getPicFormatted(qpsf, sv.cdateduration)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.cdateduration);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.cdateduration)%>' 
						<%}%>
						size='<%=sv.cdateduration.getLength()%>'
						maxLength='<%=sv.cdateduration.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(cdateduration)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.cdateduration).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
						<%} 
						else if ((new Byte((sv.cdateduration).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell"
						<%} 
						else {%>
							class=' <%=(sv.cdateduration).getColor() == null ? "input_cell"
							: (sv.cdateduration).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>
					/>
				</div>
			</div>
			
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Juvenile Age")%></label>
					<input name='juvenileage' type='text' style="width:120px;"
						value='<%=smartHF.getPicFormatted(qpsf, sv.juvenileage)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.juvenileage);
						if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.juvenileage)%>' 
						<%}%>
						size='<%=sv.juvenileage.getLength()%>'
						maxLength='<%=sv.juvenileage.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(juvenileage)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.juvenileage).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
						<%} 
						else if ((new Byte((sv.juvenileage).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" 
						<%} 
						else {%>
							class=' <%=(sv.juvenileage).getColor() == null ? "input_cell"
							: (sv.juvenileage).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>
					/>
				</div>
			</div>
		</div>
				
				<br><br>
	
		
		<div class="row">	
			    	<div class="col-md-4">          
		<div class="form-group">  
		<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
	 <%if ((new Byte((sv.contitem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='contitem'  style="width: 100px;"
type='text'

<%if((sv.contitem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.contitem.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>    
		
		
		</div></div></div>