<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%= ((SMARTHTMLFormatter)AppVars.hf).getHTMLCBVar(19,25)%>
<%String screenName = "SR52Y";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr52yScreenVars sv = (Sr52yScreenVars) fw.getVariables();%>

<%if (sv.Sr52yscreenWritten.gt(0)) {%>
	<%Sr52yscreen.clearClassString(sv);%>
	<%StringData generatedText1 = new StringData("Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = new StringData("Table123 ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = new StringData("Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText7 = new StringData("Multiple Beneficiaries Allowed ");%>
	<%sv.multind.setClassString("");%>
	<%StringData generatedText10 = new StringData("(Y/N)");%>
	<%StringData generatedText9 = new StringData("Relationship Validation        ");%>
	<%sv.relto.setClassString("");%>
	<%StringData generatedText12 = new StringData("O - Conctact Owner");%>
	<%StringData generatedText11 = new StringData("L - Life Assured");%>
	<%StringData generatedText13 = new StringData("B - Both Owner and Life Assured");%>
	<%StringData generatedText14 = new StringData("N - No Relationship Validation");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.multind.setReverse(BaseScreenData.REVERSED);
			sv.multind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.multind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.relto.setReverse(BaseScreenData.REVERSED);
			sv.relto.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.relto.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 2, generatedText1)%>

	<%=smartHF.getHTMLSpaceVar(3, 12, fw, sv.company)%>
	<%=smartHF.getHTMLF4NSVar(3, 12, fw, sv.company)%>

	<%=smartHF.getLit(3, 16, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 24, fw, sv.tabl)%>
	<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.tabl)%>

	<%=smartHF.getLit(3, 33, generatedText3)%>

	<%=smartHF.getHTMLVar(3, 40, fw, sv.item)%>

	<%=smartHF.getHTMLVar(3, 50, fw, sv.longdesc)%>

	<%=smartHF.getLit(8, 7, generatedText7)%>

	<%=smartHF.getHTMLVar(8, 39, fw, sv.multind)%>

	<%=smartHF.getLit(8, 41, generatedText10)%>

	<%=smartHF.getLit(12, 7, generatedText9)%>

	<%=smartHF.getHTMLVar(12, 39, fw, sv.relto)%>

	<%=smartHF.getLit(12, 42, generatedText12)%>

	<%=smartHF.getLit(13, 42, generatedText11)%>

	<%=smartHF.getLit(14, 42, generatedText13)%>

	<%=smartHF.getLit(15, 42, generatedText14)%>




<%}%>

<%if (sv.Sr52yprotectWritten.gt(0)) {%>
	<%Sr52yprotect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%@ include file="/POLACommon2.jsp"%>
