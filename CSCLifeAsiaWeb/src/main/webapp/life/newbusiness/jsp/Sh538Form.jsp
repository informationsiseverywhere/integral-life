<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH538";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	Sh538ScreenVars sv = (Sh538ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Owner    ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agency ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Commence ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to Date ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Instalment Amount ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "         ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Method of Payment ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billing Frequency ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"--------------------------------------------------------------------------------");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reprint Schedule ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(N  No; O  On-line; B; Batch)");
%>

<%
	{
		if (appVars.ind04.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.prtshd.setReverse(BaseScreenData.REVERSED);
			sv.prtshd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.prtshd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: 1px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.ctypdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: 1px;max-width: 300px;min-width: 100px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.pstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.pstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: 1px;max-width: 200px;min-width: 100px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %> <!-- </div> -->
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cownnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cownnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>


							</td>
							<td>
								<%
									if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ownername.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ownername.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;max-width: 300px;min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.pstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.pstate.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: 1px;min-width: 100px;max-width: 200px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.agntnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.agntnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.agentname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.agentname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.agentname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: 1px;min-width: 100px;max-width: 300px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label>
						<%} %>
                  
					<%-- <%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> --%>
					 <!-- ILJ-49 ends -->	

					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%-- <%
						}
					%> --%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					<%
						if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 90px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Amount")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
 		formatValue = smartHF.getPicFormatted(qpsf, sv.instPrem,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

 		if (!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue(formatValue);
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}

 		if (!formatValue.trim().equalsIgnoreCase("")) {
 %>
								<div class="output_cell" id="instPrem" style="text-align: right;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	} else {
 %>

								<div class="blank_cell" id="instPrem" style="text-align: right;">&nbsp;</div> <%
 	}
 %> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<%
						if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 70px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
					<%
						if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("mop");
							longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mop.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.mop.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 200px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
					<%
						if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("billfreq");
							longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billfreq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 70px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reprint Schedule")%></label>

					<%
						if ((new Byte((sv.prtshd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

							if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
								longValue = resourceBundleHandler.gettingValueFromBundle("No");
							}
							if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("O")) {
								longValue = resourceBundleHandler.gettingValueFromBundle("On Line");
							}
							if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
								longValue = resourceBundleHandler.gettingValueFromBundle("Batch");
							}
					%>

					<%
						if ((new Byte((sv.prtshd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<select name='prtshd' style="width: 100px;" onFocus='doFocus(this)'
						onHelp='return fieldHelp(prtshd)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.prtshd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.prtshd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>

						<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
						</option>
						<option value="N"
							<%if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
							Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
						<option value="O"
							<%if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("O")) {%>
							Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("On Line")%></option>
						<option value="B"
							<%if (((sv.prtshd.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%>
							Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Batch")%></option>

					</select>
					<%
						}
							longValue = null;
						}
					%>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

