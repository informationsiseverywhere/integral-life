

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH506";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%@page import="com.csc.smart400framework.utility.Validator"%>

<%
	Sh506ScreenVars sv = (Sh506ScreenVars) fw.getVariables();
    String cflg = "";
        HttpSession session = request.getSession();
    boolean checkmeBoolean = true;
    session.setAttribute("checkmeBoolean",new Boolean(checkmeBoolean));
    if(appVars.ind50.isOn()){//error    	    	
    	cflg = (String)session.getAttribute("cflg");
    	session.setAttribute("cflg",sv.cflg.getData().toString().trim());
    }else{//ok, should initialise
        session.setAttribute("cflg",sv.cflg.getData().toString().trim());
    }

//end
%>

<%
	if (sv.Sh506screenWritten.gt(0)) {
%>
<%
	Sh506screen.clearClassString(sv);
%>

<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>

<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>

<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>

<%
	sv.mandatory01.setClassString("");
%>

<%
	sv.crtable.setClassString("");
%>

<%
	sv.mandatory02.setClassString("");
%>

<%
	sv.zzsrce.setClassString("");
%>
<%
	sv.zzsrce.appendClassString("string_fld");
	sv.zzsrce.appendClassString("input_txt");
	sv.zzsrce.appendClassString("highlight");
%>
<%
	sv.ind.setClassString("");
%>

<%
	sv.cflg.setClassString("");
%>

<%
	sv.indic.setClassString("");
%>
<%
	sv.indic.appendClassString("yesno_fld");
	sv.indic.appendClassString("input_txt");
	sv.indic.appendClassString("highlight");
%>

<%
	sv.freqcy.setClassString("");
%>

<%
	sv.cnt.setClassString("");
%>

<%
	sv.branch01.setClassString("");
%>
<%
	sv.branch02.setClassString("");
%>
<%
	sv.branch03.setClassString("");
%>
<%
	sv.branch04.setClassString("");
%>
<%
	sv.branch05.setClassString("");
%>
<%
	sv.branch06.setClassString("");
%>
<%
	sv.branch07.setClassString("");
%>
<%
	sv.branch08.setClassString("");
%>
<%
	sv.branch09.setClassString("");
%>
<%
	sv.branch10.setClassString("");
%>

<%
	sv.znfopt01.setClassString("");
%>
<%
	sv.znfopt01.appendClassString("string_fld");
	sv.znfopt01.appendClassString("input_txt");
	sv.znfopt01.appendClassString("highlight");
%>

<%
	sv.confirm.setClassString("");
%>
<%
	sv.confirm.appendClassString("string_fld");
	sv.confirm.appendClassString("input_txt");
	sv.confirm.appendClassString("highlight");
%>


<%
	sv.znfopt02.setClassString("");
%>
<%
	sv.znfopt02.appendClassString("string_fld");
	sv.znfopt02.appendClassString("input_txt");
	sv.znfopt02.appendClassString("highlight");
%>

<%
	sv.yearInforce.setClassString("");
%>
<%
	sv.yearInforce.appendClassString("num_fld");
	sv.yearInforce.appendClassString("input_txt");
	sv.yearInforce.appendClassString("highlight");
%>

<%
	sv.expdays.setClassString("");
%>
<%
	sv.expdays.appendClassString("num_fld");
	sv.expdays.appendClassString("input_txt");
	sv.expdays.appendClassString("highlight");
%>

<%
	sv.nofbbwrn.setClassString("");
%>
<%
	sv.nofbbwrn.appendClassString("num_fld");
	sv.nofbbwrn.appendClassString("input_txt");
	sv.nofbbwrn.appendClassString("highlight");
%>

<%
	sv.nofbbisf.setClassString("");
%>
<%
	sv.nofbbisf.appendClassString("num_fld");
	sv.nofbbisf.appendClassString("input_txt");
	sv.nofbbisf.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
		if (appVars.ind10.isOn()) {
	sv.mandatory01.setReverse(BaseScreenData.REVERSED);
	sv.mandatory01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
	sv.mandatory01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
	sv.crtable.setReverse(BaseScreenData.REVERSED);
	sv.crtable.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
	sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
	sv.mandatory02.setReverse(BaseScreenData.REVERSED);
	sv.mandatory02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
	sv.mandatory02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
	sv.ind.setReverse(BaseScreenData.REVERSED);
	sv.ind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
	sv.ind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
	sv.cflg.setReverse(BaseScreenData.REVERSED);
	sv.cflg.setColor(BaseScreenData.RED);		
	//bug 666 Tom Chi
	checkmeBoolean = Validator.check(cflg,sv.cflg);
	if(checkmeBoolean == false){
		session.setAttribute("checkmeBoolean",new Boolean(checkmeBoolean));
	}			
	//sv.cflg.CHECK(MANDATORY_ENTER);
	//end			
		}
		if (!appVars.ind50.isOn()) {
	sv.cflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
	sv.freqcy.setReverse(BaseScreenData.REVERSED);
	sv.freqcy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
	sv.freqcy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
	sv.cnt.setReverse(BaseScreenData.REVERSED);
	sv.cnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
	sv.cnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
	sv.branch01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
	sv.branch01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
	sv.branch01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
	sv.branch02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
	sv.branch02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
	sv.branch02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
	sv.branch03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
	sv.branch03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
	sv.branch03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
	sv.branch04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
	sv.branch04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
	sv.branch04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
	sv.branch05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind24.isOn()) {
	sv.branch05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
	sv.branch05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
	sv.branch06.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind25.isOn()) {
	sv.branch06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
	sv.branch06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
	sv.branch07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind26.isOn()) {
	sv.branch07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
	sv.branch07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
	sv.branch08.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
	sv.branch08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
	sv.branch08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
	sv.branch09.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind28.isOn()) {
	sv.branch09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
	sv.branch09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
	sv.branch10.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
	sv.branch10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
	sv.branch10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
	sv.branch10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind32.isOn()) {
	sv.fatcaFlag.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}
%>

<script type="text/javascript">

function changeFieldLength(field) {
	 var str = field.value;
	 if((str.indexOf("-")!=0))
	{
		field.maxLength=2;
	}else
		{
		field.maxLength=3;
		}
	}
	
function checkInput(field, no) {
	changeFieldLength(field);
		
	return digitsOnly(field,no);
	}
	
function factorDoBlurNumber(field, event) {
	changeFieldLength(field);
	
	return doBlurNumber(event);
	}
</script>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Company")%></label>

					<%
						if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						formatValue = formatValue((sv.company.getFormData())
										.toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						}
					%>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>

					<%
						formatValue = formatValue((sv.tabl.getFormData())
										.toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						}
					%>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="row">
						<div class="col-md-2" style="padding-right: 0px;">
							<div class="form-group">
								<div class="input-group"> -->
								<table>
								<tr>
								<td>
									<%
										if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										formatValue = formatValue((sv.item.getFormData())
														.toString());
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										}
									%>
									</td>
								<!-- </div>
							</div>
						</div>
						<div class="col-md-10" style="padding-left: 0px;">
							<div class="form-group">
								<div class="input-group"> -->
								<td>
									<%
										if ((new Byte((sv.longdesc).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										formatValue = formatValue((sv.longdesc.getFormData())
														.toString());
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-left: 1px;max-width: 150px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										}
									%>
									</td>
									</tr>
									</table>
								<!-- </div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Basic Plan")%></label>

					<%
						if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "crtable" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("crtable");
								optionValue = makeDropDownList(mappedItems,
										sv.crtable.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.crtable
										.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.crtable).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables()
												.isScreenProtected())) {
					%>
					<div class="row">
                        <div class="col-md-6">
					<div
						class='<%=((longValue == null) || ("".equals(longValue
								.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					</div>
					</div>
					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.crtable).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; ">
						<%
							}
						%>

						<select name='crtable' type='list'
							<%if ((new Byte((sv.crtable).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.crtable).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.crtable).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
							}
					%>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Default Source")%></label>
					<%
						if ((new Byte((sv.zzsrce).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>


					<input name='zzsrce' type='text' style="width:60px;"
						<%formatValue = (sv.zzsrce.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.zzsrce.getLength()%>'
						maxLength='<%=sv.zzsrce.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zzsrce)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zzsrce).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zzsrce).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zzsrce).getColor() == null ? "input_cell"
								: (sv.zzsrce).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
						<%}%>>
					<%
						}
					%>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler
						.gettingValueFromBundle("Calculation Meth")%></label>
					<%
						if ((new Byte((sv.calcmeth).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<input name='calcmeth' type='text' style="width:70px;"
						<%formatValue = (sv.calcmeth.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.calcmeth.getLength()%>'
						maxLength='<%=sv.calcmeth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(calcmeth)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.calcmeth).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.calcmeth).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.calcmeth).getColor() == null ? "input_cell"
								: (sv.calcmeth).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
						<%}%>>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Proposal Date Mandatory")%></label>

					<div class="input-group">
					<%
						if ((new Byte((sv.mandatory01).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<input type='checkbox' name='mandatory01' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(mandatory01)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.mandatory01).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.mandatory01).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.mandatory01).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('mandatory01')" /> <input type='checkbox'
						name='mandatory01' value='N'
						<%if (!(sv.mandatory01).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('mandatory01')" />
					<%
						}
					%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("U/W Decision Date Mand")%></label>
						<div class="input-group">

					<%
						if ((new Byte((sv.mandatory02).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					
					<input type='checkbox' name='mandatory02' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(mandatory02)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.mandatory02).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.mandatory02).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.mandatory02).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('mandatory02')" /> <input type='checkbox'
						name='mandatory02' value='N'
						<%if (!(sv.mandatory02).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('mandatory02')" />
					<%
						}
					%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("RCD")%><%=resourceBundleHandler.gettingValueFromBundle("Based")%><%=resourceBundleHandler.gettingValueFromBundle("on")%><%=resourceBundleHandler
						.gettingValueFromBundle("Transaction")%><%=resourceBundleHandler.gettingValueFromBundle("Date")%>
					</label>

					<div class="input-group">
					<%
						if ((new Byte((sv.ind).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>

					<input type='checkbox' name='ind' value='Y' onFocus='doFocus(this)'
						onHelp='return fieldHelp(ind)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.ind).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.ind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.ind).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('ind')" />

					<input type='checkbox' name='ind' value='N'
						<%if (!(sv.ind).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('ind')" />
					<%
						}
					%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Temp. Receipt Details Mand")%></label>
					<%
						if ((new Byte((sv.cflg).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
					%>
					<div class="input-group">
					<input type='checkbox' name='cflg' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(cflg)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.cflg).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.cflg).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.cflg).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('cflg')" />

					<input type='checkbox' name='cflg' value='N'
						<%if (!(sv.cflg).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('cflg')" />
					<%
						}
					%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Enable Component Add Anytime")%></label>
					<%
						if ((new Byte((sv.indic).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>
					<div class="input-group">
					<input type='checkbox' name='indic' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(indic)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.indic).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.indic).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.indic).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('indic')" />

					<input type='checkbox' name='indic' value='N'
						<%if (!(sv.indic).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('indic')" />
					<%
						}
					%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("FATCA")%></label>

					<%
						if ((new Byte((sv.fatcaFlag).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<div class="input-group">
					<input type='checkbox' name='fatcaFlag' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(fatcaFlag)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.fatcaFlag).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.fatcaFlag).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.fatcaFlag).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('fatcaFlag')" /> <input type='checkbox'
						name='fatcaFlag' value='N'
						<%if (!(sv.fatcaFlag).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('fatcaFlag')" />
					<%
						}
					%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Back Dated Risk Commencement Date Calculation -")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler
						.gettingValueFromBundle("Frequency")%></label>
					<%
						if ((new Byte((sv.freqcy).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%>

					<%
						longValue = sv.freqcy.getFormData();
					%>

					<%
						if ((new Byte((sv.freqcy).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables()
												.isScreenProtected())) {
					%>
					<div class="row">
                     <div class="col-md-2">
					<div
						class='<%=((longValue == null) || ("".equals(longValue
								.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							longValue = null;
										}
						%>
					</div>
					</div>
					<div class="col-md-6" style="padding-left: 0px;">
								<label><%=resourceBundleHandler
										.gettingValueFromBundle("(by month,by year,etc.)")%></label>
						</div>
					</div>
					<%
						} else {
					%>
					<div class="row">
						<div class="col-md-4"
							style="padding-right: 0px; padding-right: 0px;">
							<div class="input-group">
							<input name='freqcy' type='text' id='freqcy'
								value='<%=sv.freqcy.getFormData()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(freqcy)'
								onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.freqcy).getHighLight()))
												.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" />
						  <span class="input-group-btn">
                                    <button class="btn btn-info" style="font-size: 20px;"
                                        type="button"
                                        onClick="doFocus(document.getElementById('freqcy')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
					    </div>
						<div class="col-md-6" style="padding-left: 0px;">
								<label><%=resourceBundleHandler
										.gettingValueFromBundle("(by month,by year,etc.)")%></label>
						</div>
					<%
						} else {
					%>

					class = '
					<%=(sv.freqcy).getColor() == null ? "input_cell"
									: (sv.freqcy).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' />
							<span class="input-group-btn">
                                <button class="btn btn-info" style="font-size: 20px;"
                                    type="button"
                                    onClick="doFocus(document.getElementById('freqcy')); doAction('PFKEY04')">
                                    <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
				      </div>
					<div class="col-md-8" >
							<label><%=resourceBundleHandler
										.gettingValueFromBundle("(by month,by year,etc.)")%></label>
					</div>
				<%
					}
					longValue = null;%>
					</div>
				<%}}%>

			</div>
		</div>
	<div class="col-md-2"></div>
	<div class="col-md-2">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("Factor")%></label>
			<%
				if ((new Byte((sv.cnt).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
			%>


			<%
				qpsf = fw.getFieldXMLDef((sv.cnt).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSBEFORE_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.cnt);
			%>

			<input name='cnt' type='text' style="width:60px"
				<%if ((sv.cnt).getClass().getSimpleName()
							.equals("ZonedDecimalData")) {%>
				style="text-align: right" <%}%> value='<%=formatValue%>'
				<%valueThis = smartHF.getPicFormatted(qpsf, sv.cnt);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=smartHF.getPicFormatted(qpsf, sv.cnt)%>' <%}%> size='2'
				maxLength='<%=sv.cnt.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(otheradjst)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  checkInput(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return factorDoBlurNumber(this, event);'
				<%if ((new Byte((sv.cnt).getEnabled())).compareTo(new Byte(
							BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.cnt).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.cnt).getColor() == null ? "input_cell"
								: (sv.cnt).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
			<%
				longValue = null;
						formatValue = null;
			%>
			<%
				}
			%>

		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("Only Allowed in Branch")%></label>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
					   <%
                        if ((new Byte((sv.branch01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
					       <div class="dsfsd"><%=smartHF.getHTMLVarExt(fw, sv.branch01, 3)%></div>
					    <%} else {%>
					    <div class="input-group">
					    <%=smartHF.getHTMLVarExt(fw, sv.branch01, 3)%>
						<span class="input-group-btn">
							<button class="btn btn-info" style="margin-left: 1px"
								type="button"
								onClick="doFocus(document.getElementById('branch01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						</div>
						<%} %>
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch02, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch02, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch02')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch03, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch03, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch03')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch04, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch04, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch04')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch05, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch05, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch05')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
                 </div>
			</div>
				<div class="row">
				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch06, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch06, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch06')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>
				
				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch07, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch07, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch07')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>

				<div class="col-md-2">
				    
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch08, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch08, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch08')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>

				<div class="col-md-2">
				    
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch09, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch09, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch09')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
                       <%
                        if ((new Byte((sv.branch10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                           <%=smartHF.getHTMLVarExt(fw, sv.branch10, 3)%>
                        <%} else {%>
                        <div class="input-group">
                        <%=smartHF.getHTMLVarExt(fw, sv.branch10, 3)%>
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="margin-left: 1px"
                                type="button"
                                onClick="doFocus(document.getElementById('branch10')); doAction('PFKEY04')">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                        </span>
                        </div>
                        <%} %>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("ETI Processing")%></label>
			<%
				if ((new Byte((sv.znfopt01).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						fieldItem = appVars.loadF4FieldsLong(
								new String[] { "znfopt01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("znfopt01");
						optionValue = makeDropDownList(mappedItems,
								sv.znfopt01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.znfopt01
								.getFormData()).toString().trim());
			%>

			<%
				if ((new Byte((sv.znfopt01).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables()
										.isScreenProtected())) {
			%>
			<div
				class='<%=((longValue == null) || ("".equals(longValue
								.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 180px;">
				<%
					if (longValue != null) {
				%>

				<%=longValue%>

				<%
					}
				%>
			</div>

			<%
				longValue = null;
			%>

			<%
				} else {
			%>

			<%
				if ("red".equals((sv.znfopt01).getColor())) {
			%>
			<div
				style="border: 1px; border-style: solid; border-color: #B55050;">
				<%
					}
				%>

				<select name='znfopt01' type='list' style="width:190px"
					<%if ((new Byte((sv.znfopt01).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.znfopt01).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<%
					if ("red".equals((sv.znfopt02).getColor())) {
				%>
			</div>
			<%
				}
			%>
			<%
				}
					}
			%>
		</div>
	</div>
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("ETI SI to include Accum Dividend")%></label>
				<div class="input-group">
			<%
				if ((new Byte((sv.confirm).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
			%>

			<input type='checkbox' name='confirm' value='Y'
				onFocus='doFocus(this)' onHelp='return fieldHelp(confirm)'
				onKeyUp='return checkMaxLength(this)'
				<%if ((sv.confirm).getColor() != null) {%>
				style='background-color: #FF0000;'
				<%}
					if ((sv.confirm).toString().trim().equalsIgnoreCase("Y")) {%>
				checked
				<%}
					if ((sv.confirm).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
				disabled <%}%> class='UICheck' onclick="handleCheckBox('confirm')" />

			<input type='checkbox' name='confirm' value='N'
				<%if (!(sv.confirm).toString().trim().equalsIgnoreCase("Y")) {%>
				checked <%}%> style="visibility: hidden"
				onclick="handleCheckBox('confirm')" />
			<%
				}
			%>
			</div>
		</div>
	</div>
</div>



<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("RPU Processing")%></label>
			<%
				if ((new Byte((sv.znfopt02).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						fieldItem = appVars.loadF4FieldsLong(
								new String[] { "znfopt02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("znfopt02");
						optionValue = makeDropDownList(mappedItems,
								sv.znfopt02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.znfopt02
								.getFormData()).toString().trim());
			%>

			<%
				if ((new Byte((sv.znfopt02).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables()
										.isScreenProtected())) {
			%>
			<div
				class='<%=((longValue == null) || ("".equals(longValue
								.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 180px;">
				<%
					if (longValue != null) {
				%>

				<%=longValue%>

				<%
					}
				%>
			</div>

			<%
				longValue = null;
			%>

			<%
				} else {
			%>

			<%
				if ("red".equals((sv.znfopt02).getColor())) {
			%>
			<div
				style="border: 1px; border-style: solid; border-color: #B55050; ">
				<%
					}
				%>

				<select name='znfopt02' type='list' style="width:190px"
					<%if ((new Byte((sv.znfopt02).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.znfopt02).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<%
					if ("red".equals((sv.znfopt02).getColor())) {
				%>
			</div>
			<%
				}
			%>

			<%
				}
					}
			%>
		</div>
	</div>
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("No.of years inforce before surrender")%></label>
			<%
				if ((new Byte((sv.yearInforce).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>


			<%
				qpsf = fw.getFieldXMLDef((sv.yearInforce).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>

			<input name='yearInforce' type='text' style="width: 60px;"
				value='<%=smartHF.getPicFormatted(qpsf, sv.yearInforce)%>'
				<%valueThis = smartHF.getPicFormatted(qpsf, sv.yearInforce);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=smartHF.getPicFormatted(qpsf, sv.yearInforce)%>' <%}%>
				size='<%=sv.yearInforce.getLength()%>'
				maxLength='<%=sv.yearInforce.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(yearInforce)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return doBlurNumber(event);'
				<%if ((new Byte((sv.yearInforce).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.yearInforce).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.yearInforce).getColor() == null ? "input_cell"
								: (sv.yearInforce).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
			<%
				}
			%>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("No.of Days to NTU")%></label>
			<%
				if ((new Byte((sv.expdays).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
			%>


			<%
				qpsf = fw.getFieldXMLDef((sv.expdays).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>

			<input name='expdays' type='text' style="width:60px;"
				value='<%=smartHF.getPicFormatted(qpsf, sv.expdays)%>'
				<%valueThis = smartHF.getPicFormatted(qpsf, sv.expdays);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=smartHF.getPicFormatted(qpsf, sv.expdays)%>' <%}%>
				size='<%=sv.expdays.getLength()%>'
				maxLength='<%=sv.expdays.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(expdays)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return doBlurNumber(event);'
				<%if ((new Byte((sv.expdays).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.expdays).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.expdays).getColor() == null ? "input_cell"
								: (sv.expdays).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
			<%
				}
			%>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group" style="width:145px;">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("Prod Name")%></label>
			<%=smartHF.getRichText(0, 0, fw, sv.zdmsion,
						(sv.zdmsion.getLength() + 1), null).replace("absolute",
						"relative").replace("<pre></pre>", "")%>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("Less than No of BenBill Charges Trigger Warning Letter")%></label>
			<%
				if ((new Byte((sv.nofbbwrn).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>


			<%
				qpsf = fw.getFieldXMLDef((sv.nofbbwrn).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>

			<input name='nofbbwrn' type='text' style="width: 60px;"
				value='<%=smartHF.getPicFormatted(qpsf, sv.nofbbwrn)%>'
				<%valueThis = smartHF.getPicFormatted(qpsf, sv.nofbbwrn);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=smartHF.getPicFormatted(qpsf, sv.nofbbwrn)%>' <%}%>
				size='<%=sv.nofbbwrn.getLength()%>'
				maxLength='<%=sv.nofbbwrn.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(nofbbwrn)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return doBlurNumber(event);'
				<%if ((new Byte((sv.nofbbwrn).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.nofbbwrn).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.nofbbwrn).getColor() == null ? "input_cell"
								: (sv.nofbbwrn).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
			<%
				}
			%>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label><%=resourceBundleHandler
						.gettingValueFromBundle("Less than No of BenBill Charges Trigger Insufficient Letter")%></label>
			<%
				if ((new Byte((sv.nofbbisf).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>


			<%
				qpsf = fw.getFieldXMLDef((sv.nofbbisf).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>

			<input name='nofbbisf' type='text' style="width: 60px;"
				value='<%=smartHF.getPicFormatted(qpsf, sv.nofbbisf)%>'
				<%valueThis = smartHF.getPicFormatted(qpsf, sv.nofbbisf);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=smartHF.getPicFormatted(qpsf, sv.nofbbisf)%>' <%}%>
				size='<%=sv.nofbbisf.getLength()%>'
				maxLength='<%=sv.nofbbisf.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(nofbbisf)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return doBlurNumber(event);'
				<%if ((new Byte((sv.nofbbisf).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.nofbbisf).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.nofbbisf).getColor() == null ? "input_cell"
								: (sv.nofbbisf).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
			<%
				}
			%>
		</div>
	</div>
</div>



</div>
</div>
<%
	/* set sv.cnt to default value 0
			so that when the textbox is blank
			the value of cnt is set to 0 in DB.
		 */
		sv.cnt.set(0);
%>
<%
	}
%>
<%@ include file="/POLACommon2NEW.jsp"%>
