<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH526";
%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	Sh526ScreenVars sv = (Sh526ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sh526screenWritten.gt(0)) {
%>
<%
	Sh526screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class,
						"Customer Service Hotline ");
%>
<%
	sv.ztelno.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class,
						"Underwriting Manager ");
%>
<%
	sv.zunder01.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class, "Title ");
%>
<%
	sv.zunder02.setClassString("");
%>
<%
	StringData generatedText8 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class, "Department ");
%>
<%
	sv.zunder03.setClassString("");
%>
<%
	StringData generatedText9 = resourceBundleHandler
				.gettingValueFromBundle(StringData.class,
						"Footer Details ");
%>
<%
	sv.zfooter.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind02.isOn()) {
				sv.ztelno.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind01.isOn()) {
				sv.ztelno.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind02.isOn()) {
				sv.ztelno.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.ztelno.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.zunder01.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind01.isOn()) {
				sv.zunder01.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind03.isOn()) {
				sv.zunder01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.zunder01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.zunder02.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind01.isOn()) {
				sv.zunder02.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind04.isOn()) {
				sv.zunder02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.zunder02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.zunder03.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind01.isOn()) {
				sv.zunder03.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind05.isOn()) {
				sv.zunder03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.zunder03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.zfooter.setReverse(BaseScreenData.REVERSED);
			}
			if (appVars.ind01.isOn()) {
				sv.zfooter.setEnabled(BaseScreenData.DISABLED);
			}
			if (appVars.ind08.isOn()) {
				sv.zfooter.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.zfooter.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<%=smartHF.getLit(3, 2, generatedText1)%>

<%=smartHF.getHTMLSpaceVar(3, 12, fw, sv.company)%>
<%=smartHF.getHTMLF4NSVar(3, 12, fw, sv.company)%>

<%=smartHF.getLit(3, 16, generatedText2)%>

<%=smartHF.getHTMLSpaceVar(3, 24, fw, sv.tabl)%>
<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.tabl)%>

<%=smartHF.getLit(3, 33, generatedText3)%>

<%=smartHF.getHTMLVar(3, 40, fw, sv.item)%>

<%=smartHF.getHTMLVar(3, 50, fw, sv.longdesc)%>

<%=smartHF.getLit(7, 2, generatedText5)%>

<%=smartHF.getHTMLVar(7, 29, fw, sv.ztelno)%>

<%=smartHF.getLit(8, 2, generatedText6)%>

<%=smartHF.getHTMLVar(8, 29, fw, sv.zunder01)%>

<%=smartHF.getLit(9, 2, generatedText7)%>

<%=smartHF.getHTMLVar(9, 29, fw, sv.zunder02)%>

<%=smartHF.getLit(10, 2, generatedText8)%>

<%=smartHF.getHTMLVar(10, 29, fw, sv.zunder03)%>

<%=smartHF.getLit(12, 2, generatedText9)%>

<%=smartHF.getHTMLVar(13, 2, fw, sv.zfooter)%>




<%
	}
%>

<%
	if (sv.Sh526protectWritten.gt(0)) {
%>
<%
	Sh526protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
