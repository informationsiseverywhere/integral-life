
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6378";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6378ScreenVars sv = (S6378ScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-------------------------Current Financial Details---------------------------");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Collection");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Rate ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,")");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount Due        ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Fee      ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suspense          ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Deposit   ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Validation Errors Detected");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J-Life");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cover");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payer");%>	
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Number"))%></label>
					<table>
					<tr>
					<td>
						<div
							class='<%=(sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%
								if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td>
						<div
							class='<%=(sv.cnttype.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>' style="max-width:80px;margin-left: 1px;">
							<%
								if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<div
							class='<%=(sv.ctypedes.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'style="max-width:200px;margin-left: 1px;">
							<%
								if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Owner"))%></label>
					<table>
					<tr>
					<td>
						<div
							class='<%=(sv.cownnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
							<%
								if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						
						<div class='<%= (sv.ownername.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>'style="max-width:170px;margin-left: 1px">
					<%					
					if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}
					} else  {
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}
					}
					%>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
				</td>
				</tr>
				</table>
				</div>
			</div>
		</div>
		
		<hr>
	
			
	<table>
	
		<tr style='height:40px;'>
			<td width='175'>
			<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Current Financial Details")%></label> 
			</div>
			</td>
		</tr>
		<tr></tr>

		<tr style='height:50px;'>
			<td width='175'></td>
			<td width='175'>
				<div class="label_txt">
					<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
				</div>
			</td>
			<td width='175'>
				<div class="label_txt">
					<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Collection")%></label>
				</div>
			</td>
		</tr>
		
		
		<tr style='height:20px;'>
			<td width='175'>
				<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>
				</div>
			</td>
			<!-- ILIFE-1499 STARTS  -->
			<td width='175' >
				<div class='<%= (sv.cntcurr.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style="width: 70px" align="left">
					<%					
						if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						} else  {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			</td>
			
			<td width='175'>
				<div class='<%= (sv.billcurr.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell" %>' style="width: 70px;" align="left"> <!-- ILJ-130 -->
					<%					
						if(!((sv.billcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}	
						} else  {	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			</td>
		</tr>
	
<tr style='height:10px;'></tr>
		
		<tr style='height:20px;'>
			<td width='175'>
				<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Amount Due")%></label>
				</div>
			</td>
			
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				
				if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell" align="right"  id="instPrem"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="instPrem"></div>
			<% 
				} 
			%>
			</td>
			
			<td width='175'>		
			<%	
				qpsf = fw.getFieldXMLDef((sv.premCurr).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.premCurr,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				
				if(!((sv.premCurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell"  align="right" id="premCurr"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="premCurr"></div>
			<% 
				} 
			%>
			</td>
		</tr>
		
		<tr style='height:10px;'></tr>
		
		<tr style='height:20px;'>
			<td width='175'>
			<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Contract Fee")%></label>
				</div>
			</td>
			
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.cntfee).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf,sv.cntfee,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				
				if(!((sv.cntfee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell" align="right" id="cntfee"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="cntfee" />
			<% 
				} 
			%>
			</td>
			
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.pufee).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.pufee,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				
				if(!((sv.pufee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell" ALIGN="right" id="pufee"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="pufee"></div>
			<% 
				} 
			%>
			</td>
		</tr>
		<tr style='height:10px;'></tr>
		<!-- //////////////// -->
		<tr style='height:20px;'>
			<td width='175'>
				<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Taxes")%></label>
				</div>
			</td>
			
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.taxamt01).getFieldName());
				qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
				
				
				
				if(!((sv.taxamt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell"  ALIGN="RIGHT" id="taxamt01"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="taxamt01"></div>
			<% 
				} 
			%>
			</td>
			
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.taxamt02).getFieldName());
				qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
				
				
				
				if(!((sv.taxamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell"  align="right" id="taxamt02"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell"  id="taxamt02"></div>
			<% 
				} 
			%>
			</td>
		</tr>
		<!--  -->
		<tr style='height:10px;'></tr>
		<tr style='height:20px;'>
			<td width='175px'>
			<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
				</div>
				
			
			</td>
			
			<td width='175px'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.totlprm).getFieldName());
				qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.totlprm,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
				
				if(!((sv.totlprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue ); 
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell"  ALIGN="RIGHT" id="totlprm"  ><%= XSSFilter.escapeHtml(formatValue)%></div>
				 
			<%
				} else {
			%>
				<div class="blank_cell"  id="totlprm"></div>
			<% 
				} 
			%>
			</td>
			<td width='175px'>
			
			</td>
		</tr>
		<tr style='height:10px;'></tr>
		<tr style='height:20px;'>
			<td width='175'>
				<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Suspense")%></label>
				</div>
			</td>
			
			<td width='175'>
			
			</td>	
		
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.premsusp).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
				formatValue = smartHF.getPicFormatted(qpsf,sv.premsusp,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
				
				if(!((sv.premsusp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell" align="right" id="premsusp"><%= XSSFilter.escapeHtml(formatValue)%></div>
			<%
				} else {
			%>
				<div class="blank_cell" id="premsusp"></div>
			
			<% 
				} 
			%>
			</td>
		</tr>
		<tr style='height:10px;'></tr>
		
		<!-- <tr style='height:10px;'></tr> --><!-- ILJ-130 -->
		<tr style='height:20px;'>
			<td width='175'>
			<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Deposit")%></label>
				</div>
				
			
			</td>
			
			<td width='175'>
			
			</td>
			<td width='175'>
			<%	
				qpsf = fw.getFieldXMLDef((sv.prmdepst).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
					formatValue = smartHF.getPicFormatted(qpsf,sv.prmdepst,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				
				if(!((sv.prmdepst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
					} else {
						formatValue = formatValue( longValue );
					}
				}
		
				if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
				<div class="output_cell"  align="right" id="prmdepst"><%= XSSFilter.escapeHtml(formatValue)%></div>
				<!-- ILIFE-1499 ENDS  -->
			<%
				} else {
			%>
				<div class="blank_cell"  id="prmdepst"></div>
			<% 
				} 
			%>
			</td>
		</tr>
		
		
		
		<tr style='height:10px;'></tr>
		<tr style='height:20px;'>
			<td width='220'>
			<div class="label_txt">
				<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Validations ")%></label>
				</div>
			</td>
	</table>
		
		
		
			<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="">
				<table class="table table-striped table-bordered table-hover"
					id='S6378Table' width="100%">
					<thead>
						<tr class='info'>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Error Description")%></th>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></th>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
								<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Payer")%></th>
						</tr>
					</thead>
					
					<tbody>
										<%
											GeneralTable sfl = fw.getTable("s6378screensfl");
											S6378screensfl.set1stScreenRow(sfl, appVars, sv);
											int count = 1;
											while (S6378screensfl.hasMoreScreenRows(sfl)) {
										%>
										<!--  ILIFE 2418 starts-->
										<tr  id='<%="tablerow" + count%>'>
											<!--<td style="width:440px;" align="left">	-->													
					
											<td 
												style="width: 330px; border-right: 1px solid #dddddd;"
												align="left"><%=sv.erordsc.getFormData()%></td>

											<!--<td style="width:50px;" align="left">-->
					
											<td class="tableDataTag"
												style="width: 50px; border-right: 1px solid #dddddd;"
												align="left"><%=sv.life.getFormData()%></td>

											<!--<td style="width:75px;" align="left">-->														
					
											<td class="tableDataTag"
												style="width: 75px; border-right: 1px solid #dddddd;"
												align="left"><%=sv.jlife.getFormData()%></td>

											<!--<td style="width:75px;" align="left">-->
					
											<td class="tableDataTag"
												style="width: 75px; border-right: 1px solid #dddddd;"
												align="left"><%=sv.coverage.getFormData()%></td>

											<!--<td style="width:50px;" align="left">	-->													
					
											<td class="tableDataTag"
												style="width: 50px; border-right: 1px solid #dddddd;"
												align="left"><%=sv.rider.getFormData()%></td>

											<!--<td style="width:50px;" align="left">-->
					
											<td class="tableDataTag"
												style="width: 50px; border-right: 1px solid #dddddd;"
												align="left">
												<%
													String data = sv.payrseqno.getFormData();
														if (!"0".equals(data.trim())) {
												%> <%=data%> <%} %>


											</td>
										</tr>
										<!-- ILIFE 2418 ends  -->

										<%
											count = count + 1;
												S6378screensfl.setNextScreenRow(sfl, appVars, sv);
											}
										%>


							

									</tbody>

				</table>
			</div>
		</div>
		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Emptytablemsg")%>">
	</div>
</div>






<script>
	$(document).ready(function() {
        var showval= document.getElementById('show_lbl').value;
        var toval= document.getElementById('to_lbl').value;
        var ofval= document.getElementById('of_lbl').value;
        var entriesval= document.getElementById('entries_lbl').value;
        var nextval= document.getElementById('nxtbtn_lbl').value;
        var previousval= document.getElementById('prebtn_lbl').value;
        var dtmessage =  document.getElementById('msg_lbl').value;
		$('#S6378Table').DataTable({
			ordering : false,
			searching : false,
			scrollY : "250px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false,
            language: {
                "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
                "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
                "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
                "sEmptyTable": dtmessage,
                "paginate": {
                    "next":       nextval,
                    "previous":   previousval
                }
            }
	       
		});
		
		
		$("#prmdepst").width(150)
		$("#premsusp").width(150)
		$("#taxamt02").width(150)
		$("#taxamt01").width(150)
		$("#pufee").width(150)
		$("#cntfee").width(150)
		$("#premCurr").width(150)
		$("#instPrem").width(150)		
		$("#totlprm").width(150)
		
		
		
		
		
		
		
	});
</script> 
	

			
			
		</div></div>


<%@ include file="/POLACommon2NEW.jsp"%>


