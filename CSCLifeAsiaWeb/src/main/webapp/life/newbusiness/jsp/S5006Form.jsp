

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S5006";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	S5006ScreenVars sv = (S5006ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "-");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life assured ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Joint life ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Select?");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider");
%>
<%
	appVars.rollup(new int[]{93});
%>
<% if(1 == sv.uwFlag){ %>
<script type="text/javascript">
	var uwResultMap = new Map();
<%
	Map<String, Integer> map = sv.getUwResultMap();
	if (map != null && !map.isEmpty()) {
		for (String comType : map.keySet()) {
%>
	uwResultMap.set('<%=comType%>','<%=map.get(comType)%>');
	
<% 		} 
	} %>
	
	var uwQuestionResultMap = new Map();
	
	<%
	map = sv.getUwQuestionResultMap();
	if (map != null && !map.isEmpty()) {
		for (String comType : map.keySet()) {
%>
	uwQuestionResultMap.set('<%=comType%>','<%=map.get(comType)%>');
	
<% 		} 
	} %>
</script>

<% } %>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
					<table>
						<tr>
							<td>
								<div
									class='<%=(sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%
 	fieldItem = appVars.loadF4Fields(new String[]{"ctypedes"}, sv, "E", baseModel);
 	mappedItems = (Map) fieldItem.get("ctypedes");
 	longValue = (String) mappedItems.get((sv.ctypedes.getFormData()).toString());
 %> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>

								<div style="margin-left: 1px;"
									class='<%=(sv.ctypedes.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypedes.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypedes.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%-- <%=smartHF.getHTMLVarExt(fw, sv.chdrnum)%>
						<%=smartHF.getHTMLVarExt(fw, sv.ctypedes)%> --%>
							</td>
						</tr>
					</table>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life assured"))%></label>
					<table>
						<tr>
							<td>
								<div
									class='<%=(sv.life.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.life.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.life.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>


							</td>
							<td>





								<div style="margin-left: 1px;"
									class='<%=(sv.lifcnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.lifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.lifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%
 	fieldItem = appVars.loadF4Fields(new String[]{"linsname"}, sv, "E", baseModel);
 	mappedItems = (Map) fieldItem.get("linsname");
 	longValue = (String) mappedItems.get((sv.linsname.getFormData()).toString());
 %> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>

								<div style="margin-left: 1px;"
									class='<%=(sv.linsname.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.linsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.linsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>



				</div>
			</div>




			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint life "))%></label>

					<table>
						<tr>
							<td style="min-width: 100px;">
								<div
									class='<%=(sv.jlife.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlife.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlife.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>



							</td>
							<td style="min-width: 100px;">




								<div style="margin-left: 1px;"
									class='<%=(sv.jlifcnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%
 	fieldItem = appVars.loadF4Fields(new String[]{"jlinsname"}, sv, "E", baseModel);
 	mappedItems = (Map) fieldItem.get("jlinsname");
 	longValue = (String) mappedItems.get((sv.jlinsname.getFormData()).toString());
 %> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="min-width: 100px;">

								<div style="margin-left: 2px;"
									class='<%=(sv.jlinsname.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlinsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.jlinsname.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
						</tr>
					</table>


				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s5006' width="100%">
							<thead>
								<tr class='info'>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center>
									</th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center>
									</th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center>
									</th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center>
									</th>
								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s5006screensfl");
									S5006screensfl.set1stScreenRow(sfl, appVars, sv);
								%>
								<%
									int count = 1;
									while (S5006screensfl.hasMoreScreenRows(sfl)) {
								%>
								<%
									if (appVars.ind01.isOn()) {
											sv.select.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind02.isOn()) {
											sv.select.setReverse(BaseScreenData.REVERSED);
											sv.select.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind02.isOn()) {
											sv.select.setHighLight(BaseScreenData.BOLD);
										}
								%>

								<tr>
									<td align='center'><input type='checkbox'
										maxLength='<%=sv.select.getLength()%>'
										<%if (sv.select.getFormData().equalsIgnoreCase("X")) {%>
										value='X' checked <%} else {%> value='' <%}%>
										size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(s5006screensfl.select)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s5006screensfl" + "." + "select" + "_R" + count%>'
										id='<%="s5006screensfl" + "." + "select" + "_R" + count%>'
										class="UICheck"
										style="width: <%=sv.select.getLength() * 12%> px;"
										
										<% if(1 == sv.uwFlag){ %>
										onClick="checkComUWResult('<%=count%>')"
										<% } else { %>
										onClick="selectRow('<%=count%>')"
										<% } %>
										<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
										readonly="true" disabled <%}%> /></td>
									<td><%=sv.ctable.getFormData()%></td>
									<td><%=sv.rtable.getFormData()%></td>
									<td><%=sv.longdesc.getFormData()%></td>
									
									<% if(1 == sv.uwFlag){ %>
									
									<input type='hidden' name='<%="s5006screensfl" + "." + "uworflag" + "_R" + count%>'
										id='<%="s5006screensfl" + "." + "uworflag" + "_R" + count%>' />
									<input type='hidden' name='<%="s5006screensfl" + "." + "uworreason" + "_R" + count%>'
										id='<%="s5006screensfl" + "." + "uworreason" + "_R" + count%>' />	
									<% } %>
									
								</tr>
								<%
									count = count + 1;
										S5006screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type='text/javascript'>
	$(document).ready(function() {
		$("#chdrnum").removeClass("form-control");
		$("#chdrnum").addClass("input-group-addon");
		$("#ctypedes").removeClass("input-group-addon");
		$("#ctypedes").addClass("form-control");

		$("#life").removeClass("form-control");
		$("#life").addClass("input-group-addon");
		$("#linsname").removeClass("input-group-addon");
		$("#linsname").addClass("form-control");

		$("#jlifcnum").css("min-width", "150px");
		$("#jlinsname").css("min-width", "250px");
	});
</script>

<script>
	$(document).ready(function() {
		$('#dataTables-s5006').DataTable({
			ordering : false,
			searching : false,
			scrollY : "370px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,			
	        info:     false,	        
	        orderable: false
	      
		});
		
		<% if(1 == sv.uwFlag){ %> 
		/**
		$("#continuebutton").click(function(){
			var idt = "s5006screensfl.select_R";
			for(var i=1;i<100;i++){
				var selectBox = document.getElementById(idt+i);
				if(selectBox != null){
					selectBox.checked = true;
				} else{
					break;
				}
			}
		});
		$("#refreshbutton").click(function(){
			var idt = "s5006screensfl.select_R";
			for(var i=1;i<100;i++){
				var selectBox = document.getElementById(idt+i);
				if(selectBox != null){
					selectBox.checked = true;
				} else{
					break;
				}
			}
		}); **/
			<% if(1 == sv.mainCovrDisFlag) {%>
			popupWarning(1);
			<% } %>
		<% } %>
	});
</script> 

<% if(1 == sv.uwFlag){ %>
<style type="text/css">
<!--
.popup-panel{
    display: block;
	z-index: 99;
    width: 50% !important;
    height: 30%;
    position: absolute !important;
    left: 24%;
    top: 30% !important;
    margin-bottom: 0 !important;
    border-color: #bce8f1 !important;
    background-color: #fff;
    border: 1px solid transparent;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
}
.popup-heading{
    font-weight: bolder;
    text-align: center;
    padding-right: 40px;
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1 ;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
}
.popup-body{
	padding: 0 !important;
    padding-left: 15px !important;
    height: auto !important;
    line-height: 1.42857143;
    color: #333;
    clear: both;
	display: table;
	margin-top:30px;
	width:100%;
}
-->
</style>
			

<script type='text/javascript'>
	function checkComUWResult(obj){
		var idt = "s5006screensfl.select_R" + obj;
		var checkboxValue = document.getElementById(idt).checked;
        if(checkboxValue==true){
			var comType = getComType(obj);
			if(uwResultMap.has(comType) || uwQuestionResultMap.has(comType)){
				uwResult = uwResultMap.get(comType);
				uwQuestionResult = uwQuestionResultMap.get(comType);
				if(uwResult == 2 || uwResult == 1 || uwQuestionResult ==2 || uwQuestionResult == 1){
					popupWarning(obj);
				}
			}
        } else{
        	clearUWReason(obj);
        }
		selectRow(obj);
	}
	function getComType(obj){
		var idt = "input[name='s5006screensfl.select_R" + obj + "']";
		if($($(idt).parent().parent().children()[1]).text().isBlank()){
			return $($(idt).parent().parent().children()[2]).text();
		} else {
			return $($(idt).parent().parent().children()[1]).text();
		}
	}
	function saveUWReason(obj){
		var flag = $("#popup-select").val();
		if("Y" == flag){
			if($("#popup-reason").val().isBlank()){
				alert("Please input the reason.");
				$("#popup-reason").css("cssText", "border-color:#f50000 !important");
				return;
			} else{
				var idreason = "input[name='s5006screensfl.uworreason_R" + obj + "']";
				var idflag = "input[name='s5006screensfl.uworflag_R" + obj + "']";
				$(idreason).val($("#popup-reason").val());
				$(idflag).val("Y");
			}
		} else {
			var idt = "s5006screensfl.select_R" + obj;
			document.getElementById(idt).checked = false;
		}
		closeWarning();
	}
	
	function popupWarning(obj){
   		$(".container").css("opacity","0.5");
   		$(".container").css("pointer-events","none");
   		$("#navigateButton").css("opacity","0.5");
   		$("#navigateButton").css("pointer-events","none");
   		document.getElementById("popup-button").onclick = function(){saveUWReason(obj);};
   		$(".popup-panel").css("display","block");
   		disableF5();
	}
	function closeWarning(){
   		$(".container").css("opacity","1");
   		$(".container").css("pointer-events","");
   		$("#navigateButton").css("opacity","1");
   		$("#navigateButton").css("pointer-events","");
   		$("#popup-reason").val("");
   		$("#popup-select").val("N");
   		$(".popup-panel").css("display","none");
   		$("#popup-reason").css("cssText", "border-color:#ccc !important");
   		enableF5();
	}
	function clearUWReason(obj){
		var idreason = "#s5006screensfl.uworreason_R" + obj;
		$(idreason).val("");
		var idflag = "#s5006screensfl.uworflag_R" + obj;
		$(idflag).val("");
	}
	String.prototype.isBlank = function(){
	    var s = $.trim(this);
	    if(s == "undefined" || s == null || s == "" || s.length == 0){
	        return true;
	    }
	    return false;
	};
	function disableF5(){
		document.body.onkeydown = function(){ 
			if ( event.keyCode==116) 
			{ 
				event.keyCode = 0; 
				event.cancelBubble = true; 
				return false; 
			} 
		};
	}
	function enableF5(){
		document.body.onkeydown = checkAllKeys;
	}
</script>

<% } %>
<script type='text/javascript'>
	function selectRow(obj) {
		var idt = "s5006screensfl.select_R" + obj;
		var checkboxValue = document.getElementById(idt).checked;
         if(checkboxValue==true){
        	document.getElementById(idt).value = 'X';
        }else{
        	document.getElementById(idt).value = '';
        } 
		
	}
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
 <div id="popup-panel" class="popup-panel" style="display: none;width: 60% !important;height: 40%;">
   	<div class="popup-heading" style="font-weight: bolder; text-align: left;padding-right: 40px;">
       	Warning
    </div>
    	<div class="popup-body">     
			<div class="row" style="padding-left: 15px !important;width: 100%;margin-top:2px">	
				 <div>
				 	<label style="font-size: 15px !important;width: 100%margin-top:2px">
				 	The selected benefit has higher risk do you still want to accept and proceed?
				 	</label>
				 	<select id="popup-select" name="" type="list" class="" style="width:60px;float:right;margin-top:12px">
				 		<option value="N" title="No">No</option>
				 		<option value="Y" title="Yes">Yes</option>
				 	</select>
				 </div>
				 <div style="margin-top:17px">
				 	<label style="font-size: 15px !important;margin-top:17px">Reason: </label>
				 	<input name="" id="popup-reason" type="text" size="100" style="font-size: 16px !important;overflow: hidden; white-space:
				 	 nowrap; position: relative; top: 13px; left: 0px; text-align: left; min-width: 80% !important;" 
				 	 value="" maxlength="100" class="form-control"/>
				 </div>
				 <div style="float:right;margin-top:15px;">
				 	<input type="button" id="popup-button" style="width:70px" class="popup-button" value=' OK '/>
				 </div>
			</div>
		</div>
</div>


