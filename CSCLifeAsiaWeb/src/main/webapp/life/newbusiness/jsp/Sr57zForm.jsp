

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr57z";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%Sr57zScreenVars sv = (Sr57zScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"            ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ref ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Key");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
<%		appVars.rollup(new int[] {93});
%>
<div class='outerDiv'>





			
		
		
		
		
		


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

	
						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.mandref.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
			} else {		
				calculatedValue = (sv.mandref.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.effdateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
			} else {		
				calculatedValue = (sv.effdateDisp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.bankkey.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
			} else {		
				calculatedValue = (sv.bankkey.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.bankacckey.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
			} else {		
				calculatedValue = (sv.bankacckey.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.mandstat.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.mandstat.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("Sr57zscreensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
<div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" class ="tablePos" style='top:21px; width: <%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%> height: 205px; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 210 ) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		
		<DIV id="subf" style="POSITION: relative; WIDTH: <%=totalTblWidth%>px; HEIGHT: 205px;">
		<table style="width:<%=totalTblWidth%>px;" bgcolor="#dddddd" cellspacing="1px" class="tableTag" id="table">		
		
		<tr style="height: 25px;" class="tableRowHeader">
								
														<td class="tableDataHeader tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></td>									
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[1 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[2 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[3 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[4 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></td>
										
				
			
		</tr>
		
	<%
	Sr57zscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr57zscreensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    										
													
					
					 						 
						 
						 						 	<div style='display:none; visiblity:hidden;'>
						 						 <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(Sr57zscreensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="Sr57zscreensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="Sr57zscreensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%> px;"
						  
						  >
						  
						  						 	</div>
						 						 
										
					
											
									</td>
		<%}else{%>
					<%}%>
								<%if((new Byte((sv.mandref).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" 
					<%if((sv.mandref).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
												
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="Sr57zscreensfl" + "." +
					      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.mandref.getFormData()%></span></a>							 						 		
						
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.effdateDisp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
					<%if((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.effdateDisp.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.bankkey).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
					<%if((sv.bankkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.bankkey.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.bankacckey).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
					<%if((sv.bankacckey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.bankacckey.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.mandstat).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
					<%if((sv.mandstat).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
									
											
						<%= sv.mandstat.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr57zscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
</DIV>
	
<br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>

</tr></table></div><br/></div>


<%@ include file="/POLACommon2.jsp"%>

