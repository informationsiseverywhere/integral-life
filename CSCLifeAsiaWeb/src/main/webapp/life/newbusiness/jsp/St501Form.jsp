

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "ST501";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%St501ScreenVars sv = (St501ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts with loan start date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From   ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To     ");%>

<%{
		if (appVars.ind51.isOn()) {
			sv.datefromDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefromDisp.setHasCursor(BaseScreenData.HASCURSOR);
			sv.datefromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.datefromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setHasCursor(BaseScreenData.HASCURSOR);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<!-- <script>
$(document).ready(function(){
	$("#scheduleName").css("width","130px");
	$("#scheduleNumber").css("width","50px");
	$("#acctmonth").css("width","40px");
	$("#acctyear").css("width","90px");
	$("#effdateDisp").css("width","130px");
	$("#jobq").css("width","180px");
	$("#bcompany").css("width","130px");
	$("#bbranch").css("width","130px");
	
})
</script> -->
<div class="panel panel-default">
    	<div class="panel-body">     
			<div class="row">	
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
				    <table><tr><td>	
				    		<%if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="max-width:100px" id="scheduleName">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td>
							<%	
								qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
								
								if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div  class="output_cell" style="margin-left: 1px;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" ></div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%></td></tr></table>
				    	
				    </div>
				</div>
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
				    	<table><tr><td>
				    		<%	
								qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
								
								if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div  class="output_cell" style="max-width:40px">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" > &nbsp; </div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
							
							</td>
							<td>
							
							<%	
								qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
								
								if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div  class="output_cell" style="margin-left: 1px;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" style="min-width: 100px;margin-left: 1px;"> &nbsp; </div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
				    	</td>
				    	</tr></table>
				    </div>
				</div>
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
				    	  <div class="input-group"><%					
						if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				    </div></div>
				</div>
			</div>
			<div class="row">	
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
				    	<%					
						if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				    </div>
				</div>
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("bcompany");
							longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
						%>
				    	<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
				    	<div  style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		<%=longValue%>
						   		<%}%>
						   </div>
						<%
						   longValue = null;
						   formatValue = null;
						%>
				    </div>
				</div>
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
				    	  <div class="input-group"><%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("bbranch");
							longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
						%>
						<div  style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		<%=longValue%>
						   		<%}%>
						 </div>
						<%
						   longValue = null;
						   formatValue = null;
						%>
				    </div></div>
				</div>
			</div>
			<div class="row">	
				<div class="col-md-7"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Extract All Contracts with Loan Start Date From")%></label>
				    	<table>
				    		<tr>
				    			<td>
				    				<%	
									longValue = sv.datefromDisp.getFormData();  
									%>
									
									<% 
										if((new Byte((sv.datefromDisp).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
									%>  
										 <%=smartHF.getRichTextDateInput(fw, sv.datefromDisp,(sv.datefromDisp.getLength()))%>
									<% }else {%> 
										<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datefromDisp" data-link-format="dd/mm/yyyy">
						                    <%=smartHF.getRichTextDateInput(fw, sv.datefromDisp,(sv.datefromDisp.getLength()))%>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
											
									<% }%>
				    			</td>
				    			<td>&nbsp<%="To"%>&nbsp</td>
				    			<td>
				    				<%	
									longValue = sv.datetoDisp.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.datetoDisp).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
									 <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
								<% }else {%> 
									<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy">
					                    <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
										<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					                </div>
										
								<% }%>
				    			</td>
				    		</tr>
				    	</table>
				    </div>
				</div>
			</div>
		</div>
</div>




<!-- ILIFE-2422 Coding and Unit testing - Life Cross Browser - Sprint 1 D3: Task 5 starts-->
<style>

	/* for IE 8 */

       @media \0screen\,screen\9
        {
      .output_cell{margin-left:1px}
      
          }
          
      
</style> 


<%@ include file="/POLACommon2NEW.jsp"%>

