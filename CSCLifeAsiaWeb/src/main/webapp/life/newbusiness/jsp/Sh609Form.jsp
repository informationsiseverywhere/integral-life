

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH609";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh609ScreenVars sv = (Sh609ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Staff Discount Basis ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Base premium rate / Instalment premium  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(B/I)  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Conditional Letter Follow-up Code  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Service Tax                ");%>


<div class="panel panel-default">

<div class="panel-body">
    	
    	  <div class="row">  	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
    	       <div class="input-group" style="max-width:30px">
               <input class="form-control" type="text" placeholder=<%=sv.company.getFormData().toString()%> disabled>
              </div>
            </div></div>
          
            <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
             <div class="input-group" style="max-width:60px">  <input class="form-control" type="text" placeholder=<%=sv.tabl.getFormData().toString()%> disabled>
              </div></div>
            </div>

	         <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
    	      	 <table><tr><td style="max-width:50px">
    	          <%=smartHF.getHTMLVarReadOnly(fw, sv.item)%></td><td>
    	          <%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc) %>  </td></tr></table> 	       
                </div>
    	      </div>
    	     </div> 	     
    	    
    	     
    	     <div class="row">  	  
    	     <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Base premium rate / Instalment premium")%></label>
 <div class="input-group" style="max-width:150px">
<% 
if((new Byte((sv.indic).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%> 
<div class='<%= (((sv.indic.getFormData()).toString().trim() == null)||("".equals((sv.indic.getFormData()).toString().trim()))) ? 
"blank_cell" : "output_cell" %>'> 
<%if((sv.indic.getFormData()).toString().trim() != null){
if((sv.indic.getFormData()).toString().trim().equalsIgnoreCase("B"))
{ %>
<%=resourceBundleHandler.gettingValueFromBundle("Base Premium Rate")%>
<%} if((sv.indic.getFormData()).toString().trim().equalsIgnoreCase("I")){ %>
<%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%>

<%} %>
<%} %>
</div>

<%
longValue = null;
%>
<% }else { %>

<select value='<%=sv.indic.getFormData()%>'
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(indic)'
	onKeyUp='return checkMaxLength(this)' name='indic'
	class="input_cell">
<option value=""><%=resourceBundleHandler.gettingValueFromBundle("...")%></option>
<option value="B"<% if(sv.indic.getFormData().equalsIgnoreCase("B")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Base Premium Rate")%></option>
<option value="I"<% if(sv.indic.getFormData().equalsIgnoreCase("I")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%></option>
		
					<%
						if ((new Byte((sv.indic).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0) {
					%>
					readonly="true" class="output_cell"
					<%
						} else if ((new Byte((sv.indic).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell"
		
					<%
						} else {
					%>
		
					class = '
					<%=(sv.indic).getColor() == null ? "input_cell"
										: (sv.indic).getColor().equals("red") ? "input_cell red reverse"
												: "input_cell"%>'
		
					<%
						}
					%>
				</select>

<%} %>
               </div></div>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></label>
	 <div class="input-group">
	 <%	
			qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='prcnt' 
type='text'

<%if((sv.prcnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>


	value='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.prcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
	 <%}%>

size='<%= sv.prcnt.getLength()%>'
maxLength='<%= sv.prcnt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(prcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.prcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.prcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.prcnt).getColor()== null  ? 
			"input_cell" :  (sv.prcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
         </div></div>
      </div>
    </div>
 
    <div class="row">  	  
    	     <div class="col-md-4">
    	      <div class="form-group">
    	          <label><%=resourceBundleHandler.gettingValueFromBundle("Conditional Letter Follow-up Code")%></label>
    	                    <div class="form-group">
    	                      <div class="input-group" style="max-width:100px">
    	                      
<input name='fupcdes' id='fupcdes'
type='text' 
value='<%=sv.fupcdes.getFormData()%>' 
maxLength='<%=sv.fupcdes.getLength()%>' 
size='<%=sv.fupcdes.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(fupcdes)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.fupcdes).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	 >
                      
                      
<%
	}else if((new Byte((sv.fupcdes).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
                      
                      

    	                    

 <span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('fupcdes')); doAction('PFKEY04')">
			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	    </button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.fupcdes).getColor()== null  ? 
"input_cell" :  (sv.fupcdes).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



 <span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('fupcdes')); doAction('PFKEY04')">
			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	    </button>
</span>


<%} %>
   </div>
</div>
</div>
</div>

   <div class="col-md-4"></div>

         <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Default Service Tax")%></label>
                   <div class="input-group">
                    <input name='taxind' type='text'
      <%formatValue = (sv.taxind.getFormData()).toString();%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.taxind.getLength()%>' maxLength='<%= sv.taxind.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(taxind)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.taxind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxind).getColor()== null  ? 
			"input_cell" :  (sv.taxind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
</div>
</div>

</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
