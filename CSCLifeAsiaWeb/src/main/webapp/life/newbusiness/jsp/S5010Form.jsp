<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5010";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	S5010ScreenVars sv = (S5010ScreenVars) fw.getVariables();
%>
<%

	{
	
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										} else {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.chdrnum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.cnttype.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										} else {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.cnttype.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							 	longValue = null;
							 		formatValue = null;
							 %> <%
							 	}
							 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypedes.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										} else {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.ctypedes.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 300px;min-width: 100px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	longValue = null;
								 		formatValue = null;
								 %> <%
								 	}
								 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 78px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>
						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 250px;min-width: 100px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					

						<table>
							<tr>
								<td>
									<%
										if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.lifcnum.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}
											} else {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.lifcnum.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}
											}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
										style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div> <%
								 	longValue = null;
								 		formatValue = null;
								 %> <%
								 	}
								 %>
								</td>
								<td>
									<%
										if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.linsname.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}
											} else {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.linsname.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}
											}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
										style="margin-left: 1px;max-width: 300px;min-width: 100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div> <%
									 	longValue = null;
									 		formatValue = null;
									 %> <%
									 	}
									 %>
								</td>
							</tr>
						</table>
					
				</div>
			</div>
		</div>

		<br>

		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 74;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 154;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 340;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 82;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 84;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = 72;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = 48;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			calculatedValue = 84;
			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[8] = calculatedValue;

			calculatedValue = 168;/*ILIFE-3581*/
			totalTblWidth += calculatedValue;
			tblColumnWidth[9] = calculatedValue;
			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s5010screensfl");
			GeneralTable sfl1 = fw.getTable("s5010screensfl");
			S5010screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = sfl.count() * 27;
			} else {
				height = 118;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s5010' width='100%'>
								<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary Type")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary")%></th>
							<%if (sv.actionflag.compareTo("N") != 0) {%>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Sequence")%></th>
									<%} %>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Relationship")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rev Flg")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rel to")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Code")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Share%")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Effective From")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("End Date")%></th>
								</tr>
							</thead>
							<tbody>
							<%
									S5010screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S5010screensfl.hasMoreScreenRows(sfl)) {
										appVars.rollup(new int[]{93});
										
										if (appVars.ind43.isOn()) {
											sv.bnytype.setReverse(BaseScreenData.REVERSED);
											sv.bnytype.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind43.isOn()) {
											sv.bnytype.setHighLight(BaseScreenData.BOLD);
											
										}
									 	 if (appVars.ind40.isOn()) {
											sv.bnysel.setReverse(BaseScreenData.REVERSED);
											sv.bnysel.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind40.isOn()) {
											sv.bnysel.setHighLight(BaseScreenData.BOLD);
											
										} 
										if (appVars.ind37.isOn()) {
											
											sv.cltreln.setReverse(BaseScreenData.REVERSED);
											sv.cltreln.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind37.isOn()) {
											sv.cltreln.setHighLight(BaseScreenData.BOLD);
											
										}
										if (appVars.ind38.isOn()) {
											sv.bnycd.setReverse(BaseScreenData.REVERSED);
											sv.bnycd.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind38.isOn()) {
											sv.bnycd.setHighLight(BaseScreenData.BOLD);
											
										}
										if (appVars.ind42.isOn()) {
											sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
											sv.effdateDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind42.isOn()) {
											sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
											
										}
										if (appVars.ind39.isOn()) {
											sv.bnypc.setReverse(BaseScreenData.REVERSED);
											sv.bnypc.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind39.isOn()) {
											sv.bnypc.setHighLight(BaseScreenData.BOLD);
											
										}
										hyperLinkFlag = true;
										if (appVars.ind50.isOn()) {
											sv.sequence.setReverse(BaseScreenData.REVERSED);
											sv.sequence.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind50.isOn()) {
											sv.sequence.setHighLight(BaseScreenData.BOLD);
										}
										
										
								%>
								<tr id='tr<%=count%>'>

									<td style="min-width: 180px;">
									<div class="form-group">
										<%
											if ((new Byte((sv.bnytype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.bnytype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.bnytype.getFormData();
										%>
										<div id="s5010screensfl.bnytype_R<%=count%>"
											name="s5010screensfl.bnytype_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>
											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>

										<div class="input-group" style="width: 150px;">
											<input
												name='<%="s5010screensfl" + "." + "bnytype" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "bnytype" + "_R" + count%>'
												type='text' value='<%=sv.bnytype.getFormData()%>'
												class=" <%=(sv.bnytype).getColor() == null
								? "input_cell"
								: (sv.bnytype).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.bnytype.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.bnytype)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.bnytype.getLength() * 12%> px;">

											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
													;
                                 type="button"
													onClick="doFocus(document.getElementById('<%="s5010screensfl" + "." + "bnytype" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div> <%
 	}
 %> <%
 	}
 %></div>

									</td>









									<td style="min-width: 180px;">
									<div class="form-group">
									
										<%
											if ((new Byte((sv.bnysel).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.bnysel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.bnysel.getFormData();
										%>
										<div id="s5010screensfl.bnysel_R<%=count%>"
											name="s5010screensfl.bnysel_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
										
										<div class="input-group" style="width: 150px;">
											<input
												name='<%="s5010screensfl" + "." + "bnysel" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "bnysel" + "_R" + count%>'
												type='text' value='<%=sv.bnysel.getFormData()%>'
												class=" <%=(sv.bnysel).getColor() == null
								? "input_cell"
								: (sv.bnysel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.bnysel.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.bnysel)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.bnysel.getLength() * 12%> px;">


											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
													;
                                 type="button"
													onClick="doFocus(document.getElementById('<%="s5010screensfl" + "." + "bnysel" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>


										</div>  <%
 	}
 %> <%
 	}
 %></div>

									</td>

								<!-- FWAMG3 -->
		<%if (sv.actionflag.compareTo("N") != 0) {%>
									<td style="width:80px;">
									<div class="form-group">
									
										<%if((new Byte((sv.sequence).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 
												|| (((ScreenModel) fw).getVariables().isScreenProtected())){
											longValue = sv.sequence.getFormData();
										%>
										<div class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=longValue==null?"":longValue%>
										</div>
										<%longValue=null;%>
										<% } else {%>
										<div class="input-group" style="width: 80px;">
											<input name = '<%="s5010screensfl" + "." +"sequence" + "_R" + count %>' 
											id ='<%="s5010screensfl" + "." +"sequence" + "_R" + count %>'
											type='text' 
											class = "<%=(sv.sequence).getColor()== null  ? "input_cell" :  (sv.sequence).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>" 
											value='<%=sv.sequence.getFormData().trim() %>' 
											onInput="this.value=this.value.replace(/\D/g,'')" 
											onFocus='doFocus(this)' 
											onblur='checkLength(this)'
											onHelp='return fieldHelp(s5010screensfl.sequence)'
											onKeyUp='return checkMaxLength(this)'
											maxlength="<%=sv.sequence.length() %>" 
											style="width: <%=sv.sequence.getLength() * 12%> px;">
										</div>
										<% } %>
									</div>
									</td>
									<%} %>
									<!-- FWAMG3 END-->

									<td style="min-width: 250px;">
										<%
											if ((new Byte((sv.clntsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0
														|| (new Byte((sv.clntsname).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
													hyperLinkFlag = false;
												}
										%>
										<%
											if ((new Byte((sv.clntsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>

										<%
											if (hyperLinkFlag) {
										%> <a href="javascript:;" class='tableLink'
										onClick='document.getElementById("<%="s5010screensfl" + "." + "slt" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.clntsname.getFormData()%></span></a>
										<%
											} else {
										%> <a href="javascript:;" class='tableLink'><span><%=sv.clntsname.getFormData()%></span></a>
										<%
											}
										%> <%
 	}
 %>
									</td>



									<td style="min-width: 180px;">
									<div class="form-group">
										<%
											if ((new Byte((sv.cltreln).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.cltreln).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.cltreln.getFormData();
										%>
										<div id="s5010screensfl.cltreln_R<%=count%>"
											name="s5010screensfl.cltreln_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>


										<div class="input-group" style="width: 150px;">
											<input
												name='<%="s5010screensfl" + "." + "cltreln" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "cltreln" + "_R" + count%>'
												type='text' value='<%=sv.cltreln.getFormData()%>'
												class=" <%=(sv.cltreln).getColor() == null
								? "input_cell"
								: (sv.cltreln).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.cltreln.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.cltreln)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.cltreln.getLength() * 12%> px;">


											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
													;
                                 type="button"
													onClick="doFocus(document.getElementById('<%="s5010screensfl" + "." + "cltreln" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div> <%
 	}
 %> <%
 	}
 %></div>
									</td>



									<td style="min-width: 100px;">
										<%
											if ((new Byte((sv.revcflg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											longValue = sv.revcflg.getFormData();
										%>
										<div id="s5010screensfl.revcflg_R<%=count%>"
											name="s5010screensfl.revcflg_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>



									<td style="min-width: 100px;">
										<%
											if ((new Byte((sv.relto).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											longValue = sv.relto.getFormData();
										%>
										<div id="s5010screensfl.relto_R<%=count%>"
											name="s5010screensfl.relto_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>



									<td style="min-width: 180px;">
									<div class="form-group">
										<%
											if ((new Byte((sv.bnycd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.bnycd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.bnycd.getFormData();
										%>
										<div id="s5010screensfl.bnycd_R<%=count%>"
											name="s5010screensfl.bnycd_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
										<div class="input-group" style="width: 150px;">
											<input
												name='<%="s5010screensfl" + "." + "bnycd" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "bnycd" + "_R" + count%>'
												type='text' value='<%=sv.bnycd.getFormData()%>'
												class=" <%=(sv.bnycd).getColor() == null
								? "input_cell"
								: (sv.bnycd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.bnycd.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.bnycd)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.bnycd.getLength() * 12%> px;">


											<span class="input-group-btn">
												<button class="btn btn-info"
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
													;
                                 type="button"
													onClick="doFocus(document.getElementById('<%="s5010screensfl" + "." + "bnycd" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div> <%
 	}
 %> <%
 	}
 %>
 </div>
									</td>



									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.bnypc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.bnypc).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.bnypc);
										%> <%
 	if ((new Byte((sv.bnypc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="s5010screensfl.bnypc_R<%=count%>"
											name="s5010screensfl.bnypc_R<%=count%>"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
											<%
												if (formatValue != null) {
											%>

											<%=XSSFilter.escapeHtml(formatValue)%>


											<%
												}
											%>
										</div> <%
 	} else {
 %> <input type='text' value='<%=formatValue%>'
										<%if (qpsf.getDecimals() > 0) {%>
										size='<%=sv.bnypc.getLength() + 1%>'
										maxLength='<%=sv.bnypc.getLength() + 1%>' <%} else {%>
										size='<%=sv.bnypc.getLength()%>'
										maxLength='<%=sv.bnypc.getLength()%>' <%}%>
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(s5010screensfl.bnypc)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s5010screensfl" + "." + "bnypc" + "_R" + count%>'
										id='<%="s5010screensfl" + "." + "bnypc" + "_R" + count%>'
										class=" <%=(sv.bnypc).getColor() == null
								? "input_cell"
								: (sv.bnypc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
										style="width: <%=sv.bnypc.getLength() * 12%> px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>
										<%
											}
										%> <%
 	}
 %>
 </div>
									</td>



									<td style="min-width: 150px;">
									<div class="form-group">
									
										<%
											if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											longValue = sv.effdateDisp.getFormData();
										%> <%
 	if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="s5010screensfl.effdateDisp_R<%=count%>"
											name="s5010screensfl.effdateDisp_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/MM/yyyy" data-link-field="cltdobxDisp"
											data-link-format="dd/mm/yyyy">
											<input
												name='<%="s5010screensfl" + "." + "effdateDisp" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "effdateDisp" + "_R" + count%>'
												type='text' value='<%=sv.effdateDisp.getFormData()%>'
												class=" <%=(sv.effdateDisp).getColor() == null
								? "input_cell"
								: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.effdateDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.effdateDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell"
												style="width: <%=sv.effdateDisp.getLength() * 12%> px;">


											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>


											<%
												}
											%>
											<%
												}
											%>
										</div>
										</div>
									</td>
									<!-- ILIFE-3581 starts-->
									<td style="min-width: 150px;">
										<%
											if ((new Byte((sv.enddateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.enddateDisp.getFormData();
 %> <%
 	if ((new Byte((sv.enddateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="s5010screensfl.enddateDisp_R<%=count%>"
											name="s5010screensfl.enddateDisp_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=longValue%>

											<%
												}
											%>
										</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/MM/yyyy" data-link-field="cltdobxDisp"
											data-link-format="dd/mm/yyyy">
											<input
												name='<%="s5010screensfl" + "." + "effdateDisp" + "_R" + count%>'
												id='<%="s5010screensfl" + "." + "enddateDisp" + "_R" + count%>'
												type='text' value='<%=sv.enddateDisp.getFormData()%>'
												class=" <%=(sv.enddateDisp).getColor() == null
								? "input_cell"
								: (sv.enddateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.enddateDisp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5010screensfl.enddateDisp)'
												onKeyUp='return checkMaxLength(this)' class="input_cell"
												style="width: <%=sv.enddateDisp.getLength() * 12%> px;">
											<span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
											<%
												}
											%>
											<%
												}
											%>
										
									</td>



								</tr>

								<%
									count = count + 1;
										S5010screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>

	<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">



	</div>
</div>


<script>
	$(document).ready(function() {
    		 var showval= document.getElementById('show_lbl').value;
    	        var toval= document.getElementById('to_lbl').value;
    	        var ofval= document.getElementById('of_lbl').value;
    	        var entriesval= document.getElementById('entries_lbl').value;
    	        var nextval= document.getElementById('nxtbtn_lbl').value;
    	        var previousval= document.getElementById('prebtn_lbl').value;
    	        var dtmessage =  document.getElementById('msg_lbl').value;
    			//$('#dataTables-s5010').DataTable({
    			//	ordering : false,
    				//searching : false,
    				//scrollY : "250px",
    				//scrollCollapse : true,
    			
    				$('#dataTables-s5010').DataTable({
    			    	ordering: false,
    			    	searching:false,
    			    	scrollX: true,
    			    	scrollY: '300',
    			        scrollCollapse: true,
    			  	
    				
    				
    	            language: {
    	                "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
    	                "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
    	                "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
    	                "sEmptyTable": dtmessage,
    	                "paginate": {
    	                    "next":       nextval,
    	                    "previous":   previousval
    	                }
    	            }
    		       
    			});
    			
    			
    			$("#prmdepst").width(150)
    			$("#premsusp").width(150)
    			$("#taxamt02").width(150)
    			$("#taxamt01").width(150)
    			$("#pufee").width(150)
    			$("#cntfee").width(150)
    			$("#premCurr").width(150)
    			$("#instPrem").width(150)		
    			$("#totlprm").width(150)
    			
    			
    			
    			
    			
    			
    			
    		});
    	</script> 


<%@ include file="/POLACommon2NEW.jsp"%>
<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script language="javascript">
function checkLength(element){
	if(element.value.length==1) //#ILIFE-9435 
	element.value =  (element.value > 0 && element.value < 10) ? '0'+element.value.toString() : element.value.toString();
}
	window.onload= function(){
		setDisabledMoreBtn();
	}
	</script>
<%
	}
%>
<!-- #ILIFE-2143 Cross Browser by fwang3-->
