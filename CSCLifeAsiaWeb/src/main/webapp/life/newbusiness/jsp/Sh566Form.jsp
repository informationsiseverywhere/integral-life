
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH566";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh566ScreenVars sv = (Sh566ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction direction (F/R) ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy count ");%>
<div class="panel panel-default">
       <div class="panel-body">
            <div class="row">
               <div class="col-md-4" >
	             <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </div>
</div>	

 <div class="col-md-4">
          <div class="form-group">
               <label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%> </label>
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </div> 
</div>	

<div class="col-md-4">
	<div class="form-group">
                 <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%> </label>
                <!--  <div class="input-group"> -->
                <table>
                <tr>
                <td>
               
                 <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
 <!--  </div> -->
 </tr>	
  </table>
 </div>	
 </div>
 </div>
<div class="row">
         <div class="col-md-4">
	            <div class="form-group">
                      <label> <%=resourceBundleHandler.gettingValueFromBundle("Transaction direction F/R")%></label>
                      <%	
						if((new Byte((sv.ztranind).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= (((sv.ztranind.getFormData()).toString().trim() == null)||("".equals((sv.ztranind.getFormData()).toString().trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if((sv.ztranind.getFormData()).toString().trim() != null){
						   			if((sv.ztranind.getFormData()).toString().trim().equalsIgnoreCase("F"))
						   			{ %>
						   				<%=resourceBundleHandler.gettingValueFromBundle("Forward")%>
						   			<%} if((sv.ztranind.getFormData()).toString().trim().equalsIgnoreCase("R")){ %>
						   				<%=resourceBundleHandler.gettingValueFromBundle("Reverse")%>
					   				
					   			<%} %>
						   		<%} %>
						   </div>
					
					<%
					longValue = null;
					%>
						<% }else { %>
	

				<select value='<%=sv.ztranind.getFormData()%>'
					onFocus='doFocus(this)'
					onHelp='return fieldHelp(ztranind)'
					onKeyUp='return checkMaxLength(this)' name='ztranind'
					onchange="changeTextBoxVal()";
					class="input_cell">
				<option value="">---------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>---------</option>
				<option value="F"<% if(sv.ztranind.getFormData().equalsIgnoreCase("F")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Forward")%></option>
				<option value="R"<% if(sv.ztranind.getFormData().equalsIgnoreCase("R")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Reverse")%></option>
						
					<%
						if ((new Byte((sv.ztranind).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0) {
					%>
					readonly="true" class="output_cell"
					<%
						} else if ((new Byte((sv.ztranind).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell"
		
					<%
						} else {
					%>
		
					class = '
					<%=(sv.ztranind).getColor() == null ? "input_cell"
										: (sv.ztranind).getColor().equals("red") ? "input_cell red reverse"
												: "input_cell"%>'
		
					<%
						}
					%>
				</select>
			<%} %>
   </div>
  </div>  
  <div class="col-md-4"></div> 
  <div class="col-md-2">
          <div class="form-group">
          <label> <%=resourceBundleHandler.gettingValueFromBundle("Policy count")%> </label>
          <%	
			qpsf = fw.getFieldXMLDef((sv.cnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
			%>

		<input name='cnt' type='text' style="width: 80px;"
		
		<%if((sv.cnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
		
			value='<%=smartHF.getPicFormatted(qpsf,sv.cnt) %>'
					 <%
			 valueThis=smartHF.getPicFormatted(qpsf,sv.cnt);
			 if(valueThis!=null&& valueThis.trim().length()>0) {%>
			 title='<%=smartHF.getPicFormatted(qpsf,sv.cnt) %>'
			 <%}%>
		
		size='<%= sv.cnt.getLength()%>'
		maxLength='<%= sv.cnt.getLength()%>' 
		onFocus='doFocus(this)' onHelp='return fieldHelp(cnt)' onKeyUp='return checkMaxLength(this)'  
		
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
			decimal='<%=qpsf.getDecimals()%>' 
			onPaste='return doPasteNumber(event);'
			onBlur='return doBlurNumber(event);'
		
		<% 
			if((new Byte((sv.cnt).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
		%>  
			readonly="true"
			class="output_cell"
		<%
			}else if((new Byte((sv.cnt).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		%>	
				class="bold_cell" 
		
		<%
			}else { 
		%>
		
			class = ' <%=(sv.cnt).getColor()== null  ? 
					"input_cell" :  (sv.cnt).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>'
		 
		<%
			} 
		%>
		>
	  </div> 
    </div> 
   </div>   
 </div>
</div>                   
<%@ include file="/POLACommon2NEW.jsp"%>

