<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5123";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%
	S5123ScreenVars sv = (S5123ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Contract no ");
%>
<%
	StringData generatedText2 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Life no ");
%>
<%
	StringData generatedText3 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Coverage no ");
%>
<%
	StringData generatedText4 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Rider no ");
%>
<%
	StringData generatedText5 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Life Assured ");
%>
<%
	StringData generatedText31 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Age Last Brt ");
%>
<%
	StringData generatedText6 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Stat. fund ");
%>
<%
	StringData generatedText7 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	StringData generatedText8 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Sub-sect ");
%>
<%
	StringData generatedText9 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Joint life ");
%>
<%
	StringData generatedText10 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Sum Assured ");
%>
<%
	StringData generatedText11 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText12 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Risk cess Age / Term ");
%>
<%
	StringData generatedText13 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Risk cess date ");
%>
<%
	StringData generatedText14 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Prem cess Age / Term ");
%>
<%
	StringData generatedText15 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Prem cess date ");
%>
<%
	StringData generatedText16 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Mortality class ");
%>
<%
	StringData generatedText17 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Lien code ");
%>
<%
	StringData generatedText18 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Bonus Appl Method ");
%>
<%
	StringData generatedText28 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Benefit Schedule ");
%>
<%
	StringData generatedText27 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Loaded Premium ");
%>
<%
	StringData generatedText19 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Special terms ");
%>
<%
	StringData generatedText20 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Total Premium ");
%>
<%
	StringData generatedText21 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Reassurance ");
%>
<%
	StringData generatedText22 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Premium Breakdown ");
%>
<%
	StringData generatedText23 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Total policies in plan ");
%>
<%
	StringData generatedText24 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Joint Life (J/L) ");
%>
<%
	StringData generatedText25 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Number available ");
%>
<%
	StringData generatedText29 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Instalment Details ");
%>
<%
	StringData generatedText26 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Number applicable ");
%>
<%
	StringData generatedText30 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Reducing Term ");
%>
<%
	StringData generatedText32 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Total Premium with Tax  ");
%>
<%
	StringData generatedText33 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Tax Detail ");
%>

<%
	StringData generatedText34 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class,
					"Linkage Number  ");
%>
<%
	StringData generatedText35 = resourceBundleHandler
			.gettingValueFromBundle(StringData.class, "Linkage Sub Reference Number");
%>

<%
	{
		appVars.rolldown(new int[] { 10 });
		appVars.rollup(new int[] { 10 });
	}
%>

<%
	{

		if (appVars.ind12.isOn()) {
	sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
	generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
	sv.sumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
	sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
	sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
	sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
	sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
	sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
	sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
	sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
	sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
	sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
	sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
	sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
	sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
	sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
	sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
	sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
	sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
	sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
	sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
	sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
	sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
	sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
	sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
	sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
	sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
	sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
	sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
	sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
	sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
	generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
	sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
	sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
	sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
	sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
	sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
	generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
	sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
	sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
	sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
	sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
	sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
	generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
	sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind37.isOn()) {
	sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
	sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
	sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
	sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
	generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
	sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
	sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
	sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
	sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
	sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
	generatedText20.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind22.isOn()) {
	sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
	sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind66.isOn()) {
	sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
	sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
	sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
	generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
	sv.ratypind.setReverse(BaseScreenData.REVERSED);
	sv.ratypind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind40.isOn()) {
	sv.ratypind.setInvisibility(BaseScreenData.INVISIBLE);
	sv.ratypind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
	sv.ratypind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
	generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
	sv.pbind.setReverse(BaseScreenData.REVERSED);
	sv.pbind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind34.isOn()) {
	sv.pbind.setEnabled(BaseScreenData.DISABLED);
	sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind33.isOn()) {
	sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
	generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
	sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
	generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
	sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
	sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
	sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
	generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
	sv.numavail.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
	generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
	sv.numapp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
	sv.numapp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind25.isOn()) {
	sv.numapp.setReverse(BaseScreenData.REVERSED);
	sv.numapp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
	sv.numapp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
	generatedText27.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.zlinstprem.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind36.isOn()) {
	generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
	sv.optsmode.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
	sv.optsmode.setInvisibility(BaseScreenData.INVISIBLE);
	sv.optsmode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind35.isOn()) {
	sv.optsmode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
	sv.optsmode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
	generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind38.isOn()) {
	sv.payflag.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
	sv.payflag.setInvisibility(BaseScreenData.INVISIBLE);
	sv.payflag.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind38.isOn()) {
	sv.payflag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
	sv.payflag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
	generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
	sv.zsredtrm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind39.isOn()) {
	sv.zsredtrm.setInvisibility(BaseScreenData.INVISIBLE);
	sv.zsredtrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind42.isOn()) {
	sv.zsredtrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
	sv.zsredtrm.setHighLight(BaseScreenData.BOLD);
		}

		if (appVars.ind44.isOn()) {
	generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
	sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind44.isOn()) {
	sv.taxind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
	sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
	sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
	sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
	generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
	sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-1223 STARTS
		if (appVars.ind46.isOn()) {
	sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		//ILIFE-1223 ENDS
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.loadper.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.rateadj.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.fltmort.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind32.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.VISIBLE);
		}
		if (appVars.ind53.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//ILIFE-3423:Start
		if (appVars.ind55.isOn()) {
	sv.prmbasis.setColor(BaseScreenData.RED);
		} else {
	sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
	sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind57.isOn()) {
	sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3423:End
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
	sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
	sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
	sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
	sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
	sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		if (appVars.ind73.isOn()) {
	sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6941 start*/
		if (!appVars.ind58.isOn()) {
	sv.lnkgno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind59.isOn()) {
	sv.lnkgsubrefno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6941 end*/
		
	 	if (appVars.ind60.isOn()) {
	sv.lnkgno.setEnabled(BaseScreenData.DISABLED);
		} 
		if (appVars.ind61.isOn()) {
	sv.lnkgsubrefno.setEnabled(BaseScreenData.DISABLED);
		}
		/* ILIFE-7118-starts */
		if (appVars.ind77.isOn()) {
	sv.tpdtype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* ILIFE-7118-ends */
		
		/* ILIFE-7734 starts*/
		if (appVars.ind78.isOn()) {
	sv.tpdtype.setColor(BaseScreenData.RED);
		} else {
	sv.tpdtype.setHighLight(BaseScreenData.BOLD);
		}
		/* ILIFE-7734 starts*/
		
		if (appVars.ind62.isOn()) {
	sv.lnkgind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind63.isOn()) {
	sv.lnkgind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind79.isOn()) {
	sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind64.isOn()) {
	sv.aepaydet.setReverse(BaseScreenData.REVERSED);
	sv.aepaydet.setColor(BaseScreenData.RED);
		}
		if (appVars.ind65.isOn()) {
	sv.aepaydet.setEnabled(BaseScreenData.DISABLED);			
		}
		if (!appVars.ind64.isOn()) {
	sv.aepaydet.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind67.isOn()) {
	sv.aepaydet.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		//ILJ-43
		if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		//end
	}
%>

<script>
$(document).ready(function(){
	$("#lifcnum").attr("class","input-group-addon");
	
	$("#linsname").attr("class","form-control");
	$("#jlifcnum").attr("class","input-group-addon");
	$("#jlinsname").attr("class","form-control");
	$("#ctypedes").css("width","400px").css("text-align","left");
	$("#jlifcnum").css("width","100px");
	$("#jlinsname").css("width","350px");
})
</script>
<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					
						
						<table><tr><td>
							<%
								if (!((sv.chdrnum.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>' style="min-width: 65px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								
							</div>
							
							<%
								longValue = null;
								formatValue = null;
							%>
</td><td>
							<%
								if (!((sv.cnttype.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
			</td><td>
							<%
								if (!((sv.ctypedes.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
			</td></tr></table>
						<!-- </div> -->
					</div>
				</div>
			</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
					</label>
						<table>
						<tr>
						<td>
				
							<div 	
							class='<%= (sv.lifcnum.getFormData()).trim().length() == 0 ? 
											"blank_cell" : "output_cell" %>' style="min-width: 65px" >
						<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
							</td>
							<td>
							<%
								if (!((sv.linsname.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;min-width: 65px; max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td>
			</tr>
			</table>
				
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.zagelit.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Last Birthday")%></label>
					
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
					<div class="input-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

						if (!((sv.anbAtCcd.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>

					<div class="output_cell" style="min-width: 70px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="min-width: 70px;" />

					<%
						}
					%>


					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%>
					</label>
						<table>
						<tr>
						<td>
							<%					
							if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" style="min-width:71px;">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="min-width:71px;"></div>
									
									<% 
										} 
									%>
									
							<%
							longValue = null;
							formatValue = null;
							%>
						</td>
						<td>

							<div 	
								class='<%= (sv.jlinsname.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>' style="min-width:65px;max-width: 200px;margin-left: 1px;">
							<%					
							if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td>
							</tr>
							</table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Life no")%>
					</label>


					<div
						class='<%=(sv.life.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase(
									"")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler
					.gettingValueFromBundle("Coverage no")%>
					</label>

					<div
						class='<%=(sv.coverage.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.coverage.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Rider no")%>
					</label>

					<div
						class='<%=(sv.rider.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase(
									"")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler
					.gettingValueFromBundle("Stat. fund")%>
					</label>


					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("statFund");
						longValue = (String) mappedItems.get((sv.statFund.getFormData())
								.toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 100px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler
					.gettingValueFromBundle("Section")%></label>


					<div
						class='<%=(sv.statSect.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.statSect.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%>
					</label>


					<div
						class='<%=(sv.statSubsect.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>'
						style="width: 80px;">
						<%
							if (!((sv.statSubsect.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
<br><hr><br>

<%-- IBPLIFE-2132 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  
	<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#sum_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured / Premium")%></label></a>
						</li>
						<li><a href="#cover_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></label></a>
						</li>
					</ul>
				</div>   
		</div>       
		
		<div class="tab-content">
			<div class="tab-pane fade in active" id="sum_tab">
	<%} %>
<%-- IBPLIFE-2132 end --%>
		<%--1st row --%>
		<div class="row">
			<div class="col-md-3">
			<div class="form-group">
				<label> <%=resourceBundleHandler
					.gettingValueFromBundle("Sum Assured")%>
				</label>
				<div class="input-group" style="min-width:120px;">

							<%	
							qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
				
				<input name='sumin' 
				type='text'
				<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
					value='<%=valueThis %>'
							 <%
					 valueThis=valueThis;
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis %>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)-4%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.sumin).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.sumin).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.sumin).getColor()== null  ? 
							"input_cell" :  (sv.sumin).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' 
				 
				<%
					} 
				%>
				>
				<%
				longValue = null;
				formatValue = null;
				%>
				</div>
				</div>
			</div>
			<div class="col-md-3"> 
				    <div class="form-group" >
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("mortcls");
						//ILIFE-808
						optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData() ,2,resourceBundleHandler );
						longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
						%>
						<%
							if((new Byte((sv.mortcls).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell' style="width: 170px;">
						   <%=longValue%>
						</div>
						<%longValue = null;%>
							<% }else {%>
						<select name='mortcls' type='list' style='max-width:200px;'
						<%
							if((new Byte((sv.mortcls).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							disabled
							class="output_cell"
						<%}else if((new Byte((sv.mortcls).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
								<%}else {%>
							class = ' <%=(sv.mortcls).getColor()== null  ?
									"input_cell" :  (sv.mortcls).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
						<%=optionValue%>
						</select>
						<%}%>
				    </div>
				 </div>
			<div class="col-md-3">
				<%
					longValue = null;
					formatValue = null;
				%>


				<%
					if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Currency")%>
				</label>
				<div class="input-group" style="min-width:80px;">
				<%
					fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("currcd");
						longValue = (String) mappedItems.get((sv.currcd.getFormData())
								.toString().trim());
				%>


				<%
					if (!((sv.currcd.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.currcd.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.currcd.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
				%>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<%
					longValue = null;
						formatValue = null;
				%>
				<%
					}
				%>

			</div></div>

			<div class="col-md-3">
				<%--ILIFE-3423:Start --%>
				<%
					if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Premium Basis")%>
				</label>
				<%
					}
				%>


				<%
					if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
						fieldItem = appVars.loadF4FieldsLong(
								new String[] { "prmbasis" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("prmbasis");
						optionValue = makeDropDownList(mappedItems,
								sv.prmbasis.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems
								.get((sv.prmbasis.getFormData()).toString().trim());
				%>

				<%
					if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables()
										.isScreenProtected())) {
				%>
				<div
					class='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "width:82px;" : "width:140px;"%>'>
					<%
						if (longValue != null) {
					%>

					<%=longValue%>

					<%
						}
					%>
				</div>

				<%
					longValue = null;
				%>

				<%
					} else {
				%>

				<%
					if ("red".equals((sv.prmbasis).getColor())) {
				%>
				<div
					style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
					<%
						}
					%>
					<select name='prmbasis' type='list' style="width: 145px;"
						<%if ((new Byte((sv.prmbasis).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.prmbasis).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>
						<%=optionValue%>
					</select>
					<%
						if ("red".equals((sv.prmbasis).getColor())) {
					%>
				</div>
				<%
					}
				%>
				<%
					}
					}
				%>
				<%
					longValue = null;
				%>
			</div>			
		</div>
		<div class="row">
			<%
			if ((new Byte((sv.lnkgind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			   <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Linked TPD")%></label>
		                      			
		                      			<input type='checkbox' name='lnkgind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(lnkgind)' onKeyUp='return checkMaxLength(this)'    
								<%
								
								if((sv.lnkgind).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.lnkgind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.lnkgind).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('lnkgind')"/>
								
								<input type='checkbox' name='lnkgind' value='N' 
								
								<% if(!(sv.lnkgind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('lnkgind')"/>
										                      			
		                      </div>
	         			</div>
				<%
				}
			%>
			</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<!--  ILJ-43 -->
					 <%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%>
					</label>
					<% } else{%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%>
					</label>
					<% } %>
					<!--  END  -->	
					
					
						<table>
						<tr>
						<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 70px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)' style="width:70px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								 <%}%>>

					</td>
					<td style="padding-left: 1px;">

							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 70px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)' style="width:80px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:80px;"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:80px;" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								 <%}%>>
					</td>
					</tr>
					</table>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<!--  ILJ-43 -->
					<%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%>
					</label>
					<% } else{%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%>
					</label>
					<% } %>
					<!--  END  -->	
					
					
					<!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
					
					
					
					
                <% if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
						<div class="form-group">
								<div class="input-group">
									<input name='riskCessDateDisp' id="riskCessDateDisp" type='text' value='<%=sv.riskCessDateDisp.getFormData()%>' maxLength='<%=sv.riskCessDateDisp.getLength()%>' 
										size='<%=sv.riskCessDateDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>
								</div>
						</div>                           
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
					

				<%-- 	<%
						if ((new Byte((sv.riskCessDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp,(sv.riskCessDateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-2" data-date=""
						data-date-format="dd/mm/yyyy"
						data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp,
						(sv.riskCessDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%> --%>
				
			</div>
			</div>
			<div class="col-md-3">
				<div class="form-group"><!--  ILIFE-7028 - Start-->
						<% if((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0){%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>				
				<%}else{%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Executive")%></label>
				<!--  ILIFE-7028 - End-->
				 <%}%>
					<div class="input-group" style="min-width:104px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv,
								"E", baseModel);
						mappedItems = (Map) fieldItem.get("liencd");
						optionValue = makeDropDownList(mappedItems,
								sv.liencd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.liencd.getFormData())
								.toString().trim());
						//longValue="RA";
					%>
					<%
						if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
						: "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
						<%
							}
						%>

						<select name='liencd' type='list' style="width: 140px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.liencd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div></div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((sv.dialdownoption).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Dial Down Option")%>
					</label>
					<%
						}
					%>
					<%
						if ((new Byte((sv.dialdownoption).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(
									new String[] { "dialdownoption" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("dialdownoption");
							optionValue = makeDropDownList(mappedItems,
									sv.dialdownoption.getFormData(), 2,
									resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.dialdownoption
									.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.dialdownoption).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables()
											.isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=longValue%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.dialdownoption).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
						<%
							}
						%>
						<select name='dialdownoption' type='list' style="width: 145px;"
							<%if ((new Byte((sv.dialdownoption).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.dialdownoption).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.dialdownoption).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
						}
					%>
					<%
						longValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Prem cess Age / Term")%></label>
					<table>
					<tr>
					<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 70px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								 <%}%>>
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
						</td>
						<td style="padding-left: 1px;">
							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 70px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								 <%}%>>
						</td>
						</tr>
						</table>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Prem cess date")%>
					</label>
					
					
					
					
					
                <% if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
						<div class="form-group">
							<div class="input-group">
								<input name='premCessDateDisp' id="premCessDateDisp" type='text' value='<%=sv.premCessDateDisp.getFormData()%>' maxLength='<%=sv.premCessDateDisp.getLength()%>' 
								size='<%=sv.premCessDateDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(premCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
								readonly="true"	class="output_cell"	/>
							</div>
						</div>  
						                              
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
					
					
				
				
			</div></div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Joint Life (J/L)")%>
					</label> <input name='select' type='text'
						<%formatValue = (sv.select.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.select.getLength()%>'
						maxLength='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(select)'
						onKeyUp='return checkMaxLength(this)' style="width: 104px;"
						<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.select).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.select).getColor() == null ? "input_cell"
							: (sv.select).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
						<%}%>>
					<%
						}
					%>
				</div>
			</div>
			<!-- ILIFE-7118-starts -->
			<div class="col-md-3">
				<%
					if ((new Byte((sv.tpdtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("TPD Type")%>
				</label>
				<%
					}
				%>

				<%
					if ((new Byte((sv.tpdtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(
										new String[] { "tpdtype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("tpdtype");
								
								TreeMap<String, String> lmap = new TreeMap<String, String>();
								Iterator entries = mappedItems.entrySet().iterator();
								while (entries.hasNext()) {
									Map.Entry entry = (Map.Entry) entries.next();
									String key = (String) entry.getKey();
									String value = (String) entry.getValue();
									if ((sv.crtabdesc.toString().trim()
											.equals("Total Permanent Disability"))
											&& !(key.substring(0, 4).equals("TPD1"))) {
										continue;
									}
									if ((sv.crtabdesc.toString().trim()
											.equals("Total Perm. Disability Super"))
											&& !(key.substring(0, 4).equals("TPS1"))) {
										continue;
									}
									lmap.put(key, value);
								}

								optionValue = makeDropDownList(lmap, sv.tpdtype.getFormData(),
										2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.tpdtype.getFormData())
										.toString().trim());
						%>

				<%
					if ((new Byte((sv.tpdtype).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables()
										.isScreenProtected())) {
				%>
				<div
					class='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue
							.trim()))) ? "width:82px;" : "width:140px;"%>'>
					<%
						if (longValue != null) {
					%>

					<%=longValue%>

					<%
						}
					%>
				</div>

				<%
					longValue = null;
				%>

				<%
					} else {
				%>

				<%
					if ("red".equals((sv.tpdtype).getColor())) {
				%>
				<div
					style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
					<%
						}
					%>
					<select name='tpdtype' type='list' style="width: 145px;"
						<%if ((new Byte((sv.tpdtype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.tpdtype).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>
						<%=optionValue%>
					</select>
					<%
						if ("red".equals((sv.tpdtype).getColor())) {
					%>
				</div>
				<%
					}
				%>
				<%
					}
					
				%>
				<%
					longValue = null;
				}
				%>
			</div>
			<!-- ILIFE-7118-ends -->
		</div>
<br><hr><br>
		<%--1st row --%>
		<div class="row">

			<!-- BRD-306 -->
			<%
				if ((new Byte((sv.adjustageamt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
							.gettingValueFromBundle("Age Adjusted Amount")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.adjustageamt,
							(sv.adjustageamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
				%>
				<%
					if (sv.adjustageamt.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.adjustageamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.adjustageamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>
			</div>
			<!-- <div class="col-md-1"></div> -->
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label  style="white-space: nowrap;"> <%=resourceBundleHandler
							.gettingValueFromBundle("Rate Adjusted Amount")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.rateadj,
							(sv.rateadj.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
				%>
				<%
					if (sv.rateadj.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.rateadj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.rateadj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>
			</div>
			<!-- <div class="col-md-2"></div> -->
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label  style="white-space: nowrap;"> <%=resourceBundleHandler
							.gettingValueFromBundle("Flat Mortality Amount")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.fltmort,
							(sv.fltmort.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
				%>
				<%
					if (sv.fltmort.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.fltmort,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.fltmort,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>
			</div>
			<!-- <div class="col-md-1"></div> -->
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
							.gettingValueFromBundle("Load Amount")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.loadper) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.loadper,
							(sv.loadper.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
				%>
				<%
					if (sv.loadper.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.loadper,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.loadper,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>			
		</div>
			<%
				}
			%>
		</div>
		<div class="row">
			<!-- BRD-306 -->
			<%
				if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label  style="white-space: nowrap;"> <%=resourceBundleHandler
							.gettingValueFromBundle("Premium Adjusted Amount")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.premadj) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.premadj,
							(sv.premadj.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
				%>
				<%
					if (sv.premadj.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.premadj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.premadj,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
				</div>
			</div>
			<%
				}
			%>
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {	%>
						<%
							if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
								<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label> 
						<%} else {%>
						
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
						<%}%>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.zlinstprem) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.zlinstprem,
						(sv.zlinstprem.getLength() + 1), null).replace(
						"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.zlinstprem) instanceof DecimalData) {
				%>
				<%
					if (sv.zlinstprem.equals(0)) {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF.getHTMLVar(0,0,fw,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>
			</div>
			<!-- BRD-306 -->
			<%
				if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible()))
								.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
							.gettingValueFromBundle("Basic Premium")%>
				</label>
				<%
					}
				%>
				<%
					if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem,
							(sv.zbinstprem.getLength() + 1), null).replace(
							"absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
				%>
				<%
					if (sv.zbinstprem.equals(0)) {
				%>
				<%=smartHF
								.getHTMLVar(
										0,
										0,
										fw,
										sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px;\' ")%>
				<%
					} else {
				%>
				<%=smartHF
								.getHTMLVar(
										0,
										0,
										fw,
										sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px;  text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
			</div>
			
			</div>
			<%
				}
			%>
			<%
				if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
						%>
						<%
							if (sv.zstpduty01.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				}
			%>
			
		</div>
		<div class="row">
			<div class="col-md-3">
				<%
					if ((new Byte((sv.instPrem).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						
				%>
				
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Total Premium")%>
				</label>
				<%
					}
				%>
				<%
					if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				
				
				<%
					qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				%>
				<input name='instPrem' type='text'
					<%if ((sv.instPrem).getClass().getSimpleName()
						.equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px;" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.instPrem)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.instPrem)%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(
						sv.instPrem.getLength(), sv.instPrem.getScale(), 3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(
						sv.instPrem.getLength(), sv.instPrem.getScale(), 3) - 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(instPrem)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.instPrem).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.instPrem).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%>
					class=' <%=(sv.instPrem).getColor() == null ? "input_cell"
							: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
					<%}%>>
				
				<%
					longValue = null;
					formatValue = null;
				%>
				<%
					}
				%>
			</div>
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText32).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Total Premium with Tax")%>
				</label>
				<%
					}
				%>
				<!-- ILIFE-961 STARTS -skumar498 -->
				<!-- ILIFE-1474 STARTS -->
				<%
					if ((new Byte((generatedText32).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<%
					qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
				%>
				<input name='taxamt' type='text'
					<%if ((sv.taxamt).getClass().getSimpleName()
						.equals("ZonedDecimalData")) {%>
					style="text-align: right; width: 145px" <%}%>
					value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(
						sv.taxamt.getLength(), sv.taxamt.getScale(), 3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(
						sv.taxamt.getLength(), sv.taxamt.getScale(), 3) - 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(taxamt)'
					onKeyUp='return checkMaxLength(this)' style="width:145px;"
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell" style="text-align:right;"
					<%} else if ((new Byte((sv.taxamt).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%>
					class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
							: (sv.taxamt).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
					<%}%>>
				<!-- ILIFE-1474 ENDS -->
				<!-- ILIFE-961 ENDS -->
				<%
					}
				%>
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
		</div>
		<!-- ILIFE-6941 - Start  -->
		<div class="row">
			
			
			<%
				if ((new Byte((sv.lnkgno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%> 
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Number")%></label>
					<div style="width: 175px;">
						<%
							if (((BaseScreenData) sv.lnkgno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgno, (sv.lnkgno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
	 			<%
				}
			%>

			<%
				if ((new Byte((sv.lnkgsubrefno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Sub Ref Number")%></label>
					<div style="width: 175px;">
						<%
							if (((BaseScreenData) sv.lnkgsubrefno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgsubrefno, (sv.lnkgsubrefno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgsubrefno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgsubrefno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
				<%
				}
			%>
			</div>
			<!--ILIFE-6941 end -->		
		
		<div class="row">
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText23).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler
						.gettingValueFromBundle("Total policies in plan")%>
				</label>
				<%
					}
				%>
				<%
					if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<%
					qpsf = fw.getFieldXMLDef((sv.polinc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
						formatValue = smartHF.getPicFormatted(qpsf, sv.polinc);

						if (!((sv.polinc.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
				%>
				<div class="output_cell" style="width: 140px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<%
					} else {
				%>

				<div class="blank_cell" style="width: 82px;" />
				<%
					}
				%>
				<%
					}
				%>
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText25).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>

				<label>
					<%=resourceBundleHandler
						.gettingValueFromBundle("Number available")%>
				</label>
				<%
					}
				%>
				<%
					if ((new Byte((sv.numavail).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<%
					qpsf = fw.getFieldXMLDef((sv.numavail).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
						formatValue = smartHF.getPicFormatted(qpsf, sv.numavail);

						if (!((sv.numavail.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
				%>
				<div class="output_cell" style="width: 140px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<%
					} else {
				%>

				<div class="blank_cell" style="width: 82px;" />
				<%
					}
				%>
				<%
					}
				%>
				<%
					longValue = null;
					formatValue = null;
				%>

			</div>
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText26).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>

				<label>
					<%=resourceBundleHandler
						.gettingValueFromBundle("Number applicable")%>
				</label>
				<%
					}
				%>
				<%
					if ((new Byte((sv.numapp).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
				<%
					qpsf = fw.getFieldXMLDef((sv.numapp).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
				%>
				<input name='numapp' type='text'
					value='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.numapp);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>' <%}%>
					size='<%=sv.numapp.getLength()%>'
					maxLength='<%=sv.numapp.getLength()%>' onFocus='doFocus(this)'
					onHelp='return fieldHelp(numapp)'
					onKeyUp='return checkMaxLength(this)' style="width: 140px;"
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.numapp).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell" style="width:140px;"
					<%} else if ((new Byte((sv.numapp).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" style="width:140px;" <%} else {%>
					class=' <%=(sv.numapp).getColor() == null ? "input_cell"
							: (sv.numapp).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
					<%}%>>
				<%
					}
				%>
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
			<div class="col-md-3"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<%
					if ((new Byte((generatedText18).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%>
				</label>
				<%
					}
				%>
				<%-- <%if ((new Byte((sv.bappmeth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth, (sv.bappmeth.getLength()))%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.bappmeth) %>
					</div> 

				<%} %> --%>
				
				
				
				
				
						<%
          if ((new Byte((sv.bappmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                      || fw.getVariables().isScreenProtected()) {
		%>                   
		
			<%=smartHF.getHTMLVarExt(fw, sv.bappmeth)%>			
                                         
        </div>
        <%
			} else {
		%>
        <div class="input-group" style="width: 150px;">
            <%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth)%>
             <span class="input-group-btn">
             <button class="btn btn-info" type="button"
                onClick="doFocus(document.getElementById('bappmeth')); doAction('PFKEY04')">
                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
            </button>
        </div>
        <%
         }
        %>                          
				
				
				
				
			</div>
			</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		<div  style='visibility:hidden;'>


<%

if(sv.optsmode
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optsmode
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%> 
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optsmode"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%>  

</a>
</div></div>	
<%} %>


<%
if (sv.optsmode
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.optsmode
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.optsmode'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='optsmode' id='optsmode' type='hidden'  value="<%=sv.optsmode
.getFormData()%>">
</div>
						
<%

if(sv.optextind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optextind
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>  
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' 
class="hyperLink">

<%-- <%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>  --%>      
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Special Terms"),-5,0)%>    <!--  ILIFE-2087   -->

</a>
</div>	</div>
<%} %>

<%
if (sv.optextind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.optextind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.optextind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind
.getFormData()%>">
</div>

<!-- //////////////////// -->
<%
if(sv.taxind.getInvisible()== BaseScreenData.INVISIBLE || 
		sv.taxind.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>
<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>  
<% } else { %>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>  
</a>
</div>	</div>
<%} %>
<%
if (sv.taxind.getFormData().equals("+")) {
%>
<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
<%}
	if (sv.taxind.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.taxind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
</div>
<!-- //// -->
						
<%

if(sv.ratypind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ratypind
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%>   
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ratypind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%>   

</a>
</div>	</div>
<%} %>


<%
if (sv.ratypind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.ratypind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.ratypind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='ratypind' id='ratypind' type='hidden'  value="<%=sv.ratypind
.getFormData()%>">
</div>
<%

if(sv.pbind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.pbind
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>   
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>   

</a>
</div>	</div>
<%} %>


<%
if (sv.pbind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.pbind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.pbind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='pbind' id='pbind' type='hidden'  value="<%=sv.pbind
.getFormData()%>">
</div>
						



	


<%

if(sv.payflag
.getInvisible()== BaseScreenData.INVISIBLE|| sv.payflag
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Instalment Details")%>    
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payflag"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Instalment Details")%>   

</a>
</div>	</div>
<%} %>

<%
if (sv.payflag
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.payflag
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.payflag'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='payflag' id='payflag' type='hidden'  value="<%=sv.payflag
.getFormData()%>">
</div>
						



	

<%

if(sv.zsredtrm
.getInvisible()== BaseScreenData.INVISIBLE|| sv.zsredtrm
.getEnabled()==BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){
%>

<%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%>  
<%
}
else{

%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zsredtrm"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%>  

</a>
</div>	</div>
<%} %>


<%
if (sv.zsredtrm
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.zsredtrm
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.zsredtrm'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='zsredtrm' id='zsredtrm' type='hidden'  value="<%=sv.zsredtrm
.getFormData()%>">
</div>					



	
</div><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
</tr>
<tr><td>
<input name='exclind' id='exclind' type='hidden'  value="<%=sv.exclind
.getFormData()%>">
<%
if (sv.exclind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right" style="margin-right: -3px;margin-top: -2px">

<%}else {
	
    if (sv.exclind.getFormData().equals("X") || sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
	        || sv.exclind.getEnabled() == BaseScreenData.DISABLED) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.exclind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('exclind'))" style="cursor:pointer;margin-right: -3px;margin-top: -2px"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right">
<%
}
%>
</td>

<td style="font-weight: bold;font-size: 12px; font-family: Arial">
	<%if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.exclind.getEnabled() != BaseScreenData.DISABLED){%>

	<div style="height: 15 px">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%></a>
  	</div>

	<%} %>
</td></tr>

<tr><td>
<input name='aepaydet' id='aepaydet' type='hidden'  value="<%=sv.exclind
.getFormData()%>">
<%
if (sv.aepaydet
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right" style="margin-right: -3px;margin-top: -2px">

<%}else {
	
    if (sv.aepaydet.getFormData().equals("X") || sv.aepaydet.getInvisible() == BaseScreenData.INVISIBLE
	        || sv.aepaydet.getEnabled() == BaseScreenData.DISABLED) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.exclind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('aepaydet'))" style="cursor:pointer;margin-right: -3px;margin-top: -2px"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right">
<%
}
%>
</td>

<td style="font-weight: bold;font-size: 12px; font-family: Arial">
	<%if (sv.aepaydet.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.aepaydet.getEnabled() != BaseScreenData.DISABLED){%>

	<div style="height: 15 px">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aepaydet"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("AntcptdEndwnt PayDetails")%></a>
  	</div>

	<%} %>
</td></tr>

</table></div><br/>

<%-- IBPLIFE-2132 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  

<div class="tab-pane fade" id="cover_tab">
	<div class="row">
				
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
			<table><tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcode, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcdedesc, true)%></td>
			</tr></table>
			</div>
		</div>
		
		<div class="col-md-1"></div>
		<div class="col-md-2">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					
					<%--
	
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dateeffDisp" data-link-format="dd/mm/yyyy"  onclick="document.getElementById('riskCessDateDisp').value='';">
			                    <%=smartHF.getRichTextDateInput(fw, sv.dateeffDisp,(sv.dateeffDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
						--%>
						<div class="form-group">
								<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.dateeffDisp.getFormData()%>' maxLength='<%=sv.dateeffDisp.getLength()%>' 
										size='<%=sv.dateeffDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>
								</div>
						</div>   			
		</div>
		
<%--		<div class="col-md-2"></div>
		<div class="col-md-2">
			<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid Flag")%></label>
					<% if((!fw.getVariables().isScreenProtected())){ %>
					<div class="input-group"  style="width:50px;">
						<input name='vflag' id='vflag'
						type='text'
						value='<%=sv.vflag.getFormData()%>'
						maxLength='<%=sv.vflag.getLength()%>'
						size='<%=sv.vflag.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(vflag)' onKeyUp='return checkMaxLength(this)'
						class='input_cell'/>
					<%} %>
					</div>
			</div>
		</div>	
				<%
					}
				%>
	--%>
	</div>
	
	<div class="row">
				
				
		<div class="col-md-8">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose ")%></label>		
			<div class="form-group">

<table><tr>
<td>

<textarea name='covrprpse' style='width:400px;height:70px;resize:none;border: 2px solid !important;'
type='text' 

<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.covrprpse.getFormData()).toString();

%>
 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>

size='<%= sv.covrprpse.getLength() %>'
maxLength='<%= sv.covrprpse.getLength() %>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.covrprpse).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
	disabled
<%
	}else if((new Byte((sv.covrprpse).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
		

<%
	}else { 
%>

	class = ' <%=(sv.covrprpse).getColor()== null  ? 
			"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%=XSSFilter.escapeHtml(formatValue)%></textarea>

</td></tr>

</table>

</div>
</div>
</div>
</div>
<%} %>
<%-- IBPLIFE-2132 end --%>


</div>
</div>
</div>		
<%@ include file="/POLACommon2NEW.jsp"%>
<script type='text/javascript'>
    $(document).ready(function() {
        if($("textarea[name='covrprpse']").is(":disabled")){
            $("textarea[name='covrprpse']").css("background-color","")
        }
    });
</script>			

	