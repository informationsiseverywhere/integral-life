
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR53A";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr53aScreenVars sv = (Sr53aScreenVars) fw.getVariables();%>
<%{
if (appVars.ind50.isOn()) {
	sv.ccmndref.setEnabled(BaseScreenData.DISABLED);
}
sv.mandstat.setEnabled(BaseScreenData.DISABLED);
}%>

<div class="panel panel-default">
 <div class="panel-body">     
	<div class="row">	
	    	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payor"))%></label>
        			<table><tr><td>


<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><td style="min-width:1px">
</td><td>

<%if ((new Byte((sv.payorname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:130px; max-width:140px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td></tr></table>


</div></div></div>



<div class="row">
<div class="col-md-4">
	<div class="form-group">
	<label>
<%=resourceBundleHandler.gettingValueFromBundle("Mandate Number")%>
</label>
<div class="input-group" style="max-width:90px">
<%=smartHF.getRichText(0, 0, fw, sv.ccmndref,(sv.ccmndref.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('ccmndref')); doAction('PFKEY04')">
   <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
</span>
</div></div></div></div>

<div class="row">
<div class="col-md-4">
	<div class="form-group">
	<label>
<%=resourceBundleHandler.gettingValueFromBundle("Factoring House")%>
</label>
<table><tr><td style="min-width:50px">

<%if ((new Byte((sv.facthous).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.facthous.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:50px; max-width:50px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="min-width:1px">
</td><td style="min-width:150px">
 
<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></tr></table></div></div></div>


<div class="row">
	<div class="col-md-3"> 
	    <div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Number")%>
</label>

<%if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>
</div>
<div class="col-md-1"></div>

<div class="col-md-3">
<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Type")%>
</label>
<%=smartHF.getHTMLVarReadOnly(fw, sv.cdlongdesc,1).replace("<pre></pre>","")%>
</div></div>

<div class="col-md-1"></div>

<div class="col-md-3">
<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Name on Credit Card")%>
</label>

<%if ((new Byte((sv.bankaccdsc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	    </div>
	 </div> 
</div>
<div class="row">
   	<div class="col-md-3">
   		<div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Mandate Status")%>
</label>
<%	
	if ((new Byte((sv.mandstat).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mandstat"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mandstat");
	optionValue = makeDropDownList( mappedItems , sv.mandstat.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mandstat.getFormData()).toString().trim());  
	}
%>
<%=smartHF.getDropDownExt(sv.mandstat, fw, longValue, "mandstat", optionValue) %>
</div></div>
<div class="col-md-1"></div>
 	<div class="col-md-2">
 		<div class="form-group">
   		<label>
<%=resourceBundleHandler.gettingValueFromBundle("Effective From").replace("<pre></pre>","")%>
</label>

<%=smartHF.getHTMLVarReadOnly(fw, sv.effdateDisp,1).replace("<pre></pre>","")%>
</div></div></div>
</div>

<INPUT type="HIDDEN" name="billcd" id="billcd" value="<%=	(sv.billcd.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="currcode" id="currcode" value="<%=	(sv.currcode.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="numsel" id="numsel" value="<%=	(sv.numsel.getFormData()).toString() %>" >

<br/>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
<!-- #ILIFE-2143 Cross Browser by fwang3-->
<%
if(browerVersion.equals(Firefox) || browerVersion.equals(IE11)||browerVersion.equals(Chrome)){ 
%>
	<style>
		table{border-spacing:1px 4.3px !important}
	</style>
<%}%>
