<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5013";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%S5013ScreenVars sv = (S5013ScreenVars) fw.getVariables();%>
<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%
{
		if (appVars.ind01.isOn()) {
			sv.opcda.setReverse(BaseScreenData.REVERSED);
			sv.opcda.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.opcda.setEnabled(BaseScreenData.DISABLED);
			sv.opcda.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
	
		sv.agerate.setReverse(BaseScreenData.REVERSED);
			sv.agerate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.agerate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.oppc.setReverse(BaseScreenData.REVERSED);
			sv.oppc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.oppc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.insprm.setReverse(BaseScreenData.REVERSED);
			sv.insprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.insprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.extCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.extCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.extCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		/* if (appVars.ind07.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		if (appVars.ind06.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.reasind.setReverse(BaseScreenData.REVERSED);
			sv.reasind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.reasind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.znadjperc.setReverse(BaseScreenData.REVERSED);
			sv.znadjperc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.znadjperc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.zmortpct.setReverse(BaseScreenData.REVERSED);
			sv.zmortpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.zmortpct.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.uwoverwrite.setReverse(BaseScreenData.REVERSED);
			sv.uwoverwrite.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.uwoverwrite.setEnabled(BaseScreenData.DISABLED);
			sv.uwoverwrite.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage/rider ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reas");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flat");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ind");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured%");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Load%");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjust ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duration");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality%");%>

<%{
		if (appVars.ind50.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}
%>

<script type="text/javascript">
function setValues(index){
		var checked = false;
		$("#"+"subfile_table").find("tr").each(function(j) {
			var parent = $(this);
			$(this).find("input[type='checkbox']").each(function(i) {
				var temp = $(this).attr("checked");
				if (temp == true){
					parent.find("input[type='hidden'][name*='select']").each(function (i){
						$(this).val("1");
					});
				}else{
					parent.find("input[type='hidden'][name*='select']").each(function (i){
						$(this).val("");
					});
				}
			});
		});
	}
</script>	
<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
    	 					 <div class="input-group">
                             <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
				
					<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
						} else  {
								
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
						}
					%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
				
				<%
				longValue = null;
				formatValue = null;
				%>
  <%}%>
  </div>
  </div>
  </div>
  
  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
  							 <div class="input-group">
                              <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
				
<%					
if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
  </div>
  </div></div>
  
  
  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
   						<div class="input-group">
  				   <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
				
<%					
if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
  </div>
  </div></div>
  
  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Rider_no")%></label>
  <div class="input-group">
<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

			
<%					
if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
  </div>
  
  </div>
  </div>
  
  
  </div>
  
  
  
  <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	   
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
                            
                             <table><tr><td style="min-width: 65px;">
<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
			
<%					
if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
		<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
	

</td>

<td >

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

			
<%					
if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;min-width:100px;max-width: 200px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
  </td>
  </tr>
 </table>
</div></div></div>
  
   <div class="row">
                     <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
 
	 
				<%if ((new Byte((sv.smoking01).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
							
				<%					
				if(!((sv.smoking01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.smoking01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 25px">
						<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
				  <%}%>




</div></div>


  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
					
					<%if ((new Byte((sv.occup01).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
								
					<%					
					if(!((sv.occup01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.occup01.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.occup01.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					  <%}%>
					
  </div></div>
  
  
  
  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%></label>
<table>
<tr>
<td>
 <%if ((new Byte((sv.pursuit01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
			
<%					
if(!((sv.pursuit01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>

	</td>
	<td>

<%if ((new Byte((sv.pursuit02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

	
<%					
if(!((sv.pursuit02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 110px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>
</td>
</tr>
</table>

</div></div>
</div>
  
<div class="row">
                     <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
                              <table>
                              <tr>
                              <td>
						<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
							
						<%					
						if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						  <%}%>
						  </td>
						  <td>
						  

					<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
					
						
					<%					
					if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 110px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					  <%}%>
					  </td>
					  </tr>
					  </table>
		
    	 </div>					
    	 </div>
    	 					
    </div>  
    
    
    <div class="row">
                     <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
                              
                               <%if ((new Byte((sv.smoking02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
			
								<%					
								if(!((sv.smoking02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width: 25px">
										<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								  <%}%>
                               
                               
                               
           
                               
                               
                            
                               </div>
                               </div>
                               
            <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
                            
             

							<%if ((new Byte((sv.occup02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
							
								
							<%					
							if(!((sv.occup02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occup02.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occup02.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							  <%}%>
              
                	</div></div>             
                               
                               
                               <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%></label>
                              <table>
                              <tr>
                              <td>                               

								<%if ((new Byte((sv.pursuit03).getInvisible())).compareTo(new Byte(
																BaseScreenData.INVISIBLE)) != 0) {%>
								
									
								<%					
								if(!((sv.pursuit03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								  <%}%>
								</td>
								<td>
	
								<%if ((new Byte((sv.pursuit04).getInvisible())).compareTo(new Byte(
																BaseScreenData.INVISIBLE)) != 0) {%>
								
									
								<%					
								if(!((sv.pursuit04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="width: 110px;margin-left: 1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								  <%}%>
                               
           						</td>
           						</tr>
           						</table>
            </div></div>                   
                               
         </div>
    
    
    
    
    <!-- One row need to be added -->
    
    <div class="row">
                     <div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/rider")%></label>

                               <table>
                               <tr>
                               <td>
					<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
					
						
					<%					
					if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

  <%}%>
  </td>
  <td>

<%if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

	
<%					
if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 250px;margin-left: 1px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
  <%}%>

		
    	</td>
    	</tr>
    	</table>
    	 </div>		</div>			
    	 					
    </div>
    
    <br>
    
    <div class="row">
				<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5013' width='100%'>
               <thead>
		
			        <tr class='info'>
			        
				     <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></p></th>			
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>																			
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
			     	  <%if (sv.uwFlag == 1 ) { %> 
			     	 	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></th>
			     	 <%} %>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></th>
			     
		 	        
		 	        </tr>
			 </thead>
			
			 <% GeneralTable sfl = fw.getTable("s5013screensfl");%>
			
							 
						 	<script language="javascript">
					        $(document).ready(function(){
					        	var rows = <%=sfl.count() + 1%>;
								var isPageDown = 1;
								var pageSize = 1;
								<% if(!(fw.getVariables().isScreenProtected())){%> /*BSIBF-119 START*/
								var headerRowCount= 1;
								<%} else {%>
								var headerRowCount= <%=sv.seqnbr%>;
								 <%}%>					 	/*BSIBF-119 END*/
								 //IFSU-1092 Start
								var fields = new Array();	//required fields
								fields[0] = "opcda";
								//IFSU-1092 End
					        	operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"dataTables-s5013",null,headerRowCount);
					        });
					    </script>  
			 
			 <tbody>
			 
			 <%
									
									S5013screensfl.set1stScreenRow(sfl, appVars, sv);
			                        int height=380;
									%>
							<%
	S5013screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	
	while (S5013screensfl.hasMoreScreenRows(sfl)) 
	{
		%>
		<%
	  {
		if (appVars.ind01.isOn()) {
			sv.opcda.setReverse(BaseScreenData.REVERSED);
			sv.opcda.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.opcda.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind02.isOn()) {
			sv.agerate.setReverse(BaseScreenData.REVERSED);
			sv.agerate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.agerate.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind02.isOn()) {
			sv.agerate.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind03.isOn()) {
			sv.oppc.setReverse(BaseScreenData.REVERSED);
			sv.oppc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.oppc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.oppc.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind04.isOn()) {
			sv.insprm.setReverse(BaseScreenData.REVERSED);
			sv.insprm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.insprm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.insprm.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind05.isOn()) {
			sv.extCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.extCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.extCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.extCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind06.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		/* if (appVars.ind07.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		
		if (appVars.ind06.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.reasind.setReverse(BaseScreenData.REVERSED);
			sv.reasind.setColor(BaseScreenData.RED);
			sv.reasind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.reasind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.znadjperc.setReverse(BaseScreenData.REVERSED);
			sv.znadjperc.setColor(BaseScreenData.RED);
			sv.znadjperc.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.znadjperc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.zmortpct.setReverse(BaseScreenData.REVERSED);
			sv.zmortpct.setColor(BaseScreenData.RED);
			sv.zmortpct.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.zmortpct.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		if (appVars.ind10.isOn()) {
			sv.premadj.setReverse(BaseScreenData.REVERSED);
			sv.premadj.setColor(BaseScreenData.RED);
			sv.premadj.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.premadj.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.uwoverwrite.setReverse(BaseScreenData.REVERSED);
			sv.uwoverwrite.setColor(BaseScreenData.RED);
			sv.uwoverwrite.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.uwoverwrite.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 END */
	}
%>		



<tr> 
				<td  align="center" >
					<INPUT type="checkbox" name="chk_R" id='<%="chk_R"+count %>' class="UICheck" onClick="selectedForDelete('<%="tablerow"+count %>')"
					<%
					if (((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) && sv.uwFlag == 2) {%>
										readonly="true" disabled <%}else if(sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y")){%>
										readonly="true" disabled <%}%> 
					/>
				</td>
				<td  align="left">														
					<%	
						String fieldName = "s5013screensfl.opcda_R" + count;
						String fieldValue = sv.opcda.getFormData();
						fieldItem=appVars.loadF4FieldsLong(new String[] {"opcda"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("opcda");
						optionValue = makeDropDownList( mappedItems , fieldValue,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.opcda.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.opcda).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
 <!-- smalchi2 for ILIFE-1032 STARTS --> 
			 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
							"blank_cell" : "output_cell" %>'>
	   		<%if(longValue != null){%>

	   		<%=longValue%>

	   		<%}%>
	   </div>
<!-- ENDS -->

<%
longValue = null;
%>

	<% }else {%>
	<% if("red".equals((sv.opcda).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:170px;"> 
					<%
					} 
					%>
					<!--  ILIFE-2087  changed width -->
					<select name='<%=fieldName%>' id='<%=fieldName%>' type='list' style="min-width:175px;"                               
					<%
				if((new Byte((sv.opcda).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()  || (sv.uwFlag == 1 && !sv.uwoverwrite.getFormData().trim().equals(""))){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.opcda).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
	class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.opcda).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
	                   } 
                         %>		
				</td>	
									
			<td style="min-width: 120px;">
			 <input type='text'
			 		<%if((sv.znadjperc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%> 
					maxLength='<%=sv.reasind.getLength()%>'
					 value='<%= sv.reasind.getFormData() %>' 
					 size='<%=sv.reasind.getLength()%>' 
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.reasind)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "reasind" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "reasind" + "_R" + count %>'
					 class = "input_cell"
					  style = "min-width: 120px;" 
					   <%
							if(((new Byte((sv.reasind).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()) && sv.uwFlag == 2){
								%>
						readonly="true"
						class="output_cell"
					<%}else if(sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y")){%>
						readonly="true"
						class="output_cell"
					<%
					} %>
					 >
			</td>
			  <%if (sv.uwFlag == 1) { %> 
			   
			   <td  align="left">														
					<%	
					 	longValue=sv.uwoverwrite.getFormData();
					   	if("N".equals(longValue)){
					    	longValue="No";
					    }
					   	else if("Y".equals(longValue)){
					    	longValue="Yes";
					    }
						if((new Byte((sv.uwoverwrite.getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()))){ 
					%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
											<%if(longValue != null){%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%}%>
						</div> <%
							longValue = null;
						%> <% }else {%> 
							
						<select	name='<%="s5013screensfl" + "." +
						 "uwoverwrite" + "_R" + count %>'
												id='<%="s5013screensfl" + "." +
						 "uwoverwrite" + "_R" + count %>'
												type='list' style="width: 120px;"
												onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.uwoverwrite)'
												onKeyUp='return checkMaxLength(this)'
												class="input_cell"
			   			<%
							if ((new Byte((sv.uwoverwrite).getEnabled())).compareTo(new Byte(
							BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()) ){
							%>
									readonly="true" class="output_cell"
							<%
							} else if ((new Byte((sv.uwoverwrite).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
												class="bold_cell" <%
							} else {
							%>
												class='
							<%=(sv.uwoverwrite).getColor() == null ? "input_cell"
							: (sv.uwoverwrite).getColor().equalsIgnoreCase("red") ? "input_cell red reverse"
							: "input_cell"%>'
												<%
							}if (sv.uwoverwrite.getFormData().trim().equals("")|| 
									(sv.skipautouw == 2 && !sv.uwoverwrite.getFormData().trim().equals("N"))){%>
							readonly="true"
							disabled
							class="output_cell"
							<%}
							%>>
							<%if (sv.uwoverwrite.getFormData().trim().equals("")){ %>
							<option value="" Selected readonly="true" >-----Select-----</option>
							<%}else{ %>
								<option value="Y"
									<% if(sv.uwoverwrite.getFormData().trim().equalsIgnoreCase("Y")) {%>
									Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<% if(sv.uwoverwrite.getFormData().trim().equalsIgnoreCase("N")) {%>
									Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


											</select>
											<%
					} }
					%>
							 </td>
							 <%} %>
			    <td><%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.znadjperc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
						formatValue = smartHF.getPicFormatted(qpsf,sv.znadjperc);
						
					%>					
					
					
					
					<input type='text' 
					<%if((sv.znadjperc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					maxLength='<%=sv.znadjperc.getLength()%>'
					 value='<%=formatValue %>' 
					 size='<%=sv.znadjperc.getLength()%>'
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.znadjperc)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "znadjperc" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "znadjperc" + "_R" + count %>'
					 class = "input_cell"
					  style = "width: 100px;"
					 <%
							if((new Byte((sv.znadjperc).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()  || (sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y"))){
					 %>
						readonly="true"
						class="output_cell"
						<%} %>
					  
					  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return getdoBlurNumber(event,true);'
					  
					 ></td>
					 
					 <td>
					 <%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.agerate).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.agerate);
					%>					
					
					
					
					<input type='text' 
					maxLength='<%=sv.agerate.getLength()%>'
					 value='<%=formatValue %>' 
					 size='<%=sv.agerate.getLength()%>'
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.agerate)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "agerate" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "agerate" + "_R" + count %>'
					 class = "input_cell"
					  style = "width: 30px;"
					  	<%
							if((new Byte((sv.agerate).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected() || (sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y"))){
					 	%>
							readonly="true"
							class="output_cell"	
						<%} %>
					  
					  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
					  
					 >
					 </td>
					 
					 
					 <td>
					 
					 
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.oppc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.oppc);
						
					%>					
					
					
					
					<input type='text' 
					<%if((sv.oppc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					 
					 maxLength='<%=sv.oppc.getLength()%>'
					 value='<%=formatValue %>' 
					 size='<%=sv.oppc.getLength()%>'
					 
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.oppc)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "oppc" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "oppc" + "_R" + count %>'
					 class = "input_cell"
					  style = "width: 130px;"
					  <%
							if((new Byte((sv.oppc).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected() || (sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y"))){
					  %>
							readonly="true"
							class="output_cell"
					<%} %>
					  
					  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return getdoBlurNumber(event,true);'
					  
					 >
					 				
					 
					 
					 
					 </td>
			
			
			<td style="min-width: 50px;">
			
							
				<%	
			qpsf = sfl.getCurrentScreenRow().getFieldXMLDef((sv.insprm).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.insprm,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input type='text' 
	<%if((sv.insprm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;min-width: 130px;"<% }%>
	maxLength='<%=sv.insprm.getLength()%>'
	 value='<%=valueThis %>' 
	 size='<%=sv.insprm.getLength()%>'
	 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s5013screensfl.insprm)' onKeyUp='return checkMaxLength(this)' 
	 name='<%="s5013screensfl" + "." +
	 "insprm" + "_R" + count %>'
	id='<%="s5013screensfl" + "." +
	 "insprm" + "_R" + count %>'
	 class = "input_cell"
	 <%
		if((new Byte((sv.insprm).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected() || (sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y"))){
	 %>
		readonly="true"
		class="output_cell"	
	<%} %>
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'>
			
			
			
			</td>
			
			<td style="min-width: 170px;">
			
				<%	
			qpsf = sfl.getCurrentScreenRow().getFieldXMLDef((sv.premadj).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.premadj,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input type='text' 
	<%if((sv.premadj).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;min-width: 150px;"<% }%>
	maxLength='<%=sv.premadj.getLength()%>'
	 value='<%=valueThis %>' 
	 size='<%=sv.premadj.getLength()%>'
	 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s5013screensfl.premadj)' onKeyUp='return checkMaxLength(this)' 
	 name='<%="s5013screensfl" + "." +
	 "premadj" + "_R" + count %>'
	id='<%="s5013screensfl" + "." +
	 "premadj" + "_R" + count %>'
	 class = "input_cell"
	  <%
		if((new Byte((sv.premadj).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected() || (sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y"))){
	 %>
		readonly="true"
		class="output_cell"
	<%} %>
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'>
			
			
			
			</td>
			
			
			
			<td>
			
			<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.extCessTerm).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.extCessTerm);
						
					%>					
					
					
					
					<input type='text' 
					<%if((sv.extCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					maxLength='<%=sv.extCessTerm.getLength()%>'
					 value='<%=formatValue %>' 
					 size='<%=sv.extCessTerm.getLength()%>'
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.extCessTerm)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "extCessTerm" + "_R" + count %>'
					id='<%="s5013screensfl" + "." +
					 "extCessTerm" + "_R" + count %>'
					 					 class = "input_cell"
					  style = "width: 50px;"
					  <%
							if(((new Byte((sv.extCessTerm).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()) && sv.uwFlag == 2){
	 					%>
						readonly="true"
						class="output_cell"	
						<%} else if(sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y")){%>
						readonly="true"
						class="output_cell"	
						<%
						}%>	
					  
					  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
					  
					 >
			
			
			</td>
			
			
			
			<td>
			
			 <input type='text' 
					maxLength='<%=sv.select.getLength()%>'
					 value='<%= sv.select.getFormData() %>' 
					 size='<%=sv.select.getLength()%>'
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.select)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "select" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "select" + "_R" + count %>'
					 class = "input_cell"
					  style = "width: 30px;"
					   <%
							if(((new Byte((sv.select).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()) && sv.uwFlag == 2){
	 					%>
						readonly="true"
						class="output_cell"	
						<%} else if(sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y")){%>
						readonly="true"
						class="output_cell"	
						<%
							
						}%>				  
					  >
			
		
			</td>
			
			
			
			<td style="min-width: 110px;">
			<input type='text' 
					 					 <%if((sv.zmortpct).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					maxLength='<%=sv.zmortpct.getLength()%>'
					 value='<%= sv.zmortpct.getFormData() %>' 
					 size='<%=sv.zmortpct.getLength()%>'
					 onFocus='doFocus(this)' onHelp='return fieldHelp(s5013screensfl.zmortpct)' onKeyUp='return checkMaxLength(this)' 
					 name='<%="s5013screensfl" + "." +
					 "zmortpct" + "_R" + count %>'
					 id='<%="s5013screensfl" + "." +
					 "zmortpct" + "_R" + count %>'
					 class = "input_cell"
					  style = "min-width:110px;"
					   <%
							if(((new Byte((sv.zmortpct).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()) && sv.uwFlag == 2){
	 					%>
						readonly="true"
						class="output_cell"	
						<%} else if(sv.uwFlag == 1 && sv.uwoverwrite.getFormData().trim().equals("Y")){%>
						readonly="true"
						class="output_cell"	
						<%
						}%>	
					  >
			</td>
			
			
			
			
			
			 </tr>
			 		  <%
						count = count + 1;
						S5013screensfl.setNextScreenRow(sfl, appVars, sv);
					}%>				
			 </tbody>
			 
			 
  
    </table>
    </div>
     </div>
    
    </div>
 		 <% if(sv.opcda.getFormData().trim().equals("")){ %> 
				<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="btn-group">
						<div class="sectionbutton">			
								<% if(fw.getVariables().isScreenProtected()){%>
								<a class="btn btn-success" disabled><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>		
								<%}
								else{%>
								<a id="subfile_add" class="btn btn-success" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<%} %>
								<a id="subfile_remove" class="btn btn-danger" href='javascript:;'disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
						</div>
					</div>
				</div>
			</div>
		</div>
   <%} %> 
  </div>
  </div>
  

<script>
$(document).ready(function() {
	$('#dataTables-s5013').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        paging:false,
        info:false
  	});
})
</script>


<script>
 $(document).ready(function(){

    $("#subfile_remove").click(function () {
    	
		$('input:checkbox[type=checkbox]').prop('checked',false);
	    	
	   	$("#subfile_remove").attr('disabled', 'disabled'); 

    });  
    
    
    $('input[type="checkbox"]'). click(function(){
    	if($(this). prop("checked") == true){
    		 $('#subfile_remove').removeAttr('disabled');
    	}else{
    		 $('#subfile_remove').attr("disabled","disabled");   
    	}
    
    });
  });	
</script>
<%@ include file="/POLACommon2NEW.jsp"%>



<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("No")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Code")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Smoking")%>
</div>


<br/>


	<div 	
	class='<%= (sv.smoking02.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'style="width: 25px; !important">
<%					
if(!((sv.smoking02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.smoking02.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>

	

</td></tr>

<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Code")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>
</td>
</tr></table></div>

