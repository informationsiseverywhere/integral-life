<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl05";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>

<%
	Sjl05ScreenVars sv = (Sjl05ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind05.isOn()) {
			sv.billfreq01.setReverse(BaseScreenData.REVERSED);
			sv.billfreq01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.billfreq01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.billfreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.billfreq02.setReverse(BaseScreenData.REVERSED);
			sv.billfreq02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.billfreq02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.billfreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.billfreq03.setReverse(BaseScreenData.REVERSED);
			sv.billfreq03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind47.isOn()) {
			sv.billfreq03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.billfreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq04.setReverse(BaseScreenData.REVERSED);
			sv.billfreq04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind48.isOn()) {
			sv.billfreq04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.billfreq05.setReverse(BaseScreenData.REVERSED);
			sv.billfreq05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind49.isOn()) {
			sv.billfreq05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.billfreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.billfreq06.setReverse(BaseScreenData.REVERSED);
			sv.billfreq06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.billfreq06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.billfreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.billfreq07.setReverse(BaseScreenData.REVERSED);
			sv.billfreq07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind51.isOn()) {
			sv.billfreq07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.billfreq07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.billfreq08.setReverse(BaseScreenData.REVERSED);
			sv.billfreq08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind52.isOn()) {
			sv.billfreq08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.billfreq08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.billfreq09.setReverse(BaseScreenData.REVERSED);
			sv.billfreq09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind53.isOn()) {
			sv.billfreq09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.billfreq09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.billfreq10.setReverse(BaseScreenData.REVERSED);
			sv.billfreq10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind54.isOn()) {
			sv.billfreq10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.billfreq10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind15.isOn()) {
			sv.mop01.setReverse(BaseScreenData.REVERSED);
			sv.mop01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind55.isOn()) {
			sv.mop01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind15.isOn()) {
			sv.mop01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.mop02.setReverse(BaseScreenData.REVERSED);
			sv.mop02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind56.isOn()) {
			sv.mop02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind16.isOn()) {
			sv.mop02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.mop03.setReverse(BaseScreenData.REVERSED);
			sv.mop03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind57.isOn()) {
			sv.mop03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind17.isOn()) {
			sv.mop03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.mop04.setReverse(BaseScreenData.REVERSED);
			sv.mop04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind58.isOn()) {
			sv.mop04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind18.isOn()) {
			sv.mop04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.mop05.setReverse(BaseScreenData.REVERSED);
			sv.mop05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind59.isOn()) {
			sv.mop05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind19.isOn()) {
			sv.mop05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.mop06.setReverse(BaseScreenData.REVERSED);
			sv.mop06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.mop06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind20.isOn()) {
			sv.mop06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.mop07.setReverse(BaseScreenData.REVERSED);
			sv.mop07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind61.isOn()) {
			sv.mop07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind21.isOn()) {
			sv.mop07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.mop08.setReverse(BaseScreenData.REVERSED);
			sv.mop08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind62.isOn()) {
			sv.mop08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind22.isOn()) {
			sv.mop08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.mop09.setReverse(BaseScreenData.REVERSED);
			sv.mop09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind63.isOn()) {
			sv.mop09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind23.isOn()) {
			sv.mop09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.mop10.setReverse(BaseScreenData.REVERSED);
			sv.mop10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind64.isOn()) {
			sv.mop10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind24.isOn()) {
			sv.mop10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind25.isOn()) {
			sv.fwcondt01.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind65.isOn()) {
			sv.fwcondt01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind25.isOn()) {
			sv.fwcondt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.fwcondt02.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind66.isOn()) {
			sv.fwcondt02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind26.isOn()) {
			sv.fwcondt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.fwcondt03.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind67.isOn()) {
			sv.fwcondt03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind27.isOn()) {
			sv.fwcondt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.fwcondt04.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind68.isOn()) {
			sv.fwcondt04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind28.isOn()) {
			sv.fwcondt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.fwcondt05.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind69.isOn()) {
			sv.fwcondt05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind29.isOn()) {
			sv.fwcondt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.fwcondt06.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.fwcondt06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.fwcondt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.fwcondt07.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind71.isOn()) {
			sv.fwcondt07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind31.isOn()) {
			sv.fwcondt07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.fwcondt08.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind72.isOn()) {
			sv.fwcondt08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind32.isOn()) {
			sv.fwcondt08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.fwcondt09.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind73.isOn()) {
			sv.fwcondt09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind33.isOn()) {
			sv.fwcondt09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.fwcondt10.setReverse(BaseScreenData.REVERSED);
			sv.fwcondt10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind74.isOn()) {
			sv.fwcondt10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind34.isOn()) {
			sv.fwcondt10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind35.isOn()) {
			sv.concommflg01.setReverse(BaseScreenData.REVERSED);
			sv.concommflg01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind75.isOn()) {
			sv.concommflg01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind35.isOn()) {
			sv.concommflg01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.concommflg02.setReverse(BaseScreenData.REVERSED);
			sv.concommflg02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind76.isOn()) {
			sv.concommflg02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind36.isOn()) {
			sv.concommflg02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.concommflg03.setReverse(BaseScreenData.REVERSED);
			sv.concommflg03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind77.isOn()) {
			sv.concommflg03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind37.isOn()) {
			sv.concommflg03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.concommflg04.setReverse(BaseScreenData.REVERSED);
			sv.concommflg04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind78.isOn()) {
			sv.concommflg04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind38.isOn()) {
			sv.concommflg04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.concommflg05.setReverse(BaseScreenData.REVERSED);
			sv.concommflg05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind79.isOn()) {
			sv.concommflg05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind39.isOn()) {
			sv.concommflg05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.concommflg06.setReverse(BaseScreenData.REVERSED);
			sv.concommflg06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind80.isOn()) {
			sv.concommflg06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind40.isOn()) {
			sv.concommflg06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.concommflg07.setReverse(BaseScreenData.REVERSED);
			sv.concommflg07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind81.isOn()) {
			sv.concommflg07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind41.isOn()) {
			sv.concommflg07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.concommflg08.setReverse(BaseScreenData.REVERSED);
			sv.concommflg08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind82.isOn()) {
			sv.concommflg08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind42.isOn()) {
			sv.concommflg08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.concommflg09.setReverse(BaseScreenData.REVERSED);
			sv.concommflg09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind83.isOn()) {
			sv.concommflg09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind43.isOn()) {
			sv.concommflg09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.concommflg10.setReverse(BaseScreenData.REVERSED);
			sv.concommflg10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind84.isOn()) {
			sv.concommflg10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind44.isOn()) {
			sv.concommflg10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:30px;">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Forward Contract Date")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence Flag")%></label>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop01");
						optionValue = makeDropDownList(mappedItems, sv.mop01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop01.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop01).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop01' type='list'
							<%if ((new Byte((sv.mop01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop01).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

				<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq01");
						optionValue = makeDropDownList(mappedItems, sv.billfreq01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq01.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq01).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq01' type='list'
							<%if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq01).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt01.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt01.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div  style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt01).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt01'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt01)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt01).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt01.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt01.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group"  style="width:85px;">
						<%
							if ((new Byte((sv.concommflg01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg01.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg01.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div  style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg01).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg01'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg01)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg01).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg01.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg01.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg01).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop02");
						optionValue = makeDropDownList(mappedItems, sv.mop02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop02.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop02).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop02' type='list'
							<%if ((new Byte((sv.mop02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop02).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq02");
						optionValue = makeDropDownList(mappedItems, sv.billfreq02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq02.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq02).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq02' type='list'
							<%if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq02).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt02.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt02.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt02).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt02'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt02)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt02).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt02.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt02.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg02.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg02.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg02).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg02'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg02)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg02).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg02.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg02.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg02).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop03" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop03");
						optionValue = makeDropDownList(mappedItems, sv.mop03.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop03.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop03).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop03' type='list'
							<%if ((new Byte((sv.mop03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop03).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq03" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq03");
						optionValue = makeDropDownList(mappedItems, sv.billfreq03.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq03.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq03).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq03' type='list'
							<%if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq03).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt03.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt03.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt03).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt03'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt03)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt03).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt03.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt03.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg03.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg03.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg03).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg03'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg03)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg03).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg03.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg03.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg03).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop04" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop04");
						optionValue = makeDropDownList(mappedItems, sv.mop04.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop04.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop04).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop04' type='list'
							<%if ((new Byte((sv.mop04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop04).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq04" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq04");
						optionValue = makeDropDownList(mappedItems, sv.billfreq04.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq04.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq04).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq04' type='list'
							<%if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq04).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt04.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt04.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt04).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt04'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt04)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt04).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt04.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt04.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg04.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg04.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg04).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg04'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg04)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg04).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg04.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg04.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg04).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop05" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop05");
						optionValue = makeDropDownList(mappedItems, sv.mop05.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop05.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop05).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop05' type='list'
							<%if ((new Byte((sv.mop05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop05).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq05" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq05");
						optionValue = makeDropDownList(mappedItems, sv.billfreq05.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq05.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq05).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq05' type='list'
							<%if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq05).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt05.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt05.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt05).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt05'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt05)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt05).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt05.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt05.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg05.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg05.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg05).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg05'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg05)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg05).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg05.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg05.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg05).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop06" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop06");
						optionValue = makeDropDownList(mappedItems, sv.mop06.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop06.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop06).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop06' type='list'
							<%if ((new Byte((sv.mop06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop06).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq06" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq06");
						optionValue = makeDropDownList(mappedItems, sv.billfreq06.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq06.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq06).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq06' type='list'
							<%if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq06).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt06.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt06.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt06).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt06'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt06)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt06).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt06.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt06.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg06.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg06.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg06).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg06'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg06)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg06).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg06.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg06.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg06).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop07" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop07");
						optionValue = makeDropDownList(mappedItems, sv.mop07.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop07.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop07).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop07' type='list'
							<%if ((new Byte((sv.mop07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop07).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq07" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq07");
						optionValue = makeDropDownList(mappedItems, sv.billfreq07.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq07.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq07).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq07' type='list'
							<%if ((new Byte((sv.billfreq07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq07).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt07.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt07.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt07).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt07'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt07)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt07).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt07.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt07.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt07).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg07.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg07.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg07).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg07'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg07)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg07).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg07.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg07.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg07).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width: 150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop08" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop08");
						optionValue = makeDropDownList(mappedItems, sv.mop08.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop08.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width: 150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop08).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop08' type='list'
							<%if ((new Byte((sv.mop08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop08).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq08" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq08");
						optionValue = makeDropDownList(mappedItems, sv.billfreq08.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq08.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width: 150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq08).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq08' type='list'
							<%if ((new Byte((sv.billfreq08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq08).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt08.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt08.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt08).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt08'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt08)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt08).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt08.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt08.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt08).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg08.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg08.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg08).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg08'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg08)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg08).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg08.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg08.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg08).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop09" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop09");
						optionValue = makeDropDownList(mappedItems, sv.mop09.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop09.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop09).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop09' type='list'
							<%if ((new Byte((sv.mop09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop09).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq09" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq09");
						optionValue = makeDropDownList(mappedItems, sv.billfreq09.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq09.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell'  style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq09).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq09' type='list'
							<%if ((new Byte((sv.billfreq09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq09).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt09.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt09.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt09).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt09'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt09)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt09).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt09.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt09.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt09).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg09.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg09.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg09).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg09'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg09)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg09).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg09.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg09.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg09).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop10" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop10");
						optionValue = makeDropDownList(mappedItems, sv.mop10.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop10.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.mop10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:85px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.mop10).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='mop10' type='list'
							<%if ((new Byte((sv.mop10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop10).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
					<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq10" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq10");
						optionValue = makeDropDownList(mappedItems, sv.billfreq10.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq10.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.billfreq10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.billfreq10).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='billfreq10' type='list'
							<%if ((new Byte((sv.billfreq10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq10).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.fwcondt10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.fwcondt10.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.fwcondt10.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.fwcondt10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fwcondt10).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='fwcondt10'
								onFocus='doFocus(this)' onHelp='return fieldHelp(fwcondt10)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.fwcondt10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fwcondt10).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.fwcondt10.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.fwcondt10.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<%
								if ("red".equals((sv.fwcondt10).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width:85px;">
						<%
							if ((new Byte((sv.concommflg10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.concommflg10.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Yes");
								}
								if (((sv.concommflg10.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("No");
								}
						%>

						<%
							if ((new Byte((sv.concommflg10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width:85px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.concommflg10).getColor())) {
						%>
						<div
							style="border: 2px; border-style: solid; border-color: #ec7572; width: 89px;">
							<%
								}
							%>

							<select name='concommflg10'
								onFocus='doFocus(this)' onHelp='return fieldHelp(concommflg10)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.concommflg10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.concommflg10).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="Y"
									<%if (((sv.concommflg10.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
								<option value="N"
									<%if (((sv.concommflg10.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


							</select>
							<% if("red".equals((sv.concommflg10).getColor())){%>
						</div>
						<%}%>
						<%}longValue = null;}%>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top:20px;">
	<div class="col-md-4">
        <div class="form-group">
			<a class="btn btn-info" href= "#" onClick=" JavaScript:doAction('PFKey91');"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
        	<a class="btn btn-info" href= "#" onClick=" JavaScript:doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
       </div>
	</div>
</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%@ include file="/POLACommon2NEW.jsp"%>