

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "ST500";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%St500ScreenVars sv = (St500ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts issue date From   ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To   ");%>

<%{
		if (appVars.ind99.isOn()) {
			sv.datefromDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefromDisp.setHasCursor(BaseScreenData.HASCURSOR);
			sv.datefromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind99.isOn()) {
			sv.datefromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind98.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setHasCursor(BaseScreenData.HASCURSOR);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind98.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- <script>
$(document).ready(function(){ 
	$("div").removeClass("has-error");
	$("#scheduleName").css("width","115px");
	$("#jobq").css("width","130px");
	$("#acctyear").css("width","85px");
	$("#bbranch").css("width","135px");
})
</script> -->

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Number / Name")%></label>
					<table><tr>
					<td>
							<%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
							<div class="output_cell">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
			} else {
		%>

							<div class="blank_cell" style="min-width: 100px;">&nbsp;</div>

							<% 
			} 
		%>
							<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>			
							<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div id="scheduleName" style="margin-left: 1px;"
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
						</tr>
						</table>
						</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
					<%if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div id="jobq"
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div 
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
					<table><tr>
					<td>
							<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
							<div class="output_cell">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
			} else {
		%>

							<div class="blank_cell" style="width: 50px;">&nbsp;</div>
							<% 
			} 
		%>
							<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>				
							<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
							<div id="acctyear" class="output_cell" style="margin-left: 1px;">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
			} else {
		%>

							<div class="blank_cell" style="width: 50px;margin-left: 1px;">&nbsp;</div>
							<% 
			} 
		%>
							<%
		longValue = null;
		formatValue = null;
		%>

			</td>
			</tr></table>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%					
		if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div  id="bcompany"
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>

				</div>
			</div>
			<div class="col-md-4" ></div>
			<div class="col-md-4" >
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					<%					
		if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div id="bbranch"
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>

				</div>
			</div>
		</div>
		<div class="row">

                 <div class="col-md-4">
    	           <div class="form-group">
    	            <label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts issue date From")%></label>
				    	<table>
				    		<tr>
				    			<td>
					<%	
						longValue = sv.datefromDisp.getFormData();  
					%>
					<% 
						if((new Byte((sv.datefromDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
						<%=smartHF.getRichTextDateInput(fw, sv.datefromDisp,(sv.datefromDisp.getLength()))%>
					<% }else {%>
					<div class="input-group date form_date col-md-12" style="width: 120px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
	                    <%=smartHF.getRichTextDateInput(fw, sv.datefromDisp,(sv.datefromDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                </div>
					<%}%>
	    			</td>
	    			<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
	    			<td>

					<%	
					longValue = sv.datetoDisp.getFormData();  
				%>
					<% 
						if((new Byte((sv.datetoDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
						<%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
					<% }else {%>
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" style="width:174px;"data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
	                    <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                </div>
					<%}%>
				    			</td>
				    		</tr>
				    	</table>
				</div>
			</div>
		</div>
	</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>