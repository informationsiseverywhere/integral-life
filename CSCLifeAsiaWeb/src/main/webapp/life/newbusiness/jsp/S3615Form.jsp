<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S3615";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%@ page import="java.util.*" %>
<%S3615ScreenVars sv = (S3615ScreenVars) fw.getVariables();%>

<%if (sv.S3615screenWritten.gt(0)) {%>
	<%S3615screen.clearClassString(sv);%>
	<%
		sv.onepcashless.setClassString("");
	%>
	<%
		sv.screenRow.setClassString("");
	%>
	<%
		sv.screenColumn.setClassString("");
	%>

	<%
		{
			if (appVars.ind01.isOn()) {
		sv.onepcashless.setReverse(BaseScreenData.REVERSED);
		sv.onepcashless.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
		sv.onepcashless.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.onepcashless.setEnabled(BaseScreenData.DISABLED);
			}
		}
	%>
	
<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		       		<div class="input-group">
		       		<%
		       			if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {
		       		%>
						<%
							if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.company.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}						
											}else{
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.company.getFormData()).toString()); 
												}else{
													formatValue = formatValue( longValue);
												}
											}
						%>
					<div class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell"%>' style="max-width:30px">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
								longValue = null;
											formatValue = null;
							%>
					<%
						}
					%>
					</div>
		       		</div>
		       	</div>
		       	
		       
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
		       		<div class="input-group">
		       		<%
		       			if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {
		       		%>
						<%
							if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}						
											}else{
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
												}else{
													formatValue = formatValue( longValue);
												}
											}
						%>
					<div class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
								longValue = null;
											formatValue = null;
							%>
					<%
						}
					%>
					</div>
		       		</div>
		       	</div>
		       	
		       	
					
		       	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                      <table>
						<tr>						
						<td>

        	
						<%
        								if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
        														
        																if(longValue == null || longValue.equalsIgnoreCase("")) {
        																	formatValue = formatValue( (sv.item.getFormData()).toString()); 
        																} else {
        																	formatValue = formatValue( longValue);
        																}
        																
        																
        														} else  {
        																	
        														if(longValue == null || longValue.equalsIgnoreCase("")) {
        																	formatValue = formatValue( (sv.item.getFormData()).toString()); 
        																} else {
        																	formatValue = formatValue( longValue);
        																}
        														
        														}
        							%>			
								<div class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
								longValue = null;
											formatValue = null;
							%>
						</td>
						<td>
 
  		
							<%
   										if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
   																	
   																			if(longValue == null || longValue.equalsIgnoreCase("")) {
   																				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
   																			} else {
   																				formatValue = formatValue( longValue);
   																			}
   																			
   																			
   																	} else  {
   																				
   																	if(longValue == null || longValue.equalsIgnoreCase("")) {
   																				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
   																			} else {
   																				formatValue = formatValue( longValue);
   																			}
   																	
   																	}
   									%>			
									<div class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell"%>'style="max-width:300px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
									longValue = null;
													formatValue = null;
								%>
						</td>
						</tr>
						</table>
					
						
  </div></div>
		       	</div>
		       	<br />
		       	<br />
		       	<br />
		      <div class="row">
		       	<div class="col-md-5">
		       		<div class="form-group">
		       		 <label>
		       		 <%
		       		 	if ((new Byte((sv.onepcashless).getInvisible())).compareTo(new Byte(
		       		 						BaseScreenData.INVISIBLE)) != 0) {
		       		 %>
							<%=resourceBundleHandler.gettingValueFromBundle("1P Cashless")%>

							<%
								}
							%> 
					</label>
							
							<%
															if ((new Byte((sv.onepcashless).getInvisible())).compareTo(new Byte(
																				BaseScreenData.INVISIBLE)) != 0) {
														%>
									
								<input  type='checkbox' name='onepcashless' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(multind)' onKeyUp='return checkMaxLength(this)'   
								
									
								
								<%if((sv.onepcashless).getColor()!=null){%>style='margin-left:0px;background-color:#FF0000;'
										<%}
								else{%>
									style='margin-left:0px'; 
								<%}
										if((sv.onepcashless).toString().trim().equalsIgnoreCase("Y")){%>checked
										
								      <%}if((sv.onepcashless).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('onepcashless')"/>
							
								<input type='checkbox' name='onepcashless' value='N' 
								
								<%if(!(sv.onepcashless).toString().trim().equalsIgnoreCase("Y")){%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('onepcashless')"/>
								<%}%>
		       		
		       		</div>
		       	</div>
		  </div>
		  <br>
      		

		</div>
</div>

<%}%>

<%if (sv.S3615protectWritten.gt(0)) {%>
	<%S3615protect.clearClassString(sv);%>

	<%
{
	}

	%>

<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>