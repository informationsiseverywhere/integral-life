<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR5AI";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%Sr5aiScreenVars sv = (Sr5aiScreenVars) fw.getVariables();%>
<% {
	if (appVars.ind02.isOn()) {
		sv.excda.setReverse(BaseScreenData.REVERSED);
		sv.excda.setColor(BaseScreenData.RED);
	}
	if (appVars.ind05.isOn()) {
		sv.excda.setEnabled(BaseScreenData.DISABLED);
	}
	if (!appVars.ind02.isOn()) {
		sv.excda.setHighLight(BaseScreenData.BOLD);
	}
	
if (appVars.ind07.isOn()) {
		sv.excltxt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind04.isOn()) {
		sv.exadtxt.setEnabled(BaseScreenData.DISABLED);
	}
	}%>
	
	
	
	
	
	
	
<div class="panel panel-default">
        <div class="panel-body">
<div class="row">
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Exclusion")%></label>

 <div class="form-group">
	<table><tr><td style="max-width:140px">
	
	<%	
	String[][] strArray = {{"excda"},{},{}};
	fieldItem=appVars.getShortDesc(strArray,"E","2",baseModel,sv);
	mappedItems = (Map) fieldItem.get("excda");
	optionValue = makeDropDownList( mappedItems , sv.excda.getFormData(),3,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.excda.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.excda).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.excda).getColor())){
%>
<div > 
<%
} 
%>

<select name='excda' type='list' style="width:140px;"
<% 
	if((new Byte((sv.excda).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled onclick="document.getElementById('excltxt').value='';"
	class="output_cell"
<%
	}else if((new Byte((sv.excda).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
		class="bold_cell" onclick="document.getElementById('excltxt').value='';"
<%
	}else { 
%>
	class = 'input_cell' onclick="document.getElementById('excltxt').value='';"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.excda).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</td></tr></table>
</div></div></div>










<div class="row">
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Text (Predefined)")%></label>



<textArea name="excltxt" id='excltxt' cols="120" maxlength="1500" rows="10" wrap="on" style="resize:none" onhelp="return fieldHelp('excltxt')"   
	class="textarea bold" style="z-index: 2; line-height:15px ;position: relative; width: 700px; height: 100px;  left: 10px;font-size:14;"
	
	<%if ((new Byte((sv.excltxt).getEnabled())).compareTo(new Byte(
								BaseScreenData.ENABLED)) != 0) {%>
		 disabled

	<% }%>
	><%=sv.excltxt.getFormData().toString().trim() %></textArea>
	
	
	</div></div>











<div class="row">
<div class="col-md-4">

<label><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Additional Text")%></label>



<textArea name="exadtxt" cols="120" maxlength="1500" rows="10" wrap="on" style="resize:none" onhelp="return fieldHelp('exadtxt')"   
	class="textarea bold" style="z-index: 2; line-height:15px ;position: relative; width: 700px; height: 200px;  left: 10px;font-size:14;"
	
	<%if((new Byte((sv.exadtxt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){%>	 
	disabled

	<% }%>
	><%=sv.exadtxt.getFormData().toString().trim() %></textArea>
	
	
	</div></div>











</div></div>
	
	
	
	
	 
	
	
	
	 
          
<%@ include file="/POLACommon2NEW.jsp"%>