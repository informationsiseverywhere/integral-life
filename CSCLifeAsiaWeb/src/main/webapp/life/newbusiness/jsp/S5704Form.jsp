

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5704";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5704ScreenVars sv = (S5704ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min. Base Unit ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min. Base Increment ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component Type");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of Base");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Based");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Base Unit");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.crtable01.setReverse(BaseScreenData.REVERSED);
			sv.crtable01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.crtable01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.rtable01.setReverse(BaseScreenData.REVERSED);
			sv.rtable01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.rtable01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.crtable02.setReverse(BaseScreenData.REVERSED);
			sv.crtable02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.crtable02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.rtable02.setReverse(BaseScreenData.REVERSED);
			sv.rtable02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.rtable02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.crtable03.setReverse(BaseScreenData.REVERSED);
			sv.crtable03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.crtable03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.rtable03.setReverse(BaseScreenData.REVERSED);
			sv.rtable03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.rtable03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.crtable04.setReverse(BaseScreenData.REVERSED);
			sv.crtable04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.crtable04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.rtable04.setReverse(BaseScreenData.REVERSED);
			sv.rtable04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rtable04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.crtable05.setReverse(BaseScreenData.REVERSED);
			sv.crtable05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.crtable05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.rtable05.setReverse(BaseScreenData.REVERSED);
			sv.rtable05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.rtable05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.crtable06.setReverse(BaseScreenData.REVERSED);
			sv.crtable06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.crtable06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.rtable06.setReverse(BaseScreenData.REVERSED);
			sv.rtable06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.rtable06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.crtable07.setReverse(BaseScreenData.REVERSED);
			sv.crtable07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.crtable07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.rtable07.setReverse(BaseScreenData.REVERSED);
			sv.rtable07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.rtable07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.crtable08.setReverse(BaseScreenData.REVERSED);
			sv.crtable08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.crtable08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.rtable08.setReverse(BaseScreenData.REVERSED);
			sv.rtable08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.rtable08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.crtable09.setReverse(BaseScreenData.REVERSED);
			sv.crtable09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.crtable09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.rtable09.setReverse(BaseScreenData.REVERSED);
			sv.rtable09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.rtable09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.crtable10.setReverse(BaseScreenData.REVERSED);
			sv.crtable10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.crtable10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.rtable10.setReverse(BaseScreenData.REVERSED);
			sv.rtable10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.rtable10.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		       		<div class="input-group">
		       			<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</div>
		       		</div>
		        </div>
		        
		       
					
		        <div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
		       		<div class="input-group">
		       				<%					
							if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="max-width: 900px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</div>
		       		</div>
		       	</div>
		       	
		     
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		       		<!-- <table><tr><td> -->
		       		<div class="input-group">
		       		<%					
						if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
			  		<!-- </td><td> -->
						<%					
						if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 900px;width:100px;padding-top:3px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</div>
		       		<!-- </td></tr></table> -->
		       		</div>
		       	</div>
		    </div>
		       		
		      <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
		       		<table>
				    		<tr>
				    			<td>
				    				<%if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    			<td style="padding-left: 10px;padding-right: 10px;"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
				    			<td>
				    				<%if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {			
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    		</tr>
				    	</table>
		       		</div>
		       	</div>
		       </div>   		
		       		
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Min. Base Unit")%></label>
		       		<div class="input-group">
		       		<%	
									qpsf = fw.getFieldXMLDef((sv.mbaseunt).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='mbaseunt' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.mbaseunt) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.mbaseunt);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.mbaseunt) %>'
							 <%}%>
						
						size='<%= sv.mbaseunt.getLength()%>'
						maxLength='<%= sv.mbaseunt.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(mbaseunt)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.mbaseunt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.mbaseunt).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.mbaseunt).getColor()== null  ? 
									"input_cell" :  (sv.mbaseunt).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</div>
		       		</div>
		        </div>
		        
		       
					
		        <div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Min. Base Increment")%></label>
		       		<div class="input-group">
		       		<%	
									qpsf = fw.getFieldXMLDef((sv.mbaseinc).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='mbaseinc' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.mbaseinc) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.mbaseinc);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.mbaseinc) %>'
							 <%}%>
						
						size='<%= sv.mbaseinc.getLength()%>'
						maxLength='<%= sv.mbaseinc.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(mbaseinc)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.mbaseinc).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.mbaseinc).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.mbaseinc).getColor()== null  ? 
									"input_cell" :  (sv.mbaseinc).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
		       		</div>
		       		</div>
		       	</div>
		    </div>    		
		       		
		     <div class="row">
	        	<div class="col-md-7">
		        	<label><%=resourceBundleHandler.gettingValueFromBundle("Component Type")%></label>
		       	</div>
		       	<div class="col-md-3" style="text-align: center;">
		       	
		       	<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
		       	
		       	</div>
		     </div>
		     
		     <div class="row">
	        	<div class="col-md-3">
		        	<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
		        	<div class="input-group">
		        	<% 
							fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("crtable01");
							optionValue = makeDropDownList( mappedItems , sv.crtable01.getFormData(),2,resourceBundleHandler); 
							longValue = (String) mappedItems.get((sv.crtable01.getFormData()).toString().trim());
							
					%>
					
					 <%=smartHF.getDropDownExt(sv.crtable01, fw, longValue, "crtable01", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable02");
						optionValue = makeDropDownList( mappedItems , sv.crtable02.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable02.getFormData()).toString().trim());  
					%>
					<%=smartHF.getDropDownExt(sv.crtable02, fw, longValue, "crtable02", optionValue,0,200) %>
					
					<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable03"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("crtable03");
							optionValue = makeDropDownList( mappedItems , sv.crtable03.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.crtable03.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.crtable03, fw, longValue, "crtable03", optionValue,0,200) %>
						
						<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable04");
						optionValue = makeDropDownList( mappedItems , sv.crtable04.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable04.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable04, fw, longValue, "crtable04", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable05");
						optionValue = makeDropDownList( mappedItems , sv.crtable05.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable05.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable05, fw, longValue, "crtable05", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable06"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable06");
						optionValue = makeDropDownList( mappedItems , sv.crtable06.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable06.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable06, fw, longValue, "crtable06", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable07");
						optionValue = makeDropDownList( mappedItems , sv.crtable07.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable07.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable07, fw, longValue, "crtable07", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable08");
						optionValue = makeDropDownList( mappedItems , sv.crtable08.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable08.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable08, fw, longValue, "crtable08", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable09"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable09");
						optionValue = makeDropDownList( mappedItems , sv.crtable09.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable09.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable09, fw, longValue, "crtable09", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable10"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("crtable10");
						optionValue = makeDropDownList( mappedItems , sv.crtable10.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.crtable10.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.crtable10, fw, longValue, "crtable10", optionValue,0,200) %>
						</div>
		       	</div>
		       	<div class="col-md-3">
		       	<label><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></label>
		       	<div class="input-group">
		       	<% 
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable01"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable01");
						optionValue = makeDropDownList( mappedItems , sv.rtable01.getFormData(),2,resourceBundleHandler); 
						longValue = (String) mappedItems.get((sv.rtable01.getFormData()).toString().trim());
				%>
				
				 <%=smartHF.getDropDownExt(sv.rtable01, fw, longValue, "rtable01", optionValue,0,200) %>
				 
				 <%	
					fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable02"},sv,"E",baseModel);
					mappedItems = (Map) fieldItem.get("rtable02");
					optionValue = makeDropDownList( mappedItems , sv.rtable02.getFormData(),2,resourceBundleHandler);  
					longValue = (String) mappedItems.get((sv.rtable02.getFormData()).toString().trim());  
				%>
				<%=smartHF.getDropDownExt(sv.rtable02, fw, longValue, "rtable02", optionValue,0,200) %>
				
				<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable03"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable03");
						optionValue = makeDropDownList( mappedItems , sv.rtable03.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable03.getFormData()).toString().trim());  
					%>
					<%=smartHF.getDropDownExt(sv.rtable03, fw, longValue, "rtable03", optionValue,0,200) %>
					
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable04");
						optionValue = makeDropDownList( mappedItems , sv.rtable04.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable04.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable04, fw, longValue, "rtable04", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable05");
						optionValue = makeDropDownList( mappedItems , sv.rtable05.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable05.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable05, fw, longValue, "rtable05", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable06"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable06");
						optionValue = makeDropDownList( mappedItems , sv.rtable06.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable06.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable06, fw, longValue, "rtable06", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable07");
						optionValue = makeDropDownList( mappedItems , sv.rtable07.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable07.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable07, fw, longValue, "rtable07", optionValue,0,200) %>
										 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable08");
						optionValue = makeDropDownList( mappedItems , sv.rtable08.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable08.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable08, fw, longValue, "rtable08", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable09"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable09");
						optionValue = makeDropDownList( mappedItems , sv.rtable09.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable09.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable09, fw, longValue, "rtable09", optionValue,0,200) %>
					 
					 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rtable10"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rtable10");
						optionValue = makeDropDownList( mappedItems , sv.rtable10.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rtable10.getFormData()).toString().trim());  
					%>
					 <%=smartHF.getDropDownExt(sv.rtable10, fw, longValue, "rtable10", optionValue,0,200) %>
					</div>					 
		       	</div>
		       	<div class="col-md-2">
		       	<div class="form-group">
		        	<label><%=resourceBundleHandler.gettingValueFromBundle("% of Base")%></label>
		        	<div class="input-group">
		        	<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct01).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct01' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct01) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct01);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct01) %>'
							 <%}%>
						
						size='<%= sv.unitpct01.getLength()%>'
						maxLength='<%= sv.unitpct01.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct01)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct01).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct01).getColor()== null  ? 
									"input_cell" :  (sv.unitpct01).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						
						<%	
								qpsf = fw.getFieldXMLDef((sv.unitpct02).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
								
						%>

					<input name='unitpct02' 
					type='text'
					
						value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct02) %>'
								 <%
						 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct02);
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct02) %>'
						 <%}%>
					
					size='<%= sv.unitpct02.getLength()%>'
					maxLength='<%= sv.unitpct02.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct02)' onKeyUp='return checkMaxLength(this)'  
					
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
					
					<% 
						if((new Byte((sv.unitpct02).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.unitpct02).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.unitpct02).getColor()== null  ? 
								"input_cell" :  (sv.unitpct02).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct03).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct03' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct03) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct03);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct03) %>'
							 <%}%>
						
						size='<%= sv.unitpct03.getLength()%>'
						maxLength='<%= sv.unitpct03.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct03)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct03).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct03).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct03).getColor()== null  ? 
									"input_cell" :  (sv.unitpct03).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct04).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct04' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct04) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct04);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct04) %>'
							 <%}%>
						
						size='<%= sv.unitpct04.getLength()%>'
						maxLength='<%= sv.unitpct04.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct04)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct04).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct04).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct04).getColor()== null  ? 
									"input_cell" :  (sv.unitpct04).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct05).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct05' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct05) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct05);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct05) %>'
							 <%}%>
						
						size='<%= sv.unitpct05.getLength()%>'
						maxLength='<%= sv.unitpct05.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct05)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct05).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct05).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct05).getColor()== null  ? 
									"input_cell" :  (sv.unitpct05).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct06).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct06' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct06) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct06);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct06) %>'
							 <%}%>
						
						size='<%= sv.unitpct06.getLength()%>'
						maxLength='<%= sv.unitpct06.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct06)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct06).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct06).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct06).getColor()== null  ? 
									"input_cell" :  (sv.unitpct06).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct07).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct07' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct07) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct07);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct07) %>'
							 <%}%>
						
						size='<%= sv.unitpct07.getLength()%>'
						maxLength='<%= sv.unitpct07.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct07)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct07).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct07).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct07).getColor()== null  ? 
									"input_cell" :  (sv.unitpct07).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpct08).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
									
							%>
						
						<input name='unitpct08' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct08) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct08);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct08) %>'
							 <%}%>
						
						size='<%= sv.unitpct08.getLength()%>'
						maxLength='<%= sv.unitpct08.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct08)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpct08).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpct08).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpct08).getColor()== null  ? 
									"input_cell" :  (sv.unitpct08).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
							<%	
										qpsf = fw.getFieldXMLDef((sv.unitpct09).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
										
								%>
							
							<input name='unitpct09' 
							type='text'
							
								value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct09) %>'
										 <%
								 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct09);
								 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct09) %>'
								 <%}%>
							
							size='<%= sv.unitpct09.getLength()%>'
							maxLength='<%= sv.unitpct09.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct09)' onKeyUp='return checkMaxLength(this)'  
							
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>' 
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
							
							<% 
								if((new Byte((sv.unitpct09).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.unitpct09).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.unitpct09).getColor()== null  ? 
										"input_cell" :  (sv.unitpct09).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							
							<%	
										qpsf = fw.getFieldXMLDef((sv.unitpct10).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
										
								%>
							
							<input name='unitpct10' 
							type='text'
							
								value='<%=smartHF.getPicFormatted(qpsf,sv.unitpct10) %>'
										 <%
								 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpct10);
								 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpct10) %>'
								 <%}%>
							
							size='<%= sv.unitpct10.getLength()%>'
							maxLength='<%= sv.unitpct10.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitpct10)' onKeyUp='return checkMaxLength(this)'  
							
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>' 
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
							
							<% 
								if((new Byte((sv.unitpct10).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.unitpct10).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.unitpct10).getColor()== null  ? 
										"input_cell" :  (sv.unitpct10).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
					</div>
			       	</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       	<div class="form-group">
		       	<label><%=resourceBundleHandler.gettingValueFromBundle("Based")%></label>
		       	<div class="input-group">
		       	<input name='sinsbsd01' 
						type='text'
						
						<%
						
								formatValue = (sv.sinsbsd01.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.sinsbsd01.getLength()%>'
						maxLength='<%= sv.sinsbsd01.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd01)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.sinsbsd01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.sinsbsd01).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.sinsbsd01).getColor()== null  ? 
									"input_cell" :  (sv.sinsbsd01).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<input name='sinsbsd02' 
							type='text'
							
							<%
							
									formatValue = (sv.sinsbsd02.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.sinsbsd02.getLength()%>'
							maxLength='<%= sv.sinsbsd02.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd02)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.sinsbsd02).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.sinsbsd02).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.sinsbsd02).getColor()== null  ? 
										"input_cell" :  (sv.sinsbsd02).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							
							<input name='sinsbsd03' 
									type='text'
									
									<%
									
											formatValue = (sv.sinsbsd03.getFormData()).toString();
									
									%>
										value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
									
									size='<%= sv.sinsbsd03.getLength()%>'
									maxLength='<%= sv.sinsbsd03.getLength()%>' 
									onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd03)' onKeyUp='return checkMaxLength(this)'  
									
									
									<% 
										if((new Byte((sv.sinsbsd03).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>  
										readonly="true"
										class="output_cell"
									<%
										}else if((new Byte((sv.sinsbsd03).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>	
											class="bold_cell" 
									
									<%
										}else { 
									%>
									
										class = ' <%=(sv.sinsbsd03).getColor()== null  ? 
												"input_cell" :  (sv.sinsbsd03).getColor().equals("red") ? 
												"input_cell red reverse" : "input_cell" %>'
									 
									<%
										} 
									%>
									>
									
									<input name='sinsbsd04' 
									type='text'
									
									<%
									
											formatValue = (sv.sinsbsd04.getFormData()).toString();
									
									%>
										value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
									
									size='<%= sv.sinsbsd04.getLength()%>'
									maxLength='<%= sv.sinsbsd04.getLength()%>' 
									onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd04)' onKeyUp='return checkMaxLength(this)'  
									
									
									<% 
										if((new Byte((sv.sinsbsd04).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>  
										readonly="true"
										class="output_cell"
									<%
										}else if((new Byte((sv.sinsbsd04).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>	
											class="bold_cell" 
									
									<%
										}else { 
									%>
									
										class = ' <%=(sv.sinsbsd04).getColor()== null  ? 
												"input_cell" :  (sv.sinsbsd04).getColor().equals("red") ? 
												"input_cell red reverse" : "input_cell" %>'
									 
									<%
										} 
									%>
									>
									
									<input name='sinsbsd05' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd05.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd05.getLength()%>'
										maxLength='<%= sv.sinsbsd05.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd05)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd05).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd05).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd05).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd05).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
										
										<input name='sinsbsd06' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd06.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd06.getLength()%>'
										maxLength='<%= sv.sinsbsd06.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd06)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd06).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd06).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd06).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd06).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
										
										<input name='sinsbsd07' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd07.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd07.getLength()%>'
										maxLength='<%= sv.sinsbsd07.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd07)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd07).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd07).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd07).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd07).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
										
										<input name='sinsbsd08' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd08.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd08.getLength()%>'
										maxLength='<%= sv.sinsbsd08.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd08)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd08).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd08).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd08).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd08).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
										
										<input name='sinsbsd09' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd09.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd09.getLength()%>'
										maxLength='<%= sv.sinsbsd09.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd09)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd09).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd09).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd09).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd09).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
										
										<input name='sinsbsd10' 
										type='text'
										
										<%
										
												formatValue = (sv.sinsbsd10.getFormData()).toString();
										
										%>
											value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
										
										size='<%= sv.sinsbsd10.getLength()%>'
										maxLength='<%= sv.sinsbsd10.getLength()%>' 
										onFocus='doFocus(this)' onHelp='return fieldHelp(sinsbsd10)' onKeyUp='return checkMaxLength(this)'  
										
										
										<% 
											if((new Byte((sv.sinsbsd10).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
										%>  
											readonly="true"
											class="output_cell"
										<%
											}else if((new Byte((sv.sinsbsd10).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	
												class="bold_cell" 
										
										<%
											}else { 
										%>
										
											class = ' <%=(sv.sinsbsd10).getColor()== null  ? 
													"input_cell" :  (sv.sinsbsd10).getColor().equals("red") ? 
													"input_cell red reverse" : "input_cell" %>'
										 
										<%
											} 
										%>
										>
		       	</div>
		       	</div></div>
		       	
		       	<div class="col-md-2">
		       	<div class="form-group">
		        	<label><%=resourceBundleHandler.gettingValueFromBundle("Base Unit")%></label>
		        	<div class="input-group">
							<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg01).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>

						<input name='unitpkg01' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg01) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg01);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg01) %>'
							 <%}%>
						
						size='<%= sv.unitpkg01.getLength()%>'
						maxLength='<%= sv.unitpkg01.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg01)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg01).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg01).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg01).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg01).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg02).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg02' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg02) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg02);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg02) %>'
							 <%}%>
						
						size='<%= sv.unitpkg02.getLength()%>'
						maxLength='<%= sv.unitpkg02.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg02)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg02).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg02).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg02).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg02).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg03).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg03' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg03) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg03);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg03) %>'
							 <%}%>
						
						size='<%= sv.unitpkg03.getLength()%>'
						maxLength='<%= sv.unitpkg03.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg03)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg03).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg03).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg03).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg03).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg04).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg04' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg04) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg04);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg04) %>'
							 <%}%>
						
						size='<%= sv.unitpkg04.getLength()%>'
						maxLength='<%= sv.unitpkg04.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg04)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg04).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg04).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg04).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg04).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg05).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg05' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg05) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg05);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg05) %>'
							 <%}%>
						
						size='<%= sv.unitpkg05.getLength()%>'
						maxLength='<%= sv.unitpkg05.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg05)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg05).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg05).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg05).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg05).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
								qpsf = fw.getFieldXMLDef((sv.unitpkg06).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								
						%>
					
					<input name='unitpkg06' 
					type='text'
					
						value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg06) %>'
								 <%
						 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg06);
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg06) %>'
						 <%}%>
					
					size='<%= sv.unitpkg06.getLength()%>'
					maxLength='<%= sv.unitpkg06.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg06)' onKeyUp='return checkMaxLength(this)'  
					
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
					
					<% 
						if((new Byte((sv.unitpkg06).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.unitpkg06).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.unitpkg06).getColor()== null  ? 
								"input_cell" :  (sv.unitpkg06).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg07).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg07' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg07) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg07);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg07) %>'
							 <%}%>
						
						size='<%= sv.unitpkg07.getLength()%>'
						maxLength='<%= sv.unitpkg07.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg07)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg07).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg07).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg07).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg07).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg08).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg08' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg08) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg08);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg08) %>'
							 <%}%>
						
						size='<%= sv.unitpkg08.getLength()%>'
						maxLength='<%= sv.unitpkg08.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg08)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg08).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg08).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg08).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg08).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
									qpsf = fw.getFieldXMLDef((sv.unitpkg09).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='unitpkg09' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg09) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg09);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg09) %>'
							 <%}%>
						
						size='<%= sv.unitpkg09.getLength()%>'
						maxLength='<%= sv.unitpkg09.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg09)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.unitpkg09).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.unitpkg09).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.unitpkg09).getColor()== null  ? 
									"input_cell" :  (sv.unitpkg09).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						
						<%	
										qpsf = fw.getFieldXMLDef((sv.unitpkg10).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										
								%>
							
							<input name='unitpkg10' 
							type='text'
							
								value='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg10) %>'
										 <%
								 valueThis=smartHF.getPicFormatted(qpsf,sv.unitpkg10);
								 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								 title='<%=smartHF.getPicFormatted(qpsf,sv.unitpkg10) %>'
								 <%}%>
							
							size='<%= sv.unitpkg10.getLength()%>'
							maxLength='<%= sv.unitpkg10.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitpkg10)' onKeyUp='return checkMaxLength(this)'  
							
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>' 
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
							
							<% 
								if((new Byte((sv.unitpkg10).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.unitpkg10).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.unitpkg10).getColor()== null  ? 
										"input_cell" :  (sv.unitpkg10).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
		       	</div>
		     </div>
		     </div>
		     </div>
		     
		      <div class="row">
		      <div class="col-md-3">
		       				<div class="form-group">
		       				</div></div>
		      </div>
		       		 <div class="row">
		       		 <div class="col-md-8">
		       		 </div>
	        			<div class="col-md-4">
		       				<div class="form-group">
		       				<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
		       				<div class="input-group">
		       				<input name='contitem' 
								type='text'
								
								<%
								
										formatValue = (sv.contitem.getFormData()).toString();
								
								%>
									value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
								
								size='<%= sv.contitem.getLength()%>'
								maxLength='<%= sv.contitem.getLength()%>' 
								onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  
								
								
								<% 
									if((new Byte((sv.contitem).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									class="output_cell"
								<%
									}else if((new Byte((sv.contitem).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								
								<%
									}else { 
								%>
								
									class = ' <%=(sv.contitem).getColor()== null  ? 
											"input_cell" :  (sv.contitem).getColor().equals("red") ? 
											"input_cell red reverse" : "input_cell" %>'
								 
								<%
									} 
								%>
								>
								</div>
		       				</div>
		       			</div>
		       		</div>
      		
		</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
