

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5007";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5007ScreenVars sv = (S5007ScreenVars) fw.getVariables();%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
	
	if (appVars.ind02.isOn()) {
			sv.splitBcomm.setReverse(BaseScreenData.REVERSED);
			sv.splitBcomm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.splitBcomm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.splitBpts.setReverse(BaseScreenData.REVERSED);
			sv.splitBpts.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.splitBpts.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Agency ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage split");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus points");%>
<%		appVars.rollup(new int[] {93});
%>







<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                        <table>
                        <tr>
                        <td>
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						<td>	
  		
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						<td>
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
					</tr>
					</table>
					
</div>
</div>

			    	<div class="col-md-4" > 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
                        <table>
                        <tr>
                        <td>
                        <%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	</td>
  	<td>
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td>
</tr>
</table>

</div></div>
 
                   <div class="col-md-4" > 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Agency")%></label>
                       <table>
                       <tr>
                       <td>
		<%					
		if(!((sv.sellagent.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sellagent.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sellagent.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  <td>
	
  		
		<%					
		if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agentname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
       </td>
       </tr>
       </table>

    </div></div>

</div>

<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				 	<div class="table-responsive">
	         			<table  id='dataTables-s5007' class="table table-striped table-bordered table-hover" width="100%" >
              		    <thead>
  
					  
						    <tr class='info'>
						      <th style="text-align: center;" rowspan=2><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></th>
						      <th style="text-align: center;" colspan=2><%=resourceBundleHandler.gettingValueFromBundle("Percentage Split")%></th>
						    </tr>    
						     
						    <tr class='info'>             
						       <th  style="text-align: center;" ><%=resourceBundleHandler.gettingValueFromBundle("Commission")%></th>
						       <th  style="text-align: center;" ><%=resourceBundleHandler.gettingValueFromBundle("Bonus Points")%></th>  
						    </tr>


  						</thead>
<%
		GeneralTable sfl = fw.getTable("s5007screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>

<%
	S5007screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5007screensfl
	.hasMoreScreenRows(sfl)) {	
%>
<tbody>

 <tr>
     	<td class="text-center" width="5%">
     	
     	               <%if((sv.agntsel).getClass().getSimpleName().equals("ZonedDecimalData")) {%><!-- align="right" --><% }else {%><!--  align="left"  --><%}%> 
																			
      				   <div class="input-group" >
						
						<input name='<%="s5007screensfl" + "." +
						 "agntsel" + "_R" + count %>'
						id='<%="s5007screensfl" + "." + "agntsel" + "_R" + count %>'
						type='text' 
						value='<%= sv.agntsel.getFormData() %>' 
						maxLength='<%=sv.agntsel.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s5007screensfl.agntsel)' onKeyUp='return checkMaxLength(this)' 
						 style = "min-width: 140px;"
						<%
                            if((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||
                            fw.getVariables().isScreenProtected()){
                        %>
                        readonly="true" class="output_cell">
                        <%} else {%>                        		
						class = " <%=(sv.agntsel).getColor()== null  ? 
						"input_cell" :  
						(sv.agntsel).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>"> 
					    
					    <!-- ILIFE-6893 id changed for agntsel and doFocus of btn-->
					    <span class="input-group-btn">
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('<%="s5007screensfl" + "." +
    						 "agntsel" + "_R" + count %>')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                        </span>
                        <%} %>
				
					     </div>
						
						
									
						<%--<a href="javascript:;" 
						onClick="doFocus(document.getElementById('<%="s5007screensfl" + "." +
						 "agntsel" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
						
						 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0">
						</a> --%>
						          
						          
						 <!-- <span class="input-group-btn">
					       <button class="btn btn-info" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04');">Search</button>
					     </span> -->
						</td>
	   
	    <td class="text-center">
	    
	                    <%if((sv.splitBcomm).getClass().getSimpleName().equals("ZonedDecimalData")) {%><!-- align="center" --><% }else {%><!--  align="left" --> <%}%> 									
																	
													
                      	<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.splitBcomm).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.splitBcomm);
							
						%>					
						
						
						 <div class="row" style="text-allign:center;">
						 <div class="col-md-4" style="text-allign:center;"></div>
						   <div class="col-md-2" style="text-allign:center;">
						<input type='text' 
						maxLength='<%=sv.splitBcomm.getLength()%>'
						 value='<%= XSSFilter.escapeHtml(formatValue)%>' 
						 size='<%=sv.splitBcomm.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5007screensfl.splitBcomm)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5007screensfl" + "." +
						 "splitBcomm" + "_R" + count %>'
						 id='<%="s5007screensfl" + "." +
						 "splitBcomm" + "_R" + count %>'
				 		<%if ((new Byte((sv.splitBcomm).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" <%
		 				} else {%> 
		 					class="input_cell" 
		 				<%} %>						 
						  style = "width: <%=sv.splitBcomm.getLength()*12%>px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
						 </div></div>
						 </td>
						 
						     								
		<td class="text-center" >
		
		                 <%if((sv.splitBpts).getClass().getSimpleName().equals("ZonedDecimalData")) {%><!-- align="center" --><% }else {%><!--  align="left"  --><%}%> 									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.splitBpts).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.splitBpts);
							
						%>					
						
						<div class="row" style="text-allign:center;">
						 <div class="col-md-4" style="text-allign:center;"></div>
						   <div class="col-md-2" style="text-allign:center;">
						
						<input type='text' 
						maxLength='<%=sv.splitBpts.getLength()%>'
						 value='<%= XSSFilter.escapeHtml(formatValue)%>' 
						 size='<%=sv.splitBpts.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5007screensfl.splitBpts)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5007screensfl" + "." +
						 "splitBpts" + "_R" + count %>'
						 id='<%="s5007screensfl" + "." +
						 "splitBpts" + "_R" + count %>'
				 		<%if ((new Byte((sv.splitBpts).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" <%
		 				} else {%> 
		 					class="input_cell" 
		 				<%} %>
						  style = "width: <%=sv.splitBpts.getLength()*12%>px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event);;'
						  title='<%=formatValue %>'
						 >
						 </div></div>
						 </td>
	
</tr>  

</tbody>
	<%
	count = count + 1;
	S5007screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>

</div>
</div>
</div>
</div>


</div></div>


<script>
$(document).ready(function() {
	$('#dataTables-s5007').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '350',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
       
  	});
	
		
	
})
</script>


<%@ include file="/POLACommon2NEW.jsp"%>

