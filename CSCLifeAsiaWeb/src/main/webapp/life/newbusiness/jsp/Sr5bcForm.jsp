<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR5BC";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%//@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr5bcScreenVars sv = (Sr5bcScreenVars) fw.getVariables();%>
<!-- ILIFE-7805 : Start -->	
<%
	{
		appVars.rolldown(new int[] { 27 });
		appVars.rollup(new int[] { 27 });			
		if (appVars.ind10.isOn()) {
			sv.totTranAmt.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>
<!-- ILIFE-7805 : End -->	

<div class="panel panel-default">
    	<div class="panel-body">
    	
   <div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%
			qpsf = fw.getFieldXMLDef((sv.mbrAccNum).getFieldName());
			

	%>
					<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Member Account Number"))%></label> 
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
					<%if((new Byte((sv.mbrAccNum).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){%>
<div class='<%= (sv.mbrAccNum.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.mbrAccNum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mbrAccNum.getFormData())); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.mbrAccNum.getFormData())); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%} else {	
%>

<input name=mbrAccNum 

type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mbrAccNum) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mbrAccNum);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mbrAccNum) %>'
	 <%}%>
	 
	  <%if(sv.mbrAccNum.getFormData()!=null && sv.mbrAccNum.getFormData().trim().length()>0) {%> title='<%=sv.mbrAccNum.getFormData()%>' <%}%>
maxLength=20
size='<%= sv.mbrAccNum.getLength()+5%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mbrAccNum)' onKeyUp='return checkMaxLength(this)'  onkeypress="return isNumeric(event)"
	class = ' <%=(sv.mbrAccNum).getColor()== null  ? 
			"input_cell" :  (sv.mbrAccNum).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>'
/>
<%} %>
</div>
				</div></div>
					
					
	</div>
	 <div class="row">
			<div class="col-md-4">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fund USI Number"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
					<%if((new Byte((sv.USIFndNum).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){%>
<div class='<%= (sv.USIFndNum.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'>
<%					
if(!((sv.USIFndNum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.USIFndNum.getFormData())); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.USIFndNum.getFormData())); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%} else {	
%>

<input name=USIFndNum 

type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.USIFndNum) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.USIFndNum);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.USIFndNum) %>'
	 <%}%>
	 
	  <%if(sv.USIFndNum.getFormData()!=null && sv.USIFndNum.getFormData().trim().length()>0) {%> title='<%=sv.USIFndNum.getFormData()%>' <%}%>
maxLength=11
size='<%= sv.USIFndNum.getLength()+5%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(USIFndNum)' onKeyUp='return checkMaxLength(this)'  onkeypress="return isNumeric(event)"
	class = ' <%=(sv.USIFndNum).getColor()== null  ? 
			"input_cell" :  (sv.USIFndNum).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>'
/>
<%} %>
</div>
				
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Eligible Service Period Start Date"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%
						if ((new Byte((sv.startdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width:80px"><%=smartHF.getRichTextDateInput(fw, sv.startdteDisp)%></div>
					<%
						} else {
					%>

					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.startdteDisp, (sv.startdteDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				
				
				</div>
				
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Tax Free Component Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.taxFreeCompAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxFreeCompAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='taxFreeCompAmt' 
type='text'
<%if((sv.taxFreeCompAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxFreeCompAmt.getLength(), sv.taxFreeCompAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxFreeCompAmt.getLength(), sv.taxFreeCompAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxFreeCompAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxFreeCompAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxFreeCompAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxFreeCompAmt).getColor()== null  ? 
			"input_cell" :  (sv.taxFreeCompAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
</div>
				
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Superannuation Rollover KiwiSaver Taxfree Component"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.kiwiCompAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.kiwiCompAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='kiwiCompAmt' 
type='text'
<%if((sv.kiwiCompAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.kiwiCompAmt.getLength(), sv.kiwiCompAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.kiwiCompAmt.getLength(), sv.kiwiCompAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(kiwiCompAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.kiwiCompAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.kiwiCompAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.kiwiCompAmt).getColor()== null  ? 
			"input_cell" :  (sv.kiwiCompAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Taxable Component Taxed Element Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.taxAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='taxAmt' 
type='text'
<%if((sv.taxAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxAmt.getLength(), sv.taxAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxAmt.getLength(), sv.taxAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxAmt).getColor()== null  ? 
			"input_cell" :  (sv.taxAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
			</div>	
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Taxable Component Untaxed Element"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.nonTaxAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.nonTaxAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='nonTaxAmt' 
type='text'
<%if((sv.nonTaxAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.nonTaxAmt.getLength(), sv.nonTaxAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.nonTaxAmt.getLength(), sv.nonTaxAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(nonTaxAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.nonTaxAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.nonTaxAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.nonTaxAmt).getColor()== null  ? 
			"input_cell" :  (sv.nonTaxAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Preserved Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.presAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.presAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='presAmt' 
type='text'
<%if((sv.presAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.presAmt.getLength(), sv.presAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.presAmt.getLength(), sv.presAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(presAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.presAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.presAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.presAmt).getColor()== null  ? 
			"input_cell" :  (sv.presAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("KiwiSaver Preserved Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.kiwiPresAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.kiwiPresAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='kiwiPresAmt' 
type='text'
<%if((sv.kiwiPresAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.kiwiPresAmt.getLength(), sv.kiwiPresAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.kiwiPresAmt.getLength(), sv.kiwiPresAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(kiwiPresAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.kiwiPresAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.kiwiPresAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.kiwiPresAmt).getColor()== null  ? 
			"input_cell" :  (sv.kiwiPresAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Unrestricted Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.unRestrictAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.unRestrictAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='unRestrictAmt' 
type='text'
<%if((sv.unRestrictAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unRestrictAmt.getLength(), sv.unRestrictAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unRestrictAmt.getLength(), sv.unRestrictAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unRestrictAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.unRestrictAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.unRestrictAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.unRestrictAmt).getColor()== null  ? 
			"input_cell" :  (sv.unRestrictAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Restricted Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.restrictAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.restrictAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='restrictAmt' 
type='text'
<%if((sv.restrictAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.restrictAmt.getLength(), sv.restrictAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.restrictAmt.getLength(), sv.restrictAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(restrictAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.restrictAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.restrictAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.restrictAmt).getColor()== null  ? 
			"input_cell" :  (sv.restrictAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label style="padding-top:6px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Transferred Amount"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<div class="input-group">
				<%	
			qpsf = fw.getFieldXMLDef((sv.totTranAmt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.totTranAmt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='totTranAmt' 
type='text'
<%if((sv.totTranAmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>
	
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totTranAmt.getLength(), sv.totTranAmt.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totTranAmt.getLength(), sv.totTranAmt.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(totTranAmt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.totTranAmt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.totTranAmt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.totTranAmt).getColor()== null  ? 
			"input_cell" :  (sv.totTranAmt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
				</div>
				</div></div></div>
				 <div class="row">
			<div class="col-md-6">
				<div class="form-group">
				
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Unclaimed Superannuation Money Indicator"))%></label> 
				
				</div></div>
					<div class="col-md-4">
				<div class="form-group">
				
				<input type='checkbox' name='moneyInd' value='Y'  onFocus='doFocus(this)'
onHelp='return fieldHelp(moneyInd)' onKeyUp='return checkMaxLength(this)'

<%

if((sv.moneyInd).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.moneyInd).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }if((sv.moneyInd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('moneyInd')"/>

<input type='checkbox' name='moneyInd' value=' '

<% if(!(sv.moneyInd).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }%>

style="visibility: hidden" onclick="handleCheckBox('moneyInd')"/>
				
				</div></div></div>
					
					
					
</div></div>













<%@ include file="/POLACommon2NEW.jsp"%>