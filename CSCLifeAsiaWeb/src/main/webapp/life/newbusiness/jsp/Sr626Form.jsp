

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR626";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sr626ScreenVars sv = (Sr626ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.clntnum.setReverse(BaseScreenData.REVERSED);
			sv.clntnum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.clntnum.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind12.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind14.isOn()) {
			sv.datefrmDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind15.isOn()) {
			sv.datetoDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number   ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner    ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Payor    ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency            ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commence ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of Payment  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing frequency  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Trustee");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Trustee Name");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
<%		appVars.rollup(new int[] {93});
%>
<script>
$(document).ready(function(){
	
	$("#chdrnum").attr("class","input-group-addon");
	$("#cnttype").attr("class","input-group-addon");
	$("#ctypedes").attr("class","form-control").css("width","300px");
	$("#pstate").attr("class","form-control").css("width","150px");
	$("#rstate").attr("class","input-group-addon");
	$("#clntname01").attr("class","form-control").css("width","150px");
	$("#cownnum").attr("class","input-group-addon");
	
	$("#clntname02").attr("class","form-control").css("width","150px");
	$("#payrnum").attr("class","input-group-addon");
	
	$("#clntname03").attr("class","form-control").css("width","150px");
	$("#agntnum").attr("class","input-group-addon");
	
	
})
</script>
<div class="panel panel-default">
 <div class="panel-body">   
 	<div class="row">
 	
 		<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
 	
	    	<div class="col-md-4"> 
		    		<div class="form-group">
 <label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</label>
<table>
<tr>
<td>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div   class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

	</td><td style="min-width:1px"></td><td>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:40px" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td><td style="min-width:1px"></td><td>
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:110px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
</tr>
</table>
</div></div><%}%>




<div class="col-md-4"> </div>
<div class="col-md-4"> 
	    		<div class="form-group">

<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
</label>
<%}%>
<table>
<tr>
<td>

<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

	</td><td style="min-width:1px"></td><td>
	
<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div   class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
</tr>
</table>

</div></div></div>

<div class="row">
	<div class="col-md-4">
				<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
</label>
<%}%>
<table>
<tr>
<td>

<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td><td style="min-width:1px"></td><td>
<%if ((new Byte((sv.clntname01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clntname01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
</tr>
</table>
</div>
</div>

	<div class="col-md-4">
				<div class="form-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Payor")%>
</label>
<%}%>
<table>
<tr>
<td>
<%if ((new Byte((sv.payrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

	</td><td style="min-width:1px"></td><td>

<%if ((new Byte((sv.clntname02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clntname02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td>
</tr>
</table>
</div></div>
<div class="col-md-4">
				<div class="form-group">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Agency")%>
</label>
<%}%>
<table>
<tr>
<td>
<%if ((new Byte((sv.agntnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%> 
	</td><td style="min-width:1px"></td><td>
<%if ((new Byte((sv.clntname03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clntname03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div   class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
</tr>
</table>

</div></div></div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
				<label style="white-space: nowrap;">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%> </label>
						<%} %>
                   <!-- ILJ-49 ends -->	
				</label>
				<%}%>


				<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:75px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
<div class="col-md-4">
				<div class="form-group">
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%>
</label>
<%}%>

<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("mop");
		longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());  
	%>
<%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:160px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
<div class="col-md-4">
				<div class="form-group">
					<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Billing frequency")%>
</label>
<%}%>

<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("billfreq");
		longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
	%> 
<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width:240px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
					</div>
				</div>
			</div>

	
		</br>



<div class="row">
<div class="col-md-12">
	<div >
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id='dataTables-sr626' width="100%">
              <thead>
				<tr class='info'>
					
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center></th>
					
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>									
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
	
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
			      </tr>
             </thead>
             	
         
		<%
		GeneralTable sfl = fw.getTable("sr626screensfl");
		
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
             <script language="javascript">
					        $(document).ready(function(){
					        	var rows = <%=sfl.count() + 1%>;
								var isPageDown = 1;
								var pageSize = 1;
								<%if (!appVars.ind54.isOn()) {%> /*BSIBF-119 START*/
								var headerRowCount= 1;
								<%} else {%>
								var headerRowCount= rows;
								 <%}%>					 	/*BSIBF-119 END*/
								 //IFSU-1092 Start
								var fields = new Array();	//required fields
								fields[0] = "clntnum";
								//IFSU-1092 End
					        	operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"dataTables-sr626",null,headerRowCount);
					        });
					    </script>
          							
				
		<!-- ILIFE-2422 Coding and Unit testing - Life Cross Browser - Sprint 1 D3: Task 5 ends-->
	
	<%

	Sr626screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	Map<String,Map<String,String>> reasonCdMap = appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);

	while (Sr626screensfl.hasMoreScreenRows(sfl)) {	
	%>


<%{
		if (appVars.ind01.isOn()) {
			sv.clntnum.setReverse(BaseScreenData.REVERSED);
			sv.clntnum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.clntnum.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind12.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind14.isOn()) {
			sv.datefrmDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind15.isOn()) {
			sv.datetoDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- <tbody> -->

	<tr>
	
		<td align="center"><!-- ILIFE-2185 alignment changed -->
				<INPUT type="checkbox" name="chk_R" id='<%="chk_R"+count %>'  class="UICheck" /></td>
			
						<%if((new Byte((sv.clntnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    		<td style="min-width:150px;"
					<%if((sv.clntnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%> align="right"<% }else {%> align="left" <%}%> >
					<div class="input-group" style="max-width:110px;">
						<input class="form-control" name='<%="sr626screensfl" + "." +
						 "clntnum" + "_R" + count %>'
						id='<%="sr626screensfl" + "." +
						 "clntnum" + "_R" + count %>'
						type='text' 
						value='<%= sv.clntnum.getFormData() %>'
						<%if(((new Byte((sv.clntnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (sv.isScreenProtected())){%>
										readonly="true" class="output_cell">
						<%
							}else {
						%>							 
						class = " <%=(sv.clntnum).getColor()== null  ? 
						"input_cell" :  
						(sv.clntnum).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.clntnum.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(sr626screensfl.clntnum)' onKeyUp='return checkMaxLength(this)' 
						>
						  <span class="input-group-btn">
							  <button class="btn btn-info" type="button" style="font-size:20px; margin-left:-5px"
									onClick="doFocus(document.getElementById('<%="sr626screensfl" + "." +
								    "clntnum" + "_R" + count %>'));doAction('PFKEY04');">
								    <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							  </button>
						  </span>	
						</div>	
						<% } %>				
						</td>
		             <%}else{%>
					   <td style="min-width:150px;">
					   </td>				
					<%}%>
					<%if((new Byte((sv.textone).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    		<td style="min-width:200px;" 
					<%if((sv.textone).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> ><%= sv.textone.getFormData()%></td>
		              <%}else{%>
					<td>
														
				    </td>
										
					<%}%>
					<%if((new Byte((sv.reasoncd).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    			<td  align="left">
		    			<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) reasonCdMap.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
					<!-- ILIFE-595 START -- Screen Sr626 - Fields should not be editable in enquiry mode -->
					<% if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (sv.isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>' style="width: 100px;">  
						  <%if(longValue != null){%>
						   		<%=longValue%>
						  <%}%>
					</div>
					<!-- ILIFE-595 END -- Screen Sr626 - Fields should not be editable in enquiry mode -->
					<%
					longValue = null;
					%>
					<% }else {%>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  "> 
					<%
					} 
					%>
					<select name='<%="sr626screensfl.reasoncd_R"+count%>' id='<%="sr626screensfl.reasoncd_R"+count%>' type='list'
					   class = 'input_cell'>
				<!-- 	ILIFE-3017 ended by vjain60  -->
					
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>
				    
											
									</td>
		<%}else{%>
					<td></td>
										
					<%}%>
					<%if((new Byte((sv.datefrmDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    		<td 
					<%if((sv.datefrmDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
					<%	
					longValue = sv.datefrmDisp.getFormData();  
					%>
					<% 
						if((new Byte((sv.datefrmDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "input_cell" %>' style="width: 100px;">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<div class="input-group date form_date col-md-2" data-date="" data-date-format="dd/mm/yyyy" data-link-field=''<%="sr626screensfl" + "." +
					 "datefrmDisp" + "_R" + count %>' data-link-format="dd/mm/yyyy" style="width:143px">
					<input
					 name='<%="sr626screensfl" + "." +
					 "datefrmDisp" + "_R" + count %>'
					id='<%="sr626screensfl" + "." +
					 "datefrmDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.datefrmDisp.getFormData() %>' 
					class = " <%=(sv.datefrmDisp).getColor()== null  ? 
					"input_cell" :
					(sv.datefrmDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.datefrmDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr626screensfl.datefrmDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style ="width:100px"
					>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                </div>	
					<%}%>  			
				
											
									</td>
		<%}else{%>
					<td>						
				    </td>
										
					<%}%>
					<%if((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    			<td 
					<%if((sv.datetoDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
					<%	
					longValue = sv.datetoDisp.getFormData();  
					%>
					<% 
						if((new Byte((sv.datetoDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "input_cell" %>' style="width: 100px;">  
						   	<%if(longValue != null){%>
						   		<%=longValue%>
						   	<%}%>
					 </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<div class="input-group date form_date col-md-2" data-date="" data-date-format="dd/mm/yyyy" data-link-field=''<%="sr626screensfl" + "." +
					 "datetoDisp" + "_R" + count %>' data-link-format="dd/mm/yyyy" style="width:143px">
					<input name='<%="sr626screensfl" + "." +
					 "datetoDisp" + "_R" + count %>'
					id='<%="sr626screensfl" + "." +
					 "datetoDisp" + "_R" + count %>'
					type='text' 
					value='<%=sv.datetoDisp.getFormData() %>' 
					class = " <%=(sv.datetoDisp).getColor()== null  ? 
					"input_cell" :
					(sv.datetoDisp).getColor().equals("red") ? 
					"input_cell red reverse" : 
					"input_cell" %>" 
					maxLength='<%=sv.datetoDisp.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(sr626screensfl.datetoDisp)' onKeyUp='return checkMaxLength(this)'  
					class = "input_cell"
					style ="width:100px;"
					>
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                </div>
					<%}%>  			
				    </td>
		            <%}else{%>
						 <td></td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr626screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	<!-- </tbody> -->
</table>
</div>
<!--  ILIFE-2422 use super table, end-->
</DIV>
</div></div>
	<% if(!sv.scflag.equals("I")){ %>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="btn-group">
						<div class="sectionbutton">						
								<a id="subfile_add" class="btn btn-success" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<a id="subfile_remove" class="btn btn-danger" href='javascript:;'disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
				
						</div>
					</div>
				</div>
			</div>
		</div>
		 <%} %>

     </div>
</div>

	<!-- Manual works ends -->
	 
<div style='display:none;'>
<table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
</tr></table></div>
<script>
 $(document).ready(function(){

    $("#subfile_remove").click(function () {
    	
		$('input:checkbox[type=checkbox]').prop('checked',false);
	    	
	   	$("#subfile_remove").attr('disabled', 'disabled'); 

    });  
  });	 
</script>

<script>
	$(document).ready(function() {
		$('#dataTables-sr626').DataTable({
			ordering : false,
			searching : false,
			scrollY : "350px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false
		});
	});
</script> 





<%@ include file="/POLACommon2NEW.jsp"%>

