<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6640";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6640ScreenVars sv = (S6640ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reserve Method ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Low Cost Subroutine ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversionary Bonus Method ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Low Cost Rider ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus/Dividend Allocation ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Surrender");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Dividend Method ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Dividend Withdrawal Method ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider can Outdate main Coverage (Y/N) ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.reserveMeth.setReverse(BaseScreenData.REVERSED);
			sv.reserveMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.reserveMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.lcsubr.setReverse(BaseScreenData.REVERSED);
			sv.lcsubr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.lcsubr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.revBonusMeth.setReverse(BaseScreenData.REVERSED);
			sv.revBonusMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.revBonusMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.lcrider.setReverse(BaseScreenData.REVERSED);
			sv.lcrider.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.lcrider.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zbondivalc.setReverse(BaseScreenData.REVERSED);
			sv.zbondivalc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zbondivalc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.surrenderBonusMethod.setReverse(BaseScreenData.REVERSED);
			sv.surrenderBonusMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.surrenderBonusMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.zcshdivmth.setReverse(BaseScreenData.REVERSED);
			sv.zcshdivmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.zcshdivmth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.zdivwdrmth.setReverse(BaseScreenData.REVERSED);
			sv.zdivwdrmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.zdivwdrmth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.zrmandind.setReverse(BaseScreenData.REVERSED);
			sv.zrmandind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.zrmandind.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
	<div class="panel-body">
		<%-- row 1 --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>

					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="max-width:20px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
							<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							longValue = null;
							formatValue = null;
						%>


						</div>
						</div>
					</div>
				</div>

		<%--  row 2 --%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
										 	longValue = null;
										 	formatValue = null;
										 %>
							</td>
							<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>

							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	longValue = null;
								 	formatValue = null;
								 %>

							</td>
						</tr>
					</table>
				
			</div>
		</div>
	</div>
	<%-- row 3 --%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Reserve Method")%></label>
					<div class="input-group">

					<%
						if ((new Byte((sv.reserveMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='reserveMeth' type='text' id='reserveMeth'
						value='<%=sv.reserveMeth.getFormData()%>'
						maxLength='<%=sv.reserveMeth.getLength()%>'
						size='<%=sv.reserveMeth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(reserveMeth)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.reserveMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.reserveMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" > 
					<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('reserveMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
						} else {
					%>

					class = '
					<%=(sv.reserveMeth).getColor() == null ? "input_cell"
							: (sv.reserveMeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > 
							<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('reserveMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					<%}longValue = null;} %>


				</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-3">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Low Cost Subroutine")%></label> 
						<input name='lcsubr' type='text'
						<%formatValue = (sv.lcsubr.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.lcsubr.getLength()%>'
						maxLength='<%=sv.lcsubr.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(lcsubr)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lcsubr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lcsubr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lcsubr).getColor() == null ? "input_cell"
						: (sv.lcsubr).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>
		</div>
		<%-- row 4 --%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Reversionary Bonus Method")%></label>
					<div class="input-group">
					<%
						longValue = sv.revBonusMeth.getFormData();
					%>

					<%
						if ((new Byte((sv.revBonusMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='revBonusMeth' type='text' id='revBonusMeth'
						value='<%=sv.revBonusMeth.getFormData()%>'
						maxLength='<%=sv.revBonusMeth.getLength()%>'
						size='<%=sv.revBonusMeth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(revBonusMeth)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.revBonusMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.revBonusMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" >
					<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('revBonusMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%
						} else {
					%>

					class = '
					<%=(sv.revBonusMeth).getColor() == null ? "input_cell"
							: (sv.revBonusMeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > 
							<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('revBonusMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%}longValue = null;} %>

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Low Cost Rider")%></label>
					<div class="input-group">
					<%
						longValue = sv.lcrider.getFormData();
					%>

					<%
						if ((new Byte((sv.lcrider).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="max-width:100px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='lcrider' type='text' id='lcrider'
						value='<%=sv.lcrider.getFormData()%>'
						maxLength='<%=sv.lcrider.getLength()%>'
						size='<%=sv.lcrider.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(lcrider)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.lcrider).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.lcrider).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" >
					 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('lcrider')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					<%
						} else {
					%>

					class = '
					<%=(sv.lcrider).getColor() == null ? "input_cell"
							: (sv.lcrider).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > 
							<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('lcrider')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					<%}longValue = null;} %>

					</div>
				</div>
			</div>
		</div>
		<%-- row 5 --%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus/Dividend Allocation")%></label>
					<div class="input-group">
					<%
						longValue = sv.zbondivalc.getFormData();
					%>

					<%
						if ((new Byte((sv.zbondivalc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='zbondivalc' type='text' id='zbondivalc'
						value='<%=sv.zbondivalc.getFormData()%>'
						maxLength='<%=sv.zbondivalc.getLength()%>'
						size='<%=sv.zbondivalc.getLength() + 3%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zbondivalc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zbondivalc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.zbondivalc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" >
					 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zbondivalc')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%
						} else {
					%>

					class = '
					<%=(sv.zbondivalc).getColor() == null ? "input_cell"
							: (sv.zbondivalc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > 
							 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zbondivalc')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%}longValue = null;} %>

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Surrender")%></label>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus")%></label>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Method")%></label>
					</div>
					<div class="input-group">
					<%
						longValue = sv.surrenderBonusMethod.getFormData();
					%>

					<%
						if ((new Byte((sv.surrenderBonusMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='surrenderBonusMethod' type='text' id='surrenderBonusMethod'
						value='<%=sv.surrenderBonusMethod.getFormData()%>'
						maxLength='<%=sv.surrenderBonusMethod.getLength()%>'
						size='<%=sv.surrenderBonusMethod.getLength()%>'
						onFocus='doFocus(this)'
						onHelp='return fieldHelp(surrenderBonusMethod)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.surrenderBonusMethod).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.surrenderBonusMethod).getHighLight()))
									.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" > 
					<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('surrenderBonusMethod')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%
						} else {
					%>

					class = '
					<%=(sv.surrenderBonusMethod).getColor() == null ? "input_cell"
							: (sv.surrenderBonusMethod).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>' > 
									<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('surrenderBonusMethod')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					<%}longValue = null;} %>

					</div>
				</div>
			</div>
		</div>
		<%-- row 6 --%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Dividend Method")%></label>
					<div class="input-group"  style="max-width:100px;">
					<%
						longValue = sv.zcshdivmth.getFormData();
					%>

					<%
						if ((new Byte((sv.zcshdivmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='zcshdivmth' type='text' id='zcshdivmth'
						value='<%=sv.zcshdivmth.getFormData()%>'
						maxLength='<%=sv.zcshdivmth.getLength()%>'
						size='<%=sv.zcshdivmth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zcshdivmth)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zcshdivmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.zcshdivmth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" > 
					<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zcshdivmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>


					<%
						} else {
					%>

					class = '
					<%=(sv.zcshdivmth).getColor() == null ? "input_cell"
							: (sv.zcshdivmth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' >
							<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zcshdivmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					<%}longValue = null;} %>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Dividend Withdrawal Method")%></label>
					<div class="input-group">
					<%
						longValue = sv.zdivwdrmth.getFormData();
					%>

					<%
						if ((new Byte((sv.zdivwdrmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<input name='zdivwdrmth' type='text' id='zdivwdrmth' 
						value='<%=sv.zdivwdrmth.getFormData()%>'
						maxLength='<%=sv.zdivwdrmth.getLength()%>'
						size='<%=sv.zdivwdrmth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zdivwdrmth)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zdivwdrmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell">

					<%
						} else if ((new Byte((sv.zdivwdrmth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell" > 
					<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zdivwdrmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					<%
						} else {
					%>

					class = '
					<%=(sv.zdivwdrmth).getColor() == null ? "input_cell"
							: (sv.zdivwdrmth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' >
							<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('zdivwdrmth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>


					<%}longValue = null;} %>
					</div>
				</div>
			</div>
		</div>
		<%-- row 7 --%>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Rider can Outdate main Coverage (Y/N)")%></label> 
					<input type='checkbox' name='zrmandind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(zrmandind)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.zrmandind).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.zrmandind).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.zrmandind).getEnabled() == BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('zrmandind')"/>

<input type='checkbox' name='zrmandind' value='N' 

<% if(!(sv.zrmandind).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('zrmandind')"/>
				</div>
			</div>
			
			
		</div>
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

