

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH576";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh576ScreenVars sv = (Sh576ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate Adjustment Unit ");%>
<style>
.input-group-addon{
  width:100%
}
</style>
<div class="panel panel-default">
<div class="panel-body">
    	
    	  <div class="row"> 
    	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
    	        <input class="form-control" type="text" style="width: 50px" placeholder=<%=sv.company.getFormData().toString()%> disabled>
    	      </div>
    	    </div>
    	    
    	    <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
    	       <input class="form-control" type="text" style="width: 60px" placeholder=<%=sv.tabl.getFormData().toString()%> disabled>
    	      </div>
    	    </div>
    	    
    	    <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
    	       
    	       <table>
	    	       <tr>
		    	       <td>
		    	       	<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
		    	       </td>
		    	       <td>
		    	       	 <%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc) %>	  
		    	       </td>
	    	       </tr>
    	       </table>
    	       
    	       </div>
    	     </div>
    	     </div>
    	     
    	     <div class="row"> 
    	    
    	      <div class="col-md-3">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjustment Unit")%></label>
    	       <%	
				qpsf = fw.getFieldXMLDef((sv.unit).getFieldName());
				qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
					%>
				   <input name='unit' type='text' style="text-align: right;width:110px;" 
				
				
					value='<%=smartHF.getPicFormatted(qpsf,sv.unit) %>'
							 <%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.unit);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=smartHF.getPicFormatted(qpsf,sv.unit) %>'
					 <%}%>
				
				size='<%= sv.unit.getLength()%>'
				maxLength='<%= sv.unit.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(unit)' onKeyUp='return checkMaxLength(this)'  
				
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
				
				<% 
					if((new Byte((sv.unit).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.unit).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.unit).getColor()== null  ? 
							"input_cell" :  (sv.unit).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
		</div>
</div>


</div>

</div>
</div>   	    



<%@ include file="/POLACommon2NEW.jsp"%>

