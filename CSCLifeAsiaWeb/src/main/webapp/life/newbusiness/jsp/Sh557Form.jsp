
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH557";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh557ScreenVars sv = (Sh557ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>


<div class="panel panel-default">
       <div class="panel-body">
            <div class="row">
               <div class="col-md-4" >
	             <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </div>
</div>	
<!-- <div class="col-md-2"></div> -->
 <div class="col-md-4">
          <div class="form-group">
               <label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%> </label>
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   </div> 
</div>	
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
	<div class="form-group">
                 <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%> </label>
                 <!-- <div class="input-group"> -->
                 <table><tr><td>
                 <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="item">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 <!--  </div>	 -->
 </td></tr></table>
 </div>		
</div>
</div>
<div class="row">
               <div class="col-md-6">
	             <div class="form-group">
                     <input name='zflpxtra01' 
                  type='text'

<%

		formatValue = (sv.zflpxtra01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zflpxtra01.getLength()%>'
maxLength='<%= sv.zflpxtra01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zflpxtra01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zflpxtra01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zflpxtra01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zflpxtra01).getColor()== null  ? 
			"input_cell" :  (sv.zflpxtra01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>		
</div>
<div class="col-md-6"></div>
</div>
<div class="row">
        <div class="col-md-6">
	          <div class="form-group">
                  <input name='zflpxtra02' 
type='text'

<%

		formatValue = (sv.zflpxtra02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zflpxtra02.getLength()%>'
maxLength='<%= sv.zflpxtra02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zflpxtra02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zflpxtra02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zflpxtra02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zflpxtra02).getColor()== null  ? 
			"input_cell" :  (sv.zflpxtra02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>		
</div>
<div class="col-md-6"></div>
</div>

<div class="row">
        <div class="col-md-6">
	          <div class="form-group">

           <input name='zflpxtra03'  
type='text'

<%

		formatValue = (sv.zflpxtra03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zflpxtra03.getLength()%>'
maxLength='<%= sv.zflpxtra03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zflpxtra03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zflpxtra03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zflpxtra03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zflpxtra03).getColor()== null  ? 
			"input_cell" :  (sv.zflpxtra03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>		
</div>
<div class="col-md-6"></div>
</div>		
</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

