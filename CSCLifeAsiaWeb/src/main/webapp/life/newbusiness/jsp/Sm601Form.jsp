

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM601";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sm601ScreenVars sv = (Sm601ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"GL Account");%>

<%{
		appVars.rolldown();
		appVars.rollup();
	}

	%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>

					<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:35px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
<div class="col-md-2" ></div>
			<div class="col-md-2" >
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
						%>
				</div>
			</div>

			<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-right: 3px
	}
}
</style>
<div class="col-md-2"></div>

				<div class="col-md-4">
					<div class="form-group">

						<label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
								<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:60px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
		longValue = null;
		formatValue = null;
		%>

							</td><td>
								<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
								<div
									class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell"%>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;max-width: 205px;text-align: left;border:1px solid #ccc !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
		longValue = null;
		formatValue = null;
		%>
							</td></tr></table>
					</div>
				</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("GL Account")%>
					</label>

					<div class="input-group">

						<input name='genlcdex01' type='text'
							<%if((sv.genlcdex01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex01.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex01.getLength()%>'
							maxLength='<%= sv.genlcdex01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex01)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex01).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex01).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>

			<div class="col-md-4" style="top: 22;">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex11' type='text'
							<%if((sv.genlcdex11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex11.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex11.getLength()%>'
							maxLength='<%= sv.genlcdex11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex11)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex11).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4" style="top: 22;">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex21' type='text'
							<%if((sv.genlcdex21).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex21.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex21.getLength()%>'
							maxLength='<%= sv.genlcdex21.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex21)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex21).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex21).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex02' type='text'
							<%if((sv.genlcdex02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex02.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex02.getLength()%>'
							maxLength='<%= sv.genlcdex02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex02)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex02).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex12' type='text'
							<%if((sv.genlcdex12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex12.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex12.getLength()%>'
							maxLength='<%= sv.genlcdex12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex12)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex12).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex22' type='text'
							<%if((sv.genlcdex22).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex22.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex22.getLength()%>'
							maxLength='<%= sv.genlcdex22.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex22)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex22).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex22).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex03' type='text'
							<%if((sv.genlcdex03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex03.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex03.getLength()%>'
							maxLength='<%= sv.genlcdex03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex03)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex03).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex13' type='text'
							<%if((sv.genlcdex13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex13.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex13.getLength()%>'
							maxLength='<%= sv.genlcdex13.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex13)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex13).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex23' type='text'
							<%if((sv.genlcdex23).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex23.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex23.getLength()%>'
							maxLength='<%= sv.genlcdex23.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex23)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex23).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex23).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex04' type='text'
							<%if((sv.genlcdex04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex04.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex04.getLength()%>'
							maxLength='<%= sv.genlcdex04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex04)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex04).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex14' type='text'
							<%if((sv.genlcdex14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex14.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex14.getLength()%>'
							maxLength='<%= sv.genlcdex14.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex14)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex14).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex24' type='text'
							<%if((sv.genlcdex24).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex24.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex24.getLength()%>'
							maxLength='<%= sv.genlcdex24.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex24)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex24).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex24).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex05' type='text'
							<%if((sv.genlcdex05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex05.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex05.getLength()%>'
							maxLength='<%= sv.genlcdex05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex05)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex05).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex15' type='text'
							<%if((sv.genlcdex15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex15.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex15.getLength()%>'
							maxLength='<%= sv.genlcdex15.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex15)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex15).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex25' type='text'
							<%if((sv.genlcdex25).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex25.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex25.getLength()%>'
							maxLength='<%= sv.genlcdex25.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex25)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex25).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex25).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex25).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex06' type='text'
							<%if((sv.genlcdex06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex06.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex06.getLength()%>'
							maxLength='<%= sv.genlcdex06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex06)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex06).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex16' type='text'
							<%if((sv.genlcdex16).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex16.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex16.getLength()%>'
							maxLength='<%= sv.genlcdex16.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex16)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex16).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex16).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex26' type='text'
							<%if((sv.genlcdex26).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex26.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex26.getLength()%>'
							maxLength='<%= sv.genlcdex26.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex26)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex26).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex26).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex26).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex07' type='text'
							<%if((sv.genlcdex07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex07.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex07.getLength()%>'
							maxLength='<%= sv.genlcdex07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex07)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex07).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex17' type='text'
							<%if((sv.genlcdex17).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex17.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex17.getLength()%>'
							maxLength='<%= sv.genlcdex17.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex17)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex17).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex17).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex27' type='text'
							<%if((sv.genlcdex27).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex27.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex27.getLength()%>'
							maxLength='<%= sv.genlcdex27.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex27)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex27).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex27).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex27).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex08' type='text'
							<%if((sv.genlcdex08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex08.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex08.getLength()%>'
							maxLength='<%= sv.genlcdex08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex08)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex08).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex18' type='text'
							<%if((sv.genlcdex18).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex18.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex18.getLength()%>'
							maxLength='<%= sv.genlcdex18.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex18)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex18).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex18).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="input-group">
						<input name='genlcdex28' type='text'
							<%if((sv.genlcdex28).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex28.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex28.getLength()%>'
							maxLength='<%= sv.genlcdex28.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex28)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex28).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex28).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex28).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%
	} 
%>>
					</div>
				</div>
			</div>
		</div>
		 <div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<input name='genlcdex09' type='text'
						<%if((sv.genlcdex09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex09.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex09.getLength()%>'
						maxLength='<%= sv.genlcdex09.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex09)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex09).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<input name='genlcdex19' type='text'
						<%if((sv.genlcdex19).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex19.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex19.getLength()%>'
						maxLength='<%= sv.genlcdex19.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex19)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex19).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex19).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
					</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
		<input name='genlcdex29' type='text'
						<%if((sv.genlcdex29).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex29.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex29.getLength()%>'
						maxLength='<%= sv.genlcdex29.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex29)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex29).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex29).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex29).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<input name='genlcdex10' type='text'
						<%if((sv.genlcdex10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex10.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex10.getLength()%>'
						maxLength='<%= sv.genlcdex10.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex10)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex10).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<input name='genlcdex20' type='text'
						<%if((sv.genlcdex20).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex20.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex20.getLength()%>'
						maxLength='<%= sv.genlcdex20.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex20)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex20).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex20).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%}%>>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<input name='genlcdex30' type='text'
						<%if((sv.genlcdex30).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <% }%>
						<%

		formatValue = (sv.genlcdex30.getFormData()).toString();

%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%= sv.genlcdex30.getLength()%>'
						maxLength='<%= sv.genlcdex30.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(genlcdex30)'
						onKeyUp='return checkMaxLength(this)'
						<% 
	if((new Byte((sv.genlcdex30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
						readonly="true" class="output_cell"
						<%
	}else if((new Byte((sv.genlcdex30).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
						class="bold_cell" <%
	}else { 
%>
						class=' <%=(sv.genlcdex30).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex30).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
						<%
	} 
%>>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>