<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sjl67";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%
	Sjl67ScreenVars sv = (Sjl67ScreenVars) fw.getVariables();
%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<div style="width: 70px;">
	        		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=formatValue%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<div style="width: 100px;">
	        		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=formatValue%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table>
		        		<tr>
		        			<td style="padding-right: 2px;">
				        		<%					
									if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=formatValue%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<%					
									if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=formatValue%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
	        	</div>
	        </div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Result Code")%></label>
			</div>
			
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Receive Error")%></label>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd01' type='text'
							<%formatValue = (sv.resultcd01.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd01.getLength()%>'
							maxLength='<%=sv.resultcd01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd01).getColor() == null ? "input_cell"
						: (sv.resultcd01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror01' type='text'
							<%formatValue = (sv.rcveror01.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror01.getLength()%>'
							maxLength='<%=sv.rcveror01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror01).getColor() == null ? "input_cell"
						: (sv.rcveror01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd02' type='text'
							<%formatValue = (sv.resultcd02.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd02.getLength()%>'
							maxLength='<%=sv.resultcd02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd02).getColor() == null ? "input_cell"
						: (sv.resultcd02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror02' type='text'
							<%formatValue = (sv.rcveror02.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror02.getLength()%>'
							maxLength='<%=sv.rcveror02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror02).getColor() == null ? "input_cell"
						: (sv.rcveror02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd03' type='text'
							<%formatValue = (sv.resultcd03.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd03.getLength()%>'
							maxLength='<%=sv.resultcd03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd03).getColor() == null ? "input_cell"
						: (sv.resultcd03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror03' type='text'
							<%formatValue = (sv.rcveror03.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror03.getLength()%>'
							maxLength='<%=sv.rcveror03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror03).getColor() == null ? "input_cell"
						: (sv.rcveror03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd04' type='text'
							<%formatValue = (sv.resultcd04.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd04.getLength()%>'
							maxLength='<%=sv.resultcd04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd04).getColor() == null ? "input_cell"
						: (sv.resultcd04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror04' type='text'
							<%formatValue = (sv.rcveror04.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror04.getLength()%>'
							maxLength='<%=sv.rcveror04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror04).getColor() == null ? "input_cell"
						: (sv.rcveror04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd05' type='text'
							<%formatValue = (sv.resultcd05.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd05.getLength()%>'
							maxLength='<%=sv.resultcd05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd05).getColor() == null ? "input_cell"
						: (sv.resultcd05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror05' type='text'
							<%formatValue = (sv.rcveror05.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror05.getLength()%>'
							maxLength='<%=sv.rcveror05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror05).getColor() == null ? "input_cell"
						: (sv.rcveror05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd06' type='text'
							<%formatValue = (sv.resultcd06.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd06.getLength()%>'
							maxLength='<%=sv.resultcd06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd06).getColor() == null ? "input_cell"
						: (sv.resultcd06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror06' type='text'
							<%formatValue = (sv.rcveror06.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror06.getLength()%>'
							maxLength='<%=sv.rcveror06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror06).getColor() == null ? "input_cell"
						: (sv.rcveror06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		 <div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd07' type='text'
							<%formatValue = (sv.resultcd07.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd07.getLength()%>'
							maxLength='<%=sv.resultcd07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd07).getColor() == null ? "input_cell"
						: (sv.resultcd07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror07' type='text'
							<%formatValue = (sv.rcveror07.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror07.getLength()%>'
							maxLength='<%=sv.rcveror07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror07).getColor() == null ? "input_cell"
						: (sv.rcveror07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd08' type='text'
							<%formatValue = (sv.resultcd08.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd08.getLength()%>'
							maxLength='<%=sv.resultcd08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd08).getColor() == null ? "input_cell"
						: (sv.resultcd08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror08' type='text'
							<%formatValue = (sv.rcveror08.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror08.getLength()%>'
							maxLength='<%=sv.rcveror08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror08).getColor() == null ? "input_cell"
						: (sv.rcveror08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='resultcd09' type='text'
							<%formatValue = (sv.resultcd09.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.resultcd09.getLength()%>'
							maxLength='<%=sv.resultcd09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(resultcd09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.resultcd09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.resultcd09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.resultcd09).getColor() == null ? "input_cell"
						: (sv.resultcd09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 60px">
						<input name='rcveror09' type='text'
							<%formatValue = (sv.rcveror09.getFormData()).toString();%>
							value='<%=formatValue%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rcveror09.getLength()%>'
							maxLength='<%=sv.rcveror09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(rcveror09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rcveror09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rcveror09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rcveror09).getColor() == null ? "input_cell"
						: (sv.rcveror09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
				</div>
			</div>
		</div>
		
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>