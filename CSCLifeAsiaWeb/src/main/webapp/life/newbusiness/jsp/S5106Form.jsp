<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5106";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>

<%
	S5106ScreenVars sv = (S5106ScreenVars) fw.getVariables();
%>
<%-- <%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk Comm. Date ");
%> --%>
<!-- ILJ-49 Starts -->
					<% StringData generatedText2 = null;
               		 if (sv.iljCntDteFlag.compareTo("Y") != 0){ 
               			generatedText2= resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk Comm. Date ");
        			} else { 
        				generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Date ");
        			} %>
        			<!-- ILJ-49 ends-->
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Register ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Branch ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Source of Business ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agency ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billing Method ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"First Billing Date ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Smoker ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Mortality Class ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Package Face Value/Units ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Adjustments ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Apply Cash ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.occdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.occdateDisp.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.occdateDisp, appVars.ind40);
		if (appVars.ind50.isOn()) {
			sv.occdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.occdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.register.setReverse(BaseScreenData.REVERSED);
			sv.register.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.register.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.register, appVars.ind48);
		if (!appVars.ind02.isOn()) {
			sv.register.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.srcebus.setReverse(BaseScreenData.REVERSED);
			sv.srcebus.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.srcebus.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.srcebus, appVars.ind49);
		if (!appVars.ind04.isOn()) {
			sv.srcebus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.dlvrmode.setReverse(BaseScreenData.REVERSED);
			sv.dlvrmode.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.dlvrmode.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind24.isOn()) {
			sv.dlvrmode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.agntsel, appVars.ind41);
		if (appVars.ind50.isOn()) {
			sv.agntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.agntname.setReverse(BaseScreenData.REVERSED);
			sv.agntname.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.agntname.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.ownersel.setReverse(BaseScreenData.REVERSED);
			sv.ownersel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.ownersel, appVars.ind42);
		if (appVars.ind50.isOn()) {
			sv.ownersel.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.ownersel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ownername.setReverse(BaseScreenData.REVERSED);
			sv.ownername.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ownername.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.mop.setReverse(BaseScreenData.REVERSED);
			sv.mop.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.mop, appVars.ind43);
		if (appVars.ind50.isOn()) {
			sv.mop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.mop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.billfreq, appVars.ind44);
		if (appVars.ind50.isOn()) {
			sv.billfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.billcdDisp.setReverse(BaseScreenData.REVERSED);
			sv.billcdDisp.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.billcdDisp, appVars.ind45);
		if (appVars.ind50.isOn()) {
			sv.billcdDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.billcdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.lifesel.setReverse(BaseScreenData.REVERSED);
			sv.lifesel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.lifesel, appVars.ind46);
		if (appVars.ind50.isOn()) {
			sv.lifesel.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.lifesel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.lifename.setReverse(BaseScreenData.REVERSED);
			sv.lifename.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.lifename.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.anbage.setReverse(BaseScreenData.REVERSED);
			sv.anbage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.anbage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.smoking.setReverse(BaseScreenData.REVERSED);
			sv.smoking.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.smoking.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.smoking, appVars.ind54);
		if (!appVars.ind17.isOn()) {
			sv.smoking.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.mortcls, appVars.ind47);
		if (!appVars.ind18.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.unitpkg.setReverse(BaseScreenData.REVERSED);
			sv.unitpkg.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.unitpkg.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind19.isOn()) {
			sv.unitpkg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind30.isOn()) {
			sv.prmamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.adjind.setReverse(BaseScreenData.REVERSED);
			sv.adjind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.adjind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.apcind.setReverse(BaseScreenData.REVERSED);
			sv.apcind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.apcind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.zagelit.setReverse(BaseScreenData.REVERSED);
			sv.zagelit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table><tr><td>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.descrip.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.descrip.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;max-width: 215px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
					</td>
					</tr></table>
				</div>
			</div>
			<!--  Ilife- Life Cross Browser - Sprint 1 D1 : Task 4 Start-->

			<!--  Ilife- Life Cross Browser - Sprint 1 D4 : Task 4 end-->
			<div class="col-md-4">
				<div class="form-group">
				<%-- 	<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Comm. Date")%></label> --%>
					<!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Comm. Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
					<div>
						<%
							longValue = sv.occdateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.occdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%
							if ((new Byte((sv.occdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| fw.getVariables().isScreenProtected()) {
						%>
						<input name='occdateDisp' type='text'
							value='<%=sv.occdateDisp.getFormData()%>'
							maxLength='<%=sv.occdateDisp.getLength()%>'
							size='<%=sv.occdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(occdateDisp)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">

						<%
							} else if ((new Byte((sv.occdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>

						<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('occdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
						<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
						</a> --%>
						<div class="input-group date form_date col-md-10" data-date=""
							data-date-format="dd/MM/yyyy" data-link-field="billcdDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.occdateDisp, (sv.occdateDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<%
							} else {
						%>

						<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('occdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
						<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
						</a> --%>
						<div class="input-group date form_date col-md-10" data-date=""
							data-date-format="dd/MM/yyyy" data-link-field="billcdDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.occdateDisp, (sv.occdateDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>

						<%
							}
							}
						%>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-1"> </div> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Register")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("register");
						optionValue = makeDropDownList(mappedItems, sv.register.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.register).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 160px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.register).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
						<%
							}
						%>

						<select name='register' type='list' style="width: 160px;"
							<%if ((new Byte((sv.register).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.register).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.register).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					<div class="input-group">
						<%
							if (!((sv.cntbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntbranch.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntbranch.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Source of Business")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "srcebus" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("srcebus");
						optionValue = makeDropDownList(mappedItems, sv.srcebus.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.srcebus).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.srcebus).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 190px;">
						<%
							}
						%>

						<select name='srcebus' type='list' style="width: 210px;"
							<%if ((new Byte((sv.srcebus).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.srcebus).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.srcebus).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
			<!-- <div class="col-md-1"></div> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Delivery Mode")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "dlvrmode" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("dlvrmode");
						optionValue = makeDropDownList(mappedItems, sv.dlvrmode.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.dlvrmode.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.dlvrmode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.dlvrmode).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 210px;">
						<%
							}
						%>

						<select name='dlvrmode' type='list' style="width: 225px;"
							<%if ((new Byte((sv.dlvrmode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.dlvrmode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.dlvrmode).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>


		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<table>
						<tr>
							<td>

  <div class="input-group"> 
						<%	
	longValue = sv.ownersel.getFormData();  
%>

<% 
	if((new Byte((sv.ownersel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="margin-right: 2px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
				
		        <div class="input-group" style="width: 125px;">
		            <%=smartHF.getRichTextInputFieldLookup(fw, sv.ownersel)%>
		             <span class="input-group-btn">
		             <button class="btn btn-info" type="button"
		                onClick="doFocus(document.getElementById('ownersel')); doAction('PFKEY04')">
		                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		            </button>
		        </div>
		           

<%} %>
							
							
							 </div></td>
							 <td >
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="min-width: 100px;max-width: 300px;margin-left: -2px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
					<table>
						<tr>
							<td>
							<%
                                         if ((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div style="margin-right: 2px;"><%=smartHF.getHTMLVarExt(fw, sv.agntsel)%> </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 125px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.agntsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %> 
 				
							
							</td>
							
							<td>
								<%
									if (!((sv.agntname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.agntname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.agntname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="min-width: 100px;max-width: 140px;margin-left: -2px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Method")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mop" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop");
						optionValue = makeDropDownList(mappedItems, sv.mop.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mop).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 180px;">
						<%
							}
						%>

						<select name='mop' type='list' style="width: 180px;"
							<%if ((new Byte((sv.mop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mop).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("billfreq");
						optionValue = makeDropDownList(mappedItems, sv.billfreq.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.billfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 140px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.billfreq).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='billfreq' type='list' style="width: 140px;"
							<%if ((new Byte((sv.billfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.billfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.billfreq).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("First Billing Date")%></label>

					<%
						longValue = sv.billcdDisp.getFormData();
					%>

					<%
						if ((new Byte((sv.billcdDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ((new Byte((sv.billcdDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<input name='billcdDisp' type='text'
						value='<%=sv.billcdDisp.getFormData()%>'
						maxLength='<%=sv.billcdDisp.getLength()%>'
						size='<%=sv.billcdDisp.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(billcdDisp)'
						onKeyUp='return checkMaxLength(this)' readonly="true"
						class="output_cell">

					<%
						} else if ((new Byte((sv.billcdDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>


					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="billcdDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.billcdDisp, (sv.billcdDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						} else {
					%>

					class = '
					<%=(sv.billcdDisp).getColor() == null ? "input_cell"
							: (sv.billcdDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					>

					
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="billcdDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.billcdDisp, (sv.billcdDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<%
						}
						}
					%>
				</div>
			</div>



		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					  <table><tr><td>  
				
						

<%
							longValue = sv.lifesel.getFormData();
						%>

						<%
							if ((new Byte((sv.lifesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="margin-right: 2px;min-width: 100px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						
                                         
                                         
                         
					        <div class="input-group" style="width: 125px;">
					            <%=smartHF.getRichTextInputFieldLookup(fw, sv.lifesel)%>
					             <span class="input-group-btn">
					             <button class="btn btn-info" type="button"
					                onClick="doFocus(document.getElementById('lifesel')); doAction('PFKEY04')">
					                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					            </button>
					        </div>
					                    

						<%
							}
						%>





			
			</td><td >
			 
			 
	
						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div 
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 100px;max-width: 300px;margin-left: -2px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
			   
					  </td></tr></table> 
				
			</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle((sv.zagelit.getFormData()).toString())%></label>
					<div class="input-group" style="min-width: 70px">




						<%
							qpsf = fw.getFieldXMLDef((sv.anbage).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.anbage);

							if (!((sv.anbage.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoker")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "smoking" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("smoking");
						optionValue = makeDropDownList(mappedItems, sv.smoking.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.smoking.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.smoking).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.smoking).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='smoking' type='list' style="width: 140px;"
							<%if ((new Byte((sv.smoking).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.smoking).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.smoking).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>

				<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mortcls");
						optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mortcls).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 210px;">
						<%
							}
						%>

						<select name='mortcls' type='list' style="width: 210px;"
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mortcls).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

		</div>


		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Package Face Value/Units")%></label>
					<div class="input-group" style="min-width: 100px">

						<%
							qpsf = fw.getFieldXMLDef((sv.unitpkg).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.unitpkg, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitpkg' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitpkg)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitpkg);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitpkg)%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitpkg.getLength(), sv.unitpkg.getScale(), 3)%>'
							maxLength='<%=sv.unitpkg.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(unitpkg)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitpkg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitpkg).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitpkg).getColor() == null ? "input_cell"
						: (sv.unitpkg).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>

			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">

					<%
						longValue = null;
						formatValue = null;
					%>


					<label><%=resourceBundleHandler.gettingValueFromBundle("Ttl Prem w/Tax")%></label>
					<div class="input-group" style="min-width: 100px">

						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>

			</div>
		</div>


	</div>
</div>


<Div id='mainForm_OPTS' style='display: none'>

	<table>
		<tr>
			<td width='188' style='text-align: center; font-size: 15px;'>


				<%
					if (!(sv.adjind.getInvisible() == BaseScreenData.INVISIBLE
							|| sv.adjind.getEnabled() == BaseScreenData.DISABLED)) {
				%>






				<div style="height: 15 px">
					<div id='null'>
						<a href="javascript:;"
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("adjind"))'
							class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Adjustments")%>
						</a>
					</div>
				</div> <%
 	}
 %> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<div>

					<input name='adjind' id='adjind' type='hidden'
						value="<%=sv.adjind.getFormData()%>">
				</div>
			</td>
		</tr>




		<tr >
			<td width='188' style='text-align: center; font-size: 15px;'> <%
 	if (!(sv.apcind.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.apcind.getEnabled() == BaseScreenData.DISABLED)) {
 %>





				<div style="height: 15 px">
					<div id='null'>
						<a href="javascript:;"
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("apcind"))'
							class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Apply Cash")%>
						</a>
					</div>
				</div> <%
 	}
 %> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<div>
					<input name='apcind' id='apcind' type='hidden'
						value="<%=sv.apcind.getFormData()%>">
				</div>
			</td>
		</tr>
	</table>



</Div>

<%@ include file="/POLACommon2NEW.jsp"%>

