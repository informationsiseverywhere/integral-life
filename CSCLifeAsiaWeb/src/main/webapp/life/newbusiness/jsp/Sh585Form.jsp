

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH585";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh585ScreenVars sv = (Sh585ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>

<%{
		appVars.rolldown();
		appVars.rollup();
	}

	%>
<style>
.input-group-addon{
 width:100%;
}
</style>

<div class="panel panel-default">

<div class="panel-body">
    	
    	  <div class="row"> 
    	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
               <input class="form-control" type="text" style="width:50px;" placeholder=<%=sv.company.getFormData().toString()%> disabled>
              </div>
            </div>
            
            <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
               <input class="form-control" type="text" style="width:60px;" placeholder=<%=sv.tabl.getFormData().toString()%> disabled>
              </div>
            </div>

	         <div class="col-md-4">
    	      <div class="form-group">
    	       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
    	        <table>
	    	       <tr>
	    	       	<td>
	    	       		 <%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
	    	       	</td>
	    	       	<td max-width="400px">
	    	       		<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc) %>
	    	       	</td>
	    	       </tr>    	       
    	       	</table>  		       

    	      </div>
    	     </div>
    	     
    	     </div>
    	     
    	     <div class="row"></div>
    	     <div class="row"></div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote01' 
type='text'

<%

		formatValue = (sv.hxclnote01.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote01.getLength()%>'
maxLength='<%= sv.hxclnote01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote01).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote02' 
type='text'

<%

		formatValue = (sv.hxclnote02.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote02.getLength()%>'
maxLength='<%= sv.hxclnote02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote02).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote03' 
type='text'

<%

		formatValue = (sv.hxclnote03.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote03.getLength()%>'
maxLength='<%= sv.hxclnote03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote03).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote04' 
type='text'

<%

		formatValue = (sv.hxclnote04.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote04.getLength()%>'
maxLength='<%= sv.hxclnote04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote04)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote04).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote05' 
type='text'

<%

		formatValue = (sv.hxclnote05.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote05.getLength()%>'
maxLength='<%= sv.hxclnote05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote05)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote05).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

<div class="row">
<div class="col-md-8">
<input name='hxclnote06' 
type='text'

<%

		formatValue = (sv.hxclnote06.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hxclnote06.getLength()%>'
maxLength='<%= sv.hxclnote06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote06)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hxclnote06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hxclnote06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hxclnote06).getColor()== null  ? 
			"input_cell" :  (sv.hxclnote06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
<div class="col-md-4"></div>
</div>

</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

