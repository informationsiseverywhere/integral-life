<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5004FormPart";%>

<%@page import="com.csc.life.newbusiness.screens.S5004ScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.quipoz.framework.util.AppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>



<%-- Start preparing data --%>
<%
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;
String formatValue = null;
QPScreenField qpsf = null;
String valueThis = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();

LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

LifeAsiaAppVars appVars = av;

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), request.getLocale());

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%S5004ScreenVars sv = (S5004ScreenVars) fw.getVariables();%>

<%!
public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}

public String formatValue(String aValue) {
	return aValue;
}
%>

<div class="row">
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						//ILIFE-808
						optionValue = makeDropDownList( mappedItems , sv.cntcurr.getFormData() ,2,resourceBundleHandler );
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
						%>
						<%
							if((new Byte((sv.cntcurr).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell' style="width: 170px;">
						   <%=longValue%>
						</div>
						<%longValue = null;%>
							<% }else {%>
						<select name='cntcurr' type='list'
						<%
							if((new Byte((sv.cntcurr).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							disabled
							class="output_cell"
						<%}else if((new Byte((sv.cntcurr).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
								<%}else {%>
							class = ' <%=(sv.cntcurr).getColor()== null  ?
									"input_cell" :  (sv.cntcurr).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
						<%=optionValue%>
						</select>
						<%}%>
				    </div>
				 </div>
				 
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Currency")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"billcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("billcurr");
						//ILIFE-808
						optionValue = makeDropDownList( mappedItems , sv.billcurr.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.billcurr.getFormData()).toString().trim());
						%> <%if((new Byte((sv.billcurr).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
						<div class='output_cell' style="width: 170px;">
						   <%=longValue%>
						</div>
						<%longValue = null;%>
							<% }else {%>
						<select name='billcurr' type='list'
						<%if((new Byte((sv.billcurr).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							disabled
							class="output_cell"
						<%}else if((new Byte((sv.billcurr).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
						<%}else {%>
							class = ' <%=(sv.billcurr).getColor()== null  ?
									"input_cell" :  (sv.billcurr).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
						<%=optionValue%>
						</select>
						<%}%>
				    </div>
				 </div>
				 
				<div class="col-md-4"> 
				    <div class="form-group" style="width:220px">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Non-fft opt")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"znfopt"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("znfopt");
						optionValue = makeDropDownList( mappedItems , sv.znfopt.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.znfopt.getFormData()).toString().trim());
						%><%
							if((new Byte((sv.znfopt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
													"blank_cell" : "output_cell" %>'>
							   		<%if(longValue != null){%>
							   		<%=longValue%>
							   		<%}%>
							   </div>
						<!-- ENDS -->
						<%
						longValue = null;
						%><% }else {%>
						<select name='znfopt' type='list'
						<%
							if((new Byte((sv.znfopt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
							readonly="true"
							disabled
							class="output_cell"
						<%}else if((new Byte((sv.znfopt).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
						<%}else {%>
							class = ' <%=(sv.znfopt).getColor()== null  ?
									"input_cell" :  (sv.znfopt).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
						<%=optionValue%>
						</select>
						<%}%>
				    </div>
				 </div> 
			</div>
			
			<div class="row">
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Cross Ref.Type")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reptype"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reptype");
						optionValue = makeDropDownList( mappedItems , sv.reptype.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.reptype.getFormData()).toString().trim());
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {longValue = formatValue(longValue);}
						%><%
							if((new Byte((sv.reptype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell'>
						   <%=longValue%>
						</div><%
						longValue = null;
						%><% }else {%>
						<select name='reptype' type='list'
						<%if((new Byte((sv.reptype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							disabled
							class="output_cell"
						<%}else if((new Byte((sv.reptype).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
						<%}else {%>
							class = ' <%=(sv.reptype).getColor()== null  ?
									"input_cell" :  (sv.reptype).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
						<%=optionValue%>
						</select>
						<%}%>
				    </div>
				 </div>
				 
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Number")%></label>
				    	<input name='lrepnum'
						type='text'
						<%formatValue = (sv.lrepnum.getFormData()).toString();%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						size='<%= sv.lrepnum.getLength()%>'
						maxLength='<%= sv.lrepnum.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(lrepnum)' onKeyUp='return checkMaxLength(this)'
						<%if((new Byte((sv.lrepnum).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							class="output_cell"
						<%}else if((new Byte((sv.lrepnum).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
						<%}else {%>
							class = ' <%=(sv.lrepnum).getColor()== null  ?
									"input_cell" :  (sv.lrepnum).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
				    </div>
				 </div>
				 
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Income Source")%></label>
				    	<%  qpsf = fw.getFieldXMLDef((sv.incomeSeqNo).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);  %>
						<input name='incomeSeqNo'
						type='text' style="width: 70px;"
						value='<%=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo) %>'
						<%
						valueThis=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo);
						if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.incomeSeqNo) %>'
						<%}%>
						size='<%= sv.incomeSeqNo.getLength()%>'
						maxLength='<%= sv.incomeSeqNo.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(incomeSeqNo)' onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<% if((new Byte((sv.incomeSeqNo).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
							readonly="true"
							class="output_cell"
						<%}else if((new Byte((sv.incomeSeqNo).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
								class="bold_cell"
						<%}else {%>
							class = ' <%=(sv.incomeSeqNo).getColor()== null  ?
									"input_cell" :  (sv.incomeSeqNo).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
				    </div> </div> </div>
				    
			<div class="row">
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Register")%></label>
					<%
					if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
					mappedItems = (Map) fieldItem.get("register");
					optionValue = makeDropDownList( mappedItems , sv.register.getFormData(),2,resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
					%><%=smartHF.getDropDownExt(sv.register, fw, longValue, "register", optionValue, 0) %>
					<%}%>
					</div> </div>
				 <div class="col-md-4"> 
				    <div class="form-group" style="width: 220px;">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Source")%></label>
				    	<%
							fieldItem=appVars.loadF4FieldsLong(new String[] {"srcebus"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("srcebus");
							optionValue = makeDropDownList( mappedItems , sv.srcebus.getFormData(),2,resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());
						%><%if((new Byte((sv.srcebus).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
						<div class='output_cell'>
						   <%=longValue%>
						</div>
						<%
						longValue = null;
						%><% }else {%>
							<%=smartHF.getDropDownExt(sv.srcebus, fw, longValue, "srcebus", optionValue, 0) %>
						<%  } %>
				    </div>
				 </div>
				
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing")%></label>
				    	<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"sbrdesc"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("sbrdesc");
						longValue = (String) mappedItems.get((sv.sbrdesc.getFormData()).toString());
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
						
							<div style="width: 70px;"
							class='<%= (sv.sbrdesc.getFormData()).trim().length() == 0 ?
											"blank_cell" : "output_cell" %>'
											>
						<%
						if(!((sv.sbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.sbrdesc.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						
						
									} else  {
						
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.sbrdesc.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						}%>
								<%=formatValue%>
							</div>
						<%
						longValue = null;
						formatValue = null;
						%>
				    </div>
				 </div> 
			</div>
			<div class="row">
				<div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Application Form Number")%></label>
				    	<input name='ttmprcno'
						type='text'
						
						<% formatValue = (sv.ttmprcno.getFormData()).toString(); %>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						size='<%= sv.ttmprcno.getLength()%>'
						maxLength='<%= sv.ttmprcno.getLength()%>' 
						onFocus="doFocus(this);" onHelp='return fieldHelp(ttmprcno)' onKeyUp='return checkMaxLength(this)'  

						<%  if((new Byte((sv.ttmprcno).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
							readonly="true"
							class="output_cell"
						<% }else if((new Byte((sv.ttmprcno).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){ %>
								class="bold_cell"
						
						<% }else {%>
							class = ' <%=(sv.ttmprcno).getColor()== null  ?
									"input_cell" :  (sv.ttmprcno).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						<%}%>>
				    </div>
				 </div>
			
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Application Form Date")%></label>
				    	<% if((new Byte((sv.ttmprcdteDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.ttmprcdteDisp,(sv.ttmprcdteDisp.getLength()))%>
							<% }else{%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.ttmprcdteDisp,(sv.ttmprcdteDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<% }%>
				    </div>  </div>
				    
				<div class="col-md-4">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Codes")%></label>
					<table>
						<tr class="form-group">
							
								<td style="padding-left: 0px;">
										<div class="input-group" style="display: inline-flex !important;">
											<% if ((new Byte((sv.stcal).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
													fieldItem = appVars.loadF4FieldsLong(new String[]{"stcal"}, sv, "E", baseModel);
													mappedItems = (Map) fieldItem.get("stcal");
													optionValue = makeDropDownList(mappedItems, sv.stcal.getFormData(), 2, resourceBundleHandler);
													longValue = (String) mappedItems.get((sv.stcal.getFormData()).toString().trim());
											%>
		
											<% if ((new Byte((sv.stcal).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
											%>
											<div style="width: 40px;"
												class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
												<% if (longValue != null) { %>
												<%=longValue%>
												<% } %>
											</div>
											<% longValue = null; %>
											<% } else { %>
											<%=smartHF.getDropDownExt(sv.stcal, fw, longValue, "stcal", optionValue, 0)%>
											<% } %><%}	%>
											<span style="padding: 3px;"><%=resourceBundleHandler.gettingValueFromBundle("/")%></span>
										</div> </td>
								<td 
									style="padding-right: 3px; padding-left: 0px;left: -15px;">
										<div class="input-group" style="display: inline-flex !important;">
											<% if ((new Byte((sv.stcbl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
													fieldItem = appVars.loadF4FieldsLong(new String[]{"stcbl"}, sv, "E", baseModel);
													mappedItems = (Map) fieldItem.get("stcbl");
													optionValue = makeDropDownList(mappedItems, sv.stcbl.getFormData(), 2, resourceBundleHandler);
													longValue = (String) mappedItems.get((sv.stcbl.getFormData()).toString().trim());
											%>
		
											<% if ((new Byte((sv.stcbl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) { %>
											<div style="width: 40px;"
												class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
												<% if (longValue != null) { %>
												<%=longValue%>
												<% }%>
											</div>
		
											<% longValue = null; %>
		
											<% } else { %>
											<%=smartHF.getDropDownExt(sv.stcbl, fw, longValue, "stcbl", optionValue, 0)%>
											<% } } %>
											<span style="padding: 3px;"><%=resourceBundleHandler.gettingValueFromBundle("/")%></span>
										</div>
								</div>
								<td style="padding-right: 3px; padding-left: 0px;">
										<div class="input-group" style="display: inline-flex !important;">
											<% if ((new Byte((sv.stccl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
													fieldItem = appVars.loadF4FieldsLong(new String[]{"stccl"}, sv, "E", baseModel);
													mappedItems = (Map) fieldItem.get("stccl");
													optionValue = makeDropDownList(mappedItems, sv.stccl.getFormData(), 2, resourceBundleHandler);
													longValue = (String) mappedItems.get((sv.stccl.getFormData()).toString().trim());
											%>
		
											<%   if ((new Byte((sv.stccl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) { %>
											<div style="width: 40px;"
												class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
												<% if (longValue != null) { 	%>
 												<%=longValue%>
 												<% } %>
											</div>
		
											<% longValue = null; %> <% } else { %>
											<%=smartHF.getDropDownExt(sv.stccl, fw, longValue, "stccl", optionValue, 0)%>
											<% }%><% } %>
											<span style="padding: 3px;"><%=resourceBundleHandler.gettingValueFromBundle("/")%></span>
										</div>
								</td>
								<td style="padding-left: 0px;">
										<div class="input-group">
									<%
										if ((new Byte((sv.stcdl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"stcdl"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("stcdl");
											optionValue = makeDropDownList(mappedItems, sv.stcdl.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.stcdl.getFormData()).toString().trim());
									%>
		
									<%
										if ((new Byte((sv.stcdl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div style="width: 40px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%
											if (longValue != null) {
										%>
		
										<%=longValue%>
		
										<%
											}
										%>
									</div>		
									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<%=smartHF.getDropDownExt(sv.stcdl, fw, longValue, "stcdl", optionValue, 0)%>
									<%
										}
									%>
									<%
										}
									%>
									</div>
								</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4"> 
				<div >				
				   
				  <label><%=resourceBundleHandler.gettingValueFromBundle("Agency")%></label>
				  
					<table>
					<tr>
					<td>
					<div class="form-group">
					
						<%  if ((new Byte((sv.agntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>                   
						
						<div style="margin-right: 2px">	<%=smartHF.getHTMLVarExt(fw, sv.agntsel)%></div>			
				                                         
				        <% } else { %>	
						
						  <div class="input-group" style="width: 125px;" id="baseissurance1">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.agntsel)%>
				             <span class="input-group-btn">
				             <button class="btn btn-info"
				                
				                type="button"
				                onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
						
						<div  id="baseissurance"
							class='<%= (sv.agntsel.getFormData()).trim().length() == 0 ?
											"blank_cell" : "output_cell" %>'
										style="width:110px;margin-top: 5px;display:none;">
						<%   if(!((sv.agntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agntsel.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						     } else  {
  									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agntsel.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						    } %>
								<%=formatValue%>
							</div>	
				        <%  }   %> 
				        </div>       </td> <td>
			
					<div   class='<%= (sv.agentname.getFormData()).trim().length() == 0 ?
											"blank_cell" : "output_cell" %>'
										style="width:133px;margin-left: -1px;">
						<% if(!((sv.agentname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
									} else  {
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agentname.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						} %>
								<%=formatValue%>
							</div>
						<% longValue = null;
						   formatValue = null; %>
							
					</td>      </tr>      </table>					</div>		</div>
						
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Campaign")%></label>
				    	<% fieldItem=appVars.loadF4FieldsLong(new String[] {"campaign"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("campaign");
							optionValue = makeDropDownList( mappedItems , sv.campaign.getFormData(),2,resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.campaign.getFormData()).toString().trim());
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
						%>
						
						<% if((new Byte((sv.campaign).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell'>
						   <%=longValue%>
						</div>
						
						<% longValue = null; %> <% }else {%>
							<%=smartHF.getDropDownExt(sv.campaign, fw, longValue, "campaign", optionValue, 0) %>
						<% }%>
				    </div>
				 </div>

				 <div class="col-md-4"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Auto. Increase")%></label>
                       <!--  <br/> --> <!-- ILIFE-6982 -->
                        <input type='checkbox' name='indxflg' value='Y'  onFocus='doFocus(this)' 
                            onHelp='return fieldHelp(indxflg)' onKeyUp='return checkMaxLength(this)'
                            
                            <%
                            if((sv.indxflg).getColor()!=null){
                                         %>style='background-color:#FF0000;'
                                    <%}
		if((sv.indxflg).toString().trim().equalsIgnoreCase("C") || (("Y").equalsIgnoreCase((sv.indxflg).toString().trim()))){ /* ILIFE-7145 */
                                        %>checked
  <% }else if((sv.indxflg).toString().trim().equalsIgnoreCase("U")){    	  
      %>unchecked<%
      }
		if((sv.indxflg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){
      %>
                                       disabled  <%}%>
                            class ='UICheck' onclick="handleCheckBox('indxflg')"/>
                            
                            <input type='checkbox' name='indxflg' value=' '
                            
                            <% if(!(sv.indxflg).toString().trim().equalsIgnoreCase("Y")){
                                        %>checked
      <%  }%>
                            style="visibility: hidden" onclick="handleCheckBox('indxflg')"/>
                    </div> </div> </div>
			<div class="row">
			
			<!--ICIL-147 start  -->
				<% if ((new Byte((sv.bnkout).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Outlet")%></label>
					<table>
						<tr>
							<td style="min-width: 150px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkout, true)%></td>
							<td style="padding-left: 1px; width: 170px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkoutname, true).replace("<pre></pre>", "")%></td>
						</tr> </table> </div>	</div>
			<%} %>
				<!--ICIL-147 end  -->
				
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
				    	<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("reqntype");
							optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						} %>
						
						<% if((new Byte((sv.reqntype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  %>  
						<div class='output_cell'> 
						   <%=longValue%>
						</div>
						
						<% longValue = null; %>
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.reqntype, fw, longValue, "reqntype", optionValue, 0) %>
						<% } %>  
				    </div>
				 </div>
				                 
                 
                 <div class="col-md-4"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("No Lapse Guarantee")%></label>
                            <%  if ((new Byte((sv.nlgflg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
                                if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
                                    longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
                                }
                                if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
                                    longValue=resourceBundleHandler.gettingValueFromBundle("No");
                                }
                            %>
                            <% if((new Byte((sv.nlgflg).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 
                                   || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
                            %>  
                              <div class='<%= ((longValue == null) 
                                      || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>' style="max-width:150px;">  
                                    <%if(longValue != null){%>
                                        <%=longValue%>
                                    <%}%>
                               </div>
                            <%   longValue = null;  %>
                            <% }else {%>
                                <% if("red".equals((sv.nlgflg).getColor())){%>
                                  <div style="border:1px; border-style: solid; border-color: #B55050;"> 
                                <%} %>
                            <select name='nlgflg'
                                onFocus='doFocus(this)'
                                onHelp='return fieldHelp(nlgflg)'
                                onKeyUp='return checkMaxLength(this)'
                            <% 
                                if((new Byte((sv.nlgflg).getEnabled()))
                                .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                            %>  
                                readonly="true"
                                disabled
                                class="output_cell"
                            <%
                                }else if((new Byte((sv.nlgflg).getHighLight())).
                                    compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                            %>  
                                    class="bold_cell" 
                            <%}else { %>
                                class = 'input_cell' 
                            <% } %> >
                                <option value="Y"<% if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
                                <option value="N"<% if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
                            </select>
	                            <% if("red".equals((sv.nlgflg).getColor())) {%>
	                              </div>
	                            <%} %>
                            <%}%>
                        <%}%>
                        <%longValue = null;%>
                    </div>  
                 </div> 
                 
                 <!--  IBPLIFE-2264 : Starts -->
                <% if ((new Byte((sv.rskComOpt).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %> 
                 <div class="col-md-4"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Option")%></label>
                        <input type='checkbox' name='rskComOpt' value='Y'  onFocus='doFocus(this)' 
                            onHelp='return fieldHelp(rskComOpt)' onKeyUp='return checkMaxLength(this)'
                            
                            <%
                            if((sv.rskComOpt).getColor()!=null){
                                         %>style='background-color:#FF0000; outline: 2px solid #c00;' 
                                    <%}
		if((sv.flag).toString().trim().equalsIgnoreCase("Y")){ 
                                        %>checked
  <% }else if((sv.flag).toString().trim().equalsIgnoreCase("O") || (sv.flag).toString().trim().equalsIgnoreCase("N")){    	  
      %>unchecked<%
      }%>
        <%if((sv.rskComOpt).toString().trim().equalsIgnoreCase("Y")){ 
            %>checked
<% }else if((sv.rskComOpt).toString().trim().equalsIgnoreCase("N")){    	  
%>unchecked<%
} %>
		<%if((sv.rskComOpt).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){
      %>
                                       disabled  
                                       <%}%>
                            class ='UICheck' onclick="handleCheckBox('rskComOpt')"/>
                            
                            <input type='checkbox' name='rskComOpt' value=' '
                            
                            <% if(!(sv.rskComOpt).toString().trim().equalsIgnoreCase("Y")){
                                        %>checked
      <%  }%>
                            style="visibility: hidden" onclick="handleCheckBox('')"/>
                    </div> </div>
                     <%  }%>
                    <!-- IBPLIFE-2264 : Ends -->
                 <!-- ILIFE-8583 : Starts -->
                 <% if ((new Byte((sv.custrefnum).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Customer Reference Number")%></label>
					<table>
						<tr>							
							<td style="padding-left: 1px; min-width: 170px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.custrefnum, true)%></td>
						</tr> </table> </div>	</div>
			       <%} %> 
			     <!-- ILIFE-8583 : Ends -->                  
              </div>
                 <div class="row">             
                 
			<!--ICIL-147 start  -->
			 
				<% if ((new Byte((sv.bnktel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label>
					<table>
						<tr>
							<td><input name='bnksm' id='bnksm' type='text'
								style="min-width: 150px;" value='<%=sv.bnksm%>'
								maxLength='<%=sv.bnksm.getLength()%>'
								size='<%=sv.bnksm.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bnksm)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control"></td>

							<td>
							<td>
								<%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label> --%>
								<input name='bnksmname' id='bnksmname' style="margin-left: 1px;"
								type='text' value='<%=sv.bnksmname%>'
								maxLength='<%=sv.bnksmname.getLength()%>'
								size='<%=sv.bnksmname.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bnksm)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control">
							</td> </tr> </table> </div> </div> <%} %>
				<!--ICIL-147 end  -->
                 
                 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Delivery Mode")%></label>
				    	<%
							fieldItem=appVars.loadF4FieldsLong(new String[] {"dlvrmode"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("dlvrmode");
							optionValue = makeDropDownList( mappedItems , sv.dlvrmode.getFormData(),2,resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.dlvrmode.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						} %>
						
						<% if((new Byte((sv.dlvrmode).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ %>
						<div class='output_cell'>
						   <%=longValue%>
						</div>
						
						<% longValue = null; %>
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.dlvrmode, fw, longValue, "dlvrmode", optionValue, 0) %>
						<% } %>
				    </div>  </div>
				
		
			  <%	if ((new Byte((sv.billday).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                <div class="col-md-4"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Billing day")%></label>
                        <%longValue=(sv.billday.getFormData()).toString().trim();
						    if((new Byte((sv.billday).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						    	||(((ScreenModel) fw).getVariables().isScreenProtected())) { 
						%>  
						  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						                            "blank_cell" : "output_cell" %>' style="width: 100px;">  
						            <%if(longValue != null){%>
						            
						            <%=longValue%>
						            
						            <%}%>
						       </div>
						<% longValue = null;%>
						<% }else {%>
							<% if("red".equals((sv.billday).getColor())){%>
							<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
							<%} %>
							<select name='billday'    
							    onFocus='doFocus(this)'
							    onHelp='return fieldHelp(billday)'
							    onKeyUp='return checkMaxLength(this)'
							<% 
							    if((new Byte((sv.billday).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
							    readonly="true"
							    disabled
							    class="output_cell"
							<%
							    }else if((new Byte((sv.billday).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>  
							        class="bold_cell" 
							<%  }else { %>
							    class = 'input_cell' 
							<%  } %>>
							<option value="">--------<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--------</option>
							<option value="01"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("01")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("01")%></option>
							<option value="02"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("02")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("02")%></option>
							<option value="03"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("03")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("03")%></option>
							<option value="04"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("04")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("04")%></option>
							<option value="05"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("05")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("05")%></option>
							<option value="06"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("06")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("06")%></option>
							<option value="07"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("07")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("07")%></option>
							<option value="08"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("08")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("08")%></option>
							<option value="09"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("09")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("09")%></option>
							<option value="10"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("10")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("10")%></option>
							<option value="11"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("11")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("11")%></option>
							<option value="12"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("12")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("12")%></option>
							<option value="13"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("13")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("13")%></option>
							<option value="14"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("14")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("14")%></option>
							<option value="15"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("15")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("15")%></option>
							<option value="16"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("16")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("16")%></option>
							<option value="17"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("17")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("17")%></option>
							<option value="18"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("18")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("18")%></option>
							<option value="19"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("19")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("19")%></option>
							<option value="20"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("20")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("20")%></option>
							<option value="21"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("21")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("21")%></option>
							<option value="22"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("22")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("22")%></option>
							<option value="23"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("23")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("23")%></option>
							<option value="24"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("24")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("24")%></option>
							<option value="25"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("25")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("25")%></option>
							<option value="26"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("26")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("26")%></option>
							<option value="27"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("27")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("27")%></option>
							<option value="28"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("28")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("28")%></option>
							<option value="29"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("29")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("29")%></option>
							<option value="30"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("30")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("30")%></option>
							<option value="31"<% if(((sv.billday.getFormData()).toString()).trim().equalsIgnoreCase("31")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("31")%></option>
							</select>
							<% if("red".equals((sv.billday).getColor())){%>
							    </div> <%} %>
							<%  longValue = null;%> <%} %>
					</div> </div> <%} %></div>
			    
			     <div class="row">
			     
			         <!--ICIL-147 start  -->
           
				<%
				if ((new Byte((sv.bnktel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Teller")%></label>
					<table>
						<tr>
							<td style="width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktel, true)%></td>
							<td style="margin-left: 1px;width: 170px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktelname, true).replace("<pre></pre>", "")%></td>
						</tr>
					</table>
				</div>
			</div>
			<%} %>
				
			<!--ICIL-147 end  -->
			    <!-- fwang3 China localization start -->
				<!-- ICIL-658 -->
          	 	<%
	           	if (sv.rfundflg.getInvisible() != BaseScreenData.INVISIBLE) {
			 	%>
				<div class="col-md-4"> 
				 	<div class="form-group" style="max-width: 332px;">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Refund_Overpay")%></label>
						<%
							mappedItems = new HashMap<String,String>();
							mappedItems.put("Y", resourceBundleHandler.gettingValueFromBundle("Yes"));
							mappedItems.put("N", resourceBundleHandler.gettingValueFromBundle("No"));
							optionValue = makeDropDownList(mappedItems,sv.refundOverpay.getFormData(),2,resourceBundleHandler);
							longValue = (String) mappedItems.get(sv.refundOverpay.getFormData().trim());
						%>				    	
						<%
							if((new Byte((sv.refundOverpay).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 
								|| fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell' style="width: 175px;">
							<%=longValue==null?"":longValue%>
						</div>
						<% } else { %>
							<% if("red".equals((sv.refundOverpay).getColor())){ %>
							<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;">
							<% } %>
							<select name='refundOverpay' id='refundOverpay' type='list' 
							   	onFocus='doFocus(this)'
                                onHelp='return fieldHelp(refundOverpay)'
                                onKeyUp='return checkMaxLength(this)'
							<% if((new Byte((sv.refundOverpay).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){ %>
								class="bold_cell"
							<% } else { %>
								class='input_cell'
							<% } %>
							>
							<%=optionValue%>
							</select>
							<% if("red".equals((sv.refundOverpay).getColor())){ %>
							</div>
							<% } %>
						<% } %>
						<%
							mappedItems = null;
							optionValue = null;
							longValue=null;
						%>
				 	</div>
				</div>
				 <%
				 }
				 %>
				<!-- fwang3 China localization end --> 
			    <div class="col-md-4">
			   
				 <%if ((new Byte((sv.fatcastatus).getInvisible())).compareTo(new Byte(
                     BaseScreenData.INVISIBLE)) != 0) { %>
                 
                 <div class="col-md-4"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("FATCA Status")%></label>
                        <div class="input-group">
                        <span class="input-group-addon" name='fatcastatus' id='fatcastatus'>
                            <%=sv.fatcastatus.getFormData()%>
                         </span>
                         <input name='fatcastatusdesc' id='fatcastatusdesc'
                            type='text'
                            value='<%=sv.fatcastatusdesc.getFormData()%>'
                            onFocus='doFocus(this)' onHelp='return fieldHelp(fatcastatusdesc)' onKeyUp='return checkMaxLength(this)'
                            <%-- MIBT-77 --%>
                            readonly="true"  class="form-control"/>
                         </div></div> </div><%} %>
            </div></div></div></div>
<!-- <div style='visibility:hidden;'><table> -->
<div style='display:none;'><table>
<tr style='height:22px;'>
<td>

<!-- Code for making the Anniversary as INVISIBLE -->
 if (appVars.ind58.isOn()) {
			generatedText45.setInvisibility(BaseScreenData.INVISIBLE);
		}
if (appVars.ind58.isOn()) {
			sv.aiind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.aiind.setEnabled(BaseScreenData.DISABLED);
		}
</td>

<td width='188'>&nbsp; &nbsp;<br/>

	<div class='<%= (sv.heading.getFormData()).trim().length() == 0 ?
					"blank_cell" : "output_cell" %>'>
<% if(!((sv.heading.getFormData()).toString()).trim().equalsIgnoreCase("")) {

					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.heading.getFormData()).toString());
					} else {
						formatValue = formatValue( longValue);
					}

			} else  {

			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.heading.getFormData()).toString());
					} else {
						formatValue = formatValue( longValue);
					}
}%>
		<%=formatValue%>
	</div>
<% longValue = null;
   formatValue = null;%>

</td>  <tr> <td>

<% if ((new Byte((sv.stcel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"stcel"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("stcel");
	optionValue = makeDropDownList( mappedItems , sv.stcel.getFormData(),2,resourceBundleHandler);
	longValue = (String) mappedItems.get((sv.stcel.getFormData()).toString().trim());
%>

<% if((new Byte((sv.stcel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
			   ||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
							"blank_cell" : "output_cell" %>'>
	   		<%if(longValue != null){%>

	   		<%=longValue%>

	   		<%}%>
	   </div>

<% longValue = null;
%>  <% }else {%>

<% if("red".equals((sv.stcel).getColor())){ %>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;">
<% } %>

<select name='stcel' type='list' style="width:140px;"
<% if((new Byte((sv.stcel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ %>
	readonly="true"
	disabled
	class="output_cell"
<% }else if((new Byte((sv.stcel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){ %>
		class="bold_cell"
<% }else { %>
	class = 'input_cell'
<% } %> >
<%=optionValue%>
</select>
<% if("red".equals((sv.stcel).getColor())){ %>
</div>
<%}%><%}} %> </td></tr> <tr> <td>

<div class='<%= (sv.premStatDesc.getFormData()).trim().length() == 0 ?
					"blank_cell" : "output_cell" %>'>
<% if(!((sv.premStatDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premStatDesc.getFormData()).toString());
					} else {
						formatValue = formatValue( longValue);
					}
} else  {
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.premStatDesc.getFormData()).toString());
					} else {
						formatValue = formatValue( longValue);
					}
}%>
		<%=formatValue%>
	</div>
<% longValue = null;
   formatValue = null; %>
</td></tr>

<td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Business")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Branch")%>
</div>

<br/>&nbsp; &nbsp; &nbsp;</td>
</tr>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>


<% if(sv.jownind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.jownind
.getEnabled()==BaseScreenData.DISABLED) { %>

Joint Owner
<% }
else { %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("jownind"))'
class="hyperLink">

 Joint Owner

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.jownind.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.jownind.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.jownind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='jownind' id='jownind' type='hidden'  value="<%=sv.jownind.getFormData()%>">
</div> </td></tr>
<!-- ALS-4555 starts -->
<tr>
<td width='188'>&nbsp; &nbsp;<br/>
<%    if(sv.zmultOwner.getInvisible()== BaseScreenData.INVISIBLE|| sv.zmultOwner.getEnabled()==BaseScreenData.DISABLED){ %>
<% } else{ %>

<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zmultOwner"))' 
class="hyperLink">

Multiple Owner

</a> </div>	<%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.zmultOwner.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.zmultOwner.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.zmultOwner'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%	 }%>
<div>
<input name='zmultOwner' id='zmultOwner' type='hidden'  value="<%=sv.zmultOwner.getFormData()%>">
</div>

</td></tr>
<!-- ALS-4555 ends -->

<tr style='height:22px;'><td width='188'>&nbsp;&nbsp;<br/>


<% if(sv.asgnind.getInvisible()== BaseScreenData.INVISIBLE|| sv.asgnind.getEnabled()==BaseScreenData.DISABLED){ %>

Assignee
<% } else{ %>

<div id='null'><a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("asgnind"))'
class="hyperLink">

 Assignee

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.asgnind.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.asgnind.getFormData().equals("X")) { %>
 <img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.asgnind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='asgnind' id='asgnind' type='hidden'  value="<%=sv.asgnind.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.apcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.apcind.getEnabled()==BaseScreenData.DISABLED){ %>

Apply cash
<% }else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("apcind"))'
class="hyperLink">

 Apply cash

</a> </div><%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.apcind.getFormData().equals("+")) {%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.apcind.getFormData().equals("X")) {%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.apcind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='apcind' id='apcind' type='hidden'  value="<%=sv.apcind.getFormData()%>">
</div> </td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.addtype
.getInvisible()== BaseScreenData.INVISIBLE|| sv.addtype
.getEnabled()==BaseScreenData.DISABLED){
%>

Despatch Address
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("addtype"))'
class="hyperLink">

 Despatch Address

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.addtype
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.addtype
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.addtype'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='addtype' id='addtype' type='hidden'  value="<%=sv.addtype
.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.payind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.payind
.getEnabled()==BaseScreenData.DISABLED){
%>

Payor
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payind"))'
class="hyperLink">

 Payor

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.payind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.payind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.payind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% }%>
<div>
<input name='payind' id='payind' type='hidden'  value="<%=sv.payind
.getFormData()%>">
</div> </td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<%   if(sv.comind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.comind
.getEnabled()==BaseScreenData.DISABLED){
%>

Commission
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))'
class="hyperLink">

 Commission

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%  if (sv.comind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.comind
.getFormData().equals("X")) { %>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.comind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='comind' id='comind' type='hidden'  value="<%=sv.comind
.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<%  if(sv.ddind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
.getEnabled()==BaseScreenData.DISABLED){
%>

Direct Debit
<% }else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))'
class="hyperLink">

 Direct Debit

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%  if (sv.ddind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.ddind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.ddind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='ddind' id='ddind' type='hidden'  value="<%=sv.ddind
.getFormData()%>">
</div> </td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED){
%>

Follow-ups
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))'
class="hyperLink">

 Follow-ups

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.fupflg
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
<%}
	if (sv.fupflg
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.fupflg'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg
.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.bnfying
.getInvisible()== BaseScreenData.INVISIBLE|| sv.bnfying
.getEnabled()==BaseScreenData.DISABLED){ %>

Beneficiaries
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bnfying"))'
class="hyperLink">

 Beneficiaries

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
if (sv.bnfying
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.bnfying
.getFormData().equals("X")) { %>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.bnfying'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='bnfying' id='bnfying' type='hidden'  value="<%=sv.bnfying
.getFormData()%>">
</div></td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.grpind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.grpind
.getEnabled()==BaseScreenData.DISABLED){
%>

Group Details
<% }else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("grpind"))'
class="hyperLink">

 Group Details

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.grpind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.grpind
.getFormData().equals("X")) { %>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.grpind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% }%>
<div>
<input name='grpind' id='grpind' type='hidden'  value="<%=sv.grpind
.getFormData()%>">
</div></td>

<td width='188'>&nbsp; &nbsp;<br/>

<% if(sv.aiind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.aiind.getEnabled()==BaseScreenData.DISABLED){ %>

Anniversary
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aiind"))'
class="hyperLink">

 Anniversary

</a>
</div>
<%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.aiind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="5">
<%}
	if (sv.aiind.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.aiind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/xicon.gif" border="5">;
<% } %>
<div>
<input name='aiind' id='aiind' type='hidden'  value="<%=sv.aiind.getFormData()%>">
</div> </td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.ctrsind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ctrsind
.getEnabled()==BaseScreenData.DISABLED){
%>

Trustee
<% }else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ctrsind"))'
class="hyperLink">

 Trustee

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%  if (sv.ctrsind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.ctrsind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.ctrsind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%    }  %>
<div>
<input name='ctrsind' id='ctrsind' type='hidden'  value="<%=sv.ctrsind
.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.transhist
.getInvisible()== BaseScreenData.INVISIBLE|| sv.transhist
.getEnabled()==BaseScreenData.DISABLED){  %>

Trans History
<% } else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("transhist"))'
class="hyperLink">

 Trans History

</a> </div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
if (sv.transhist
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.transhist
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.transhist'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='transhist' id='transhist' type='hidden'  value="<%=sv.transhist
.getFormData()%>">
</div> </td></tr>


<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
<%  if(sv.ind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ind
.getEnabled()==BaseScreenData.DISABLED){
%>

Policy Notes
<%  }
else{ %>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ind"))'
class="hyperLink">

 Policy Notes

</a></div> <%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
if (sv.ind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.ind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.ind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% } %>
<div>
<input name='ind' id='ind' type='hidden'  value="<%=sv.ind
.getFormData()%>">
</div> </td>


<td width='188'>&nbsp; &nbsp;<br/>
<%  if(sv.zsredtrm
.getInvisible()== BaseScreenData.INVISIBLE|| sv.zsredtrm
.getEnabled()==BaseScreenData.DISABLED){ %>

Reducing Term
<% }
else{%>

<div id='null'><a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zsredtrm"))'
class="hyperLink">

 Reducing Term

</a></div><%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
if (sv.zsredtrm
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.zsredtrm
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.zsredtrm'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<% }%>
<div>
<input name='zsredtrm' id='zsredtrm' type='hidden'  value="<%=sv.zsredtrm
.getFormData()%>">
</div>

</td></tr>

			<tr>
				<td><input name='zdpind' id='zdpind' type='hidden'
					value="<%=sv.zdpind.getFormData()%>"> <%
 	if (sv.zdpind.getFormData().equals("+")) {
 %> <img
					src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
					border="0"> <%
 	}
 	if (sv.zdpind.getFormData().equals("X")) {
 %> <img
					onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zdpind'))"
					style="cursor: pointer"
					src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
					border="0"> <%
 	}%></td>
				<td style="font-weight: bold; font-size: 12px; font-family: Arial">
					<% if (!(sv.zdpind.getInvisible() == BaseScreenData.INVISIBLE || sv.zdpind
								.getEnabled() == BaseScreenData.DISABLED)) { %>
								
					<div style="height: 15 px">
						<a href="javascript:;"
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zdpind"))'
							class="hyperLink"> <%=resourceBundleHandler
						.gettingValueFromBundle("Direct Credit")%>
						</a> </div> <%}%></td></tr><tr>
					
				<td><input name='zdcind' id='zdcind' type='hidden'
					value="<%=sv.zdcind.getFormData()%>"> <%
 	if (sv.zdcind.getFormData().equals("+")) { %>                  
 	                <img
					src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif"
					border="0"> <% }
 	if (sv.zdcind.getFormData().equals("X")) {  %> <img
					onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zdcind'))"
					style="cursor: pointer"
					src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
					border="0"> <% } %></td>
				<td style="font-weight: bold; font-size: 12px; font-family: Arial">
					<% if (!(sv.zdcind.getInvisible() == BaseScreenData.INVISIBLE || sv.zdcind
								.getEnabled() == BaseScreenData.DISABLED)) {%>
					<div style="height: 15 px">
						<a href="javascript:;"
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zdcind"))'
							class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payout")%>

						</a></div> <%} %>
				</td></tr>




<tr>
<td width='188'>&nbsp; &nbsp;<br/>
<% if(sv.zctaxind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.zctaxind
.getEnabled()==BaseScreenData.DISABLED){ %>

Contribution Details
<% }
else{ %>

<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zctaxind"))' 
class="hyperLink">

Contribution Details

</a></div>	<%} %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%if (sv.zctaxind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
<%}
	if (sv.zctaxind
.getFormData().equals("X")) {%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.zctaxind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%	} %>
<div>
<input name='zctaxind' id='zctaxind' type='hidden'  value="<%=sv.zctaxind
.getFormData()%>">
</div> </td></tr>
<!-- ALS-313 ends by pmujavadiya*/ -->

<!-- ALS-700 starts by rsubramani42*/ -->
<tr>
<td width='188'>&nbsp; &nbsp;<br/>
<%  if(sv.zroloverind
.getInvisible()== BaseScreenData.VISIBLE) { 
if(sv.zroloverind
.getEnabled()==BaseScreenData.DISABLED){ %>

Rollover Details
<% }
else{ %>

<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zroloverind"))' 
class="hyperLink">

Rollover Details

</a>
</div>	
<%} }%>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (sv.zroloverind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
<%}
	if (sv.zroloverind
.getFormData().equals("X")) { %>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.zroloverind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%	} %>
<div>
<input name='zroloverind' id='zroloverind' type='hidden'  value="<%=sv.zroloverind
.getFormData()%>">
<input name='bancAssuranceFlag' id='bancAssuranceFlag' type='hidden'  value="<%=sv.bancAssuranceFlag.getFormData()%>">	<!-- ILIFE-8544 -->
</div> </td></tr>

			<!-- MIBT-239 STARTS-->
<tr>
<td>
<input name='crcind' id='crcind' type='hidden'  value="<%=sv.crcind
.getFormData()%>">
<% if (sv.crcind
.getFormData().equals("+")) { %>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0">
<%}
	if (sv.crcind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('crcind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<% } %>
</td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial">
<%  if(!(sv.crcind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.crcind
.getEnabled()==BaseScreenData.DISABLED)){ %>
<div style="height: 15 px">
<a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("crcind"))'
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Credit Card")%>
</a></div>
<%} %>
</td> </tr>
