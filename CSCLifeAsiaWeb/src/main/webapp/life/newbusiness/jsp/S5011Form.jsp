<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!--<%=((SMARTHTMLFormatter) AppVars.hf).getHTMLCBVar(19, 25)%>-->
<%
	String screenName = "S5011";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	S5011ScreenVars sv = (S5011ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");
%>

<%

	{
		if (appVars.ind39.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind39.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.fupcdes.setReverse(BaseScreenData.REVERSED);
			sv.fupcdes.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupcdes.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.fupcdes.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.lifeno.setReverse(BaseScreenData.REVERSED);
			sv.lifeno.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.lifeno.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind31.isOn()) {
			sv.lifeno.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.jlife.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind36.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.fupstat.setReverse(BaseScreenData.REVERSED);
			sv.fupstat.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupstat.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind32.isOn()) {
			sv.fupstat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.fupremdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.fupremdtDisp.setColor(BaseScreenData.RED);
			sv.fupremdtDisp.setEnabled(BaseScreenData.DISABLED);		
					
		}
		
		if(appVars.ind40.isOn()){
			sv.crtdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.crtdateDisp.setColor(BaseScreenData.RED);
			sv.crtdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind33.isOn()) {
			sv.fupremdtDisp.setHighLight(BaseScreenData.BOLD);
			
			//start ILIFE-973
			sv.crtdateDisp.setHighLight(BaseScreenData.BOLD);
			//end ILIFE-973			
		}
// 		MIBT-132
		if (!appVars.ind40.isOn()) {
			sv.crtdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.fupremk.setReverse(BaseScreenData.REVERSED);
			sv.fupremk.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupremk.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind34.isOn()) {
			sv.fupremk.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.fuptype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.fuprcvdDisp.setReverse(BaseScreenData.REVERSED);
			sv.fuprcvdDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fuprcvdDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind33.isOn()) {
			sv.fuprcvdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.exprdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.exprdateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.exprdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind42.isOn()) {
			sv.exprdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Doctor ");
%>
<%
	StringData generatedText12 = new StringData(
			"1 - Exclusion clause, 2 - Follow up extended text");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act?");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/L");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reminder Date");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Created Date");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cat ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Received Date");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Expiry Date");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Remarks");
%>

<%
	{
		if (appVars.ind35.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
			sv.zdoctor.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind35.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
	}
%>








<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                            
                       <table>
                       <tr><td>
                       <div
			class='<%=(sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%>'>
		<%
			if (!((sv.chdrnum.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.chdrnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.chdrnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
			longValue = null;
			formatValue = null;
		%>
                       </td>
                       <td>
                       <div
			class='<%=(sv.cnttype.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%>' style="margin-left:1px;">
		<%
			if (!((sv.cnttype.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.cnttype.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.cnttype.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
			longValue = null;
			formatValue = null;
		%>
                       </td>
                       
                       <td>
                       <div
			class='<%=(sv.ctypedes.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%>' style="margin-left:1px;">
		<%
			if (!((sv.ctypedes.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.ctypedes.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.ctypedes.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
			longValue = null;
			formatValue = null;
		%>
                       </td>
                       </table>
		</div></div>	
		<div class="col-md-3"> </div>
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
		
		<table><tr><td>
		
		<div
			class='<%=(sv.cownnum.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%>'>
		<%
			if (!((sv.cownnum.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.cownnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.cownnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
			longValue = null;
			formatValue = null;
		%>
		</td><td>

		<div
			class='<%=(sv.ownername.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%>' style="margin-left:1px;min-width: 100px;max-width: 250px;">
		<%
			if (!((sv.ownername.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.ownername.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.ownername.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%></div>
		<%
			longValue = null;
			formatValue = null;
		%>
		</td></tr></table>
		
		</div>
		
		</div></div>
		
		<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">   
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Doctor")%></label>
    	 					<table><tr><td>
		            		                    
		                         
		                         <%
                                         if ((new Byte((sv.zdoctor).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div  style="margin-right: 2px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.zdoctor)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 150px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.zdoctor)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                     
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
                                  </td><td >
                                  <div class='<%=(sv.zdocname.getFormData()).trim().length() == 0 ? "blank_cell"
													: "output_cell"%> iconPos' style="margin-left: -2px;max-width:300px;min-width: 100px;">
								<%
									if (!((sv.zdocname.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zdocname.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.zdocname.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%> <%=XSSFilter.escapeHtml(formatValue)%></div>
								<%
									longValue = null;
									formatValue = null;
								%>
                                  </td></tr></table>
		                         
		                         
		                         
	
</div></div>
	
</div>



<br>

<%
	GeneralTable sfl = fw.getTable("s5011screensfl");
	int height;
	if (sfl.count() * 27 > 210) {
		height = 210;
	} else {
		height = sfl.count() * 27;
	}
%>



<script language="javascript">
        $(document).ready(function(){
	
			new superTable("s5011Table", {
				fixedCols : 0,					
				colWidths : [120,80,50,50,150,100,100,50,100,120,155],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
		
				addRemoveBtn:"N", 
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-s5011' width='100%'>
               
               <thead>
		
			        <tr class='info'>
			      
			        <th ><center><%=resourceBundleHandler.gettingValueFromBundle("Act?")%></center></th>
				    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
					<th style="min-width:60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Life")%></center></th>
					<th style="min-width:60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></center></th>
					<th style="min-width:130px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Reminder Date ")%></center></th>
					<th style="min-width:130px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Created Date")%></center></th>
					<th style="min-width:60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Cat")%></center></th>
					<th style="min-width:130px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Received Date")%></center></th>
					<th style="min-width:130px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Expiry Date")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Remarks")%></center></th>			     
		 	        
		 	        </tr>
			 </thead>
			 
			
			 <tbody>
			 
 		
							<%
							String backgroundcolor = "#FFFFFF";
	S5011screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5011screensfl.hasMoreScreenRows(sfl)) 
	{
		%>
		<%
	  {
				if (appVars.ind39.isOn()) {
					sv.select.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind01.isOn()) {
					sv.select.setInvisibility(BaseScreenData.INVISIBLE);
					sv.select.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind39.isOn()) {
					sv.select.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind39.isOn()) {
					sv.select.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind30.isOn()) {
					sv.fupcdes.setReverse(BaseScreenData.REVERSED);
					sv.fupcdes.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.fupcdes.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind30.isOn()) {
					sv.fupcdes.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind31.isOn()) {
					sv.lifeno.setReverse(BaseScreenData.REVERSED);
					sv.lifeno.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.lifeno.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind31.isOn()) {
					sv.lifeno.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind36.isOn()) {
					sv.jlife.setReverse(BaseScreenData.REVERSED);
					sv.jlife.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.jlife.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind36.isOn()) {
					sv.jlife.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind32.isOn()) {
					sv.fupstat.setReverse(BaseScreenData.REVERSED);
					sv.fupstat.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.fupstat.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind32.isOn()) {
					sv.fupstat.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind43.isOn()) {
					sv.fupremdtDisp.setReverse(BaseScreenData.REVERSED);
					sv.fupremdtDisp.setColor(BaseScreenData.RED);
					sv.fupremdtDisp.setEnabled(BaseScreenData.DISABLED);
					
								
				}
				if(appVars.ind40.isOn()){
					sv.crtdateDisp.setReverse(BaseScreenData.REVERSED);
					sv.crtdateDisp.setColor(BaseScreenData.RED);
					sv.crtdateDisp.setEnabled(BaseScreenData.DISABLED);
				}
		
				if (!appVars.ind33.isOn()) {
					sv.fupremdtDisp.setHighLight(BaseScreenData.BOLD);				
				}


				if (!appVars.ind40.isOn()) {
					sv.crtdateDisp
							.setInvisibility(BaseScreenData.INVISIBLE);
					sv.crtdateDisp.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind34.isOn()) {
					sv.fupremk.setReverse(BaseScreenData.REVERSED);
					sv.fupremk.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.fupremk.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind34.isOn()) {
					sv.fupremk.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind41.isOn()) {
					sv.fuptype.setInvisibility(BaseScreenData.INVISIBLE);
				}
				if (appVars.ind33.isOn()) {
					sv.fuprcvdDisp.setReverse(BaseScreenData.REVERSED);
					sv.fuprcvdDisp.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.fuprcvdDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind33.isOn()) {
					sv.fuprcvdDisp.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind42.isOn()) {
					sv.exprdateDisp.setReverse(BaseScreenData.REVERSED);
					sv.exprdateDisp.setColor(BaseScreenData.RED);
				}
				if (appVars.ind38.isOn()) {
					sv.exprdateDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (!appVars.ind42.isOn()) {
					sv.exprdateDisp.setHighLight(BaseScreenData.BOLD);
				}
			}
	%>
             <tr
			style="background:<%=backgroundcolor%>;"
			id='<%="tablerow" + count%>'>
			<td
				style="color: #434343; padding: 5px; position: relative;  border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left"><select value='<%=sv.select.getFormData()%>'
				size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.select)'
				onKeyUp='return checkMaxLength(this)'
				name='<%="s5011screensfl.select_R" + count%>' id='<%="s5011screensfl.select_R" + count%>' class="input_cell">
				<option value="">...</option>
				<option value="1" <%=sv.select.getFormData().trim().equalsIgnoreCase("1")? "Selected":""%>>Exclusion Clause</option>
				<option value="2"<%=sv.select.getFormData().trim().equalsIgnoreCase("2") ? "Selected":""%>>Follow-up</option>
			</select></td>

			<td
				style="color: #434343;position: relative;   border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left">
				<div class="input-group">
				<input name='<%="s5011screensfl.fupcdes" + "_R" +count %>' id='<%="s5011screensfl.fupcdes" + "_R"+count %>' type='text'
				value='<%=sv.fupcdes.getFormData()%>' style="width: 60px !important;"
				maxLength='<%=sv.fupcdes.getLength()%>'
				size='<%=sv.fupcdes.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.fupcdes)'
				onKeyUp='return checkMaxLength(this)'
		 		<%if ((new Byte((sv.fupcdes).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0) {%>
				readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.fupcdes).getHighLight()))
 				.compareTo(new Byte(BaseScreenData.BOLD)) == 0) { %>
 class="bold_cell" >
  	<%-- <a href="javascript:;"
				onClick="doFocus(document.getElementById('<%="s5011screensfl.fupcdes" + "_R" +count %>')); changeF4Image(this); doAction('PFKEY04')">
			<img style="margin-left: -2.5px; margin-bottom: 0.1px"
				src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/search.gif"
				border="0" > </a> --%> 
				                                    
                     <span class="input-group-btn">
                        <button class="btn btn-info"
                              type="button" onClick="doFocus(document.getElementById('<%="s5011screensfl.fupcdes" + "_R" +count %>')); doAction('PFKEY04')">
                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                         </button>
                     </span>
                
			<%
 	} else {
 %> class='<%=(sv.fupcdes).getColor() == null ? "input_cell"
									: (sv.fupcdes).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' > 
										<%-- 	<a
				href="javascript:;" style="position: relative; top:1px; left:0px" 
				onClick="doFocus(document.getElementById('<%="s5011screensfl.fupcdes" + "_R" +count %>')); changeF4Image(this); doAction('PFKEY04')">
			<img style="margin-left: -2.5px; margin-top: 0.1px"
				src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/search.gif"
				border="0" > </a>  --%>
				                                     
                     <span class="input-group-btn">
                        <button class="btn btn-info"
                              type="button" onClick="doFocus(document.getElementById('<%="s5011screensfl.fupcdes" + "_R" +count %>')); doAction('PFKEY04')">
                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                         </button>
                     </span>
               
                </div>
				
				<%
 	}
 %></td>

			<td
				style="color: #434343; padding: 5px; position: relative;  width: 50px; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left">
			<%
				sm = sfl.getCurrentScreenRow();
					qpsf = sm.getFieldXMLDef((sv.lifeno).getFieldName());
					qpsf
							.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf, sv.lifeno);
			%> <input type='text' maxLength='<%=sv.lifeno.getLength()%>'
				value='<%=formatValue%>' size='<%=sv.lifeno.getLength()%>'
				onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.lifeno)'
				onKeyUp='return checkMaxLength(this)'
				name='<%="s5011screensfl" + "." + "lifeno" + "_R"
								+ count%>'
								id='<%="s5011screensfl" + "." + "lifeno" + "_R"
								+ count%>'
				class="input_cell" style="width: 70px;"
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
				decimal='<%=qpsf.getDecimals()%>'
				onPaste='return doPasteNumber(event);'
				onBlur='return doBlurNumber(event);'></td>

			<td
				style="color: #434343; padding: 5px; position: relative;  width: 70px; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left"><input type='text'
				maxLength='<%=sv.jlife.getLength()%>'
				value='<%=sv.jlife.getFormData()%>'
				size='<%=sv.jlife.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.jlife)'
				onKeyUp='return checkMaxLength(this)'
				name='<%="s5011screensfl" + "." + "jlife" + "_R" + count%>'
				id='<%="s5011screensfl" + "." + "jlife" + "_R" + count%>'
				class="input_cell" style="width: 70px;">
			</td>

			<td
				style="color: #434343; padding: 5px; position: relative;  width: 140px; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left">
			<%
				String fieldName = "s5011screensfl.fupstat_R" + count;
				String fieldValue = sv.fupstat.getFormData();
					fieldItem = appVars.loadF4FieldsShort(
							new String[] { "fupstat" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("fupstat");
					optionValue = makeDropDownList(mappedItems, fieldValue,3 ,resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.fupstat.getFormData())
							.toString().trim());
			%> <%
 	if ((new Byte((sv.fupstat).getEnabled())).compareTo(new Byte(
 				BaseScreenData.DISABLED)) == 0) {
 %>
 <!-- smalchi2 for ILIFE-1032 STARTS -->
			 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
							"blank_cell" : "output_cell" %>' style="min-width: 120px;">
	   		<%if(longValue != null){%>

	   		<%=longValue%>

	   		<%}%>
	   </div>
<!-- ENDS -->
			<%
				longValue = null;
			%> <%
 	} else {
 %> <select name='<%=fieldName%>' id='<%=fieldName%>' type='list'
				style="width: 120px;"
				class=" <%=(sv.fupstat).getColor() == null ? "input_cell"
									: (sv.fupstat).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>">
				<%=optionValue%>
			</select> <%
 	}
 %>
			</td>

			<td style="color:#434343;font-weight: bold;font-size: 12px; font-family: Arial; position: relative; align="left">
			<% if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
              <div style="width: 80px;">   <%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp)%></div>
                                       
                             
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="fupremdtDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp, (sv.fupremdtDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>

			</td>

			<!-- smalchi2 for ILIFE-1032 STARTS -->
			<td >
			
			
			<%if(((new Byte((sv.crtdateDisp).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) { %>
			
			 <input
				name='<%="s5011screensfl.crtdateDisp_R" + count%>'
				id='<%="s5011screensfl.crtdateDisp_R" + count%>'
				type='text'
				value='<%=sv.crtdateDisp.getFormData()%>'				
				maxLength='<%=sv.crtdateDisp.getLength()%>'
				size='<%=sv.crtdateDisp.getLength()%>'
				onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.crtdateDisp)'
				onKeyUp='return checkMaxLength(this)'
				style="width: 80px;" readonly="true" class="output_cell">
				
			<%}else {   %>          
                  
                
             <div class="input-group date form_date col-md-12" data-date=""
                  data-date-format="dd/mm/yyyy" data-link-field="crtdateDisp"
                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                   <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp, (sv.crtdateDisp.getLength()))%>
                        <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                 </span>
             </div>
                                  
             <%}%>
              
			</td>	
	
	
				
			
				
			<%-- end ILIFE-973 --%>
			<td style="color:#434343; position: relative;padding: 5px;width:30px;font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;" align="left"><%=sv.fuptype.getFormData()%></td>

			<td style="color:#434343;  width:120px;font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;" align="left">
			<% if ((new Byte((sv.fuprcvdDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                          <div style="width:80px"> <%=smartHF.getRichTextDateInput(fw, sv.fuprcvdDisp)%></div>
                                       
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="fuprcvdDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.fuprcvdDisp, (sv.fuprcvdDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
</td>

			<td style="color:#434343; padding: 5px;  position: relative; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;" align="left">
			<% if ((new Byte((sv.exprdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div style="width:80px"><%=smartHF.getRichTextDateInput(fw, sv.exprdateDisp)%>
                                       
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="exprdateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.exprdateDisp, (sv.exprdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
</td>
<!-- ENDS -->
			<td style="color:#434343; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.scrollLeft); padding: 5px; width:230px;font-weight: bold;font-size: 12px; font-family: Arial; border-top: 1px solid #dddddd;" align="left"><input
				type='text' maxLength='<%=sv.fupremk.getLength()%>'
				value='<%=sv.fupremk.getFormData()%>'
				size='<%=sv.fupremk.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(s5011screensfl.fupremk)'
				onKeyUp='return checkMaxLength(this)'
				name='<%="s5011screensfl.fupremk_R" + count%>' id='<%="s5011screensfl.fupremk_R" + count%>' class="input_cell"
				style="width: 350px;"
				<%if(!(fw.getVariables().isScreenProtected())) { %>
				readonly="true" class="output_cell"
				<%} %>
				>
				</td>
		</tr>
          
       <%
									count = count + 1;
									S5011screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>    
          


</tbody>
</table>

</div>




</div></div>




<div class="row">
  
                  <div class="col-md-4">
        			<div class="form-group">
						<a class="btn btn-info" href= "#" onClick=" JavaScript:doAction('PFKey91');"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
        			    <a class="btn btn-info" href= "#" onClick=" JavaScript:doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
       
        			  
        			</div>
        		</div>


<div style='visibility: hidden;'>
<table>
	<tr style='height: 22px;'>
		<td width='188'>&nbsp; &nbsp;<br />
		<input name='select' type='text'
			<%formatValue = (sv.select.getFormData()).toString();%>
			value='<%= XSSFilter.escapeHtml(formatValue)%>'
			<%if (formatValue != null && formatValue.trim().length() > 0) {%>
			title='<%=formatValue%>' <%}%> size='<%=sv.select.getLength()%>'
			maxLength='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
			onHelp='return fieldHelp(s5011screensfl.select)'
			onKeyUp='return checkMaxLength(this)'
			<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0) {%>
			readonly="true" class="output_cell"
			<%} else if ((new Byte((sv.select).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
			class="bold_cell" <%} else {%>
			class='<%=(sv.select).getColor() == null ? "input_cell"
								: (sv.select).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			<%}%>></td>
	</tr>
</table>
</div>
</div>

<script>
$(document).ready(function() {
	$('#dataTables-s5011').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
        orderable: false
  	});
	
	 $("#crtdateDisp").width(80)
	 
});
</script>


</div></div>


<%@ include file="/POLACommon2NEW.jsp"%>
