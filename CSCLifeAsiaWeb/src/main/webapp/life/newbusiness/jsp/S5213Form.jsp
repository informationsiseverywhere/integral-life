

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5213";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%S5213ScreenVars sv = (S5213ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Alter From Inception");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Contract Enquiry");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Cancel From Inception");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Freelook Cancellation");%>	
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		
		
		if (appVars.ind03.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.effdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
        }
	}

	%>


<div class="panel panel-default">
    <div class="panel-heading">
      <%=resourceBundleHandler.gettingValueFromBundle("Input")%>
     </div>
     
     <div class="panel-body">     
            <div class="row">   
                    <div class="col-md-4"> 
                            <div class="form-group">
                                <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                                <div class="input-group" style="min-width:120px;">
                                    <input name='chdrsel' id='chdrsel'
									type='text' 
									value='<%=sv.chdrsel.getFormData()%>' 
									maxLength='<%=sv.chdrsel.getLength()%>' 
									size='<%=sv.chdrsel.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
									
									<% 
									    if((new Byte((sv.chdrsel).getEnabled()))
									    .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>  
									readonly="true"
									class="output_cell"  >
									
									<%
									    }else if((new Byte((sv.chdrsel).getHighLight())).
									        compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									    
									%>  
									class="bold_cell" >
									 
									<span class="input-group-btn">
                                    <button class="btn btn-info" style="font-size: 20px;" type="button" 
                                        onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    </button>
                                    </span>
									
									<%
									    }else { 
									%>
									
									class = ' <%=(sv.chdrsel).getColor()== null  ? 
									"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>' >
									
									<span class="input-group-btn">
                                    <button class="btn btn-info" style="font-size: 19px;" type="button" 
                                        onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                    </button>
                                    </span>
									
									<%} %>
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-4"> 
				    <div class="form-group">
							<% if ((new Byte((sv.effdateDisp).getInvisible()))
                                    .compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
							<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
				    </div>
				 </div>
                    
				 
                    </div>
    </div>
    </div>

<div class="panel panel-default">
        <div class="panel-heading">
            <%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

        <div class="panel-body">     
            <div class="row">   
                <div class="col-md-3">
                    <label class="radio-inline">
                        <b><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
                        <%=resourceBundleHandler.gettingValueFromBundle("Alter From Inception")%>
                    </b></label>
                </div>
                <div class="col-md-3">          
                    <label class="radio-inline">
                        <b><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
                        <%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%>
                   </b> </label>            
                </div>              
           
               
                <div class="col-md-3">
                    <label class="radio-inline">
                        <b><%= smartHF.buildRadioOption(sv.action, "action", "C")%> 
                        <%=resourceBundleHandler.gettingValueFromBundle("Cancel From Inception")%>
                    </b></label>
                </div>
                <div class="col-md-3">          
                    <label class="radio-inline">
                       <b> <%= smartHF.buildRadioOption(sv.action, "action", "D")%> 
                        <%=resourceBundleHandler.gettingValueFromBundle("Freelook Cancellation")%>
                    </b></label>            
                </div>              
            </div>
        </div>
</div>
<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)' />



<%@ include file="/POLACommon2NEW.jsp"%>

