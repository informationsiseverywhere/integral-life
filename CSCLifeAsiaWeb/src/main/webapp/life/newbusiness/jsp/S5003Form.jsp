
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5003";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="java.text.NumberFormat"%>
<%@ page import="java.math.BigDecimal"%>

<%S5003ScreenVars sv = (S5003ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.cntBranch.setReverse(BaseScreenData.REVERSED);
			sv.cntBranch.setColor(BaseScreenData.RED);
		}
		if (appVars.ind16.isOn()) {
			sv.cntBranch.setEnabled(BaseScreenData.DISABLED);
			sv.cntBranch.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind15.isOn()) {
			sv.cntBranch.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Use Refresh key for pre-issue");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"validation");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Select, 2 - Add to, 9 - Delete");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act?");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
<div class="panel-body" >

	<div class="row">
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
		<!-- <div class="input-group" >  -->
		<table><tr><td>		
		  <div 	
	class='<%= (sv.cntBranch.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="max-width:50px"><!-- ILJ-568 -->
<%					
if(!((sv.cntBranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntBranch.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cntBranch.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;%>
</td><td>
<div 	
	class='<%= (sv.branchdesc.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 220px;"><!-- ILJ-568 --> <!-- style="float: left!important;margin-left: 2px !important;border-radius: 5px !important;
						height: 25px !important;font-size: 12px !important;min-width:90px"> -->
<%					
if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;%>
			</td></tr></table>
	<!-- 	</div> -->
		</div>
		</div>
		
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting_Month")%></label>
		<!-- <div class="input-group" > -->
		<table><tr><td>

<div 	
	class='<%= (sv.acctmonth.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="max-width:30px">
<%					
if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
	
<%
longValue = null;
formatValue = null;
%>
</td><td style="width:2px;"></td><td>
<div 	
	class='<%= (sv.acctyear.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' > <!-- style="float: left!important;margin-left: 2px !important;border-radius: 5px !important;
						height: 25px !important;font-size: 12px !important;max-width:50px"> -->
<%					
if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>
	<%
longValue = null;
formatValue = null;
%>
			</td></tr></table>
		<!-- </div> -->
		</div>
		</div>	
		
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract_Plan")%></label>
		
<div 	
	class='<%= (sv.contractPlan.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="max-width:100px">
<%					
if(!((sv.contractPlan.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.contractPlan.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.contractPlan.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;%>
	

		</div>
		</div>
	</div>
	
		<div class="row">
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		
		<table><tr><td>
			<div class='<%= (sv.chdrnum.getFormData()).trim().length() == 0 ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">
			<%					
			if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td style="padding-left: 1px;">
	
		<div class='<%= (sv.cnttype.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>' style="max-width:70px;" ><!-- ILJ-568 -->
			<%					
			if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
			</td>
			<td >

			<div class='<%= (sv.ctypedes.getFormData()).trim().length() == 0 ? 
							"blank_cell" : "output_cell" %>' style="max-width: 220px;margin-left: 1px;" id="ctypedes"> <!-- ILJ-568 -->
				<%					
				if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

			</td></tr></table>
		
		</div>
		</div>
		
		
		
		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
		 
		<table><tr><td>
	
			<div class='<%= (sv.cownnum.getFormData()).trim().length() == 0 ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%					
				if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		</td>
		<td style="padding-left: 1px;">

	<div class='<%= (sv.ownername.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="max-width:440px;" > <!-- ILJ-568 -->
				<%					
				if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

				</td></tr></table>
		
		</div>
		</div>	
	
	</div>

	<br>

	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<div class="table-responsive">
	         <table  id='dataTables-s5003' class="table table-striped table-bordered table-hover" width="100%" >
	                         	<thead>
	                            	<tr class='info'>								
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>									
	
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
				
		        <th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
		        
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
	
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
				
				<%if ((new Byte((sv.cntBranch).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) { %>
						
					<th ><center ><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
					
					<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></center></th>
					
					<!--  ILJ-43 -->
					<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
						<th ><center ><%=resourceBundleHandler.gettingValueFromBundle("Contract Term")%></center></th>
					</label>
					<% } else{%>
						<th ><center ><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
					<% } %>
					<!--  END  -->	
					
					<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
					
				
			
				<%} %>
	                               </tr>
	                            </thead>
<tbody>



<%
		GeneralTable sfl = fw.getTable("s5003screensfl");
		int height;
		if(sfl.count()*27 > 350) {
		height = 350 ;
		} else {
		height = sfl.count()*27;
		}	
		int totalTblWidth = 744;
		int totalDivHeight = 350; 
		if ((new Byte((sv.cntBranch).getInvisible())).compareTo(new Byte(
				BaseScreenData.INVISIBLE)) != 0) { 
			
			
			totalTblWidth = 960;
		  //ILIFE - 2001 SART BY mranpise//
		   //totalDivHeight = 280;			
			totalDivHeight = 350;
		}
		    //ILIFE - 2001 ENDS//
		%>
		
		<%
	
	
	S5003screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5003screensfl
	.hasMoreScreenRows(sfl)) {
	{
		if (appVars.ind01.isOn()) {	
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.cntBranch.setReverse(BaseScreenData.REVERSED);
			sv.cntBranch.setColor(BaseScreenData.RED);
		}
		if (appVars.ind16.isOn()) {
			sv.cntBranch.setEnabled(BaseScreenData.DISABLED);
			sv.cntBranch.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind15.isOn()) {
			sv.cntBranch.setHighLight(BaseScreenData.BOLD);
		}
	}
	
%>

<tr>
				<td align="center" style="vertical-align:middle;">
				<%
					if(!("+".equalsIgnoreCase(sv.life.getFormData().trim()))){
				 %>
																		
					<select value='<%= sv.action.getFormData() %>' 
							size='<%=sv.action.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(s5003screensfl.action)' onKeyUp='return checkMaxLength(this)' 
							name='<%="s5003screensfl" + "." + "action" + "_R" + count %>' style="padding:6px 6px; min-width:100px;"
							class = "input_cell">
								<option value="">...</option>
								<option value="1"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
								<option value="2"><%=resourceBundleHandler.gettingValueFromBundle("Add to")%></option>
								<option value="9"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></option>
					</select>	
					<%} %>
										 					 
				</td>	
						
				<td  align="center" style="vertical-align:middle;">							
					<%= sv.life.getFormData()%>
				</td>		
					
				<td  align="center" style="vertical-align:middle;">
					<%= sv.coverage.getFormData()%>
				</td>		
					
				<td align="center"  style="vertical-align:middle;">
					<%= sv.rider.getFormData()%>
				</td>		
					
				<td align="left"  style="vertical-align:middle;">	
					<%= sv.elemkey.getFormData()%> - <%= sv.elemdesc.getFormData()%>
				</td>
				<%if ((new Byte((sv.cntBranch).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) { %>
					<!-- ILIFE-2001 start by mranpise -->	
						<%if ((new Byte((sv.sumin).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									<%
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.sumin).getFieldName());									
									valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);							
									
									}else{
										valueThis = null;
										
									} %>
						
					<td  align="right" style="vertical-align:middle;padding: 3px !important;">			<!-- ILIFE-2087 ends -->
						<%=valueThis.replaceAll("[.][0-9]+$", "")%>
						<!-- ILIFE-2001 ends -->			
					</td>
					
					
					
					
					
					
					<!-- ILIFE-2001 starts by mranpise -->	
					<%
					//ILJ-388
					if(sv.cntEnqScreenflag.equals("N")){
					if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									<%
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.instPrem).getFieldName());									
									valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);							
									
									}else{
										valueThis = null;	
									
									} 
					}else{
						if(sv.currencyScreenflag.equals("Y")){
							if(!(sv.instPrem.toString().trim().equals("")) && !(sv.instPrem.toString().equals("00000000000000000"))){ 
								valueThis=NumberFormat.getNumberInstance(Locale.US).format(sv.instPrem.getbigdata().intValue());
								}else{
									valueThis="";
									
								}
							}
							else{  
								if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.instPrem).getFieldName());									
									valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);							
									
									}else{
										valueThis = null;	
									
								} 
							 } 
					
					}%>
					<td align="right" style="vertical-align:middle;padding: 3px !important;">			<!-- ILIFE-2087 ends -->			
						<%= valueThis%>
						<!-- ILIFE-2001 ends -->
					</td>
					
						
				<%} %>
				
				<td  align="center" style="vertical-align:middle;">								
						<%= sv.riskCessTerm.getFormData()%>
					</td>
					<td align="center" style="vertical-align:middle;">								
						<%= sv.premCessTerm.getFormData()%>
					</td>
			</tr>


	<%
	count = count + 1;
	S5003screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>

</tbody>
</table>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="form-group">
	<label>
	<%=resourceBundleHandler.gettingValueFromBundle("Press Refresh button for Pre- Issue Validation.")%>
	</label>
</div>
</div>
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-s5003').DataTable({
			ordering : false,
			searching : false,
			scrollY : "350px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false,
	       
		});
		
		if (screen.height == 900) {
			
			$('#ctypedes').css('max-width','215px')
		}
		
	});
</script> 


<!-- ILIFE-963 ENDS -->
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Use Refresh key for pre-issue")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("validation")%>
</div>

</tr></table></div>




</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

<script type="text/javascript">
<%if(!cobolAv3.isPagedownEnabled()){%>
		window.onload = function(){
			setDisabledMoreBtn();
		}
<%}%>
</script>