

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5657";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5657ScreenVars sv = (S5657ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default loading/discount -");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate Adjustment ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-standard life ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Exclusion Clause  ");%>

<div class="panel panel-default">	
    <div class="panel-body">
		
		<div class="row">
		  <div class="col-md-4">
    	 		<div class="form-group"> 	
    	 		     <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>  
    	 		      		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:900px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				    		</div>
				       </div>
				       
		          
	 <div class="col-md-4">
    	<div class="form-group"> 	
    	 	<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>  
               	
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:900px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>    
                   </div>
                </div>
                
                 
   
         <div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td>
						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
	</div>
&nbsp;&nbsp;
    <div class="row">
     <div class="col-md-6">
       <div class="label_txt">
     <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default loading/discount -"))%></label>
	   </div>
	    </div>
	    </div>
	
	 
	 &nbsp;&nbsp;  
	 <div class="row">  
	    <div class="col-md-4">
    	 <div class="form-group"> 	
    	    <label><%=resourceBundleHandler.gettingValueFromBundle("Age")%></label>  
    	    <table><tr><td>
              <%	
			qpsf = fw.getFieldXMLDef((sv.agerate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
	%>

<input name='agerate' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.agerate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.agerate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.agerate) %>'
	 <%}%>

size='<%= sv.agerate.getLength()%>'
maxLength='<%= sv.agerate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(agerate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.agerate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.agerate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.agerate).getColor()== null  ? 
			"input_cell" :  (sv.agerate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	</td></tr></table>
          </div>
         </div>
          
          <div class="col-md-4">
    	 <div class="form-group"> 	
    	    <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></label>  
    	     <table><tr><td>
             <%	
			qpsf = fw.getFieldXMLDef((sv.oppc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='oppc' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.oppc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.oppc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.oppc) %>'
	 <%}%>

size='<%= sv.oppc.getLength()%>'
maxLength='<%= sv.oppc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(oppc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	<%--/** start ILIFE-926 **/--%>
	<%--onBlur='return doBlurNumber(event);'--%>
	onBlur="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	<%--/** end ILIFE-926 **/--%>

<% 
	if((new Byte((sv.oppc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.oppc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.oppc).getColor()== null  ? 
			"input_cell" :  (sv.oppc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	</td></tr></table>
          </div>
         </div> 
         
           <div class="col-md-4">
    	 <div class="form-group"> 	
    	    <label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjustment")%></label>  
    	     <table><tr><td>
             <%	
			qpsf = fw.getFieldXMLDef((sv.insprm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
	%>

<input name='insprm' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.insprm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.insprm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.insprm) %>'
	 <%}%>

size='<%= sv.insprm.getLength()%>'
maxLength='<%= sv.insprm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(insprm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.insprm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.insprm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.insprm).getColor()== null  ? 
			"input_cell" :  (sv.insprm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	</td></tr></table>
          </div>
         </div> 
	</div>
&nbsp;&nbsp;
	 <div class="row">
         <div class="col-md-4">
    	 		<div class="form-group"> 
    	 		<%
                       ScreenDataHelper subStandard = new ScreenDataHelper(fw.getVariables(), sv.sbstdl);
                     %>	
    	 		     <label><%=resourceBundleHandler.gettingValueFromBundle("Sub-standard life")%></label> 
    	 		     <table><tr><td> 	
                      <%if (subStandard.isReadonly()) {%>
                     <input name='sbstdl' type='text' value='<%=subStandard.value()%>'
                      class="blank_cell" readonly
                      onFocus='doFocus(this)' onHelp='return fieldHelp(sbstdl)'
                         onKeyUp='return checkMaxLength(this)' size='1'>
                     <%} else {%>
                   <select name="sbstdl" type="list" class="<%=subStandard.clazz()%>">
 
  		<!-- option <%--=subStandard.selected("")--%> value=""--><!-- /option -->
 
                 <option <%=subStandard.selected("Y")%> value="Y">Y</option>
                    <option <%=subStandard.selected("N")%> value="N">N</option>
                           </select>
<%}%>
</td></tr></table>
		       </div>
        </div>
         <div class="col-md-4"></div>
         <div class="col-md-4">
    	 		<div class="form-group"> 
    	 		<%
                 ScreenDataHelper exclusionClause = new ScreenDataHelper(fw.getVariables(), sv.tabtype);
                   %>	
    	 		     <label><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Clause")%></label> 
    	 		     <table><tr><td> 	
    	 		     <%if (exclusionClause.isReadonly()) {%>
        <input name='tabtype' type='text' value='<%=exclusionClause.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)' onHelp='return fieldHelp(tabtype)'
       onKeyUp='return checkMaxLength(this)' size='1'>
         <%} else {%>
        <select name="tabtype" type="list" class="<%=exclusionClause.clazz()%>">
  <%--/** ILIFE-926 **/ --%>
  		<!--option <%--=exclusionClause.selected("")--%> value=""--><!--/option-->
  <%--/** ILIFE-926 **/ --%>
  <option <%=exclusionClause.selected("Y")%> value="Y">Y</option>
  <option <%=exclusionClause.selected("N")%> value="N">N</option>
  </select>
         <%}%>
         </td></tr></table>
		       </div>
        </div>
   </div>
</div>

</div>
<%@ include file="/POLACommon2NEW.jsp"%>
<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>

  