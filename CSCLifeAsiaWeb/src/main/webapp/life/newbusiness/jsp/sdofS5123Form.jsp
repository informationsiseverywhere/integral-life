﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import='com.quipoz.framework.util.*'%>
<%@ page import='com.quipoz.framework.screenmodel.*'%>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel'%>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@ page import="com.csc.life.newbusiness.screens.S5123ScreenVars" %>
<%@ page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@ page import="com.quipoz.framework.datatype.StringBase"%>
<%@ page import="com.quipoz.framework.error.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.properties.PropertyLoader" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<%
	BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
	ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
	COBOLAppVars cobolAv3 = (COBOLAppVars) bm3.getApplicationVariables();
	response.addHeader("Cache-Control", "no-store"); 
	response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  	response.addHeader("X-Content-Type-Options", "NOSNIFF");
	response.addHeader("Pragma", "no-cache"); 


	String lang1 = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();
	String imageFolder = PropertyLoader.getFolderName(request.getLocale().toString());
	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw3.getScreenName(),request.getLocale());

	AppConfig appCfg = AppConfig.getInstance();
%>

<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--  LINK REL="StyleSheet" HREF="theme/<%=lang1.toLowerCase() %>/style.css" TYPE="text/css" -->
<!-- bootstrap -->
<link href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
<link href="../../../bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
<script src='../../../bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='../../../bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src='../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='../../../bootstrap/sb/dist/js/sb-admin-2.js'></script>
<script src="../../../bootstrap/integral/integral-admin-sidebar.js"></script>
<!-- YY -->
<link href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">

<script language='javaScript'>
	<%char[] c = cobolAv3.additionalValidKeys.toCharArray();
			String validKeys = "[";
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '1') {
					validKeys += i + ",";
				}
			}
			if (validKeys.endsWith("]")) {
				validKeys += "";//empty array
			} else {
				validKeys = validKeys.substring(0, validKeys.length() - 1)
						+ "]";
			}%>
	var validKeyArr = <%=validKeys%>;
	var isSupported = false;
	var lang1 = "<%=lang1%>";
	function isSupportKey(validKeyArr,action){
		if (validKeyArr.length > 0){
			for (var i=0;i<validKeyArr.length;i++){
				if (validKeyArr[i] == action){
					isSupported = true;
					break;
				}
			}
			if (isSupported){
				clearFField(); // fix bug46
				doAction('PFKEY0'+action);
			}else{
			/*<% if ("CHI".equals(lang1)) { %>
				alert("功能键" + action + "不能在当前屏幕使用");
			<% } else {%>
				alert("Function key " + action + " is not active on this screen at this time.");
			<%}%>*/
			callCommonAlert(lang1,"No0002",action);
			}
		}else{
			//alert("no keys are supported");
			callCommonAlert(lang1,"No00020");
		}
	}

  
</script>

<%@ page session="false"%>
<title>Generic SideBar</title>


<%
	HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess
			.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel != null) {
		baseModel.getApplicationVariables();
		ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
		SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter) AppVars.hf;
%>

<script type="text/javascript">
function hyperLinkTo(nextField) {
	//ILIFE-3537
	var fieldName=nextField.name;
	var doc=parent.frames["mainForm"].document;
	doc.getElementById(fieldName).value="X";
	/* var sideFrame = parent.frames[1]; */
	var sideFrame = parent.frames["mainForm"];
	sideFrame.document.form1.activeField.value = nextField;
	sideFrame.document.form1.action_key.value = "PFKEY0";	
    sideFrame.doSub();		
}
function removeXfield(xfield){
	var doc=parent.frames["mainForm"].document;
	doc.getElementById(xfield.id).value="";
	doAction('PFKEY05');
}
function doAction(act) {
	  parent.frames["mainForm"].document.form1.action_key.value = act;
	  parent.frames["mainForm"].doSub();
/* 	pageloading(); */
}
</script>

</HEAD>
<BODY id="sideBar" style="background-color: #ffffff;font-size: 13px !important; border: 1px solid gainsboro !important;">
<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) Begin-->
<%
	String msgs = resourceBundleHandler.gettingValueFromBundle("No Message");
		String fontStyle = " font-family:Arial; font-weight:bold; font-size:12px;";
		String lang = request.getParameter("lang");
		/**
		if ("CHI".equalsIgnoreCase(lang)) {
			msgs = "没有信息";
			fontStyle = " font-type:宋体; font-weight:normal; font-size:12px;";
		}*/
		try {
			sess = request.getSession();
			BaseModel bm = (BaseModel) sess
					.getAttribute(BaseModel.SESSION_VARIABLE);
			AppVars avs = bm.getApplicationVariables();
			lang = avs.getUserLanguage().toString().trim();
			if (!avs.mainFrameLoaded) {
				for (int i = 0; i < 40; i++) {
					avs.waitabit(250);
					if (avs.mainFrameLoaded) {
						break;
					}
				}
			}
			if (!avs.mainFrameLoaded) {
				msgs = resourceBundleHandler.gettingValueFromBundle("Messages failed to load after 10 seconds.")
						+ "<br>"+resourceBundleHandler.gettingValueFromBundle("This is usually caused by slow response on the main form; errors may be incorrect.")
						+ "<br>"+resourceBundleHandler.gettingValueFromBundle("Press the refresh button here when the main form loads.")
						+ "<button onClick='document.location.reload(false)'>"+resourceBundleHandler.gettingValueFromBundle("Refresh errors")+"</Button>";
			} else {
				String ctx = request.getContextPath() + "/";
				MessageList list = avs.getMessages();
				Iterator i = list.iterator();
				StringBuffer sb = new StringBuffer();
				while (i.hasNext()) {
					String str = i.next().toString();
					HTMLFormatter formatter = new HTMLFormatter();

					/* Remove any trailing ? which is historic and means "stop underlining" */
					str = QPUtilities.removeTrailing(str.trim(), "?");
					//added by wayne to parse errorno
					String[] arr = str.split("ErrorMessage");
					if (arr.length < 2)
						sb.append(str + "<br>");
					else {
						sb.append(arr[0]);
							for (int j = 1; j < arr.length - 1; j++) {
							sb.append("<a href='#'>")
									.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?")))
									.append("</a><br/>");
						}
						sb.append("<a href='#'>")
								.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[arr.length - 1].substring(4),"?")))
								.append("</a>");
						sb.append("<br>");
					}
				}

				msgs = sb.toString();

				msgs = msgs.replaceFirst("Message:", "");
			}
		} catch (Exception e) {
		}
%>
<script language="javascript">
	//boostrap
	$(document).ready(function(){
	if(<%=msgs.length()%> == 0)	$("#msg-panel").css("display","none");
		calculateMenuHeight();
});	
	function calculateMenuHeight(){
		var screenBodyHeight = screen.height;
		var sidebarBodyHeight = $("#sideBar").height();
   		//var logoHeight = $("#topPanel").outerHeight(true);
		// ILIFE-8864 STARTS
		var logoHeight = 0; 
		if($("#sidebar-hide").length > 0){
			logoHeight = 20;
		}
		// ILIFE-8864 ENDS
   		var sidebar = $(".sidearea");
   		var message = $(".err-msg");
   		var messPane = $("#msg-panel");
   		if(screenBodyHeight > 1000) {
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = sidebarBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = sidebarBodyHeight - logoHeight;
   			}
   		}else{  
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = screenBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = screenBodyHeight - logoHeight;
   			}
   		}
   		sidebar.css('height', sidebarHeight + "px");
   	}
</script>
<style>
.row.message {
	padding-left: 15px;
}
#msg-panel> .panel-heading{
	 font-weight: bolder !important;
	 text-align: center !important;
	 padding-right: 40px !important;
}
/* #msg-panel{
     top: 305px !important;
} */
</style>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
//public static final boolean ieEmulationOn = true;	//IGROUP-1211 make emulation to be configurable
public static String emulationVer="off";			//IGROUP-1211
%>
<%	
	// IGroup-1086 add the brower version in common jsp
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	emulationVer = "off";	//IGROUP-1211
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10; //IGROUP-1211
		}
	} else if (userAgent.contains("Trident/7.0")) {
			browerVersion = IE11;
		    if(AppConfig.ieEmulationEnable) emulationVer =  IE11;	//IGROUP-1211
	}		
%>
	<%-- <div id="topPanel" class="row" style="border:0px;margin:0 0 0 -1;background-color:#2c2cce;height: 50px;width: 300px;">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; margin-top: 2px;background-color: #2c2cce;">
				<% if ( browerVersion.equals(Chrome)) {%>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;">				
				<%}else{ %>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;height: 59px">	
				<%} %>
					<li><a><img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/dxc_symbol_wht_rgb_150.png" width="55px" border="0"/></a></li>
					<li><a style="left: -15px;top: -4px;"><div style="font-size:15px;font-weight: bold;color: white !important;">INTEGRAL Admin</div>
						<table style="font-size: 10px !important;font-weight: bold;width:91%;">
				         <tr>
					         	<td style="color: #ffffff !important;"><%=AppConfig.getInstance().getVersion() %> </td>
					         	<td style="text-align: right; color: #fff !important;"><%=fw.getScreenName()%></td>
					         </tr>
				         </table>
				         </a>
					</li>
				</ul>
			</nav>
	 </div> --%>
	<div class="sidearea" style="overflow: auto;">
<%
	// To decide whether menus are displayed or not
		LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel
				.getApplicationVariables();
		boolean isShow = av.isMenuDisplayed();
		if (!isShow) {
%>
<!-- ILIFE-8864 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<div id="sidebar-hide" style="padding-left: 93%;">
	<span id="sidebar-pin" class="glyphicon glyphicon-pushpin" style="line-height: 20px; cursor:pointer"></span>
	<span id="sidebar-unpin" class="glyphicon glyphicon-chevron-left" style="line-height: 20px; cursor:pointer"></span>
</div>

<script>
	$(document).ready(function(){
		var mainDoc=parent.frames["mainForm"];
		var msgDiv = document.getElementById("msg-panel");
		if(msgDiv.style.display !=="none"){
			mainDoc.document.getElementById("msg-panel").innerHTML += msgDiv.innerHTML;
			mainDoc.document.getElementById("msg-panel").style.display="block";
		}	
	})
	if(sessionStorage.getItem("sidebar") !== "pined"){
		$("#sidebar-pin").css("display", "block");
		$("#sidebar-unpin").css("display", "none");
	}else{
		$("#sidebar-pin").css("display", "none");
		$("#sidebar-unpin").css("display", "block");
		window.top.document.getElementsByName("realContent")[0].cols = "275,*";
	}
	$("#sidebar-hide>span").click(function(){
		if(sessionStorage.getItem("sidebar") === "pined"){
			$("#sidebar-pin").css("display", "block");
			$("#sidebar-unpin").css("display", "none");
			sessionStorage.setItem("sidebar", "unpined");
			window.top.document.getElementsByName("realContent")[0].cols = "0,*"
		}else{
			$("#sidebar-pin").css("display", "none");
			$("#sidebar-unpin").css("display", "block");
			sessionStorage.setItem("sidebar", "pined");
		}
	});
</script>
<%}%>
<!-- ILIFE-8864 ENDS-->
<!-- <div class="logoarea" style="height: 50px;">
	<a class="navbar-brand" href="#">Integral Admin</a>
</div> -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse" style="display: block;">
		<ul class="nav" id="side-menu">
			<li class="active">
				<a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Extra_Info")%></a>
				<%if(fw.getScreenName().equalsIgnoreCase("S5123")){
					S5123ScreenVars sv = (S5123ScreenVars) fw.getVariables();
				%>
				<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>
					    <li class="active">
						<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.optextind.getInvisible()== BaseScreenData.INVISIBLE|| sv.optextind
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.optextind.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.optextind.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.taxind.getInvisible()== BaseScreenData.INVISIBLE|| sv.taxind
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.taxind.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.taxind.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='optsmode' id='optsmode' type='hidden'  value="<%=sv.optsmode.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.optsmode.getInvisible()== BaseScreenData.INVISIBLE|| sv.optsmode
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optsmode"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.optsmode.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.optsmode.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='ratypind' id='ratypind' type='hidden'  value="<%=sv.ratypind.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.ratypind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ratypind
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ratypind"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.ratypind.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.ratypind.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='pbind' id='pbind' type='hidden'  value="<%=sv.pbind.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.pbind.getInvisible()== BaseScreenData.INVISIBLE|| sv.pbind
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.pbind.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.pbind.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='payflag' id='payflag' type='hidden'  value="<%=sv.payflag.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.payflag.getInvisible()== BaseScreenData.INVISIBLE|| sv.payflag
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Instalment Details")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payflag"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Instalment Details")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.payflag.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.payflag.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='zsredtrm' id='zsredtrm' type='hidden'  value="<%=sv.zsredtrm.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.zsredtrm.getInvisible()== BaseScreenData.INVISIBLE|| sv.zsredtrm
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zsredtrm"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.zsredtrm.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.zsredtrm.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					<li>
						<input name='exclind' id='exclind' type='hidden'  value="<%=sv.exclind.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.exclind.getInvisible()== BaseScreenData.INVISIBLE|| sv.exclind
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.exclind.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.exclind.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					
					<li>
						<input name='aepaydet' id='aepaydet' type='hidden'  value="<%=sv.aepaydet.getFormData()%>">   	
						<!-- text -->
						<%
							if((sv.aepaydet.getInvisible()== BaseScreenData.INVISIBLE|| sv.aepaydet
								.getEnabled()==BaseScreenData.DISABLED)){
						%> 
							<!-- <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("AntcptdEndwnt PayDetails")%></a> -->
						<%
			 				} else {
						%>
							<a href="javascript:;" 
							onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aepaydet"))' class="hyperLink"> 
							<%=resourceBundleHandler.gettingValueFromBundle("AntcptdEndwnt PayDetails")%>
						<%}%>
									
						<!-- icon -->
						<%
							if (sv.aepaydet.getFormData().equals("+")) {
						%> 
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
						<%}
			 				if (sv.aepaydet.getFormData().equals("X")) {
			 			%>
			 				<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 			<%}%>
			 			</a>
					</li>
					
				</ul>
				<%} %>
			</li>
		</ul>
	</div>
</div>
<div class="panel-group" id="accordion">
			<div class="panel panel-info" id='msg-panel'>
			    <div class="panel-heading">
			        <h4 class="panel-title">
			            <%=resourceBundleHandler.gettingValueFromBundle("Messages")%>
			        </h4>
			    </div>
			    <div class="panel-body">
					<div class="row">	
							<%=msgs%>
					</div>
			    </div>
			</div>	
</div>
<%} %>
<%} %>
</div>
</BODY>
</HTML>
