<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR591";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%
	appVars.rollup();
	appVars.rolldown();
%>
<%Sr591ScreenVars sv = (Sr591ScreenVars) fw.getVariables();%>

<%if (sv.Sr591screenWritten.gt(0)) { %>
	<%Sr591screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy No ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow-up Code ");%>
	<%sv.fupcode.setClassString("");%>
<%	sv.fupcode.appendClassString("string_fld");
	sv.fupcode.appendClassString("output_txt");
	sv.fupcode.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Letter Type ");%>
	<%sv.hxcllettyp.setClassString("");%>
	<%sv.hxclnote01.setClassString("");%>
<%	sv.hxclnote01.appendClassString("string_fld");
	sv.hxclnote01.appendClassString("input_txt");
	sv.hxclnote01.appendClassString("highlight");
%>
	<%sv.hxclnote02.setClassString("");%>
<%	sv.hxclnote02.appendClassString("string_fld");
	sv.hxclnote02.appendClassString("input_txt");
	sv.hxclnote02.appendClassString("highlight");
%>
	<%sv.hxclnote03.setClassString("");%>
<%	sv.hxclnote03.appendClassString("string_fld");
	sv.hxclnote03.appendClassString("input_txt");
	sv.hxclnote03.appendClassString("highlight");
%>
	<%sv.hxclnote04.setClassString("");%>
<%	sv.hxclnote04.appendClassString("string_fld");
	sv.hxclnote04.appendClassString("input_txt");
	sv.hxclnote04.appendClassString("highlight");
%>
	<%sv.hxclnote05.setClassString("");%>
<%	sv.hxclnote05.appendClassString("string_fld");
	sv.hxclnote05.appendClassString("input_txt");
	sv.hxclnote05.appendClassString("highlight");
%>
	<%sv.hxclnote06.setClassString("");%>
<%	sv.hxclnote06.appendClassString("string_fld");
	sv.hxclnote06.appendClassString("input_txt");
	sv.hxclnote06.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action");%>
	<%sv.action.setClassString("");%>
	<%sv.notemore.setClassString("");%>
<%	sv.notemore.appendClassString("string_fld");
	sv.notemore.appendClassString("output_txt");
	sv.notemore.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind11.isOn()) {
			sv.hxclnote01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind20.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.hxcllettyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.hxcllettyp.setReverse(BaseScreenData.REVERSED);
			sv.hxcllettyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.hxcllettyp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy No")%></label>

					<%					
					   if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Follow-up Code")%></label>
					<%					
		if(!((sv.fupcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
					longValue = null;
					formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Letter Type")%></label>
					<%	
						longValue = sv.hxcllettyp.getFormData();  
					%>
					<% 
						if((new Byte((sv.hxcllettyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						    ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
					<div
						class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
						<%if(longValue != null){%>

						<%=longValue%>

						<%}%>
					</div>
					<%
					   longValue = null;
					%>
					<% }else {%>
					<div class="input-group">
						<%=smartHF.getRichText(0, 0, fw, sv.hxcllettyp,(sv.hxcllettyp.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('hxcllettyp'));doAction('PFKEY04')">
							   <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
						<%
							}
						%>
					</div>
				</div>
			</div>

		<div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote01,(sv.hxclnote01.getLength()),null)%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class=form-group>
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote02,(sv.hxclnote02.getLength()),null)%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote03,(sv.hxclnote03.getLength()),null)%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote04,(sv.hxclnote04.getLength()),null)%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote05,(sv.hxclnote05.getLength()),null)%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <%=smartHF.getRichText(0, 0, fw, sv.hxclnote06,(sv.hxclnote06.getLength()),null)%>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Action")%>
					</label>
					<div class="input-group">
							<%
							   if ((new Byte((sv.action).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
		
								if (((sv.action.getFormData()).toString()).trim()
										.equalsIgnoreCase("A")) {
									longValue = resourceBundleHandler
											.gettingValueFromBundle("Add");
								}
								if (((sv.action.getFormData()).toString()).trim()
										.equalsIgnoreCase("M")) {
									longValue = resourceBundleHandler
											.gettingValueFromBundle("Modify");
								}
								if (((sv.action.getFormData()).toString()).trim()
										.equalsIgnoreCase("D")) {
									longValue = resourceBundleHandler
											.gettingValueFromBundle("Delete");
								}
					           %>
							<% 
							if((new Byte((sv.action).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								    ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>
							<div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
								    "blank_cell" : "output_cell" %>'>
								<%if(longValue != null){%>
							
									<%=longValue%>
								<%} %>
							</div>
							<%
							longValue = null;
							%>
							<% }else { %>
							<% if("red".equals((sv.action).getColor())){%>
							<div
								style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%}%>

								<select name='action'
									onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
									onKeyUp='return checkMaxLength(this)'
									<%
										if((new Byte((sv.action).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>
										 readonly="true" disabled class="output_cell"
																		<%
									}else if((new Byte((sv.action).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>
																		class="bold_cell" <%
									} else {
									%> class='form-controll' <%
									}
									%>>

									<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
									</option>
									<option value="A"
										<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%>
										Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Add")%></option>
									<option value="M"
										<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("M")) {%>
										Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></option>
									<option value="D"
										<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("D")) {%>
										Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></option>

								</select>
								<% if("red".equals((sv.action).getColor())){%>
							</div>
							<%
							} 
							%>
							<%					
							}longValue = null;} 
							%>
							<%
								if(!((sv.notemore.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.notemore.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.notemore.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
							%>
							<span class="input-group-addon" id="sizing-addon1"><%=formatValue%></span>
							<%
							longValue = null;
							formatValue = null;
							%>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
	</div>
</div>

<%if (sv.Sr591protectWritten.gt(0)) {%>
		<%Sr591protect.clearClassString(sv);%>

		<%}%>
		<%@ include file="/POLACommon2NEW.jsp"%>