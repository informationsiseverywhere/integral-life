

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH535";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh535ScreenVars sv = (Sh535ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Subroutine ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.optionChange.setReverse(BaseScreenData.REVERSED);
			sv.optionChange.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.optionChange.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
<div class="panel-body">
	<div class="row">
	<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	
	<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>

	<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	<table>
	<tr>
	<td>
	
	<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
  
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:225px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
	</tr>
	</table>

</div>
</div>
	</div>
	
	<div class="row">
	<div class="col-md-3">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Processing Subroutine")%></label>
	<input name='optionChange' 
		type='text' style="margin-top: 0.5px;width: 105px;"
		
		<%
		
				formatValue = (sv.optionChange.getFormData()).toString();
		
		%>
			value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
		
		size='<%= sv.optionChange.getLength()%>'
		maxLength='<%= sv.optionChange.getLength()%>' 
		onFocus='doFocus(this)' onHelp='return fieldHelp(optionChange)' onKeyUp='return checkMaxLength(this)'  
		
		
		<% 
			if((new Byte((sv.optionChange).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
			readonly="true"
			class="output_cell"
		<%
			}else if((new Byte((sv.optionChange).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		%>	
				class="bold_cell" 
		
		<%
			}else { 
		%>
		
			class = ' <%=(sv.optionChange).getColor()== null  ? 
					"input_cell" :  (sv.optionChange).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>'
		 
		<%
			} 
		%>
		>
	</div>
	</div>
	</div>

</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
