<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sjl47";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.contractservicing.screens.*" %>

<%
	Sjl47ScreenVars sv = (Sjl47ScreenVars) fw.getVariables();
%>

<%
	if (appVars.ind01.isOn()) {
		sv.lincsubr.setReverse(BaseScreenData.REVERSED);
		sv.lincsubr.setColor(BaseScreenData.RED);
	}
%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	        		<%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	        		<%					
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
						}
						else  {		
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	        		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	        		<table>
		        		<tr>
		        			<td>
				        		<%					
									if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td>
								<%					
									if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}							
									}
									else  {		
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									}
								%>
								<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>
	        	</div>
	        </div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
				<table>
					<tr>
						<td>
							<div class="form-group">
								<div class="input-group"">
									<input name='lincsubr' id="lincsubr" type='text' value='<%=sv.lincsubr.getFormData()%>' maxLength='<%=sv.lincsubr.getLength()%>' 
										size='<%=sv.lincsubr.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(lincsubr)' onKeyUp='return checkMaxLength(this)'  
										<% 
											if((new Byte((sv.lincsubr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
										%>	readonly="true"	class="output_cell"	
										<%
											}else if((new Byte((sv.lincsubr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>	class="bold_cell" 
										<%
											}else{
										%>
											class = '<%=(sv.lincsubr).getColor() == null ? "input_cell" : (sv.lincsubr).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
										<%	
											}
										%>
									/>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Record Type")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Change Type")%></label>
			</div>
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-5">
				<label><%=resourceBundleHandler.gettingValueFromBundle("NB, Termination, Add Rider and Increase SA")%></label>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='rectype01' id="rectype01" type='text' value='<%=sv.rectype01.getFormData()%>' maxLength='<%=sv.rectype01.getLength()%>' 
							size='<%=sv.rectype01.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(rectype01)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.rectype01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.rectype01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.rectype01).getColor() == null ? "input_cell" : (sv.rectype01).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='chgtype01' id="chgtype01" type='text' value='<%=sv.chgtype01.getFormData()%>' maxLength='<%=sv.chgtype01.getLength()%>' 
							size='<%=sv.chgtype01.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(chgtype01)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.chgtype01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.chgtype01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.chgtype01).getColor() == null ? "input_cell" : (sv.chgtype01).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Decrease SA & fulfil LINC Registration Rules")%></label>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='rectype02' id="rectype02" type='text' value='<%=sv.rectype02.getFormData()%>' maxLength='<%=sv.rectype02.getLength()%>' 
							size='<%=sv.rectype02.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(rectype02)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.rectype02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.rectype02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.rectype02).getColor() == null ? "input_cell" : (sv.rectype02).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='chgtype02' id="chgtype02" type='text' value='<%=sv.chgtype02.getFormData()%>' maxLength='<%=sv.chgtype02.getLength()%>' 
							size='<%=sv.chgtype02.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(chgtype02)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.chgtype02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.chgtype02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.chgtype02).getColor() == null ? "input_cell" : (sv.chgtype02).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Decrease SA & not fulfil LINC Registration Rules")%></label>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='rectype03' id="rectype03" type='text' value='<%=sv.rectype03.getFormData()%>' maxLength='<%=sv.rectype03.getLength()%>' 
							size='<%=sv.rectype03.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(rectype02)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.rectype03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.rectype03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.rectype03).getColor() == null ? "input_cell" : (sv.rectype03).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group"">
						<input name='chgtype03' id="chgtype03" type='text' value='<%=sv.chgtype03.getFormData()%>' maxLength='<%=sv.chgtype03.getLength()%>' 
							size='<%=sv.chgtype03.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(chgtype03)' onKeyUp='return checkMaxLength(this)'  
							<% 
								if((new Byte((sv.chgtype03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>	readonly="true"	class="output_cell"	
							<%
								}else if((new Byte((sv.chgtype03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	class="bold_cell" 
							<%
								}else{
							%>
								class = '<%=(sv.chgtype03).getColor() == null ? "input_cell" : (sv.rectype03).getColor().equalsIgnoreCase("red") ? "input_cell red reverse" : "input_cell" %>'
							<%	
								}
							%>
						/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>