<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6226";%> 
<%@ include file="/POLACommon1NEW.jsp"%> 
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%S6226ScreenVars sv = (S6226ScreenVars) fw.getVariables();%>

	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind36.isOn()) {
			sv.asgnsel.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.asgnsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind36.isOn()) {
			sv.asgnsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.asgnsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind37.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.commfromDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.commfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.commfromDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.commfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.commtoDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.commtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.commtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.commtoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                                              ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                                              ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  Client");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                    ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"<-------Assignment------->");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assignee Name");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                 ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"       ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ----------------------------------------------------------------------");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
<%		appVars.rollup(new int[] {93});
%>

				<%
				GeneralTable sfl = fw.getTable("s6226screensfl");
				
				%>
 
<div class="panel panel-default">
	 <div class="panel-body">
	       <div class="row">		
		 		<div class="col-md-12">
		 		<div class=""> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s6226' width='100%'> 
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="max-width: 70px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
		           		<th style="min-width: 240px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%>/<%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>									
						<th style="max-width: 100px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
						<th style="max-width: 100px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
						<th style="max-width: 100px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
						</tr>	
			         	</thead>
			         		         	
			         
					    
			         	<script language="javascript">
				        $(document).ready(function(){
					
				        	var rows = $("table[id='s6226Table']").find("tr:not(:hidden):gt(1)").length + 2;
							var isPageDown = 1;
							var pageSize = 1;
							var fields = new Array("asgnsel");
							operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"dataTables-s6226",null,1);	
				        	
			

      					  });
    					</script>
					      <tbody>
					      <%
					      String backgroundcolor="#FFFFFF";
								
								S6226screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								double sflLine = 0.0;
								int doingLine = 0;
								int sflcols = 1;
								int linesPerCol = 6;
								Map<String,Map<String,String>> reasonCdMap = appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
								
								while (S6226screensfl.hasMoreScreenRows(sfl)) {
								
									{
										if (appVars.ind36.isOn()) {
											sv.asgnsel.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind26.isOn()) {
											sv.asgnsel.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind36.isOn()) {
											sv.asgnsel.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind36.isOn()) {
											sv.asgnsel.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind27.isOn()) {
											sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind37.isOn()) {
											sv.reasoncd.setReverse(BaseScreenData.REVERSED);
											sv.reasoncd.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind37.isOn()) {
											sv.reasoncd.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind21.isOn()) {
											sv.commfromDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind11.isOn()) {
											sv.commfromDisp.setReverse(BaseScreenData.REVERSED);
											sv.commfromDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind11.isOn()) {
											sv.commfromDisp.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind22.isOn()) {
											sv.commtoDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind12.isOn()) {
											sv.commtoDisp.setReverse(BaseScreenData.REVERSED);
											sv.commtoDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind12.isOn()) { 
											sv.commtoDisp.setHighLight(BaseScreenData.BOLD);
										}
									}  
								   
							%> 
							
							<tr style="background:<%= backgroundcolor%>;  <%if (((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
		fw.getVariables().isScreenProtected()) && count>0 && sv.asgnsel.getFormData().trim().equals("")) 
		{%>visibility: hidden; display: none; <%}else if(count>1 && sv.asgnsel.getFormData().trim().equals("")){%>visibility: hidden; display: none;
		<% } %>" id='<%="tablerow"+count%>' >
			<td style="color:#434343; padding: 5px;width:150px; border-right: 1px solid #dddddd;" align="center">
				<INPUT type="checkbox" name="chk_R" id='<%="chk_R"+count %>' class="UICheck" />
			</td> 
					<td style="color:#434343; padding: 5px;width:300px;" align="left">	
					<div class="form-group">													
						<!-- added for ILIFE-549 -->
						<% if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
								fw.getVariables().isScreenProtected()){
						%>
						<%=sv.asgnsel.getFormData()%>&nbsp;<%= sv.assigneeName.getFormData()%>
						<%}else{%>
						<div class="input-group">
							<input name='<%="s6226screensfl.asgnsel_R" + count %>'
							id='<%="s6226screensfl.asgnsel_R" + count %>'
							type='text' 
							value='<%= sv.asgnsel.getFormData() %>' 
							class = " <%=(sv.asgnsel).getColor()== null  ? 
							"input_cell" :  
							(sv.asgnsel).getColor().equals("red") ? 
							"input_cell red reverse" : 
							"input_cell" %>" 
							maxLength='<%=sv.asgnsel.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(s6226screensfl.asgnsel)' onKeyUp='return checkMaxLength(this)' 
							 style = "width: 70px !important;"
							>		
										
							<%-- <a href="javascript:;" 
							onClick="doFocus(document.getElementById('<%="s6226screensfl" + "." +
							 "asgnsel" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
							
							<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
							</a> --%>
						<span class="input-group-btn">
						<button class="btn btn-info"							
							type="button"
							onClick="doFocus(document.getElementById('<%="s6226screensfl" + "." +
							 "asgnsel" + "_R" + count %>')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
						</span>
						
						<%= sv.assigneeName.getFormData()%>
						</div>
						<!-- added for ILIFE-549  -->
						<%}%>
						</div>
					</td>			
					
				<td class="tableDataTag" style="width:200px;" 
					<%if((sv.reasoncd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	
					<div class="form-group">					
						<%if((new Byte((sv.reasoncd).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>					
				    <%	
						mappedItems = (Map) reasonCdMap.get("reasoncd");
						optionValue = makeDropDownList1( mappedItems , sv.reasoncd,2,resourceBundleHandler);						
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());						 
					%>
					<% if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					<!-- modified for ILIFE-549 -->
					<%if(longValue != null){%>
						<div class='output_cell'> 
					   		<%=longValue%>
						</div>
					<%} %>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:2px; border-style: solid; border-color: #ec7572; "> 
					<%
					} 
					%>
					<select name='<%="s6226screensfl.reasoncd_R"+count%>' id='<%="s6226screensfl.reasoncd_R"+count%>' type='list' style="width:200+45px;"
					
					class = 'input_cell'
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>			
				<%}%>
				</div>
			</td>
			
					
					<td style="color:#434343; width:150px;font-weight: bold;">
					<div class="form-group">									
						<!-- added for ILIFE-549 --> 
						<% if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
								fw.getVariables().isScreenProtected()){
						%>
						<%=sv.commfromDisp.getFormData()%>
						<%}else{%>						
						<%-- <input name='<%="s6226screensfl.commfromDisp_R" + count %>'
							id='<%="s6226screensfl.commfromDisp_R" + count %>'
							type='text' 
							value='<%=sv.commfromDisp.getFormData() %>' 
							class = " <%=(sv.commfromDisp).getColor()== null  ? 
							"input_cell" :
							(sv.commfromDisp).getColor().equals("red") ? 
							"input_cell red reverse" : 
							"input_cell" %>" 
							maxLength='<%=sv.commfromDisp.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(s6226screensfl.commfromDisp)' onKeyUp='return checkMaxLength(this)'  
							class = "input_cell"
							style = "width: <150-iwidth>px;"
							>
							
							<a href="javascript:;" 
							onClick="showCalendar(this, document.getElementById(
							'<%="s6226screensfl" + "." +
							 "commfromDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
							 
							 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
							</a> --%>
							
							
							<div class="input-group date form_date col-md-12 three-controller"
	                             data-date="" data-date-format="dd/MM/yyyy"
	                             data-link-field="commfromDisp" data-link-format="dd/mm/yyyy">
	                            <%=smartHF.getRichTextDateInput(fw, sv.commfromDisp)%>
	                            <span class="input-group-addon"><span
                                    class="glyphicon glyphicon-calendar"></span></span>
                        	</div>
							
						<!-- added for ILIFE-549 -->
						<%}%>
						</div>	
					</td>	
					
							
				 	
					<td style="color:#434343;  width:150px;font-weight: bold;">	
					<div class="form-group">								
						<!-- added for ILIFE-549 -->
						<% if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
								fw.getVariables().isScreenProtected()){
						%>
						<%=sv.commtoDisp.getFormData()%>
						<%}else{%>							
						<%-- <input name='<%="s6226screensfl.commtoDisp_R" + count %>'
						id='<%="s6226screensfl.commtoDisp_R" + count %>'
						type='text' 
						value='<%=sv.commtoDisp.getFormData() %>' 
						class = " <%=(sv.commtoDisp).getColor()== null  ? 
						"input_cell" :
						(sv.commtoDisp).getColor().equals("red") ? 
						"input_cell red reverse" : 
						"input_cell" %>" 
						maxLength='<%=sv.commtoDisp.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(s6226screensfl.commtoDisp)' onKeyUp='return checkMaxLength(this)'  
						class = "input_cell"
						style = "width: <150-iwidth>px;"
						> --%>
						
						<%-- <a href="javascript:;" 
						onClick="showCalendar(this, document.getElementById(
						'<%="s6226screensfl" + "." +
						 "commtoDisp" + "_R" + count %>'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
						 
						 <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
						</a> --%>
						
						
						<div class="input-group date form_date col-md-12 three-controller"
                             data-date="" data-date-format="dd/MM/yyyy"
                             data-link-field="commtoDisp" data-link-format="dd/mm/yyyy">
                            <%=smartHF.getRichTextDateInput(fw, sv.commtoDisp)%>
                            <span class="input-group-addon"><span
                                    class="glyphicon glyphicon-calendar"></span></span>
                        </div>
						
						
					  <!-- added for ILIFE-549 -->
					  <%}%>	
					  </div>
					</td>		
					
	
					</tr>
								<%
								
									sflLine += 1;
									doingLine++;
									if (doingLine % linesPerCol == 0 && sflcols > 1) {
										sflLine = 0.0;
									}
									count = count+1;
									
									S6226screensfl.setNextScreenRow(sfl, appVars, sv);
										
									} 
									%>
									
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
 	      <div class="row">      
 	      
 	      	
			<div class="col-md-4">
				<div class="form-group">
					<div class="btn-group">
						<div class="sectionbutton">
							<div style="font-size: 12px; font-weight: bold;">
								<a id="subfile_add" class="btn btn-success" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<a id="subfile_remove" class="btn btn-danger"
									href='javascript:;' disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
 	      
        		
        	</div> 
	 </div>

<script>
$(document).ready(function() {
	$('#dataTables-s6226').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
        orderable: false,
  	});
})
</script>

<script>
 $(document).ready(function(){

    $("#subfile_remove").click(function () {
    	
		$('input:checkbox[type=checkbox]').prop('checked',false);
	    	
	   	$("#subfile_remove").attr('disabled', 'disabled'); 

    }); 
    
    
    $('input[type="checkbox"]'). click(function(){
    	if($(this). prop("checked") == true){
    		 $('#subfile_remove').removeAttr('disabled');
    	}else{
    		 $('#subfile_remove').attr("disabled","disabled");   
    	}
    
    });
    
  });	 
</script>



<%@ include file="/POLACommon2NEW.jsp"%>

