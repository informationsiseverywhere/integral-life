

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5005";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*"%>

<%
	appVars.rollup();
	appVars.rolldown();
%>
<%
	S5005ScreenVars sv = (S5005ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract number ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life no ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint life no ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sex ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Date of birth ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age admitted");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Medical evidence ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Smoking ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Occupation ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Pursuit code ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Relationship ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Height/Weight ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "/");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "BMI ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Use roll keys to display joint life details");
	String occupOptions = "";
	String industryOptions = "";
%>

<%
	{
		if (appVars.ind51.isOn()) {
			generatedText4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind51.isOn()) {
			sv.jlife.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.lifesel.setReverse(BaseScreenData.REVERSED);
			sv.lifesel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind13.isOn()) {
			sv.lifesel.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.lifesel, appVars.ind50);
		if (!appVars.ind01.isOn()) {
			sv.lifesel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.sex.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.sex.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.sex.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.sex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.dobDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.dobDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.dobDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.dobDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.dummy.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.anbage.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.anbage.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.anbage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.anbage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.selection.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.selection.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.selection.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.selection.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.smoking.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.smoking.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.smoking.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.smoking.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.occup.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.occup.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.occup.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.occup.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.pursuit01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.pursuit01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.pursuit01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.pursuit01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.pursuit02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.pursuit02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.pursuit02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.pursuit02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.relation.setReverse(BaseScreenData.REVERSED);
			sv.relation.setColor(BaseScreenData.RED);
		}
		if (appVars.ind52.isOn()) {
			sv.relation.setInvisibility(BaseScreenData.INVISIBLE);
			sv.relation.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.relation.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.rollit.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind53.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind25.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.height.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.height.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.height.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.height.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.height.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.weight.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.weight.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.weight.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind24.isOn()) {
			sv.weight.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.weight.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.bmi.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.optdsc02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind15.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.optdsc03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind29.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind17.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.optind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
		}
		//fwang3 
		if (appVars.ind30.isOn()) {
			sv.relationwithowner.setColor(BaseScreenData.RED);
			sv.relationwithowner.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.relationwithowner.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.relationwithowner.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.relationwithowner.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.optind04.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ICIL-160
		if (appVars.ind62.isOn()) {
			sv.occupationClass.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.industry.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.industry.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind38.isOn()) {
			sv.industry.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.industry.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
/* .input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

table, tr {
	width: 100% !important;
} */

/* @media screen and (-webkit-min-device-pixel-ratio: 1) and
	(max-device-width: 1600px) and (min-device-width: 1400px) {
	form[name="form1"]>div.row>div.col-md-9 {
		width: 86% !important;
		top: 15px;
	}
} */
</style>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					<div id="chdrnumDiv" class='<%=(sv.chdrnum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
					<div
						class='<%=(sv.life.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life no")%></label>
					<div
						class='<%=(sv.jlife.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-4">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
				<table>
					<tr class="input-group">
						<td>
							<%
								if ((new Byte((sv.lifesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| fw.getVariables().isScreenProtected()) {
							%> <%=smartHF.getHTMLVarExt(fw, sv.lifesel)%> <%
						 	} else {
						 %>
							<div class="input-group" style="width: 130px;">
								<input name='lifesel' id='lifesel' type='text'
									value='<%=sv.lifesel.getFormData()%>'
									maxLength='<%=sv.lifesel.getLength()%>'
									size='<%=sv.lifesel.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(lifesel)'
									onKeyUp='return checkMaxLength(this)' style="width: 60px;"
									<%if ((new Byte((sv.lifesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
									readonly="true" class="output_cell">

								<%
									} else if ((new Byte((sv.lifesel).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
								%>
								class="bold_cell" >
								 <span class="input-group-btn">
									<button class="btn btn-info"
										style="bottom: 1px; font-size: 13px; border-bottom-width: 0px !important; top: 0px; right: -3px !important;"
										type="button"
										onClick="doFocus(document.getElementById('lifesel')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
								<%
									} else {
								%>

								class = '
								<%=(sv.lifesel).getColor() == null ? "input_cell"
							: (sv.lifesel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
									class="input-group-btn">
									<button class="btn btn-info"										
										type="button"
										onClick="doFocus(document.getElementById('lifesel')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
								<%
									}
								%>

							</div> 
							<%
							 	}
							 %>
						</td>

						<td>
							<div
								style="min-width: 100px; max-width: 105px; margin-left: -1px;"
								class='<%=(sv.lifename.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%> '>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
					</tr>
				</table>

			</div>
			<div class="col-md-4">
				<div class="form-group" style="width: 140px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "sex" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("sex");
						optionValue = makeDropDownList(mappedItems, sv.sex.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.sex.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.sex).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
					%>
					<div class='output_cell' style="min-width: 50px;">
						<%=longValue%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
					<select name='sex' type='list' id='sex'
						<%if ((new Byte((sv.sex).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						style="min-width: 50px;"
						<%} else if ((new Byte((sv.sex).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="min-width:50px;" <%} else {%>
						class=' <%=(sv.sex).getColor() == null ? "input_cell"
							: (sv.sex).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						}
					%>
				</div>
			</div>
<!-- fwang3 ICIL-4 -->
			<%
				if ((new Byte((sv.relationwithowner).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-4">
				<div class="form-group" style="width: 190px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Owner")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "relationwithowner" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("relationwithowner");
						optionValue = makeDropDownList(mappedItems, sv.relationwithowner.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.relationwithowner.getFormData()).toString().trim());
					%>

					<% 
						if((new Byte((sv.relationwithowner).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
						<div class='output_cell' style="width: 190px;">
							<%=longValue==null?"":longValue%>
						</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<select name='relationwithowner' type='list' id="relationwithowner"
								onFocus='doFocus(this)'
                                onHelp='return fieldHelp(relationwithowner)'
                                onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.relationwithowner).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="min-width:220px;" 
						<%}else if("red".equals((sv.relationwithowner).getColor())){%>
							class= "input_cell red reverse" 
						<%} else {%>
							class="input_cell"
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						optionValue=null;
						}
					%>
				</div>
			</div>
					<%
						}
					%>			
<!-- fwang3 ICIL-4 END-->

		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date of birth")%></label>
					<%
						if ((new Byte((sv.dobDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 80px"><%=smartHF.getRichTextDateInput(fw, sv.dobDisp, (sv.dobDisp.getLength()))%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.dobDisp, (sv.dobDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>

				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.anbage).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
					%>
					<input name='anbage' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.anbage)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.anbage);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.anbage)%>' <%}%>
						size='<%=sv.anbage.getLength()%>'
						maxLength='<%=sv.anbage.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(anbage)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.anbage).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="width: 50px;"
						<%} else if ((new Byte((sv.anbage).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="width:50px;" <%} else {%>
						class=' <%=(sv.anbage).getColor() == null ? "input_cell"
						: (sv.anbage).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Medical evidence")%></label>
					<div style="width: 210px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "selection" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("selection");
							optionValue = makeDropDownList(mappedItems, sv.selection.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.selection.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.selection).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
						%>
						<div class='output_cell' >
							<%=longValue%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<%=smartHF.getDropDownExt(sv.selection, fw, longValue, "selection", optionValue, 0)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "smoking" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("smoking");
						optionValue = makeDropDownList(mappedItems, sv.smoking.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.smoking.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.smoking).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
					%>
					<!-- smalchi2 for ILIFE-811 STARTS -->
					<div style="width: 110px;"
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<!-- ENDS -->
					<%
						longValue = null;
					%>
					<%
						} else {
					%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
					<select name='smoking' type='list' id='smoking'
						<%if ((new Byte((sv.smoking).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.smoking).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.smoking).getColor() == null ? "input_cell"
							: (sv.smoking).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group" style="max-width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "occup" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("occup");
						optionValue = makeDropDownList(mappedItems, sv.occup.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.occup.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.occup).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
					%>
					<!-- smalchi2 for ILIFE-811 STARTS -->
					<div
						class='<%=(sv.occup.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
						if (!((sv.occup.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occup.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occup.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
					%>
					<!-- ENDS -->
					<%
						} else {
							occupOptions = optionValue;
					%><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  -->
					<select style="width: 230px;" name='occup' type='list' id='occup'
						<%if ((new Byte((sv.occup).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.occup).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.occup).getColor() == null ? "input_cell"
							: (sv.occup).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						<%=optionValue%>
					</select>
					<%
						}
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit code")%></label>
					<table>
						<tr>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "pursuit01" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("pursuit01");
									optionValue = makeDropDownList(mappedItems, sv.pursuit01.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.pursuit01.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.pursuit01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
 %> <!-- smalchi2 for ILIFE-811 STARTS -->
								<div style="width: 100px;"
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <%
 	} else {
 %><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  --> 
 <select
								style="max-width: 145px;" name='pursuit01' type='list'
								id='pursuit01'
								<%if ((new Byte((sv.pursuit01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.pursuit01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.pursuit01).getColor() == null ? "input_cell"
							: (sv.pursuit01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
									<%=optionValue%>
							</select> <%
 	}
 %>
							</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "pursuit02" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("pursuit02");
									optionValue = makeDropDownList(mappedItems, sv.pursuit02.getFormData(), 2, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.pursuit02.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.pursuit02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
 %> <!-- smalchi2 for ILIFE-811 STARTS -->
								<div style="margin-left: 1px;width: 100px;"
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <!-- ENDS --> <%
 	} else {
 %><!-- Ticket-ILIFE-2143 by liwei 2016.3.4  --> <select
								style="max-width: 145px; margin-left: 1px;" name='pursuit02'
								type='list' id='pursuit02'
								<%if ((new Byte((sv.pursuit02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.pursuit02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.pursuit02).getColor() == null ? "input_cell"
							: (sv.pursuit02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
									<%=optionValue%>
							</select> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>



			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Relationship")%></label>
					<%
						}
					%>

					<%
						if ((new Byte((sv.relation).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[] { "relation" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("relation");
							optionValue = makeDropDownList(mappedItems, sv.relation.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.relation.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.relation).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<!-- smalchi2 for ILIFE-811 STARTS -->
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<!-- ENDS -->
					<%
						} else {
					%>
					<%=smartHF.getDropDownExt(sv.relation, fw, longValue, "relation", optionValue, 0)%>
					<%
						}
						}
					%>
					<%
						longValue = null;
					%>

				</div>
				<!-- </div> -->
			</div>
			<% if(1 == sv.uwFlag){ %>
			<div class="col-md-4">				
			<div class="form-group" class="combo-select" style="width: 230px;">
				<% if ((new Byte((sv.industry).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Industry")%></label>
					<%fieldItem = appVars.loadF4FieldsLong(new String[]{"industry"}, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("industry");
					optionValue = makeDropDownList(mappedItems, sv.industry.getFormData(), 2, resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.industry.getFormData()).toString().trim());
				%>
				<% if ((new Byte((sv.industry).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
						  fw.getVariables().isScreenProtected()) { %>
				<div class='<%=(sv.industry.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
					<%
					if (!((sv.industry.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.industry.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
						} else {
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.industry.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					}
				%>
					<%=formatValue==null?"":XSSFilter.escapeHtml(formatValue)%>
				</div>

				<%
				longValue = null;
			%>

				<%
				} else {
					industryOptions = optionValue;
			%>
				<select name='industry' id='industry' type='list'
					style="width: 230px;"
					<%if ((new Byte((sv.industry).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.industry).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell <%=sv.industry.getColor()%>' <%}%>>
					<%=optionValue%>
				</select>
				<% } 
				longValue = null;
				%>
				
			<% } %>
			</div>
			</div>
			<% }else{ %>
			<div class="col-md-4">
				<div class="form-group">
				    <%
						if ((new Byte((sv.occupationClass).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0) {
							fieldItem = appVars.loadF4FieldsShort(new String[] { "occupationClass" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("occupationClass");
							optionValue = makeDropDownList(mappedItems, sv.occupationClass.toString(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get(sv.occupationClass.toString().trim());
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>
					<div style="width: 230px;" name='occupationClass'  id='occupationClass' class="output_cell">
			        <%=longValue==null?"":longValue%>
					</div>
					<%
					 }
				      longValue = null;
				    %>
				</div>	
		</div><%} %>
	</div>
		<!-- </div> -->

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Height/Weight")%></label>
					<table>
						<tr>
							<td>
							<%
									qpsf = fw.getFieldXMLDef((sv.height).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						
							%>

					<input name='height'
					type='text'
					
						value='<%=smartHF.getPicFormatted(qpsf,sv.height) %>'
								 <%
						 valueThis=smartHF.getPicFormatted(qpsf,sv.height);
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=smartHF.getPicFormatted(qpsf,sv.height) %>'
						 <%}%>
					
					size='<%= sv.height.getLength()%>'
					maxLength='<%= sv.height.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(height)' onKeyUp='return checkMaxLength(this)'
					style="width:64px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						
					
					<%
						if((new Byte((sv.height).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
					%>
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.height).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
							class="bold_cell"  	style="width:67px;"
					
					<%
						}else {
					%>
					
						class = ' <%=(sv.height).getColor()== null  ?
								"input_cell" :  (sv.height).getColor().equals("red") ?
								"input_cell red reverse" : "input_cell" %>'
								style="width:64px;"
					
					<%
						}
					%>
					>
							</td>
							<td>
								<%
											qpsf = fw.getFieldXMLDef((sv.weight).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								
									%>
								
								<input name='weight'
								type='text'
								
									value='<%=smartHF.getPicFormatted(qpsf,sv.weight) %>'
											 <%
									 valueThis=smartHF.getPicFormatted(qpsf,sv.weight);
									 if(valueThis!=null&& valueThis.trim().length()>0) {%>
									 title='<%=smartHF.getPicFormatted(qpsf,sv.weight) %>'
									 <%}%>
								
								size='<%= sv.weight.getLength()%>'
								maxLength='<%= sv.weight.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(weight)' onKeyUp='return checkMaxLength(this)'
									style="width:67px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									
								
								<%
									if((new Byte((sv.weight).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
								%>
									readonly="true"
									class="output_cell"
								<%
									}else if((new Byte((sv.weight).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>
										class="bold_cell" 	style="width:67px;"
								
								<%
									}else {
								%>
								
									class = ' <%=(sv.weight).getColor()== null  ?
											"input_cell" :  (sv.weight).getColor().equals("red") ?
											"input_cell red reverse" : "input_cell" %>
												style="width:67px;"'
								
								<%
									}
								%>
								>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("BMI")%></label>
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.bmi).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.bmi);
							if (!((sv.bmi.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}
							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-4" style="margin-bottom: 10px">
				<div class="btn-group" role="group" aria-label="...">
					<button type="button" onClick="JavaScript:doAction('PFKey90');"
						class="btn btn-primary"><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></button>
					<button style="margin-left: 2px" type="button"
						onClick="JavaScript:doAction('PFKey91');" class="btn btn-primary"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></button>

				</div>
			</div>
		</div>
	</div>
</div>
<div style='visibility: hidden; overflow: hidden; height: 3px;'>
	<%=smartHF.getHTMLDTTSHyperLinkVar(21, 8, fw, sv.optdsc02, sv.optind02)%>

	<%=smartHF.getHTMLTSAInputBoxVar(21, 27, fw, sv.optind02).replace("name='optind02'",
					"name='optind02' id='optind02' ")%>

	<%=smartHF.getHTMLDTTSHyperLinkVar(21, 33, fw, sv.optdsc03, sv.optind03)%>

	<%=smartHF.getHTMLTSAInputBoxVar(21, 50, fw, sv.optind03).replace("name='optind03'",
					"name='optind03' id='optind03' ")%>

	<%=smartHF.getHTMLDTTSHyperLinkVar(21, 61, fw, sv.optdsc01, sv.optind01)%>

	<%=smartHF.getHTMLTSAInputBoxVar(21, 77, fw, sv.optind01).replace("name='optind01'",
					"name='optind01' id='optind01' ")%>
	<%=smartHF.getHTMLDTTSHyperLinkVar(21, 92, fw, sv.optdsc04, sv.optind04)%>

	<%=smartHF.getHTMLTSAInputBoxVar(21, 108, fw, sv.optind04).replace("name='optind04'",
					"name='optind04' id='optind04' ")%>
</div>
<!-- ILIFE-2410 Life Cross Browser - Sprint 1 D1 : Task 1  -->
<%@ include file="/POLACommon2NEW.jsp"%>
<% if(1 == sv.uwFlag){ %>
	<LINK REL="StyleSheet" HREF="<%=ctx%>theme/combo.select.css" TYPE="text/css">  
	<script src="<%=ctx%>js/jquery.combo.select.js"></script>
	<script type="text/javascript">
	//  This will build an array to receive all the dropdown items
	var industryOccuMap = new Array();
	var occupOptionsStr ='<%=occupOptions.replace("'","\"")%>';
	var industryOptionsStr ='<%=industryOptions.replace("'","\"")%>';
<%
	Map<String, Map<String, String>> map = sv.getIndustryOccuMap();
	if (map != null && !map.isEmpty()) {
		for (String indukey : map.keySet()) {
			Map<String, String> occuMap = map.get(indukey);
			int i=0;
			%>
			industryOccuMap['<%=indukey%>'] = new Array();
			<% for (String occukey : occuMap.keySet()) { %>
				industryOccuMap['<%=indukey%>'][<%=i%>] = new Array('<%=occukey%>','<%=occuMap.get(occukey)%>');
			<% i++;
			} 
		} 
	}%>
	function changeIndustry(){
		var industryCode = $("select[name='industry']").val();
		var occuCode = $("select[name='occup']").val();
		if(industryCode == "" || typeof(industryOccuMap[industryCode]) == "undefined"){
			$("select[name='occup']").find('option').remove();
			$("select[name='occup']").html(occupOptionsStr);
			$("select[name='occup']").val(occuCode);
			if(occuCode == ""){
				$("select[name='industry']").html(industryOptionsStr);
				$("select[name='industry']").val("");
				$("select[name='industry']").css("display","");
				$("select[name='industry']").comboSelect({"inputClass":'form-control'});
				$("select[name='industry']").css("display","none");
			} 
		}else{
			$("select[name='occup']").find('option').remove();
			var occpDrop = $("select[name='occup']")[0];
			
			var count = 0;
			occpDrop.options[count] = new Option('<%=resourceBundleHandler.gettingValueFromBundle("---------Select---------")%>', "");
			count++;
			for (var i = 0; i < industryOccuMap[industryCode].length; i++) {
				if(industryOccuMap[industryCode][i][0] == occuCode){
					occpDrop.options[count] = new Option(industryOccuMap[industryCode][i][1], industryOccuMap[industryCode][i][0], true, true);
				}else{
					occpDrop.options[count] = new Option(industryOccuMap[industryCode][i][1], industryOccuMap[industryCode][i][0]);
				}
				count++;
			}
			removeUselessItem(industryCode);
		}
		$("select[name='occup']").css("display","");
		$("select[name='occup']").comboSelect({"inputClass":'form-control'});
		$("select[name='occup']").css("display","none");
	}
	function changeOccuCode(){
		var occuCode = $("select[name='occup']").val();
		if(occuCode != ""){
			var count = 0;
			for(var industryCode in industryOccuMap){
				for(var i=0;i<industryOccuMap[industryCode].length;i++){
					if(occuCode == industryOccuMap[industryCode][i][0]){
						removeUselessItem(industryCode);
						removeUselessOccu(industryCode);
						return;
					}
				}
			}
		} else {
			var industryCode = $("select[name='industry']").val();
			if(industryCode == "" || typeof(industryOccuMap[industryCode]) == "undefined"){
				$("select[name='industry']").html(industryOptionsStr);
				$("select[name='industry']").val("");
				$("select[name='industry']").css("display","");
				$("select[name='industry']").comboSelect({"inputClass":'form-control'});
				$("select[name='industry']").css("display","none");
				
				$("select[name='occup']").find('option').remove();
				$("select[name='occup']").html(occupOptionsStr);
				$("select[name='occup']").val("");
				$("select[name='occup']").css("display","");
				$("select[name='occup']").comboSelect({"inputClass":'form-control'});
				$("select[name='occup']").css("display","none");
			}
		}

	}
	function removeUselessOccu(industryCode){
		var occuCode = $("select[name='occup']").val();
		$("select[name='occup']").find('option').remove();
		var occpDrop = $("select[name='occup']")[0];
		var count = 0;
		occpDrop.options[count] = new Option('<%=resourceBundleHandler.gettingValueFromBundle("---------Select---------")%>', "");
		count++;
		for (var i = 0; i < industryOccuMap[industryCode].length; i++) {
			if(industryOccuMap[industryCode][i][0] == occuCode){
				occpDrop.options[count] = new Option(industryOccuMap[industryCode][i][1], industryOccuMap[industryCode][i][0], true, true);
			}else{
				occpDrop.options[count] = new Option(industryOccuMap[industryCode][i][1], industryOccuMap[industryCode][i][0]);
			}
			count++;
		}
		$("select[name='occup']").css("display","");
		$("select[name='occup']").comboSelect({"inputClass":'form-control'});
		$("select[name='occup']").css("display","none");
	}
	function initEmptyIndustry(occuCode){
		for(var industryCode in industryOccuMap){
			for(var i=0;i<industryOccuMap[industryCode].length;i++){
				if(occuCode == industryOccuMap[industryCode][i][0]){
					$("select[name='industry']").val(industryCode);
					var emptyOption = new Option('<%=resourceBundleHandler.gettingValueFromBundle("---------Select---------")%>', "");
					var selectedOption = new Option($("select[name='industry'] option:checked").text(), industryCode);
					$("select[name='industry']").find('option').remove();
					$("select[name='industry']")[0].options[0] = emptyOption;
					$("select[name='industry']")[0].options[1] = selectedOption;
					$("select[name='industry']").val("");
					$("select[name='industry']").css("display","");
					$("select[name='industry']").comboSelect({"inputClass":'form-control'});
					$("select[name='industry']").css("display","none");
					break;
				}
			}
		}
	}
	function removeUselessItem(industryCode){
		$("select[name='industry']").html(industryOptionsStr);
		$("select[name='industry']").val(industryCode);
		var emptyOption = new Option('<%=resourceBundleHandler.gettingValueFromBundle("---------Select---------")%>', "");
		var selectedOption = new Option($("select[name='industry'] option:checked").text(), industryCode);
		$("select[name='industry']").find('option').remove();
		$("select[name='industry']")[0].options[0] = emptyOption;
		$("select[name='industry']")[0].options[1] = selectedOption;
		$("select[name='industry']").val(industryCode);
		$("select[name='industry']").css("display","");
		$("select[name='industry']").comboSelect({"inputClass":'form-control'});
		$("select[name='industry']").css("display","none");
		//$("#industry").val($("select[name='industry'] option:checked").text());
	}
	$("select[name='industry']").change(changeIndustry);
	$("select[name='occup']").change(changeOccuCode);
	$("#industry").comboSelect();
	$("select[name='industry']").css("display","none");


	</script>
<% } %>
<script>
$(document).ready(function(){
	<% if(1 == sv.uwFlag){ %>
	changeIndustry();
	var industryCode = $("select[name='industry']").val();
	var occuCode = $("select[name='occup']").val();
	if(occuCode != "" && industryCode == "" ){
		initEmptyIndustry(occuCode);
	}
	<% if ((new Byte((sv.industry).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || 
			  fw.getVariables().isScreenProtected()) { %>
	$("#combo-select-id").css("margin-top","0px");
	<% }%>
	<% } %>
	$("#chdrnumDiv").css("width", "80px");
	
});
</script>
