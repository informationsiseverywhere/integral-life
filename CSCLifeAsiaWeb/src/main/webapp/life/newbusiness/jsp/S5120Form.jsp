<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5120";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S5120ScreenVars sv = (S5120ScreenVars) fw.getVariables();%>


<div class="panel panel-default">
    
         
    	<div class="panel-body">     
			<div class="row">	
			    
					    
					
					<div class="col-md-2">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							</div>
							</div>  
							<div class="col-md-5">
							<div class="input-group">
								<input class="form-group" style="min-width:90px;" type="text" placeholder='<%=sv.chdrnum.getFormData().toString()%>' disabled>
								<div class="input-group-addon"><%=sv.cnttype%></div>
								<div class="input-group-addon" style="max-width:400px;"><%=sv.ctypedes%></div>
							</div>
						</div>											
					   </div>  
					  
					    
					  
					  
					  <div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label> 
					 </div>
				</div>	 
				<div class="col-md-2"> 
				    		<div class="form-group">
				    		<div class="input-group"> 
				    		<%=smartHF.getHTMLVarExt(fw, sv.rstate)%>

	<%=smartHF.getHTMLVarExt(fw, sv.pstate)%>
	</div>
				    		</div>
				    	</div>	
				    	
				    <div class="col-md-3"> </div>
				    	
				    	<div class="col-md-1"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
				    		
				    		</div>
				    		</div>
				    		<div class="col-md-2"> 
				    		<div class="form-group">
				    		
				    		<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.occdateDisp)%> --%>
	                        <%=smartHF.getHTMLCalNSVar(fw, sv.occdateDisp)%>
				    	
				    	</div>	
				</div>
	</div>
	
	           <div class="row">	
			    	<div class="col-md-1"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label> 
					 </div>
				</div>	 
				<div class="col-md-2"> 
				    		<div class="form-group">
				    		  <div class="input-group exchange_addon"> 
				    		<%=smartHF.getHTMLVarExt(fw, sv.cownnum)%>

	<%=smartHF.getHTMLVarExt(fw, sv.ownernum)%>
				    		</div>
				    		</div>
				    	</div>
				    </div>		
				    
				     <div class="row">	
			    	<div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label> 
					 </div>
				</div>	 
				<div class="col-md-2"> 
				    		<div class="form-group">
				    		
				  <%--   <%=smartHF.getHTMLSpaceVar(fw, sv.ptdateDisp)%> --%>
	<%=smartHF.getHTMLCalNSVar(fw, sv.ptdateDisp)%>
	                       
	                     </div>
	              </div>
	              
	               <div class="col-md-2"> </div>
	               
	              <div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Bill-to-date ")%></label> 
					 </div>
				</div>	 
				<div class="col-md-2"> 
				    		<div class="form-group">
				    		<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.btdateDisp)%> --%>
	<%=smartHF.getHTMLCalNSVar(fw, sv.btdateDisp)%>
				    		</div>
				    		</div>
	              
	              
	              
	              </div>
	              
	              <div class="row">	
			    	<div class="col-md-12"> 
				    		<div class="form-group">  	  
					    	<label><%=resourceBundleHandler.gettingValueFromBundle("---------------------------- List of Lives -----------------------------------")%></label> 	
					 </div>
				</div>
				</div>	 
				
				 <div class="row">	
				 <div class="col-md-1"> </div>
			    	<div class="col-md-1"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;Life")%></label> 
				    		</div>
				    	</div>	
				    	
				    	<div class="col-md-1"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;J/L")%></label> 
				    		</div>
				    	</div>
				    	
				    	<div class="col-md-1"> 
				    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;Client")%></label> 
				    		</div>
				    	</div>
	</div>
	
	 <div class="row">	
	                <div class="col-md-1"> </div>
				 <div class="col-md-1"> 
				 <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;No ")%></label> 
				 </div>
	</div>		
	
	<div class="col-md-1"> 
				 <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;No ")%></label> 
				 </div>
	</div>		
	
	<div class="col-md-2"> 
				 <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;No ")%></label> 
				 </div>
	</div>	
	
	<div class="col-md-2"> 
				 <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name ")%></label> 
				 </div>
	</div>	
	
	<div class="col-md-3"> </div>
	
	<div class="col-md-2"> 
				 <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle(" &nbsp;Status ")%></label> 
				 </div>
	</div>	
	
	</div>
	<%if (sv.S5120screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s5120screensfl");
	savedInds = appVars.saveAllInds();
	S5120screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 10;
	String height = smartHF.fmty(10);
	smartHF.setSflLineOffset(12);
	%>
	
	<%while (S5120screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.select.setClassString("");%>
<%	sv.select.appendClassString("string_fld");
	sv.select.appendClassString("input_txt");
	sv.select.appendClassString("highlight");
%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.lifcnum.setClassString("");%>
<%	sv.lifcnum.appendClassString("string_fld");
	sv.lifcnum.appendClassString("output_txt");
	sv.lifcnum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%sv.stdescsh.setClassString("");%>
<%	sv.stdescsh.appendClassString("string_fld");
	sv.stdescsh.appendClassString("output_txt");
	sv.stdescsh.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>

	<%
{
	}

	%>
	
		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S5120screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%
{
		appVars.rollup(new int[] {93});
	}

	%>
	
	 <div class="row">	
	                <div class="col-md-1"> 
	                 <div class="form-group">
	                 <%=smartHF.getHTMLVarExt(fw, sv.select)%>
	                
	                 </div>
	                 </div>
	               
				 <div class="col-md-1"> 
				 <div class="form-group">
				 <%=smartHF.getHTMLVarExt(fw, sv.life)%>
				 </div>
				 </div>
				 
				  <div class="col-md-1"> 
				 <div class="form-group">
				 <%=smartHF.getHTMLVarExt(fw, sv.jlife)%>
				 </div>
				 </div>
				 
				  
				  <div class="col-md-2"> 
				 <div class="form-group">
				 <div class="input-group" style="max-width:100px;"> 
				 <%=smartHF.getHTMLVarExt(fw, sv.lifcnum)%>
				 </div>
				 </div>
				 </div>
				 
				 <div class="col-md-2"> 
				 <div class="form-group">
				  <div class="input-group" style="max-width:200px;"> 
				 <%=smartHF.getHTMLVarExt(fw, sv.lifename)%>
				 </div>
				 </div>
				 </div>
				 
				  <div class="col-md-3"> </div>
				 <div class="col-md-2"> 
				 <div class="form-group">
				  <div class="input-group" style="max-width:150px;"> 
				 <%=smartHF.getHTMLVarExt(fw, sv.stdescsh)%>
				 </div>
				 </div>
				 </div>
	 </div>
</div>
</div>		

<!-- <script type="text/javascript">
$(document).ready(function() {
	exchangeAddon=("exchange_addon");
});
</script> 	 -->

<script type="text/javascript">

	$(document).ready(function() {
    	exchangeAddon(".exchange_addon");
      	});

</script>   		


<%@ include file="/POLACommon2NEW.jsp"%>
