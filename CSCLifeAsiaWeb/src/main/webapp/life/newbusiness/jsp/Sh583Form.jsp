<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH583";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%
	appVars.rollup();
	appVars.rolldown();
%>
<%Sh583ScreenVars sv = (Sh583ScreenVars) fw.getVariables();%>

<%if (sv.Sh583screenWritten.gt(0)) {%>
	<%Sh583screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy No ");%>

	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow-up Code ");%>
	
	<%sv.fupcode.setClassString("");%>
<%	sv.fupcode.appendClassString("string_fld");
	sv.fupcode.appendClassString("output_txt");
	sv.fupcode.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Letter Type ");%>
	
	<%sv.hxcllettyp.setClassString("");%>
	<%sv.hxclnote01.setClassString("");%>
<%	sv.hxclnote01.appendClassString("string_fld");
	sv.hxclnote01.appendClassString("input_txt");
	sv.hxclnote01.appendClassString("highlight");
%>
	<%sv.hxclnote02.setClassString("");%>
<%	sv.hxclnote02.appendClassString("string_fld");
	sv.hxclnote02.appendClassString("input_txt");
	sv.hxclnote02.appendClassString("highlight");
%>
	<%sv.hxclnote03.setClassString("");%>
<%	sv.hxclnote03.appendClassString("string_fld");
	sv.hxclnote03.appendClassString("input_txt");
	sv.hxclnote03.appendClassString("highlight");
%>
	<%sv.hxclnote04.setClassString("");%>
<%	sv.hxclnote04.appendClassString("string_fld");
	sv.hxclnote04.appendClassString("input_txt");
	sv.hxclnote04.appendClassString("highlight");
%>
	<%sv.hxclnote05.setClassString("");%>
<%	sv.hxclnote05.appendClassString("string_fld");
	sv.hxclnote05.appendClassString("input_txt");
	sv.hxclnote05.appendClassString("highlight");
%>
	<%sv.hxclnote06.setClassString("");%>
<%	sv.hxclnote06.appendClassString("string_fld");
	sv.hxclnote06.appendClassString("input_txt");
	sv.hxclnote06.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action  (A=Add,M=Modify,D=Delete)");%>
	
	<%sv.action.setClassString("");%>
	<%sv.notemore.setClassString("");%>
<%	sv.notemore.appendClassString("string_fld");
	sv.notemore.appendClassString("output_txt");
	sv.notemore.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind11.isOn()) {
			sv.hxclnote01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.hxclnote06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind20.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.hxcllettyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.hxcllettyp.setReverse(BaseScreenData.REVERSED);
			sv.hxcllettyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.hxcllettyp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">

<div class="panel-body">
    	
    	  <div class="row">  	  
    	    <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Policy No")%></label>
    	        <%					
					if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
    	       </div>
    	     </div>
    	     <div class="col-md-4"></div>
    	      <div class="col-md-4">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Follow-up Code")%></label>
    	       <%					
				if(!((sv.fupcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.fupcode.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>

    	      </div>
    	     </div>
    	   </div>
    	   
    	  
    	  <div class="row">  	  
    	    <div class="col-md-2">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Action ")%></label>
  				<% if((new Byte((sv.action).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ %>  
    	         <input class="form-control" type="text" placeholder='<%=sv.action.getFormData()%>' disabled>
    	         <% }else {%>

						<select value='<%=sv.action.getFormData() %>' size='<%=sv.action.getLength()%>'
							 	onFocus='doFocus(this)' onHelp='return fieldHelp(sh583screensfl.action)' 
							 	onKeyUp='return checkMaxLength(this)' name='<%="action" %>'
							 	class = "input_cell"   style="width: 100px;">							 	
							 	
		  					<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
							<option value="A"<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Add")%></option>
							<option value="M"<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("M")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></option>
							<option value="D"<% if(((sv.action.getFormData()).toString()).trim().equalsIgnoreCase("D")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></option>
		  					
						</select>
				
					<% }%>
    	       </div>
    	     </div>
    	     <div class="col-md-6"></div>
    	      <div class="col-md-3">
    	      <div class="form-group">
    	        <label><%=resourceBundleHandler.gettingValueFromBundle("Letter Type")%></label>
    	   		
		  <%
                             String fieldName = "s5h583screensfl.hxcllettyp";
                            String fieldValue = sv.hxcllettyp.getFormData();
                            fieldItem=appVars.loadF4FieldsLong(new String[] {"hxcllettyp"},sv,"E",baseModel);
                            mappedItems = (Map) fieldItem.get("hxcllettyp");
                            optionValue = makeDropDownList(mappedItems, sv.hxcllettyp.getFormData(), 2, resourceBundleHandler);  
                            longValue = (String) mappedItems.get((sv.hxcllettyp.getFormData()).toString().trim());  
                        %>
                        
                        <% 
                            if((new Byte((sv.hxcllettyp).getEnabled()))
                            .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
                        %>  
                        <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>' style="width:140px;">
                            <%if(longValue!=null){ %>
                           <%=longValue%>
                           <%}%>
                        </div>
                        
                        <%
                        longValue = null;
                        %>
                        
                            <% }else {%>

 

                        <select name='hxcllettyp' type='list' style="width:200px;">
                        <%=optionValue%>
                        </select>
                     <%
                        } 
                    %>

    	      </div>
    	     </div>
    	   </div>
    	   
    	   <hr>
    	   <br>
    	    
    	    <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	        <input name='hxclnote01' 
					type='text'
					
					<%
					
							formatValue = (sv.hxclnote01.getFormData()).toString();
					
					%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.hxclnote01.getLength()%>'
					maxLength='<%= sv.hxclnote01.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote01)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.hxclnote01).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.hxclnote01).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.hxclnote01).getColor()== null  ? 
								"input_cell" :  (sv.hxclnote01).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
    	      </div>
    	    </div>
    	    </div>
    	    
    	   <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	      <input name='hxclnote02' 
				type='text'
				
				<%
				
						formatValue = (sv.hxclnote02.getFormData()).toString();
				
				%>
					value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.hxclnote02.getLength()%>'
				maxLength='<%= sv.hxclnote02.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote02)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.hxclnote02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.hxclnote02).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.hxclnote02).getColor()== null  ? 
							"input_cell" :  (sv.hxclnote02).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
    	      </div>
    	    </div>
    	  </div>
    	  
    	   <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	        <input name='hxclnote03' 
					type='text'
					
					<%
					
							formatValue = (sv.hxclnote03.getFormData()).toString();
					
					%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.hxclnote03.getLength()%>'
					maxLength='<%= sv.hxclnote03.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote03)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.hxclnote03).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.hxclnote03).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.hxclnote03).getColor()== null  ? 
								"input_cell" :  (sv.hxclnote03).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
    	      </div>
    	    </div>
    	  </div>
    	  
    	   <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	      <input name='hxclnote04' 
				type='text'
				
				<%
				
						formatValue = (sv.hxclnote04.getFormData()).toString();
				
				%>
					value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.hxclnote04.getLength()%>'
				maxLength='<%= sv.hxclnote04.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote04)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.hxclnote04).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.hxclnote04).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.hxclnote04).getColor()== null  ? 
							"input_cell" :  (sv.hxclnote04).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
    	      </div>
    	    </div>
    	   </div>
    	   
    	   <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	      <input name='hxclnote05' 
					type='text'
					
					<%
					
							formatValue = (sv.hxclnote05.getFormData()).toString();
					
					%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.hxclnote05.getLength()%>'
					maxLength='<%= sv.hxclnote05.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote05)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.hxclnote05).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.hxclnote05).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.hxclnote05).getColor()== null  ? 
								"input_cell" :  (sv.hxclnote05).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
    	      </div>
    	    </div>
    	   </div>
 
     	   <div class="row">  	  
    	    <div class="col-md-8">
    	      <div class="form-group">
    	        <input name='hxclnote06' 
				type='text'
				
				<%
				
						formatValue = (sv.hxclnote06.getFormData()).toString();
				
				%>
					value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.hxclnote06.getLength()%>'
				maxLength='<%= sv.hxclnote06.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(hxclnote06)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.hxclnote06).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.hxclnote06).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.hxclnote06).getColor()== null  ? 
							"input_cell" :  (sv.hxclnote06).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
    	      </div>
    	    </div>
    	  </div>



			<div style = "display:hidden">
			<input name='action' 
			type='hidden'
			value='<%=sv.action.getFormData()%>'
			size='<%=sv.action.getLength()%>'
			maxLength='<%=sv.action.getLength()%>' 
			class = "input_cell"
			onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
			
			</div>
</div>
</div>

<% } %>

<%@ include file="/POLACommon2NEW.jsp"%>

