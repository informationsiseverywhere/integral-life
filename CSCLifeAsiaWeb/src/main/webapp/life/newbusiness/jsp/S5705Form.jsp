

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5705";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5705ScreenVars sv = (S5705ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Currency ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Register ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Branch ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Source of Business ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Source of Income ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Campaign Number ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of Payment ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Commence Date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Agent ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cross Reference Type/Number ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statistical Codes ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Medical Evidence ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class Indicator ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit Codes ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.cntcurr.setReverse(BaseScreenData.REVERSED);
			sv.cntcurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.cntcurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.billcurr.setReverse(BaseScreenData.REVERSED);
			sv.billcurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.billcurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.register.setReverse(BaseScreenData.REVERSED);
			sv.register.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.register.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cntbranch.setReverse(BaseScreenData.REVERSED);
			sv.cntbranch.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cntbranch.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.srcebus.setReverse(BaseScreenData.REVERSED);
			sv.srcebus.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.srcebus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.campaign.setReverse(BaseScreenData.REVERSED);
			sv.campaign.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.campaign.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.mop.setReverse(BaseScreenData.REVERSED);
			sv.mop.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.mop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.rcdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.rcdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.rcdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.reptype.setReverse(BaseScreenData.REVERSED);
			sv.reptype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.reptype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.stcal.setReverse(BaseScreenData.REVERSED);
			sv.stcal.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.stcal.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.stcbl.setReverse(BaseScreenData.REVERSED);
			sv.stcbl.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.stcbl.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.stccl.setReverse(BaseScreenData.REVERSED);
			sv.stccl.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.stccl.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.stcdl.setReverse(BaseScreenData.REVERSED);
			sv.stcdl.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.stcdl.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.stcel.setReverse(BaseScreenData.REVERSED);
			sv.stcel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.stcel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.selection.setReverse(BaseScreenData.REVERSED);
			sv.selection.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.selection.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.pursuit01.setReverse(BaseScreenData.REVERSED);
			sv.pursuit01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.pursuit01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.pursuit02.setReverse(BaseScreenData.REVERSED);
			sv.pursuit02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.pursuit02.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
	        <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		       		<div class="input-group">
		       		<%if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>  		
		       		</div>
		       	</div>
		       	
					
		       	<div class="col-md-4">
		       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			       		<div class="input-group">
				       	<%if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'style="max-width: 900px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</div>
		       		</div>
		       	</div>
		       		
		       
					
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		       		<div class="input-group">
						       		<%if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="max-width: 900px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
				  		
						<%if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 900px;width:100px;padding-top:3px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </div>
		       		</div>
		       		</div>
	        </div>
	           <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
		       	<table>
				    		<tr>
				    			<td>
				    				<%if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    			<td style="padding-left: 10px;padding-right: 10px;"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
				    			<td>
				    				<%if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {			
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
				    			</td>
				    		</tr>
				    	</table>
		       	</div>
		       </div>
		
						
		       		</div>
		      
		     
		       	
		       	<div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>
		       		<div class="input-group">
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							optionValue = makeDropDownList( mappedItems , sv.cntcurr.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>
						 <%=smartHF.getDropDownExt(sv.cntcurr, fw, longValue, "cntcurr", optionValue,0,180) %>
					</div>
		       		</div>
		       	</div>
		       
		       		
		      
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Currency")%></label>
		       		<div class="input-group">
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"billcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("billcurr");
							optionValue = makeDropDownList( mappedItems , sv.billcurr.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.billcurr.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.billcurr, fw, longValue, "billcurr", optionValue,0,180) %>
						</div>
		       		</div>
		       	</div>
		    	
		       
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Register")%></label>
		       		<div class="input-group">
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							optionValue = makeDropDownList( mappedItems , sv.register.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.register, fw, longValue, "register", optionValue,0,180) %>
						</div>
		       		</div>
		       	</div>
		      </div>
		      
		      	<div class="row">
	        			<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Branch")%></label>
			       		<div class="input-group">
			       		<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"cntbranch"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("cntbranch");
								optionValue = makeDropDownList( mappedItems , sv.cntbranch.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.cntbranch.getFormData()).toString().trim());  
							%>
							<%=smartHF.getDropDownExt(sv.cntbranch, fw, longValue, "cntbranch", optionValue,0,180) %>
							</div>
			       		</div></div>
			       		
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Source of Business")%></label>
			       		<div class="input-group">
			       		<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"srcebus"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("srcebus");
								optionValue = makeDropDownList( mappedItems , sv.srcebus.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());  
							%>
							<%=smartHF.getDropDownExt(sv.srcebus, fw, longValue, "srcebus", optionValue,0,180) %>
							</div>
			       		</div></div>
			       		
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Source of Income")%></label>
			       		<div class="input-group">
			       		<input name='incomeSeqNo' 
							type='text'
							
							<%
							
									formatValue = (sv.incomeSeqNo.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.incomeSeqNo.getLength()%>'
							maxLength='<%= sv.incomeSeqNo.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(incomeSeqNo)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.incomeSeqNo).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.incomeSeqNo).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.incomeSeqNo).getColor()== null  ? 
										"input_cell" :  (sv.incomeSeqNo).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							</div>
			       		</div></div>
		       	</div>
		       	
		      
		<%--        	<div class="row">
	        		<div class="col-md-3">
			       		<div class="form-group">
			       		
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"cntbranch"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("cntbranch");
								optionValue = makeDropDownList( mappedItems , sv.cntbranch.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.cntbranch.getFormData()).toString().trim());  
							%>
							<%=smartHF.getDropDownExt(sv.cntbranch, fw, longValue, "cntbranch", optionValue,0,140) %>
			       		</div>
		       		</div>
		       		
		       			
					
		       		<div class="col-md-3">
			       		<div class="form-group">
			       		
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"srcebus"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("srcebus");
								optionValue = makeDropDownList( mappedItems , sv.srcebus.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.srcebus.getFormData()).toString().trim());  
							%>
							<%=smartHF.getDropDownExt(sv.srcebus, fw, longValue, "srcebus", optionValue,0,140) %>
			       		</div>
		       		</div>
		       		
					
		       		<div class="col-md-1">
			       		<div class="form-group">
			       		
										       		<input name='incomeSeqNo' 
							type='text'
							
							<%
							
									formatValue = (sv.incomeSeqNo.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.incomeSeqNo.getLength()%>'
							maxLength='<%= sv.incomeSeqNo.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(incomeSeqNo)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.incomeSeqNo).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.incomeSeqNo).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.incomeSeqNo).getColor()== null  ? 
										"input_cell" :  (sv.incomeSeqNo).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
			       		</div>
		       		</div>
		       	</div> --%>
		       	
		       	<div class="row">
	        			<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies")%></label>
			       		<div class="input-group">
			       		<%	
									qpsf = fw.getFieldXMLDef((sv.plansuff).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='plansuff' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.plansuff) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.plansuff);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.plansuff) %>'
							 <%}%>
						
						size='<%= sv.plansuff.getLength()%>'
						maxLength='<%= sv.plansuff.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(plansuff)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.plansuff).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.plansuff).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.plansuff).getColor()== null  ? 
									"input_cell" :  (sv.plansuff).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
			       		</div>
			       		</div></div>
			       		
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Campaign Number")%></label>
			       		<div class="input-group">
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"campaign"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("campaign");
							optionValue = makeDropDownList( mappedItems , sv.campaign.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.campaign.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.campaign, fw, longValue, "campaign", optionValue,0,180) %>
			       		</div>
			       		</div></div>
			       		
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
			       		<div class="input-group">
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("billfreq");
							optionValue = makeDropDownList( mappedItems , sv.billfreq.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.billfreq, fw, longValue, "billfreq", optionValue,0,180) %>
			       		</div>
			       		</div></div>
		       	</div>
		       	
		       	
		       	 <%-- 	<div class="row">
	        		<div class="col-md-1">
			       		<div class="form-group">
						
									       		<%	
									qpsf = fw.getFieldXMLDef((sv.plansuff).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									
							%>
						
						<input name='plansuff' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.plansuff) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.plansuff);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.plansuff) %>'
							 <%}%>
						
						size='<%= sv.plansuff.getLength()%>'
						maxLength='<%= sv.plansuff.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(plansuff)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.plansuff).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.plansuff).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.plansuff).getColor()== null  ? 
									"input_cell" :  (sv.plansuff).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
			       		</div>
			       	</div>
			       		
			       		<div class="col-md-2">
					<div class="form-group">
					</div></div>
					
			       		<div class="col-md-3">
			       		<div class="form-group">
			       	
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"campaign"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("campaign");
							optionValue = makeDropDownList( mappedItems , sv.campaign.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.campaign.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.campaign, fw, longValue, "campaign", optionValue,0,140) %>
			       		</div>
			       	</div>
			       	
			       	<div class="col-md-3">
			       		<div class="form-group">
			       		
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("billfreq");
							optionValue = makeDropDownList( mappedItems , sv.billfreq.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.billfreq, fw, longValue, "billfreq", optionValue,0,140) %>
			       		</div>
			       	</div>
			       	
			       </div> --%>
			       
			       <div class="row">
	        		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payment")%></label>
			       		<div class="input-group">
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("mop");
							optionValue = makeDropDownList( mappedItems , sv.mop.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.mop, fw, longValue, "mop", optionValue,0,180) %>
			       		</div>
			       		</div>
			       	</div>
			       		
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commence Date")%></label>
			       		<div class="input-group" style="min-width:100px;">
			       		<%	
							longValue = sv.rcdateDisp.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.rcdateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "input_cell" %>'style="max-width:900px;">  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						<%
						longValue = null;
						%>
						<% }else {%> 
						
						
						<% 
							if((new Byte((sv.rcdateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
					
						
						<%
							}else if((new Byte((sv.rcdateDisp).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						
					
						
						<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="rcdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.rcdateDisp,(sv.rcdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			            </div>
						
						<%
							}else { 
						%>
						
						class = ' <%=(sv.rcdateDisp).getColor()== null  ? 
						"input_cell" :  (sv.rcdateDisp).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="rcdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.rcdateDisp,(sv.rcdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			              </div>
						
						<%}} %>
			       		</div>
			       		</div>
			       	</div>
			       	
			       	<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Default Agent")%></label>
			       		<div class="input-group"style="min-width:100px;">
										       	<%	
								longValue = sv.agntsel.getFormData();  
							%>
							
							<% 
								if((new Byte((sv.agntsel).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>  
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "input_cell" %>'style="max-width:900px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
							<%
							longValue = null;
							%>
							<% }else {%>  
							<input name='agntsel' 
							type='text' 
							value='<%=sv.agntsel.getFormData()%>' 
							maxLength='<%=sv.agntsel.getLength()%>' 
							size='<%=sv.agntsel.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(agntsel)' onKeyUp='return checkMaxLength(this)'  
							
							<% 
								if((new Byte((sv.agntsel).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
							readonly="true"
							class="output_cell"	 >
							
							<%
								}else if((new Byte((sv.agntsel).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								
							%>	
							class="bold_cell" >
							 
									<span class="input-group-btn">
										<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
							       </span>
															       
							<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('agntsel')); changeF4Image(this); doAction('PFKEY04')"> 
							<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
							</a> --%>
							
							<%
								}else { 
							%>
							
							class = ' <%=(sv.agntsel).getColor()== null  ? 
							"input_cell" :  (sv.agntsel).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' >
							
									<span class="input-group-btn">
										<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
							       </span>
							       
							<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('agntsel')); changeF4Image(this); doAction('PFKEY04')"> 
							<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
							</a> --%>
							
							<%} }%>
			       	
			       		</div>
			       		</div>
			       	</div>
			       	
			       </div>
			       
			      
			        <div class="row">
	        		<div class="col-md-4"  style="padding-right: 0%;">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Cross Reference Type/Number")%></label>
			       		<div class="input-group">
			       		<!-- <table><tr><td style="
    width: 100px;
"> -->
							<%	
									longValue = sv.reptype.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.reptype).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
								<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "input_cell" %>'>  
									   		<%if(longValue != null){%>
									   		
									   		<%=longValue%>
									   		
									   		<%}%>
									   </div>
								<%
								longValue = null;
								%>
								<% }else {%>  
								<input name='reptype' id='reptype'
								type='text' 
								value='<%=sv.reptype.getFormData()%>' 
								maxLength='<%=sv.reptype.getLength()%>' 
								size='<%=sv.reptype.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(reptype)' onKeyUp='return checkMaxLength(this)'  
								
								<% 
									if((new Byte((sv.reptype).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
								readonly="true"
								class="output_cell"	 >
								
								<%
									}else if((new Byte((sv.reptype).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									
								%>	
								class="bold_cell" >
								 <span class="input-group-btn">
								<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
								       </span>
								 
																
								<!-- <span class="input-group-btn">
					        		<button class="btn btn-info" type="button" onClick="doFocus(document.getElementById('reptype')); doAction('PFKEY04');">Search</button>
					      		</span> -->
								
								<%
									}else { 
								%>
								
								class = ' <%=(sv.reptype).getColor()== null  ? 
								"input_cell" :  (sv.reptype).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>' >
								
								<span class="input-group-btn">
								<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
								       </span>
								
								<!-- <span class="input-group-btn">
					        		<button class="btn btn-info" type="button" onClick="doFocus(document.getElementById('reptype')); doAction('PFKEY04');">Search</button>
					      		</span> -->
					      			
								<%} }%>
								<!-- </td><td> -->
			       				<input name='lrepnum' 
								type='text'
								
								<%
								
										formatValue = (sv.lrepnum.getFormData()).toString();
								
								%>
									value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
								
								size='<%= sv.lrepnum.getLength()%>'
								maxLength='<%= sv.lrepnum.getLength()%>' 
								onFocus='doFocus(this)' onHelp='return fieldHelp(lrepnum)' onKeyUp='return checkMaxLength(this)'  
								
								
								<% 
									if((new Byte((sv.lrepnum).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									class="output_cell"
								<%
									}else if((new Byte((sv.lrepnum).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								
								<%
									}else { 
								%>
								
									class = ' <%=(sv.lrepnum).getColor()== null  ? 
											"input_cell" :  (sv.lrepnum).getColor().equals("red") ? 
											"input_cell red reverse" : "input_cell" %>'
								 
								<%
									} 
								%>
								>
							<!-- </td></tr></table> -->
								</div>       		
						</div>
			       		</div>
			      	</div>
			      	
			  	<div class="row">
	        		<div class="col-md-2">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Statistical Codes")%></label>
	<!-- 						<div class="input-group" style="
    min-width: 166px;
"> -->
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"stcal"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("stcal");
							optionValue = makeDropDownList( mappedItems , sv.stcal.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.stcal.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.stcal, fw, longValue, "stcal", optionValue,0,180) %>
						<!-- </div> -->
						</div>
						</div>
						
						<div class="col-md-2" style="
    margin-left: 20px;
">
			       		<div class="form-group">
			       		<label><br></label>
			       		<!-- <div class="input-group"style="
    min-width: 166px;
"> -->
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"stcbl"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("stcbl");
							optionValue = makeDropDownList( mappedItems , sv.stcbl.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.stcbl.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.stcbl, fw, longValue, "stcbl", optionValue,0,180) %>
		<!-- 				</div> -->
							</div>
						</div>
						
						<div class="col-md-2"style="
    margin-left: 20px;
">
			       		<div class="form-group">
						<label><br></label>
						<!-- <div class="input-group"style="
    min-width: 166px;
"> -->
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"stccl"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("stccl");
							optionValue = makeDropDownList( mappedItems , sv.stccl.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.stccl.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.stccl, fw, longValue, "stccl", optionValue,0,180) %>
						<!-- </div> -->
							</div>
						</div>
						
						<div class="col-md-2" style="
    margin-left: 20px;
">
			       		<div class="form-group">
			       		<label><br></label>
			       		<!-- <div class="input-group"style="
    min-width: 166px;
"> -->
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"stcdl"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("stcdl");
							optionValue = makeDropDownList( mappedItems , sv.stcdl.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.stcdl.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.stcdl, fw, longValue, "stcdl", optionValue,0,180) %>
						<!-- </div> -->
							</div>
						</div>
						
						<div class="col-md-2" style="
    margin-left: 20px;
">
			       		<div class="form-group">
			       		<label><br></label>
	<!-- 		       		<div class="input-group"style="
    min-width: 166px;
"> -->
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"stcel"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("stcel");
							optionValue = makeDropDownList( mappedItems , sv.stcel.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.stcel.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.stcel, fw, longValue, "stcel", optionValue,0,180) %>
<!-- </div> -->
					
			       		</div></div></div>
			       
			       	<div class="row">
	        		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Medical Evidence")%></label>
			       		<div class="input-group">
			       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"selection"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("selection");
							optionValue = makeDropDownList( mappedItems , sv.selection.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.selection.getFormData()).toString().trim());  
						%>
						<%=smartHF.getDropDownExt(sv.selection, fw, longValue, "selection", optionValue,0,220) %>
						</div>
			       		</div></div></div>
		   		
			       	
			       	<div class="row">
	        		<div class="col-md-4">
			       		<div class="form-group">
			       			<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
			       		<div class="input-group">
			       		<input name='smkrqd' 
							type='text'
							
							<%
							
									formatValue = (sv.smkrqd.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.smkrqd.getLength()%>'
							maxLength='<%= sv.smkrqd.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(smkrqd)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.smkrqd).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.smkrqd).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.smkrqd).getColor()== null  ? 
										"input_cell" :  (sv.smkrqd).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							</div>
			       		</div></div>
			       		
			       	
					
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class Indicator")%></label>
			       		<div class="input-group">
			       		<input name='mortality' 
							type='text'
							
							<%
							
									formatValue = (sv.mortality.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.mortality.getLength()%>'
							maxLength='<%= sv.mortality.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(mortality)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.mortality).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.mortality).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.mortality).getColor()== null  ? 
										"input_cell" :  (sv.mortality).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							</div>
			       		</div></div>

			       		<div class="col-md-4" >
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit Codes")%></label>		       		
						<!-- <div class="input-group" > -->
						<table><tr><td>
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"pursuit01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("pursuit01");
							optionValue = makeDropDownList( mappedItems , sv.pursuit01.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.pursuit01.getFormData()).toString().trim());  
						%>
						 <%=smartHF.getDropDownExt(sv.pursuit01, fw, longValue, "pursuit01", optionValue,0,160) %>
						 </td><td>
			       		 <%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"pursuit02"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("pursuit02");
							optionValue = makeDropDownList( mappedItems , sv.pursuit02.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.pursuit02.getFormData()).toString().trim());  
						%>
						 <%=smartHF.getDropDownExt(sv.pursuit02, fw, longValue, "pursuit02", optionValue,0,160) %>
						<!-- </div> -->
						</td></tr></table>
			       		</div></div>
			       		
			       		</div>
        	</div>
</div>
        
<%@ include file="/POLACommon2NEW.jsp"%>

 