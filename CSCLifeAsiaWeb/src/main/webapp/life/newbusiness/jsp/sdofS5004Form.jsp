﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import='com.quipoz.framework.util.*'%>
<%@ page import='com.quipoz.framework.screenmodel.*'%>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel'%>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@ page import="com.csc.life.newbusiness.screens.S5004ScreenVars" %>
<%@ page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@ page import="com.quipoz.framework.datatype.StringBase"%>
<%@ page import="com.quipoz.framework.error.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.properties.PropertyLoader" %>
<%@page import="com.resource.ResourceBundleHandler"%>

<%
	BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
	ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
	COBOLAppVars cobolAv3 = (COBOLAppVars) bm3.getApplicationVariables();
	response.addHeader("Cache-Control", "no-store"); 
	response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  	response.addHeader("X-Content-Type-Options", "NOSNIFF");
	response.addHeader("Pragma", "no-cache"); 

	

	String lang1 = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();
	String imageFolder = PropertyLoader.getFolderName(request.getLocale().toString());
	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw3.getScreenName(),request.getLocale());

	AppConfig appCfg = AppConfig.getInstance();
%>

<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--  LINK REL="StyleSheet" HREF="theme/<%=lang1.toLowerCase() %>/style.css" TYPE="text/css" -->
<!-- bootstrap -->
<link href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
<link href="../../../bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
<script src='../../../bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='../../../bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src='../../../bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='../../../bootstrap/sb/dist/js/sb-admin-2.js'></script>
<script src="../../../bootstrap/integral/integral-admin-sidebar.js"></script>
<!-- YY -->
<link href="../../../bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">

<script language='javaScript'>
	<%char[] c = cobolAv3.additionalValidKeys.toCharArray();
			String validKeys = "[";
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '1') {
					validKeys += i + ",";
				}
			}
			if (validKeys.endsWith("]")) {
				validKeys += "";//empty array
			} else {
				validKeys = validKeys.substring(0, validKeys.length() - 1)
						+ "]";
			}%>
	var validKeyArr = <%=validKeys%>;
	var isSupported = false;
	var lang1 = "<%=lang1%>";
	function isSupportKey(validKeyArr,action){
		if (validKeyArr.length > 0){
			for (var i=0;i<validKeyArr.length;i++){
				if (validKeyArr[i] == action){
					isSupported = true;
					break;
				}
			}
			if (isSupported){
				clearFField(); // fix bug46
				doAction('PFKEY0'+action);
			}else{
			/*<% if ("CHI".equals(lang1)) { %>
				alert("功能键" + action + "不能在当前屏幕使用");
			<% } else {%>
				alert("Function key " + action + " is not active on this screen at this time.");
			<%}%>*/
			callCommonAlert(lang1,"No0002",action);
			}
		}else{
			//alert("no keys are supported");
			callCommonAlert(lang1,"No00020");
		}
	}

 
</script>

<%@ page session="false"%>
<title>Generic SideBar</title>
<%
	HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess
			.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel != null) {
		baseModel.getApplicationVariables();
		ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
		SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter) AppVars.hf;
%>

<script type="text/javascript">
function hyperLinkTo(nextField) {
	//ILIFE-3537
	var fieldName=nextField.name;
	var doc=parent.frames["mainForm"].document;
	doc.getElementById(fieldName).value="X";
	var sideFrame = parent.frames["mainForm"];
	sideFrame.document.form1.activeField.value = nextField;
	sideFrame.document.form1.action_key.value = "PFKEY0";	
    sideFrame.doSub();		
}
function removeXfield(xfield){
	var doc=parent.frames["mainForm"].document;
	doc.getElementById(xfield.id).value="";
	doAction('PFKEY05');
}

function doAction(act) {
	  parent.frames["mainForm"].document.form1.action_key.value = act;
	  parent.frames["mainForm"].doSub();
/* 	pageloading(); */
}
</script>

</HEAD>
<BODY id="sideBar" style="background-color: #ffffff;font-size: 13px !important; border: 1px solid gainsboro !important;">
<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) Begin-->
<%
	String msgs = resourceBundleHandler.gettingValueFromBundle("No Message");
		String fontStyle = " font-family:Arial; font-weight:bold; font-size:12px;";
		String lang = request.getParameter("lang");
		/**if ("CHI".equalsIgnoreCase(lang)) {
			msgs = "没有信息";
			fontStyle = " font-type:宋体; font-weight:normal; font-size:12px;";
		}*/
		try {
			sess = request.getSession();
			BaseModel bm = (BaseModel) sess
					.getAttribute(BaseModel.SESSION_VARIABLE);
			AppVars avs = bm.getApplicationVariables();
			lang = avs.getUserLanguage().toString().trim();
			if (!avs.mainFrameLoaded) {
				for (int i = 0; i < 40; i++) {
					avs.waitabit(250);
					if (avs.mainFrameLoaded) {
						break;
					}
				}
			}
			if (!avs.mainFrameLoaded) {
				msgs = resourceBundleHandler.gettingValueFromBundle("Messages failed to load after 10 seconds.")
						+ "<br>"+resourceBundleHandler.gettingValueFromBundle("This is usually caused by slow response on the main form; errors may be incorrect.")
						+ "<br>"+resourceBundleHandler.gettingValueFromBundle("Press the refresh button here when the main form loads.")
						+ "<button onClick='document.location.reload(false)'>"+resourceBundleHandler.gettingValueFromBundle("Refresh errors")+"</Button>";
			} else {
				String ctx = request.getContextPath() + "/";
				MessageList list = avs.getMessages();
				Iterator i = list.iterator();
				StringBuffer sb = new StringBuffer();
				while (i.hasNext()) {
					String str = i.next().toString();
					HTMLFormatter formatter = new HTMLFormatter();

					/* Remove any trailing ? which is historic and means "stop underlining" */
					str = QPUtilities.removeTrailing(str.trim(), "?");
					//added by wayne to parse errorno
					String[] arr = str.split("ErrorMessage");
					if (arr.length < 2)
						sb.append(str + "<br>");
					else {
						sb.append(arr[0]);
						for (int j = 1; j < arr.length - 1; j++) {
							sb.append("<a class='err-msg' href='#'>")
									.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?")))
									.append("</a><br/>");
						}
						sb.append("<a class='err-msg' href='#'>")
								.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[arr.length - 1].substring(4),"?")))
								.append("</a>");
						sb.append("<br>");
					}
				}

				msgs = sb.toString();

				msgs = msgs.replaceFirst("Message:", "");
			}
		} catch (Exception e) {
		}
%>
<script language="javascript">
	//boostrap
	$(document).ready(function(){
	if(<%=msgs.length()%> == 0)	$("#msg-panel").css("display","none");
		calculateMenuHeight();
});	
	function calculateMenuHeight(){
		var screenBodyHeight = screen.height;
		var sidebarBodyHeight = $("#sideBar").height();
   		//var logoHeight = $("#topPanel").outerHeight(true);
		// ILIFE-8864 STARTS
		var logoHeight = 0; 
		if($("#sidebar-hide").length > 0){
			logoHeight = 20;
		}
		// ILIFE-8864 ENDS
   		var sidebar = $(".sidearea");
   		var message = $(".err-msg");
   		var messPane = $("#msg-panel");
   		if(screenBodyHeight > 1000) {
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = sidebarBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = sidebarBodyHeight - logoHeight;
   			}
   		}else{  
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = screenBodyHeight - 200 + 5;		 //IBPLIFE-1698
   			}else{
   				var sidebarHeight = screenBodyHeight - 200;			 //IBPLIFE-1698
   			}
   		}
   		sidebar.css('height', sidebarHeight + "px");
   	}
</script>
<style>
.row.message {
	padding-left: 15px;
}
#msg-panel> .panel-heading{
	 font-weight: bolder !important;
	 text-align: center !important;
	 padding-right: 40px !important;
}
/* #msg-panel{
     top: 305px !important;
} */
</style>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
//public static final boolean ieEmulationOn = true;	//IGROUP-1211 make emulation to be configurable
public static String emulationVer="off";			//IGROUP-1211
%>
<%	
	// IGroup-1086 add the brower version in common jsp
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	emulationVer = "off";	//IGROUP-1211
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10; //IGROUP-1211
		}
	} else if (userAgent.contains("Trident/7.0")) {
			browerVersion = IE11;
		    if(AppConfig.ieEmulationEnable) emulationVer =  IE11;	//IGROUP-1211
	}		
%>
	<%-- <div id="topPanel" class="row" style="border:0px;margin:0 0 0 -1;background-color:#2c2cce;height: 50px;width: 300px;">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;margin-top: 2px !important;background-color: #2c2cce;">
				<% if ( browerVersion.equals(Chrome)) {%>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;">				
				<%}else{ %>
				<ul class="nav navbar-top-links navbar-right" style="background-color: #2c2cce;margin-top: -9.7px !important;height: 59px">	
				<%} %>
					<li><a><img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/dxc_symbol_wht_rgb_150.png" width="55px" border="0"/></a></li>
					<li><a style="left: -15px;top: -4px;"><div style="font-size:15px;font-weight: bold;color: white !important;">INTEGRAL Admin</div>
						<table style="font-size: 10px !important;font-weight: bold;width:91%;">
				         <tr>
					         	<td style="color: #ffffff !important;"><%=AppConfig.getInstance().getVersion() %> </td>
					         	<td style="text-align: right; color: #fff !important;"><%=fw.getScreenName()%></td>
					         </tr>
				         </table>
				         </a>
					</li>
				</ul>
			</nav>
	 </div> --%>
	<div class="sidearea" style="overflow: auto;">

<%
	// To decide whether menus are displayed or not
		LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel
				.getApplicationVariables();
		boolean isShow = av.isMenuDisplayed();
		if (!isShow) {
%>
<!-- <div class="logoarea" style="height: 50px;">
	<a class="navbar-brand" href="#">Integral Admin</a>
</div> -->
<!-- ILIFE-8864 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<div id="sidebar-hide" style="padding-left: 93%;">
	<span id="sidebar-pin" class="glyphicon glyphicon-pushpin" style="line-height: 20px; cursor:pointer"></span>
	<span id="sidebar-unpin" class="glyphicon glyphicon-chevron-left" style="line-height: 20px; cursor:pointer"></span>
</div>

<script>
	$(document).ready(function(){
		var mainDoc=parent.frames["mainForm"];
		var msgDiv = document.getElementById("msg-panel");
		if(msgDiv.style.display !=="none"){
			mainDoc.document.getElementById("msg-panel").innerHTML += msgDiv.innerHTML;
			mainDoc.document.getElementById("msg-panel").style.display="block";
		}	
	})
	if(sessionStorage.getItem("sidebar") !== "pined"){
		$("#sidebar-pin").css("display", "block");
		$("#sidebar-unpin").css("display", "none");
	}else{
		$("#sidebar-pin").css("display", "none");
		$("#sidebar-unpin").css("display", "block");
		window.top.document.getElementsByName("realContent")[0].cols = "275,*";
	}
	$("#sidebar-hide>span").click(function(){
		if(sessionStorage.getItem("sidebar") === "pined"){
			$("#sidebar-pin").css("display", "block");
			$("#sidebar-unpin").css("display", "none");
			sessionStorage.setItem("sidebar", "unpined");
			window.top.document.getElementsByName("realContent")[0].cols = "0,*"
		}else{
			$("#sidebar-pin").css("display", "none");
			$("#sidebar-unpin").css("display", "block");
			sessionStorage.setItem("sidebar", "pined");
		}
	});
</script>
<%}%>
<!-- ILIFE-8864 ENDS-->

<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="display: block;">
                    <ul class="nav" id="side-menu">
                        <li class="active">
							<a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Extra_Info")%></a>
							<%if(fw.getScreenName().equalsIgnoreCase("S5004")){
								S5004ScreenVars sv = (S5004ScreenVars) fw.getVariables();
							%>	
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>
								<li>
									<input name='jownind' id='jownind' type='hidden'  value="<%=sv.jownind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.jownind.getInvisible()== BaseScreenData.INVISIBLE|| sv.jownind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Joint Owner ")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("jownind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Joint Owner ")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.jownind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.jownind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('jownind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<!-- ALS-4555 (Multiple Owner) -->
								<%
									if ((new Byte((sv.zmultOwner).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>	
								<li>
									<input name='zmultOwner' id='zmultOwner' type='hidden' value="<%=sv.zmultOwner.getFormData()%>">
									<!-- text -->
									<%
									if( sv.zmultOwner
											.getEnabled()==BaseScreenData.DISABLED){
									%> 
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zmultOwner"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Multiple Owner")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zmultOwner.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zmultOwner.getFormData().equals("X") && sv.zmultOwner.getInvisible()!= BaseScreenData.INVISIBLE) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zmultOwner'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<%} %>
								<!-- ALS-4555 (Multiple Owner) -->
								<li>
									<input name='asgnind' id='asgnind' type='hidden'  value="<%=sv.asgnind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.asgnind.getInvisible()== BaseScreenData.INVISIBLE|| sv.asgnind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Assignee ")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("asgnind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Assignee ")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.asgnind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.asgnind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('asgnind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='apcind' id='apcind' type='hidden'  value="<%=sv.apcind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.apcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.apcind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Apply Cash")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("apcind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Apply Cash")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.apcind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.apcind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('apcind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='addtype' id='addtype' type='hidden' value="<%=sv.addtype.getFormData()%>">
									<!-- text -->
									<%
									if((sv.addtype.getInvisible()== BaseScreenData.INVISIBLE|| sv.addtype
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Despatch Address")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("addtype"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Despatch Address")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.addtype.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.addtype.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('addtype'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='payind' id='payind' type='hidden' value="<%=sv.payind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.payind.getInvisible()== BaseScreenData.INVISIBLE|| sv.payind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Payor")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Payor")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.payind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.payind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('payind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='comind' id='comind' type='hidden' value="<%=sv.comind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.comind.getInvisible()== BaseScreenData.INVISIBLE|| sv.comind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Commission")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.comind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.comind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('comind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='crcind' id='crcind' type='hidden' value="<%=sv.crcind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.crcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.crcind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payment")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("crcind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payment")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.crcind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.crcind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('crcind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='ddind' id='ddind' type='hidden' value="<%=sv.ddind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Debit")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Debit")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ddind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ddind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ddind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='fupflg' id='fupflg' type='hidden' value="<%=sv.fupflg.getFormData()%>">
									<!-- text -->
									<%
									if((sv.fupflg.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Follow-ups")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Follow-ups")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.fupflg.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.fupflg.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('fupflg'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='bnfying' id='bnfying' type='hidden' value="<%=sv.bnfying.getFormData()%>">
									<!-- text -->
									<%
									if((sv.bnfying.getInvisible()== BaseScreenData.INVISIBLE|| sv.bnfying
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Beneficiaries")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bnfying"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Beneficiaries")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.bnfying.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.bnfying.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('bnfying'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='grpind' id='grpind' type='hidden' value="<%=sv.grpind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.grpind.getInvisible()== BaseScreenData.INVISIBLE|| sv.grpind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Group Details")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("grpind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Group Details")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.grpind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.grpind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('grpind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='aiind' id='aiind' type='hidden' value="<%=sv.aiind.getFormData()%>">
									<!-- text -->
									<%

									if(!(sv.aiind
									.getInvisible()== BaseScreenData.INVISIBLE|| sv.aiind
									.getEnabled()==BaseScreenData.DISABLED)){
									%>
									<!-- <a href="#" class="disabledLink"> -->
										<%-- <%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%> --%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aiind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Anniversary")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.aiind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.aiind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('aiind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='ctrsind' id='ctrsind' type='hidden' value="<%=sv.ctrsind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.ctrsind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ctrsind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Trustee")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ctrsind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Trustee")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ctrsind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ctrsind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ctrsind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='transhist' id='transhist' type='hidden' value="<%=sv.transhist.getFormData()%>">
									<!-- text -->
									<%
									if((sv.transhist.getInvisible()== BaseScreenData.INVISIBLE|| sv.transhist
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Transactions History")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("transhist"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Transactions History")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.transhist.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.transhist.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('transhist'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='ind' id='ind' type='hidden' value="<%=sv.ind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.ind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Policy Notes")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Policy Notes")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='zsredtrm' id='zsredtrm' type='hidden' value="<%=sv.zsredtrm.getFormData()%>">
									<!-- text -->
									<%
									if((sv.zsredtrm.getInvisible()== BaseScreenData.INVISIBLE|| sv.zsredtrm
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zsredtrm"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zsredtrm.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zsredtrm.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zsredtrm'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='zdcind' id='zdcind' type='hidden' value="<%=sv.zdcind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.zdcind.getInvisible()== BaseScreenData.INVISIBLE|| sv.zdcind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payout")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zdcind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Credit Card Payout")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zdcind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zdcind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zdcind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<li>
									<input name='zdpind' id='zdpind' type='hidden' value="<%=sv.zdpind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.zdpind.getInvisible()== BaseScreenData.INVISIBLE|| sv.zdpind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Credit")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zdpind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Direct Credit")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zdpind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zdpind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zdpind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								
								<%
									if ((new Byte((sv.zctaxind).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>	
								<li>
									<input name='zctaxind' id='zctaxind' type='hidden' value="<%=sv.zctaxind.getFormData()%>">
									<!-- text -->
									<%
									if( sv.zctaxind
											.getEnabled()==BaseScreenData.DISABLED){
									%> 
									 <%--<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Contribution Details")%>--%><!-- ILIFE-7014 -->
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zctaxind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Contribution Details")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zctaxind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zctaxind.getFormData().equals("X") && sv.zctaxind.getInvisible()!= BaseScreenData.INVISIBLE) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zctaxind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								<%} %>
								<%
									if ((new Byte((sv.zroloverind).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>						
								
								<li>
									<input name='zroloverind' id='zroloverind' type='hidden' value="<%=sv.zroloverind.getFormData()%>">
									<!-- text -->
									<%
									if(sv.zroloverind.getEnabled()==BaseScreenData.DISABLED){
									%> 
								<%--<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Rollover Details")%>--%><!-- ILIFE-7014 -->
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zroloverind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Rollover Details")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.zroloverind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.zroloverind.getFormData().equals("X") && sv.zroloverind.getInvisible()!= BaseScreenData.INVISIBLE) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('zroloverind'))"></i> 
			 						<%}%>
			 						</a>
								</li>
								
								<%} %>
							</ul>
							<%} %>
						</li>
						
					</ul>
				</div>
</div>
<%} %>

<div class="panel-group" id="accordion">
			<div class="panel panel-info" id='msg-panel'>
			    <div class="panel-heading" style=" text-align: center;padding-right: 40px;">
			        <h4 class="panel-title" style="font-size: 13px !important;font-weight: bolder;">
			            <%=resourceBundleHandler.gettingValueFromBundle("Messages")%>
			        </h4>
			    </div>
			    <div class="panel-body" style="font-size: 13px !important;">
					<div class="row" style="padding-left: 15px!important;">	
							<%=msgs%>
					</div>
			    </div>
			</div>	
		</div>


<%} %>
</div>

</BODY>
</HTML>
