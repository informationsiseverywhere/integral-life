
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH564";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh564ScreenVars sv = (Sh564ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select Criteria ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Totals");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(leave blank for all)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Channel         ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"District        ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Product         ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.ctrlbrk01.setReverse(BaseScreenData.REVERSED);
			sv.ctrlbrk01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ctrlbrk01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.ctrlbrk02.setReverse(BaseScreenData.REVERSED);
			sv.ctrlbrk02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.ctrlbrk02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ctrlbrk03.setReverse(BaseScreenData.REVERSED);
			sv.ctrlbrk03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ctrlbrk03.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name")%></label>
                              <%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>                              
      </div>
    </div>
    <div class="col-md-4">
	                    <div class="form-group"> 
	                  
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Number")%></label>
                               <table><tr><td>
                             <%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" >	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank`_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>	
		 </td></tr></table>
		
       </div>
     
     </div>
     
      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
                            <%					
		if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
      </div>
     </div>
   </div>
   
     <div class="row">
                      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
                              <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
      </div>
    </div>
    <div class="col-md-4"> </div>
   
     
      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%></label>
                            <!--   <div class="input-group"> -->
                            <table><tr><td>
                           <%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" >	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td>
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="margin-left: 1px;width: 100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="margin-left: 1px;width: 100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>	
	</td></tr></table>
      <!-- </div> -->
     </div>
   </div>
   </div>  
   <div class="row">
                      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                             <%					
		if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:30px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
      </div>
    </div>
    <div class="col-md-4">
	                
                            
      </div>
    
     
      <div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
                           <%					
		if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:40px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
      </div>
     </div>
   </div>
    
   <div class="row">
                      <div class="col-md-4">
	                   
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Select Criteria")%></label>                       
             
       </div>
   
   
                      <div class="col-md-4">
	                      <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Totals")%></label>
                         </div>
                    </div>
                    <div class="col-md-4"></div>
   
     </div>
      <div class="row">
                      <div class="col-md-4">
	                   
                              
                              <label><%=resourceBundleHandler.gettingValueFromBundle("(leave blank for all)")%></label>           
       </div>    
       <div class="col-md-4"></div>
       <div class="col-md-4"></div>  
     </div>
       <div class="row">
                      <div class="col-md-4">
	                   
                              
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Channel")%></label>
             
       </div>   
       <div class="col-md-4"></div>
       <div class="col-md-4"></div>   
     </div>
       <div class="row">
                      <div class="col-md-4">
      <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch");
	optionValue = makeDropDownList( mappedItems , sv.branch.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.branch).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.branch).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='branch' type='list' style="width:200px;"
<% 
	if((new Byte((sv.branch).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.branch).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.branch).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
                      </div>     
                      
                       <div class="col-md-4"> 
                       <%	
	if ((new Byte((sv.ctrlbrk03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals");
	}
	if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals");
	}
		 
%>

<% 
	if((new Byte((sv.ctrlbrk03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.ctrlbrk03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='ctrlbrk03' style="width:200px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(ctrlbrk03)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.ctrlbrk03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.ctrlbrk03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="T"<% if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("T")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals")%></option>
<option value="P"<% if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals")%></option>


</select>
<% if("red".equals((sv.ctrlbrk03).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
                        </div>
     </div>
   

     <div class="row">
                      <div class="col-md-4">
	                   
                              
                              <label><%=resourceBundleHandler.gettingValueFromBundle("District")%></label>
             
       </div>   
       <div class="col-md-4"></div>
       <div class="col-md-4"></div>   
     </div>
               <div class="row">
                      <div class="col-md-4">
	                      <div class="form-group">
	                      <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"aracde"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("aracde");
	optionValue = makeDropDownList( mappedItems , sv.aracde.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.aracde.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.aracde).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.aracde).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='aracde' type='list' style="width:200px;"
<% 
	if((new Byte((sv.aracde).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.aracde).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.aracde).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
                              
                         </div>
                    </div>
                       <div class="col-md-4">
	                      
	                      
                              <%	
	if ((new Byte((sv.ctrlbrk02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.ctrlbrk02.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals");
	}
	if(((sv.ctrlbrk02.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals");
	}
		 
%>

<% 
	if((new Byte((sv.ctrlbrk02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.ctrlbrk02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='ctrlbrk02' style="width:200px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(ctrlbrk02)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.ctrlbrk02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.ctrlbrk02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="T"<% if(((sv.ctrlbrk02.getFormData()).toString()).trim().equalsIgnoreCase("T")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals")%></option>
<option value="P"<% if(((sv.ctrlbrk02.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals")%></option>


</select>
<% if("red".equals((sv.ctrlbrk02).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
                         
                    </div>
                    <div class="col-md-4"></div>
                </div>
   
    <div class="row">
                      <div class="col-md-4">
	                   
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Product")%></label>                       
             
                       </div>
                       <div class="col-md-4"></div>
                       <div class="col-md-4"></div>
   </div>
        	<div class="row">
                      <div class="col-md-4">
	                      <div class="form-group">
                             <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"chdrtype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("chdrtype");
	optionValue = makeDropDownList( mappedItems , sv.chdrtype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.chdrtype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.chdrtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.chdrtype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='chdrtype' type='list' style="width:200px;"
<% 
	if((new Byte((sv.chdrtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.chdrtype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.chdrtype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
                         </div>
                    </div>
                    <div class="col-md-4">
	                      
                           <%	
	if ((new Byte((sv.ctrlbrk03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals");
	}
	if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals");
	}
		 
%>

<% 
	if((new Byte((sv.ctrlbrk03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.ctrlbrk03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
<%
} 
%>

<select name='ctrlbrk03' style="width:200px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(ctrlbrk03)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.ctrlbrk03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.ctrlbrk03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="T"<% if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("T")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Control Break with Totals")%></option>
<option value="P"<% if(((sv.ctrlbrk03.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Page Break with Totals")%></option>


</select>
<% if("red".equals((sv.ctrlbrk03).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>  
                         </div>
                         <div class="col-md-4"></div>
                    </div>
                  </div>
   
     
   </div>
<%@ include file="/POLACommon2NEW.jsp"%>


