<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6799";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6799ScreenVars sv = (S6799ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					
					<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.company.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						<table>
						<tr>
						<td>
						<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.item.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						</td>
						<td>
						<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width: 225px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<%-- lables row --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Generate Underwriting Records")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width:90px;">
					<%
						if (((BaseScreenData) sv.subprog) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.subprog, (sv.subprog.getLength() + 1), null)
						.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.subprog) instanceof DecimalData) {
					%>
					<%=smartHF.getHTMLVar(fw, sv.subprog, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					<%
						} else {
					%>
					hello
					<%
						}
					%>
				</div>
			</div>
			

			<div class="col-md-4">
				<div class="form-group" style="width:110px;">
					<%
						if (((BaseScreenData) sv.ind) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.ind, (sv.ind.getLength() + 1), null).replace("absolute",
						"relative")%>
					<%
						} else if (((BaseScreenData) sv.ind) instanceof DecimalData) {
					%>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.ind, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					<%
						} else {
					%>
					hello
					<%}%>
				</div>
			</div>

		</div>


	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
