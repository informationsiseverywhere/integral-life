<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5070";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%S5070ScreenVars sv = (S5070ScreenVars) fw.getVariables();%>

	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payer ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate No");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factoring House   ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account  ");%>
	<%StringData generatedText46 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText47 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText48 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText49 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText50 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText51 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText52 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText53 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText54 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText55 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mandref.setReverse(BaseScreenData.REVERSED);
			sv.mandref.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mandref.setHighLight(BaseScreenData.BOLD);
		}
		//added for ILIFE-550
		if (appVars.ind50.isOn()) {
			sv.mandref.setEnabled(BaseScreenData.DISABLED);
			sv.bankkey.setEnabled(BaseScreenData.DISABLED);
		}
		 /*IFSU-852 starts*/
		if (appVars.ind02.isOn()) {
			sv.mrbnk.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 /*IFSU-852 ends*/
	}

	%>
	
	
	
	
	<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Payor")%></label>
					    		<table>
					    		<tr>
					    		<td>
					    		<%					
								if(!((sv.payrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									}
								%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell1" : "output_cell" %>'  style="max-width:230px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td>
		
	  		 					<td>
							<%					
							if(!((sv.payorname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 200px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								</td>
								</tr>
								</table>
				      		</div>
				      	</div>
				    </div>
				    
			    	  <div class="row"> 
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Mandate Number")%></label>
					    	<div class="input-group" style="max-width:135px;">
					    		
					    		<input name='mandref' id='mandref' 
		type='text' 
		value='<%=sv.mandref.getFormData()%>' 
		maxLength='<%=sv.mandref.getLength()%>' 
		size='<%=sv.mandref.getLength()%>'
		onFocus='doFocus(this)' onHelp='return fieldHelp(mandref)' onKeyUp='return checkMaxLength(this)'  
		
		<% 
			if((new Byte((sv.mandref).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
		%>  
		readonly="true"
		class="output_cell"	 >
		
		<%
			}else if((new Byte((sv.mandref).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
		%>	
		class="bold_cell" >
		
		 <span class="input-group-btn">
						    			<button class="btn btn-info" style="font-size: 19px;border-bottom-width: 2px !important;" type="button" onClick="doFocus(document.getElementById('mandref')); doAction('PFKEY04')">
						        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						        		</button>
					      			</span>
		 
		<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
		<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos' >
		</a>
		 --%>
		<%
			}else { 
		%>
		
		class = ' <%=(sv.mandref).getColor()== null  ? 
		"input_cell" :  (sv.mandref).getColor().equals("red") ? 
		"input_cell red reverse" : "input_cell" %>' >
		 <span class="input-group-btn">
						    			<button class="btn btn-info" style="font-size: 19px;border-bottom-width: 2px !important;" type="button" onClick="doFocus(document.getElementById('mandref')); doAction('PFKEY04')">
						        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						        		</button>
					      			</span>
		<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px" onClick="doFocus(document.getElementById('mandref')); changeF4Image(this); doAction('PFKEY04')"> 
		<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos' >
		</a> --%>
		
		<%} %>
		
		
				      		</div></div>
				    	</div></div>
			    	
			    	
			    	 <div class="row">	
			   	
				    
		<div class="col-md-6">
			<div class="form-group">  	  
				<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Sort Code")%></label>
		
			<table>
			<tr>
			<td >
		 <!-- ILIFE-3948 -->
		 <div class="input-group">
						<input name='bankkey' id='bankkey' 
						type='text' 
						value='<%=sv.bankkey.getFormData()%>' 
						maxLength='<%=sv.bankkey.getLength()%>' 
						size='<%=sv.bankkey.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  
						
						<% 
							if((new Byte((sv.bankkey).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
						%>  
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.bankkey).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						 
						 <span class="input-group-btn">
						    <button class="btn btn-info" style="font-size: 19px;border-bottom-width: 2px !important;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
						        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						     </button>
					      </span>
						<%-- <%=smartHF.getHTMLF4NSVarExt(fw, sv.bankkey, -3)%> --%>
						
						<%
							}else { 
						%>
						
						class = ' <%=(sv.bankkey).getColor()== null  ? 
						"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						 <span class="input-group-btn">
						    <button class="btn btn-info" style="font-size: 19px;border-bottom-width: 2px !important;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
						        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						     </button>
					      </span>
						
						<%} %>
						</div>
						</td>
						<td>
				  		
						<%					
						if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell1" : "output_cell" %>' style="width: 100px;margin-left: -1px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						<td>
				  		
						<%					
						if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell1" : "output_cell" %>' style="width: 100px;margin-left: 1px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
				  
      
			    	  <div class="col-md-4">
			    	  <%if ((new Byte((sv.mrbnk).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {  %>
			    	  	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Classification")%></label> 	
				    
				    	
		<%
		fieldItem = appVars.loadF4FieldsLong(new String[] { "mrbnk" }, sv, "E", baseModel);
		mappedItems = (Map) fieldItem.get("mrbnk");
		longValue = (String) mappedItems.get((sv.mrbnk.getFormData()).toString().trim());
	%> 
	<%
	 	if (!((sv.mrbnk.getFormData()).toString()).trim().equalsIgnoreCase("")) {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	} else {
	
	 		if (longValue == null || longValue.equalsIgnoreCase("")) {
	 			formatValue = formatValue((sv.mrbnk.getFormData()).toString());
	 		} else {
	 			formatValue = formatValue(longValue);
	 		}
	
	 	}
	 %>
		<div
			class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width: 85px;">
			<%=XSSFilter.escapeHtml(formatValue)%>
		</div> 
	<%
	 	longValue = null;
	 	formatValue = null;
	 %>	  
	 	

				     
			       </div> 
			        
			        <%} %>
			        
			        </div></div>
			        
			        
			         <div class="row">	
			    	 <div class="col-md-4">
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Factoring House")%></label> 
							<table>
							<tr>
							<td> 
							<%					
									if(!((sv.facthous.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell1" : "output_cell" %>' style="width:100px" >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td>
								<td>
								<%					
								if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="width:100px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</td>
								</tr>
								</table>
				      </div>
			        </div>
			       </div>
			   
			        
			         <div class="row">	
			    	 	 <div class="col-md-4">
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%></label> 	  
				    	 <table>
				    	 <tr>
				    	 <td>
								<%					
							if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										} else  {
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										}
										%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell1" : "output_cell" %>' style="width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td>
							<td>
							<%					
							if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										} else  {
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										}
										%>
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px;" >
							<%=XSSFilter.escapeHtml(formatValue)%>
							</td>
							</tr>
							</table>
							 

				      </div>
			        </div>
			        </div>
			       

			        
			        </div></div>
			     
			      


<%@ include file="/POLACommon2NEW.jsp"%>



