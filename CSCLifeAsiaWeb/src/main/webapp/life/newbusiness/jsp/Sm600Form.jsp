

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM600";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sm600ScreenVars sv = (Sm600ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OCC%         ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Banding Required  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"GL Accounts for PREMIUM");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"GL Accounts for ACTUAL");%>

<%{
		appVars.rolldown();
		appVars.rollup();
	}

	%>

	<script>
$(document).ready(function(){
	$("#item").attr("class","input-group-addon");
	$("#longdesc").css("width","300px");

	$("#longdesc").attr("class","form-control")
})
</script>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:35px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
			<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-right: 2px
	}
}
</style><div class="col-md-2"></div>
			<div class="col-md-2" >
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
 	if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase(
 			"")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.tabl.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.tabl.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">

					<label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
								
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:40px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 150px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

						</div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Description")%>
					</label>
					<div class="input-group">
					

<input name='genldesc' 
type='text'

<%if((sv.genldesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.genldesc.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.genldesc.getLength()%>'
maxLength='<%= sv.genldesc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(genldesc)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.genldesc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.genldesc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.genldesc).getColor()== null  ? 
			"input_cell" :  (sv.genldesc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					</div>
				</div>
			</div>
			<div class="col-md-4">
							<div class="form-group">
								<label> <%=resourceBundleHandler.gettingValueFromBundle("OCC%")%></label>
								<div class="input-group">
									<%
									qpsf = fw.getFieldXMLDef((sv.occpct).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
								%>
									<input name='occpct' type='text'
										<%if((sv.occpct).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <% }%>
										value='<%=smartHF.getPicFormatted(qpsf,sv.occpct) %>'
										<%
				 valueThis=smartHF.getPicFormatted(qpsf,sv.occpct);
				 if(valueThis!=null&& valueThis.trim().length()>0) {%>
										title='<%=smartHF.getPicFormatted(qpsf,sv.occpct) %>' <%}%>
										size='<%= sv.occpct.getLength()%>'
										maxLength='<%= sv.occpct.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(occpct)'
										onKeyUp='return checkMaxLength(this)'
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);'
										<% 
				if((new Byte((sv.occpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>
										readonly="true" class="output_cell"
										<%
				}else if((new Byte((sv.occpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>
										class="bold_cell" <%
				}else { 
			%>
										class=' <%=(sv.occpct).getColor()== null  ? 
						"input_cell" :  (sv.occpct).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
										<%
				} 
			%>>
					</div>
				</div>
			</div>			
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Banding Required")%>
					</label> <input type='checkbox' name='ynflag' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ynflag)'
						onKeyUp='return checkMaxLength(this)'
						<%

if((sv.ynflag).getColor()!=null){
			 %>
						style='background-color: #FF0000;'
						<%}
		if((sv.ynflag).toString().trim().equalsIgnoreCase("Y")){
			%>
						checked
						<% }if((sv.ynflag).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('ynflag')" />
					<input type='checkbox' name='ynflag' value='N'
						<% if(!(sv.ynflag).toString().trim().equalsIgnoreCase("Y")){
			%>
						checked <% }%> style="visibility: hidden"
						onclick="handleCheckBox('ynflag')" />
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("From")%></label>
					<table>
						<tr>
							<td>
									<%
						qpsf = fw.getFieldXMLDef((sv.statfrom).getFieldName());
						//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.statfrom,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
									<input name='statfrom' type='text'
										<%if((sv.statfrom).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <% }%> value='<%=valueThis%>'
										<%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
										title='<%=valueThis%>' <%}%>
										size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.statfrom.getLength(), sv.statfrom.getScale(),3)%>'
										maxLength='<%= sv.statfrom.getLength()%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(statfrom)'
										onKeyUp='return checkMaxLength(this)'
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event,true);'
										onBlur='return doBlurNumberNew(event,true);'
										<% 
	if((new Byte((sv.statfrom).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
										readonly="true" class="output_cell"
										<%
	}else if((new Byte((sv.statfrom).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
										class="bold_cell" <%
	}else { 
%>
										class=' <%=(sv.statfrom).getColor()== null  ? 
			"input_cell" :  (sv.statfrom).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
										<%
	} 
%>>
							</td>
							<td>&nbsp&nbsp<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp&nbsp</td>
							<td>
								<%	
			qpsf = fw.getFieldXMLDef((sv.statto).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis=smartHF.getPicFormatted(qpsf,sv.statto,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%> <input name='statto' type='text'
								<% if(browerVersion.equals(IE11)){%>
								style="margin-left: -7px; position: absolute;" <% }%>
								<%if((sv.statto).getClass().getSimpleName().equals("ZonedDecimalData")) 
{%>
								style="text-align: right; " <% }%>
								value='<%=valueThis%>'
								<%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								title='<%=valueThis%>' <%}%>
								size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.statto.getLength(), sv.statto.getScale(),3)%>'
								maxLength='<%= sv.statto.getLength()%>'
								onFocus='doFocus(this),onFocusRemoveCommas(this)'
								onHelp='return fieldHelp(statto)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event,true);'
								onBlur='return doBlurNumberNew(event,true);'
								<% 
	if((new Byte((sv.statto).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
								readonly="true" class="output_cell"
								<%
	}else if((new Byte((sv.statto).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
								class="bold_cell" <%
	}else { 
%>
								class=' <%=(sv.statto).getColor()== null  ? 
			"input_cell" :  (sv.statto).getColor().equals("red") ? 
			"input_cell red reverse"
								: "input_cell"%>'
								<%}%>>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("GL Accounts for PREMIUM")%></label>
					<div class="input-group">
						<input name='genlcdex' type='text'
							<%if((sv.genlcdex).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdex.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdex.getLength()%>'
							maxLength='<%= sv.genlcdex.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(genlcdex)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdex).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdex).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdex).getColor()== null  ? 
			"input_cell" :  (sv.genlcdex).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("GL Accounts for ACTUAL")%></label>
					<div class="input-group">
						<input name='genlcdey' type='text'
							<%if((sv.genlcdey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <% }%>
							<%

		formatValue = (sv.genlcdey.getFormData()).toString();

%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null && formatValue.trim().length()>0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%= sv.genlcdey.getLength()%>'
							maxLength='<%= sv.genlcdey.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(genlcdey)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.genlcdey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>
							readonly="true" class="output_cell"
							<%
	}else if((new Byte((sv.genlcdey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
							class="bold_cell" <%
	}else { 
%>
							class=' <%=(sv.genlcdey).getColor()== null  ? 
			"input_cell" :  (sv.genlcdey).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>