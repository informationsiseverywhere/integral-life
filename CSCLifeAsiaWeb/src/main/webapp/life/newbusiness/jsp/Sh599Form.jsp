

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH599";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh599ScreenVars sv = (Sh599ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Queue ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select Criteria ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Totals");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(leave blank for all)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Channel         ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Product         ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Month    Year");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Production Period ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.branch.setReverse(BaseScreenData.REVERSED);
			sv.branch.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.branch.setColor(BaseScreenData.WHITE);
			sv.branch.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.ctrlbrk01.setReverse(BaseScreenData.REVERSED);
			sv.ctrlbrk01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.ctrlbrk01.setColor(BaseScreenData.WHITE);
			sv.ctrlbrk01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.chdrtype.setReverse(BaseScreenData.REVERSED);
			sv.chdrtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrtype.setColor(BaseScreenData.WHITE);
			sv.chdrtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ctrlbrk02.setReverse(BaseScreenData.REVERSED);
			sv.ctrlbrk02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ctrlbrk02.setColor(BaseScreenData.WHITE);
			sv.ctrlbrk02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mnth.setReverse(BaseScreenData.REVERSED);
			sv.mnth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mnth.setColor(BaseScreenData.WHITE);
			sv.mnth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.year.setReverse(BaseScreenData.REVERSED);
			sv.year.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.year.setColor(BaseScreenData.WHITE);
			sv.year.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<style>
#acctyear{
 width:50%;
}
</style>

<div class="panel panel-default">
<div class="panel-body"> 
    
				<div class="row">	
			    	<div class="col-md-4"> 
				    	<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
				    		  <table>
				    		  	<tr>
					    		  	<td>
					    		  	 <%=smartHF.getHTMLVarReadOnly(fw, sv.scheduleName)%>
					    		  	</td>
					    		  	<td>
					    		  	<%=smartHF.getHTMLVarReadOnly(fw, sv.scheduleNumber) %>
					    		  	</td>
				    		  	</tr>
				    		  </table>				    		 
				    	</div>
				    </div>
				    	
				    <div class="col-md-4"> 
				    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
				    		 <table>
				    		  	<tr>
					    		  	<td>
					    		  	  <%=smartHF.getHTMLVarReadOnly(fw, sv.acctmonth)%>
					    		  	</td>
					    		  	<td>
					    		  	<%=smartHF.getHTMLVarReadOnly(fw, sv.acctyear) %>
					    		  	</td>
				    		  	</tr>
				    		  </table>	
				       </div>
				    </div>
				                 
				    <div class="col-md-4"> 
				    	<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		                         <%					
								if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
					</div>
				</div>
				
		</div>
					   
		<div class="row">	
			<div class="col-md-4"> 
				<div class="form-group">
				    <label><%=resourceBundleHandler.gettingValueFromBundle(" Job Queue")%></label>
                           <%					
							if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>	
			 </div>
		</div>	
					   
		 <div class="col-md-4"> 
			<div class="form-group">
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		                        		
					<%					
					if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="min-width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			</div>
		 </div>
			            
			<div class="col-md-4"> 
				<div class="form-group">
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
			               
  		
						<%					
						if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>	
				</div>
			</div>		
	</div>
				
				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Select Criteria")%><%=resourceBundleHandler.gettingValueFromBundle("(leave blank for all)")%></label>
				             </div>
				     </div>
				     <div class="col-md-4"> 
				    		<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Totals")%><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
				    		 </div>
				    </div>
				 </div>
				 
				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Channel")%></label>

								<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"branch"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("branch");
									optionValue = makeDropDownList( mappedItems , sv.branch.getFormData(),2,resourceBundleHandler);  
									longValue = (String) mappedItems.get((sv.branch.getFormData()).toString().trim());  
								%>
								
								<% 
									if((new Byte((sv.branch).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
								  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "output_cell" %>'>  
									   		<%if(longValue != null){%>
									   		
									   		<%=longValue%>
									   		
									   		<%}%>
									   </div>
								
								<%
								longValue = null;
								%>
								
									<% }else {%>
									
								<% if("red".equals((sv.branch).getColor())){
								%>
								<div style="border:1px; border-style: solid; border-color: #B55050;  width:230px;"> 
								<%
								} 
								%>
								
								<select name='branch' type='list' style="width:230px;"
								<% 
									if((new Byte((sv.branch).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
									readonly="true"
									disabled
									class="output_cell"
								<%
									}else if((new Byte((sv.branch).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								%>	
										class="bold_cell" 
								<%
									}else { 
								%>
									class = 'input_cell' 
								<%
									} 
								%>
								>
								<%=optionValue%>
								</select>
								<% if("red".equals((sv.branch).getColor())){
								%>
								</div>
								<%
								} 
								%>
								
								<%
								} 
								%>
                   </div>
                 </div>
                  <div class="col-md-1"> 
				    		<div class="form-group">
				    		  <label>&nbsp;</label>
							<input name='ctrlbrk01' 
							type='text'
							
							<%if((sv.ctrlbrk01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
							
							<%
							
									formatValue = (sv.ctrlbrk01.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.ctrlbrk01.getLength()%>'
							maxLength='<%= sv.ctrlbrk01.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(ctrlbrk01)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.ctrlbrk01).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.ctrlbrk01).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.ctrlbrk01).getColor()== null  ? 
										"input_cell" :  (sv.ctrlbrk01).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
              </div>
         </div>
       </div>
       
       <div class="row">	
			  <div class="col-md-4"> 
				   <div class="form-group">
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Product")%></label>
       
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"chdrtype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("chdrtype");
							optionValue = makeDropDownList( mappedItems , sv.chdrtype.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.chdrtype.getFormData()).toString().trim());  
						%>
						
						<% 
							if((new Byte((sv.chdrtype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
							
						<% if("red".equals((sv.chdrtype).getColor())){
						%>
						<div style="border:1px; border-style: solid; border-color: #B55050;  width:230px;"> 
						<%
						} 
						%>
						
						<select name='chdrtype' type='list' style="width:230px;"
						<% 
							if((new Byte((sv.chdrtype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
							readonly="true"
							disabled
							class="output_cell"
						<%
							}else if((new Byte((sv.chdrtype).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						<%
							}else { 
						%>
							class = 'input_cell' 
						<%
							} 
						%>
						>
						<%=optionValue%>
						</select>
						<% if("red".equals((sv.chdrtype).getColor())){
						%>
						</div>
						<%
						} 
						%>
						
						<%
						} 
						%>
      			</div>
   	  </div>
     <div class="col-md-1"> 
		<div class="form-group">
		 <label>&nbsp;</label>
			
				<input name='ctrlbrk02' 
				type='text'
				
				<%if((sv.ctrlbrk02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
				
				<%
				
						formatValue = (sv.ctrlbrk02.getFormData()).toString();
				
				%>
					value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.ctrlbrk02.getLength()%>'
				maxLength='<%= sv.ctrlbrk02.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(ctrlbrk02)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.ctrlbrk02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.ctrlbrk02).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.ctrlbrk02).getColor()== null  ? 
							"input_cell" :  (sv.ctrlbrk02).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
			</div>
		</div>
</div>

				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">
				    		  <label><%=resourceBundleHandler.gettingValueFromBundle("Last Production Period")%></label>
				    		    <div class="row">	
			    	              <div class="col-md-3" style="padding-right:0px;"> 
			    	                <div class="form-group">


									<input name='mnth' 
									type='text'
									
									<%if((sv.mnth).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
									
									<%
									
											formatValue = (sv.mnth.getFormData()).toString();
									
									%>
										value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
									
									size='<%= sv.mnth.getLength()%>'
									maxLength='<%= sv.mnth.getLength()%>' 
									onFocus='doFocus(this)' onHelp='return fieldHelp(mnth)' onKeyUp='return checkMaxLength(this)'  
									
									
									<% 
										if((new Byte((sv.mnth).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
									%>  
										readonly="true"
										class="output_cell"
									<%
										}else if((new Byte((sv.mnth).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>	
											class="bold_cell" 
									
									<%
										}else { 
									%>
									
										class = ' <%=(sv.mnth).getColor()== null  ? 
												"input_cell" :  (sv.mnth).getColor().equals("red") ? 
												"input_cell red reverse" : "input_cell" %>'
									 
									<%
										} 
									%>
									>
						</div>
				</div>
				
 <div class="col-md-3" style="padding-left:0px;"> 
	<div class="form-group">




			<input name='year' 
			type='text'
			
			<%if((sv.year).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
			
			<%
			
					formatValue = (sv.year.getFormData()).toString();
			
			%>
				value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
			
			size='<%= sv.year.getLength()%>'
			maxLength='<%= sv.year.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(year)' onKeyUp='return checkMaxLength(this)'  
			
			
			<% 
				if((new Byte((sv.year).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
				readonly="true"
				class="output_cell"
			<%
				}else if((new Byte((sv.year).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			
			<%
				}else { 
			%>
			
				class = ' <%=(sv.year).getColor()== null  ? 
						"input_cell" :  (sv.year).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>'
			 
			<%
				} 
			%>
			>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

