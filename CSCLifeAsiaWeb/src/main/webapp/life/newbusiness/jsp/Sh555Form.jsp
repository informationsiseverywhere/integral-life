<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH555";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%Sh555ScreenVars sv = (Sh555ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind01.isOn()) {
	sv.zundwrt.setEnabled(BaseScreenData.DISABLED);
}
}%>
<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4" >
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			    </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 <!--  <div class="col-md-2"></div>   -->
      <div class="col-md-4">
          <div class="form-group">
            <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>    
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
   </div>
 </div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
             <!--  <div class="input-group">	 -->	
             <table><tr><td>
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
   <!--  </div> -->
   </td></tr></table>
  </div>
 </div>
</div>
 <div class="row">
	<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
	<div class="col-md-4"></div>
	<div class="col-md-4"></div>
  </div>
  </br>
  <%
    ScreenDataHelper underwriteSubject = new ScreenDataHelper(fw.getVariables(), sv.zundwrt);
  %>
<div class="row">
      <div class="col-md-4">
	          <div class="form-group">
	           
                      <label><%=resourceBundleHandler.gettingValueFromBundle("Subject to Underwriting")%></label>
						<table><tr><td> 
                  <%if (underwriteSubject.isReadonly()) {%>
                   <%=smartHF.getRichText(0,0,fw,sv.zundwrt,( sv.zundwrt.getLength()+1),null).replace("absolute","relative")%>
                   <%} else {%>
                   <select name="zundwrt" type="list" class="<%=underwriteSubject.clazz()%>">
                   <option <%=underwriteSubject.selected("Y")%> value="Y">Y</option>
                   <option <%=underwriteSubject.selected("N")%> value="N">N</option>
                   </select><!-- Ticket ILIFE-770 ends-->
                  <%}%>
                  <!-- <div style="width:25px;"/> -->
 </td></tr></table> 
           </div>	    
          
        </div>		
     </div>
     <div class="col-md-4"></div>
     <div class="col-md-4"></div>
  </div>		
 </div>
<!-- Ticket ILIFE-770 starts--><%@ include file="/POLACommon2NEW.jsp"%>
<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%><!-- Ticket ILIFE-770 ends-->
