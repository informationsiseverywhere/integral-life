<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR529";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*" %>
<%Sr529ScreenVars sv = (Sr529ScreenVars) fw.getVariables();%>

<%if (sv.Sr529screenWritten.gt(0)) {%>
	<%Sr529screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Payment (at end of year)");%>
<%	generatedText10.appendClassString("label_txt");
	generatedText10.appendClassString("information_txt");
	generatedText10.appendClassString("underline");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
<%	generatedText11.appendClassString("label_txt");
	generatedText11.appendClassString("information_txt");
%>
	<%sv.zrnoyrs01.setClassString("");%>
<%	sv.zrnoyrs01.appendClassString("num_fld");
	sv.zrnoyrs01.appendClassString("input_txt");
	sv.zrnoyrs01.appendClassString("highlight");
%>
	<%sv.zrnoyrs02.setClassString("");%>
<%	sv.zrnoyrs02.appendClassString("num_fld");
	sv.zrnoyrs02.appendClassString("input_txt");
	sv.zrnoyrs02.appendClassString("highlight");
%>
	<%sv.zrnoyrs03.setClassString("");%>
<%	sv.zrnoyrs03.appendClassString("num_fld");
	sv.zrnoyrs03.appendClassString("input_txt");
	sv.zrnoyrs03.appendClassString("highlight");
%>
	<%sv.zrnoyrs04.setClassString("");%>
<%	sv.zrnoyrs04.appendClassString("num_fld");
	sv.zrnoyrs04.appendClassString("input_txt");
	sv.zrnoyrs04.appendClassString("highlight");
%>
	<%sv.zrnoyrs05.setClassString("");%>
<%	sv.zrnoyrs05.appendClassString("num_fld");
	sv.zrnoyrs05.appendClassString("input_txt");
	sv.zrnoyrs05.appendClassString("highlight");
%>
	<%sv.zrnoyrs06.setClassString("");%>
<%	sv.zrnoyrs06.appendClassString("num_fld");
	sv.zrnoyrs06.appendClassString("input_txt");
	sv.zrnoyrs06.appendClassString("highlight");
%>
	<%sv.zrnoyrs07.setClassString("");%>
<%	sv.zrnoyrs07.appendClassString("num_fld");
	sv.zrnoyrs07.appendClassString("input_txt");
	sv.zrnoyrs07.appendClassString("highlight");
%>
	<%sv.zrnoyrs08.setClassString("");%>
<%	sv.zrnoyrs08.appendClassString("num_fld");
	sv.zrnoyrs08.appendClassString("input_txt");
	sv.zrnoyrs08.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Term");%>
<%	generatedText12.appendClassString("label_txt");
	generatedText12.appendClassString("information_txt");
%>
	<%sv.zrterm01.setClassString("");%>
<%	sv.zrterm01.appendClassString("num_fld");
	sv.zrterm01.appendClassString("input_txt");
	sv.zrterm01.appendClassString("highlight");
%>
	<%sv.zrpcpd01.setClassString("");%>
<%	sv.zrpcpd01.appendClassString("num_fld");
	sv.zrpcpd01.appendClassString("input_txt");
	sv.zrpcpd01.appendClassString("highlight");
%>
	<%sv.zrpcpd02.setClassString("");%>
<%	sv.zrpcpd02.appendClassString("num_fld");
	sv.zrpcpd02.appendClassString("input_txt");
	sv.zrpcpd02.appendClassString("highlight");
%>
	<%sv.zrpcpd03.setClassString("");%>
<%	sv.zrpcpd03.appendClassString("num_fld");
	sv.zrpcpd03.appendClassString("input_txt");
	sv.zrpcpd03.appendClassString("highlight");
%>
	<%sv.zrpcpd04.setClassString("");%>
<%	sv.zrpcpd04.appendClassString("num_fld");
	sv.zrpcpd04.appendClassString("input_txt");
	sv.zrpcpd04.appendClassString("highlight");
%>
	<%sv.zrpcpd05.setClassString("");%>
<%	sv.zrpcpd05.appendClassString("num_fld");
	sv.zrpcpd05.appendClassString("input_txt");
	sv.zrpcpd05.appendClassString("highlight");
%>
	<%sv.zrpcpd06.setClassString("");%>
<%	sv.zrpcpd06.appendClassString("num_fld");
	sv.zrpcpd06.appendClassString("input_txt");
	sv.zrpcpd06.appendClassString("highlight");
%>
	<%sv.zrpcpd07.setClassString("");%>
<%	sv.zrpcpd07.appendClassString("num_fld");
	sv.zrpcpd07.appendClassString("input_txt");
	sv.zrpcpd07.appendClassString("highlight");
%>
	<%sv.zrpcpd08.setClassString("");%>
<%	sv.zrpcpd08.appendClassString("num_fld");
	sv.zrpcpd08.appendClassString("input_txt");
	sv.zrpcpd08.appendClassString("highlight");
%>
	<%sv.zrterm02.setClassString("");%>
<%	sv.zrterm02.appendClassString("num_fld");
	sv.zrterm02.appendClassString("input_txt");
	sv.zrterm02.appendClassString("highlight");
%>
	<%sv.zrpcpd09.setClassString("");%>
<%	sv.zrpcpd09.appendClassString("num_fld");
	sv.zrpcpd09.appendClassString("input_txt");
	sv.zrpcpd09.appendClassString("highlight");
%>
	<%sv.zrpcpd10.setClassString("");%>
<%	sv.zrpcpd10.appendClassString("num_fld");
	sv.zrpcpd10.appendClassString("input_txt");
	sv.zrpcpd10.appendClassString("highlight");
%>
	<%sv.zrpcpd11.setClassString("");%>
<%	sv.zrpcpd11.appendClassString("num_fld");
	sv.zrpcpd11.appendClassString("input_txt");
	sv.zrpcpd11.appendClassString("highlight");
%>
	<%sv.zrpcpd12.setClassString("");%>
<%	sv.zrpcpd12.appendClassString("num_fld");
	sv.zrpcpd12.appendClassString("input_txt");
	sv.zrpcpd12.appendClassString("highlight");
%>
	<%sv.zrpcpd13.setClassString("");%>
<%	sv.zrpcpd13.appendClassString("num_fld");
	sv.zrpcpd13.appendClassString("input_txt");
	sv.zrpcpd13.appendClassString("highlight");
%>
	<%sv.zrpcpd14.setClassString("");%>
<%	sv.zrpcpd14.appendClassString("num_fld");
	sv.zrpcpd14.appendClassString("input_txt");
	sv.zrpcpd14.appendClassString("highlight");
%>
	<%sv.zrpcpd15.setClassString("");%>
<%	sv.zrpcpd15.appendClassString("num_fld");
	sv.zrpcpd15.appendClassString("input_txt");
	sv.zrpcpd15.appendClassString("highlight");
%>
	<%sv.zrpcpd16.setClassString("");%>
<%	sv.zrpcpd16.appendClassString("num_fld");
	sv.zrpcpd16.appendClassString("input_txt");
	sv.zrpcpd16.appendClassString("highlight");
%>
	<%sv.zrterm03.setClassString("");%>
<%	sv.zrterm03.appendClassString("num_fld");
	sv.zrterm03.appendClassString("input_txt");
	sv.zrterm03.appendClassString("highlight");
%>
	<%sv.zrpcpd17.setClassString("");%>
<%	sv.zrpcpd17.appendClassString("num_fld");
	sv.zrpcpd17.appendClassString("input_txt");
	sv.zrpcpd17.appendClassString("highlight");
%>
	<%sv.zrpcpd18.setClassString("");%>
<%	sv.zrpcpd18.appendClassString("num_fld");
	sv.zrpcpd18.appendClassString("input_txt");
	sv.zrpcpd18.appendClassString("highlight");
%>
	<%sv.zrpcpd19.setClassString("");%>
<%	sv.zrpcpd19.appendClassString("num_fld");
	sv.zrpcpd19.appendClassString("input_txt");
	sv.zrpcpd19.appendClassString("highlight");
%>
	<%sv.zrpcpd20.setClassString("");%>
<%	sv.zrpcpd20.appendClassString("num_fld");
	sv.zrpcpd20.appendClassString("input_txt");
	sv.zrpcpd20.appendClassString("highlight");
%>
	<%sv.zrpcpd21.setClassString("");%>
<%	sv.zrpcpd21.appendClassString("num_fld");
	sv.zrpcpd21.appendClassString("input_txt");
	sv.zrpcpd21.appendClassString("highlight");
%>
	<%sv.zrpcpd22.setClassString("");%>
<%	sv.zrpcpd22.appendClassString("num_fld");
	sv.zrpcpd22.appendClassString("input_txt");
	sv.zrpcpd22.appendClassString("highlight");
%>
	<%sv.zrpcpd23.setClassString("");%>
<%	sv.zrpcpd23.appendClassString("num_fld");
	sv.zrpcpd23.appendClassString("input_txt");
	sv.zrpcpd23.appendClassString("highlight");
%>
	<%sv.zrpcpd24.setClassString("");%>
<%	sv.zrpcpd24.appendClassString("num_fld");
	sv.zrpcpd24.appendClassString("input_txt");
	sv.zrpcpd24.appendClassString("highlight");
%>
	<%sv.zrterm04.setClassString("");%>
<%	sv.zrterm04.appendClassString("num_fld");
	sv.zrterm04.appendClassString("input_txt");
	sv.zrterm04.appendClassString("highlight");
%>
	<%sv.zrpcpd25.setClassString("");%>
<%	sv.zrpcpd25.appendClassString("num_fld");
	sv.zrpcpd25.appendClassString("input_txt");
	sv.zrpcpd25.appendClassString("highlight");
%>
	<%sv.zrpcpd26.setClassString("");%>
<%	sv.zrpcpd26.appendClassString("num_fld");
	sv.zrpcpd26.appendClassString("input_txt");
	sv.zrpcpd26.appendClassString("highlight");
%>
	<%sv.zrpcpd27.setClassString("");%>
<%	sv.zrpcpd27.appendClassString("num_fld");
	sv.zrpcpd27.appendClassString("input_txt");
	sv.zrpcpd27.appendClassString("highlight");
%>
	<%sv.zrpcpd28.setClassString("");%>
<%	sv.zrpcpd28.appendClassString("num_fld");
	sv.zrpcpd28.appendClassString("input_txt");
	sv.zrpcpd28.appendClassString("highlight");
%>
	<%sv.zrpcpd29.setClassString("");%>
<%	sv.zrpcpd29.appendClassString("num_fld");
	sv.zrpcpd29.appendClassString("input_txt");
	sv.zrpcpd29.appendClassString("highlight");
%>
	<%sv.zrpcpd30.setClassString("");%>
<%	sv.zrpcpd30.appendClassString("num_fld");
	sv.zrpcpd30.appendClassString("input_txt");
	sv.zrpcpd30.appendClassString("highlight");
%>
	<%sv.zrpcpd31.setClassString("");%>
<%	sv.zrpcpd31.appendClassString("num_fld");
	sv.zrpcpd31.appendClassString("input_txt");
	sv.zrpcpd31.appendClassString("highlight");
%>
	<%sv.zrpcpd32.setClassString("");%>
<%	sv.zrpcpd32.appendClassString("num_fld");
	sv.zrpcpd32.appendClassString("input_txt");
	sv.zrpcpd32.appendClassString("highlight");
%>
	<%sv.zrterm05.setClassString("");%>
<%	sv.zrterm05.appendClassString("num_fld");
	sv.zrterm05.appendClassString("input_txt");
	sv.zrterm05.appendClassString("highlight");
%>
	<%sv.zrpcpd33.setClassString("");%>
<%	sv.zrpcpd33.appendClassString("num_fld");
	sv.zrpcpd33.appendClassString("input_txt");
	sv.zrpcpd33.appendClassString("highlight");
%>
	<%sv.zrpcpd34.setClassString("");%>
<%	sv.zrpcpd34.appendClassString("num_fld");
	sv.zrpcpd34.appendClassString("input_txt");
	sv.zrpcpd34.appendClassString("highlight");
%>
	<%sv.zrpcpd35.setClassString("");%>
<%	sv.zrpcpd35.appendClassString("num_fld");
	sv.zrpcpd35.appendClassString("input_txt");
	sv.zrpcpd35.appendClassString("highlight");
%>
	<%sv.zrpcpd36.setClassString("");%>
<%	sv.zrpcpd36.appendClassString("num_fld");
	sv.zrpcpd36.appendClassString("input_txt");
	sv.zrpcpd36.appendClassString("highlight");
%>
	<%sv.zrpcpd37.setClassString("");%>
<%	sv.zrpcpd37.appendClassString("num_fld");
	sv.zrpcpd37.appendClassString("input_txt");
	sv.zrpcpd37.appendClassString("highlight");
%>
	<%sv.zrpcpd38.setClassString("");%>
<%	sv.zrpcpd38.appendClassString("num_fld");
	sv.zrpcpd38.appendClassString("input_txt");
	sv.zrpcpd38.appendClassString("highlight");
%>
	<%sv.zrpcpd39.setClassString("");%>
<%	sv.zrpcpd39.appendClassString("num_fld");
	sv.zrpcpd39.appendClassString("input_txt");
	sv.zrpcpd39.appendClassString("highlight");
%>
	<%sv.zrpcpd40.setClassString("");%>
<%	sv.zrpcpd40.appendClassString("num_fld");
	sv.zrpcpd40.appendClassString("input_txt");
	sv.zrpcpd40.appendClassString("highlight");
%>
	<%sv.zrterm06.setClassString("");%>
<%	sv.zrterm06.appendClassString("num_fld");
	sv.zrterm06.appendClassString("input_txt");
	sv.zrterm06.appendClassString("highlight");
%>
	<%sv.zrpcpd41.setClassString("");%>
<%	sv.zrpcpd41.appendClassString("num_fld");
	sv.zrpcpd41.appendClassString("input_txt");
	sv.zrpcpd41.appendClassString("highlight");
%>
	<%sv.zrpcpd42.setClassString("");%>
<%	sv.zrpcpd42.appendClassString("num_fld");
	sv.zrpcpd42.appendClassString("input_txt");
	sv.zrpcpd42.appendClassString("highlight");
%>
	<%sv.zrpcpd43.setClassString("");%>
<%	sv.zrpcpd43.appendClassString("num_fld");
	sv.zrpcpd43.appendClassString("input_txt");
	sv.zrpcpd43.appendClassString("highlight");
%>
	<%sv.zrpcpd44.setClassString("");%>
<%	sv.zrpcpd44.appendClassString("num_fld");
	sv.zrpcpd44.appendClassString("input_txt");
	sv.zrpcpd44.appendClassString("highlight");
%>
	<%sv.zrpcpd45.setClassString("");%>
<%	sv.zrpcpd45.appendClassString("num_fld");
	sv.zrpcpd45.appendClassString("input_txt");
	sv.zrpcpd45.appendClassString("highlight");
%>
	<%sv.zrpcpd46.setClassString("");%>
<%	sv.zrpcpd46.appendClassString("num_fld");
	sv.zrpcpd46.appendClassString("input_txt");
	sv.zrpcpd46.appendClassString("highlight");
%>
	<%sv.zrpcpd47.setClassString("");%>
<%	sv.zrpcpd47.appendClassString("num_fld");
	sv.zrpcpd47.appendClassString("input_txt");
	sv.zrpcpd47.appendClassString("highlight");
%>
	<%sv.zrpcpd48.setClassString("");%>
<%	sv.zrpcpd48.appendClassString("num_fld");
	sv.zrpcpd48.appendClassString("input_txt");
	sv.zrpcpd48.appendClassString("highlight");
%>
	<%sv.zrterm07.setClassString("");%>
<%	sv.zrterm07.appendClassString("num_fld");
	sv.zrterm07.appendClassString("input_txt");
	sv.zrterm07.appendClassString("highlight");
%>
	<%sv.zrpcpd49.setClassString("");%>
<%	sv.zrpcpd49.appendClassString("num_fld");
	sv.zrpcpd49.appendClassString("input_txt");
	sv.zrpcpd49.appendClassString("highlight");
%>
	<%sv.zrpcpd50.setClassString("");%>
<%	sv.zrpcpd50.appendClassString("num_fld");
	sv.zrpcpd50.appendClassString("input_txt");
	sv.zrpcpd50.appendClassString("highlight");
%>
	<%sv.zrpcpd51.setClassString("");%>
<%	sv.zrpcpd51.appendClassString("num_fld");
	sv.zrpcpd51.appendClassString("input_txt");
	sv.zrpcpd51.appendClassString("highlight");
%>
	<%sv.zrpcpd52.setClassString("");%>
<%	sv.zrpcpd52.appendClassString("num_fld");
	sv.zrpcpd52.appendClassString("input_txt");
	sv.zrpcpd52.appendClassString("highlight");
%>
	<%sv.zrpcpd53.setClassString("");%>
<%	sv.zrpcpd53.appendClassString("num_fld");
	sv.zrpcpd53.appendClassString("input_txt");
	sv.zrpcpd53.appendClassString("highlight");
%>
	<%sv.zrpcpd54.setClassString("");%>
<%	sv.zrpcpd54.appendClassString("num_fld");
	sv.zrpcpd54.appendClassString("input_txt");
	sv.zrpcpd54.appendClassString("highlight");
%>
	<%sv.zrpcpd55.setClassString("");%>
<%	sv.zrpcpd55.appendClassString("num_fld");
	sv.zrpcpd55.appendClassString("input_txt");
	sv.zrpcpd55.appendClassString("highlight");
%>
	<%sv.zrpcpd56.setClassString("");%>
<%	sv.zrpcpd56.appendClassString("num_fld");
	sv.zrpcpd56.appendClassString("input_txt");
	sv.zrpcpd56.appendClassString("highlight");
%>
	<%sv.zrterm08.setClassString("");%>
<%	sv.zrterm08.appendClassString("num_fld");
	sv.zrterm08.appendClassString("input_txt");
	sv.zrterm08.appendClassString("highlight");
%>
	<%sv.zrpcpd57.setClassString("");%>
<%	sv.zrpcpd57.appendClassString("num_fld");
	sv.zrpcpd57.appendClassString("input_txt");
	sv.zrpcpd57.appendClassString("highlight");
%>
	<%sv.zrpcpd58.setClassString("");%>
<%	sv.zrpcpd58.appendClassString("num_fld");
	sv.zrpcpd58.appendClassString("input_txt");
	sv.zrpcpd58.appendClassString("highlight");
%>
	<%sv.zrpcpd59.setClassString("");%>
<%	sv.zrpcpd59.appendClassString("num_fld");
	sv.zrpcpd59.appendClassString("input_txt");
	sv.zrpcpd59.appendClassString("highlight");
%>
	<%sv.zrpcpd60.setClassString("");%>
<%	sv.zrpcpd60.appendClassString("num_fld");
	sv.zrpcpd60.appendClassString("input_txt");
	sv.zrpcpd60.appendClassString("highlight");
%>
	<%sv.zrpcpd61.setClassString("");%>
<%	sv.zrpcpd61.appendClassString("num_fld");
	sv.zrpcpd61.appendClassString("input_txt");
	sv.zrpcpd61.appendClassString("highlight");
%>
	<%sv.zrpcpd62.setClassString("");%>
<%	sv.zrpcpd62.appendClassString("num_fld");
	sv.zrpcpd62.appendClassString("input_txt");
	sv.zrpcpd62.appendClassString("highlight");
%>
	<%sv.zrpcpd63.setClassString("");%>
<%	sv.zrpcpd63.appendClassString("num_fld");
	sv.zrpcpd63.appendClassString("input_txt");
	sv.zrpcpd63.appendClassString("highlight");
%>
	<%sv.zrpcpd64.setClassString("");%>
<%	sv.zrpcpd64.appendClassString("num_fld");
	sv.zrpcpd64.appendClassString("input_txt");
	sv.zrpcpd64.appendClassString("highlight");
%>
	<%sv.zrterm09.setClassString("");%>
<%	sv.zrterm09.appendClassString("num_fld");
	sv.zrterm09.appendClassString("input_txt");
	sv.zrterm09.appendClassString("highlight");
%>
	<%sv.zrpcpd65.setClassString("");%>
<%	sv.zrpcpd65.appendClassString("num_fld");
	sv.zrpcpd65.appendClassString("input_txt");
	sv.zrpcpd65.appendClassString("highlight");
%>
	<%sv.zrpcpd66.setClassString("");%>
<%	sv.zrpcpd66.appendClassString("num_fld");
	sv.zrpcpd66.appendClassString("input_txt");
	sv.zrpcpd66.appendClassString("highlight");
%>
	<%sv.zrpcpd67.setClassString("");%>
<%	sv.zrpcpd67.appendClassString("num_fld");
	sv.zrpcpd67.appendClassString("input_txt");
	sv.zrpcpd67.appendClassString("highlight");
%>
	<%sv.zrpcpd68.setClassString("");%>
<%	sv.zrpcpd68.appendClassString("num_fld");
	sv.zrpcpd68.appendClassString("input_txt");
	sv.zrpcpd68.appendClassString("highlight");
%>
	<%sv.zrpcpd69.setClassString("");%>
<%	sv.zrpcpd69.appendClassString("num_fld");
	sv.zrpcpd69.appendClassString("input_txt");
	sv.zrpcpd69.appendClassString("highlight");
%>
	<%sv.zrpcpd70.setClassString("");%>
<%	sv.zrpcpd70.appendClassString("num_fld");
	sv.zrpcpd70.appendClassString("input_txt");
	sv.zrpcpd70.appendClassString("highlight");
%>
	<%sv.zrpcpd71.setClassString("");%>
<%	sv.zrpcpd71.appendClassString("num_fld");
	sv.zrpcpd71.appendClassString("input_txt");
	sv.zrpcpd71.appendClassString("highlight");
%>
	<%sv.zrpcpd72.setClassString("");%>
<%	sv.zrpcpd72.appendClassString("num_fld");
	sv.zrpcpd72.appendClassString("input_txt");
	sv.zrpcpd72.appendClassString("highlight");
%>
	<%sv.zrterm10.setClassString("");%>
<%	sv.zrterm10.appendClassString("num_fld");
	sv.zrterm10.appendClassString("input_txt");
	sv.zrterm10.appendClassString("highlight");
%>
	<%sv.zrpcpd73.setClassString("");%>
<%	sv.zrpcpd73.appendClassString("num_fld");
	sv.zrpcpd73.appendClassString("input_txt");
	sv.zrpcpd73.appendClassString("highlight");
%>
	<%sv.zrpcpd74.setClassString("");%>
<%	sv.zrpcpd74.appendClassString("num_fld");
	sv.zrpcpd74.appendClassString("input_txt");
	sv.zrpcpd74.appendClassString("highlight");
%>
	<%sv.zrpcpd75.setClassString("");%>
<%	sv.zrpcpd75.appendClassString("num_fld");
	sv.zrpcpd75.appendClassString("input_txt");
	sv.zrpcpd75.appendClassString("highlight");
%>
	<%sv.zrpcpd76.setClassString("");%>
<%	sv.zrpcpd76.appendClassString("num_fld");
	sv.zrpcpd76.appendClassString("input_txt");
	sv.zrpcpd76.appendClassString("highlight");
%>
	<%sv.zrpcpd77.setClassString("");%>
<%	sv.zrpcpd77.appendClassString("num_fld");
	sv.zrpcpd77.appendClassString("input_txt");
	sv.zrpcpd77.appendClassString("highlight");
%>
	<%sv.zrpcpd78.setClassString("");%>
<%	sv.zrpcpd78.appendClassString("num_fld");
	sv.zrpcpd78.appendClassString("input_txt");
	sv.zrpcpd78.appendClassString("highlight");
%>
	<%sv.zrpcpd79.setClassString("");%>
<%	sv.zrpcpd79.appendClassString("num_fld");
	sv.zrpcpd79.appendClassString("input_txt");
	sv.zrpcpd79.appendClassString("highlight");
%>
	<%sv.zrpcpd80.setClassString("");%>
<%	sv.zrpcpd80.appendClassString("num_fld");
	sv.zrpcpd80.appendClassString("input_txt");
	sv.zrpcpd80.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured Reduced by Benefit Payment Amount");%>
<%	generatedText13.appendClassString("label_txt");
	generatedText13.appendClassString("information_txt");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Non-Blank denotes YES");%>
<%	generatedText14.appendClassString("label_txt");
	generatedText14.appendClassString("information_txt");
%>
	<%sv.zredsumas.setClassString("");%>
<%	sv.zredsumas.appendClassString("string_fld");
	sv.zredsumas.appendClassString("input_txt");
	sv.zredsumas.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-1">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>

                           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        	
        	</div>
        	
        	<div class="col-md-3"></div>
        <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			
        			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        		
        		</div>
        	</div>
	
	<div class="col-md-1"></div>
	
	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<div class="input-group">	
        			<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>			
        </div>
        
         <div class="row">	
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>
				   <table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;</td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
        </div>
         <div class="row">	
		       	<div class="col-md-12">
				<div class="form-group">
				<center><label><u><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" Benefit Payment (at end of year)"))%></u></label></center>
									
				</div>
				</div>
			</div>		

             <div class="row">		
		 		<div class="col-md-1" style="max-width:110px;text-align: center;"> 
				<div class="form-group">
						<%-- <%=smartHF.getLit(7, 4, generatedText19)%> --%>
						<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Term"))%></label>				 
		 		</div>
		 		</div>
		 		
		 	
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:30px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrnoyrs08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 	
		 		</div>
   
                <div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd01, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd02, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd03, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd04, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd05, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd06, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd07, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd08, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	 <div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd09, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd10, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd11, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd12, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd13, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd14, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd15, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd16, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>

                 <div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd17, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd18, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd19, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd20, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd21, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd22, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd23, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd24, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	                 <div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd25, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd26, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd27, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd28, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd29, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd30, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd31, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd32, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	<div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd33, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd34, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd35, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd36, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd37, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd38, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd39, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd40, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
                
                <div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd41, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd42, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd43, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd44, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd45, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd46, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd47, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd48, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	<div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd49, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd50, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd51, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd52, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd53, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd54, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd55, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd56, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	               	<div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd57, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd58, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd59, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd60, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd61, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd62, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd63, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd64, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	               	<div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd65, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd66, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd67, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd68, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd69, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd70, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd71, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd72, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
               	
               	               	<div class="row">	
               <div class="col-md-1" style="min-width:100px;text-align: center;"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrterm10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		 		</div>
		 		</div>
		 		<!-- <div class="col-md-1" ></div> -->
		 		<div class="col-md-1" style="min-width:95px">
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd73, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd74, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">   
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd75, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">    
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd76, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd77, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd78, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 	    <div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd79, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1" style="min-width:95px">  
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zrpcpd80, COBOLHTMLFormatter.S3VS2)%>
		 		</div>
		 		</div> 
               	</div>
                  
                  &nbsp;
                   	<div class="row">	
                  <div class="col-md-6"> 
				<div class="form-group">
					<b><%=smartHF.getLit(generatedText13)%> </b>
						 
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1"> </div>
		 		<div class="col-md-3"> 
				<div class="form-group">
					<b><%=smartHF.getLit(generatedText14)%> </b>
						 
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">
					<%=smartHF.getHTMLVar(fw, sv.zredsumas)%>
						 
		 		</div>
		 		</div>
              </div>

</div>
</div>
















	<%--

<div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<!-- ILIFE 2433starts -->
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>
<!-- ILIFE 2433ends -->

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

&nbsp;

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
&nbsp;  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table>
</div> --%>

<%--
	<%=smartHF.getLit(7, 25, generatedText10)%>

	<%=smartHF.getLit(9, 2, generatedText11)%>

	<%=smartHF.getHTMLVar(9, 14, fw, sv.zrnoyrs01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 21, fw, sv.zrnoyrs02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 29, fw, sv.zrnoyrs03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 37, fw, sv.zrnoyrs04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 45, fw, sv.zrnoyrs05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 53, fw, sv.zrnoyrs06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 61, fw, sv.zrnoyrs07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 69, fw, sv.zrnoyrs08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(10, 3, generatedText12)%>

	<%=smartHF.getHTMLVar(11, 4, fw, sv.zrterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 12, fw, sv.zrpcpd01, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 20, fw, sv.zrpcpd02, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 28, fw, sv.zrpcpd03, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 36, fw, sv.zrpcpd04, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 44, fw, sv.zrpcpd05, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 52, fw, sv.zrpcpd06, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 60, fw, sv.zrpcpd07, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 68, fw, sv.zrpcpd08, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 4, fw, sv.zrterm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 12, fw, sv.zrpcpd09, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 20, fw, sv.zrpcpd10, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 28, fw, sv.zrpcpd11, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 36, fw, sv.zrpcpd12, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 44, fw, sv.zrpcpd13, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 52, fw, sv.zrpcpd14, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 60, fw, sv.zrpcpd15, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 68, fw, sv.zrpcpd16, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 4, fw, sv.zrterm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 12, fw, sv.zrpcpd17, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 20, fw, sv.zrpcpd18, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 28, fw, sv.zrpcpd19, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 36, fw, sv.zrpcpd20, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 44, fw, sv.zrpcpd21, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 52, fw, sv.zrpcpd22, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 60, fw, sv.zrpcpd23, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 68, fw, sv.zrpcpd24, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 4, fw, sv.zrterm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 12, fw, sv.zrpcpd25, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 20, fw, sv.zrpcpd26, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 28, fw, sv.zrpcpd27, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 36, fw, sv.zrpcpd28, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 44, fw, sv.zrpcpd29, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 52, fw, sv.zrpcpd30, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 60, fw, sv.zrpcpd31, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 68, fw, sv.zrpcpd32, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 4, fw, sv.zrterm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 12, fw, sv.zrpcpd33, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 20, fw, sv.zrpcpd34, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 28, fw, sv.zrpcpd35, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 36, fw, sv.zrpcpd36, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 44, fw, sv.zrpcpd37, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 52, fw, sv.zrpcpd38, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 60, fw, sv.zrpcpd39, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 68, fw, sv.zrpcpd40, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 4, fw, sv.zrterm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 12, fw, sv.zrpcpd41, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 20, fw, sv.zrpcpd42, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 28, fw, sv.zrpcpd43, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 36, fw, sv.zrpcpd44, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 44, fw, sv.zrpcpd45, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 52, fw, sv.zrpcpd46, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 60, fw, sv.zrpcpd47, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(16, 68, fw, sv.zrpcpd48, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 4, fw, sv.zrterm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 12, fw, sv.zrpcpd49, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 20, fw, sv.zrpcpd50, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 28, fw, sv.zrpcpd51, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 36, fw, sv.zrpcpd52, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 44, fw, sv.zrpcpd53, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 52, fw, sv.zrpcpd54, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 60, fw, sv.zrpcpd55, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(17, 68, fw, sv.zrpcpd56, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 4, fw, sv.zrterm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 12, fw, sv.zrpcpd57, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 20, fw, sv.zrpcpd58, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 28, fw, sv.zrpcpd59, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 36, fw, sv.zrpcpd60, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 44, fw, sv.zrpcpd61, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 52, fw, sv.zrpcpd62, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 60, fw, sv.zrpcpd63, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(18, 68, fw, sv.zrpcpd64, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 4, fw, sv.zrterm09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 12, fw, sv.zrpcpd65, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 20, fw, sv.zrpcpd66, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 28, fw, sv.zrpcpd67, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 36, fw, sv.zrpcpd68, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 44, fw, sv.zrpcpd69, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 52, fw, sv.zrpcpd70, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 60, fw, sv.zrpcpd71, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(19, 68, fw, sv.zrpcpd72, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 4, fw, sv.zrterm10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 12, fw, sv.zrpcpd73, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 20, fw, sv.zrpcpd74, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 28, fw, sv.zrpcpd75, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 36, fw, sv.zrpcpd76, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 44, fw, sv.zrpcpd77, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 52, fw, sv.zrpcpd78, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 60, fw, sv.zrpcpd79, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(20, 68, fw, sv.zrpcpd80, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getLit(22, 2, generatedText13)%>

	<%=smartHF.getLit(22, 49, generatedText14)%>

	<%=smartHF.getHTMLVar(22, 73, fw, sv.zredsumas)%>
 --%>


<%}%>

<%if (sv.Sr529protectWritten.gt(0)) {%>
	<%Sr529protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>

<%@ include file="/POLACommon2NEW.jsp"%>
