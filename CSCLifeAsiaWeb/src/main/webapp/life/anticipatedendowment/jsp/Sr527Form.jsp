

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR527";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*" %>
<%Sr527ScreenVars sv = (Sr527ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Options for manipulating Cash Accounts ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandatory Options  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"----------------------------------------------------------------");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Option     ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other Options      ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.payOption01.setReverse(BaseScreenData.REVERSED);
			sv.payOption01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.payOption01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.payOption02.setReverse(BaseScreenData.REVERSED);
			sv.payOption02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.payOption02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.payOption03.setReverse(BaseScreenData.REVERSED);
			sv.payOption03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.payOption03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.payOption04.setReverse(BaseScreenData.REVERSED);
			sv.payOption04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.payOption04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payOption05.setReverse(BaseScreenData.REVERSED);
			sv.payOption05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.payOption05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.payOption06.setReverse(BaseScreenData.REVERSED);
			sv.payOption06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.payOption06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.payOption07.setReverse(BaseScreenData.REVERSED);
			sv.payOption07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.payOption07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.payOption08.setReverse(BaseScreenData.REVERSED);
			sv.payOption08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.payOption08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.payOption09.setReverse(BaseScreenData.REVERSED);
			sv.payOption09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.payOption09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.payOption10.setReverse(BaseScreenData.REVERSED);
			sv.payOption10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.payOption10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.payOption11.setReverse(BaseScreenData.REVERSED);
			sv.payOption11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.payOption11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.payOption12.setReverse(BaseScreenData.REVERSED);
			sv.payOption12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.payOption12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.payOption13.setReverse(BaseScreenData.REVERSED);
			sv.payOption13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.payOption13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.payOption14.setReverse(BaseScreenData.REVERSED);
			sv.payOption14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.payOption14.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">	
    <div class="panel-body">     
			 <div class="row">	
			    <div class="col-md-1"> 
    	 			<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%					
							                    if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.company.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.company.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					    		</div>
					    	</div>
					    	
					    	<div class="col-md-3"> </div>
					    	
					    	<div class="col-md-2"> 
    	 			<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		
					    		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
					   </div>
					   </div> 		
					   
					    <div class="col-md-2"> </div>
					   <div class="col-md-2"> 
    	 			<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    <div class="input-group"> 			
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                                   </div>
					    		</div>
					    	</div>	
					   </div> 	

                 <div class="row">	
			    <div class="col-md-6"> 
    	 			<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Options for manipulating Cash Accounts")%></label>
					   </div>
					 </div>
				</div>	
				
				<div class="row">	
			    <div class="col-md-3"> 
    	 			<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Mandatory Options")%></label>
					   </div>
					 </div>
					 
					 <div class="col-md-2"> 
    	 			<div class="form-group"> 	        				    			  
					    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption01");
	optionValue = makeDropDownList( mappedItems , sv.payOption01.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					   </div>
					 </div>
					 
					  <div class="col-md-2"> 
    	 			<div class="form-group">
    	 			<%					
		if(!((sv.pymdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 			</div>
    	 			</div>
				</div>    
			<!-- 2 -->	
				<div class="row">	
				
				<div class="col-md-3"></div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption02");
	optionValue = makeDropDownList( mappedItems , sv.payOption02.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   </div>
    	 	   
    	 	   <!-- 3 -->
    	 	   
    	 	   <div class="row">	
				
				<div class="col-md-3"></div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption03");
	optionValue = makeDropDownList( mappedItems , sv.payOption03.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   </div>
    	 	   
    	 	   <!-- 4 -->
    	 	   
    	 	   <div class="row">	
				
				<div class="col-md-3"></div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption04");
	optionValue = makeDropDownList( mappedItems , sv.payOption04.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 				<%					
		if(!((sv.pymdesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 	   </div>
    	 	   
    	 	   </div>
    	 <hr>	   
    	 
    	 <div class="row">	
				
				<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Default Option")%></label>
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption05");
	optionValue = makeDropDownList( mappedItems , sv.payOption05.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 		</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 		</div>
    	 		</div>
    	 		
    	 		</div>	
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Other Options")%></label>
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption06");
	optionValue = makeDropDownList( mappedItems , sv.payOption06.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>			
    	 		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption07");
	optionValue = makeDropDownList( mappedItems , sv.payOption07.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption08");
	optionValue = makeDropDownList( mappedItems , sv.payOption08.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 					<%					
		if(!((sv.pymdesc08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption09");
	optionValue = makeDropDownList( mappedItems , sv.payOption09.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption10");
	optionValue = makeDropDownList( mappedItems , sv.payOption10.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption11");
	optionValue = makeDropDownList( mappedItems , sv.payOption11.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption11' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc11.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption12");
	optionValue = makeDropDownList( mappedItems , sv.payOption12.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption12' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc12.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption13"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption13");
	optionValue = makeDropDownList( mappedItems , sv.payOption13.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption13.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption13).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption13' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption13).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc13.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		
    	 		
    	 		<div class="row">	
				
				<div class="col-md-3">
				
				</div>
				
			    <div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption14"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption14");
	optionValue = makeDropDownList( mappedItems , sv.payOption14.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption14.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption14).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption14' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption14).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
    	 			</div>
    	 		</div>
    	 		
    	 		<div class="col-md-2"> 
    	 			<div class="form-group"> 
    	 			<%					
		if(!((sv.pymdesc14.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 			</div>
    	 		</div>	
    	 		
    	 		</div>		   				
			</div>
	</div>				    		


















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<!-- ILIFE 2433starts -->
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>
<!-- ILIFE 2433starts -->


<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Options for manipulating Cash Accounts")%>
</div>
</td></tr></table><br/><table>

<tr style='height:22px;'><td width='140px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Mandatory Options")%>
</div>
</td>


<td></td><td width='180px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption01");
	optionValue = makeDropDownList( mappedItems , sv.payOption01.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='180px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption02");
	optionValue = makeDropDownList( mappedItems , sv.payOption02.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption03");
	optionValue = makeDropDownList( mappedItems , sv.payOption03.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption04");
	optionValue = makeDropDownList( mappedItems , sv.payOption04.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/>
<div class='grayline' style='position: relative;  
left:5px; height:10px; width:750px;'>
&nbsp;
</div><br/><table>

<tr style='height:22px;'><td width='140px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Default Option")%>
</div>
</td>


<td></td><td width='180px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption05");
	optionValue = makeDropDownList( mappedItems , sv.payOption05.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='140px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Other Options")%>
</div>
</td>


<td></td><td width='180px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption06");
	optionValue = makeDropDownList( mappedItems , sv.payOption06.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption07");
	optionValue = makeDropDownList( mappedItems , sv.payOption07.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption08");
	optionValue = makeDropDownList( mappedItems , sv.payOption08.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption09");
	optionValue = makeDropDownList( mappedItems , sv.payOption09.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption10");
	optionValue = makeDropDownList( mappedItems , sv.payOption10.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption11");
	optionValue = makeDropDownList( mappedItems , sv.payOption11.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption11' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc11.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption12");
	optionValue = makeDropDownList( mappedItems , sv.payOption12.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption12' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc12.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption13"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption13");
	optionValue = makeDropDownList( mappedItems , sv.payOption13.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption13.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption13).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption13' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption13).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc13.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>


<tr style='height:22px;'><td></td><td></td><td width='140px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption14"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption14");
	optionValue = makeDropDownList( mappedItems , sv.payOption14.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption14.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption14).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption14' type='list' style="width:140px;"
<% 
	if((new Byte((sv.payOption14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption14).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td></td><td>


	
  		
		<%					
		if(!((sv.pymdesc14.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("----------------------------------------------------------------")%>
</div>

</tr></table></div><br/></div> --%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>

<%@ include file="/POLACommon2NEW.jsp"%>

