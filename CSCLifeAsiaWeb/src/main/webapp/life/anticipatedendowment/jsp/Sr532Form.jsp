<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR532";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*" %>
<%Sr532ScreenVars sv = (Sr532ScreenVars) fw.getVariables();%>

<%if (sv.Sr532screenWritten.gt(0)) {%>
	<%Sr532screen.clearClassString(sv);%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
	<%sv.chdrcoy.setClassString("");%>
<%	sv.chdrcoy.appendClassString("string_fld");
	sv.chdrcoy.appendClassString("output_txt");
	sv.chdrcoy.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Product");%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.cntdesc.setClassString("");%>
<%	sv.cntdesc.appendClassString("string_fld");
	sv.cntdesc.appendClassString("output_txt");
	sv.cntdesc.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cover");%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Currency");%>
	<%sv.paycurr.setClassString("");%>
<%	sv.paycurr.appendClassString("string_fld");
	sv.paycurr.appendClassString("output_txt");
	sv.paycurr.appendClassString("highlight");
%>
	<%sv.currdesc.setClassString("");%>
<%	sv.currdesc.appendClassString("string_fld");
	sv.currdesc.appendClassString("output_txt");
	sv.currdesc.appendClassString("highlight");
%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Due Date");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percent");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Release Date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay Status");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay Method");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Option");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method");%>
	<%sv.zrdate01Disp.setClassString("");%>
<%	sv.zrdate01Disp.appendClassString("string_fld");
	sv.zrdate01Disp.appendClassString("output_txt");
	sv.zrdate01Disp.appendClassString("highlight");
%>
	<%sv.prcnt01.setClassString("");%>
<%	sv.prcnt01.appendClassString("num_fld");
	sv.prcnt01.appendClassString("output_txt");
	sv.prcnt01.appendClassString("highlight");
%>
	<%sv.zrpaydt01Disp.setClassString("");%>
<%	sv.zrpaydt01Disp.appendClassString("string_fld");
	sv.zrpaydt01Disp.appendClassString("output_txt");
	sv.zrpaydt01Disp.appendClassString("highlight");
%>
	<%sv.paysts01.setClassString("");%>
<%	sv.paysts01.appendClassString("string_fld");
	sv.paysts01.appendClassString("output_txt");
	sv.paysts01.appendClassString("highlight");
%>
	<%sv.paymentMethod01.setClassString("");%>
<%	sv.paymentMethod01.appendClassString("string_fld");
	sv.paymentMethod01.appendClassString("output_txt");
	sv.paymentMethod01.appendClassString("highlight");
%>
	<%sv.payOption01.setClassString("");%>
<%	sv.payOption01.appendClassString("string_fld");
	sv.payOption01.appendClassString("output_txt");
	sv.payOption01.appendClassString("highlight");
%>
	<%sv.paid01.setClassString("");%>
<%	sv.paid01.appendClassString("num_fld");
	sv.paid01.appendClassString("output_txt");
	sv.paid01.appendClassString("highlight");
%>
	<%sv.zrdate02Disp.setClassString("");%>
<%	sv.zrdate02Disp.appendClassString("string_fld");
	sv.zrdate02Disp.appendClassString("output_txt");
	sv.zrdate02Disp.appendClassString("highlight");
%>
	<%sv.prcnt02.setClassString("");%>
<%	sv.prcnt02.appendClassString("num_fld");
	sv.prcnt02.appendClassString("output_txt");
	sv.prcnt02.appendClassString("highlight");
%>
	<%sv.zrpaydt02Disp.setClassString("");%>
<%	sv.zrpaydt02Disp.appendClassString("string_fld");
	sv.zrpaydt02Disp.appendClassString("output_txt");
	sv.zrpaydt02Disp.appendClassString("highlight");
%>
	<%sv.paysts02.setClassString("");%>
<%	sv.paysts02.appendClassString("string_fld");
	sv.paysts02.appendClassString("output_txt");
	sv.paysts02.appendClassString("highlight");
%>
	<%sv.paymentMethod02.setClassString("");%>
<%	sv.paymentMethod02.appendClassString("string_fld");
	sv.paymentMethod02.appendClassString("output_txt");
	sv.paymentMethod02.appendClassString("highlight");
%>
	<%sv.payOption02.setClassString("");%>
<%	sv.payOption02.appendClassString("string_fld");
	sv.payOption02.appendClassString("output_txt");
	sv.payOption02.appendClassString("highlight");
%>
	<%sv.paid02.setClassString("");%>
<%	sv.paid02.appendClassString("num_fld");
	sv.paid02.appendClassString("output_txt");
	sv.paid02.appendClassString("highlight");
%>
	<%sv.zrdate03Disp.setClassString("");%>
<%	sv.zrdate03Disp.appendClassString("string_fld");
	sv.zrdate03Disp.appendClassString("output_txt");
	sv.zrdate03Disp.appendClassString("highlight");
%>
	<%sv.prcnt03.setClassString("");%>
<%	sv.prcnt03.appendClassString("num_fld");
	sv.prcnt03.appendClassString("output_txt");
	sv.prcnt03.appendClassString("highlight");
%>
	<%sv.zrpaydt03Disp.setClassString("");%>
<%	sv.zrpaydt03Disp.appendClassString("string_fld");
	sv.zrpaydt03Disp.appendClassString("output_txt");
	sv.zrpaydt03Disp.appendClassString("highlight");
%>
	<%sv.paysts03.setClassString("");%>
<%	sv.paysts03.appendClassString("string_fld");
	sv.paysts03.appendClassString("output_txt");
	sv.paysts03.appendClassString("highlight");
%>
	<%sv.paymentMethod03.setClassString("");%>
<%	sv.paymentMethod03.appendClassString("string_fld");
	sv.paymentMethod03.appendClassString("output_txt");
	sv.paymentMethod03.appendClassString("highlight");
%>
	<%sv.payOption03.setClassString("");%>
<%	sv.payOption03.appendClassString("string_fld");
	sv.payOption03.appendClassString("output_txt");
	sv.payOption03.appendClassString("highlight");
%>
	<%sv.paid03.setClassString("");%>
<%	sv.paid03.appendClassString("num_fld");
	sv.paid03.appendClassString("output_txt");
	sv.paid03.appendClassString("highlight");
%>
	<%sv.zrdate04Disp.setClassString("");%>
<%	sv.zrdate04Disp.appendClassString("string_fld");
	sv.zrdate04Disp.appendClassString("output_txt");
	sv.zrdate04Disp.appendClassString("highlight");
%>
	<%sv.prcnt04.setClassString("");%>
<%	sv.prcnt04.appendClassString("num_fld");
	sv.prcnt04.appendClassString("output_txt");
	sv.prcnt04.appendClassString("highlight");
%>
	<%sv.zrpaydt04Disp.setClassString("");%>
<%	sv.zrpaydt04Disp.appendClassString("string_fld");
	sv.zrpaydt04Disp.appendClassString("output_txt");
	sv.zrpaydt04Disp.appendClassString("highlight");
%>
	<%sv.paysts04.setClassString("");%>
<%	sv.paysts04.appendClassString("string_fld");
	sv.paysts04.appendClassString("output_txt");
	sv.paysts04.appendClassString("highlight");
%>
	<%sv.paymentMethod04.setClassString("");%>
<%	sv.paymentMethod04.appendClassString("string_fld");
	sv.paymentMethod04.appendClassString("output_txt");
	sv.paymentMethod04.appendClassString("highlight");
%>
	<%sv.payOption04.setClassString("");%>
<%	sv.payOption04.appendClassString("string_fld");
	sv.payOption04.appendClassString("output_txt");
	sv.payOption04.appendClassString("highlight");
%>
	<%sv.paid04.setClassString("");%>
<%	sv.paid04.appendClassString("num_fld");
	sv.paid04.appendClassString("output_txt");
	sv.paid04.appendClassString("highlight");
%>
	<%sv.zrdate05Disp.setClassString("");%>
<%	sv.zrdate05Disp.appendClassString("string_fld");
	sv.zrdate05Disp.appendClassString("output_txt");
	sv.zrdate05Disp.appendClassString("highlight");
%>
	<%sv.prcnt05.setClassString("");%>
<%	sv.prcnt05.appendClassString("num_fld");
	sv.prcnt05.appendClassString("output_txt");
	sv.prcnt05.appendClassString("highlight");
%>
	<%sv.zrpaydt05Disp.setClassString("");%>
<%	sv.zrpaydt05Disp.appendClassString("string_fld");
	sv.zrpaydt05Disp.appendClassString("output_txt");
	sv.zrpaydt05Disp.appendClassString("highlight");
%>
	<%sv.paysts05.setClassString("");%>
<%	sv.paysts05.appendClassString("string_fld");
	sv.paysts05.appendClassString("output_txt");
	sv.paysts05.appendClassString("highlight");
%>
	<%sv.paymentMethod05.setClassString("");%>
<%	sv.paymentMethod05.appendClassString("string_fld");
	sv.paymentMethod05.appendClassString("output_txt");
	sv.paymentMethod05.appendClassString("highlight");
%>
	<%sv.payOption05.setClassString("");%>
<%	sv.payOption05.appendClassString("string_fld");
	sv.payOption05.appendClassString("output_txt");
	sv.payOption05.appendClassString("highlight");
%>
	<%sv.paid05.setClassString("");%>
<%	sv.paid05.appendClassString("num_fld");
	sv.paid05.appendClassString("output_txt");
	sv.paid05.appendClassString("highlight");
%>
	<%sv.zrdate06Disp.setClassString("");%>
<%	sv.zrdate06Disp.appendClassString("string_fld");
	sv.zrdate06Disp.appendClassString("output_txt");
	sv.zrdate06Disp.appendClassString("highlight");
%>
	<%sv.prcnt06.setClassString("");%>
<%	sv.prcnt06.appendClassString("num_fld");
	sv.prcnt06.appendClassString("output_txt");
	sv.prcnt06.appendClassString("highlight");
%>
	<%sv.zrpaydt06Disp.setClassString("");%>
<%	sv.zrpaydt06Disp.appendClassString("string_fld");
	sv.zrpaydt06Disp.appendClassString("output_txt");
	sv.zrpaydt06Disp.appendClassString("highlight");
%>
	<%sv.paysts06.setClassString("");%>
<%	sv.paysts06.appendClassString("string_fld");
	sv.paysts06.appendClassString("output_txt");
	sv.paysts06.appendClassString("highlight");
%>
	<%sv.paymentMethod06.setClassString("");%>
<%	sv.paymentMethod06.appendClassString("string_fld");
	sv.paymentMethod06.appendClassString("output_txt");
	sv.paymentMethod06.appendClassString("highlight");
%>
	<%sv.payOption06.setClassString("");%>
<%	sv.payOption06.appendClassString("string_fld");
	sv.payOption06.appendClassString("output_txt");
	sv.payOption06.appendClassString("highlight");
%>
	<%sv.paid06.setClassString("");%>
<%	sv.paid06.appendClassString("num_fld");
	sv.paid06.appendClassString("output_txt");
	sv.paid06.appendClassString("highlight");
%>
	<%sv.zrdate07Disp.setClassString("");%>
<%	sv.zrdate07Disp.appendClassString("string_fld");
	sv.zrdate07Disp.appendClassString("output_txt");
	sv.zrdate07Disp.appendClassString("highlight");
%>
	<%sv.prcnt07.setClassString("");%>
<%	sv.prcnt07.appendClassString("num_fld");
	sv.prcnt07.appendClassString("output_txt");
	sv.prcnt07.appendClassString("highlight");
%>
	<%sv.zrpaydt07Disp.setClassString("");%>
<%	sv.zrpaydt07Disp.appendClassString("string_fld");
	sv.zrpaydt07Disp.appendClassString("output_txt");
	sv.zrpaydt07Disp.appendClassString("highlight");
%>
	<%sv.paysts07.setClassString("");%>
<%	sv.paysts07.appendClassString("string_fld");
	sv.paysts07.appendClassString("output_txt");
	sv.paysts07.appendClassString("highlight");
%>
	<%sv.paymentMethod07.setClassString("");%>
<%	sv.paymentMethod07.appendClassString("string_fld");
	sv.paymentMethod07.appendClassString("output_txt");
	sv.paymentMethod07.appendClassString("highlight");
%>
	<%sv.payOption07.setClassString("");%>
<%	sv.payOption07.appendClassString("string_fld");
	sv.payOption07.appendClassString("output_txt");
	sv.payOption07.appendClassString("highlight");
%>
	<%sv.paid07.setClassString("");%>
<%	sv.paid07.appendClassString("num_fld");
	sv.paid07.appendClassString("output_txt");
	sv.paid07.appendClassString("highlight");
%>
	<%sv.zrdate08Disp.setClassString("");%>
<%	sv.zrdate08Disp.appendClassString("string_fld");
	sv.zrdate08Disp.appendClassString("output_txt");
	sv.zrdate08Disp.appendClassString("highlight");
%>
	<%sv.prcnt08.setClassString("");%>
<%	sv.prcnt08.appendClassString("num_fld");
	sv.prcnt08.appendClassString("output_txt");
	sv.prcnt08.appendClassString("highlight");
%>
	<%sv.zrpaydt08Disp.setClassString("");%>
<%	sv.zrpaydt08Disp.appendClassString("string_fld");
	sv.zrpaydt08Disp.appendClassString("output_txt");
	sv.zrpaydt08Disp.appendClassString("highlight");
%>
	<%sv.paysts08.setClassString("");%>
<%	sv.paysts08.appendClassString("string_fld");
	sv.paysts08.appendClassString("output_txt");
	sv.paysts08.appendClassString("highlight");
%>
	<%sv.paymentMethod08.setClassString("");%>
<%	sv.paymentMethod08.appendClassString("string_fld");
	sv.paymentMethod08.appendClassString("output_txt");
	sv.paymentMethod08.appendClassString("highlight");
%>
	<%sv.payOption08.setClassString("");%>
<%	sv.payOption08.appendClassString("string_fld");
	sv.payOption08.appendClassString("output_txt");
	sv.payOption08.appendClassString("highlight");
%>
	<%sv.paid08.setClassString("");%>
<%	sv.paid08.appendClassString("num_fld");
	sv.paid08.appendClassString("output_txt");
	sv.paid08.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total");%>
	<%sv.paid09.setClassString("");%>
<%	sv.paid09.appendClassString("num_fld");
	sv.paid09.appendClassString("output_txt");
	sv.paid09.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Payment");%>
	<%sv.zrdate09Disp.setClassString("");%>
<%	sv.zrdate09Disp.appendClassString("string_fld");
	sv.zrdate09Disp.appendClassString("output_txt");
	sv.zrdate09Disp.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Option");%>
	<%sv.payOption09.setClassString("");%>
	<%sv.pymdesc.setClassString("");%>
<%	sv.pymdesc.appendClassString("string_fld");
	sv.pymdesc.appendClassString("output_txt");
	sv.pymdesc.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee");%>
	<%sv.payeesel.setClassString("");%>
	<%sv.payeenme.setClassString("");%>
<%	sv.payeenme.appendClassString("string_fld");
	sv.payeenme.appendClassString("output_txt");
	sv.payeenme.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Meth");%>
	<%sv.payMethod.setClassString("");%>
	<%sv.descrip.setClassString("");%>
<%	sv.descrip.appendClassString("string_fld");
	sv.descrip.appendClassString("output_txt");
	sv.descrip.appendClassString("highlight");
%>
	<%sv.optdsc.setClassString("");%>
<%	sv.optdsc.appendClassString("string_fld");
	sv.optdsc.appendClassString("output_txt");
	sv.optdsc.appendClassString("highlight");
%>
	<%sv.optind.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.payOption09.setReverse(BaseScreenData.REVERSED);
			sv.payOption09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.payOption09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.payeesel.setReverse(BaseScreenData.REVERSED);
			sv.payeesel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.payeesel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.payMethod.setReverse(BaseScreenData.REVERSED);
			sv.payMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.payMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.optind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.optind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.optind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.optind.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<style type="text/css">

.panel.panel-default{

  height: 750px !important;
}
}

</style>
<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-1">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			
        			   <%					
		if(!((sv.chdrcoy.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrcoy.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrcoy.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        	
        	</div>	
        	
        	<div class="col-md-3"></div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
        				<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        			</div>
        		</div>		
        	
        	<div class="col-md-2"></div>
        	
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Product"))%></label>
        				    <div class="input-group">
        				<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cntdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>                  
                       </div>
        			</div>
        		</div>		
        	</div>
        	
        	
   		
   		 <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life"))%></label>
        				<div class="input-group">
        				<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        				</div>
        			</div>
        		</div>		
        		
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Cover"))%></label>
        				<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue =  formatValue(longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        			</div>
        		</div>		
        		
        		<div class="col-md-2"></div>
        		<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rider"))%></label>
        				<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue =  formatValue(longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue =  longValue;
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        			</div>
        		</div>		
        		</div>
        		
        	 <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee"))%></label>
        				<div class="input-group">
        				 <%if ((new Byte((sv.payeesel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.payeesel.getFormData();  
%>

<% 
	if((new Byte((sv.payeesel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell1" : "output_cell1" %>' style="max-width:300px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='payeesel' 
type='text' 
value='<%=sv.payeesel.getFormData()%>' 
maxLength='<%=sv.payeesel.getLength()%>' 
size='<%=sv.payeesel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(payeesel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.payeesel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.payeesel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('payeesel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payeesel')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
<%
	}else { 
%>

class = ' <%=(sv.payeesel).getColor()== null  ? 
"input_cell" :  (sv.payeesel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payeesel')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>

<%}longValue = null;}} %>






<%if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<!-- ILIFE-2185 top element Added -->
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>' style="float: left !important;margin-left: 14px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;"> 
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        			</div>
        		</div>			
        	</div>
        		
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%if (sv.actionflag.compareTo("N") != 0) {%><label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Option"))%></label><%}%>
        		<%if (sv.actionflag.compareTo("N") == 0) {%> <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Option"))%></label> <%}%>
        				<%	
	if ((new Byte((sv.payOption09).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption09");
	optionValue = makeDropDownList( mappedItems , sv.payOption09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption09'

<% 	if (appVars.ind07.isOn()) { %> 
 onchange="populatePayMethod(this.value);"  
 <% } %>

 type='list' style="width:140px; font-family: Arial, Verdana,  Helvetica, sans-serif"
<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption09).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
        			</div>
        		</div>		
        		
        		<div class="col-md-2"></div>
        		 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Meth"))%></label>
        				<%	
	if ((new Byte((sv.payMethod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payMethod");
	optionValue = makeDropDownList( mappedItems , sv.payMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>


		

<select name='payMethod' type='list' 

<% 	if (appVars.ind07.isOn()) { %>
disabled		
		<%}%> 

style="width:140px;font-family: Arial, Verdana,  Helvetica, sans-serif"
<% 
	if((new Byte((sv.payMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% 	if (appVars.ind07.isOn()) { %>
 <input type="hidden" id="hiddenPayMethod" name="payMethod" value=""/>
		<%}%> 

<% if("red".equals((sv.payMethod).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
        			</div>
        		</div>		
        	</div>
        	
        	 <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Payment"))%></label>
        				<%if ((new Byte((sv.zrdate09Disp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.zrdate09Disp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zrdate09Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zrdate09Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        	 </div>
        	 </div>	
        	 
        	  <div class="col-md-2"></div>

        	  <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Currency"))%></label>
        				<%if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("paycurr");
		longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.paycurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paycurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paycurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        	 </div>
        	 </div>		
        	 
        	 <div class="col-md-1"></div>
        	 
        	  <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Currency"))%></label>
        				<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        	 </div>
        	 </div>				
        	</div>
        	
        	<div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText10)%></label>
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1" style="min-width:50px;">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText11)%></label>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText12)%></label>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText14)%></label>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText16)%></label>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"  style="min-width:100px;">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText18)%></label>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group">
        				<label><%=smartHF.getLit(generatedText19)%></label>
        				
        		</div>
        		</div>
        	   </div>	
        	   
        	   <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate01Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt01, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt01Disp)%>
	
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts01).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod01).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption01)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>	
        	   
        	           	   <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate02Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt02, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt02Disp)%>
	
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts02).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod02).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption02)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>	
        	   
        	    <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate03Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt03, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt03Disp)%>
	
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts03).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod03).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption03)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	   
        	           	    <div class="row">
             <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate04Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt04, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt04Disp)%>
	
        				
        		</div>
        		</div>
        		
        		<div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts04).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod04).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption04)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	
        	
        	        	           	    <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate05Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt05, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt05Disp)%>
	
        				
        		</div>
        		</div>
        		
        		<div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts05).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod05).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption05)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	   
        	  <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate06Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt06, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt06Disp)%>
	
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts06).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod06).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption06)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid06,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	   
        	   <div class="row">
              <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate07Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt07, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt07Disp)%>
	
        				
        		</div>
        		</div>
        		
        		<div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts07).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod07).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption07)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid07,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	   
        	    <div class="row">
             <div class="col-md-1" style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrdate08Disp)%>
	                    
        				
        		</div>
        		</div>		
        		
        		 <div class="col-md-1">
               <div class="form-group" style="min-width:60px;">
        				<%=smartHF.getHTMLVar(fw, sv.prcnt08, COBOLHTMLFormatter.S3VS2)%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.zrpaydt08Disp)%>
	
        				
        		</div>
        		</div>
        		
        		<div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.paysts08).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1"style="min-width:130px;">
                <div class="form-group">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.paymentMethod08).replace("width:8px;","width:12px;")%>
        				
        		</div>
        		</div>
        		
        		 <div class="col-md-1">
                <div class="form-group" style="min-width:90px;">
        				<%=smartHF.getHTMLSpaceVar(fw, sv.payOption08)%>
        				
        		</div>
        		</div>
        		
        		
        		 <div class="col-md-1" style="min-width:210px; padding-left: 36px;">
                <div class="form-group">
        				
        				<%	
							qpsf = fw.getFieldXMLDef((sv.paid08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid08,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
        		</div>
        		</div>
        	   </div>
        	   
        	   <div class="row">
        	   <div class="col-md-8"></div>
				<div class="col-md-1">
					<div class="form-group" style="min-width:90px;">
						<label><%=smartHF.getLit(generatedText24)%></label>
					</div>
				</div>
				
				<div class="col-md-1" style="min-width:210px;">
					<div class="form-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.paid09).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.paid09,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.paid09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>			
        	
        	</div>
        	</div>
        	
        	
        	
        	
        	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<%--
	<div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.chdrcoy).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrcoy.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrcoy.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrcoy.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
<style>
@media \0screen\,screen\9
{
.output_cell{margin-right:2px}
}
</style>
<td width='251'>
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Product")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cntdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>
</table>
<table>
<tr style='height:42px;'><td width='251'>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
<td width='251'>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Cover")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue =  formatValue(longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	


<td width='251'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rider")%>
</div>
<%}%>

<br/>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue =  formatValue(longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue =  longValue;
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
</tr>
<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payee")%>
</div>
<%}%>



<br/>
<%if ((new Byte((sv.payeesel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.payeesel.getFormData();  
%>

<% 
	if((new Byte((sv.payeesel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell1" : "output_cell1" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='payeesel' 
type='text' 
value='<%=sv.payeesel.getFormData()%>' 
maxLength='<%=sv.payeesel.getLength()%>' 
size='<%=sv.payeesel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(payeesel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.payeesel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.payeesel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('payeesel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.payeesel).getColor()== null  ? 
"input_cell" :  (sv.payeesel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('payeesel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}longValue = null;}} %>






<%if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<!-- ILIFE-2185 top element Added -->
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell1" %>' style="padding-bottom:5px;top:1px;margin-top:1px;"> 
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
<td width='251'>
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Option")%>
</div>
<%}%>



<br/>
<%	
	if ((new Byte((sv.payOption09).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payOption09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payOption09");
	optionValue = makeDropDownList( mappedItems , sv.payOption09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payOption09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payOption09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payOption09' type='list' style="width:140px; font-family: Arial, Verdana,  Helvetica, sans-serif"
<% 
	if((new Byte((sv.payOption09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payOption09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payOption09).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
</td>

<td width='251'>
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Meth")%>
</div>
<%}%>



<br/>
<%	
	if ((new Byte((sv.payMethod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"payMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("payMethod");
	optionValue = makeDropDownList( mappedItems , sv.payMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.payMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.payMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.payMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='payMethod' type='list' style="width:140px;font-family: Arial, Verdana,  Helvetica, sans-serif"
<% 
	if((new Byte((sv.payMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.payMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.payMethod).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
</td></tr>

<tr style='height:42px;'><td width='251'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Next Payment")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.zrdate09Disp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.zrdate09Disp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zrdate09Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zrdate09Disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
<td width='251'>
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("paycurr");
		longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.paycurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paycurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.paycurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
</tr>


</table></div> --%>
	


	<%-- <%=smartHF.getLit(11, 3, generatedText10)%>

	<%=smartHF.getLit(11, 16, generatedText11)%>

	<%=smartHF.getLit(11, 25, generatedText12)%>

	<%=smartHF.getLit(11, 38, generatedText14)%>

	<%=smartHF.getLit(11, 50, generatedText16)%>

	<%=smartHF.getLit(11, 62, generatedText18)%>

	<%=smartHF.getLit(11, 73.5, generatedText19)%>

	

	

	<%=smartHF.getHTMLSpaceVar(12, 3, fw, sv.zrdate01Disp)%>
	<%=smartHF.getHTMLCalNSVar(12, 3, fw, sv.zrdate01Disp)%>

	<%=smartHF.getHTMLVar(12, 16, fw, sv.prcnt01, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(12, 25, fw, sv.zrpaydt01Disp)%>
	<%=smartHF.getHTMLCalNSVar(12, 25, fw, sv.zrpaydt01Disp)%>
	<!-- ILIFE-946 START -- Screen Sr532 UI to be modified -->
	<%=smartHF.getHTMLVar(12, 38, fw, sv.paysts01).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(12, 50, fw, sv.paymentMethod01).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(12, 50, fw, sv.paymentMethod01)%>

	<%=smartHF.getHTMLSpaceVar(12, 62, fw, sv.payOption01)%>
	<%=smartHF.getHTMLF4NSVar(12, 56, fw, sv.payOption01)%>

	<%=smartHF.getHTMLVar(12, 71, fw, sv.paid01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(13, 3, fw, sv.zrdate02Disp)%>
	<%=smartHF.getHTMLCalNSVar(13, 3, fw, sv.zrdate02Disp)%>

	<%=smartHF.getHTMLVar(13, 16, fw, sv.prcnt02, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(13, 25, fw, sv.zrpaydt02Disp)%>
	<%=smartHF.getHTMLCalNSVar(13, 25, fw, sv.zrpaydt02Disp)%>

	<%=smartHF.getHTMLVar(13, 38, fw, sv.paysts02).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(13, 50, fw, sv.paymentMethod02).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(13, 50, fw, sv.paymentMethod02)%>

	<%=smartHF.getHTMLSpaceVar(13, 62, fw, sv.payOption02)%>
	<%=smartHF.getHTMLF4NSVar(13, 56, fw, sv.payOption02)%>

	<%=smartHF.getHTMLVar(13, 71, fw, sv.paid02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(14, 3, fw, sv.zrdate03Disp)%>
	<%=smartHF.getHTMLCalNSVar(14, 3, fw, sv.zrdate03Disp)%>

	<%=smartHF.getHTMLVar(14, 16, fw, sv.prcnt03, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(14, 25, fw, sv.zrpaydt03Disp)%>
	<%=smartHF.getHTMLCalNSVar(14, 25, fw, sv.zrpaydt03Disp)%>

	<%=smartHF.getHTMLVar(14, 38, fw, sv.paysts03).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(14, 50, fw, sv.paymentMethod03).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(14, 50, fw, sv.paymentMethod03)%>

	<%=smartHF.getHTMLSpaceVar(14, 62, fw, sv.payOption03)%>
	<%=smartHF.getHTMLF4NSVar(14, 56, fw, sv.payOption03)%>

	<%=smartHF.getHTMLVar(14, 71, fw, sv.paid03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(15, 3, fw, sv.zrdate04Disp)%>
	<%=smartHF.getHTMLCalNSVar(15, 3, fw, sv.zrdate04Disp)%>

	<%=smartHF.getHTMLVar(15, 16, fw, sv.prcnt04, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(15, 25, fw, sv.zrpaydt04Disp)%>
	<%=smartHF.getHTMLCalNSVar(15, 25, fw, sv.zrpaydt04Disp)%>

	<%=smartHF.getHTMLVar(15, 38, fw, sv.paysts04).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(15, 50, fw, sv.paymentMethod04).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(15, 50, fw, sv.paymentMethod04)%>

	<%=smartHF.getHTMLSpaceVar(15, 62, fw, sv.payOption04)%>
	<%=smartHF.getHTMLF4NSVar(15, 56, fw, sv.payOption04)%>

	<%=smartHF.getHTMLVar(15, 71, fw, sv.paid04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(16, 3, fw, sv.zrdate05Disp)%>
	<%=smartHF.getHTMLCalNSVar(16, 3, fw, sv.zrdate05Disp)%>

	<%=smartHF.getHTMLVar(16, 16, fw, sv.prcnt05, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(16, 25, fw, sv.zrpaydt05Disp)%>
	<%=smartHF.getHTMLCalNSVar(16, 25, fw, sv.zrpaydt05Disp)%>

	<%=smartHF.getHTMLVar(16, 38, fw, sv.paysts05).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(16, 50, fw, sv.paymentMethod05).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(16, 50, fw, sv.paymentMethod05)%>

	<%=smartHF.getHTMLSpaceVar(16, 62, fw, sv.payOption05)%>
	<%=smartHF.getHTMLF4NSVar(16, 56, fw, sv.payOption05)%>

	<%=smartHF.getHTMLVar(16, 71, fw, sv.paid05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(17, 3, fw, sv.zrdate06Disp)%>
	<%=smartHF.getHTMLCalNSVar(17, 3, fw, sv.zrdate06Disp)%>

	<%=smartHF.getHTMLVar(17, 16, fw, sv.prcnt06, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(17, 25, fw, sv.zrpaydt06Disp)%>
	<%=smartHF.getHTMLCalNSVar(17, 25, fw, sv.zrpaydt06Disp)%>

	<%=smartHF.getHTMLVar(17, 38, fw, sv.paysts06).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(17, 50, fw, sv.paymentMethod06).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(17, 50, fw, sv.paymentMethod06)%>

	<%=smartHF.getHTMLSpaceVar(17, 62, fw, sv.payOption06)%>
	<%=smartHF.getHTMLF4NSVar(17, 56, fw, sv.payOption06)%>

	<%=smartHF.getHTMLVar(17, 71, fw, sv.paid06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(18, 3, fw, sv.zrdate07Disp)%>
	<%=smartHF.getHTMLCalNSVar(18, 3, fw, sv.zrdate07Disp)%>

	<%=smartHF.getHTMLVar(18, 16, fw, sv.prcnt07, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(18, 25, fw, sv.zrpaydt07Disp)%>
	<%=smartHF.getHTMLCalNSVar(18, 25, fw, sv.zrpaydt07Disp)%>

	<%=smartHF.getHTMLVar(18, 38, fw, sv.paysts07).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(18, 50, fw, sv.paymentMethod07).replace("width:8px;","width:12px;")%>
	<%=smartHF.getHTMLF4NSVar(18, 50, fw, sv.paymentMethod07)%>

	<%=smartHF.getHTMLSpaceVar(18, 62, fw, sv.payOption07)%>
	<%=smartHF.getHTMLF4NSVar(18, 56, fw, sv.payOption07)%>

	<%=smartHF.getHTMLVar(18, 71, fw, sv.paid07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getHTMLSpaceVar(19, 3, fw, sv.zrdate08Disp)%>
	<%=smartHF.getHTMLCalNSVar(19, 3, fw, sv.zrdate08Disp)%>

	<%=smartHF.getHTMLVar(19, 16, fw, sv.prcnt08, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLSpaceVar(19, 25, fw, sv.zrpaydt08Disp)%>
	<%=smartHF.getHTMLCalNSVar(19, 25, fw, sv.zrpaydt08Disp)%>

	<%=smartHF.getHTMLVar(19, 38, fw, sv.paysts08).replace("width:8px;","width:12px;")%>

	<%=smartHF.getHTMLSpaceVar(19, 50, fw, sv.paymentMethod08).replace("width:8px;","width:12px;")%>
	<!-- ILIFE-946 END -- Screen Sr532 UI to be modified -->
	<%=smartHF.getHTMLF4NSVar(19, 50, fw, sv.paymentMethod08)%>

	<%=smartHF.getHTMLSpaceVar(19, 62, fw, sv.payOption08)%>
	<%=smartHF.getHTMLF4NSVar(19, 56, fw, sv.payOption08)%>

	<%=smartHF.getHTMLVar(19, 71, fw, sv.paid08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>

	<%=smartHF.getLit(20.3, 67, generatedText24)%>

	<%=smartHF.getHTMLVar(20, 71, fw, sv.paid09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("width:184px;","width:130px;")%>
 --%>
	
<div style="position:absolute;
	top:370px;
	left:22px;">


<Div id='mainForm_OPTS' style='visibility:hidden;'>
<%if (sv.actionflag.compareTo("N") != 0) {%><%=smartHF.getMenuLink(sv.optind, resourceBundleHandler.gettingValueFromBundle("Bank Details"))%><%}%>			
<%if (sv.actionflag.compareTo("N") == 0) {%> <%=smartHF.getMenuLink(sv.optind, resourceBundleHandler.gettingValueFromBundle("Bank Dets"))%>  <%}%> 

</div>	

<%-- 

<Div id='mainForm_OPTS' style='visibility:hidden'>

<%

if(sv.optind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind
.getEnabled()==BaseScreenData.DISABLED){
%>

<%=sv.optdsc
.getFormData()%>
<%
}
else{

%>

<div id='optdsc'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind"))' 
class="hyperLink">

<%= sv.optdsc
.getFormData() %>
</a>
</div>	
<%} %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
if (sv.optind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">

<%}
	if (sv.optind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.optind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
<%							            
}
%>
<div>
<input name='optind' id='optind' type='hidden'  value="<%=sv.optind
.getFormData()%>">
</div>
						



	
</div> --%>

</div>
<%}%>

<%if (sv.Sr532protectWritten.gt(0)) {%>
	<%Sr532protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<!-- ILIFE-2185 started by pmujavadiya -->
<style>
.iconPos{bottom:1px;}
div[id*='zrdate']{padding-right:2px !important} 
div[id*='prcnt']{padding-right:2px !important}
div[id*='zrpaydt']{padding-right:2px !important}
 div[id*='paid']{margin-left:20px !important} 
 div[id*='payOption']{padding-right:2px !important}
 @media \0screen\,screen\9
{
.iconPos{margin-bottom:1px}
}
  </style>  
<!-- ILIFE-2185 ends -->
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>


<script>
<% 	if (appVars.ind07.isOn()) { %> 
var payOpt = $("select[name='payOption09']").val();

populatePayMethod(payOpt);

 function populatePayMethod(payOption)
{

	  var check = true;
		<% for ( String key : sv.mapLoccde.keySet() ) { %>

		   if((payOption  == '<%=key%>') && (check == true))
		   {
				 $("select[name='payMethod']").val('<%=sv.mapLoccde.get(key)%>');
				 $("#hiddenPayMethod").val( $("select[name='payMethod']").val());
			   check = false;
			}
		<%}%>
	


}
 

 <%} %>
</script>


<%@ include file="/POLACommon2NEW.jsp"%>