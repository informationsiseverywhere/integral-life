

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR533";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*"%>
<%
	Sr533ScreenVars sv = (Sr533ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                                   ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bank Code       ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                   ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Account         ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "   ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                                   ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			
					<%
						if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%>
					</label>
					<%
						}
					%>
					<table>
						<tr class="input-group three-controller">
							<td>	<div class="form-group">
								<div class="input-group" style="width: 150px;">
									<%
										if ((new Byte((sv.bankkey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										longValue = sv.bankkey.getFormData();
									%>

									<%
										if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div>

									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<input name='bankkey' id='bankkey' type='text'
										value='<%=sv.bankkey.getFormData()%>'
										maxLength='<%=sv.bankkey.getLength()%>'
										size='<%=sv.bankkey.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(bankkey)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.bankkey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
										} else {
									%>

									class = '
									<%=(sv.bankkey).getColor() == null ? "input_cell"
								: (sv.bankkey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									> <span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
									<%
										}
												longValue = null;
											}
										}
									%>

								</div>
							</td>
							<td style="padding-left:14px">
								<%
									if ((new Byte((sv.bankdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
								<td style="padding-left:5px">
								<%
									if ((new Byte((sv.branchdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.branchdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.branchdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			
					<%
						if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Account")%>
					</label>
					<%
						}
					%>
					<table>
						<tr class="input-group three-controller">
							<td>	<div class="form-group">
								<div class="input-group" style="width: 150px;">
									<%
										if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>

									<%
										longValue = sv.bankacckey.getFormData();
									%>

									<%
										if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div>

									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<input name='bankacckey' id='bankacckey' type='text'
										value='<%=sv.bankacckey.getFormData()%>'
										maxLength='<%=sv.bankacckey.getLength()%>'
										size='<%=sv.bankacckey.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(bankkey)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.bankacckey).getHighLight()))
														.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
										} else {
									%>

									class = '
									<%=(sv.bankacckey).getColor() == null ? "input_cell"
								: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									> <span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
										}
												longValue = null;
											}
										}
									%>

								</div></div>
							</td>
							<td style="padding-left:14px">
								<%
									if ((new Byte((sv.bankaccdsc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText0).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
</div>