

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR536";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*"%>
<%
	Sr536ScreenVars sv = (Sr536ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Schedule Name         ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Accounting Month      ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Number       ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Year       ");
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date        ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Company               ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Job Queue             ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Branch                ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Lead Times (Months)");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cash Flow");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payments Advice");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.zrcshlead.setReverse(BaseScreenData.REVERSED);
			sv.zrcshlead.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.zrcshlead.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.zradvlead.setReverse(BaseScreenData.REVERSED);
			sv.zradvlead.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.zradvlead.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
					<table><tr>
					<td>
						<%
							if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.scheduleName.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.scheduleName.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div id="scheduleName"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>
						<%
							qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.scheduleNumber);

							if (!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"  style="margin-left: 1px;min-width: 100px;"></div>

						<%
							}

							longValue = null;
							formatValue = null;
						%>
					</td></tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Accounting Month")%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.acctmonth);

								if (!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 50px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
						</td>
						<td>
						<%
							if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.acctyear);

								if (!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="margin-left: 1px;min-width: 50px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</td></tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jobq.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jobq.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bcompany" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bcompany");
						longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div style="width: 120px;">
						<div style="position: relative; margin-left: 1px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=longValue%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bbranch" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bbranch");
						longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<div style="width: 120px;">
						<div style="position: relative; margin-left: 1px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>
							<%=longValue%>
							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Lead Times (Months)")%>
					</label>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Cash Flow")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.zrcshlead).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.zrcshlead).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='zrcshlead' type='text'
							<%if ((sv.zrcshlead).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zrcshlead)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zrcshlead);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zrcshlead)%>' <%}%>
							size='<%=sv.zrcshlead.getLength()%>'
							maxLength='<%=sv.zrcshlead.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(zrcshlead)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zrcshlead).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zrcshlead).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zrcshlead).getColor() == null ? "input_cell"
							: (sv.zrcshlead).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Payments Advice")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.zradvlead).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.zradvlead).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='zradvlead' type='text'
							<%if ((sv.zradvlead).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zradvlead)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zradvlead);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zradvlead)%>' <%}%>
							size='<%=sv.zradvlead.getLength()%>'
							maxLength='<%=sv.zradvlead.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(zradvlead)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zradvlead).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zradvlead).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zradvlead).getColor() == null ? "input_cell"
							: (sv.zradvlead).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Number")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<%
					if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Year")%>
				</label>
				<%
					}
				%>
			</div>
		</div>
	</div>
</div>