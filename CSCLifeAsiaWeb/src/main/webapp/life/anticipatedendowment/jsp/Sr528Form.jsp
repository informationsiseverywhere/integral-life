

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR528";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*" %>
<%Sr528ScreenVars sv = (Sr528ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Details required ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Payment Method   ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Defaulted Selection   ");%>
	
<%{
		if (appVars.ind01.isOn()) {
			sv.bankaccreq.setReverse(BaseScreenData.REVERSED);
			sv.bankaccreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankaccreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.paymentMethod.setReverse(BaseScreenData.REVERSED);
			sv.paymentMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.paymentMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.defaultSel.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>



<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		
					    		<div class="input-group" style="max-width:100px;">  
					    			
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

  
	
				    		</div>
					</div></div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							
							<div class="input-group" style="max-width:100px;">  
								<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						</div>
				   </div>		</div>
			
			    	<div class="col-md-4" style="max-width:600px;">
						<div class="form-group">	
						
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						  
							 <div class="input-group"  style="max-width:100px;">
							<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:700px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
						</div>
						</div>
				   </div>	
		    </div>
				   
				 
				  <table>




<tr style='height:22px;'>

<%
ScreenDataHelper paymentRequired = new ScreenDataHelper(fw.getVariables(), sv.bankaccreq);
%>
<td style="visibility:<%=paymentRequired.visibility()%>" width='200' ; >
<label style="font-size:15px">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Details required")%>
</label>

<%if (paymentRequired.isReadonly()) {%>
<div style="max-width:70px">
<input name='bankaccreq' type='text'  value='<%=paymentRequired.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)'
       onHelp='return fieldHelp(bankaccreq)'
       onKeyUp='return checkMaxLength(this)'>
</div> <%} else {%>
<div style="max-width:70px">
<select name="bankaccreq" type="list" class="<%=paymentRequired.clazz()%>">
  <option <%=paymentRequired.selected("Y")%> value="Y">Y</option>
  <option <%=paymentRequired.selected("N")%> value="N">N</option>
</select></div>
<%}%>
</td>
<td width='95'>
<td width='251'>
<label style="font-size:15px">
<%=resourceBundleHandler.gettingValueFromBundle("Default Payment Method")%>
</label>

<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paymentMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paymentMethod");
	optionValue = makeDropDownList( mappedItems , sv.paymentMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.paymentMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paymentMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.paymentMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:202px;"> 
<%
} 
%>

<select name='paymentMethod' type='list' style="width:200px;"
<% 
	if((new Byte((sv.paymentMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.paymentMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.paymentMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>
<td width='40'>
<%
ScreenDataHelper defaultSelection = new ScreenDataHelper(fw.getVariables(), sv.defaultSel);
%>
<td style="visibility:<%=defaultSelection.visibility()%>" width='200' ; >
<label style="font-size:15px">
<%=resourceBundleHandler.gettingValueFromBundle("Defaulted Selection")%>
</label>

<%if (defaultSelection.isReadonly()) {%>
<div style="max-width:70px">
<input name='defaultSel' type='text'  value='<%=defaultSelection.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)'
       onHelp='return fieldHelp(defaultSel)'
       onKeyUp='return checkMaxLength(this)'>
</div> <%} else {%>
<div style="max-width:70px">
<select name="defaultSel" type="list" class="<%=defaultSelection.clazz()%>">
  <option <%=defaultSelection.selected("S")%> value="---Select---">---Select---</option>
  <option <%=defaultSelection.selected("Y")%> value="Y">Y</option>
  <option <%=defaultSelection.selected("N")%> value="N">N</option>
</select></div>
<%}%>
</td>

</tr></table>

</div>
				      </div>
			        
				   
				   

<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>
<style>
		
		div[id*='bankaccreq']{padding-right:1px !important} 
			
	</style> 
