<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR530";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*"%>
<%
	Sr530ScreenVars sv = (Sr530ScreenVars) fw.getVariables();
%>

<%
	if (sv.Sr530screenWritten.gt(0)) {
%>
<%
	Sr530screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Dates effective     ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	sv.itmtoDisp.appendClassString("string_fld");
		sv.itmtoDisp.appendClassString("output_txt");
		sv.itmtoDisp.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Partial Payment Payable at Policy Anniversary for child aged");
%>
<%
	generatedText12.appendClassString("label_txt");
		generatedText12.appendClassString("information_txt");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Age at");
%>
<%
	generatedText13.appendClassString(resourceBundleHandler.gettingValueFromBundle("label_txt"));
		generatedText13.appendClassString(resourceBundleHandler.gettingValueFromBundle("information_txt"));
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "entry");
%>
<%
	generatedText14.appendClassString(resourceBundleHandler.gettingValueFromBundle("label_txt"));
		generatedText14.appendClassString(resourceBundleHandler.gettingValueFromBundle("information_txt"));
%>
<%
	sv.zrage01.setClassString("");
%>
<%
	sv.zrage01.appendClassString("num_fld");
		sv.zrage01.appendClassString("input_txt");
		sv.zrage01.appendClassString("highlight");
%>
<%
	sv.zrage02.setClassString("");
%>
<%
	sv.zrage02.appendClassString("num_fld");
		sv.zrage02.appendClassString("input_txt");
		sv.zrage02.appendClassString("highlight");
%>
<%
	sv.zrage03.setClassString("");
%>
<%
	sv.zrage03.appendClassString("num_fld");
		sv.zrage03.appendClassString("input_txt");
		sv.zrage03.appendClassString("highlight");
%>
<%
	sv.zrage04.setClassString("");
%>
<%
	sv.zrage04.appendClassString("num_fld");
		sv.zrage04.appendClassString("input_txt");
		sv.zrage04.appendClassString("highlight");
%>
<%
	sv.zrage05.setClassString("");
%>
<%
	sv.zrage05.appendClassString("num_fld");
		sv.zrage05.appendClassString("input_txt");
		sv.zrage05.appendClassString("highlight");
%>
<%
	sv.zrage06.setClassString("");
%>
<%
	sv.zrage06.appendClassString("num_fld");
		sv.zrage06.appendClassString("input_txt");
		sv.zrage06.appendClassString("highlight");
%>
<%
	sv.zrage07.setClassString("");
%>
<%
	sv.zrage07.appendClassString("num_fld");
		sv.zrage07.appendClassString("input_txt");
		sv.zrage07.appendClassString("highlight");
%>
<%
	sv.zrage08.setClassString("");
%>
<%
	sv.zrage08.appendClassString("num_fld");
		sv.zrage08.appendClassString("input_txt");
		sv.zrage08.appendClassString("highlight");
%>
<%
	sv.zragfr01.setClassString("");
%>
<%
	sv.zragfr01.appendClassString("num_fld");
		sv.zragfr01.appendClassString("input_txt");
		sv.zragfr01.appendClassString("highlight");
%>
<%
	sv.zragto01.setClassString("");
%>
<%
	sv.zragto01.appendClassString("num_fld");
		sv.zragto01.appendClassString("input_txt");
		sv.zragto01.appendClassString("highlight");
%>
<%
	sv.zrpcpd01.setClassString("");
%>
<%
	sv.zrpcpd01.appendClassString("num_fld");
		sv.zrpcpd01.appendClassString("input_txt");
		sv.zrpcpd01.appendClassString("highlight");
%>
<%
	sv.zrpcpd02.setClassString("");
%>
<%
	sv.zrpcpd02.appendClassString("num_fld");
		sv.zrpcpd02.appendClassString("input_txt");
		sv.zrpcpd02.appendClassString("highlight");
%>
<%
	sv.zrpcpd03.setClassString("");
%>
<%
	sv.zrpcpd03.appendClassString("num_fld");
		sv.zrpcpd03.appendClassString("input_txt");
		sv.zrpcpd03.appendClassString("highlight");
%>
<%
	sv.zrpcpd04.setClassString("");
%>
<%
	sv.zrpcpd04.appendClassString("num_fld");
		sv.zrpcpd04.appendClassString("input_txt");
		sv.zrpcpd04.appendClassString("highlight");
%>
<%
	sv.zrpcpd05.setClassString("");
%>
<%
	sv.zrpcpd05.appendClassString("num_fld");
		sv.zrpcpd05.appendClassString("input_txt");
		sv.zrpcpd05.appendClassString("highlight");
%>
<%
	sv.zrpcpd06.setClassString("");
%>
<%
	sv.zrpcpd06.appendClassString("num_fld");
		sv.zrpcpd06.appendClassString("input_txt");
		sv.zrpcpd06.appendClassString("highlight");
%>
<%
	sv.zrpcpd07.setClassString("");
%>
<%
	sv.zrpcpd07.appendClassString("num_fld");
		sv.zrpcpd07.appendClassString("input_txt");
		sv.zrpcpd07.appendClassString("highlight");
%>
<%
	sv.zrpcpd08.setClassString("");
%>
<%
	sv.zrpcpd08.appendClassString("num_fld");
		sv.zrpcpd08.appendClassString("input_txt");
		sv.zrpcpd08.appendClassString("highlight");
%>
<%
	sv.zragfr02.setClassString("");
%>
<%
	sv.zragfr02.appendClassString("num_fld");
		sv.zragfr02.appendClassString("input_txt");
		sv.zragfr02.appendClassString("highlight");
%>
<%
	sv.zragto02.setClassString("");
%>
<%
	sv.zragto02.appendClassString("num_fld");
		sv.zragto02.appendClassString("input_txt");
		sv.zragto02.appendClassString("highlight");
%>
<%
	sv.zrpcpd09.setClassString("");
%>
<%
	sv.zrpcpd09.appendClassString("num_fld");
		sv.zrpcpd09.appendClassString("input_txt");
		sv.zrpcpd09.appendClassString("highlight");
%>
<%
	sv.zrpcpd10.setClassString("");
%>
<%
	sv.zrpcpd10.appendClassString("num_fld");
		sv.zrpcpd10.appendClassString("input_txt");
		sv.zrpcpd10.appendClassString("highlight");
%>
<%
	sv.zrpcpd11.setClassString("");
%>
<%
	sv.zrpcpd11.appendClassString("num_fld");
		sv.zrpcpd11.appendClassString("input_txt");
		sv.zrpcpd11.appendClassString("highlight");
%>
<%
	sv.zrpcpd12.setClassString("");
%>
<%
	sv.zrpcpd12.appendClassString("num_fld");
		sv.zrpcpd12.appendClassString("input_txt");
		sv.zrpcpd12.appendClassString("highlight");
%>
<%
	sv.zrpcpd13.setClassString("");
%>
<%
	sv.zrpcpd13.appendClassString("num_fld");
		sv.zrpcpd13.appendClassString("input_txt");
		sv.zrpcpd13.appendClassString("highlight");
%>
<%
	sv.zrpcpd14.setClassString("");
%>
<%
	sv.zrpcpd14.appendClassString("num_fld");
		sv.zrpcpd14.appendClassString("input_txt");
		sv.zrpcpd14.appendClassString("highlight");
%>
<%
	sv.zrpcpd15.setClassString("");
%>
<%
	sv.zrpcpd15.appendClassString("num_fld");
		sv.zrpcpd15.appendClassString("input_txt");
		sv.zrpcpd15.appendClassString("highlight");
%>
<%
	sv.zrpcpd16.setClassString("");
%>
<%
	sv.zrpcpd16.appendClassString("num_fld");
		sv.zrpcpd16.appendClassString("input_txt");
		sv.zrpcpd16.appendClassString("highlight");
%>
<%
	sv.zragfr03.setClassString("");
%>
<%
	sv.zragfr03.appendClassString("num_fld");
		sv.zragfr03.appendClassString("input_txt");
		sv.zragfr03.appendClassString("highlight");
%>
<%
	sv.zragto03.setClassString("");
%>
<%
	sv.zragto03.appendClassString("num_fld");
		sv.zragto03.appendClassString("input_txt");
		sv.zragto03.appendClassString("highlight");
%>
<%
	sv.zrpcpd17.setClassString("");
%>
<%
	sv.zrpcpd17.appendClassString("num_fld");
		sv.zrpcpd17.appendClassString("input_txt");
		sv.zrpcpd17.appendClassString("highlight");
%>
<%
	sv.zrpcpd18.setClassString("");
%>
<%
	sv.zrpcpd18.appendClassString("num_fld");
		sv.zrpcpd18.appendClassString("input_txt");
		sv.zrpcpd18.appendClassString("highlight");
%>
<%
	sv.zrpcpd19.setClassString("");
%>
<%
	sv.zrpcpd19.appendClassString("num_fld");
		sv.zrpcpd19.appendClassString("input_txt");
		sv.zrpcpd19.appendClassString("highlight");
%>
<%
	sv.zrpcpd20.setClassString("");
%>
<%
	sv.zrpcpd20.appendClassString("num_fld");
		sv.zrpcpd20.appendClassString("input_txt");
		sv.zrpcpd20.appendClassString("highlight");
%>
<%
	sv.zrpcpd21.setClassString("");
%>
<%
	sv.zrpcpd21.appendClassString("num_fld");
		sv.zrpcpd21.appendClassString("input_txt");
		sv.zrpcpd21.appendClassString("highlight");
%>
<%
	sv.zrpcpd22.setClassString("");
%>
<%
	sv.zrpcpd22.appendClassString("num_fld");
		sv.zrpcpd22.appendClassString("input_txt");
		sv.zrpcpd22.appendClassString("highlight");
%>
<%
	sv.zrpcpd23.setClassString("");
%>
<%
	sv.zrpcpd23.appendClassString("num_fld");
		sv.zrpcpd23.appendClassString("input_txt");
		sv.zrpcpd23.appendClassString("highlight");
%>
<%
	sv.zrpcpd24.setClassString("");
%>
<%
	sv.zrpcpd24.appendClassString("num_fld");
		sv.zrpcpd24.appendClassString("input_txt");
		sv.zrpcpd24.appendClassString("highlight");
%>
<%
	sv.zragfr04.setClassString("");
%>
<%
	sv.zragfr04.appendClassString("num_fld");
		sv.zragfr04.appendClassString("input_txt");
		sv.zragfr04.appendClassString("highlight");
%>
<%
	sv.zragto04.setClassString("");
%>
<%
	sv.zragto04.appendClassString("num_fld");
		sv.zragto04.appendClassString("input_txt");
		sv.zragto04.appendClassString("highlight");
%>
<%
	sv.zrpcpd25.setClassString("");
%>
<%
	sv.zrpcpd25.appendClassString("num_fld");
		sv.zrpcpd25.appendClassString("input_txt");
		sv.zrpcpd25.appendClassString("highlight");
%>
<%
	sv.zrpcpd26.setClassString("");
%>
<%
	sv.zrpcpd26.appendClassString("num_fld");
		sv.zrpcpd26.appendClassString("input_txt");
		sv.zrpcpd26.appendClassString("highlight");
%>
<%
	sv.zrpcpd27.setClassString("");
%>
<%
	sv.zrpcpd27.appendClassString("num_fld");
		sv.zrpcpd27.appendClassString("input_txt");
		sv.zrpcpd27.appendClassString("highlight");
%>
<%
	sv.zrpcpd28.setClassString("");
%>
<%
	sv.zrpcpd28.appendClassString("num_fld");
		sv.zrpcpd28.appendClassString("input_txt");
		sv.zrpcpd28.appendClassString("highlight");
%>
<%
	sv.zrpcpd29.setClassString("");
%>
<%
	sv.zrpcpd29.appendClassString("num_fld");
		sv.zrpcpd29.appendClassString("input_txt");
		sv.zrpcpd29.appendClassString("highlight");
%>
<%
	sv.zrpcpd30.setClassString("");
%>
<%
	sv.zrpcpd30.appendClassString("num_fld");
		sv.zrpcpd30.appendClassString("input_txt");
		sv.zrpcpd30.appendClassString("highlight");
%>
<%
	sv.zrpcpd31.setClassString("");
%>
<%
	sv.zrpcpd31.appendClassString("num_fld");
		sv.zrpcpd31.appendClassString("input_txt");
		sv.zrpcpd31.appendClassString("highlight");
%>
<%
	sv.zrpcpd32.setClassString("");
%>
<%
	sv.zrpcpd32.appendClassString("num_fld");
		sv.zrpcpd32.appendClassString("input_txt");
		sv.zrpcpd32.appendClassString("highlight");
%>
<%
	sv.zragfr05.setClassString("");
%>
<%
	sv.zragfr05.appendClassString("num_fld");
		sv.zragfr05.appendClassString("input_txt");
		sv.zragfr05.appendClassString("highlight");
%>
<%
	sv.zragto05.setClassString("");
%>
<%
	sv.zragto05.appendClassString("num_fld");
		sv.zragto05.appendClassString("input_txt");
		sv.zragto05.appendClassString("highlight");
%>
<%
	sv.zrpcpd33.setClassString("");
%>
<%
	sv.zrpcpd33.appendClassString("num_fld");
		sv.zrpcpd33.appendClassString("input_txt");
		sv.zrpcpd33.appendClassString("highlight");
%>
<%
	sv.zrpcpd34.setClassString("");
%>
<%
	sv.zrpcpd34.appendClassString("num_fld");
		sv.zrpcpd34.appendClassString("input_txt");
		sv.zrpcpd34.appendClassString("highlight");
%>
<%
	sv.zrpcpd35.setClassString("");
%>
<%
	sv.zrpcpd35.appendClassString("num_fld");
		sv.zrpcpd35.appendClassString("input_txt");
		sv.zrpcpd35.appendClassString("highlight");
%>
<%
	sv.zrpcpd36.setClassString("");
%>
<%
	sv.zrpcpd36.appendClassString("num_fld");
		sv.zrpcpd36.appendClassString("input_txt");
		sv.zrpcpd36.appendClassString("highlight");
%>
<%
	sv.zrpcpd37.setClassString("");
%>
<%
	sv.zrpcpd37.appendClassString("num_fld");
		sv.zrpcpd37.appendClassString("input_txt");
		sv.zrpcpd37.appendClassString("highlight");
%>
<%
	sv.zrpcpd38.setClassString("");
%>
<%
	sv.zrpcpd38.appendClassString("num_fld");
		sv.zrpcpd38.appendClassString("input_txt");
		sv.zrpcpd38.appendClassString("highlight");
%>
<%
	sv.zrpcpd39.setClassString("");
%>
<%
	sv.zrpcpd39.appendClassString("num_fld");
		sv.zrpcpd39.appendClassString("input_txt");
		sv.zrpcpd39.appendClassString("highlight");
%>
<%
	sv.zrpcpd40.setClassString("");
%>
<%
	sv.zrpcpd40.appendClassString("num_fld");
		sv.zrpcpd40.appendClassString("input_txt");
		sv.zrpcpd40.appendClassString("highlight");
%>
<%
	sv.zragfr06.setClassString("");
%>
<%
	sv.zragfr06.appendClassString("num_fld");
		sv.zragfr06.appendClassString("input_txt");
		sv.zragfr06.appendClassString("highlight");
%>
<%
	sv.zragto06.setClassString("");
%>
<%
	sv.zragto06.appendClassString("num_fld");
		sv.zragto06.appendClassString("input_txt");
		sv.zragto06.appendClassString("highlight");
%>
<%
	sv.zrpcpd41.setClassString("");
%>
<%
	sv.zrpcpd41.appendClassString("num_fld");
		sv.zrpcpd41.appendClassString("input_txt");
		sv.zrpcpd41.appendClassString("highlight");
%>
<%
	sv.zrpcpd42.setClassString("");
%>
<%
	sv.zrpcpd42.appendClassString("num_fld");
		sv.zrpcpd42.appendClassString("input_txt");
		sv.zrpcpd42.appendClassString("highlight");
%>
<%
	sv.zrpcpd43.setClassString("");
%>
<%
	sv.zrpcpd43.appendClassString("num_fld");
		sv.zrpcpd43.appendClassString("input_txt");
		sv.zrpcpd43.appendClassString("highlight");
%>
<%
	sv.zrpcpd44.setClassString("");
%>
<%
	sv.zrpcpd44.appendClassString("num_fld");
		sv.zrpcpd44.appendClassString("input_txt");
		sv.zrpcpd44.appendClassString("highlight");
%>
<%
	sv.zrpcpd45.setClassString("");
%>
<%
	sv.zrpcpd45.appendClassString("num_fld");
		sv.zrpcpd45.appendClassString("input_txt");
		sv.zrpcpd45.appendClassString("highlight");
%>
<%
	sv.zrpcpd46.setClassString("");
%>
<%
	sv.zrpcpd46.appendClassString("num_fld");
		sv.zrpcpd46.appendClassString("input_txt");
		sv.zrpcpd46.appendClassString("highlight");
%>
<%
	sv.zrpcpd47.setClassString("");
%>
<%
	sv.zrpcpd47.appendClassString("num_fld");
		sv.zrpcpd47.appendClassString("input_txt");
		sv.zrpcpd47.appendClassString("highlight");
%>
<%
	sv.zrpcpd48.setClassString("");
%>
<%
	sv.zrpcpd48.appendClassString("num_fld");
		sv.zrpcpd48.appendClassString("input_txt");
		sv.zrpcpd48.appendClassString("highlight");
%>
<%
	sv.zragfr07.setClassString("");
%>
<%
	sv.zragfr07.appendClassString("num_fld");
		sv.zragfr07.appendClassString("input_txt");
		sv.zragfr07.appendClassString("highlight");
%>
<%
	sv.zragto07.setClassString("");
%>
<%
	sv.zragto07.appendClassString("num_fld");
		sv.zragto07.appendClassString("input_txt");
		sv.zragto07.appendClassString("highlight");
%>
<%
	sv.zrpcpd49.setClassString("");
%>
<%
	sv.zrpcpd49.appendClassString("num_fld");
		sv.zrpcpd49.appendClassString("input_txt");
		sv.zrpcpd49.appendClassString("highlight");
%>
<%
	sv.zrpcpd50.setClassString("");
%>
<%
	sv.zrpcpd50.appendClassString("num_fld");
		sv.zrpcpd50.appendClassString("input_txt");
		sv.zrpcpd50.appendClassString("highlight");
%>
<%
	sv.zrpcpd51.setClassString("");
%>
<%
	sv.zrpcpd51.appendClassString("num_fld");
		sv.zrpcpd51.appendClassString("input_txt");
		sv.zrpcpd51.appendClassString("highlight");
%>
<%
	sv.zrpcpd52.setClassString("");
%>
<%
	sv.zrpcpd52.appendClassString("num_fld");
		sv.zrpcpd52.appendClassString("input_txt");
		sv.zrpcpd52.appendClassString("highlight");
%>
<%
	sv.zrpcpd53.setClassString("");
%>
<%
	sv.zrpcpd53.appendClassString("num_fld");
		sv.zrpcpd53.appendClassString("input_txt");
		sv.zrpcpd53.appendClassString("highlight");
%>
<%
	sv.zrpcpd54.setClassString("");
%>
<%
	sv.zrpcpd54.appendClassString("num_fld");
		sv.zrpcpd54.appendClassString("input_txt");
		sv.zrpcpd54.appendClassString("highlight");
%>
<%
	sv.zrpcpd55.setClassString("");
%>
<%
	sv.zrpcpd55.appendClassString("num_fld");
		sv.zrpcpd55.appendClassString("input_txt");
		sv.zrpcpd55.appendClassString("highlight");
%>
<%
	sv.zrpcpd56.setClassString("");
%>
<%
	sv.zrpcpd56.appendClassString("num_fld");
		sv.zrpcpd56.appendClassString("input_txt");
		sv.zrpcpd56.appendClassString("highlight");
%>
<%
	sv.zragfr08.setClassString("");
%>
<%
	sv.zragfr08.appendClassString("num_fld");
		sv.zragfr08.appendClassString("input_txt");
		sv.zragfr08.appendClassString("highlight");
%>
<%
	sv.zragto08.setClassString("");
%>
<%
	sv.zragto08.appendClassString("num_fld");
		sv.zragto08.appendClassString("input_txt");
		sv.zragto08.appendClassString("highlight");
%>
<%
	sv.zrpcpd57.setClassString("");
%>
<%
	sv.zrpcpd57.appendClassString("num_fld");
		sv.zrpcpd57.appendClassString("input_txt");
		sv.zrpcpd57.appendClassString("highlight");
%>
<%
	sv.zrpcpd58.setClassString("");
%>
<%
	sv.zrpcpd58.appendClassString("num_fld");
		sv.zrpcpd58.appendClassString("input_txt");
		sv.zrpcpd58.appendClassString("highlight");
%>
<%
	sv.zrpcpd59.setClassString("");
%>
<%
	sv.zrpcpd59.appendClassString("num_fld");
		sv.zrpcpd59.appendClassString("input_txt");
		sv.zrpcpd59.appendClassString("highlight");
%>
<%
	sv.zrpcpd60.setClassString("");
%>
<%
	sv.zrpcpd60.appendClassString("num_fld");
		sv.zrpcpd60.appendClassString("input_txt");
		sv.zrpcpd60.appendClassString("highlight");
%>
<%
	sv.zrpcpd61.setClassString("");
%>
<%
	sv.zrpcpd61.appendClassString("num_fld");
		sv.zrpcpd61.appendClassString("input_txt");
		sv.zrpcpd61.appendClassString("highlight");
%>
<%
	sv.zrpcpd62.setClassString("");
%>
<%
	sv.zrpcpd62.appendClassString("num_fld");
		sv.zrpcpd62.appendClassString("input_txt");
		sv.zrpcpd62.appendClassString("highlight");
%>
<%
	sv.zrpcpd63.setClassString("");
%>
<%
	sv.zrpcpd63.appendClassString("num_fld");
		sv.zrpcpd63.appendClassString("input_txt");
		sv.zrpcpd63.appendClassString("highlight");
%>
<%
	sv.zrpcpd64.setClassString("");
%>
<%
	sv.zrpcpd64.appendClassString("num_fld");
		sv.zrpcpd64.appendClassString("input_txt");
		sv.zrpcpd64.appendClassString("highlight");
%>
<%
	sv.zragfr09.setClassString("");
%>
<%
	sv.zragfr09.appendClassString("num_fld");
		sv.zragfr09.appendClassString("input_txt");
		sv.zragfr09.appendClassString("highlight");
%>
<%
	sv.zragto09.setClassString("");
%>
<%
	sv.zragto09.appendClassString("num_fld");
		sv.zragto09.appendClassString("input_txt");
		sv.zragto09.appendClassString("highlight");
%>
<%
	sv.zrpcpd65.setClassString("");
%>
<%
	sv.zrpcpd65.appendClassString("num_fld");
		sv.zrpcpd65.appendClassString("input_txt");
		sv.zrpcpd65.appendClassString("highlight");
%>
<%
	sv.zrpcpd66.setClassString("");
%>
<%
	sv.zrpcpd66.appendClassString("num_fld");
		sv.zrpcpd66.appendClassString("input_txt");
		sv.zrpcpd66.appendClassString("highlight");
%>
<%
	sv.zrpcpd67.setClassString("");
%>
<%
	sv.zrpcpd67.appendClassString("num_fld");
		sv.zrpcpd67.appendClassString("input_txt");
		sv.zrpcpd67.appendClassString("highlight");
%>
<%
	sv.zrpcpd68.setClassString("");
%>
<%
	sv.zrpcpd68.appendClassString("num_fld");
		sv.zrpcpd68.appendClassString("input_txt");
		sv.zrpcpd68.appendClassString("highlight");
%>
<%
	sv.zrpcpd69.setClassString("");
%>
<%
	sv.zrpcpd69.appendClassString("num_fld");
		sv.zrpcpd69.appendClassString("input_txt");
		sv.zrpcpd69.appendClassString("highlight");
%>
<%
	sv.zrpcpd70.setClassString("");
%>
<%
	sv.zrpcpd70.appendClassString("num_fld");
		sv.zrpcpd70.appendClassString("input_txt");
		sv.zrpcpd70.appendClassString("highlight");
%>
<%
	sv.zrpcpd71.setClassString("");
%>
<%
	sv.zrpcpd71.appendClassString("num_fld");
		sv.zrpcpd71.appendClassString("input_txt");
		sv.zrpcpd71.appendClassString("highlight");
%>
<%
	sv.zrpcpd72.setClassString("");
%>
<%
	sv.zrpcpd72.appendClassString("num_fld");
		sv.zrpcpd72.appendClassString("input_txt");
		sv.zrpcpd72.appendClassString("highlight");
%>
<%
	sv.zragfr10.setClassString("");
%>
<%
	sv.zragfr10.appendClassString("num_fld");
		sv.zragfr10.appendClassString("input_txt");
		sv.zragfr10.appendClassString("highlight");
%>
<%
	sv.zragto10.setClassString("");
%>
<%
	sv.zragto10.appendClassString("num_fld");
		sv.zragto10.appendClassString("input_txt");
		sv.zragto10.appendClassString("highlight");
%>
<%
	sv.zrpcpd73.setClassString("");
%>
<%
	sv.zrpcpd73.appendClassString("num_fld");
		sv.zrpcpd73.appendClassString("input_txt");
		sv.zrpcpd73.appendClassString("highlight");
%>
<%
	sv.zrpcpd74.setClassString("");
%>
<%
	sv.zrpcpd74.appendClassString("num_fld");
		sv.zrpcpd74.appendClassString("input_txt");
		sv.zrpcpd74.appendClassString("highlight");
%>
<%
	sv.zrpcpd75.setClassString("");
%>
<%
	sv.zrpcpd75.appendClassString("num_fld");
		sv.zrpcpd75.appendClassString("input_txt");
		sv.zrpcpd75.appendClassString("highlight");
%>
<%
	sv.zrpcpd76.setClassString("");
%>
<%
	sv.zrpcpd76.appendClassString("num_fld");
		sv.zrpcpd76.appendClassString("input_txt");
		sv.zrpcpd76.appendClassString("highlight");
%>
<%
	sv.zrpcpd77.setClassString("");
%>
<%
	sv.zrpcpd77.appendClassString("num_fld");
		sv.zrpcpd77.appendClassString("input_txt");
		sv.zrpcpd77.appendClassString("highlight");
%>
<%
	sv.zrpcpd78.setClassString("");
%>
<%
	sv.zrpcpd78.appendClassString("num_fld");
		sv.zrpcpd78.appendClassString("input_txt");
		sv.zrpcpd78.appendClassString("highlight");
%>
<%
	sv.zrpcpd79.setClassString("");
%>
<%
	sv.zrpcpd79.appendClassString("num_fld");
		sv.zrpcpd79.appendClassString("input_txt");
		sv.zrpcpd79.appendClassString("highlight");
%>
<%
	sv.zrpcpd80.setClassString("");
%>
<%
	sv.zrpcpd80.appendClassString("num_fld");
		sv.zrpcpd80.appendClassString("input_txt");
		sv.zrpcpd80.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sum Assured Reduced by Benefit Payment Amount");
%>
<%
	generatedText10.appendClassString("label_txt");
		generatedText10.appendClassString("information_txt");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Non-Blank denotes YES");
%>
<%
	generatedText11.appendClassString("label_txt");
		generatedText11.appendClassString("information_txt");
%>
<%
	sv.zredsumas.setClassString("");
%>
<%
	sv.zredsumas.appendClassString("string_fld");
		sv.zredsumas.appendClassString("input_txt");
		sv.zredsumas.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
		}
%>




<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><label style="padding-top: 8px;"><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
				<div class="form-group">
					<label style="align: center;"><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						width='100%'>
						<thead>
							<tr>
								<th colspan="2" class="info"><label><%=smartHF.getLit(generatedText13)%></label>&nbsp;<label><%=smartHF.getLit(generatedText14)%></label></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
								<th><%=smartHF.getHTMLVarExt(fw, sv.zrage08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd01, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd02, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd03, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd04, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd05, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd06, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd07, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd08, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd09, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd10, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd11, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd12, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd13, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd14, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd15, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd16, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd17, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd18, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd19, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd20, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd21, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd22, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd23, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd24, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd25, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd26, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd27, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd28, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd29, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd30, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd31, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd32, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd33, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd34, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd35, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd36, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd37, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd38, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd39, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd40, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd41, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd42, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd43, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd44, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd45, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd46, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd47, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd48, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd49, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd50, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd51, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd52, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd53, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd54, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd55, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd56, COBOLHTMLFormatter.S3VS2)%></td>

							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd57, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd58, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd59, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd60, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd61, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd62, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd63, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd64, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd65, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd66, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd67, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd68, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd69, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd70, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd71, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd72, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragfr10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zragto10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd73, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd74, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd75, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd76, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd77, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd78, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd79, COBOLHTMLFormatter.S3VS2)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.zrpcpd80, COBOLHTMLFormatter.S3VS2)%></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label style="padding-top: 10px;"><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>

			<div class="col-md-5 col-md-offset-1">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 10px;"><%=smartHF.getLit(generatedText11)%></label>
							</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td>
								<div style="width: 150px;">
									<%=smartHF.getHTMLVarExt(fw, sv.zredsumas).replaceAll("width:100%", "width:150%")%>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%
	}
%>

<%
	if (sv.Sr530protectWritten.gt(0)) {
%>
<%
	Sr530protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>