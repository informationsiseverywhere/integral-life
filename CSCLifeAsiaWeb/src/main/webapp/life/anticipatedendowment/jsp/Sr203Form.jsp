<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR203";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.anticipatedendowment.screens.*" %>
<% com.csc.life.anticipatedendowment.screens.Sr203ScreenVars sv = (com.csc.life.anticipatedendowment.screens.Sr203ScreenVars) fw.getVariables();%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind06.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.tranamt.setReverse(BaseScreenData.REVERSED);
			sv.tranamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.tranamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.sacscode.setReverse(BaseScreenData.REVERSED);
			sv.sacscode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.sacscode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.sacstypw.setReverse(BaseScreenData.REVERSED);
			sv.sacstypw.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.sacstypw.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
            sv.sacsflag.setInvisibility(BaseScreenData.INVISIBLE);
        }
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Records to be");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount to be");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Entered");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Entered");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deduct Stamp Duty (Y/N)");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Print Letter (Y/N)");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Records Actually");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount Actually");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Entered");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Entered");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind01.isOn()) {
			sv.trandatexDisp.setReverse(BaseScreenData.REVERSED);
			sv.trandatexDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.trandatexDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.zrnofrec01.setReverse(BaseScreenData.REVERSED);
			sv.zrnofrec01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.zrnofrec01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.totalamt01.setReverse(BaseScreenData.REVERSED);
			sv.totalamt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.totalamt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.tcsd.setReverse(BaseScreenData.REVERSED);
			sv.tcsd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.tcsd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.letterPrintFlag.setReverse(BaseScreenData.REVERSED);
			sv.letterPrintFlag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.letterPrintFlag.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<script>
	var firstTime = true;
	var copy;
</script> 
<div class="panel panel-default">
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-3"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Records to be Entered")%></label>
							<div class="form-group" style="max-width:150px";>
							<%if ((new Byte((sv.zrnofrec01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrnofrec01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='zrnofrec01' 
type='text'

<%if((sv.zrnofrec01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right" <% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrnofrec01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrnofrec01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrnofrec01) %>'
	 <%}%>

size='<%= sv.zrnofrec01.getLength() + 5 %>'   
maxLength='<%= sv.zrnofrec01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrnofrec01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrnofrec01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" 				
	
<%
	}else if((new Byte((sv.zrnofrec01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  				
		

<%
	}else { 
%>

	class = ' <%=(sv.zrnofrec01).getColor()== null  ? 
			"input_cell" :  (sv.zrnofrec01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"  %> 				
			'
 
<%
	} 
%>
>
<%}%>
</div></div>
<div class="col-md-3">
						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Amount to be")%></label>
							<div class="form-group"> 
							<%if ((new Byte((sv.totalamt01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
			qpsf = fw.getFieldXMLDef((sv.totalamt01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S16VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.totalamt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='totalamt01' 
type='text'

<%if((sv.totalamt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totalamt01.getLength(), sv.totalamt01.getScale(),3)%>'
maxLength='<%= sv.totalamt01.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(totalamt01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.totalamt01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.totalamt01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.totalamt01).getColor()== null  ? 
			"input_cell" :  (sv.totalamt01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
</div></div>
<div class="col-md-3">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label> 
       <div class="form-group" style="max-width:100px;">   <%	
	if ((new Byte((sv.trandatexDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.trandatexDisp.getFormData();  
%>

<% 
	if((new Byte((sv.trandatexDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else { 
%>


<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="trandatexDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.trandatexDisp,(sv.trandatexDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>
		</div>
		</div>                                       
</div>
</div>
<div class="row">        
					<div class="col-md-3"> 
						<%
longValue = null;
formatValue = null;
if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Records Actually")%></label>
							<%}%>
							<div class="form-group" style="max-width:150px";>
							<%if ((new Byte((sv.zrnofrec02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zrnofrec02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zrnofrec02);
			
			if(!((sv.zrnofrec02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
			
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 </div>
 </div>
 <div class="col-md-3">
		<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Amount Actually")%></label>
							<%}%>
							<div class="form-group">
		<%if ((new Byte((sv.totalamt02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totalamt02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S16VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalamt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.totalamt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" >  <!--ILIFE-3716-->
					
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" 				style= "width:150px;" > <!-- ILIFE-3716-->
				 &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>					
</div>
</div>
 <div class="col-md-3">
	<div class="form-group">
		<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		<label><%=resourceBundleHandler.gettingValueFromBundle("Deduct Stamp Duty (Y/N)")%></label>
		 <%}%>
		<%if ((new Byte((sv.tcsd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

		<input type='checkbox' name='tcsd' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(tcsd)' onKeyUp='return checkMaxLength(this)'    
		<%
		
		if((sv.tcsd).getColor()!=null){
					 %>style='background-color:#FF0000;'
				<%}
				if((sv.tcsd).toString().trim().equalsIgnoreCase("Y")){
					%>checked
				
		      <% }if((sv.tcsd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
		    	   disabled
				
				<%}%>
		class ='UICheck' onclick="handleCheckBox('tcsd')"/>

		<input type='checkbox' name='tcsd' value='N' 
		
		<% if(!(sv.tcsd).toString().trim().equalsIgnoreCase("Y")){
					%>checked
				
		      <% }%>
		
		style="visibility: hidden" onclick="handleCheckBox('tcsd')"/>
		<%}%>
	</div>
</div>	
			
 <div class="col-md-3">
	<div class="form-group">
		<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		<label><%=resourceBundleHandler.gettingValueFromBundle("Print Letter (Y/N)")%></label>	
		<%}%>
		
		<%if ((new Byte((sv.letterPrintFlag).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

		<input type='checkbox' name='letterPrintFlag' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(letterPrintFlag)' onKeyUp='return checkMaxLength(this)'    
		<%
		
		if((sv.letterPrintFlag).getColor()!=null){
					 %>style='background-color:#FF0000;'
				<%}
				if((sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
					%>checked
				
		      <% }if((sv.letterPrintFlag).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
		    	   disabled
				
				<%}%>
		class ='UICheck' onclick="handleCheckBox('letterPrintFlag')"/>

		<input type='checkbox' name='letterPrintFlag' value='N' 
		
		<% if(!(sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
					%>checked
				
		      <% }%>
		
		style="visibility: hidden" onclick="handleCheckBox('letterPrintFlag')"/>
		<%}%>
				
	</div>
</div>
																																
 </div>
 <div class="row">
 	<%if ((new Byte((sv.sacsflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-8">
	<% } else {%>
				<div class="col-md-6">
	<% } %>
					<div class="table-responsive">
						<table id="dataTables-sr203" class="table table-striped table-bordered table-hover" width='100%'>
							<%if ((new Byte((sv.sacsflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
								<col style="width:25%">
								<col style="width:25%">
								<col style="width:25%">
								<col style="width:25%">
							<% } %>
                         	<thead>
                            	<tr class='info'>
                            		<%if ((new Byte((sv.sacsflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
	                            		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sacs Code")%></center></th>
	                            		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sacs Type")%></center></th>
                            		<% } %>
                            		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></center></th>
		         					<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Amount")%></center></th>
		         				</tr>
		         			</thead>
		         	<%if (appVars.ind09.isOn()) {
		            sv.sacsflag.setInvisibility(BaseScreenData.INVISIBLE);
		        }	%>	
		        
		        <%if ((new Byte((sv.sacsflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
		         	 <script type="text/javascript">
		         	 	function loadSacscode(count){
		         	 		var options;
		         	 		<%
		         	 			if(sv.sacscodeMap != null && !sv.sacscodeMap.isEmpty()){
		         	 				for(Map.Entry<String, List<String>> entry: sv.sacscodeMap.entrySet()){%>
		         	 					options = document.createElement("option");
		         	 					options.text = "<%=entry.getKey()%>";
		         	 					document.getElementById("sr203screensfl.sacscode_R"+count).add(options);
		         	 		<%		}
		         	 			}
		         	 		%>
		         	 	}
						function loadSacstype(count){
							var map = new Map();
							var i = 0;
							var sacscode = document.getElementById("sr203screensfl.sacscode_R"+count).value;
							<%for(Map.Entry<String, List<String>> entry: sv.sacscodeMap.entrySet()){%>
								var inner = new Array();
								if(sacscode == "<%=entry.getKey()%>"){
									<%for(String val: entry.getValue()){ %>
										inner[i] = "<%=val%>";
										i++;
									<% } %>
								}
								map.set("<%=entry.getKey()%>", inner);
							<% } %>
							var options = "";
							var sacstype = map.get(sacscode);
							var elem = document.getElementById("sr203screensfl.sacstypw_R"+count);
							if(firstTime){
								copy = elem.cloneNode(true);
							}
							elem.options.length = 0;
							options = document.createElement("option");
							options.value = copy.options[0].value;
							options.title = copy.options[0].title;
							options.text = copy.options[0].title;
							elem.add(options);
							for(var i=0; i<sacstype.length; i++){
								if(typeof sacstype[i] !== 'undefined' && sacstype[i] !== ""){
									for(var j=0; j<copy.options.length; j++){
										if(copy.options[j].value == sacstype[i]){
											options = document.createElement("option");
											options.value = sacstype[i];
											options.title = copy.options[j].title;
											options.text = copy.options[j].title;
											elem.add(options);
										}
									}
								}
							}
							firstTime = false;
						}
						
						function loadInitSacstype(count){
		         	 		var elem = document.getElementById("sr203screensfl.sacstypw_R"+count);
		         	 		for(var i=0; i<elem.options.length; i++){
		         	 			elem.options[i].style.display = "none";
		         	 		}
		         	 	}
					</script>
					<%}%>
		        	<%
		        	GeneralTable sfl = fw.getTable("sr203screensfl");
		        	com.csc.life.anticipatedendowment.screens.Sr203screensfl.set1stScreenRow(sfl, appVars, sv);
		        	int count = 1;
		        	while (com.csc.life.anticipatedendowment.screens.Sr203screensfl
		        			.hasMoreScreenRows(sfl)) {	
		        %>
		         <%
		        if (appVars.ind08.isOn()) {
					sv.sacscode.setReverse(BaseScreenData.REVERSED);
					sv.sacscode.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind08.isOn()) {
					sv.sacscode.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind10.isOn()) {
					sv.sacstypw.setReverse(BaseScreenData.REVERSED);
					sv.sacstypw.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind10.isOn()) {
					sv.sacstypw.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind09.isOn()) {
		            sv.sacsflag.setInvisibility(BaseScreenData.INVISIBLE);
		        }
				if (appVars.ind06.isOn()) {
					sv.chdrnum.setReverse(BaseScreenData.REVERSED);
					sv.chdrnum.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind06.isOn()) {
					sv.chdrnum.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind07.isOn()) {
					sv.tranamt.setReverse(BaseScreenData.REVERSED);
					sv.tranamt.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind07.isOn()) {
					sv.tranamt.setHighLight(BaseScreenData.BOLD);
				}
		        %> 
		<tr class="tableRowTag" id='<%="tablerow"+count%>' >
				<%if ((new Byte((sv.sacsflag).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
						<td>
							<%
								fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("sacscode");
								mappedItems.keySet().retainAll(sv.sacscodeMap.keySet());
								optionValue = makeDropDownList( mappedItems , sv.sacscode.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());
								if("red".equals((sv.sacscode).getColor())){
							%>
								<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572; ">
							<%	}
							%>
								<select name=<%="sr203screensfl"+"."+"sacscode"+"_R"+count%> id=<%="sr203screensfl"+"."+"sacscode"+"_R"+count%> type='list'
								style = " width:230px "
									<%
										if((new Byte((sv.sacscode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>
											class="bold_cell"
									<%} else {%>
											class = 'input_cell'
									<%}%> onchange = "loadSacstype(<%=count%>);">
									<%=optionValue%>
								</select>
								<% if("red".equals((sv.sacscode).getColor())){%>
									</div>
							<%	}
								longValue = null;
								optionValue = null;
							%>
						</td>
						<td>
							<%
								fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstypw"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("sacstypw");
								optionValue = makeDropDownList( mappedItems , sv.sacstypw.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.sacstypw.getFormData()).toString().trim());
								if("red".equals((sv.sacstypw).getColor())){
							%>
								<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;">
							<%	}
							%>
								<select name=<%="sr203screensfl"+"."+"sacstypw"+"_R"+count%> id=<%="sr203screensfl"+"."+"sacstypw"+"_R"+count%> type='list'
								style = " min-width:264px "
									<%
										if((new Byte((sv.sacstypw).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>
											class="bold_cell"
									<%}else {%>
											class = 'input_cell'
									<%}%>onfocus = "loadSacstype(<%=count%>);">
										<%=optionValue%>
								</select>
								<% if("red".equals((sv.sacstypw).getColor())){%>
									</div>
							<%	}
								longValue = null;
								optionValue = null;
							%>
						</td>
						<script>
		        			loadInitSacstype(<%=count%>);
		        		</script>
				<% } %>
						<%if((new Byte((sv.chdrnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>
																			
						 
		           <%=smartHF.getHTMLVarExt(fw, sv.chdrnum, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 10,101)%>
        </div>
					</td>		
				  <% }%>	  
						  						 
										
					
											
			<%if((new Byte((sv.tranamt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
						<div class="form-group">
		           <%=smartHF.getHTMLVarExt(fw, sv.tranamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 10,150)%>
        </div>
      
				</td>		
				  <% }%>			
	</tr>

	<%
	count = count + 1;
	com.csc.life.anticipatedendowment.screens.Sr203screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
</div>
</div>		
</div></div></div>

<!-- <script>
$(document).ready(function() {
    $('#sr203Table').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false
       
    } );
   
} );

</script> --> 

 <script>
       $(document).ready(function() {
              $('#dataTables-sr203').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "405px",
                     scrollCollapse : false,
                     paging:   false,
                     ordering: false,
              		 info:     false,
               		 searching: false
              });
       });
</script>
<!-- 
<script type="text/javascript">
$(document).ready(function() {
	var x = $("select[id^='sr203screensfl.sacscode_R']");
	for (var i = 0; i < x.length; i++) {
		x[i].onchange = function() {
			loadSelect(this);
		}
		//x[i].size=2;
		/*$(x[i]).bind("beforeactivate",function(e){
			displayDropdown(this,true);
			$(this).css("visibility","hidden");
			//e.preventDefault();
			//e.stopPropagation();
			//return false;
		});*/
		<!-- GEPAS-15 START -->
		<!-- var saccode = x[i].value;
		var id = x[i].id.replace('sacscode', 'sacstypw');
		var sactype = document.getElementById(id).value;
		var sflIdx = id.substring(id.indexOf("_R")+2);
		if((saccode == 'RF' && sactype == 'P1') ||
		(saccode == 'CF' && sactype == 'CG')
		|| (saccode == 'RF' && sactype == '41')
		|| (saccode == 'RF' && sactype == '43')
		|| (saccode == 'RF' && sactype == '44')){
			$('#iCon'+ sflIdx).css('display','inline');
		}else{
			$('#iCon'+ sflIdx).css('display','none');
		}
	}
	x= $("select[id^='sr203screensfl.sacstypw_R']");
	for(var  i=0; i < x.length; i++){
		x[i].onchange = function(){loadParticipant(this);}
		
	}
});
</script> -->

<!-- <script>
       $(document).ready(function() {
       $('#dataTables-sr203').DataTable({
             ordering: false,
             searching:false,
             scrollY: "300px",
             scrollCollapse:true,
      });
 
    });
</script>  -->

<%@ include file="/POLACommon2NEW.jsp"%>

