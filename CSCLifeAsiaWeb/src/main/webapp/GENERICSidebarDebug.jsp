<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@page import="com.properties.PropertyLoader"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>

<!-- This code supports the debugging functions available.                        -->
<!-- It is a separate file as both Generic and Specific SideBars can be created.  -->
<!-- In order for the following code to work, the following must be true:         -->
<!-- 1. Normal jsp setup for the application, including basemodel and screenmodel.-->
<!-- 2. Inclusion of Sidebar.js                                                   -->
<!-- 3. Inclusion of Standard Style Sheet.                                        -->

<%		BaseModel bm1 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
		if (bm1 != null) {
			ScreenModel fw1 = (ScreenModel) bm1.getScreenModel();
			LifeAsiaAppVars av = (LifeAsiaAppVars)bm1.getApplicationVariables(); 
			String lang = av.getInstance().getUserLanguage().toString().trim();
			SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw1.getScreenName(),lang);
			String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());
			if (fw1.getAppVars().getDebugAllowed() > 0) {%>
<div id="menuContainer" style="text-wrap:on; white-space: normal; display: normal;">
<div id="menu1" class="cmenu" style="text-wrap:on; white-space: normal; display: normal;">
<div id="menuItem1_1" class="cmenuItem"><A
	href="javaScript:window.showModalDialog ('MessagesRev.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;')">Messages</A></div>
<div id="menuItem1_2" class="cmenuItem"><A
	href="javaScript:window.showModalDialog ('Diagnostics.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;')">Diagnostics</A></div>
<div id="menuItem1_3" class="cmenuItem"><A
	href="javaScript:window.showModalDialog ('Timings.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;')">Timings</A></div>
<div id="menuItem1_4" class="cmenuItem"><A
	href="javaScript:doAction('GENStack')">Stack</A></div>
</div>
</div>
<%=		AppVars.hf.getActionButton("Debug/REF any.jsp\" menu=menu1 spare=\"")%>
<%		}

		else {%>
<div id="menuContainer">
<div id="menu1" class="cmenu">
<div id="menuItem1_1" class="cmenuItem"><A
	href="javaScript:callCommonAlert(<%=imageFolder%>,'No00023');">Unavailable</A></div>
</div>
</div>
<%=		AppVars.hf.getActionButton("Debug/REF any.jsp\" menu=menu1 spare=\"")%>
<%		}
}%>