$(document).ready(function(){
	// Remove input-group class when the page is read onnly for button
	 var children = $(".readOnly-remove").children("span.input-group-btn");
	 if (!(children.length > 0)) {
		$(".readOnly-remove").removeClass("input-group");
	 }
	 
	 $(".nav-tabs a").click(function(){
	     $(this).tab('show');
	 });
	 $("div[style*='visibility:hidden']").css('position','fixed');
	 $("div[style*='visibility: hidden']").css('position','fixed');
	 
	//change controllers to bootstrap style
	$("select").addClass("form-control");
	$("input[type='text']").removeClass("bold_cell");
	$("input[type='text']").addClass("form-control");
	$('[data-toggle="tooltip"]').tooltip();
	
	$("input[class*='bold_cell'], input[class*='red']").each(function(){
		$(this).addClass("form-control");
	})
	//tables and codes item desc
	
	if (screen.height == 900) {
		
		$('#idesc').css('max-width','230px')
	} 
	if (screen.height == 768) {
		
		$('#idesc').css('max-width','220px')
	}
	if (screen.height == 1024) {
	
	$('#idesc').css('max-width','195px')
	}  
	//set calendar style
	//alert($('#businessDate').val());
	$('.form_date').attr("data-date", $('#businessDate').val());
	
	$('.form_date input').each(function(){
		$(this).val($.trim($(this).val()));
	})
	
	/*$('.form_date').datepicker({
		format: "dd/mm/yyyy",
	    language:  'en',
	    weekStart: 1,
	    todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
	});	
	*/
	//set error style for controllers(input,select, textarea)
	$("div[class^='form-group']:has(input[class*='red']),div[class^='form-group']:has(div[class*='red']),span[class^='form-group']:has(input[class*='red'])," +
			"td[class^='form-group']:has(input[class*='red']), div[class^='form-group']:has(select[class*='red'])," +
			"div[class^='form-group']:has(textarea[class*='textareaerror'])").each(function(){
		$(this).addClass("has-error");
	});
	
	//add class btn-xs size for button to display like previous/continue/exit button
	$("a.btn-info").each(function(){
		$(this).addClass("btn-xs");
		$(this).css('font-weight','bold');
	});
	
	//set error style for calendar
	$(".form_date").find("input").each(function(){
		if($(this).hasClass("red"))
		{
			$(this).parent().addClass("has-error");
		}
	});
	
	//for div with output_cell(read-only scenario)
	$("div[class*='output_cell']").each(function(){
		$(this).removeClass("output_cell");
		$(this).addClass("form-control");
		$(this).css("background-color","#eee");
		$(this).css("color","#999");
		$(this).css("font-weight","bold");
	});
	
	$("div[class*='input-group-addon']").each(function(){
		$(this).css("font-weight","bold");
		$(this).css("color","#999");
	});
	
	//for disabled <input> field
	//make it act the same as read-only <div>
	$("input[class*='form-control']:disabled").each(function(){
		$(this).css("font-weight","bold");
	});
	
	$("input[class*='form-control'][readonly='true']").each(function(){
		$(this).css("font-weight","bold");	
		$(this).css("color","#999");
	});
	
	$("div[class*='blank_cell']").each(function(){
		$(this).removeClass("blank_cell");
		$(this).addClass("form-control");
		$(this).css("background-color","#eee");
	});
	
	//replace form-control by input-group-addon for several inputs stand closely
	$("div[class*='input-group']").find("div:gt(0)").each(function(){
		$(this).removeClass("form-control");
		$(this).addClass("input-group-addon");
	});
		
	$('input[type=checkbox]').each(function(){
		if($(this).css('visibility') == 'hidden'){
			$(this).css('margin-top','-13px');
			$(this).css('margin-left','13px');
		}
	});
	calculatePageHeight();
    var length = strLen($(this).text().replace(/(^\s+|\s+$)/g, ""));
	$("div[style*='background-color: rgb(238, 238, 238)']").each(function(){
		if( length != 0){
			if(length <= 3 ){
				$(this).css('width','100px');//reduce size client no
			} else if(length > 3 && length <= 8 ){
				$(this).css('width','71px');//reduce size client no
			} else if(length > 8 && length <= 10 ){
				$(this).css('width','85px');//reduce size date
			} else if(length > 10  && length <= 20 ){
				$(this).css('width','145px');//reduce size amount
			}
		}
		if((length < $(this).width()) || (length > 25)){
			$(this).addClass("ellipsis");
			$(this).attr('title', this.innerText);
			 if ($(window).width() > 1280) {
				 $('div.col-md-4').find("div.col-md-4:not(':eq(0)')").css('margin-left','-30px');  
				 $('div.col-md-4').find("div.col-md-4:not(':eq(0)')").css('min-width','315px'); 
			 }
		} 
	});
	
	$('input').on('keypress', function (event) {
	    var regex = document.getElementById('xssblacklist').value;    // IJTI-1635
	    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    var isAngleBracket = regex.includes(key);
	    if (isAngleBracket) {
	       event.preventDefault();
	       return false;
	    }
	});
	if($("div[style*='background-color: rgb(238, 238, 238)']")!= null){	
		if($("#item").attr('title')!=undefined){
			if($("#item").attr('title').length<=4){			
				$("#item").width(50);
			}else if($("#item").attr('title').length>4 && $("#item").attr('title').length<=8){	
				$("#item").width(75);
			}
		}
		if($("#shortdesck").attr('title')!=undefined){
			if($("#shortdesck").attr('title').length<=5 ){			
				$("#shortdesck").width(70);
			}else if($("#shortdesck").attr('title').length>5 && $("#item").attr('title').length<=10 && $("#shortdesck").attr('title').length !=undefined){
				$("#shortdesck").width(100);
			}	
		}
	}
	 $("#schname").width(83); 
	 $("#adjustageamt").width(160);
	 $("#zbinstprem").width(160);
	 $("#rateadj").width(160);
	 $("#fltmort").width(160);
	 $("#loadper").width(160);
	 $("#premadj").width(160);
	 $("#zlinstprem").width(160);
	 $("#instPrem").width(160);
	 $("#taxamt").width(160);	 
	 $("#zstpduty01").width(160);
	 $("#numapp").width(160);
	 $("#totprem").width(160);
	 $("#linstamt").width(160);
	 $("#zstpduty01").width(160);
	 $("#singlePremium").width(160);
	 $("#taxamt01").width(160);
	 $("#instpramt").width(160);
	 $("#nextinsamt").width(160);
	 $("#cbillamt").width(160);
	 $("#scheduleName").width(110);
	 
	 $("input[name*='instPrem']").width(160);
	 $("input[name*='taxamt']").width(160);
	 $("input[name*='linstamt']").width(160);
	 $("input[name*='sumin']").width(124);
	 
	 
	 
});

document.addEventListener("DOMContentLoaded", function(event) { 
    //after page load successfully
    $("#chdrsel").parent().css('width','120px');//reduce size Contract number
    $("#chdrsel").css("min-width","65px");
    $("#clttwo").css("min-width","74px");
    
    $("#clntnum").css("min-width","71px");
    
   
    
    if($("div[class*='input-group'] :input[name*='ownersel']")!= null && $("div[class*='input-group'] :input[name*='ownersel']").parents()[2]!= undefined){
    		$("div[class*='input-group'] :input[name*='ownersel']").parents()[2].style.width = '110px';//reduce size Contract Owner
    }
    if($("div[class*='input-group'] :input[name*='agntsel']")!= null && $("div[class*='input-group'] :input[name*='agntsel']").parents()[2]!= undefined){
    	$("div[class*='input-group'] :input[name*='agntsel']").parents()[2].style.width = '110px';//reduce size Agency number
    }
    if($("div[class*='input-group'] :input[name*='clttwo']")!= null && $("div[class*='input-group'] :input[name*='clttwo']").parents()[2]!= undefined){
    	$("div[class*='input-group'] :input[name*='clttwo']").parents()[1].style.width = '110px';//reduce size Client number
    	$("div[class*='input-group'] :input[name*='clttwo']").parents()[0].style.width = '74px';//reduce size Client number
    }
    $("#lifesel").css('width','65px !important');//reduce size life number 
    $(".form-group").find('label').each(function(){
    	if($(this).innerText == 'Joint Owner'){
    		$(this).css('width','65px');
    	}
    });
    
	// fix the null value div too small problem
	var secondDiv = $(".three-controller").find("div:eq(1)");
	var thirdDiv = $(".three-controller").find("div:eq(2)");
	 if (secondDiv.val()!= undefined && secondDiv.val().length < 1) {
		 secondDiv.css("min-width", "65px");
	 }
	 if (thirdDiv.val()!= undefined && thirdDiv.val().length < 1) {
		 thirdDiv.css("min-width", "150px");
	 }
	
	var pagination = $('#dataTables-s2473_wrapper div.row').last();
	if (pagination.length == 1){
		pagination.css("margin-top","5px !important");
	}
});

function exchangeAddon(exchangeDiv) {
	var firstElement = $(exchangeDiv).children(".form-control");
    var secondElement = $(exchangeDiv).children(".input-group-addon");
    if (firstElement.length > 0 && secondElement.length > 0) {
        firstElement.removeClass("form-control");
        firstElement.addClass("input-group-addon");
        secondElement.removeClass("input-group-addon");
        secondElement.addClass("form-control");
    }
}
//calculate page's height
function calculatePageHeight(){
	var panelContent1 = $(".container div[class*='panel-default']").first();
	var panelContent2 = $(".container").find("div[class*='panel-default']:not(':eq(0)')");
	var bodyContent = $('#mainBody');
	var navigateHeight = $('#navigateButton');
	var containerHeight = 0;
	var pageHeader = $("#page-header").height();
	var menuVisible = $("div[style*='visibility: hidden']");
	if(panelContent2.length != 0){ // page has 2 panels
		panelContent2.addClass("secondPanel");
		$('.secondPanel').find('div.row').addClass("row-subMenu");
		var panelContent1AfterHeight = panelContent1.outerHeight(true);
		var marginPanel2 =  $(".secondPanel").outerHeight(true) - $(".secondPanel").height();
		var rowSubMenuHeight =  $(".row-subMenu").outerHeight(true) - $(".row-subMenu").height();
		if($('#s0017Row').length == 1){
			panelContent2.css('height','250px')
		}
		else{
			if(menuVisible.length == 1){
				menuVisible.css('height','0 !important');
			}
			//	containerHeight = bodyContent.height() - panelContent1AfterHeight - navigateHeight.outerHeight(true) - parseInt(marginPanel2, 10) - pageHeader + 5 ; // 5px end of page
			
			/*panelContent2.css('height',containerHeight + "px");*/
			
			panelContent2.css('height','auto');
		}
	}else{
		if($('.table-responsive').length >= 1){
					
				if (screen.height == 1024) {
					/*panelContent1.css('height','745px');*/
					
					panelContent1.css('height','auto');

			 	}else if (screen.height == 900) {
			 			/*panelContent1.css('height','1180');*/
			 			panelContent1.css('height','auto');
				 	  } else{ 
					 	/*panelContent1.css('height','700px !important');*/
					 	panelContent1.css('height','auto');
				 	  }	
		}else{
			if($('.sidebar-nav.navbar-collapse').length == 1){
				$('.sidebar-nav.navbar-collapse').removeAttr('style');
				$('.sidebar-nav.navbar-collapse').hide();
			}
			
			//containerHeight = bodyContent.height() - pageHeader  - navigateHeight.outerHeight(true) ;
			
			/*panelContent1.css('max-height',containerHeight + "px");
			panelContent1.css('height',containerHeight + "px");*/
			
			
			panelContent1.css('height','auto');
			var mainHeight = bodyContent.height() - $('#topPanel').outerHeight(true)- navigateHeight.outerHeight(true) - 5;
			var bodyScreenHeight = screen.height - navigateHeight.outerHeight(true);
			if(screen.height > 1000 && mainHeight < screen.height){
				if(!!window.chrome){
					mainHeight = bodyScreenHeight - navigateHeight.outerHeight(true) - $('#topPanel').outerHeight(true) - 5;
				}else{
					mainHeight = bodyScreenHeight - navigateHeight.outerHeight(true) - $('#topPanel').outerHeight(true) + 10;
				}
			}else{
				mainHeight = screen.height - $('#topPanel').outerHeight(true);
			}
			$('#mainareaDiv').css({'width' : '100%','height' : 'auto'});
		}
	}
	
	
	
	var panelContent3 = $(".container div[class*='panel-default']").last();
	var panelVal= $('.panel-heading:not(:has(a))').contents().last().text().toString();
	var trimPanelVal = panelVal.replace(/^\s+|\s+$/g, "");
	
/*	var subpageHeight=0;*/
	
	if(trimPanelVal=="Actions"){		
		/*subpageHeight = subpageHeight+300;*/
		panelContent3.css('height','auto');
		/*panelContent3.css('height',subpageHeight + "px");*/
	}
	
	
}	
//calculate page's height


// Calculate the length of the string (1 character in English and 2 characters in Chinese)
function strLen(str){
    var len = 0;
    for (var i=0; i<str.length; i++) {
        var c = str.charCodeAt(i);
        if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) {
            len++;
        }
        else {
            len+=2;
        }
    }
    return len;
}
//IBPTE-1528 starts
function loadExtraInfo(extraInfoDiv) {
	if(parent.frames["frameMenu"].document.readyState == "complete") {
		loadExtraInfoData(extraInfoDiv);
	} else {
		setTimeout(loadExtraInfo, 100, extraInfoDiv);
	}
}

function loadExtraInfoData(extraInfoDiv) {
	var doc1=parent.frames["frameMenu"].document;
	var obj = doc1.getElementById("sidebar_OPTS");
	if(obj != null && extraInfoDiv) {
		obj.innerHTML =  extraInfoDiv.innerHTML;
	} else {
		setTimeout(loadExtraInfoData, 100, extraInfoDiv);
	}
}
//IBPTE-1528 ends
