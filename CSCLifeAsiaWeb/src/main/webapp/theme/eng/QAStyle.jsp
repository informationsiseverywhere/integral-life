<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page session="false"%>
<%
	/* Contains CSS entries dependent on the context path.*/
	String ctx = request.getContextPath() + "/";

response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");
response.addHeader("X-Frame-Options", "SAMEORIGIN");
response.addHeader("X-Content-Type-Options", "NOSNIFF");
response.addHeader("Cache-Control", "no-store"); 
response.addHeader("Pragma", "no-cache"); 
%>

.blink { behavior:url(<%=ctx%>theme/blink.htc);} 
.reverse {behavior:url(<%=ctx%>theme/reverse.htc);} 
.alert_txt { behavior:url(<%=ctx%>theme/blink.htc);}
.dropNotSelected { 
border:1px solid #ccc; 
padding-right:1em;
font-size:100%; 
background:url(<%=ctx%>/images/arrow.gif) top center no-repeat; 
display:block;
 position:relative;
 } 
.dropSelected {
border:1px solid #ccc;
 color:white;
 padding-right:1em;
 font-size:100%;
background: blue;
 display:block;
 position:relative;
 }
