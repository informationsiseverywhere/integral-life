<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             

<%
	//AWPL-Integral Integration related code changes. Get the query string value and pass it to FirstServlet. 
	//If query string contains only property name as 'ticket' in the URL then CAS will throw the error. 
	//So renamed it as 'ticketNo' and passed to FirstServlet. 
	//If query string contains only ticket information then no need to send query string to FirstServlet.	
 	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  	response.addHeader("X-Content-Type-Options", "NOSNIFF");
	response.addHeader("Cache-Control", "no-store"); 
	response.addHeader("Pragma", "no-cache");
		response.sendRedirect(request.getContextPath()+"/FirstServlet");
	 		
%>