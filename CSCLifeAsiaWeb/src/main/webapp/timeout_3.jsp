<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<html>
<head>
	<title>logout</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
	<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
</head>

<body class="main">
<%
	long time = 0;
	ResourceBundleHandler resourceBundle = new ResourceBundleHandler(request.getLocale());
	String units = "";
	try{
	if (session != null) {
		BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    	if ( baseModel != null) {
			AppVars av = baseModel.getApplicationVariables();
			if (av != null) {
				time = av.getAppConfig().userTimeout/1000;
				units = resourceBundle.gettingValueFromBundle("seconds");
				//units = "seconds";
				if (time > 60) {
					time = time/60;
					units = resourceBundle.gettingValueFromBundle("minutes");
					//units = "minutes";
					if (time > 60) {
						time = time/60;
						units = resourceBundle.gettingValueFromBundle("hours");
						//units = "hours";
					}
				}
			}
		}
		session.invalidate();
	}
	}catch(Throwable e){
	}
	if (time > 0) {%>
		<p><%=resourceBundle.gettingValueFromBundle("You have been timed out after more than")%><%=time%> <%=units%> 
		<%=resourceBundle.gettingValueFromBundle("of	inactivity")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("You will have to log on again in order to use the system")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("We apologise for the inconvenience")%>,
		<%=resourceBundle.gettingValueFromBundle("however such timeouts are required for security reasons")%>.</p>
<%	}
	else {%>
		<p><%=resourceBundle.gettingValueFromBundle("You have been timed out after an extended period of inactivity")%>.</p>
		<p><%=resourceBundle.gettingValueFromBundle("You will have to log on again in order to use the system")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("We apologise for the inconvenience")%>,
		<%=resourceBundle.gettingValueFromBundle(" however such timeouts are required for security reasons")%>.</p>
<%	}%>

</body>
</html>
