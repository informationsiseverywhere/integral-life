<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%><HTML>
<HEAD>
<title>Timings</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.sessionfacade.*" %>

<%
	GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
	BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
	LifeAsiaAppVars av = (LifeAsiaAppVars)bm.getApplicationVariables();
	String actionClass = av.getSupportPath() + ".commonroutines." + "QPGENClearStatics";
	gd.processAction(bm, actionClass);
	String msgs = av.getNewMessages();
%>

</head>
<BODY class="main">
<script src="js/Sidebar.js"></script>
<h2>Clear static caches</h2>
<p>This screen clears some static caches to allow reloading without stopping the server.
<p>
<Button onClick='doAgain()'>Repeat</Button>
<br><Button onClick="dontDoAgain()">Close this window</Button>
<p><%=msgs%>
<Script>
	
  function dontDoAgain() {
  	returnValue = "done";
  	window.close();
  }
  
  function doAgain() {
  	returnValue = "redo";
  	window.close();
  }
</Script>
</BODY>
</HTML>