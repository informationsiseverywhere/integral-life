<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<%@page import="com.ia.framework.dialect.FrameworkDialectFactory"%>
<%@ page session="true" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.csc.lifeasia.runtime.variables.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.smart400framework.SMARTAppVars"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatterNew"%>
<%@ page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@ page import="com.csc.lifeasia.runtime.core.LifeAsiaAppLocatorCode" %>
<%@ page import="com.resource.ResourceBundleHandler"%>
<html> 
<%
    char fileSeparator = '/'; //System.getProperty("file.separator");
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    String screenForm = "UnavailableSingle.jsp";
    String sideofScreen="POLASidebarNEW.jsp";
    if ( baseModel != null) {
    	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
		/* String lang = av.getUserLanguage().toString().trim().toLowerCase(); */
		String lang = av.getUserLanguage().toString().trim().toLowerCase();
		String scr = av.getNextSingleModelJSPScreen();
		String path;
		//Test jsp
		String jspConfigPath=av.getJspConfigPath();
		String jspPath=jspConfigPath;
		if(jspConfigPath==null || jspConfigPath.isEmpty()){
			jspPath="eng";
		}
		
		/* JSP path should be relevant to SxxxxScreenVars other than program 
		 * e.g. 
		 *  P4135 is in com.csc.polisy.underwriting.programs
		 *  S4135ScreenVars is in com.csc.polisy.reporting.screens
		 *  S4135Form.jsp is in web_root/polisy/reporting/...
		 * Further, to use java package of screen vars will improve performance as well
		 *  as it does not need to use Class.forName() a lot.
		 */
		
		VarModel screenVars = baseModel.getScreenModel().getVariables();
		if (screenVars == null) {
			path = LifeAsiaAppLocatorCode.findPath(scr);
		} else {
			path = screenVars.getClass().getPackage().getName();	
		}
		String[] sa = path.split("\\.");
		if (sa.length < 4 || scr.startsWith("JSPTester") || scr.startsWith("TestOnline")) {
			screenForm = scr;
		} else {
			String prefix = sa[2] + fileSeparator;
			if (sa.length >= 5) {
				for(int i=3; i < sa.length-1; i++){
					prefix += sa[i] + fileSeparator;
				}
			}
			//Test JSP
			screenForm = prefix + jspPath + fileSeparator + scr;
			
			/* System.out.println("Screen>>>>>>>"+screenForm); */
			/* ~cpatodi #IGROUP-1761 Sidebar values are not displaying for "jap" locale as path contain "jap" folder which does not exist*/
			//sideofScreen=prefix + lang + fileSeparator+"sdof"+scr;
			sideofScreen=prefix + jspPath + fileSeparator+"sdof"+scr;
			/* ~cpatodi #IGROUP-1761 Sidebar values are not displaying for "jap" locale as path contain "jap" folder which does not exist*/
            // If there is no Chinese version jsp, then switch to English version
            if (!lang.equals("eng") && request.getSession().getServletContext().getResource(fileSeparator + screenForm) == null) {
                screenForm = prefix + jspPath + fileSeparator + scr;
            }
            if(sideofScreen.equals("smart/"+jspPath+"/sdofS0028Form.jsp")) {
            	if(!FrameworkDialectFactory.getInstance().isLife()) {
            		sideofScreen="POLASidebarNEW.jsp";	
            	}
            }
            else {
            	if(request.getSession().getServletContext().getResource(fileSeparator + sideofScreen) == null) {
                    sideofScreen="POLASidebarNEW.jsp";
                }	
            } 
		}
	}
    String ctx = request.getContextPath() + fileSeparator;
    LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
    ResourceBundleHandler rb = new ResourceBundleHandler(av.getLocale());
    // added by wayne 2008-04-07
   	// Fix bug 3858 (wrong menu displayed when logout then login again)
    // Get system menu and master menu from instances saved in the session.
    // Getting menus from ApVars.getInstance() would not work as menu data
    // is generated and saved in different AppVars instance in different thread.
    Object[] systemMenuArray = AppVars.getInstance().getSystemMenu();
    HashMap map = AppVars.getInstance().getMasterMenu();
  	if (request.getSession().getAttribute("SYSTEM_MENU") != null && request.getSession().getAttribute("MASTER_MENU") != null) {
   	    systemMenuArray = (Object[]) request.getSession().getAttribute("SYSTEM_MENU");
   	    map = (HashMap) request.getSession().getAttribute("MASTER_MENU");
   	    request.getSession().removeAttribute("SYSTEM_MENU");
   	    request.getSession().removeAttribute("MASTER_MENU");
   	}
   	// added by wayne 2008-04-07
    ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
	String masterMenu = "[]";
	String systemMenu = "[]";
	//for multi-user login, systemMenuArray is null. so need to bypass null pointer error. revised by Bear, 2010-3-10
	if (systemMenuArray == null){
	}else{
		//build systemMenu array in javascript
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 1;i<systemMenuArray.length;i++){
		//for (int i = 1;i<2;i++){
			String masterMenuName = systemMenuArray[i].toString().substring(11).trim();
			sb.append("[");
			sb.append("\"" + masterMenuName + "\"");
			sb.append(",");
			sb.append("\""+i+"\"");
			sb.append("]");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		systemMenu = sb.toString();
		
		sb.setLength(0);
		Iterator iter = map.keySet().iterator();
		sb.append("[");
		while (iter.hasNext()){
		//{
			String key = (String)iter.next();
			String index = key.substring(0, key.indexOf("map"));
			String subMenuName = map.get(key).toString().substring(11).trim();
			String keymap = map.get(key).toString().substring(0,11); //ILIFE-8520
			sb.append("[");
			sb.append("\""+index+"\"");
			sb.append(",");
			sb.append("\"" + subMenuName + "\"");
			sb.append(",");
			sb.append("\""+keymap+"\""); //ILIFE-8520
			sb.append(","); //ILIFE-8520
			sb.append("\""+key+"\"");
			sb.append("]");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		masterMenu = sb.toString();
	}
%>


  <HEAD>
  	<%
  		//ILIFE-1948
  		if(AppConfig.ieEmulationEnable){
  	%>
  	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
  	<%}%>
  	<meta http-equiv="pragram" content="no-cache">
  	<meta http-equiv="cache-control" content="no-cache, must-revalidate">
  	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script language='javaScript'>		
		var contextPathName = "<%= request.getContextPath() %>";
    </script>
    	<!--IGroup-937-->

        <SCRIPT language=javascript src="js/jquery.min.js"> <!-- IJTI-1571 -->
        </SCRIPT>
        <!-- SCRIPT language=javascript src="js/menuG4Loaderfs.js"></SCRIPT  -->
         <!--  SCRIPT language=javascript type=text/javascript src="js/menuG4IE5fs.js"></SCRIPT  -->
        <!-- IGROUP-2837 START -->
        <%
        	if(AppVars.isIgnoreButton()){
        %>
       <script language=javascript type="text/javascript">
	        $(document).ready(function() {
				function disableBack() {
					window.history.forward()
				}
				window.onload = disableBack();
				window.onpageshow = function(evt) {
					if (evt.persisted)
						disableBack()
				}
			});
        </script>
        <%
        	}
        %>
        <!-- IGROUP-2837 END --> 
        <%	// To decide whether menus are displayed or not
	boolean isShow = av.isMenuDisplayed();
	if  (isShow){%>
      <script language=javascript type="text/javascript">
        var systemMenu = <%=systemMenu%>;
        var masterMenu = <%=masterMenu%>;
 </SCRIPT>
        <%	}%>
    <TITLE><%=rb.gettingValueFromBundle("DXC Assure Policy")%> </TITLE>
    <link rel="icon" href="<%=ctx%>screenFiles/dxc-titlebar-logo.svg">
  </HEAD>

	<Script>ctx = "<%=ctx%>";</Script>
	<!-- ILIFE-8864 -->
	<FRAMESET rows="50,*" frameborder="no" framespacing="0">
		<FRAME id="topFrame" frameborder="no" src='<%=ctx%>top.jsp' name='topFrame' scrolling="no" noresize="noresize">
    	<FRAMESET name="realContent" cols='<%=(av.getAppConfig().isUiEnrichSubfile()?"0,*":"275,*")%>' frameBorder="0" framespacing="0">
           <FRAME id="frameMenu" frameborder="no"  noresize="noresize" NAME="frameMenu" src='<%=ctx%><%=sideofScreen %>' scrolling="no">
           <FRAMESET name="activeframe" ROWS="*, 0%"  frameBorder="no" framespacing="0">
            	<%if  (request.getParameter("currentdisplaypage") != null) { %>
				<FRAME style="border: 0" SRC='<%=ctx%><%=screenForm%>?currentdisplaypage=<%=request.getParameter("currentdisplaypage")%>' noresize="noresize" frameborder='0' NAME='mainForm'>
				<% }else{   
					//IJTI-1024
					if(null!=screenForm && (screenForm.indexOf("Sr2deForm")>0 || screenForm.indexOf("Sr2g3Form")>0)){ %>
                	<FRAME id="mainForm" style="border: 0" SRC='<%=ctx%><%=screenForm%>' frameborder='0' NAME='mainForm' >
            		<%
						String errorFile = request.getParameter("fileuploaderrorfile");
						String fileUploadErrorMessage = request.getParameter("validationErrorMsg");
						String fileUploadMessage = request.getParameter("fileuploadmessage");
					    //IGROUP-2585 added to Remove hard coding of context in Sr2deForm.jsp file for redirecting to File upload utility 
						String fileUploadUtilityContext = request.getParameter("fileUploadUtilityContext");
						if(fileUploadErrorMessage!=null){
							session.setAttribute("fileUploadErrorMessage", fileUploadErrorMessage);
						}
						if(fileUploadMessage!=null){
							session.setAttribute("fileUploadMessage", fileUploadMessage);
						}
						//IGROUP-2585 added to Remove hard coding of context in Sr2deForm.jsp file for redirecting to File upload utility 
						if(fileUploadUtilityContext!=null){
							session.setAttribute("fileUploadUtilityContext", fileUploadUtilityContext);
						}	
						//IGROUP-2585 ends
						if(errorFile!=null){
							session.setAttribute("errorFile", errorFile);
							String errorFileName = request.getParameter("errorFileName");
							session.setAttribute("errorFileName", errorFileName);
						}
					}else{%>
	       	  			<FRAME id="mainForm" style="border: 0" SRC='<%=ctx%><%=screenForm%>' frameborder='0' NAME='mainForm' >
	       			<%}%>
       			<%}%>
       			<FRAME id="heartbeat" style="border: 0" SRC='<%=ctx%>heartbeat.jsp' NAME='heartbeat'>
			</FRAMESET>
	    </FRAMESET>
    </FRAMESET>
</HTML>