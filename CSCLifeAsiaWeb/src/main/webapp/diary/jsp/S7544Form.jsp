<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7544";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>
<%@ page import="com.csc.util.XSSFilter" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S7544ScreenVars sv = (S7544ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">

	<div class="panel-body">
	
	<div class="row">
	<div class="col-md-3">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Parameters For Printing")%></label>
	
	</div>
	</div>
	
	
	<div class="row">
	<div class="col-md-3">
	<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("Companies")%></label>
		
		
		<% if ((new Byte((sv.company01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company01");
	optionValue = makeDropDownList( mappedItems , sv.company01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company01, fw, longValue, "company01", optionValue) %>
<%}%>
</div></div>

<div class="col-md-3">
<div class="form-group"> 
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;")%></label>
<% if ((new Byte((sv.company02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company02");
	optionValue = makeDropDownList( mappedItems , sv.company02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company02, fw, longValue, "company02", optionValue) %>
<%}%>

</div></div>

<div class="col-md-3">
<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;")%></label>
<% if ((new Byte((sv.company03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company03");
	optionValue = makeDropDownList( mappedItems , sv.company03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company03, fw, longValue, "company03", optionValue) %>
<%}%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;")%></label>
<% if ((new Byte((sv.company04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company04");
	optionValue = makeDropDownList( mappedItems , sv.company04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company04, fw, longValue, "company04", optionValue) %>
<%}%>
</div>
</div>   

</div>

	
	<div class="row">
	<div class="col-md-3">
	<div class="form-group">
	<% if ((new Byte((sv.company05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company05");
	optionValue = makeDropDownList( mappedItems , sv.company05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company05, fw, longValue, "company05", optionValue) %>
<%}%>
	
	
	
</div>
</div>
</div>
	
	<div class="row">
	<div class="col-md-3">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Branches")%></label>
	
	</div>
	
	</div>
	<div class="row">
	<div class="col-md-3" style="width: 200px">
	 
	<div class="form-group">   
	
	
	<% if ((new Byte((sv.branch01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch01");
	optionValue = makeDropDownList( mappedItems , sv.branch01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch01, fw, longValue, "branch01", optionValue) %>  
<%}%>

	</div>
	
	</div>
	
	
	<div class="col-md-3" style="width: 200px ;margin-left: -20px">   
	<div class="form-group">    
	 

<% if ((new Byte((sv.branch02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch02");
	optionValue = makeDropDownList( mappedItems , sv.branch02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch02, fw, longValue, "branch02", optionValue) %>
<%}%>
	</div>
	</div>
	
	<div class="col-md-2" style="width: 200px;margin-left: -20px">
	<div class="form-group">
	

<% if ((new Byte((sv.branch03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch03");
	optionValue = makeDropDownList( mappedItems , sv.branch03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch03, fw, longValue, "branch03", optionValue) %>
<%}%>
	</div>
	
	</div>
	
	<div class="col-md-2" style="width: 200px;margin-left: -20px">
	<div class="form-group">
	

<% if ((new Byte((sv.branch04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch04");
	optionValue = makeDropDownList( mappedItems , sv.branch04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch04, fw, longValue, "branch04", optionValue) %>
<%}%> 
	</div>
	
	</div>
	
	<div class="col-md-2" style="width: 200px;margin-left: -20px">
	<div class="form-group" >
	

<% if ((new Byte((sv.branch05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch05");
	optionValue = makeDropDownList( mappedItems , sv.branch05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>   
	<%=smartHF.getDropDownExt(sv.branch05, fw, longValue, "branch05", optionValue) %>
<%}%>
	
	
</div></div></div>



<div class="row">
	<div class="col-md-3">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
	<%-- 	<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
	 <<%-- %=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%></div> --%>
	<div class="input-group date form_date col-md-8" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			        </div> 
			        </div>
	<div class="col-md-2" style="max-width: 190px">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Run Number")%></label>
		
	<%=smartHF.getHTMLVarExt(fw, sv.diaryRunNumber)%>
	
	</div>
	<div class="col-md-3">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Language")%></label>
		
		<div class="input-group" style="max-width: 190px">
	<% if ((new Byte((sv.language).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"language"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("language");
	optionValue = makeDropDownList( mappedItems , sv.language.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.language.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.language, fw, longValue, "language", optionValue) %>
<%}%>
</div>
	</div>
	<div class="col-md-2">
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
		
	<%=smartHF.getHTMLVarExt(fw, sv.jobq)%> 

	</div>
	
	</div>
	
	
<div class="row">
	<div class="col-md-3">
	<label><%=resourceBundleHandler.gettingValueFromBundle("User Reports")%></label>
	
	
	
	
	
	
	
	</div>
	</div>
	
<div class="row">
	<div class="col-md-5">
	
	<label><%=resourceBundleHandler.gettingValueFromBundle("Use *ALL, *NONE or Report Name(s):")%></label>
	
	
	
	
	
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName01");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName01, fw, longValue, "diaryReportName01", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4">
	<div class="form-group">

<% if ((new Byte((sv.diaryReportName02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName02");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName02, fw, longValue, "diaryReportName02", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName03");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName03, fw, longValue, "diaryReportName03", optionValue) %>
<%}%>
</div>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-4">
	<div class="form-group">

<% if ((new Byte((sv.diaryReportName04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName04");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName04, fw, longValue, "diaryReportName04", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName05");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName05, fw, longValue, "diaryReportName05", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName06");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName06, fw, longValue, "diaryReportName06", optionValue) %>
<%}%>
</div>
	</div>
	</div> 
	
	<div class="row">
	<div class="col-md-4">
	<div class="form-group">

<% if ((new Byte((sv.diaryReportName07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName07");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName07, fw, longValue, "diaryReportName07", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4"> 
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName08");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName08, fw, longValue, "diaryReportName08", optionValue) %>
<%}%>
</div>
	</div>
	
	
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName09");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName09, fw, longValue, "diaryReportName09", optionValue) %>
<%}%>
</div>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-4">
	<div class="form-group">
<% if ((new Byte((sv.diaryReportName10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName10");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName10, fw, longValue, "diaryReportName10", optionValue) %>
<%}%>
	
	
	
</div>
</div>
</div>
	
	<div class="row">
	<div class="col-md-3">
	<label><%=resourceBundleHandler.gettingValueFromBundle("System Reports")%></label>
	
	
	
	
	
	
	
	</div>
	</div>
	
	
	
<div class="row">
	<div class="col-md-3">
	<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("Control Totals Report (Y/N)")%></label>
		<div class="input-group" style="max-width: 125px">
		<%=smartHF.getHTMLVarExt(fw, sv.xopt01)%>
		</div></div></div>
	<div class="col-md-3">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Message Log Report (Y/N)")%></label>
		<div class="input-group" style="max-width: 125px">
	<%=smartHF.getHTMLVarExt(fw, sv.xopt02)%>
	</div></div></div>
	<div class="col-md-2">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Error Report (Y/N)")%></label>
		<div class="input-group" style="max-width: 125px">
	<%=smartHF.getHTMLVarExt(fw, sv.xopt03)%>
	</div></div></div>
	<div class="col-md-4">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Job QueueHeld Entities Report (Y/N)")%></label>
		<div class="input-group" style="max-width: 125px">
	 <%=smartHF.getHTMLVarExt(fw, sv.xopt04)%>
	</div></div></div>
	
	</div>
	
	<div class="row">
	<div class="col-md-3">
	<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("Level")%></label>
		
		<%	
	if ((new Byte((sv.stmtlevel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("S")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Subroutine");
	}
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Thread");
	}
		 
%>


<% 
	if((new Byte((sv.stmtlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.stmtlevel).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='stmtlevel' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(stmtlevel)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.stmtlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.stmtlevel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="S"<% if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("S")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></option>
<option value="T"<% if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("T")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Thread")%></option>


</select>
<% if("red".equals((sv.stmtlevel).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
		</div>
		</div>
		</div>
	
	<%-- <div class="row">
	<div class="col-md-3">
	<div class="form-group">
		
		<%	
	if ((new Byte((sv.stmtlevel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("S")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Subroutine");
	}
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Thread");
	}
		 
%>


<% 
	if((new Byte((sv.stmtlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
		</div>
		</div>
		</div>	 --%>
	
	
	<div class="row">
	<div class="col-md-6">
	<div class="form-group">
		
		<label><%=resourceBundleHandler.gettingValueFromBundle("Fill Parameters and Press Enter to Start Printing")%></label>
		</div>
		</div>
		</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		
		
		
		
		
		
		</div>
		
		
		
	
	
	
	
	</div>

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	








































<!--  <div class='outerDiv'> -->
<%-- <table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%>
<br/>

<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Parameters for Printing"))%>
</td></tr>
<tr style='height:22px;'>
<td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Companies"))%>
<br/>
<% if ((new Byte((sv.company01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company01");
	optionValue = makeDropDownList( mappedItems , sv.company01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company01, fw, longValue, "company01", optionValue) %>
<%}%>

<% if ((new Byte((sv.company02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company02");
	optionValue = makeDropDownList( mappedItems , sv.company02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company02, fw, longValue, "company02", optionValue) %>
<%}%>

<% if ((new Byte((sv.company03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company03");
	optionValue = makeDropDownList( mappedItems , sv.company03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company03, fw, longValue, "company03", optionValue) %>
<%}%>

<% if ((new Byte((sv.company04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company04");
	optionValue = makeDropDownList( mappedItems , sv.company04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company04, fw, longValue, "company04", optionValue) %>
<%}%>

<% if ((new Byte((sv.company05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company05");
	optionValue = makeDropDownList( mappedItems , sv.company05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company05, fw, longValue, "company05", optionValue) %>
<%}%>
</td></tr>
<tr style='height:22px;'><td>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branches"))%>
<br/>
<% if ((new Byte((sv.branch01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch01");
	optionValue = makeDropDownList( mappedItems , sv.branch01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch01, fw, longValue, "branch01", optionValue) %>
<%}%>

<% if ((new Byte((sv.branch02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch02");
	optionValue = makeDropDownList( mappedItems , sv.branch02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch02, fw, longValue, "branch02", optionValue) %>
<%}%>

<% if ((new Byte((sv.branch03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch03");
	optionValue = makeDropDownList( mappedItems , sv.branch03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch03, fw, longValue, "branch03", optionValue) %>
<%}%>

<% if ((new Byte((sv.branch04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch04");
	optionValue = makeDropDownList( mappedItems , sv.branch04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch04, fw, longValue, "branch04", optionValue) %>
<%}%>

<% if ((new Byte((sv.branch05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch05");
	optionValue = makeDropDownList( mappedItems , sv.branch05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch05, fw, longValue, "branch05", optionValue) %>
<%}%>
</td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%>
<br/>
	<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%>
</td>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number"))%>
<br/>
<%=smartHF.getHTMLVarExt(fw, sv.diaryRunNumber)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Language"))%>
<br/>
<% if ((new Byte((sv.language).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"language"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("language");
	optionValue = makeDropDownList( mappedItems , sv.language.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.language.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.language, fw, longValue, "language", optionValue) %>
<%}%>
</td>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Job Queue"))%>
<br/>
<%=smartHF.getHTMLVarExt(fw, sv.jobq)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
</table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("User Reports"))%>
<br/>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Use *ALL, *NONE or Report Name(s):"))%>
</td>
</tr> </table>
<table width='100%'>
<tr style='height:22px;'>
<td>
<% if ((new Byte((sv.diaryReportName01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName01");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName01.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName01, fw, longValue, "diaryReportName01", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName02");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName02.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName02, fw, longValue, "diaryReportName02", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName03");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName03.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName03, fw, longValue, "diaryReportName03", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName04");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName04.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName04, fw, longValue, "diaryReportName04", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName05");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName05.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName05, fw, longValue, "diaryReportName05", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName06");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName06, fw, longValue, "diaryReportName06", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName07");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName07, fw, longValue, "diaryReportName07", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName08");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName08, fw, longValue, "diaryReportName08", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName09");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName09, fw, longValue, "diaryReportName09", optionValue) %>
<%}%>

<% if ((new Byte((sv.diaryReportName10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diaryReportName10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diaryReportName10");
	optionValue = makeDropDownList( mappedItems , sv.diaryReportName10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diaryReportName10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.diaryReportName10, fw, longValue, "diaryReportName10", optionValue) %>
<%}%>

<br/>

<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%>
</td>
</tr> </table>

<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%>
<br/>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Reports"))%>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Control Totals Report (Y/N)"))%>
<br/><%=smartHF.getHTMLVarExt(fw, sv.xopt01)%>
</td>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Message Log Report (Y/N)"))%>
<br/><%=smartHF.getHTMLVarExt(fw, sv.xopt02)%>
</td>
<td width='251'><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Error Report (Y/N)"))%>
<br/><%=smartHF.getHTMLVarExt(fw, sv.xopt03)%></td>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Held Entities Report (Y/N)"))%>
<br/><%=smartHF.getHTMLVarExt(fw, sv.xopt04)%>
</td>
</tr>

<tr></tr>
<tr style='height:22px;'>
<td width='251'>
 <%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level"))%>
<br/>
<%	
	if ((new Byte((sv.stmtlevel).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("S")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Subroutine");
	}
	if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("T")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Thread");
	}
		 
%>


<% 
	if((new Byte((sv.stmtlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.stmtlevel).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='stmtlevel' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(stmtlevel)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.stmtlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.stmtlevel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>

<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="S"<% if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("S")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></option>
<option value="T"<% if(((sv.stmtlevel.getFormData()).toString()).trim().equalsIgnoreCase("T")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Thread")%></option>


</select>
<% if("red".equals((sv.stmtlevel).getColor())){
%>
</div>
<%
} 
%>

<%
}longValue = null;} 
%>
</td>
</tr>
</table>

<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fill Parameters and Press Enter to Start Printing"))%>
</td>
<td></td>
<td></td>
</tr> </table>
<br/>
</div> --%>
 
<%@ include file="/POLACommon2NEW.jsp"%>

