<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7511";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7511ScreenVars sv = (S7511ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table:"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item:"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Defaults for Start of Diary System"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number of Threads"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.nofThreads)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Types"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.dryenttp01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp01" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp01");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp01.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp01.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp01, fw, longValue, "dryenttp01", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.dryenttp02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp02" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp02");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp02.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp02.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp02, fw, longValue, "dryenttp02", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.dryenttp03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp03" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp03");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp03.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp03.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp03, fw, longValue, "dryenttp03", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-md-offset-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.dryenttp04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp04" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp04");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp04.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp04.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp04, fw, longValue, "dryenttp04", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.dryenttp05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp05" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp05");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp05.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp05.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp05, fw, longValue, "dryenttp05", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commit Point per Thread"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.cmtpnt)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Monitor Parameters"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 180px;">
						<%
							if ((new Byte((sv.drymonparm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "drymonparm" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("drymonparm");
								optionValue = makeDropDownList(mappedItems, sv.drymonparm.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.drymonparm.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.drymonparm, fw, longValue, "drymonparm", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

