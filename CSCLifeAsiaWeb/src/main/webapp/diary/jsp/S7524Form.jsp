<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7524";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7524ScreenVars sv = (S7524ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 48;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 72;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 132;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 564;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7524screensfl");
			GeneralTable sfl1 = fw.getTable("s7524screensfl");
			S7524screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = sfl.count() * 27;
			} else {
				height = 118;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7524' width='100%'>
							<thead>
								<tr class='info'>
									<th>Type</th>
									<th>Number</th>
									<th>Date</th>
									<th>Time</th>
									<th>STTIME</th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7524screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7524screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<%=smartHF.getHTMLHiddenFieldSFL(sv.dryhldent, "dryhldent", "s7524screensfl", count)%>


									<td style="width:<%=tblColumnWidth[0]%>px;"
										<%if (!(((BaseScreenData) sv.sel) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLFieldSFL(sv.sel, "sel", "s7524screensfl", count, fw, hyperLinkFlag)%>
									</td>





									<td style="width:<%=tblColumnWidth[1]%>px;"
										<%if (!(((BaseScreenData) sv.diaryEntityType) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLAnchorFieldSFL(sv.diaryEntityType, "diaryEntityType", "s7524screensfl", count,
						hyperLinkFlag)%></td>



									<td style="width:<%=tblColumnWidth[2]%>px;"
										<%if (!(((BaseScreenData) sv.diaryEntity) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.diaryEntity, "diaryEntity", "s7524screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[3]%>px;"
										<%if (!(((BaseScreenData) sv.nxtprcdateDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLDateFieldSFL(sv.nxtprcdate, "nxtprcdateDisp", "s7524screensfl", count, fw,
						localeimageFolder)%></td>



									<td style="width:<%=tblColumnWidth[4]%>px;"
										<%if (!(((BaseScreenData) sv.sttime) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.sttime, "sttime", "s7524screensfl", count)%>
									</td>



								</tr>
								<%
									count = count + 1;
										S7524screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-s7524').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

