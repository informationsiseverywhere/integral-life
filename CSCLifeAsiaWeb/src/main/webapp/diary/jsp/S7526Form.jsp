<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7526";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7526ScreenVars sv = (S7526ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Header Details"))%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company    "))%></label>
					<table><tr><td>
					<!-- <div class="input-group three-controller" style="width: 150px;"> -->
						<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
						</td><td style= "padding-left:1px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc01, 1)%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<table><tr><td>
					<!-- <div class="input-group three-controller" style="width: 150px;"> -->
						<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
						</td><td style= "padding-left:1px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc, 1)%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Type  "))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc02, 1)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Number"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntity)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Processing Date"))%></label>
					<%
						if ((new Byte((sv.nxtprcdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.nxtprcdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.nxtprcdateDisp, (sv.nxtprcdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Processing Time"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.entime)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Transaction Details"))%></label>
			</div>
		</div>

		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 36;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 96;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 384;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 120;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = 72;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = 60;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7526screensfl");
			GeneralTable sfl1 = fw.getTable("s7526screensfl");
			S7526screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = sfl.count() * 27;
			} else {
				height = 118;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7526' width='100%'>
							<thead>
								<tr class='info'>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sel"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Process"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Name"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prc.Date"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prc.Time"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Seq No"))%></th>
									<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Hold"))%></th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7526screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7526screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0]%>px;"
										<%if ((sv.sel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="center" <%} else {%> align="center" <%}%>>
										<%
											if ((new Byte((sv.sel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7526screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7526screensfl.sel_R<%=count%>'
										id='s7526screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7526screensfl.sel_R<%=count%>')"
										class="radio" disabled="disabled" /> <%
 	} else {
 %> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7526screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7526screensfl.sel_R<%=count%>'
										id='s7526screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7526screensfl.sel_R<%=count%>')"
										class="radio" /> <%
 	}
 %>
									
									<td style="width:<%=tblColumnWidth[1]%>px;"
										<%if (!(((BaseScreenData) sv.diaryProcessCode) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.diaryProcessCode, "diaryProcessCode", "s7526screensfl",
						count)%></td>



									<td style="width:<%=tblColumnWidth[2]%>px;"
										<%if (!(((BaseScreenData) sv.literal) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.literal, "literal", "s7526screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[3]%>px;"
										<%if (!(((BaseScreenData) sv.effdateDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLDateFieldSFL(sv.effdateDisp, "effdateDisp", "s7526screensfl", count, fw,
						localeimageFolder)%></td>



									<td style="width:<%=tblColumnWidth[4]%>px;"
										<%if (!(((BaseScreenData) sv.sttime) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.sttime, "sttime", "s7526screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[5]%>px;"
										<%if (!(((BaseScreenData) sv.procSeqNo) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.procSeqNo, "procSeqNo", "s7526screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[6]%>px;"
										<%if (!(((BaseScreenData) sv.suprflg) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.suprflg, "suprflg", "s7526screensfl", count)%>
									</td>



								</tr>
								<%
									count = count + 1;
										S7526screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="btn-group">
				<div class="sectionbutton">
					<%
						if ("A".equalsIgnoreCase(sv.action.trim()) || "C".equalsIgnoreCase(sv.action.trim())) {
					%>
					<p style="font-size: 12px; font-weight: bold;">
						<a class="btn btn-info" href="#"
							onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
						<a class="btn btn-danger" href="#"
							onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
						<a class="btn btn-success" href="#"
							onClick="JavaScript:perFormOperation(3)"><%=resourceBundleHandler.gettingValueFromBundle("Hold")%></a>
						<a class="btn btn-danger" href="#"
							onClick="JavaScript:perFormOperation(6)"><%=resourceBundleHandler.gettingValueFromBundle("Release")%></a>
					</p>

					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<BODY style="background-color: #f8f8f8;">
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span>
					<%
										String sidebartext = "";
									%> 
							<%-- <ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<%
 	sidebartext = "";
 %>
									<div id='null'>
										<%
											if (sv.optdscx01.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx01.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx01"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx01' id='optdscx01' type='hidden'
											value="<%=sv.optdscx01.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx02.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx02.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx02"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx02' id='optdscx02' type='hidden'
											value="<%=sv.optdscx02.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx03.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx03.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx03"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx03' id='optdscx03' type='hidden'
											value="<%=sv.optdscx03.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx04.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx04.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx04"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx04' id='optdscx04' type='hidden'
											value="<%=sv.optdscx04.getFormData()%>">
									</div>
								</li>
							</ul> --%>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>

<script>
	$(document).ready(function() {
		$('#dataTables-s7526').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

