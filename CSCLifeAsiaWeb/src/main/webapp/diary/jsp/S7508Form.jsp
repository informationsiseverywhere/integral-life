<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7508";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7508ScreenVars sv = (S7508ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company:"))%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table:"))%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item:"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label style="padding-left: 18px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity"))%></label>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Process Code"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("1"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp01" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp01");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp01.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp01.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp01, fw, longValue, "dryenttp01", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces01)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("2"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp02" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp02");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp02.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp02.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp02, fw, longValue, "dryenttp02", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces02)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("3"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp03" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp03");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp03.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp03.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp03, fw, longValue, "dryenttp03", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces03)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("4"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp04" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp04");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp04.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp04.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp04, fw, longValue, "dryenttp04", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces04)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("5"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp05" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp05");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp05.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp05.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp05, fw, longValue, "dryenttp05", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces05)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("6"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp06" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp06");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp06.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp06.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp06, fw, longValue, "dryenttp06", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces06)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("7"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp07" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp07");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp07.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp07.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp07, fw, longValue, "dryenttp07", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces07)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("8"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp08" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp08");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp08.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp08.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp08, fw, longValue, "dryenttp08", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces08)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("9"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp09" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp09");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp09.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp09.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp09, fw, longValue, "dryenttp09", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces09)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("10"))%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.dryenttp10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp10" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("dryenttp10");
										optionValue = makeDropDownList(mappedItems, sv.dryenttp10.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.dryenttp10.getFormData()).toString().trim());
										if (null == longValue) {
											longValue = "";
										}
								%> <%=smartHF.getDropDownExt(sv.dryenttp10, fw, longValue, "dryenttp10", optionValue)%>
								<%
									}
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.proces10)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

