<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7543";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S7543ScreenVars sv = (S7543ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind01.isOn()) {
	sv.cntno.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind01.isOn()) {
	sv.drytxtdesc.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>


<div class="panel panel-default">

    	<div class="panel-body">     
			
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Status and Parameters")%></label>
				    		
				    </div>
				  </div>
				  
			<div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Controller Number:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.cntno)%>
				    </div>
				    </div>
				      <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Status:")%></label>
				             	<%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%>
				    </div>
				    </div>
				  </div>	
				  
			
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Level:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%>
				    </div>
				    
				      <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
				             <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
	                         <%=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%>
	                         </div>
				    </div>
				  </div>	
				  
				  <div class="row">	
			    	<div class="col-md-4"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Active Threads:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.threadno)%>
				    </div></div>
				    
				      <div class="col-md-1"> </div>
				    <div class="col-md-2"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Companies:")%></label>
				    	<div class="input-group">	
				            <%=smartHF.getHTMLVarReadOnly(fw, sv.company01)%>
                            <%=smartHF.getHTMLVarReadOnly(fw, sv.company02)%>
                            <%=smartHF.getHTMLVarReadOnly(fw, sv.company03)%>
                            <%=smartHF.getHTMLVarReadOnly(fw, sv.company04)%>
                            <%=smartHF.getHTMLVarReadOnly(fw, sv.company05)%>
				    </div>
				  </div>	    
				</div>
				</div>  
				
				<div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Language:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.language)%>
				    </div>
				    </div>
				      <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Branches:")%></label>
				    		<div class="input-group">	
				             	<%=smartHF.getHTMLVarReadOnly(fw, sv.branch01)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.branch02)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.branch03)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.branch04)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.branch05)%>
				    </div>
				    </div>
				  </div>
				</div>  	
				
				<div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Commit Point:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.cmtpnta)%>
				    </div>
				    </div>
				      <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Entity Types:")%></label>
				    		<div class="input-group">	
				             	<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp01)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp02)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp03)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp04)%>
                                <%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp05)%>
				    </div>
				    </div>
				  </div>
				</div>
				
				<div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Run Number:")%></label>
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%>
				    </div>
				    </div>
				      <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Entity Range:")%></label>
				    	     <table>
				    	     <tr>
				    	     <td>
				    	     <%=smartHF.getHTMLVarReadOnly(fw, sv.entfrom)%>
				    	     </td>
				    	     <td>
				    	     <b>&nbsp;to</b>
				    	     </td>
				    	     
				    	      <td>
				    	     <b>&nbsp;to&nbsp;</b>
				    	     </td>
				    	     
				    	     <td>
				    	     <%=smartHF.getHTMLVarReadOnly(fw, sv.entto)%>
				    	     </td>
				    	     
				    	     
				    	     
				    	     </tr>
				    	     
				    	     </table>
				             	
				    
				    </div>
				  </div>
				</div>
				
				<div class="row">	
			    	<div class="col-md-2" style="min-width:170px;"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Monitor Parameters:")%></label>
				    		<div class="input-group">
				    		<%=smartHF.getHTMLVarReadOnly(fw, sv.drymonprm)%>
                           <%=smartHF.getHTMLVarReadOnly(fw, sv.drytxtdesc)%>
				    </div>
				    </div>
				    </div>
				    
				  </div>
				  
				  <div class="row">	
			    	<div class="col-md-8"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("To (Re)Start a Controller Press Continue")%></label>
				    		
				    </div>
				  </div>
				  
				  
				   <div class="row">	
			    	<div class="col-md-2"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
				    		<div class="input-group date form_date col-md-12 "  style="min-width:150px;" data-date="" data-date-format="dd/MM/yyyy" data-link-field="dryeffdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.dryeffdateDisp,(sv.dryeffdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			        </div> 
				    		<%-- <%=smartHF.getRichTextDateInput(fw, sv.dryeffdateDisp)%>
	                        <%=smartHF.getHTMLCalNSVarExt(fw, sv.dryeffdateDisp)%> --%>
				    </div>
				   </div> 
				   
				   <div class="col-md-2"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Companies:")%></label>
				    	<% if ((new Byte((sv.company06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company06");
	optionValue = makeDropDownList( mappedItems , sv.company06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company06, fw, longValue, "company06", optionValue) %>
<%}%>	
				    
				  </div>
				 </div>
			  </div>	  
			  
			   <div class="row">	
			    	<!-- <div class="col-md-2"> 
			    
				   </div>  -->
				   
				   <div class="col-md-4"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		
				    	<% if ((new Byte((sv.company07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company07");
	optionValue = makeDropDownList( mappedItems , sv.company07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company07, fw, longValue, "company07", optionValue) %>
<%}%>	
				    
				  </div>
				 </div>
			  </div>	 
			  
			  
			   <div class="row">	
			    	<!-- <div class="col-md-2"> 
			    
				   </div>  -->
				   
				   <div class="col-md-4"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		
				    	<% if ((new Byte((sv.company08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company08");
	optionValue = makeDropDownList( mappedItems , sv.company08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company08, fw, longValue, "company08", optionValue) %>
<%}%>	
				    
				  </div>
				 </div>
			  </div> 
			  
			   <div class="row">	
			    	<!-- <div class="col-md-2"> 
			    
				   </div>  -->
				   
				   <div class="col-md-4"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		
				    	<% if ((new Byte((sv.company09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company09");
	optionValue = makeDropDownList( mappedItems , sv.company09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company09, fw, longValue, "company09", optionValue) %>
<%}%>	
				    
				  </div>
				 </div>
			  </div> 
			  
			   <div class="row">	
			    	<!-- <div class="col-md-2"> 
			    
				   </div>  -->
				   
				   <div class="col-md-4"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		
<% if ((new Byte((sv.company10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company10");
	optionValue = makeDropDownList( mappedItems , sv.company10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company10, fw, longValue, "company10", optionValue) %>
<%}%>    
				  </div>
				 </div>
			  </div> 
				  
			
			<div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Branches:")%></label>
				    		<% if ((new Byte((sv.branch06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch06");
	optionValue = makeDropDownList( mappedItems , sv.branch06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch06, fw, longValue, "branch06", optionValue) %>
<%}%>
				    		
				    </div>
				   </div> 
				   
				   <div class="col-md-1"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Entity Types:")%></label>
				    	<% if ((new Byte((sv.dryenttp06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp06");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp06, fw, longValue, "dryenttp06", optionValue) %>
<%}%>
				    
				  </div>
				 </div>
			  </div>	 	  
			  
			  <div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    		
				    	<% if ((new Byte((sv.branch07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch07");
	optionValue = makeDropDownList( mappedItems , sv.branch07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch07, fw, longValue, "branch07", optionValue) %>
<%}%>
				    		
				    </div>
				   </div> 
				   
				   <div class="col-md-1"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    	
				    <% if ((new Byte((sv.dryenttp07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp07");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp07, fw, longValue, "dryenttp07", optionValue) %>
<%}%>
				    
				  </div>
				 </div>
			  </div>	 	  
			  
			    <div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    	<% if ((new Byte((sv.branch08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch08");
	optionValue = makeDropDownList( mappedItems , sv.branch08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch08, fw, longValue, "branch08", optionValue) %>
<%}%>
				    		
				    </div>
				   </div> 
				   
				   <div class="col-md-1"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    	
				  <% if ((new Byte((sv.dryenttp08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp08");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp08, fw, longValue, "dryenttp08", optionValue) %>
<%}%>
				    
				  </div>
				 </div>
			  </div>	 	  
				  
						    <div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    	<% if ((new Byte((sv.branch09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch09");
	optionValue = makeDropDownList( mappedItems , sv.branch09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch09, fw, longValue, "branch09", optionValue) %>
<%}%>
				    		
				    </div>
				   </div> 
				   
				   <div class="col-md-1"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    	
				 <% if ((new Byte((sv.dryenttp09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp09");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp09, fw, longValue, "dryenttp09", optionValue) %>
<%}%>
				    
				  </div>
				 </div>
			  </div>		  
			  
			   <div class="row">	
			    	<div class="col-md-3"> 
			    	<div class="form-group">
				    	<% if ((new Byte((sv.branch10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch10");
	optionValue = makeDropDownList( mappedItems , sv.branch10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch10, fw, longValue, "branch10", optionValue) %>
<%}%>
				    		
				    </div>
				   </div> 
				   
				   <div class="col-md-1"> </div>
				    <div class="col-md-3"> 
				    <div class="form-group">
				    	
				<% if ((new Byte((sv.dryenttp10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp10");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp10, fw, longValue, "dryenttp10", optionValue) %>
<%}%>
				    
				  </div>
				 </div>
			  </div>		  
				
				 <div class="row">
				 <div class="col-md-3"> 
				    <div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Entity Range:")%></label>
				    	     <table>
				    	     <tr>
				    	     <td>
				    	     <%=smartHF.getHTMLVarReadOnly(fw, sv.entityfrom)%>
				    	     </td>
				    	     <td>
				    	     <b>&nbsp;to</b>
				    	     </td>
				    	     
				    	      <td>
				    	     <b>&nbsp;to&nbsp;</b>
				    	     </td>
				    	     
				    	     <td>
				    	     <%=smartHF.getHTMLVarReadOnly(fw, sv.entityto)%>
				    	     </td>
				    	     
				    	     
				    	     
				    	     </tr>
				    	     
				    	     </table>
				             	
				    </div>
				    </div>
				  </div>  
				  
			</div>	    		
</div>












<%-- <div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status and Parameters"))%>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("<br/>"))%>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Controller Number    :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.cntno)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status    :"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("<br/>"))%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level          :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%>
<br/>
	<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number of Active Threads:"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.threadno)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Companies   :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company01)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company02)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company03)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company04)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company05)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Language        :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.language)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branches   :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch01)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch02)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch03)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch04)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch05)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commit Point      :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.cmtpnta)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Types :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp01)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp02)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp03)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp04)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp05)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number       :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Range :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.entfrom)%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp;"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.entto)%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Monitor Parameters   :"))%>
<br/>
<%=smartHF.getHTMLVarReadOnly(fw, sv.drymonprm)%>
<%=smartHF.getHTMLVarReadOnly(fw, sv.drytxtdesc)%>
</td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To (Re)Start a Controller Press Continue"))%>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("<br/>"))%>
</td><!-- END TD FOR ROW 4,7 etc --> 
<td width='251'></td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%>
<br/>
	<%=smartHF.getRichTextDateInput(fw, sv.dryeffdateDisp)%>
	<%=smartHF.getHTMLCalNSVarExt(fw, sv.dryeffdateDisp)%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("<br/>"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Companies   :"))%>
<% if ((new Byte((sv.company06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company06");
	optionValue = makeDropDownList( mappedItems , sv.company06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company06, fw, longValue, "company06", optionValue) %>
<%}%>
<% if ((new Byte((sv.company07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company07");
	optionValue = makeDropDownList( mappedItems , sv.company07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company07, fw, longValue, "company07", optionValue) %>
<%}%>
<% if ((new Byte((sv.company08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company08");
	optionValue = makeDropDownList( mappedItems , sv.company08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company08, fw, longValue, "company08", optionValue) %>
<%}%>
<% if ((new Byte((sv.company09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company09");
	optionValue = makeDropDownList( mappedItems , sv.company09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company09, fw, longValue, "company09", optionValue) %>
<%}%>
<% if ((new Byte((sv.company10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"company10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("company10");
	optionValue = makeDropDownList( mappedItems , sv.company10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.company10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.company10, fw, longValue, "company10", optionValue) %>
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branches   :"))%>
<br/>
<% if ((new Byte((sv.branch06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch06");
	optionValue = makeDropDownList( mappedItems , sv.branch06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch06, fw, longValue, "branch06", optionValue) %>
<%}%>
<% if ((new Byte((sv.branch07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch07");
	optionValue = makeDropDownList( mappedItems , sv.branch07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch07, fw, longValue, "branch07", optionValue) %>
<%}%>
<% if ((new Byte((sv.branch08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch08");
	optionValue = makeDropDownList( mappedItems , sv.branch08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch08, fw, longValue, "branch08", optionValue) %>
<%}%>
<% if ((new Byte((sv.branch09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch09");
	optionValue = makeDropDownList( mappedItems , sv.branch09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch09, fw, longValue, "branch09", optionValue) %>
<%}%>
<% if ((new Byte((sv.branch10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"branch10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("branch10");
	optionValue = makeDropDownList( mappedItems , sv.branch10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.branch10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.branch10, fw, longValue, "branch10", optionValue) %>
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Types :"))%>
<br/>
<% if ((new Byte((sv.dryenttp06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp06");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp06.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp06, fw, longValue, "dryenttp06", optionValue) %>
<%}%>
<% if ((new Byte((sv.dryenttp07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp07");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp07.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp07, fw, longValue, "dryenttp07", optionValue) %>
<%}%>
<% if ((new Byte((sv.dryenttp08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp08");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp08.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp08, fw, longValue, "dryenttp08", optionValue) %>
<%}%>
<% if ((new Byte((sv.dryenttp09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp09");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp09.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp09, fw, longValue, "dryenttp09", optionValue) %>
<%}%>
<% if ((new Byte((sv.dryenttp10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dryenttp10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dryenttp10");
	optionValue = makeDropDownList( mappedItems , sv.dryenttp10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dryenttp10.getFormData()).toString().trim());
	if (null == longValue){ longValue = ""; }
%>
	<%=smartHF.getDropDownExt(sv.dryenttp10, fw, longValue, "dryenttp10", optionValue) %>
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Range :"))%>
<br/>
<%=smartHF.getHTMLVarExt(fw, sv.entityfrom)%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp;"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%>
<%=smartHF.getHTMLVarExt(fw, sv.entityto)%>
</td>
<td></td>
</tr> </table>
<br/>
</div> --%>
 
<%@ include file="/POLACommon2NEW.jsp"%>

