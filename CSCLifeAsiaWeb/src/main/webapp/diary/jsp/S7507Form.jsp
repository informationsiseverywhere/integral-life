<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7507";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7507ScreenVars sv = (S7507ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company:"))%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table:"))%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item:"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Subroutine to Print Report"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Override Parameter"))%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Override Value"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm01)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue01)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm02)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue02)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm03)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue03)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm04)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue04)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm05)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue05)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm06)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue06)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm07)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue07)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm08)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue08)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm09)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue09)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm10)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue10)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm11)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue11)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm12)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue12)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm13)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue13)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrparm14)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 230px;"><%=smartHF.getHTMLVarExt(fw, sv.ovrvalue14)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

