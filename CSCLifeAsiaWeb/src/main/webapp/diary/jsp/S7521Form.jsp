<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7545";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7521ScreenVars sv = (S7521ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Type"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.shortds)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity "))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntity)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.endate)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.diaryRunNumber)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Date"))%></label>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Time"))%></label>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.entime)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Subroutine"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.subrname)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Message Type "))%></label>
					<div class="input-group three-controller" style="width: 170px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.logtype)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #5bc0de;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Error Message"))%></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div><%=sv.logmes01%></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<div><%=sv.logmes02%></div>
						</div>
					</div>
				</div>
				<br />
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

