<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7503";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7503ScreenVars sv = (S7503ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Parameters"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("01"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam01, (sv.systemParam01.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam01')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("02"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam02, (sv.systemParam02.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam02')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("03"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam03, (sv.systemParam03.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam03')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("04"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam04, (sv.systemParam04.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam04')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("05"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam05, (sv.systemParam05.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam05')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("06"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam06, (sv.systemParam06.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam06')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("07"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam07, (sv.systemParam07.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam07')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("08"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam08, (sv.systemParam08.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam08')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("09"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam09, (sv.systemParam09.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam09')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("10"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam10, (sv.systemParam10.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam10')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("11"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam11, (sv.systemParam11.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam11')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("12"))%></label>
							</td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam12, (sv.systemParam12.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam12')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("13"))%></label>

							</td>
							<td><div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam13, (sv.systemParam13.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam13')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("14"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam14, (sv.systemParam14.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam14')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("15"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam15, (sv.systemParam15.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam15')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("16"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam16, (sv.systemParam16.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam16')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("17"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam17, (sv.systemParam17.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam17')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("18"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam18, (sv.systemParam18.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam18')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("19"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam19, (sv.systemParam19.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam19')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("20"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam20, (sv.systemParam20.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam20')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("21"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam21, (sv.systemParam21.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam21')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("22"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam22, (sv.systemParam22.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam22')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("23"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam23, (sv.systemParam23.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam23')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("24"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam24, (sv.systemParam24.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam24')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="padding-top: 12px; padding-right: 10px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("25"))%></label></td>
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.systemParam25, (sv.systemParam25.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('systemParam25')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

