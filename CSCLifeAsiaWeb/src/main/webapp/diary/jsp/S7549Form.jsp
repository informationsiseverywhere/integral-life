<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7549";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>
<%@ page import="com.csc.util.XSSFilter" %>
<%
	S7549ScreenVars sv = (S7549ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Date"))%></label>
					<%
						if ((new Byte((sv.startDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.startDateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.startDateDisp, (sv.startDateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Time"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.zapptime01)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Date"))%></label>
					<%
						if ((new Byte((sv.dateendDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.dateendDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.dateendDisp, (sv.dateendDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Time"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.zapptime02)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Types"))%></label>
					<table>
						<tr class="input-group three-controller">
							<td>
								<%
									if ((new Byte((sv.shrtdesc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.shrtdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc01.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc01.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.shrtdesc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.shrtdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc02.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc02.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.shrtdesc03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.shrtdesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc03.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc03.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.shrtdesc04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.shrtdesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc04.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc04.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.shrtdesc05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.shrtdesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc05.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.shrtdesc05.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Entity Range")%></label>
					<table>
						<tr>
							<td><input name='entityfrom' type='text'
								<%formatValue = (sv.entityfrom.getFormData()).toString();%>
								value='<%=XSSFilter.escapeHtml(formatValue)%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>
								size='<%=sv.entityfrom.getLength()%>'
								maxLength='<%=sv.entityfrom.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(entityfrom)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.entityfrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.entityfrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.entityfrom).getColor() == null ? "input_cell"
						: (sv.entityfrom).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>></td>
							<td>&nbsp;&nbsp;</td>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td><input name='entityto' type='text'
								<%formatValue = (sv.entityto.getFormData()).toString();%>
								value='<%=XSSFilter.escapeHtml(formatValue)%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>
								size='<%=sv.entityto.getLength()%>'
								maxLength='<%=sv.entityto.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(entityto)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.entityto).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.entityto).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.entityto).getColor() == null ? "input_cell"
						: (sv.entityto).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>></td>

						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
