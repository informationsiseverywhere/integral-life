

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7523";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>
<%@ page import="com.csc.util.XSSFilter" %>
<%
	S7523ScreenVars sv = (S7523ScreenVars) fw.getVariables();
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Header Details ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Company        ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Branch ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Entity Type    ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Entity Number ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Next Processing Date ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Next Processing Time ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Entity for Processing ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "OR");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Roll Forward to Date and Time");
%>

<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Transaction Details ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Process");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Name");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prc.Date");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prc.Time");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Seq.no");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind04.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.dryenddateDisp.setReverse(BaseScreenData.REVERSED);
			sv.dryenddateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.dryenddateDisp.setInvisibility(BaseScreenData.INVISIBLE);
			sv.dryenddateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dryenddateDisp.setHighLight(BaseScreenData.BOLD);
		}

		if (appVars.ind02.isOn()) {
			sv.dryendtime.setReverse(BaseScreenData.REVERSED);
			sv.dryendtime.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.dryendtime.setInvisibility(BaseScreenData.INVISIBLE);
			sv.dryendtime.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.dryendtime.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.releaseIndicator.setReverse(BaseScreenData.REVERSED);
			sv.releaseIndicator.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.releaseIndicator.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind03.isOn()) {
			sv.releaseIndicator.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText2).replace("absolute", "relative")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText7).replace("absolute", "relative")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.diaryEntityCompany).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.diaryEntityCompany.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityCompany.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityCompany.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>





						<%
							if ((new Byte((sv.shortdesc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.shortdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.shortdesc01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.shortdesc01.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-md-offset-3">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText8).replace("absolute", "relative")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.diaryEntityBranch).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.diaryEntityBranch.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityBranch.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityBranch.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>





						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText3).replace("absolute", "relative")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.diaryEntityType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.diaryEntityType.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityType.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntityType.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>





						<%
							if ((new Byte((sv.shortdesc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.shortdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.shortdesc02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.shortdesc02.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText4).replace("absolute", "relative")%></label>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.diaryEntity).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.diaryEntity.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntity.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.diaryEntity.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText5).replace("absolute", "relative")%></label>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.nxtprcdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.nxtprcdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.nxtprcdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.nxtprcdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText6).replace("absolute", "relative")%></label>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.entime).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.entime.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.entime.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.entime.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
 	if ((new Byte((sv.txf).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (!((sv.txf.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.txf.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.txf.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
						<div style='height: 10px'
							; class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "label_txt" : "label_txt"%>'>
							<%=formatValue%>
						</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
					</label> <label> <%=smartHF.getLit(0, 0, generatedText19).replace("absolute", "relative")%>
					</label>
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.releaseIndicator, (sv.releaseIndicator.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<label><%=smartHF.getLit(0, 0, generatedText14).replace("absolute", "relative")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.dryenddateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| fw.getVariables().isScreenProtected()) {
								%>
								<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.dryenddateDisp)%></div>
								<%
									} else {
								%>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.dryenddateDisp, (sv.dryenddateDisp.getLength()))%>
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-calendar"></span>
									</span>
								</div> <%
 	}
 %>
							</td>
							<td><%=smartHF.getRichText(0, 0, fw, sv.dryendtime, (sv.dryendtime.getLength() + 1),
					COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL).replace("absolute", "relative")%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	StringData newLabel2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
 			"Transaction Details");
 %> <%=smartHF.getLit(0, 0, newLabel2).replace("absolute", "relative")%></label>
				</div>
			</div>
		</div>
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[6];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Select").length()) * 12;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.diaryProcessCode.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.diaryProcessCode.getFormData()).length() * 12;
			}

			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.literal.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 13;
			} else {
				calculatedValue = (sv.literal.getFormData()).length() * 13;
			}

			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.effdateDisp.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 10;
			} else {
				calculatedValue = (sv.effdateDisp.getFormData()).length() * 10;
			}

			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.sttime.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 10;
			} else {
				calculatedValue = (sv.sttime.getFormData()).length() * 10;
			}

			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;
			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.procSeqNo.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 11;
			} else {
				calculatedValue = (sv.procSeqNo.getFormData()).length() * 11;
			}

			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			if (false) {
				totalTblWidth -= tblColumnWidth[0];
			}
			if (totalTblWidth > 730) {
				totalTblWidth = 750;
			}
			arraySize = tblColumnWidth.length;
		%>
		<%
			GeneralTable sfl = fw.getTable("s7523screensfl");
			GeneralTable sfl1 = fw.getTable("s7523screensfl");
			S7523screensfl.set1stScreenRow(sfl, appVars, sv);
			int height = 260;
		%>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7523' width='100%'>
							<thead>
								<tr class='info'>
									<%
										if (false) {
									%>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
									<%
										}
									%>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Process")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Name")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Prc. Date")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Prc. Time")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Seq. No")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7523screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7523screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<%
										if (false) {
									%>
									<td style="width:<%=tblColumnWidth[0]%>px;" align="center">
										<input type='checkbox' onclick='setValues(<%=count%>)'>
										<input name='<%="chk_R" + count%>' id='<%="chk_R" + count%>'
										type='hidden' value="">
									</td>
									<%
										}
									%>
									<td style="width:<%=tblColumnWidth[1]%>px;"
										<%if (!(((BaseScreenData) sv.diaryProcessCode) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.diaryProcessCode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0
														|| (new Byte((sv.diaryProcessCode).getEnabled()))
																.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
													hyperLinkFlag = false;
												}
										%> <%
 	if ((new Byte((sv.diaryProcessCode).getInvisible()))
 				.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	longValue = sv.diaryProcessCode.getFormData();
 %>
										<div id="s7523screensfl.diaryProcessCode_R<%=count%>"
											name="s7523screensfl.diaryProcessCode_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>
									<td style="width:<%=tblColumnWidth[2]%>px;"
										<%if (!(((BaseScreenData) sv.literal) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.literal).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.literal.getFormData();
 %>
										<div id="s7523screensfl.literal_R<%=count%>"
											name="s7523screensfl.literal_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>
									<td style="width:<%=tblColumnWidth[3]%>px;"
										<%if (!(((BaseScreenData) sv.effdateDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.effdateDisp.getFormData();
 %>
										<div id="s7523screensfl.effdateDisp_R<%=count%>"
											name="s7523screensfl.effdateDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>
									<td style="width:<%=tblColumnWidth[4]%>px;"
										<%if (!(((BaseScreenData) sv.sttime) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.sttime).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.sttime.getFormData();
 %>
										<div id="s7523screensfl.sttime_R<%=count%>"
											name="s7523screensfl.sttime_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>
									<td style="width:<%=tblColumnWidth[5]%>px;"
										<%if (!(((BaseScreenData) sv.procSeqNo) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.procSeqNo).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.procSeqNo.getFormData();
 %>
										<div id="s7523screensfl.procSeqNo_R<%=count%>"
											name="s7523screensfl.procSeqNo_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>

								</tr>

								<%
									count = count + 1;
										S7523screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!--  -->
		<div style='position:relative; top:<%=height - 20%>; left:2px'>
			<table>
				<tr style='height: 22px;'>
					<td>
						<%
							if ((sv.optdsc01) != null) {

								String token = ((sv.optdsc01.toString()).indexOf('=') >= 0) ? "=" : "-";
								String[] strControls = (sv.optdsc01.toString()).split(token);
								if (strControls != null && strControls.length > 1) {
									String strKey = strControls[0];
									String strValue = strControls[1];
						%>
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a href="#" onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							</p>
						</div> <%
 	}
 	}
 %>


					</td>
					<td>
						<%
							if ((sv.optdsc02) != null) {

								String token = ((sv.optdsc01.toString()).indexOf('=') >= 0) ? "=" : "-";
								String[] strControls = (sv.optdsc02.toString()).split(token);
								if (strControls != null && strControls.length > 1) {
									String strKey = strControls[0];
									String strValue = strControls[1];
						%>
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a href="#" onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							</p>
						</div> <%
 	}
 	}
 %>


					</td>
					<td>
						<%
							if ((sv.optdsc03) != null) {

								String token = ((sv.optdsc01.toString()).indexOf('=') >= 0) ? "=" : "-";
								String[] strControls = (sv.optdsc03.toString()).split(token);
								if (strControls != null && strControls.length > 1) {
									String strKey = strControls[0];
									String strValue = strControls[1];
						%>
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a href="#" onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							</p>
						</div> <%
 	}
 	}
 %>


					</td>
					<td>
						<%
							if ((sv.optdsc04) != null) {

								String token = ((sv.optdsc01.toString()).indexOf('=') >= 0) ? "=" : "-";
								String[] strControls = (sv.optdsc04.toString()).split(token);
								if (strControls != null && strControls.length > 1) {
									String strKey = strControls[0];
									String strValue = strControls[1];
						%>
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a href="#" onClick="JavaScript:perFormOperation(<%=strKey%>)"><%=resourceBundleHandler.gettingValueFromBundle(strValue)%></a>
							</p>
						</div> <%
 	}
 	}
 %> <!--  -->
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-s7523').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>