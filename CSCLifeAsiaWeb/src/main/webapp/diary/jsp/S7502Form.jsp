<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7502";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7502ScreenVars sv = (S7502ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Processing to be Performed"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s7502' width='100%'>
						<thead>
							<tr class='info'>
								<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Seq. Nr."))%></th>
								<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Process Code"))%></th>
								<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Successfull Subroutine"))%></th>
								<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Unsuccessfull Subroutine"))%></th>
								<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Pamameter Key"))%></th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo01, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode01,
					(sv.diaryProcessCode01.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode01')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine01, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine13, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey01"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey01");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey01.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey01.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey01, fw, longValue, "procParmKey01", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo02, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode02,
					(sv.diaryProcessCode02.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode02')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine02, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine14, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey02"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey02");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey02.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey02.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey02, fw, longValue, "procParmKey02", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo03, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode03,
					(sv.diaryProcessCode03.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode03')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine03, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine15, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey03"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey03");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey03.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey03.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey03, fw, longValue, "procParmKey03", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo04, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode04,
					(sv.diaryProcessCode04.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode04')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine04, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine16, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey04"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey04");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey04.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey04.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey04, fw, longValue, "procParmKey04", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo05, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode05,
					(sv.diaryProcessCode05.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode05')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine05, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine17, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey05"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey05");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey05.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey05.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey05, fw, longValue, "procParmKey05", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo06, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode06,
					(sv.diaryProcessCode06.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode06')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine06, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine18, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey06"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey06");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey06.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey06.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey06, fw, longValue, "procParmKey06", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo07, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode07,
					(sv.diaryProcessCode07.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode07')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine07, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine19, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey07"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey07");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey07.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey07.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey07, fw, longValue, "procParmKey07", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo08, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode08,
					(sv.diaryProcessCode08.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode08')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine08, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine20, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey08"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey08");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey08.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey08.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey08, fw, longValue, "procParmKey08", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo09, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode09,
					(sv.diaryProcessCode09.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode09')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine09, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine21, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey09"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey09");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey09.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey09.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey09, fw, longValue, "procParmKey09", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
							<tr>
								<td><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo10, 0, 80)%></td>
								<td>
									<div class="input-group" style="width: 150px;">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode10,
					(sv.diaryProcessCode10.getLength()))%>
										<span class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
												type="button"
												onclick="doFocus(document.getElementById('diaryProcessCode10')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine10, 0, 120)%></td>
								<td><%=smartHF.getHTMLVarExt(fw, sv.threadSubroutine22, 0, 120)%></td>
								<td>
									<%
										if ((new Byte((sv.procParmKey10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
											fieldItem = appVars.loadF4FieldsLong(new String[]{"procParmKey10"}, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("procParmKey10");
											optionValue = makeDropDownList(mappedItems, sv.procParmKey10.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.procParmKey10.getFormData()).toString().trim());
											if (null == longValue) {
												longValue = "";
											}
									%><!-- Ticket #ILIFE-1720 start by akhan203 --> <%=smartHF.getDropDownExt(sv.procParmKey10, fw, longValue, "procParmKey10", optionValue, 0, 190)%>
									<!-- Ticket #ILIFE-1720 ends --> <%
 	}
 %>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-6">
				<div class="btn-group">
					<div class="sectionbutton">
						<p style="font-size: 12px; font-weight: bold;">
							<a class="btn btn-primary" href="#"
								onClick=" JavaScript:doAction('PFKey91') ;"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
							<a class="btn btn-primary" href="#"
								onClick=" JavaScript:doAction('PFKey90') ;"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-s7502').DataTable({
			ordering : false,
			searching : false,
			paging : false,
			info : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

