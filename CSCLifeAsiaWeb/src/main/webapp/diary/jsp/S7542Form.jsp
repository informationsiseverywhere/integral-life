<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7542";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S7542ScreenVars sv = (S7542ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind55.isOn()) {
	sv.cntno.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind55.isOn()) {
	sv.drytxtdesc.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>




<div class="panel panel-default">
        <div class="panel-body"> 
        
            
            <div class="row">   
                <div class="col-md-6"> 
              
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Status and Parameters")%></label>
                 
                </div>
             </div>
             
             
 
    
            <div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Controller Number    :")%></label>
                         <div style="width:50px">
                         <%=smartHF.getHTMLVarReadOnly(fw, sv.cntno)%>
                         </div>
                         </div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Status    :")%></label>
                        <div style="width:80px">
                         <%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%>
                         </div>
                         </div>
                       </div>
                       
                   </div>  





<div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Level          :")%></label>
                         <div style="width:100px">
                          <%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%>
                         </div></div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
                         	<table><tr><td>
                         	<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
	                        </td><td>
	                        <%=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%>
                             </td></tr></table>
                         </div>
                       </div>
                       
                   </div> 
                   
                   
                   
                   
                   
                   <div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Number of Active Threads:")%></label>
                         <div style="width:80px">
                          <%=smartHF.getHTMLVarReadOnly(fw, sv.threadno)%>
                         </div></div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Companies   :")%></label>
                         	<table><tr>
                         	 <td><%=smartHF.getHTMLVarReadOnly(fw, sv.company01)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.company02)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.company03)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.company04)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.company05)%></td>
                              </tr></table>
                         </div>
                       </div>
                       
                   </div>
                   
                   
                   
                   
                   
                   
                   
                                      <div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Language        :")%></label>
                         <div style="width:50px">
                          <%=smartHF.getHTMLVarReadOnly(fw, sv.language)%>
                         </div></div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Companies   :")%></label>
                         	<table><tr>
                         	 <td><%=smartHF.getHTMLVarReadOnly(fw, sv.branch01)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.branch02)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.branch03)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.branch04)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.branch05)%></td>
                            </tr></table>
                         </div>
                       </div>
                       
                   </div>









                                      <div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Commit Point      :")%></label>
                         <div style="width:80px">
                          <%=smartHF.getHTMLVarReadOnly(fw, sv.cmtpnta)%>
                         </div></div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Entity Types :")%></label>
                         	<table><tr>
                         	 <td><%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp01)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp02)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp03)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp04)%></td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.dryenttp05)%></td>
                            </tr></table>
                         </div>
                       </div>
                       
                   </div>
                   
                   
                   
                   
                   
                   
                   
                   
                                                         <div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Run Number       :")%></label>
                         <div style="width:120px">
                          <%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%>
                         </div></div>
                       </div>
                       
                       
                       <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Entity Range :")%></label>
                         	<table><tr>
                         	 <td><%=smartHF.getHTMLVarReadOnly(fw, sv.entfrom)%></td>
                             <td>&nbsp;</td>
                             <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%></td>
                             <td>&nbsp;</td>
                             <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%></td>
                             <td>&nbsp;</td>
                             <td><%=smartHF.getHTMLVarReadOnly(fw, sv.entto)%></td>
                            </tr></table>
                         </div>
                       </div>
                       
                   </div>








<div class="row">   
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Monitor Parameters   :")%></label>
                         
                         <table>
                         <tr>
                         <td> <%=smartHF.getHTMLVarReadOnly(fw, sv.drymonprm)%></td>
                         <td> &nbsp; </td>
                         <td> <%=smartHF.getHTMLVarReadOnly(fw, sv.drytxtdesc)%></td>
                         </tr>
                         </table>                                            
                 </div></div>
         </div>
                   
                   
                   
                   
                   
                   
                   
                   
                   <div class="row">   
                <div class="col-md-12"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("To Increase Number of Active Threads, Fill a Parameter Below and Press Enter")%></label>
                        
                 </div></div>
 
                   </div>
                   
                   
                   
                 <div class="row">   
                <div class="col-md-12"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Increase by")%></label>
                        
                        <table>
                        <tr>
                        <td><%=smartHF.getHTMLVarExt(fw, sv.threads01)%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Threads"))%></td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to to"))%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getHTMLVarExt(fw, sv.threads03)%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Threads"))%></td>
                        </table>
                 </div></div>
 
                   </div>




<br>







                   <div class="row">   
                <div class="col-md-12"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("To Decrease Number of Active Threads, Fill a Parameter Below and Press Enter")%></label>
                        
                 </div></div>
 
                   </div>
                   
                   
                   
                 <div class="row">   
                <div class="col-md-12"> 
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Decrease by")%></label>
                        
                        <table>
                        <tr>
                        <td><%=smartHF.getHTMLVarExt(fw, sv.threads02)%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Threads"))%></td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to to"))%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getHTMLVarExt(fw, sv.threads04)%></td>
                        <td>&nbsp;</td>
                        <td><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Threads"))%></td>
                        </table>
                 </div></div>
 
                   </div>








</div></div>































































































 
<%@ include file="/POLACommon2NEW.jsp"%>

