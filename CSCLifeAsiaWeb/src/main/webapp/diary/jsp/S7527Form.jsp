<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7527";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7527ScreenVars sv = (S7527ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind40.isOn()) {
			sv.diaryProcessCode.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Process "))%></label>
					<table>
						<tr class="input-group three-controller">
							<td>
								<div class="input-group" style="width: 150px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.diaryProcessCode, (sv.diaryProcessCode.getLength()))%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
											type="button"
											onclick="doFocus(document.getElementById('diaryProcessCode')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</td>
							<td><%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Processing Date"))%></label>
					<%
						if ((new Byte((sv.nxtprcdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.nxtprcdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.nxtprcdateDisp, (sv.nxtprcdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Prc Time"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.nxtprctime)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sequence Number"))%></label>
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.procSeqNo)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

