<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7547";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7547ScreenVars sv = (S7547ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Diary Testing Tool")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Clear Diary Data")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div style="width: 150px;">
					<input name='action' type='hidden'
						value='<%=sv.action.getFormData()%>'
						size='<%=sv.action.getLength()%>'
						maxLength='<%=sv.action.getLength()%>' class="input_cell"
						onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
						onKeyUp='return checkMaxLength(this)'>
				</div>
			</div>
		</div>
	</div>
</div>
