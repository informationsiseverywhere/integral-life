<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.csc.util.XSSFilter" %>
<%
	String screenName = "S7506";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7506ScreenVars sv = (S7506ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("------------------")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("L.H.S.")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("------------------")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("------------------")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("R.H.S.")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("------------------")%></label>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Process")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Control")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Sign")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Process")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Control")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Sign")%></label>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Name")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
			</div>
			<div class="col-md-2">
				<label></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Name")%></label>
			</div>
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
			</div>
			<div class="col-md-2">
				<label></label>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr01, (sv.lhssubr01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno1) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno1, (sv.lhtotno1.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno1) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign1) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign1, (sv.lhssign1.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign1) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr01, (sv.rhssubr01.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr01) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno1) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno1, (sv.rhtotno1.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno1) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign1) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign1, (sv.rhssign1.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign1) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign1, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr02, (sv.lhssubr02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno2) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno2, (sv.lhtotno2.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno2) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno2, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign2) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign2, (sv.lhssign2.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign2) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign2, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr02) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr02, (sv.rhssubr02.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr02) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno2) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno2, (sv.rhtotno2.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno2) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno2, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign2) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign2, (sv.rhssign2.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign2) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign2, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr03) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr03, (sv.lhssubr03.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr03) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno3) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno3, (sv.lhtotno3.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno3) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno3, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign3) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign3, (sv.lhssign3.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign3) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign3, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr03) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr03, (sv.rhssubr03.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr03) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno3) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno3, (sv.rhtotno3.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno3) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno3, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign3) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign3, (sv.rhssign3.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign3) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign3, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr04) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr04, (sv.lhssubr04.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr04) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno4) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno4, (sv.lhtotno4.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno4) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno4, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign4) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign4, (sv.lhssign4.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign4) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign4, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr04) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr04, (sv.rhssubr04.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr04) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno4) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno4, (sv.rhtotno4.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno4) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno4, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign4) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign4, (sv.rhssign4.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign4) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign4, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr05) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr05, (sv.lhssubr05.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr05) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno5) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno5, (sv.lhtotno5.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno5) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno5, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign5) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign5, (sv.lhssign5.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign5) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign5, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr05) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr05, (sv.rhssubr05.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr05) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno5) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno5, (sv.rhtotno5.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno5) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno5, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign5) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign5, (sv.rhssign5.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign5) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign5, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.lhssubr06) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssubr06, (sv.lhssubr06.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssubr06) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssubr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhtotno6) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhtotno6, (sv.lhtotno6.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhtotno6) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhtotno6, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.lhssign6) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lhssign6, (sv.lhssign6.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lhssign6) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.lhssign6, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div>
						<%
							if (((BaseScreenData) sv.rhssubr06) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssubr06, (sv.rhssubr06.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssubr06) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssubr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhtotno6) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhtotno6, (sv.rhtotno6.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhtotno6) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhtotno6, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.rhssign6) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.rhssign6, (sv.rhssign6.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.rhssign6) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rhssign6, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 127px;">
						<%
							if ((new Byte((sv.xdsmore).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.xdsmore.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.xdsmore.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.xdsmore.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
