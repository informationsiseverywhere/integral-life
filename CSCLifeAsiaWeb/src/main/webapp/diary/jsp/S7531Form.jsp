<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7531";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7531ScreenVars sv = (S7531ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.sel.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.sel.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind13.isOn()) {
			sv.sel.setInvisibility(BaseScreenData.INVISIBLE);
		}

		if (appVars.ind01.isOn()) {
			sv.sel.setReverse(BaseScreenData.REVERSED);
			sv.sel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.sel.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Details"))%></label>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company    "))%></label>
					<table>
					<tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
					
					</td><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc, 1)%>
					
					</td></tr>
					</table>
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch    "))%></label>
						<table>
					<tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
					
					</td><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc, 1)%>
					
					</td></tr>
					</table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level "))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Active Threads"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.nofThreads)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Information"))%></label>
				</div>
			</div>
		</div>
		<br>
		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 36;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 120;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 156;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 120;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = 156;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = 180;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7531screensfl");
			GeneralTable sfl1 = fw.getTable("s7531screensfl");
			S7531screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = 210;
			} else {
				height = 210;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7531' width='100%'>
							<thead>
								<tr class='info'>
									<th><center> Sel</center></th>
									<th><center>Run Date</center></th>
									<th><center>Run Number</center></th>
									<th><center>Start Date</center></th>
									<th><center>Start Time</center></th>
									<th><center>End Date</center></th>
									<th><center>End Time</center></th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7531screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7531screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<input type='hidden' maxLength='<%=sv.timestart.getLength()%>'
										value='<%=sv.timestart.getFormData()%>'
										size='<%=sv.timestart.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(s7531screensfl.timestart)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s7531screensfl" + "." + "timestart" + "_R" + count%>'
										id='<%="s7531screensfl" + "." + "timestart" + "_R" + count%>'>


									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0]%>px;"
										<%if ((sv.sel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="center" <%}%>>
										<%
											if ((new Byte((sv.sel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7531screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7531screensfl.sel_R<%=count%>'
										id='s7531screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7531screensfl.sel_R<%=count%>')"
										class="radio" disabled="disabled" /> <%
 	} else {
 %> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7531screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7531screensfl.sel_R<%=count%>'
										id='s7531screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7531screensfl.sel_R<%=count%>')"
										class="radio" /> <%
 	}
 %>

									</td>
									<td style="width:<%=tblColumnWidth[1]%>px;"
										<%if (!(((BaseScreenData) sv.effdateDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.effdateDisp, "effdate", "s7531screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[2]%>px;"
										<%if (!(((BaseScreenData) sv.runNumber) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.runNumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%> <%
 	longValue = sv.runNumber.getFormData();
 %>
										<div id="s7531screensfl.runNumber_R<%=count%>"
											name="s7531screensfl.runNumber_R<%=count%>">
											<%=longValue%>
										</div> <%
 	longValue = null;
 %> <%
 	}
 %>
									</td>

									<td style="width:<%=tblColumnWidth[3]%>px;"
										<%if (!(((BaseScreenData) sv.datestartDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.datestartDisp, "datestartDisp", "s7531screensfl", count)%>
									</td>
									<td style="width:<%=tblColumnWidth[4]%>px;"
										<%if (!(((BaseScreenData) sv.zapptime01) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.zapptime01, "zapptime01", "s7531screensfl", count)%>
									</td>
									<td style="width:<%=tblColumnWidth[5]%>px;"
										<%if (!(((BaseScreenData) sv.dateendDisp) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.dateendDisp, "dateendDisp", "s7531screensfl", count)%>
									</td>
									<td style="width:<%=tblColumnWidth[6]%>px;"
										<%if (!(((BaseScreenData) sv.zapptime02) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.zapptime02, "zapptime02", "s7531screensfl", count)%>
									</td>
								</tr>
								<%
									count = count + 1;
										S7531screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input type="hidden" name="timestart" id="timestart"
							value="<%=(sv.timestart.getFormData()).toString()%>">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="btn-group">
					<div class="sectionbutton">
						
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(5)"><%=resourceBundleHandler.gettingValueFromBundle("Details")%></a>
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(6)"><%=resourceBundleHandler.gettingValueFromBundle("Monitor")%></a>
						
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-s7531').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
