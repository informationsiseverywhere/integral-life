<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7545";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7545ScreenVars sv = (S7545ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>


<script>
function perFormOperationMultiSelect(act, length){
	var elementSelected=false;
	for(var i=1; i < length; i++){
		var checkbox = document.getElementById("s7545screensfl.select_R"+i);
		if(checkbox.checked == true){
			checkbox.value=act;
			elementSelected=true;
		}	
	}
	
	if(elementSelected){
		doAction('PFKEY0'); 
	}
}



</script>


<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">

				<label><%=resourceBundleHandler.gettingValueFromBundle("System Details")%></label>

			</div>
		</div>


		<div class="row">
			<div class="col-md-4">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Company :")%></label>
				
				<table>
				<tr><td>
				<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
				</td><td>
				<%=smartHF.getHTMLVarReadOnly(fw, sv.shortds,1)%>
				</td></tr>
				</table>
					

					
				
			</div>

			<div class="col-md-4">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Branch :")%></label>
				<table>
				<tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
				</td><td>
				<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc,1)%>
				</td></tr>
				</table>
				
			</div>
		</div>




		<div class="row">
			<div class="col-md-4">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Level :")%></label>

				<%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%>



			</div>

			
			<div class="col-md-4" style="width: 125px">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
				<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%>
				<%=smartHF.getHTMLCalNSVarExt(fw, sv.effdateDisp)%>


			</div>



		</div>
		<br>

		<div class="row">
			<div class="col-md-3">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Soft Lock Details")%></label>


			</div>
		</div>
		<br> 

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s7545' width='100%'>
						<thead>
							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Set")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Entity")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Lock Date")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Lock Time")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("User")%></center></th>


							</tr>

							<%
								appVars.rollup(new int[]{93});
							%>
							<%
								int[] tblColumnWidth = new int[22];
								int totalTblWidth = 0;
								int calculatedValue = 0;
								int arraySize = 0;

								calculatedValue = 48;
								totalTblWidth += calculatedValue;
								tblColumnWidth[0] = calculatedValue;

								calculatedValue = 96;
								totalTblWidth += calculatedValue;
								tblColumnWidth[1] = calculatedValue;

								calculatedValue = 132;
								totalTblWidth += calculatedValue;
								tblColumnWidth[2] = calculatedValue;
								/* ILPI-196 */
								calculatedValue = 96;
								totalTblWidth += calculatedValue;
								tblColumnWidth[3] = calculatedValue;

								calculatedValue = 168;
								totalTblWidth += calculatedValue;
								tblColumnWidth[4] = calculatedValue;

								calculatedValue = 180;
								totalTblWidth += calculatedValue;
								tblColumnWidth[5] = calculatedValue;

								calculatedValue = 132;
								totalTblWidth += calculatedValue;
								tblColumnWidth[6] = calculatedValue;
								/* ILPI-196 */
								/* calculatedValue=144;
								totalTblWidth += calculatedValue;
								tblColumnWidth[7] = calculatedValue; */

								if (totalTblWidth > 730) {
									totalTblWidth = 730;
								}
								arraySize = tblColumnWidth.length;
								GeneralTable sfl = fw.getTable("s7545screensfl");
								GeneralTable sfl1 = fw.getTable("s7545screensfl");
								S7545screensfl.set1stScreenRow(sfl, appVars, sv);
								int height;
								if (sfl.count() * 27 > 210) {
									height = 210;
								} else if (sfl.count() * 27 > 118) {
									/* ILPI-196 */
									height = 210;
								} else {
									height = 210;
								}
							%>
						
						<tbody>
							<%
								S7545screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (S7545screensfl.hasMoreScreenRows(sfl)) {
									hyperLinkFlag = true;
							%>



							<tr id='tr<%=count%>' height="30">
								<td class="tableDataTag tableDataTagFixed"
									style="width:<%=tblColumnWidth[0]%>px;"
									<%if ((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<%
										if ("*DELETED*".equalsIgnoreCase(sv.shortdesc.getFormData().trim())) {
									%> <input type="checkbox" onFocus='doFocus(this)'
									onHelp='return fieldHelp("s7545screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='s7545screensfl.select_R<%=count%>'
									id='s7545screensfl.select_R<%=count%>'
									onClick="selectedRow('s7545screensfl.select_R<%=count%>')"
									class="UICheck" disabled=disabled /> <%
 	} else {
 %> <%=smartHF.getHTMLCheckBoxSFL(sv.select, "select", "s7545screensfl", count)%>
									<%
										}
									%>



								</td>




								<td style="width:<%=tblColumnWidth[1]%>px;"
									<%if (!(((BaseScreenData) sv.drytxtdesc) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLAnchorFieldSFL(sv.drytxtdesc, "drytxtdesc", "s7545screensfl", count,
						hyperLinkFlag)%></td>



								<td style="width:<%=tblColumnWidth[2]%>px;"
									<%if (!(((BaseScreenData) sv.diaryEntity) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.diaryEntity, "diaryEntity", "s7545screensfl", count)%>
								</td>



								<td style="width:<%=tblColumnWidth[3]%>px;"
									<%if (!(((BaseScreenData) sv.shortdesc) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.shortdesc, "shortdesc", "s7545screensfl", count)%>
								</td>



								<td style="width:<%=tblColumnWidth[4]%>px;"
									<%if (!(((BaseScreenData) sv.trandateDisp) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.trandateDisp, "trandateDisp", "s7545screensfl", count)%>
								</td>



								<td style="width:<%=tblColumnWidth[5]%>px;"
									<%if (!(((BaseScreenData) sv.sttime) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.sttime, "sttime", "s7545screensfl", count)%>
								</td>



								<td style="width:<%=tblColumnWidth[6]%>px;"
									<%if (!(((BaseScreenData) sv.cruser) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.cruser, "cruser", "s7545screensfl", count)%>
								</td>



							</tr>
							<%
								count = count + 1;
									S7545screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>

					



				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="btn-group">
					<div class="sectionbutton">
						
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperationMultiSelect(2,<%=count%>)"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperationMultiSelect(5, <%=count%>)"><%=resourceBundleHandler.gettingValueFromBundle("Enquire")%></a>
						
					</div>
				</div>
			</div>
		</div>
	


	</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTables-s7545').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
paging:   true,
        scrollCollapse: true,
  	});
})
</script>


<BODY>
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span> <%
 	String sidebartext = "";
 %>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx01.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx01.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx01"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx01' id='optdscx01' type='hidden'
											value="<%=sv.optdscx01.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx02.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx02.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx02"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx02' id='optdscx02' type='hidden'
											value="<%=sv.optdscx02.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx03.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx03.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx03"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx03' id='optdscx03' type='hidden'
											value="<%=sv.optdscx03.getFormData()%>">
									</div>
								</li>
								<li>
									<div>
										<input name='optdscx04' id='optdscx04' type='hidden'
											value="<%=sv.optdscx04.getFormData()%>">
									</div>
								</li>
							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>

<%@ include file="/POLACommon2NEW.jsp"%>

