<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7530";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7530ScreenVars sv = (S7530ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Inquiry Date"))%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number :"))%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.diaryRunNumber)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Action")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Diary System General Inquiry")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Control Totals Inquiry")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Reconciliation Rules Inquiry")%></b>
					</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Diary System Error Inquiry")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Diary Message Inquiry")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "F")%><%=resourceBundleHandler.gettingValueFromBundle("Output Inquiry")%></b>
					</label>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div>
					<input name='action' type='hidden'
						value='<%=sv.action.getFormData()%>'
						size='<%=sv.action.getLength()%>'
						onHelp='return fieldHelp(action)'
						maxLength='<%=sv.action.getLength()%>' class="input_cell"
						onFocus='doFocus(this)' onKeyUp='return checkMaxLength(this)'>
				</div>
			</div>
		</div>
	</div>
</div>
