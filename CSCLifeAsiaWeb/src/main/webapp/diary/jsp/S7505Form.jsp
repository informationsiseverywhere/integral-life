<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7505";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7505ScreenVars sv = (S7505ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%
							StringData COMPANY_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
						%><%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute", "relative")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=formatValue%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
 	StringData TABL_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
 %>
						<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute", "relative")%>
					</label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=formatValue%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label> <%
 	StringData ITEM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
 %>
						<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute", "relative")%>
					</label>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=formatValue%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=formatValue%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label> <%
 	StringData S7505_1_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Total Number");
 %>
					<%=smartHF.getLit(0, 0, S7505_1_LBL).replace("absolute", "relative")%>
				</label>
			</div>
			<div class="col-md-4">
				<label> <%
 	StringData S7505_2_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
 			"Total Description");
 %>
					<%=smartHF.getLit(0, 0, S7505_2_LBL).replace("absolute", "relative")%>
				</label>
			</div>
			<div class="col-md-2">
				<label> <%
 	StringData S7505_3_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Print Format");
 %>
					<%=smartHF.getLit(0, 0, S7505_3_LBL).replace("absolute", "relative")%>
				</label>
			</div>
			<div class="col-md-2">
				<label> <%
 	StringData S7505_4_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency Symbol");
 %>
					<%=smartHF.getLit(0, 0, S7505_4_LBL).replace("absolute", "relative")%>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno01, (sv.totalno01.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc01, (sv.totdesc01.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat01, (sv.pformat01.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol01, (sv.currencySymbol01.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno02, (sv.totalno02.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc02, (sv.totdesc02.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat02, (sv.pformat02.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol02, (sv.currencySymbol02.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno03, (sv.totalno03.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc03, (sv.totdesc03.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat03, (sv.pformat03.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol03, (sv.currencySymbol03.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno04, (sv.totalno04.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc04, (sv.totdesc04.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat04, (sv.pformat04.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol04, (sv.currencySymbol04.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno05, (sv.totalno05.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc05, (sv.totdesc05.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat05, (sv.pformat05.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol05, (sv.currencySymbol05.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno06, (sv.totalno06.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc06, (sv.totdesc06.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat06, (sv.pformat06.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol06, (sv.currencySymbol06.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno07, (sv.totalno07.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc07, (sv.totdesc07.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat07, (sv.pformat07.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol07, (sv.currencySymbol07.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno08, (sv.totalno08.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc08, (sv.totdesc08.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat08, (sv.pformat08.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol08, (sv.currencySymbol08.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno09, (sv.totalno09.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc09, (sv.totdesc09.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat09, (sv.pformat09.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol09, (sv.currencySymbol09.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno10, (sv.totalno10.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc10, (sv.totdesc10.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat10, (sv.pformat10.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol10, (sv.currencySymbol10.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno11, (sv.totalno11.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc11, (sv.totdesc11.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat11, (sv.pformat11.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol11, (sv.currencySymbol11.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno12, (sv.totalno12.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc12, (sv.totdesc12.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat12, (sv.pformat12.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol12, (sv.currencySymbol12.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno13, (sv.totalno13.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc13, (sv.totdesc13.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat13, (sv.pformat13.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol13, (sv.currencySymbol13.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getRichText(0, 0, fw, sv.totalno14, (sv.totalno14.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 220px;"><%=smartHF.getRichText(0, 0, fw, sv.totdesc14, (sv.totdesc14.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.pformat14, (sv.pformat14.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 50px;"><%=smartHF.getRichText(0, 0, fw, sv.currencySymbol14, (sv.currencySymbol14.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.xdsmore).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.xdsmore.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.xdsmore.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.xdsmore.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=formatValue%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
