<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7546";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7546ScreenVars sv = (S7546ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(
					"To Test an Entity or a Range of Entities, Fill in Parameters  and Press Enter"))%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Parameters"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Type  :"))%></label>
					<div style="width: 170px;">
						<%
							if ((new Byte((sv.dryenttp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dryenttp" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dryenttp");
								optionValue = makeDropDownList(mappedItems, sv.dryenttp.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dryenttp.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.dryenttp, fw, longValue, "dryenttp", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Number  :"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.entityfrom)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Number :"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.entityto)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(
					"To Debug a Diary Subroutine Start the Debug Facility Before Entering Driver"))%></label>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

