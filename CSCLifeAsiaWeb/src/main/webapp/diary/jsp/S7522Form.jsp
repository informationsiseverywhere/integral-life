<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7522";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7522ScreenVars sv = (S7522ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Type :"))%></label>
					<div style="width: 200px;">
						<%
							if ((new Byte((sv.diaryEntityType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "diaryEntityType" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("diaryEntityType");
								optionValue = makeDropDownList(mappedItems, sv.diaryEntityType.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.diaryEntityType.getFormData()).toString().trim());
								if (null == longValue) {
									longValue = "";
								}
						%>
						<%=smartHF.getDropDownExt(sv.diaryEntityType, fw, longValue, "diaryEntityType", optionValue)%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entity Number:"))%></label>
					<div class="input-group" style="width: 150px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.dryhldent, (sv.dryhldent.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('dryhldent')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>

<!-- Close div panel panel-default -->
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Action")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "A")%><b><%=resourceBundleHandler.gettingValueFromBundle("Roll Forward Processing")%></b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><b><%=resourceBundleHandler.gettingValueFromBundle("Hold an Entity for Processing")%></b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%><b><%=resourceBundleHandler.gettingValueFromBundle("Enquire on Held Entity")%></b></label>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div>
					<input name='action' type='hidden'
						value='<%=sv.action.getFormData()%>'
						size='<%=sv.action.getLength()%>'
						onHelp='return fieldHelp(action)'
						maxLength='<%=sv.action.getLength()%>' class="input_cell"
						onFocus='doFocus(this)' onKeyUp='return checkMaxLength(this)'>
				</div>
			</div>
		</div>
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

