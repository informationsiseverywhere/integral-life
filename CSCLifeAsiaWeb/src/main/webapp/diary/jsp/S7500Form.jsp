<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7500";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7500ScreenVars sv = (S7500ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table:"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item:"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Financial Batching Transaction Code:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.batctrcde)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commit Database Changes"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Before Processing:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.combef)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("After Processing:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.comaft)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label> <%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Transaction Processing Subroutines:"))%>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine04)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine05)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine06)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine07)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine08)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine09)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine10)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine11)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine12)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine13)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine14)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine15)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine16)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine17)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine18)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine19)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.diarySubroutine20)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Paramerters:"))%></label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam04)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam05)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam06)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam07)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam08)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam09)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam10)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam11)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam12)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam13)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam14)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam15)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam16)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam17)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam18)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam19)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam20)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam21)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam22)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam23)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam24)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.systemParam25)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

