<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7533";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7533ScreenVars sv = (S7533ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company "))%></label>
					<div class="input-group three-controller" style="width: 150px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<div class="input-group three-controller" style="width: 320px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level    "))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Nr"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reconciliation Rules"))%></label>
					<div></div>
				</div>
			</div>
		</div>
		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 48;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 420;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 192;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7533screensfl");
			GeneralTable sfl1 = fw.getTable("s7533screensfl");
			S7533screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = sfl.count() * 27;
			} else {
				height = 118;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7533' width='100%'>
							<thead>
								<tr class='info'>
									<th>Sel</th>
									<th>Rule</th>
									<th>Description</th>
									<th>Result</th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7533screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7533screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<td style="width:<%=tblColumnWidth[0]%>px;"
										<%if (!(((BaseScreenData) sv.sel) instanceof StringBase)) {%>
										align="right" <%} else {%> align="center" <%}%>>
										<%-- <%=smartHF.getHTMLCheckBoxSFL(sv.sel, "sel", "s7533screensfl", count)%> --%>
										<%-- <%=smartHF.getHTMLFieldSFL(sv.sel, "sel", "s7533screensfl", count, fw, hyperLinkFlag)%> --%>



										<%
											if ((new Byte((sv.sel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7533screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7533screensfl.sel_R<%=count%>'
										id='s7533screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7533screensfl.sel_R<%=count%>')"
										class="radio" disabled="disabled" /> <%
 	} else {
 %> <%
 	if ("N/A".equalsIgnoreCase(sv.result.getFormData().trim())) {
 			} else {
 %> <input type="radio" value='<%=sv.sel.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7533screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='s7533screensfl.sel_R<%=count%>'
										id='s7533screensfl.sel_R<%=count%>'
										onClick="selectedRow('s7533screensfl.sel_R<%=count%>')"
										class="radio" /> <%
 	}
 		}
 %>
									</td>





									<td style="width:<%=tblColumnWidth[1]%>px;"
										<%if (!(((BaseScreenData) sv.reconrule) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.reconrule, "reconrule", "s7533screensfl", count)%>
										<%-- <%=smartHF.getHTMLAnchorFieldSFL(sv.reconrule, "reconrule", "s7533screensfl", count, hyperLinkFlag)%> --%>
									</td>



									<td style="width:<%=tblColumnWidth[2]%>px;"
										<%if (!(((BaseScreenData) sv.descrip) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.descrip, "descrip", "s7533screensfl", count)%>
									</td>



									<td style="width:<%=tblColumnWidth[3]%>px;"
										<%if (!(((BaseScreenData) sv.result) instanceof StringBase)) {%>
										align="right" <%} else {%> align="left" <%}%>><%=smartHF.getHTMLOutputFieldSFL(sv.result, "result", "s7533screensfl", count)%>
									</td>



								</tr>
								<%
									count = count + 1;
										S7533screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="btn-group">
					<div class="sectionbutton">
						<p style="font-size: 12px; font-weight: bold;">
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(5)"><%=resourceBundleHandler.gettingValueFromBundle("Details")%></a>

						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<BODY style="background-color: #f8f8f8;">
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span> <%
 	String sidebartext = "";
 %>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx01.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx01.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx01"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx01' id='optdscx01' type='hidden'
											value="<%=sv.optdscx01.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx02.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx02.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx02"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx02' id='optdscx02' type='hidden'
											value="<%=sv.optdscx02.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx03.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx03.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx03"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx03' id='optdscx03' type='hidden'
											value="<%=sv.optdscx03.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx04.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx04.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx04"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx04' id='optdscx04' type='hidden'
											value="<%=sv.optdscx04.getFormData()%>">
									</div>
								</li>
							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>
<script>
	$(document).ready(function() {
		$('#dataTables-s7533').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

