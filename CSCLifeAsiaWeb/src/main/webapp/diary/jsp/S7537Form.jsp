<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7537";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>


<%S7537ScreenVars sv = (S7537ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">

<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company    "))%>
<div class="input-group">
 <%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
        <%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc)%> 
</div></div></div>


<div class="col-md-2"></div>
<div class="col-md-3">
                <div class="form-group">
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%>
<div class="input-group">
 <%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
 	<span class="input-group-addon" style="min-width:250px;">
										
						<%
						if(!((sv.longdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc01.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						
						
									} else  {
						
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc01.getFormData()).toString());
											} else {
												formatValue = formatValue( longValue);
											}
						
									}
									%>
								<%=formatValue%>
							
							
							
							
						<%
						longValue = null;
						formatValue = null;
						%>
</div></div></div></div>


 <div class="row">
              <div class="col-md-3">
                <div class="form-group">
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%>

  <%=smartHF.getHTMLVarReadOnly(fw, sv.effdateDisp)%>  
</div></div>

 <div class="col-md-2"></div>
<div class="col-md-4">
                <div class="form-group">
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rule"))%>

<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc02)%> 
</div></div></div>
<%-- 
<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%>
<br/>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Subroutine Details"))%>
</td>
<td></td>
<td></td>
</tr> </table>--%>
<%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=48;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=180;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=132;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=168;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=96;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

calculatedValue=144;
totalTblWidth += calculatedValue;
tblColumnWidth[5] = calculatedValue;

calculatedValue=180;
totalTblWidth += calculatedValue;
tblColumnWidth[6] = calculatedValue;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("s7537screensfl");
GeneralTable sfl1 = fw.getTable("s7537screensfl");
S7537screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = 210;
} else {
height = 210;
}	
%>
<style type="text/css">
.fakeContainer {
	width:<%=totalTblWidth%>px;	
	height:<%=height%>px; 	
}
</style>
<script language="javascript">
		$(document).ready(function(){
			var rows = <%=sfl1.count()+1%>;
			var isPageDown = 1;
			var pageSize = 1;
			var headerRowCount=1;
			var fields = new Array();
			var colWidth = new Array();
			var j=0;
			<% for(int i=0;i<arraySize;i++){	%>
				colWidth[j]=<%=tblColumnWidth[i]%>;
				j=j+1;
			<%}%>
	<%if(false){%>	
		operateTableForSuperTable(rows,isPageDown,pageSize,fields,"s7537Table",null,headerRowCount);
	<%}%>
			new superTable("s7537Table", {
				fixedCols : 0,					
				colWidths : [297,60,296,60],
				<%if(false){%>	
					headerRows :headerRowCount,		
					addRemoveBtn:"Y",
				<%}%>
				hasHorizonScroll: "Y"
			
				
			});
		});
	</script>
	<div style="position:relative; top:5px; width:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>900px;<%}%>">
	
<br/>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5736'>
							<thead>
								<tr class='info'>
								<th colspan='2' style="text-align:center">Left Side</th>

<th colspan='2' style="text-align:center">Right Side</th>
							</tr>	
								<tr class='info'>
<th style="text-align:center">Description</th>
<th style="text-align:center">Value</th>
<th style="text-align:center">Description</th>
<th style="text-align:center">Value</th>

</tr>
<% String backgroundcolor="#FFFFFF";%>	
<%
S7537screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (S7537screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>
<tr id='tr<%=count%>' height="30">

<%-- <td style="width:<%=tblColumnWidth[0]%>px;" 
		<%if(!(((BaseScreenData)sv.select) instanceof StringBase)) {%>align="right"<% }else {%> align="center" <%}%> >									
	  <%=smartHF.getHTMLFieldSFL(sv.select, "select", "s7532screensfl", count, fw, hyperLinkFlag)  
	

	 <% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){%>	 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s7532screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s7532screensfl.select_R<%=count%>'
						 id='s7532screensfl.select_R<%=count%>'
						 onClick="selectedRow('s7532screensfl.select_R<%=count%>')"
						 class="radio"
						 
						 	
				disabled="disabled"
						
						
					 />
					 <%}else{ %>
					 		<input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s7532screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s7532screensfl.select_R<%=count%>'
						 id='s7532screensfl.select_R<%=count%>'
						 onClick="selectedRow('s7532screensfl.select_R<%=count%>')"
						 class="radio"
						 />		
					
											
									<%}%>  
</td> --%>




		
<td style="width:<%=tblColumnWidth[1]%>px;" 
	<%if(!(((BaseScreenData)sv.totdesc01) instanceof StringBase)) {%>align="center"<% }else {%> align="center" <%}%> >						
	<%=smartHF.getHTMLOutputFieldSFL(sv.totdesc01, "totdesc01", "s7537screensfl", count)%>	
</td>



<td style="width:<%=tblColumnWidth[2]%>px;" <%if(!(((BaseScreenData)sv.dryval01) instanceof StringBase)) {%> align="left"<% }else {%> align="left" <%}%>>
	<% if("".equalsIgnoreCase(sv.dryval01.getFormData().trim())){  }else{%>
	<%-- <%=smartHF.getHTMLOutputFieldSFL(sv.dryval01, "dryval01", "s7537screensfl", count)%> --%>
	<%=smartHF.getHTMLOutputFieldSFLWithFormater((BaseScreenData)sv.dryval01, "s7537screensfl", count, sfl, "")%>
	<%} %>
	
</td>


<td style="width:<%=tblColumnWidth[3]%>px;" 
	<%if(!(((BaseScreenData)sv.totdesc02) instanceof StringBase)) {%>align="center"<% }else {%> align="center" <%}%> >						
	<%=smartHF.getHTMLOutputFieldSFL(sv.totdesc02, "totdesc02", "s7537screensfl", count)%>	
</td>

<%-- 
<td style="width:<%=tblColumnWidth[3]%>px;" <%if(!(((BaseScreenData)sv.totdesc02) instanceof StringBase)) {%> align="center"<% }else {%> align="right" <%}%>>
	<%=smartHF.getHTMLOutputFieldSFL(sv.totdesc02, "totdesc02", "s7537screensfl", count)%>
</td>
 --%>


<td style="width:<%=tblColumnWidth[4]%>px;" <%if(!(((BaseScreenData)sv.dryval02) instanceof StringBase)) {%> align="left"<% }else {%> align="left" <%}%>>
	<% if("00000000".equalsIgnoreCase(sv.dryval02.getFormData().trim())){  }else{%>
	<%=smartHF.getHTMLOutputFieldSFLWithFormater((BaseScreenData)sv.dryval02, "s7537screensfl", count, sfl, "")%>
	
	<%} %>
</td>






</tr>

<%	count = count + 1;
S7537screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>

<tr height="30">
<td align='center'>Total:</td>
<td align='left'> <%=sv.dryval03%></td>
<td align='center'>Total:</td>
<td align='left'> <%=sv.dryval04%></td>

</tr>

</table>
<div class="sectionbutton pull-right">
							<a href="javascript:;" onmouseout="changeMoreImageOut(this);"
								onmouseover="changeMoreImage(this);"
								onClick="doAction('PFKey90');" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("More")%></a>
						</div>


</div>
</div>

</div></div></div></div></div>

<%@ include file="/POLACommon2NEW.jsp"%>

