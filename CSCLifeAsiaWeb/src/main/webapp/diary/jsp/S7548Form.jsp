<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7548";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7548ScreenVars sv = (S7548ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label style="font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(
					"To clear all or a range of Diary Data, complete parameters and press Enter"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Parameters:"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start/End date of clearing data"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">

					<table>
						<tr>
							<td style="padding-top: 12px; font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Date From"))%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.datefrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| fw.getVariables().isScreenProtected()) {
								%>
								<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp)%></div>
								<%
									} else {
								%>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp, (sv.datefrmDisp.getLength()))%>
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-calendar"></span>
									</span>
								</div> <%
 	}
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td style="padding-top: 12px; font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("to"))%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if ((new Byte((sv.datetoDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| fw.getVariables().isScreenProtected()) {
								%>
								<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.datetoDisp)%></div>
								<%
									} else {
								%>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.datetoDisp, (sv.datetoDisp.getLength()))%>
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-calendar"></span>
									</span>
								</div> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label style="font-size: 14px;"> <%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(
					"The following parameters are to clear Diary Header and Transaction details."))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table>
						<tr>
							<td style="font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Clear Primary Data		"))%></td>
							<td>&nbsp;&nbsp;</td>
							<td><%=smartHF.getRichTextDateInput(fw, sv.flag)%></td>
							<td>&nbsp;&nbsp;</td>
							<td style="font-size: 14px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Clear DHDRPF and DTRDPF(Y/N)?"))%></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

