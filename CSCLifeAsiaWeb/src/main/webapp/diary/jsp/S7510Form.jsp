<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7510";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7510ScreenVars sv = (S7510ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company:"))%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.company)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table:"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%></div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item:"))%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="padding-top: 12px;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum Number of Errors Allowed"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.selectNumber)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="padding-top: 12px;"><%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Cancel Method When Maximum Reached"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.cancelMode)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Time From"))%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Time To"))%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Entities on Queue"))%><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number of Threads"))%><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number Of Threads"))%><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm01)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm01)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm01)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx01)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm02)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm02)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm02)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx02)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm03)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm03)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm03)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx03)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt04)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm04)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm04)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm04)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx04)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt05)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm05)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm05)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm05)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx05)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.monfrmt06)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 90px;"><%=smartHF.getHTMLVarExt(fw, sv.montotm06)%></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.monentm06)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrm06)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.monthrx06)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

