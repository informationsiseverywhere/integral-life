<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7535";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>
<%@ page import="com.csc.util.XSSFilter" %>

<%S7535ScreenVars sv = (S7535ScreenVars) fw.getVariables();%>
<%{
}%>
<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">

<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Details"))%></label>
</div></div></div>
 
 
 
      <div class="row">
              <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company    "))%></label>
<table><tr><td>
<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
</td><td style="padding-left:1px;">
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>

		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}


					} else  {

					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}

					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null; 
		formatValue = null;
		%>
  <%}%> 
  </td></tr></table>
  
</div></div>
  
  <!-- END TD FOR ROW 2,5 etc -->
<div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
<table>
<tr><td>
<%=smartHF.getHTMLVarReadOnly(fw, sv.branch)%>

</td><td>
<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc,1)%>
</td></tr>
</table>

</div></div>
</div>
<br>

<div class="row">
<div class="col-md-3">
                <div class="form-group">
                <label>
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Informational Messages"))%></label>
</div></div></div>
<%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=90;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=90;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=110;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=320;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=105;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

calculatedValue=90;
totalTblWidth += calculatedValue;
tblColumnWidth[5] = calculatedValue;

calculatedValue=650;
totalTblWidth += calculatedValue;
tblColumnWidth[6] = calculatedValue;



if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("s7535screensfl");
GeneralTable sfl1 = fw.getTable("s7535screensfl");
S7535screensfl.set1stScreenRow(sfl, appVars, sv);
int height =350;
%>

<script language="javascript">
		$(document).ready(function(){
			var rows = <%=sfl1.count()+1%>;
			var isPageDown = 1;
			var pageSize = 1;
			var headerRowCount=1;
			var fields = new Array();
			var colWidth = new Array();
			var j=0;
			<% for(int i=0;i<arraySize;i++){	%>
				colWidth[j]=<%=tblColumnWidth[i]%>;
				j=j+1;
			<%}%>
	<%if(false){%>	
		operateTableForSuperTable(rows,isPageDown,pageSize,fields,"s7535Table",null,headerRowCount);
	<%}%>
			new superTable("s7535Table", {
				fixedCols : 2,					
				colWidths : [80,80,80,80,50,80,80,80,80,245],
				<%if(false){%>	
					headerRows :headerRowCount,		
					addRemoveBtn:"Y",
				<%}%>
				hasHorizonScroll: "Y"
			
				
			});
		});
	</script>
	<div style="position:relative; top:5px; width:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>830px;<%}%>">
	
<br/>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s7535' width='100%'>
							<thead>
								<tr class='info'>
<th><center>Effective Date</center></th>
<th><center>Run Number</center></th>
<th><center>Subroutine</center></th>
<th><center>Company</center></th>
<th><center>Branch</center></th>
<th><center>Entity Type</center></th>
<th><center>Entity</center></th>
<th><center>System Date</center></th>
<th><center>System Time</center></th>
<th><center>Message Info</center></th>
</tr>
</thead><tbody>
<% String backgroundcolor="#FFFFFF";%>	
<%
S7535screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (S7535screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
sv.effdate.setEnabled(BaseScreenData.ENABLED);
%>
<%sv.effdateDisp.setClassString("");%>
<%	sv.effdateDisp.appendClassString("string_fld");
	sv.effdateDisp.appendClassString("output_txt");
	sv.effdateDisp.appendClassString("highlight");
%>
	<%sv.runNumber.setClassString("");%>
<%	sv.runNumber.appendClassString("string_fld");
	sv.runNumber.appendClassString("output_txt");
	sv.runNumber.appendClassString("highlight");
%>
	<%sv.subrname.setClassString("");%>
<%	sv.subrname.appendClassString("string_fld");
	sv.subrname.appendClassString("output_txt");
	sv.subrname.appendClassString("highlight");
%>
	<%sv.diaryEntityCompany.setClassString("");%>
<%	sv.diaryEntityCompany.appendClassString("string_fld");
	sv.diaryEntityCompany.appendClassString("output_txt");
	sv.diaryEntityCompany.appendClassString("highlight");
%>
	<%sv.diaryEntityBranch.setClassString("");%>
<%	sv.diaryEntityBranch.appendClassString("string_fld");
	sv.diaryEntityBranch.appendClassString("output_txt");
	sv.diaryEntityBranch.appendClassString("highlight");
%>
	<%sv.diaryEntityType.setClassString("");%>
<%	sv.diaryEntityType.appendClassString("string_fld");
	sv.diaryEntityType.appendClassString("output_txt");
	sv.diaryEntityType.appendClassString("highlight");
%>
	<%sv.diaryEntity.setClassString("");%>
<%	sv.diaryEntity.appendClassString("string_fld");
	sv.diaryEntity.appendClassString("output_txt");
	sv.diaryEntity.appendClassString("highlight");
%>
	<%sv.endate.setClassString("");%>
<%	sv.endate.appendClassString("string_fld");
	sv.endate.appendClassString("output_txt");
	sv.endate.appendClassString("highlight");
%>
	<%sv.entime.setClassString("");%>
<%	sv.entime.appendClassString("string_fld");
	sv.entime.appendClassString("output_txt");
	sv.entime.appendClassString("highlight");
%>
	<%sv.errmsg.setClassString("");%>
<%	sv.errmsg.appendClassString("string_fld");
	sv.errmsg.appendClassString("output_txt");
	sv.errmsg.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>
<tr id='tr<%=count%>' height="40">

		
<td style="width:50px;" 
	<%if(!(((BaseScreenData)sv.effdateDisp) instanceof StringBase)) {%>align="right"<% }else {%> align="center" <%}%> >	
		<%=sv.effdateDisp.getFormData() %>
</td>



<td style="width:100px;" <%if(!(((BaseScreenData)sv.runNumber) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>

	<%=sv.runNumber.getFormData() %>
</td>



<td style="width:50px;" <%if(!(((BaseScreenData)sv.subrname) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>

		<%=sv.subrname.getFormData() %>
	
</td>


<style>
	.s7535TdDiv div
	{
		padding-right:0px !important;
		width: 70px !important;
	}

</style>
<td class="s7534TdDiv" style="width:20px;" <%if(!(((BaseScreenData)sv.diaryEntityCompany) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>
		<div style="position: relative; margin-left: 1px;">
		<%=sv.diaryEntityCompany.getFormData()%>
		</div>
<td class="s7535TdDiv" style="width:40px;" <%if(!(((BaseScreenData)sv.diaryEntityCompany) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>		
		<div style="position: relative; margin-left: 1px;">
		<%=sv.diaryEntityBranch.getFormData() %>
		</div>
<td class="s7535TdDiv" style="width:80px;" <%if(!(((BaseScreenData)sv.diaryEntityCompany) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>
		<div style="position: relative; margin-left: 1px;">
		<%=sv.shortdesc.getFormData() %>
		</div>
<td class="s7535TdDiv" style="width:50px;" <%if(!(((BaseScreenData)sv.diaryEntityCompany) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>		
		<%=smartHF.getHTMLF4SSVar(0, 0, sfl, sv.diaryEntityType).replace("absolute","relative").replace("style:", "style: margin-left:1px;")%>
		<div style="position: relative; margin-left: 1px;">
		<%=sv.diaryEntity.getFormData() %>
		</div>
</td>



<td style="width:80px;" <%if(!(((BaseScreenData)sv.diaryEntityBranch) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>

	<%=sv.endate.getFormData() %>
</td>



<td style="width:80px;" <%if(!(((BaseScreenData)sv.diaryEntityType) instanceof StringBase)) {%> align="right"<% }else {%> align="center" <%}%>>

<%=sv.entime.getFormData() %>
</td>



<td style="width:245px;" <%if(!(((BaseScreenData)sv.diaryEntity) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
<%=sv.errmsg.getFormData() %>
</td>







</tr>
<%	count = count + 1;
S7535screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</tbody>
</table>


</div>
</div>


</div>

</div></div></div></div>
<script>
$(document).ready(function() {
	$('#dataTables-s7535').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
paging:   true,
        scrollCollapse: true,
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

