<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7536";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>
<%S7536ScreenVars sv = (S7536ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">


<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Run Number"))%>

<%=smartHF.getHTMLVarReadOnly(fw, sv.runNumber)%>
</div></div>

    <div class="col-md-2"></div>
 <div class="col-md-3">
                <div class="form-group">
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%>

<%=smartHF.getHTMLVarReadOnly(fw, sv.effdateDisp)%>
</div></div></div>

<div class="row">
              <div class="col-md-3">
                <div class="form-group">
<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Subroutine"))%>

<%=smartHF.getHTMLVarReadOnly(fw, sv.diarySubroutine)%>
</div></div></div>



<%	appVars.rollup(new int[] {93}); %>
<% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=36;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=300;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=600;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = 376;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("s7536screensfl");
GeneralTable sfl1 = fw.getTable("s7536screensfl");
S7536screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 118;
}	
%>
<style type="text/css">
.fakeContainer {
	width:<%=totalTblWidth%>px;	
	height:<%=height%>px; 	
}
</style>
<script language="javascript">
		$(document).ready(function(){
			var rows = <%=sfl1.count()+1%>;
			var isPageDown = 1;
			var pageSize = 1;
			var headerRowCount=1;
			var fields = new Array();
			var colWidth = new Array();
			var j=0;
			<% for(int i=0;i<arraySize;i++){	%>
				colWidth[j]=<%=tblColumnWidth[i]%>;
				j=j+1;
			<%}%>
	<%if(false){%>	
		operateTableForSuperTable(rows,isPageDown,pageSize,fields,"s7536Table",null,headerRowCount);
	<%}%>
			new superTable("s7536Table", {
				fixedCols : 0,					
				colWidths : [50,324,340],
				<%if(false){%>	
					headerRows :headerRowCount,		
					addRemoveBtn:"Y",
				<%}%>
				hasHorizonScroll: "Y"
			
				
			});
		});
	</script>
	<div style="position:relative; top:5px; width:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>900px;<%}%>">
	<div style="position:relative; left:<%if(totalTblWidth < 730 ) {%> <%=totalTblWidth - 87%>px;<%} else { %>643px;<%}%>">
		<%--  <a id="more" style="margin-top:10px; margin-bottom:-3px; height:20px" name="more" href="javascript:;" onmouseout="changeMoreImageOut(this);" onmouseover="changeMoreImage(this);" onClick="pressMoreButton('PFKey90');">
			<img id="hasMore" name="hasMore" src="<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif" border="0" style="height:25px">
		 </a>	 --%>
	</div>	
<br/>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s5736'>
							<thead>
								<tr class='info'>
<th style="text-align:center">Sr.No</th>
<th style="text-align:center">Description</th>
<th style="text-align:center">Value</th>
</tr>
<% String backgroundcolor="#FFFFFF";%>	
<%
S7536screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (S7536screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>
<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.optcode.getLength()%>'
 value='<%= sv.optcode.getFormData() %>' 
 size='<%=sv.optcode.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s7536screensfl.optcode)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s7536screensfl" + "." + "optcode" + "_R" + count %>'
 id='<%="s7536screensfl" + "." + "optcode" + "_R" + count %>'		  >
<td style="width:<%=tblColumnWidth[0]%>px;" 
	<%if(!(((BaseScreenData)sv.totalno) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
			<%if((new Byte((sv.totalno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
					<%
								longValue=sv.totalno.getFormData();
							%>	
							<div id="s7536screensfl.totalno_R<%=count%>" name="s7536screensfl.totalno_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>									

														 
				
									<%}%>
			 			</td>
<td style="width:<%=tblColumnWidth[1]%>px;" 
	<%if(!(((BaseScreenData)sv.totdesc) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.totdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.totdesc.getFormData();
							%>
					 		<div id="s7536screensfl.totdesc_R<%=count%>" name="s7536screensfl.totdesc_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>
		    <td class="tableDataTag" style="width:<%=tblColumnWidth[3 ] - 60 %>px;" 
					<%if((sv.dryctval).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%> >									
								
									 
								



<%if ((new Byte((sv.dryctval).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									<%
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.dryctval).getFieldName());									
									valueThis=smartHF.getPicFormatted(qpsf,sv.dryctval,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);							
									
									}else{
										valueThis = null;
										
									} %>
<%=valueThis %>
</td>

</tr>
<%	count = count + 1;
S7536screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</table>
</div>
</div>
<br/>

<INPUT type="HIDDEN" name="optcode" id="optcode" value="<%=	(sv.optcode.getFormData()).toString() %>" >

<br/>
</div></div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
</div></div></div>
<%@ include file="/POLACommon2NEW.jsp"%>
