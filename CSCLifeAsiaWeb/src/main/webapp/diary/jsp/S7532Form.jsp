<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7532";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>


<%
	S7532ScreenVars sv = (S7532ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("System Details")%></label>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company    :")%></label>
					<table>
					<tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityCompany)%>
					
					</td><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc,1)%>
					
					</td></tr>
					</table>
						
						
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Branch:")%></label>
					<table>
					<tr><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.diaryEntityBranch)%>
					
					</td><td>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc,1)%>
					
					</td></tr>
					</table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level     :"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.curlevl)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status:"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.drystatus)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Active Threads:"))%></label>
					<div style="width: 100px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.currNofThreads)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Subroutine Details"))%></label>
				</div>
			</div>
		</div>
		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 48;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 180;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 132;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 96;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = 144;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = 180;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7532screensfl");
			GeneralTable sfl1 = fw.getTable("s7532screensfl");
			S7532screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = 210;
			} else {
				height = 210;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7532' width='100%'>
							<thead>
								<tr class='info'>
									<th><center> Sel</center></th>
									<th><center>Effective Date</center></th>
									<th><center>Run Number</center></th>
									<th><center>Subroutine</center></th>
									<th><center>Last Update</center></th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7532screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7532screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr>
									<td>
										<%
											if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%> <input type="radio" value='<%=sv.select.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7532screensfl" + "." +
						 "select")'
										onKeyUp='return checkMaxLength(this)'
										name='s7532screensfl.select_R<%=count%>'
										id='s7532screensfl.select_R<%=count%>'
										onClick="selectedRow('s7532screensfl.select_R<%=count%>')"
										class="radio" disabled="disabled" /> <%
 	} else {
 %> <input type="radio" value='<%=sv.select.getFormData()%>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("s7532screensfl" + "." +
						 "select")'
										onKeyUp='return checkMaxLength(this)'
										name='s7532screensfl.select_R<%=count%>'
										id='s7532screensfl.select_R<%=count%>'
										onClick="selectedRow('s7532screensfl.select_R<%=count%>')"
										class="radio" /> <%
 	}
 %>
									</td>





									<td><%=smartHF.getHTMLOutputFieldSFL(sv.effdateDisp, "effdate", "s7532screensfl", count)%>
									</td>



									<td><%=smartHF.getHTMLOutputFieldSFL(sv.runNumber, "runNumber", "s7532screensfl", count)%>
									</td>



									<td><%=smartHF.getHTMLOutputFieldSFL(sv.subrname, "subrname", "s7532screensfl", count)%>
									</td>

									<td>
									<div>
									<%=smartHF.getHTMLOutputFieldSFL(sv.stdate, "stdate", "s7532screensfl", count)%></div>
										<div style="
    margin-top: -22px;
    padding-left: 70px;
"><%=smartHF.getHTMLOutputFieldSFL(sv.sttime, "sttime", "s7532screensfl", count)%></div>
									</td>



								</tr>
								<%
									count = count + 1;
										S7532screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="btn-group">
					<div class="sectionbutton">
						
							<a class="btn btn-info" href="#"
								onClick="JavaScript:perFormOperation(5)"><%=resourceBundleHandler.gettingValueFromBundle("Details")%></a>

						
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<BODY>
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span> <%
 	String sidebartext = "";
 %>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx01.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx01.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx01"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx01' id='optdscx01' type='hidden'
											value="<%=sv.optdscx01.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx02.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx02.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx02"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx02' id='optdscx02' type='hidden'
											value="<%=sv.optdscx02.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx03.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx03.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx03"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx03' id='optdscx03' type='hidden'
											value="<%=sv.optdscx03.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx04.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx04.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx04"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx04' id='optdscx04' type='hidden'
											value="<%=sv.optdscx04.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx05.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx05.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx05"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx05' id='optdscx05' type='hidden'
											value="<%=sv.optdscx05.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx06.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx06.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx06"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx06' id='optdscx06' type='hidden'
											value="<%=sv.optdscx06.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx07.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx07.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx07"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx07' id='optdscx07' type='hidden'
											value="<%=sv.optdscx07.getFormData()%>">
									</div>
								</li>
								<li>
									<%
										sidebartext = "";
									%>
									<div id='null'>
										<%
											if (sv.optdscx08.getInvisible() == BaseScreenData.INVISIBLE
													|| sv.optdscx08.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<%=sidebartext%>
										<%
											} else {
										%>
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optdscx08"))'
											class="hyperLink"> <%=sidebartext%>
										</a>
										<%
											}
										%>
									</div>
									<div>
										<input name='optdscx08' id='optdscx08' type='hidden'
											value="<%=sv.optdscx08.getFormData()%>">
									</div>
								</li>
							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>
<script>
	$(document).ready(function() {
		$('#dataTables-s7532').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

