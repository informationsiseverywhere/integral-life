<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S7538";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*" %>

<%S7538ScreenVars sv = (S7538ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-8">
		       		<div class="form-group">
		       		
		       		<table><tr><td>
		       		<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Transaction Process For a "))%>
		       		</td>
		       		<td></td>
		       		<td style='position:relative; bottom:3px; font-weight: bold; padding-left: 5px;'>
		       		<%=sv.shortdesc.getFormData() %>
		       		</td>
		       		</tr></table>
		       		
		       		</div>
		       	</div>
		    </div>
		    
		    <%	appVars.rollup(new int[] {93}); %>
			<% 
			 int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize=0;
			
			calculatedValue=36;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;
			
			calculatedValue=96;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;
			
			calculatedValue=384;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;
			
			calculatedValue=168;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
			
			calculatedValue=120;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;
			
			calculatedValue=72;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;
			
			calculatedValue=60;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;
			
			if(totalTblWidth>730){
					totalTblWidth=730;
			}
			arraySize=tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7538screensfl");
			GeneralTable sfl1 = fw.getTable("s7538screensfl");
			S7538screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if(sfl.count()*27 > 210) {
			height = 210 ;
			} else if(sfl.count()*27 > 118) {
			height = sfl.count()*27;
			} else {
			height = 118;
			}	
			%>
			
 		<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s7538'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%></th>
						<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%></th>
						<th><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp"))%></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <% String backgroundcolor="#FFFFFF";%>	
							<%
							S7538screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							boolean hyperLinkFlag;
							while (S7538screensfl.hasMoreScreenRows(sfl)) {	
							hyperLinkFlag=true;
							%>
							
							<tr id='tr<%=count%>' height="30">
							<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;"
												<%if((sv.sel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="center" <%}%> >
																										
																		
																	
																			
													
												 <% if((new Byte((sv.sel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){%>	 
												 <input type="radio" 
													 value='<%= sv.sel.getFormData() %>' 
													 onFocus='doFocus(this)' onHelp='return fieldHelp("s7538screensfl" + "." +
													 "sel")' onKeyUp='return checkMaxLength(this)' 
													 name='s7538screensfl.sel_R<%=count%>'
													 id='s7538screensfl.sel_R<%=count%>'
													 onClick="selectedRow('s7538screensfl.sel_R<%=count%>')"
													 class="radio"
													 
													 
											disabled="disabled"
													
													
												 />
												 <%}else{ %>
												 		<input type="radio" 
													 value='<%= sv.sel.getFormData() %>' 
													 onFocus='doFocus(this)' onHelp='return fieldHelp("s7538screensfl" + "." +
													 "sel")' onKeyUp='return checkMaxLength(this)' 
													 name='s7538screensfl.sel_R<%=count%>'
													 id='s7538screensfl.sel_R<%=count%>'
													 onClick="selectedRow('s7538screensfl.sel_R<%=count%>')"
													 class="radio"
													 />		
												
																		
																<%}%>
							
							
									
							<td style="width:<%=tblColumnWidth[1]%>px;" 
								<%if(!(((BaseScreenData)sv.procname) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
											<%if((new Byte((sv.procname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
														<%
															longValue=sv.procname.getFormData();
														%>
												 		<div id="s7538screensfl.procname_R<%=count%>" name="s7538screensfl.procname_R<%=count%>">
																<%=longValue%>
														</div>
														<%
															longValue = null;
														%>
														
													<%}%>
							</td>
							
							
							
							<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ] - 60 %>px;" 
												<%if((sv.longdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="left" <%}%> >									
															
																 
															
							
							
							
							<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
							<%
															longValue=sv.longdesc.getFormData();
														%>
												 		<div id="s7538screensfl.longdesc_R<%=count%>" align="left" name="s7538screensfl.longdesc_R<%=count%>">
																<%=longValue%>
														</div>
														<%
															longValue = null;
														%>
														
													<%}%>
							
							</td>
							</tr>
							<%	count = count + 1;
							S7538screensfl.setNextScreenRow(sfl, appVars, sv);
							}
							%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	 </div>
	 </div>

<script>
	$(document).ready(function() {
    	$('#dataTables-s7538').DataTable({
        	ordering: false,
        	searching:false
      	});
    });
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

