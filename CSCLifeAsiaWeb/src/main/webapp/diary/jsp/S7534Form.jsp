<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S7534";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.diary.screens.*"%>

<%
	S7534ScreenVars sv = (S7534ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("System Details"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company    "))%></label>
					<div class="input-group three-controller" style="width: 160px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.shortdesc, 1)%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<div class="input-group three-controller" style="width: 320px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.branch)%>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc, 1)%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Error Messages"))%></label>
				</div>
			</div>
		</div>
		<%
			appVars.rollup(new int[]{93});
		%>
		<%
			int[] tblColumnWidth = new int[22];
			int totalTblWidth = 0;
			int calculatedValue = 0;
			int arraySize = 0;

			calculatedValue = 90;
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			calculatedValue = 90;
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = 110;
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = 320;
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = 105;
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = 90;
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = 650;
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (totalTblWidth > 730) {
				totalTblWidth = 730;
			}
			arraySize = tblColumnWidth.length;
			GeneralTable sfl = fw.getTable("s7534screensfl");
			GeneralTable sfl1 = fw.getTable("s7534screensfl");
			S7534screensfl.set1stScreenRow(sfl, appVars, sv);
			int height = 350;
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s7534' width='100%'>
							<thead>
								<tr class='info'>
									<th>Effective Date</th>
									<th>Run Number</th>
									<th>Subroutine</th>
									<th>Company</th>
									<th>Branch</th>
									<th>Entity Type</th>
									<th>Entity</th>
									<th>System Date</th>
									<th>System Time</th>
									<th></th>
								</tr>
							</thead>

							<tbody>
								<%
									String backgroundcolor = "#FFFFFF";
								%>
								<%
									S7534screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (S7534screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
										sv.effdate.setEnabled(BaseScreenData.ENABLED);
								%>
								<%
									sv.effdateDisp.setClassString("");
								%>
								<%
									sv.effdateDisp.appendClassString("string_fld");
										sv.effdateDisp.appendClassString("output_txt");
										sv.effdateDisp.appendClassString("highlight");
								%>
								<%
									sv.runNumber.setClassString("");
								%>
								<%
									sv.runNumber.appendClassString("string_fld");
										sv.runNumber.appendClassString("output_txt");
										sv.runNumber.appendClassString("highlight");
								%>
								<%
									sv.subrname.setClassString("");
								%>
								<%
									sv.subrname.appendClassString("string_fld");
										sv.subrname.appendClassString("output_txt");
										sv.subrname.appendClassString("highlight");
								%>
								<%
									sv.diaryEntityCompany.setClassString("");
								%>
								<%
									sv.diaryEntityCompany.appendClassString("string_fld");
										sv.diaryEntityCompany.appendClassString("output_txt");
										sv.diaryEntityCompany.appendClassString("highlight");
								%>
								<%
									sv.diaryEntityBranch.setClassString("");
								%>
								<%
									sv.diaryEntityBranch.appendClassString("string_fld");
										sv.diaryEntityBranch.appendClassString("output_txt");
										sv.diaryEntityBranch.appendClassString("highlight");
								%>
								<%
									sv.diaryEntityType.setClassString("");
								%>
								<%
									sv.diaryEntityType.appendClassString("string_fld");
										sv.diaryEntityType.appendClassString("output_txt");
										sv.diaryEntityType.appendClassString("highlight");
								%>
								<%
									sv.diaryEntity.setClassString("");
								%>
								<%
									sv.diaryEntity.appendClassString("string_fld");
										sv.diaryEntity.appendClassString("output_txt");
										sv.diaryEntity.appendClassString("highlight");
								%>
								<%
									sv.endate.setClassString("");
								%>
								<%
									sv.endate.appendClassString("string_fld");
										sv.endate.appendClassString("output_txt");
										sv.endate.appendClassString("highlight");
								%>
								<%
									sv.entime.setClassString("");
								%>
								<%
									sv.entime.appendClassString("string_fld");
										sv.entime.appendClassString("output_txt");
										sv.entime.appendClassString("highlight");
								%>
								<%
									sv.errmsg.setClassString("");
								%>
								<%
									sv.errmsg.appendClassString("string_fld");
										sv.errmsg.appendClassString("output_txt");
										sv.errmsg.appendClassString("highlight");
								%>
								<%
									sv.logtype.setClassString("");
								%>
								<%
									sv.logtype.appendClassString("string_fld");
										sv.logtype.appendClassString("output_txt");
										sv.logtype.appendClassString("highlight");
										sv.logtype.setInvisibility(BaseScreenData.INVISIBLE);
								%>
								<%
									sv.screenIndicArea.setClassString("");
								%>
								<tr>
									<td style="width: 50px;"
										<%if (!(((BaseScreenData) sv.effdate) instanceof StringBase)) {%>
										align="center" <%} else {%> align="left" <%}%>><%=sv.effdateDisp.getFormData()%>
									</td>



									<td style="width: 50px;"
										<%if (!(((BaseScreenData) sv.runNumber) instanceof StringBase)) {%>
										align="center" <%} else {%> align="center" <%}%>><%=sv.runNumber.getFormData()%>
									</td>



									<td style="width: 50px;"
										<%if (!(((BaseScreenData) sv.subrname) instanceof StringBase)) {%>
										align="center" <%} else {%> align="center" <%}%>><%=sv.subrname.getFormData()%>
									</td>


									<style>
.s7534TdDiv div {
	padding-right: 0px !important;
	max-width: 90px !important;
}
</style>
									<td class="s7534TdDiv" style="width: 20px;"
										<%if (!(((BaseScreenData) sv.diaryEntityCompany) instanceof StringBase)) {%>
										align="right" <%} else {%> align="center" <%}%>>
										<div style="position: relative; margin-left: 1px;">
											<%=sv.diaryEntityCompany.getFormData()%>
										</div>
									<td class="s7534TdDiv" style="width: 40px;"
										<%if (!(((BaseScreenData) sv.diaryEntityCompany) instanceof StringBase)) {%>
										align="right" <%} else {%> align="center" <%}%>>
										<div style="position: relative; margin-left: 1px;">
											<%=sv.diaryEntityBranch.getFormData()%>
										</div>
									<td class="s7534TdDiv" style="width: 50px;"
										<%if (!(((BaseScreenData) sv.diaryEntityCompany) instanceof StringBase)) {%>
										align="right" <%} else {%> align="center" <%}%>>
										<div style="position: relative; margin-left: 1px;">
											<%=sv.shortdesc.getFormData()%>
										</div>
									<td class="s7534TdDiv" style="width: 50px;"
										<%if (!(((BaseScreenData) sv.diaryEntityCompany) instanceof StringBase)) {%>
										align="right" <%} else {%> align="center" <%}%>><%=smartHF.getHTMLF4SSVar(0, 0, sfl, sv.diaryEntityType).replace("absolute", "relative")
						.replace("style:", "style: margin-left:1px;")%>
										<div style="position: relative; margin-left: 1px;">
											<%=sv.diaryEntity.getFormData()%>
										</div></td>



									<td style="width: 80px;"
										<%if (!(((BaseScreenData) sv.diaryEntityBranch) instanceof StringBase)) {%>
										align="center" <%} else {%> align="center" <%}%>><%=sv.endate.getFormData()%>
									</td>



									<td style="width: 80px;"
										<%if (!(((BaseScreenData) sv.diaryEntityType) instanceof StringBase)) {%>
										align="center" <%} else {%> align="center" <%}%>><%=sv.entime.getFormData()%>
									</td>



									<td style="width: 245px;"
										<%if (!(((BaseScreenData) sv.diaryEntity) instanceof StringBase)) {%>
										align="center" <%} else {%> align="left" <%}%>><%=sv.errmsg.getFormData()%>
										<%=sv.logtype.getFormData()%></td>







								</tr>
								<%
									count = count + 1;
										S7534screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-s7534').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

