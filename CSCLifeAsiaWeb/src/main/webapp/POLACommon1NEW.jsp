<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page session="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.Map" %> 
<%@ page import="java.util.*" %>  
<%@ page import="java.util.HashMap" %>  
<%@ page import="com.quipoz.framework.screendef.QPScreenField" %>   
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%@ page import="com.csc.life.enquiries.screens.*" %> 
<%@ page import="com.csc.life.cashdividends.screens.*" %> 
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%@ page import="com.csc.life.terminationclaims.screens.*" %> 
<%@ page import="com.csc.smart.screens.*" %>
<%@ page import="com.csc.fsu.financials.screens.*" %>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatterNew"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="java.util.Map.Entry"%>
<%@ page import="com.csc.lifeasia.web.SessionCleaner1" %>
<%@ page import="com.csc.lifeasia.web.AutoHold" %>

<%@page import="com.csc.life.enquiries.screens.S6241ScreenVars"%>
<%@page import="com.csc.fsu.general.screens.Sr343ScreenVars"%>
<%@page import="com.csc.smart400framework.batch.cls.Qlrsetce"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="com.quipoz.framework.util.IntegralDMSConfig"%>
<%@page import="com.csc.fsuframework.core.FeaConfg"%>
<%@page import="com.csc.util.XSSFilter"%>

<html>
<head>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
//public static final boolean ieEmulationOn = true;	//ILIFE-1948 make emulation to be configurable
public static String emulationVer="off";			//ILIFE-1948
%>
<% 
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
response.setHeader("Pragma","no-cache"); //HTTP 1.0 
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
response.addHeader("X-Frame-Options", "SAMEORIGIN");
response.addHeader("Cache-Control", "private");/* IJTI-1212 */
response.addHeader("Cache-Control", "no-store");/* IJTI-1212 */
response.addHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains;"); /* JTI-1214 */
response.addHeader("X-Content-Type-Options", "nosniff");/* IJTI-1213 */
response.addHeader("X-Content-Security-Policy", "default-src 'self';");/* IJTI-1219 */
response.addHeader("X-XSS-Protection", "1; mode=block");/* IJTI-1215 */
%>
<title>Life Asia application</title>
<meta http-equiv="pragram" content="no-cache">
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
<!-- sb admin begin -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- sb admin end -->

<%
	String ctx = request.getContextPath() + "/";
//Move these variable here to judge system language.
	BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
	List<String> itemItem = AppVars.getInstance().getExcelExportScreensName(); //IGROUP-4864
	
  if ( baseModel == null) {
  	return;
  }
  ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
	String lan = av.getUserLanguage().toString().trim().toLowerCase();
	
	/* IGB-615 Start */
	Class<?> clazz = fw.getVariables().getClass();
	String screensfl = "s" + fw.getScreenName().substring(1) + "screensfl";
	
	Field field = null;
	try {
		field = clazz.getField(screensfl);
	} catch (Exception e){}
	String hasSubfile = "false";
	if(field!=null)
		hasSubfile = "true";
	/* IGB-615 End */
	
	//ILIFE-2143
	String screenNameTemp= fw.getScreenName();
	final String[] listIE11={"S5035", "S2201","S2200","Sr25c","Msgbox","S2085","S5002","S5004","S2464","S2465","Sr23x","S2501","Sr208","S6765","S5070","S6278","S2571","S2081","Sr595","S5005","S5006","S5123","Sr676","S5125","Sr516","S5003","Sr53a","Sr25m","Sr26j","Sr25j","Sr25k","S6774","Sr677","S5010"};
	boolean specialEmlation = true;
	
	for(String templistIE11:listIE11){
		if(screenNameTemp.equalsIgnoreCase(templistIE11)){
			specialEmlation = false;
			break;
		}
	}
	
if ("".equals(lan)){lan = "eng";}
	String localeimageFolder = lan;
	emulationVer = "off";	//ILIFE-1948
	// ILIFE-1807 add the brower version in common jsp
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10; //ILIFE-1948
		}
	} else if (userAgent.contains("Trident/7.0")) {
		browerVersion = IE11;
			if(AppConfig.ieEmulationEnable&&specialEmlation) emulationVer =  IE11;	//ILIFE-1948
	}
	//ILIFE-1948 begin
	if(emulationVer.equals(IE11) || emulationVer.equals(IE10))
	{
		%>
		<style type="text/css">
			.bold_cell,.input_cell{margin-top:-1px !important;}
			.iconPos{margin-top:0px !important;}
			div.blank_cell.iconPos{left:-1px !important;vertical-align:baseline !important;} /*for div after search button*/
			select.input_cell{background-color:white !important;}
		</style>
		<%			
	}
	//ILIFE-1948 end
	//ILIFE-2143
	if(browerVersion.equals(IE11)&&!specialEmlation){
%>
<style>
.iconPos{margin-top:0px !important;}
</style>
<%} %>
<!--  sb admin
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/QAStyle.jsp" TYPE="text/css"> 
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/QAStyle.css" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/tabpane.css" TYPE="text/css">
--> 
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/superTables.css" TYPE="text/css">  
<!-- sb admin
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/style.css" TYPE="text/css">-->
<!--  script language="javascript" src='<%//=ctx%>js/superTables.js'></script>--> <!-- ILIFE-1328 for the right imported src   --> 
<!-- Linking of SuperTable ends here -->
<!-- bootstrap YY <script language="javascript" src="<%=ctx%>js/menuG4f.js"></script> -->

<!-- sb admin begin -->
<link href="<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
<%-- <link href="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"> --%>
<link href="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.css" rel="stylesheet" media="screen">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables/css/dataTables.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
<script src='<%=ctx%>bootstrap/sb/vendor/jquery/jquery.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/dist/js/sb-admin-2.js'></script>
<%-- <script src="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.js" charset="UTF-8"></script> --%>
<script src="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.js" charset="UTF-8"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables/js/dataTables.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables/js/dataTables.bootstrap.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables-responsive/dataTables.responsive.js"></script>

<script src='<%=ctx%>bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src="<%=ctx%>bootstrap/integral/integral-admin.js"></script>
<!-- sb admin end -->


<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.quipoz.COBOLFramework.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.*" %>
<%@ page import="com.quipoz.framework.datatype.*" %>
<%@ page import="com.quipoz.COBOLFramework.TableModel.*" %>
<%@ page import="com.csc.lifeasia.runtime.variables.*"%>
<%@ page import="com.csc.smart400framework.SmartVarModel" %>
<%@ page import="com.csc.smart400framework.AppVersionInfo" %>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@ page import="java.lang.reflect.Field" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.quipoz.framework.util.DataModel"%>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<!-- ILIFE-4957 start -->
<style>

 #navigateButton
 {
display:none;
}
 
.container
{
display:none;
}


.modalmsg {
    display: none; 
    position: absolute; 
    z-index: 1000;  /*ICIL-348*/
    border: 1px solid #5085b5;
    left: 150px;
    top: 350px;
    width: 500px; 
    height: 130px; 
    overflow: auto;
    background-color: white;
}
.modalmsg-content {
    background-color: #fefefe;
    width: 400px;
    height: 80px;
}
/* ILIFE-5717 START*/
.confirmModal-content {
    background: white;
    position: absolute;
    left: 20%;
    top: 20%;
    width: 60%;
    height: 24%;
    border: 1px solid rgba(0, 0, 0, 0.3);
    border-radius: 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    text-align: center;
    z-index: 1000;
    
}
#confirmModal{
	display: none;
	width: 100%;
    height: 100%;
    position: absolute;
    z-index: 1000;
}
.regular-checkbox {
	display: none;
}
.regular-checkbox + label {
	background-color: #fafafa;
	border: 1px solid #cacece;
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
	padding: 9px;
	border-radius: 3px;
	display: inline-block;
	position: relative;
}
.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}
.regular-checkbox:checked + label {
	background-color: #e9ecee;
	border: 1px solid #adb8c0;
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
	color: #99a1a7;
}
.regular-checkbox:checked + label:after {
	content: '\2714';
	font-size: 14px;
	position: absolute;
	top: -10px;
	left: 3px;
	color: #99a1a7;
}
.big-checkbox + label {
	padding: 12px;
}
.big-checkbox:checked + label:after {
	font-size: 28px;
	left: 2px;
}
/* ILIFE-5717 END */
</style>
<!-- ILIFE-4957 end -->
</head>
<%!
     public String formatValue(String aValue) { 
     
     return aValue;
     	/*
     	 This method was converting  o to space
     	 now commented
     	char[] valueCharArray = new char[aValue.length()];
     	boolean nonZeroFlag = false; 
     	
     	if(!aValue.trim().equalsIgnoreCase("")) {
     			valueCharArray = aValue.toCharArray();
     			for (int i = 0; i < valueCharArray.length; i++) {
						if(valueCharArray[i] != '0')  {
							nonZeroFlag =  true;
							break;
						}							
				}     	
     	} else {
     	
     	return " ";
     	
     	}
     		
     	if(nonZeroFlag) { 
     		return aValue;
     	}
     	else {
     		return " ";
     	}
      */
     }

/* INTEGRAL IPB-252  START*/
public void sendNotificationsToAll(String sessionID, String msg) {	
	SessionCleaner1 obj = new SessionCleaner1();
	obj.sendNtifictnMessage(sessionID, msg);
}

public void unholdSystem() {	
	SessionCleaner1 obj = new SessionCleaner1();	
	obj.stopNotifications();
}
/* INTEGRAL IPB-252  END*/
public void sendNotificationsToAllForSchedule(String sessionID,String User,String msg)
{
	AutoHold.scheduleHold( sessionID, msg);
}

public void ReleaseSchedular()
{
	AutoHold.stopHoldSchedule();
}
/* INTEGRAL IPB-253  START*/
public void clearSelectedSessions(List killSessionList) {	
	SessionCleaner1 obj = new SessionCleaner1();
	obj.clearSelectedSessions(killSessionList);	
}
//ILIFE-8507 starts
public void clearAllSessions(String sessionID) {	
	SessionCleaner1 obj = new SessionCleaner1();
	obj.clearAllSessions(sessionID);	
}
//ILIFE-8507 ends
public void sendSelectedNtifictnMessage(List remindMessageList, String ntfmsg) {	
	SessionCleaner1 obj = new SessionCleaner1();
	obj.sendSelectedNtifictnMessage(remindMessageList, ntfmsg);
}
/* INTEGRAL IPB-253  END*/

//ILIFE-416 Begins
public int getLength(FixedLengthStringData str)
{

	if((str.length() == 0)||(str.getFormData().trim().length()==0))
	{	
		return 60;
	}/*else if(((str.length()*9)>120)||(str.getFormData().trim().length()>120))
	{
		return 120;
	}*/else
	{
		if((str.length())<(str.getFormData().trim().length()))
		{
			return (str.length()*5+25); //((str.length()*9)<12?12:str.length()*9);
		}else
		{
			return (str.getFormData().trim().length()*5+25); //((str.getFormData().trim().length()*9)<12?12:str.getFormData().trim().length()*9);
		}
	}

}

public int getLength(ZonedDecimalData str)
{
	
	if((str.getLength() == 0)||(str.getFormData().trim().length()==0))
	{	
		return 60;
	}/*else if(((str.getLength()*9)>120)||(str.getFormData().trim().length()>120))
	{
		return 120;
	}*/else
	{
		if((str.getLength())<(str.getFormData().trim().length()))
		{
			return (str.getLength()*5+25); //((str.getLength()*9)<12?12:str.getLength()*9);
		}else
		{
			return  (str.getFormData().trim().length()*5+25); //((str.getFormData().trim().length()*9)<12?12:str.getFormData().trim().length()*9);
		}
	}

}
//ILIFE-416 ENDS


     
     public  String getKeysFromValue(Map hm, String value){
     Set set= hm.keySet(); 
     Iterator it = set.iterator();
	    while(it.hasNext()){
	       Object o=it.next();
	        if(((String)hm.get(o)).equals(value)) {
	            return (String)o;
	        }
	    }
	    return null;
	  }
     
     //Amit for sorting
     class KeyValueBean implements Comparable{
     
     private String key;
     
     private String value;

     
	public KeyValueBean(String key, String value) {
		this.key = key;
		this.value = value;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int compareTo(Object o) {
		return this.value.compareTo(((KeyValueBean)o).getValue());
	}


	public String toString() {
		
		return "Key is "+key+" value is "+value;
	}
     
    }
     
     //secoond class
     
     public class KeyComarator implements Comparator{

		public int compare(Object o1, Object o2) {
			
			return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
		}

	}
	//ILIFE-416 BEGINS
 	public String makeDropDownList(String val,String longValue,String strKeyValue){
		ArrayList keyValueList=makeList(val) ;
		String opValue = "";
		
		int size = keyValueList.size();
		opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" +  "</option>"; 
		String mainValue ="";
		//Collections.sort(keyValueList);
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
					+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";
					
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
					+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			} 
		} 
		return opValue;
	}     
	//ILIFE-416 ENDS
 	public String makeDropDownList(Map mp, Object val, int i) {

		String opValue = "";
		Map tmp = new HashMap();
		tmp = mp;
		String aValue = "";
		if (val != null) {
			if (val instanceof String) {
				aValue = ((String) val).trim();
			} else if (val instanceof FixedLengthStringData) {
				aValue = ((FixedLengthStringData) val).getFormData().trim();
			}
		}

		Iterator mapIterator = tmp.entrySet().iterator();
		ArrayList keyValueList = new ArrayList();

		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
			keyValueList.add(bean);
		}

		int size = keyValueList.size();

		opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" + "</option>";
		String mainValue = "";
		
		//Option 1 fr displaying code
		if (i == 1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList, new KeyComarator());
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
				}
			}
		}
		//Option 2 for long description
		if (i == 2) {
			Collections.sort(keyValueList);

			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		//Option 3 for Short description
		if (i == 3) {
			Collections.sort(keyValueList);
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}

		//Option 4 for format Code--Description
		if (i == 4) {
			Collections.sort(keyValueList);
			opValue = "";
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" + keyValueBean.getValue()
							+ "\" title=\"" + keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
							+ keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" + keyValueBean.getValue()
							+ "\" title=\"" + keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--"
							+ keyValueBean.getValue() + "</option>";
				}
			}
		}
	
		return opValue;
		
	}
 	/* This method is added to solve the bugzilla defect 1838 --(to solve the refresh issue in subfiles)*/
	public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

		String opValue = "";
		Map tmp = new HashMap();
		tmp = mp;
		String aValue = "";
		if (val != null) {
			if (val instanceof String) {
				aValue = ((String) val).trim();
			} else if (val instanceof FixedLengthStringData) {
				aValue = ((FixedLengthStringData) val).getFormData().trim();
			}
		}

		Iterator mapIterator = tmp.entrySet().iterator();
		ArrayList keyValueList = new ArrayList();

		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
			keyValueList.add(bean);
		}

		int size = keyValueList.size();

		String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
		opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
				+ strSelect + "---------" + "</option>";
		String mainValue = "";
		//Option 1 fr displaying code
		if (i == 1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList, new KeyComarator());
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
				}
			}
		}
		//Option 2 for long description
		if (i == 2) {
			Collections.sort(keyValueList);

			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		//Option 3 for Short description
		if (i == 3) {
			Collections.sort(keyValueList);
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		//Option 4 for format Code--Description
		if (i == 4) {
			Collections.sort(keyValueList);

			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
							+ keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
							+ "</option>";
				}
			}
		}
		return opValue;
	}
 	
	//Start IFSU-168
	public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler, String lang) {

		String opValue = "";
		Map tmp = new HashMap();
		tmp = mp;
		String aValue = "";
		if (val != null) {
			if (val instanceof String) {
				aValue = ((String) val).trim();
			} else if (val instanceof FixedLengthStringData) {
				aValue = ((FixedLengthStringData) val).getFormData().trim();
			}
		}

		Iterator mapIterator = tmp.entrySet().iterator();
		ArrayList keyValueList = new ArrayList();

		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
			keyValueList.add(bean);
		}

		int size = keyValueList.size();

		String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
		opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
				+ strSelect + "---------" + "</option>";
		String mainValue = "";

		//format Description but with 3 Char Codes
		
		if (i == 1) {			
			Collections.sort(keyValueList);
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				String itemLang;
				itemLang = keyValueBean.getKey().substring(0,1).toString().trim().toUpperCase();
				
					//IFSU-394 START by dpuhawan
					//if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					if (keyValueBean.getKey().equalsIgnoreCase(lang + "" + aValue)) {
					//IFSU-394 END
					
						if (lang.equals(itemLang))
						//IFSU-394 START by dpuhawan
						//opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1) + "^" + keyValueBean.getValue()
						opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1)
						//IFSU-394 END
								+ "\" title=\"" + keyValueBean.getValue() + "\" SELECTED>" + 
								 keyValueBean.getValue() + "</option>";
							
					} else {
						if (lang.equals(itemLang))
						//IFSU-394 START by dpuhawan
						//opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1) + "^" + keyValueBean.getValue()
						opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1)
						//IFSU-394 END
								+ "\" title=\"" + keyValueBean.getValue() + "\">" + 
								 keyValueBean.getValue() + "</option>";
					}
				
			}
		}				
		return opValue;
	} 	
	//End IFSU-168
	
	public String makeDropDownList1(Map mp, Object val, int i) {

		String opValue = "";
		Map tmp = new HashMap();
		tmp = mp;
		String aValue = "";
		if (val != null) {
			if (val instanceof String) {
				aValue = ((String) val).trim();
			} else if (val instanceof FixedLengthStringData) {
				aValue = ((FixedLengthStringData) val).getFormData().trim();
			}
		}

		Iterator mapIterator = tmp.entrySet().iterator();
		ArrayList keyValueList = new ArrayList();

		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
			keyValueList.add(bean);
		}

		int size = keyValueList.size();

		opValue = opValue + "<option value='    ' title='---------Select---------' SELECTED>---------Select---------"
				+ "</option>";
		String mainValue = "";
		//Option 1 fr displaying code
		if (i == 1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList, new KeyComarator());
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
				}
			}
		}
		//Option 2 for long description
		if (i == 2) {
			Collections.sort(keyValueList);

			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		//Option 3 for Short description
		if (i == 3) {
			Collections.sort(keyValueList);
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		return opValue;

	}

	public String makeDropDownList1(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

		String opValue = "";
		Map tmp = new HashMap();
		tmp = mp;
		String aValue = "";
		if (val != null) {
			if (val instanceof String) {
				aValue = ((String) val).trim();
			} else if (val instanceof FixedLengthStringData) {
				aValue = ((FixedLengthStringData) val).getFormData().trim();
			}
		}

		Iterator mapIterator = tmp.entrySet().iterator();
		ArrayList keyValueList = new ArrayList();

		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
			keyValueList.add(bean);
		}

		int size = keyValueList.size();
		String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
		opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
				+ strSelect + "---------" + "</option>";
		String mainValue = "";
		//Option 1 fr displaying code
		if (i == 1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList, new KeyComarator());
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
				}
			}
		}
		//Option 2 for long description
		if (i == 2) {
			Collections.sort(keyValueList);

			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		//Option 3 for Short description
		if (i == 3) {
			Collections.sort(keyValueList);
			for (int ii = 0; ii < size; ii++) {
				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
				if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
				} else {
					opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
							+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
				}
			}
		}
		return opValue;

	}
    
    public String makeDropDownList(ArrayList keyValueList, String strKeyValue) {
        String opValue = "";
        int size = keyValueList.size();

        String mainValue = "";
        //Collections.sort(keyValueList);
        for (int ii = 0; ii < size; ii++) {
            KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
            if (keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
                opValue = opValue + "<option value=\"" + keyValueBean.getKey()
                        + "^" + keyValueBean.getValue() + "\" title=\""
                        + keyValueBean.getValue() + "\" SELECTED>"
                        + keyValueBean.getKey() + "--"
                        + keyValueBean.getValue() + "</option>";

            } else {
                opValue = opValue + "<option value=\"" + keyValueBean.getKey()
                        + "^" + keyValueBean.getValue() + "\" title=\""
                        + keyValueBean.getValue() + "\">"
                        + keyValueBean.getKey() + "--"
                        + keyValueBean.getValue() + "</option>";
            }
        }
        return opValue;
    }
    //ILIFE-416 BEGINS
	public ArrayList makeList(String val){
		String strValues[]=val.split(",");
		ArrayList keyValueList= new ArrayList();
		String opValue = "";
		if(strValues!=null){
			for(int i=0;i<strValues.length;i++){
				String strValueString[]=strValues[i].split("-");
				if(strValueString!=null){
					KeyValueBean bean = new KeyValueBean(strValueString[1],strValueString[0]);
					keyValueList.add(bean);
				}
			}
		}
		return keyValueList;
	}    
	public String createDiscription(String val,String strKeyValue){
		ArrayList keyValueList=makeList(val) ;
		int size = keyValueList.size();
		String longValue="";
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				longValue=keyValueBean.getValue();
			}		
		} 
		if(longValue.equals("")){
			longValue=strKeyValue;
		}
		return longValue;
	}
	 public String makeLongDescription(Map mp , Object aValue ) {   
        
     		
     		Map tmp= new HashMap(); 
			tmp = mp;			 
			String longDesc ="";
			
			
		
				longDesc = (String)tmp.get("BB");
			return longDesc; 
	}      
	//ILIFE-416 ENDS 
    
    public String createDiscription(ArrayList keyValueList, String strKeyValue) {
        int size = keyValueList.size();
        String longValue = "";
        for (int ii = 0; ii < size; ii++) {
            KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
            if (keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
                longValue = keyValueBean.getValue();
            }
        }
        if (longValue.equals("")) {
            longValue = strKeyValue;
        }
        return longValue;
    }
%>
<%
	/* BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel == null) {
    	return;
    }
    ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables(); */
	//SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter)AppVars.hf;
	LifeAsiaAppVars appVars = av;
	boolean[] savedInds = null;
	if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */
	av.reinitVariables();
	String lang = av.getInstance().getUserLanguage().toString().trim();
	localeimageFolder = lang;
	String dtFormat =  av.getInstance().getDtFormat();
	/* Added For Internationalization */

	String tit = "";
	QPScreenField qpsf = null;
	DataModel sm = null;
	String formatValue = null;
	String valueThis = null;
	String longValue = null;

	SMARTHTMLFormatterNew smartHF = null;

		smartHF = new SMARTHTMLFormatterNew(
				fw.getScreenName(), lang);

	
	/* SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(
			fw.getScreenName(), lang); */
	smartHF.setLocale(request.getLocale());// used to store locale in smartHF

	//modified for the I18N of integral life. 
	//ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),lang);

	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(
			fw.getScreenName(), request.getLocale(), appVars.getCompany().toString());

	//smartHF.setName(fw.getScreenName());
	//smartHF.setLang(lang);

	//StringDataHelper stringDataHelper = new StringDataHelper(fw.getScreenName(),lang);

	/** if (lang.equalsIgnoreCase("ENG")) {
		lang = "E";
	} else {
		lang = "S";
	}*/

	Map fieldItem = new HashMap();//used to store page Dropdown List
	String[] dropdownItems = null; //used to store page Dropdown List
	String[][] dropdownItemsUIG = null;   //used to store page Dropdown List
	Map mappedItems = null;
	String optionValue = null;

	String imageFolder = PropertyLoader.getFolderName(smartHF
			.getLocale().toString());//used to fetch image folder name.
	smartHF.setFolderName(imageFolder);
%>
<!-- added following section for the I18N of integral life.  -->

<%
	/* String titTemp =""; */
	String screenNumber = this.getClass().getSimpleName();
	if (screenNumber.endsWith("_jsp")) {
		screenNumber = QPUtilities.removeTrailing(screenNumber, "_jsp");
	} else {
		screenNumber = screenNumber.substring(1);
	}

	//MIBT-57 STARTS
	if(screenNumber.startsWith("_")){
		screenNumber = screenNumber.replaceFirst("_","");
	}
	//MIBT-57 ENDS
		tit = tit
				+ (av.formatTitle(fw.getPageTitle(),
						(SmartVarModel) fw.getVariables())).replaceAll(
						"&nbsp;", "");
		tit = resourceBundleHandler.gettingValueFromBundleforTitle(tit);
	

	smartHF.setContinueButtonValue(resourceBundleHandler
			.gettingValueFromBundle("Continue"));
	smartHF.setRefreshButtonValue(resourceBundleHandler
			.gettingValueFromBundle("Refresh"));
	smartHF.setPreviousButtonValue(resourceBundleHandler
			.gettingValueFromBundle("Exit"));
	smartHF.setExitButtonValue(resourceBundleHandler
			.gettingValueFromBundle("Previous"));
%>

<!-- bootstrap -->
<body id="mainBody" onload="doLoad();" onKeyDown="return checkAllKeys();" <%="onResize='doResize();'"%> onHelp="return screenHelp();" onClick='doClick(this);' onBeforeUnload='return doCheck()' onUnload='doRedirect()' unUnload='doCancel()'>
	<!-- ILIFE-4957 start -->
<div id="polaModal" class="modalmsg" >
  <div class="modalmsg-content">
    <p id="errorMsg" style = "padding-top:25px;padding-left:35px"></p>
    <BR><BR>
    <input id="bttCloseModel" type="button" style="margin-left:220px;width:50px;height:24px;" value="OK" onclick="javascript:closeModelMsg();">
  </div>
</div>
<!-- ILIFE-4957 end -->
<!-- ILIFE-5717 START -->
<div id="confirmModal" class="modalmsgC" >
	<div class="confirmModal-content" id="confirmModal-content">
		<p id="confirmMsg" style = "padding-top:25px;">
		</p>
		<br>
	  	<div id="divConfirmation">
	  	</div>
	  	<br>
	  	<input id="btnOk" type="button" style="width:65px;height:25px; border-radius: 5px" 
			value="Yes" onclick="doConfirm('Yes');">
		<input id="btnCancel" type="button" style="margin-left:10%;width:65px;height:25px;border-radius: 5px" 
			value="No" onclick="doConfirm('No');">
	</div>
</div>
<!-- ILIFE-5717 END -->		
<%@ include file="commonScript1NEW.jsp"%>
<%DecimalFormat df = (DecimalFormat)DecimalFormat.getNumberInstance(av.getLocale());
DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
dfs = df.getDecimalFormatSymbols();
%>
<%
	if (baseModel == null) {
		return;
	}
	av.reinitVariables();
	String userID1=(String)request.getSession().getAttribute("LONGUSERID");//IGROUP-4177 //IGROUP-4229
	String lastLoginTime1=(String)request.getSession().getAttribute("LastLoginTime");
	if ("never".equals(lastLoginTime1)) {
        lastLoginTime1 = "";
	}
	else
	{
		String[] time=lastLoginTime1.split(" ");
		if(time.length>1){
		    lastLoginTime1=resourceBundleHandler.gettingValueFromBundle("Login time :")+"&nbsp;"+time[0]+"&nbsp;"+resourceBundleHandler.gettingValueFromBundle("At")+"&nbsp;"+time[1];
		}else{
		    lastLoginTime1=resourceBundleHandler.gettingValueFromBundle("Login time :")+"&nbsp;"+lastLoginTime1;
		}
	}
%>
<%-- <div id="topPanel" class="row" style="border:0px;margin:0 0 0 0;background-color:#000000 !important; height: 50px">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0;margin-top: -2px !important;background-color: #000000;">
			<%if(fw.getScreenName().equals("S2465") ||fw.getScreenName().equals("S5004")&& !browerVersion.equals(Chrome)){%>
			<ul class="nav navbar-top-links navbar-right" style="background-color: #000000;margin-top: 0px !important;margin-right: 70px !important;">
			<%}else{ %>
			<ul class="nav navbar-top-links navbar-right" style="background-color: #000000;margin-top: 1px !important;margin-right: 70px !important;">	
			<%} %>	
				<!-- IGROUP-4864 Start --> 
				<%if(null != itemItem && itemItem.contains(fw.getScreenName())){ %> <!-- check Excel file download icon enable or disable -->
						 <li><a href="javascript:;" onClick="exportExcel();doAction('PFKEY69')" title="Download SFL data in csv file"><img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/eng/exceldownload.png" width="25px" border="0"/></a></li> 
					<%}%>
					<!-- IGROUP-4864 End -->
				<%
					//IJTI-1625
					if (av.getAppConfig().isIDMFlagEnabled()) {
				%>
				<li><a href="<%=IntegralDMSConfig.getDMSURL()%>"
					target="_blank" style="color: #fff; font-size: 20px !important;"
					title="<%=resourceBundleHandler.gettingValueFromBundle("DMS")%>"><i
						class="fa fa-file" style="color: white;"></i></a></li>
				<%} %>

				<% 
			//IJTI-1153 starts
			if(FeaConfg.isFeatureExist("2", "UNDNB071", av,"IT")){ %>
					<li><a href="<%= IntegralConfig.getSisenseURL()%>" target="_blank" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Sisense")%>"><i class="fa fa-bar-chart" style="color:white;"></i></a></li>
				<%} 
				//IJTI-1153 ends
				%>
				<!-- ILIFE-8126 -->
				<% if(FeaConfg.isFeatureExist("2", "ITEMTEST", av,"IT")){ %>
				<li><a href="javascript:;" onClick="window.open('../../tableData.jsp','_blank');" style="color: #fff;font-size:20px !important;" title="Item Test"><i style="font-size:14px;" >Item Test</i></i></a></li>
				<%} %> 
				<!-- ILIFE-7886 -->
				 <!-- ILIFE-8752 start -->
				 <li><a href="<%= AppVars.getInstance().getAppConfig().getPsUrl()%>" target="_blank" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("PS")%>"><i class="fa fa-file" style="color:white;"></i></a></li>
			    <li><a href="<%= AppVars.getInstance().getAppConfig().getPsUrl()%>" target="_blank" style="color: #fff;font-size:20px !important;top:2px;" title="<%=resourceBundleHandler.gettingValueFromBundle("Product Stage")%>"><i class="fa fa-briefcase" style="color:white;"></i></a></li>
				 <!-- ILIFE-8752 end -->
				 <li><a href="javascript:;" onClick="doAction('PFKEY16')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Home")%>"><i class="fa fa-home" style="color:white;"></i></a></li>
				<li><a href="javascript:;" onClick="doAction('PFKEY02')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Session Info")%>"><i class="fa fa-info-circle" style="color:white;"></i></a></li>
				<li><a href="javascript:;" onClick="doAction('PFKEY01')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Help")%>"><i class="fa fa-question-circle" style="color:white;"></i></a></li>
				<li><a href="javascript:;" onClick="kill();return false;" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Logout")%>"><i class="fa fa-sign-out" style="color:white;"></i></a></li>
				<li class="userInfo"  title="<%=lastLoginTime1%><%=resourceBundleHandler.gettingValueFromBundle("Business Date")%>:<%=AppVars.getInstance().getBusinessdate()%>">
				    <a href="#">               
				        <span class="glyphicon glyphicon-user" style="color:white;"></span>
				         <span style="padding-left:10px;color:white;"><%=userID1%></span> <!-- IGROUP-4177 -->
				    </a>
				</li>
			</ul>
		</nav>
</div> --%>
	<div id="mainareaDiv">
		<%-- ILIFE-8864 --%>
		<div id="headerTopLink"
			style="top: 2px; left: 1px; right: 1px; z-index: 5;" class="header">
			<!-- IGB-615 Start -->
			<input type="hidden" name="sflheaderList" id="sflheaderList">
			<input type="hidden" name="sflVarList" id="sflVarList">
			<%-- ILIFE-8864 --%>

		</div>
	</div>
	<%
		String screenN2 = this.getClass().getSimpleName();
		if (screenN2.endsWith("_jsp")) {
			screenN2 = QPUtilities.removeTrailing(screenN2, "_jsp");
		} else {
			screenN2 = screenN2.substring(1);
		}
		screenN2 = QPUtilities.removeTrailing(screenN2, "Form");
	%>
<!-- AWPL-Integral Integration related changes start -->
<% if(request.getSession().getAttribute("sourceSystem") != null && "awpl".equalsIgnoreCase((String)request.getSession().getAttribute("sourceSystem"))) { %>
	<input type="hidden" value=',<%=av.getAppConfig().getComponentDetailsByScreenName(screenN2)%>,' id="components" name="components" />
<%}%>	
<!-- AWPL-Integral Integration related changes end -->

<script language='javaScript'>
	/* ILIFE-1328 remove the addBtnText_ENG and removeBtnText_ENG, because they defined in the JS by xma3*/
	/*  addBtnText_ENG =*/ <%//=resourceBundleHandler.gettingValueFromBundle("addBtnText")%>/*;*/
	/* removeBtnText_ENG = '*/<%//=resourceBundleHandler.gettingValueFromBundle("removeBtnText")%>/*';*/

$(document).ready(function() { 
	 $('.container').css("display","block"); 
	 $('#navigateButton').css("display","block"); 
});
	
var decimalSeparator = '<%=dfs.getDecimalSeparator()%>';
var groupingSeparator ='<%=dfs.getGroupingSeparator()%>';
var amountSepFlag = '<%=av.getInstance().getAppConfig().localAmountSeparatorFlag%>';
var localeLoc = '<%=av.getLocale()%>';

var imageFolder = '<%=imageFolder%>';
moreBtnText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("moreBtnText")%>';
rowNumText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("rowNumText")%>';
var holdMsgFlag = '<%=av.getHoldMsgNeeded()%>';
var notifyMessage = '<%=av.getNotifyMessage()%>'; 
var holdMsgTimeDrtn = '<%=av.getInstance().getAppConfig().holdLogoutTimeInMin%>';
var holdMsgSysTime = '<%=av.getHoldDateTm()%>';
</script>

<script type="text/javascript">		
	function kill(language) {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm(language,"No00031"));
		if (x == true) {
			// fix bug147
			top.frames["heartbeat"].frames["heart"].clearTimeout(
				top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}
</script>


<script language='javaScript'> 
$(document).ready(function() { 
	
	   window.scrollTo(0,0);
	//ILIFE-1948 begin
	<%if(emulationVer.equals(IE11) || emulationVer.equals(IE10)){%>
		$(".iconPos").parent().css("display","inline-block");
		$(".input_cell").css("color","");
		$("input[class*=bold_cell]").css("color","");
	<%}%>
	//ILIFE-1948 end	
	//set the policy header information to the newly added fields	
	//$("#screenTitleP").text($("#invisibletitle").val());
	//set error tab
	var i=1;
	var j=0;
	while (document.getElementById("content"+i) != null) {
		var isError = false;
		var element=document.getElementById("content"+i);
		$("#content"+i).find("input,div").each(function(j) {
		    if ($(this).hasClass("red reverse")||$(this).css("background-color")=="#ff0000")
				isError = true;
		});
		if (isError){
			error_display(true,i);
			if(j==0){
				j=i;
			}
		}else{
			error_display(false,i);
		}
		i++;
	}
	if(i>0&&j>0){//When the page has tab and error fields,set the error tab as current tab. 
		if($("input[name='tabcurrent']").length!=0){
			$("input[name='tabcurrent']").val((i-1)+","+j);
		}
	}
	changeScrollBar("subfo");
	//set default tab
	if($("input[name='tabcurrent']")!=null && $("input[name='tabcurrent']").length!=0){
		var curtab=$("input[name='tabcurrent']").val();
		var comp=curtab.indexOf(",");		
		if(comp>0){
			var number=parseInt($.trim(curtab.substring(0,comp)));
			var index=parseInt($.trim(curtab.substring(comp+1)));
			if(j>0){
				change_option(number,j);
			}else{
				change_option(number,index);
			}
		}
	}
	
	// AWPL-Integral Integration related changes starts
	<% if(request.getSession().getAttribute("sourceSystem") != null && 
		"awpl".equalsIgnoreCase((String)request.getSession().getAttribute("sourceSystem"))) { %>
		jQuery("img,select,input").each(function(){
			var iid = this.id;
			if(iid=="") {
				this.id = this.name;
			}
		});
	
	 
		jQuery("<%=av.getAppConfig().getConfigComponentsByScreenName(screenN2)%>").focus(function () {
			var compons = document.getElementById("components").value;
			var index = compons.search(','+this.id+':');
			var endIndex = compons.indexOf(',',index+1);				
			var searchedComponent = compons.substring(index,endIndex);
			var component = searchedComponent.substring(searchedComponent.indexOf(':')+1,searchedComponent.indexOf(";"));
			// For Question section we have to pass the question id. Question id is configured in ALT attribute in jsp.
			// So if this value is set as 'true', question id will be retrieved from alt and passed. 
			if(searchedComponent.indexOf(";true") != -1) {
				component = this.alt;								
			}
			//alert(component);
			self.parent.top.dsiaMainframe.showAnnotationDSIA(component);
		});
	<% } %>	
	// AWPL-Integral Integration related changes ends
	
	 // set focus evt in select 
	jQuery("select,input:radio").focus(function () {
		doFocus(this);
	});
	//#ILIFE-ILIFE-1328 modify the removeAttribute to removeAttr method for Jquery by xma3 
	// Ticket #ILIFE-132 - remove the color style from reverse.htc
	jQuery("input").each(function(){
		if(this.type == "text"){
			$(this).removeAttr('color');
	    }
	});
	
});

//This function deal with "locate to the record at the begining of the next page".
	function changeScrollBar(thi){
		if(document.getElementById(thi) != null) {
			var obj = document.getElementById(thi);
			obj.scrollTop = obj.scrollHeight;
		}	
	}

 var isfirstTime=true;
	
	//ILIFE-943
	var isConfirmFlag=false;
	function doCheck() {
		keyClosing = (event.altKey == 1 || event.ctrlKey == 1) && thisKey == 115;
		if (!submitted && (event.clientY <= 0 || keyClosing))  {
			event.returnValue = "Your transactions will be rolled back and you will be logged off."; 
			//ILIFE-943
			isConfirmFlag = true;
		}
	}
	function doCancel() {
		window.showModelessDialog ("<%=cs1ctx%>/AutoKill.jsp", null, "dialogWidth:640px; dialogHeight:420px; resizable:yes;");
	}
	
	//ILIFE-943 starts
	function doRedirect(){
		if (isConfirmFlag==true) {
			top.frames["heartbeat"].frames["heart"].clearTimeout(
					top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}
	//ILIFE-943 ends
	
	//Deal with the hyperlink which related to two fields.	
	function hyperLinkTo(nextField){
			nextField.value="X";
			doAction('PFKEY0');
	}
	
	function error_display(ind,index)  
	{
		if(ind == true){
			if(document.getElementById('current' + index).className == 'tabcurrent') {
				document.getElementById('current' + index).className = 'tabcurrenterror';
			} else {
				document.getElementById('current' + index).className = 'taberror';
			}
		} 
			
	}
	
	
	//Text counter is used to control the textarea	
	function textCounter(theField,maxChars,maxLines,maxPerLine){
		var strTemp = "";
		var strLineCounter = 0;
		var strCharCounter = 0;

		for (var i = 0; i < theField.value.length; i++){
				var strChar = theField.value.substring(i, i + 1);
				if (strChar == '\n'){
					strTemp += strChar;
					strCharCounter = 1;
					strLineCounter += 1;
				}
		
				else if (strCharCounter == maxPerLine){
					strTemp +=  strChar;
					strCharCounter = 1;
					strLineCounter += 1;
				}		
				else{
					strTemp += strChar;
					strCharCounter ++;
				}
			}
			
			if(maxChars<=strTemp.length||maxLines<=strLineCounter){
				theField.value=strTemp.substring(0,maxChars-1);				
				//alert("You have entered to much characters!");
				callCommonAlert(<%=imageFolder%>,"No00035");
				theField.focus();
				return false;
			}
	}

	//Show help information DIV
	//475PX;
	
	function setVisible(divName){
	if(document.getElementById(divName).style.visibility=="hidden"){
	document.getElementById(divName).style.visibility="visible";	
	}else{	
	return false;
	}
	}
	
	function hideHelpDiv(divName){
	// if(!timeoutProcess){
	 clearTimeout(timeoutProcess);	 
	// }
	if(document.getElementById(divName)==null){
		return false;
	}
	if(document.getElementById(divName).style.visibility=="visible"){
	document.getElementById(divName).style.visibility="hidden";
	//alert(document.getElementById(divName));
	document.body.removeChild(document.getElementById(divName));
		
	}else{
	return false;
	}
	
	
	}
	
	//F4 Items
	var f4fieldn="";
	function findF4field(fieldName){
		var data=fieldName+",<%=screenN2+","+lang%>";
		f4fieldn=fieldName+"dropdown";
		testClass.searchFieldString(data,displaySearchField);
	}
	
	function adjustitem(itemName){
	 if ( window.event.keyCode == '9' //tab
   || window.event.keyCode == '13' //enter
   || window.event.keyCode == '16' //shift
   || window.event.keyCode == '17' //ctrl
   || window.event.keyCode == '18' //alt
   || window.event.keyCode == '20' //caps lock
   || window.event.keyCode == '27' //esc
   || window.event.keyCode == '33' //page up
   || window.event.keyCode == '34' //page down
   || window.event.keyCode == '35' //end
   || window.event.keyCode == '36' //home
   || window.event.keyCode == '37' //left arrow
   || window.event.keyCode == '38' //up arrow
   || window.event.keyCode == '39' //right arrow
   || window.event.keyCode == '40' //down arrow
   || window.event.keyCode == '45' //print screen
   || window.event.keyCode == '93' //windows right-click
   || window.event.keyCode == '96' //ins key
   || (window.event.keyCode >= '112' && window.event.keyCode <= '123')   //down arrow
   || window.event.keyCode == '144' //num lock
   || window.event.keyCode == '145' //pause
   || lastKey              == '18' //alt keys
    )
   return true;
   
	var selectName=itemName+'dropdowns';
	var discrName=itemName+'discr';
	displaySearchField(itemName);
	//alert(selectName+','+discrName);
	var nowValue=document.getElementById(discrName).value;
	//alert(nowValue);
	nowValue=nowValue+"";
	nowValue=nowValue.Trim();
	var selectTag=document.getElementById(selectName);   
	     
	var colls=selectTag.options;
	var slength=colls.length;
	//alert(slength);
	//alert(colls(0).text.indexOf('O'));
	var valuePos=0;
	for(var i=0;i<slength;i++){
		if(Number(colls(i).text.indexOf(nowValue))==0){
			 colls(i).selected = true; 
			return;
		}
	}	
}

	//Used to handle tabs 
	function change_option(number,index)
	{
	 var error_flag = 0;
	 var error_index = new Array(number + 1);
	
	 if($("input[name='currenttab']").length!=0){
		$("input[name='currenttab']").val(number+","+index);
	}
	 
     // Fix the different between Polisy, Life, Group (FSU Web consolidation)
     // In Group tabcontent id is: tabcontent[i]
     // In Polisy, Life, tabcontent id is: content[i]
     var tabContentId = '';
     if (document.getElementById('content1') != null) {
         tabContentId = 'content';
     } else {
         tabContentId = 'tabContent';
     }
     
	 for (var i = 1; i <= number; i++) {
	 	if((document.getElementById('current' + i).className != 'tabcurrenterror') && (document.getElementById('current' + i).className != 'taberror') )	
	    {	
	        document.getElementById('current' + i).className = 'tab';//set class for tabs by Francis
	      document.getElementById(tabContentId + i).style.display = 'none';
	    } else {
	    	error_flag ++;
			error_index[i] = i;
	    }
	 }
	 if(error_flag == 0){
	 	 document.getElementById('current' + index).className = 'tabcurrent';//set class for tabs by Francis
	  document.getElementById(tabContentId + index).style.display = 'block';
	 } else{//Modified the class name by maxia (2010-7-27)
		  for (var i = 1; i <= number; i++) {
		     if(error_index[i] != undefined) {
				 if(error_index[i] == index) {
				      if(document.getElementById('current' + error_index[i]).className == "taberror") {
				      	  document.getElementById('current' + error_index[i]).className = 'tabcurrenterror';
	    			      document.getElementById(tabContentId + error_index[i]).style.display = 'block';
	    			      continue;
				      }
				      if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
				      	  document.getElementById(tabContentId + error_index[i]).style.display = 'block';
				      }
				 } else {
	 				  if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
	    			      document.getElementById('current' + error_index[i]).className = 'taberror';
	    	              document.getElementById(tabContentId + error_index[i]).style.display = 'none';
		              } else if(document.getElementById('current' + error_index[i]).className == "taberror"){
		              	  document.getElementById(tabContentId + error_index[i]).style.display = 'none'; 
		              } else  {
		              	  document.getElementById('current' + index).className = 'tab';
	 				      document.getElementById(tabContentId + index).style.display = 'block';
		              }
					  
	             }
					 
			 } else {
			     if(i == index) {
			 	     document.getElementById('current' + i).className = 'tabcurrent';
	 	 			 document.getElementById(tabContentId + i).style.display = 'block';
			 	 }			 
	}

		  }
	 }

}

	function findDiscr(field){
		var fieldDropdown=field+"dropdown";
		var fieldDisc=field+"discr";
		var selectField=field+"dropdowns";
		var found=false;
		if(document.getElementById(fieldDropdown)==null){
		return false;
		}
		if(document.getElementById(selectField)==null){
		return false;
		}
		if(document.getElementById(selectField).options==null){
		return false;
		}
		var itemLength=document.getElementById(selectField).options.length;
		var itemco=document.getElementById(field).value;
		for(i=0;i<itemLength;i++){
		var opv=document.getElementById(selectField).options[i].value;
		var op=opv.substring(0,opv.indexOf("-"));
		if(op==itemco){
		found=true;
		document.getElementById(fieldDisc).style.color="black";
		document.getElementById(fieldDisc).value=opv.substring(itemco.length+1);
		}
		}
		if(!found){
		if(itemco.Trim()==""){
		document.getElementById(fieldDisc).value="";
			return false;
		}
		document.getElementById(fieldDisc).value="Invalid value!";
		document.getElementById(fieldDisc).style.color="red";
		}		
	}
	//function search
	String.prototype.Trim = function() { return this.replace(/(^\s*)|(\s*$)/g, ""); } 

	
function changeContinueImage(thi,actionKey){
   var sorce=thi.childNodes[0];
   var img1 = new Image();
   var flagForSorce = typeof(sorce);
  
  if(flagForSorce=="undefined")
  {
   sorce=thi.src;
  }else{
   sorce=thi.childNodes[0].src;
  }
    
   var endObj = sorce.indexOf("_hover");
   
   
   if(endObj!=-1){
   sorce=sorce.substring(0,endObj)+".gif";   
   }
   if(sorce.indexOf("_after")==-1){
   
   img1.src=sorce.replace(".gif","_after.gif");
       if(flagForSorce=="undefined"){
        thi.src=img1.src;
       }else{
        thi.childNodes[0].src=img1.src;
       }
       
       thi.onclick=function(){return  false;};
       doAction(actionKey);
  
   }else{
   	thi.onclick=function(){return  false;};
   }
}

function doAction(act) {
	  parent.frames["mainForm"].document.form1.action_key.value = act;
	  parent.frames["mainForm"].doSub();
	pageloading();
}


function changeMoreImage(thi){
	   var sorce=thi.childNodes[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);
	  
	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=thi.childNodes[0].src;
	  }
	    
	   var endObj = sorce.indexOf("_hover");
	   
	   
	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";   
	   }
	   if(sorce.indexOf("_after")==-1){
	   
	   img1.src=sorce.replace(".gif","_after.gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }
	       
	       
	  
	   }else{
	   
	   }
	}

function changeMoreImageOut(thi)
{
	   var sorce=thi.childNodes[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);
	  
	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=thi.childNodes[0].src;
	  }
	    
	   var endObj = sorce.indexOf("_hover");
	   
	   
	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";   
	   }
	   if(sorce.indexOf("_after")==-1){
	   	   
	   }else{
		   img1.src=sorce.replace("_after.gif",".gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }
	   }
	}
//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeContinueImagePNG(thi,actionKey){
   var sorce=thi.childNodes[0];
   var img1 = new Image();
   var flagForSorce = typeof(sorce);
  
  if(flagForSorce=="undefined")
  {
   sorce=thi.src;
  }else{
   sorce=thi.childNodes[0].src;
  }
    
   var endObj = sorce.indexOf("_hover");
   
   
   if(endObj!=-1){
   sorce=sorce.substring(0,endObj)+".png";   
   }
   if(sorce.indexOf("_after")==-1){
   
   img1.src=sorce.replace(".png","_after.png");
       if(flagForSorce=="undefined"){
        thi.src=img1.src;
       }else{
        thi.childNodes[0].src=img1.src;
       }
       
       thi.onclick=function(){return  false;};
       doAction(actionKey);
  
   }else{
   	thi.onclick=function(){return  false;};
   }
} 

function changeMouseover(thi){
   var sorce=thi.childNodes[0];
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".gif","_hover.gif");
   thi.src=img1.src; 
   }
   }
   else{
   sorce = sorce.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".gif","_hover.gif");
   thi.childNodes[0].src=img1.src; 
   }
   }
}
//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeMouseoverPNG(thi){
   var sorce=thi.childNodes[0];
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".png","_hover.png");
   thi.src=img1.src; 
   }
   }
   else{
   sorce = sorce.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".png","_hover.png");
   thi.childNodes[0].src=img1.src; 
   }
   }
}
function changeMouseout(thi){
   var sorce=thi.childNodes[0];
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.gif",".gif");
   thi.src=img1.src; 
   }
   }else{
   sorce = sorce.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.gif",".gif");
   thi.childNodes[0].src=img1.src; 
   }
   }
}
//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeMouseoutPNG(thi){
   var sorce=thi.childNodes[0];
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.png",".png");
   thi.src=img1.src; 
   }
   }else{
   sorce = sorce.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.png",".png");
   thi.childNodes[0].src=img1.src; 
   }
   }
}
	

//Ticket ILIFE-1328 for F4 button in the FF&Chrome browser by xma3 	
function changeF4Image(thi){
 // var sorce=thi.childNodes[0].src;  
	var sorce;		
	for(var i = 0; i < thi.childNodes.length; i++){
		sorce=thi.childNodes[i].src; 
		 if(sorce == null) continue;
		 if(sorce.indexOf("_after")==-1){
			    var img1 = new Image();
			             img1.src=sorce.replace(".gif","_after.gif");
			             thi.childNodes[0].src=img1.src;
			   }else{
			   	thi.onclick=function(){return  false;};
			   }
	}
 
}
//get the position of element
function getElementPos(elementId) {
 var ua = navigator.userAgent.toLowerCase();
 var isOpera = (ua.indexOf('opera') != -1);
 var isIE = (ua.indexOf('msie') != -1 && !isOpera); // not opera spoof
 var el = document.getElementById(elementId);
 if(el.parentNode === null || el.style.display == 'none') {
  return false;
 }      
 var parent = null;
 var pos = [];     
 var box;     
 if(el.getBoundingClientRect)    //IE
 {         
  box = el.getBoundingClientRect();
  var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
  var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
  return {x:box.left + scrollLeft, y:box.top + scrollTop};
 }else if(document.getBoxObjectFor)    // gecko    
 {
  box = document.getBoxObjectFor(el); 
  var borderLeft = (el.style.borderLeftWidth)?parseInt(el.style.borderLeftWidth):0; 
  var borderTop = (el.style.borderTopWidth)?parseInt(el.style.borderTopWidth):0; 
  pos = [box.x - borderLeft, box.y - borderTop];
 } else    // safari & opera    
 {
  pos = [el.offsetLeft, el.offsetTop];  
  parent = el.offsetParent;     
  if (parent != el) { 
   while (parent) {  
    pos[0] += parent.offsetLeft; 
    pos[1] += parent.offsetTop; 
    parent = parent.offsetParent;
   }  
  }   
  if (ua.indexOf('opera') != -1 || ( ua.indexOf('safari') != -1 && el.style.position == 'absolute' )) { 
   pos[0] -= document.body.offsetLeft;
   pos[1] -= document.body.offsetTop;         
  }    
 }              
 if (el.parentNode) { 
    parent = el.parentNode;
   } else {
    parent = null;
   }
 while (parent && parent.tagName != 'BODY' && parent.tagName != 'HTML') { // account for any scrolled ancestors
  pos[0] -= parent.scrollLeft;
  pos[1] -= parent.scrollTop;
  if (parent.parentNode) {
   parent = parent.parentNode;
  } else {
   parent = null;
  }
 }
 return {x:pos[0], y:pos[1]};
}

//Added by Amit for checkbox
function handleCheckBox(name)
{
	var cbs = document.forms[0].elements[name];
	
	if(!cbs[0].checked){
		cbs[1].checked=true;
	}else{
		cbs[1].checked=false;
	}
	
	
}

function perFormOperation(act){
	if(selectedRow1!=null && selectedRow1!=""){
		document.getElementById(selectedRow1).value=act;
		doAction('PFKEY0');
	}else{		
		var elementSelected=false;
		for (var index = 0; index < idRowArray.length; ++index) {
			var item = idRowArray[index];
			if(item!=null && item!=""){
				document.getElementById(item).value=act;
				elementSelected=true;
			}
		}
		if(elementSelected){
			doAction('PFKEY0'); 
		}
	}
}

function changeSearchImage(thi,actionKey)
{
   var sorce=thi.childNodes[0].src;
   if(sorce.indexOf("_after")==-1)
   {
	   var img1 = new Image();
	   img1.src=sorce.replace(".gif","_after.gif");
	   eval(thi.childNodes[0].src=img1.src);
	   thi.onclick=function(){return  false;};
	   thi.onclick=function(){return  false;};
	   doAction(actionKey);
   }else
   {
   	thi.onclick=function(){return  false;};
   }
}




var idRowArray=new Array();
var radioOptionElementId;
var radioElementCounter;
var checkedValue='1'; //IJTI-1224
function selectedRowChecked(idt1,idt2,count)
{
	count=count-1;
		if(document.getElementById(idt2).type!='radio'){
			
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value=checkedValue; //IJTI-1224
				idRowArray[count]=idt1;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
			}
		}else{
		
			if(radioOptionElementId!=null && radioOptionElementId!=""){
				var item = idRowArray[radioElementCounter]; 
				if(item!=null && item!=""){
					if(document.getElementById(radioOptionElementId).checked){
						document.getElementById(item).value=' ';
						document.getElementById(radioOptionElementId).selected=false;
						document.getElementById(radioOptionElementId).checked=false;
						idRowArray[radioElementCounter]="";
						radioOptionElementId="";
					}
				}
			}
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value=checkedValue; //IJTI-1224
				idRowArray[count]=idt1;
				radioElementCounter=count;
				radioOptionElementId=idt2;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
				radioElementCounter="";
				radioOptionElementId="";
			}
		}
}
function createOperationArray(idt1,idt2,count)
{
	count=count-1;
	idRowArray[count]=idt1;
	radioElementCounter=count;
	radioOptionElementId=idt2;
}


var selectedRow1;
var firstTimeOver=false;
function selectedRow(idt){

	document.getElementById(idt).value=checkedValue; //IJTI-1224
	document.getElementById(idt).selected=true;

	if(firstTimeOver && document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value=' ';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;	
}
var selectedRow1;
var firstTimeOver=false;
function selectedRowYN(idt){

	document.getElementById(idt).value='Y';
	document.getElementById(idt).selected=true;

	if(firstTimeOver && document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value='N';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;
}



var rowID;
function selectedForDelete(idt)
{
   rowID=idt;
   //alert(rowID);
}

//**************************** Add Row Start *********************************

function addRow(strtindx , endindx) 
{
	  var table = document.getElementById("table");
	  var rowCount = table.rows.length;
	  var row=table.rows;
	  var tempvisibility = null, tempvisibility1 = 'hidden'.toString();
	  var count;
		
		for(var z=0;z<rowCount;z++)
		{
			tempvisibility = row[z].style.visibility;
			
			if (tempvisibility.toLowerCase() == tempvisibility1)
			{
					count = parseInt(z-1);
					break;
			}
		}
	//alert("Row Count : "+count);
	if(parseInt(count)<parseInt(rowCount))
	{	


	  var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";
		
		var check = "",flagname = true,flagtype = true, flg = true;
		for(var r=0; r<2; r++)
		{
			check[r] = 'nxtrow';
		}

	 //alert("strtindx : "+strtindx+" endindx : "+endindx);
		
	 for(var i=count; i<=count; i++)
	 {
	    var visibility = row[i].style.visibility;
	    var disp = row[i].style.display;
		
		for (var j=1; j<=parseInt(endindx); j++)
		{
			str = row[i].cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;

			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}
				
				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}
		
				
				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype)
				{
					typeOfElement = 'text'
					flagtype = false;
				}
				//alert("nameOfElement : "+nameOfElement+"typeOfElement : "+typeOfElement);
			}

			if(typeOfElement.toLowerCase() ==  'select')
				{
				//alert("In Select : "+document.getElementById(nameOfElement.toString()).selectedIndex);
					if(document.getElementById(nameOfElement.toString()).selectedIndex == 0)
					{
						check[k] = 'nxtrow1';
						flg = false;
					}
						typeOfElement = "";
						nameOfElement = "";
				}
				
				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					//alert("In text value is : "+document.getElementById(nameOfElement.toString()).value.trim());
					//alert("In text Condition is: "+(document.getElementById(nameOfElement.toString()).value.trim() == null || document.getElementById(nameOfElement.toString()).value.trim() == ""));
					//MIBT-58 STARTS
					if(nameOfElement != ""){
					//MIBT-58 ENDS
					if(document.getElementById(nameOfElement.toString()).value.trim() == null || document.getElementById(nameOfElement.toString()).value.trim() == "")
					{
						flg = false;
					}
					//MIBT-58 STARTS
					}
					//MIBT-58 ENDS
					typeOfElement = "";
					nameOfElement = "";
				}
			
			flagtype = true;
			flagname = true;
		}
	}
		//alert("<<<<<<<<<<<<<< Flag >>>>>>>>>>>> "+flg);
		if(!flg)
		{
			callCommonAlert("<%=imageFolder%>","No00019");
		}else
		{
			  row[parseInt(count)+1].style.visibility='visible';
			  row[parseInt(count)+1].style.display = '';
		}
	}
}


//**************************** Add Row End *********************************

// **************************** Reset Field for Delete row operation Start *********************************
function resetfields(subStr)
{
	//alert(subStr);
		//alert("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<In Reset Field Method >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";
		var check = "",flagname = true,flagtype = true;

		var noofChild = document.getElementById(subStr).cells.length;

		var row=document.getElementById(subStr);
		for (var j=0; j<parseInt(noofChild); j++)
		{
			str = row.cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;
			
			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}
				
				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}

				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype)
				{
					typeOfElement = 'text'
					flagtype = false;
				}
				
			}


				if(typeOfElement.toLowerCase() ==  'select')
				{
					//alert('select in')
					//alert(nameOfElement.toString());
					document.getElementById(nameOfElement.toString()).selectedIndex = 0;
				}
				
				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					//alert(nameOfElement.toString());
					if (document.getElementById(nameOfElement.toString()) != null)
					    document.getElementById(nameOfElement.toString()).value = "";
				}
			
			flagtype = true;
			flagname = true;
		}
}
// **************************** Reset Field for Delete row operation End *********************************

// **************************** Delete Row Start *********************************
var indx = 0;
 function getSelected(opt) 
 {
	var selected = new Array();
	
	for (var intLoop = 0; intLoop < opt.length; intLoop++) 
	{
		//alert("***************** intLoop *************** "+intLoop);

		//alert("Opt in Get Selected : "+opt[intLoop].checked);
	   if (opt[intLoop].checked) 
		{
		  indx = selected.length;
		  selected[indx] = new Object;
		  selected[indx].value = opt[intLoop].value;
		//alert("selected[indx].value : "+selected[indx].value );
		  selected[indx].index = intLoop;
		//alert("selected[indx].index : "+selected[indx].index );
	   }
	}
	//alert("return selected");

	for(var i=0; i<selected.length;i++)
	{
		//alert("================ selected[i].value : "+ selected[i].value);
		//alert("================ selected[i].index : "+ selected[i].index);
	}
	return selected;
 }

 function removeSelected(opt) 
 {
	indx = 0;
	//alert("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<In Uncheck Function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	for (var intLoop = 0; intLoop < opt.length; intLoop++) 
	{
		////alert("Opt : "+opt[intLoop].toString());
	   if (opt[intLoop].checked) 
		{
			opt[intLoop].checked = false;
			//alert("opt[intLoop].checked : "+opt[intLoop].checked );
	   }
	}
 }

 function getCheckBoxName(rowID)
 {

 	//alert("HELLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
 	//alert(document.getElementById(rowID).innerHTML);
 	var innerHtml = document.getElementById(rowID).innerHTML;
 	var nameOfElement, subStr1;
 	var str1 = innerHtml.split("<td>");
 	var len = str1.length;
 			
 	if((parseInt(str1[0].indexOf('id='))!= parseInt(-1)))
 	{
 		
 		 subStr1 = str1[0].substring(parseInt(str1[0].indexOf('id='))+3);

 		 nameOfElement = subStr1.substring(0,parseInt(subStr1.indexOf(" ")));
 		 
 			
 		if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
 			nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))

 			//alert("1111111111111111111111111111111111111111111"+nameOfElement)
 	}
 	return nameOfElement;
 }
 

	
function deleteRow(opt) 
{	

var indexstr = 8;
var row=table.rows.length;
var totalRow;
var substr1,substr,str;

//alert("Opt is : "+opt);
var sel = new Array();
sel = getSelected(opt);
//alert("get return selected : " +sel.length);

for(var i=0; i<sel.length;i++)
{
	//alert("str is : "+ sel[i].index);
	substr = parseInt(sel[i].index);//str.substring(indexstr);
	//alert("substr is : "+ substr);
	totalRow = parseInt(row)-1;
	

	if(parseInt(substr)<totalRow)
	{
		substr1 = parseInt(substr)+1;
			//alert("alert in if "+substr1);
	}else
	{
		substr1 = parseInt(substr);
			//alert("alert in else "+substr1);
	}
	var substr2 = "tablerow"+substr1; 
	//alert("substr2 is : "+ substr2);

	
	var chkname = getCheckBoxName(substr2); 
	
	//alert("chkname is : "+ chkname);
		
	if(substr1<totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
	if(substr1==totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
}

//alert(" Going to call Reset & Remove Selected : "+ substr1);
for(var i=0; i<opt.length;i++)
{
	//alert("opt[i].checked : "+opt[i].checked)
}
resetfields(substr2);
removeSelected(opt);
}

// MIBT-187
function DeleteReceipt(opt) 
{	

var indexstr = 8;
var row=table.rows.length;
var totalRow;
var substr1,substr,str;


var sel = new Array();
sel = getSelected(opt);



		for(var i=0; i<sel.length;i++)
		{
			mflag=1;
			substr = parseInt(sel[i].index);			
			totalRow = parseInt(row)-1;		
			if(parseInt(substr)<totalRow)
			{
				substr1 = parseInt(substr)+1;
					
			}else
			{
				substr1 = parseInt(substr);
					
			}
			var substr2 = "tablerow"+substr1;			
			var chkname = getCheckBoxName(substr2);	

			
			if(substr1<totalRow &&  substr1!=1)
			{
				document.getElementById(chkname).checked = 'false';
				document.getElementById(substr2).style.visibility='hidden';
				document.getElementById(substr2).style.display='none';
				
			}
			
			
		}
		if (substr1!=1)
		{
		//resetfields(substr2);
		removeSelected(opt);
		}	
	
}

/* IFSU-137 start sgadkari */

function deleteReceiptRow(opt) 
{	

var indexstr = 8;
var row=table.rows.length;
var totalRow;
var substr1,substr,str;

//alert("Opt is : "+opt);
var sel = new Array();
sel = getSelectedReceipt(opt);
//alert("get return selected : " +sel.length);

for(var i=0; i<sel.length;i++)
{
	//alert("str is : "+ sel[i].index);
	substr = parseInt(sel[i].index);//str.substring(indexstr);
	//alert("substr is : "+ substr);
	totalRow = parseInt(row)-1;
	

	if(parseInt(substr)<totalRow)
	{
		substr1 = parseInt(substr)+1;
			//alert("alert in if "+substr1);
	}else
	{
		substr1 = parseInt(substr);
			//alert("alert in else "+substr1);
	}
	var substr2 = "tablerow"+substr1; 
	//alert("substr2 is : "+ substr2);

	
	var chkname = getCheckBoxName(substr2); 
	
	//alert("chkname is : "+ chkname);
		
	if(substr1<totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
	if(substr1==totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
}


function exportExcel(){
 

	 parent.frames["mainForm"].getExcelSeedData();
 
} 

//alert(" Going to call Reset & Remove Selected : "+ substr1);
for(var i=0; i<opt.length;i++)
{
	//alert("opt[i].checked : "+opt[i].checked)
}
resetfields(substr2);
removeSelected(opt);
}
function getSelectedReceipt(opt) 
{
	var selected = new Array();
	
	for (var intLoop = 1; intLoop < opt.length; intLoop++) 
	{
		//alert("***************** intLoop *************** "+intLoop);

		//alert("Opt in Get Selected : "+opt[intLoop].checked);
	   if (opt[intLoop].checked) 
		{
		  indx = selected.length;
		  selected[indx] = new Object;
		  selected[indx].value = opt[intLoop].value;
		//alert("selected[indx].value : "+selected[indx].value );
		  selected[indx].index = intLoop;
		//alert("selected[indx].index : "+selected[indx].index );
	   }
	}
	//alert("return selected");

	for(var i=0; i<selected.length;i++)
	{
		//alert("================ selected[i].value : "+ selected[i].value);
		//alert("================ selected[i].index : "+ selected[i].index);
	}
	return selected;
}

/*  IFSU-137 Ends */

// **************************** Delete Row End *********************************


function moveScroll()
{
   var div1 = document.getElementById("subfo");
   var scrollAmt = div1.scrollTop;
  
  div1.scrollTop = parseInt(div1.scrollHeight);
}


function clearFocusField() {
	lastSelectedF = null;
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
	if (aForm.focusInField) {
      	aForm.focusInField.value = "";
    }
}


/* IGB-615 Start */
function getExcelSeedData() {
	var delimiter = "|"; 
	var colTitles = "";
	var screenname = '<%=fw.getScreenName()%>';
	var hasSubfile = '<%=hasSubfile%>';
	if(hasSubfile=="false") {
		alert("There is no data to export!");
		return false;
	}
	screenname = "-s" + screenname.substring(1).trim();
	$("#dataTables" + screenname + " thead tr:first").find("th").each(function(key, item) {
		
		if(!($(item).text() == 'Select')){
			colTitles+= $(item).text() + delimiter;
			if( $(item).text() == "Member"){
				colTitles += "__"  + delimiter;
			}
		}
	});
	
	var goodSampleRow = false;
	var sampleRow = "";
	var cellValue;
	$("#dataTables" + screenname + " tbody tr").each(function(key, item) {
		var countq = 1;
		//alert(key);
		if(key==0) {  
			goodSampleRow = true;
			sampleRow  = "";
			$(item).find("td").each(function() {
				var divId= ($(this).attr("id"));
				 
				if(divId != undefined){
					 
					if(countq >= 1){
						$(item).find("td").each(function() {
							divId= ($(this).attr("id"));
							 
							if(divId !== undefined){ //alert(divId);
							 
							  var id = divId.substr(0,divId.indexOf("."));
							 
							 sampleRow +=  id + delimiter;
							}
						});
						countq++;
						 
					}
				}else{ 
					 
					countq++;
				}
				
			});
		}
		//if(goodSampleRow)
			//return false;
	});	
	// alert(colTitles);	
	// alert(sampleRow);
	 $('#sflheaderList').val(colTitles);
	 $('#sflVarList').val(sampleRow);
}


function startButton(event, buttonName, textAreaID) {
	//alert('hai');
	var recognizing = false;
	 // Test browser support
    window.SpeechRecognition = window.SpeechRecognition       ||
                               window.webkitSpeechRecognition;
    if (window.SpeechRecognition === null) {
        alert("browser not supported");
    } else {
    	
    	var maxlength;    	
        var recognizer = new SpeechRecognition();//new webkitSpeechRecognition();//window.SpeechRecognition();
        var transcription = document.getElementById(textAreaID);     
        transcription.value=transcription.value.trimRight();
        transcription.innerHTML=transcription.value;
        maxlength =  transcription.maxLength;
        var origtranscription = document.getElementById(textAreaID).value;
      
        var modLocale;
	    if(localeLoc.search("_") > 0){
	    	modLocale= localeLoc.replace('_','-');
	    }
	    else{
	    	
	    	modLocale = localeLoc;
	    	//this fix has been added because of the difference in formatting/localization for in and in_ID
	    	//we check if there is any country code along with lang code. If not we repeat the language code in caps.
	    	var indexOfUndrScr = modLocale.indexOf('-');
	    	if(indexOfUndrScr == -1){
	    		modLocale = modLocale + '-' + modLocale.toUpperCase();
	    	} 
	    }
        recognizer.lang= modLocale;
        recognizer.interimResults=true;

        // Recogniser doesn't stop listening even if the user pauses
        recognizer.continuous = true;

        // Start recognising
        recognizer.onresult = function(event) {
          //transcription.textContent = '';

          for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
            	if(transcription.textLength < maxlength){
              transcription.textContent += event.results[i][0].transcript;
              document.getElementById(textAreaID).value = transcription.textContent.substr(0,maxlength);
              transcription.innerHTML = transcription.textContent;
            	}
            	else{
            		break;
            	}
            } else {
              //dont want to c intermediate results
            }
          }
        };

        // Listen for errors
        recognizer.onerror = function(event) {
        	//alert('hai1');
            if (event.error == 'audio-capture') {
                alert("please connect headphones");
              }
            else if (event.error == 'not-allowed') {
            	alert("Action not allowed by browser");
            }
            else{
	            console.log(event.error);
	            alert(event.error);  
            }
        };

      

        document.getElementById(buttonName).addEventListener('click', function() {
        	//alert('hai2');
          recognizer.stop();
          recognizing = false;      
          document.getElementById(textAreaID).value= transcription.value;
          document.getElementById(textAreaID).innerHTML=transcription.value;
        });

        try {
        	if(recognizing){
        		recognizer.stop();        	    
        	}
          recognizer.start();
          recognizing = true;
          
        } catch(ex) {
          
        }  
      }
}
	
	
/* IGB-615 End */
 </script>	
 
<input type="text" id="dtformat" value="<%=dtFormat %>" style="visibility: hidden;">
 <input type="text" id="global_path" value="<%=ctx %>" style="visibility: hidden;">
 
 <input type="text" id="lang" value="<%=lang %>" style="visibility: hidden;">
 <input type="hidden" id="xssblacklist" value='<%=XSSFilter.unEscapeHtml(AppVars.getInstance().getAppConfig().getXssBlackList())%>' style="visibility: hidden;"> <%-- IJTI-1635 --%>
 <input type="text" id="input_value" value="<%=resourceBundleHandler.gettingValueFromBundle("Input")%>" style="visibility: hidden;">
 <input type="text" id="action_value" value="<%=resourceBundleHandler.gettingValueFromBundle("Action")%>" style="visibility: hidden;">
 <input type="hidden" id="istk" name="istk" value="<%=av.getCsrfToken()%>" style="visibility: hidden;"><!-- IJTI-472 -->
 <script>		
		$(document).ready(function(){ 
		
			var pathName =document.getElementById('global_path').value;
			
			var language = document.getElementById('lang').value;
			var inputValue = document.getElementById('input_value').value;
			var actionValue = document.getElementById('action_value').value;
			//console.log("pathName==================="+pathName)
			
			var classname1 = $(".secondPanel").attr('class');
			var classname  = $(".panel-default").attr('class');
			
		
			
			var input_imgurl = pathName+'screenFiles'+'/'+language+'/'+'menu'+'/'+'Input_icon02.png'
			var action_imgurl = pathName+'screenFiles'+'/'+language+'/'+'menu'+'/'+'Action_icon02.png'
			
			if(classname == ("panel panel-default")) {
		
				$(".panel-default > .panel-heading").empty();
			
				$('.panel-default > .panel-heading').append("<img src='"+input_imgurl+"' height='25' title='Input'>"+ inputValue) ;
				
			}
			if(classname1 == ("panel panel-default secondPanel")){
			
				$(".secondPanel> .panel-heading").empty(); 
				 
				$('.secondPanel > .panel-heading ').append("<img src='"+action_imgurl+"' height='25' title='Actions'>"+ actionValue);
						
			}
			
			//for date format change
			 $("div").removeAttr("data-date-format");
			 $("div").removeAttr("data-link-format");		
			var date_format = document.getElementById('dtformat').value;
			if(date_format=="dd/MM/yyyy" || date_format=="DD/MM/YYYY"){				
				var date_format1 = "dd/mm/yyyy"	
			}
			if(date_format=="yyyy/MM/dd" || date_format=="YYYY/MM/DD"){				
				var date_format1 = "yyyy/mm/dd"	
			}
			  $(function () {
				$(".date").datepicker({ 
			        format: date_format1,			       
			       // todayBtn: true,
			       	todayBtn: "linked",			      
			        autoclose: true,
                    forceParse: false,
                    Default: true,
                    pickDate: true,
                    todayHighlight: true,
                    orientation: 'bottom'
			    }); 
		    });
		});
</script>

	<div class="container" style="margin-top: -116px !important;">      
	<%if(!fw.getScreenName().equals("S0017")){%>
		<div id="page-header" class="row">			
				<h1 style="margin: 0px 15px 15px 15px !important;font-family: 'Oswald', sans-serif !important;font-weight: 500 !important;font-size: 24px !important; border-bottom: 1px solid #eee !important; border-bottom-style: double !important;margin-bottom: 10px""><%=tit%></h1>
		         			         
		</div>
		<%}%>
		<!-- ILIFE-8864 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<div id="msg-panel" class="panel panel-info mainform-msg" style="display:none">
	<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
</div>
<script>
	if(sessionStorage.getItem("sidebar") ==="pined"){
		document.getElementById("msg-panel").style.visibility = 'hidden';
	}
</script>
<%}%>
<!-- ILIFE-8864 ENDS-->