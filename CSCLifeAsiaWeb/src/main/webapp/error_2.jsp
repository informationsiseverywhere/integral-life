<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import="java.util.*" %> 
<%@ page import="com.quipoz.framework.error.*" %>   
<%@ page import="com.quipoz.framework.exception.*" %> 
<%@ page import="com.quipoz.framework.util.*" %>  
<%@ page import="com.quipoz.framework.screenmodel.*" %> 
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.properties.PropertyLoader" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<%String ctx = request.getContextPath() + "/";%> 

<%	String imageFolder = PropertyLoader.getFolderName(request.getLocale().toString()); 
		ResourceBundleHandler resourceBundle = new ResourceBundleHandler(request.getLocale());
		String menu=resourceBundle.gettingValueFromBundle("MAIN MENU");
		String title=resourceBundle.gettingValueFromBundle("Return to Main Menu");
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  	response.addHeader("X-Content-Type-Options", "NOSNIFF");//IJTI-676
	response.addHeader("Cache-Control", "no-store"); //IJTI-676
	response.addHeader("Pragma", "no-cache"); //IJTI-676
	

%>
<%session.invalidate();%> 						  
<HTML>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<%AppVars av = AppVars.getInstance();
	if(av.getAppConfig().getJspConfigPath()!=null && !av.getAppConfig().getJspConfigPath().isEmpty()){
		av.setJspConfigPath(av.getAppConfig().getJspConfigPath());
}%>
 	<%if(av.getJspConfigPath().equals("jsp")){ %>
	 	<meta http-equiv="pragram" content="no-cache">
	  	<meta http-equiv="cache-control" content="no-cache, must-revalidate">
	  	<meta http-equiv="expires" content="0">
		<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
		<link href="<%=ctx%>bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
	<body class="menu" style="background-color: #FFFFFF;"><!-- IBPLIFE-5274 -->
		<div class="sidearea">
			<div class="logoarea" style="height: 50px;">
				<a class="navbar-brand" style="padding: 0 !important;"><img src="screenFiles/<%=imageFolder%>/dxc_symbol.jpg" height="50px !important"></a>
			</div>
			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse" style="display: block;">
					<ul class="nav">
						 <li class="active">
							<a href="<%=request.getContextPath()%>/" target="_top" title="<%=title%>"><%=menu%></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</body>	
	<%}else{ %>
		<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
		<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
		<BODY class="menu">
			<table>
				<tr><td><img src="screenFiles/<%=imageFolder%>/dxc_symbol.jpg" class="logo"></td></tr>
			</table>
		<BR/>
			<P TITLE="<%=title%>"><A href="<%=request.getContextPath()%>/" TARGET="_top"><%=menu%></A></P>
		</BODY>
  <%} %>
</HTML>