<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.*' %>
<%@ page import='com.quipoz.framework.screenmodel.*' %>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel' %>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars" %>
<%@ page import="java.util.HashMap" %>
<% String focusName = "";%>
<%BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
  COBOLAppVars cobolAv3 = (COBOLAppVars)bm3.getApplicationVariables();
  // fix bug749 and bug789
  String sessionSyn = (String) (request.getSession().getAttribute(RequestParms.PLACE_HOLDER_NOT_EQUAL) == null
  	? "" : request.getSession().getAttribute(RequestParms.PLACE_HOLDER_NOT_EQUAL));
  	if (request.getSession().getAttribute("FocusObjects") != null) {
  	HashMap fObject = (HashMap) request.getSession().getAttribute("FocusObjects");
  	if (fObject.containsKey(fw3.getScreenName())) {
  		focusName = (String) fObject.get(fw3.getScreenName());
  	}
  }
%>
	<input type="hidden" name="<%=RequestParms.ACTION%>" value="<%=cobolAv3.actionKey%>"><!-- ILIFE-5717 -->
    <input type="hidden" name="<%=RequestParms.SCREEN_NAME%>" value="<%=fw3.getScreenName()%>"/>
    <input type="hidden" name="<%=RequestParms.FOCUSFIELD%>" value="">
    <input type="hidden" name="<%=RequestParms.FOCUSINFIELD%>" value="<%=focusName%>"> 
    <input type="hidden" name="<%=RequestParms.FOCUSPREVF%>" value="">
    <input type="hidden" name="<%=RequestParms.FOCUSNXTPR%>" value="">
    <input type="hidden" name="<%=RequestParms.SEMAPHORE%>"  value="">
    <!-- boostrap - add style -->
    <input class="invisibleSubmit" type="SUBMIT" name="lastResort" value="Enter" style="visibility: hidden;display: none;"
    onClick="if (document.activeElement.name != 'lastResort') {return false;}" onFocus="selectFirstField()" onMouseDown="selectCurrentField();return false"  onSelectStart="selectFirstField()" TABINDEX=32767>


<%=AppVars.hf.getLit("</form>")%>

<script language='javaScript'>
	rollUpEnabled = <%=cobolAv3.isPagedownEnabled()%>;
   	rollDownEnabled = <%=cobolAv3.isPageupEnabled()%>;

   	<%=AppVars.hf.getValidKeys(fw3.getFormActions())%>
    alerts = "<%=AppVars.hf.formatMessageBox(fw3.getAlerts())%>";
    if (alerts != "") {
      alert(alerts);
      alerts = "";
    }
    // fix bug749 and bug789
    <%if (sessionSyn.equals("NO")) {%>
    	//alert("The server is still processing your last request. Please wait for response before proceeding.");
    	callCommonAlert(<%=imageFolder%>,"No00015");
    <%}%>
    promptstr = "<%=fw3.getPrompt()%>";
    if (promptstr != "") {
      if (confirm(promptstr)) {
            document.forms[0].action_key.value = "Yes";
      }
      else {
            document.forms[0].action_key.value = "No";
      }
      doLoad();
      doSub();
    }
    /*IPNC-1245 yzhong4 start*/
    function changeContinueImage(thi,actionKey){
       //var sorce=thi.childNodes[0];
       var sorce;
       var sorceSrc;
       $(thi).find("img").each(function(i) {
    		sorce = $(this);
    		sorceSrc = $(this).attr("src");
    		return false;
    	});  
       var img1 = new Image();
       var flagForSorce = typeof(sorce);
      
      if(flagForSorce=="undefined")
      {
       sorce=thi.src;
      }else{
       //sorce=thi.childNodes[0].src;
    	  sorce=sorceSrc;
      }
        
       var endObj = sorce.indexOf("_hover");
       
       
       if(endObj!=-1){
       sorce=sorce.substring(0,endObj)+".gif";   
       }
       if(sorce.indexOf("_after")==-1){
       
       img1.src=sorce.replace(".gif","_after.gif");
           if(flagForSorce=="undefined"){
            thi.src=img1.src;
           }else{
           // thi.childNodes[0].src=img1.src;
        	   $(thi).find("img").each(function(i) {
       			$(this).attr("src",img1.src);
       			return false;
       		});
           }
           
           thi.onclick=function(){return  false;};
           doAction(actionKey);
      
       }else{
       	thi.onclick=function(){return  false;};
       }
    }
    
  //Added it for the style after clicking the button (@Michelle 2010-7-19)    
    function changeDivClass(thi,actionKey){

    if($(thi).parent().parent().hasClass("sectionbtndisable")){
      thi.onclick=function(){return  false;};
     }
    else{
      $(thi).parent().parent().removeClass();
      $(thi).parent().parent().addClass("sectionbtndisable");
      thi.onclick=function(){return  false;};
      doAction(actionKey);
    }
       
    }

    //copy a duplicated method to handle png image
    //gu lizhi 2010-7-21
    function changeContinueImagePNG(thi,actionKey){
       thi.onclick=function(){return  false;};
       doAction(actionKey);
    } 
    function getSorce(thi){
    	var sorce;
    	var sorceSrc;
    	   $(thi).find("img").each(function(i) {
    			sorce = $(this);
    			sorceSrc = $(this).attr("src");
    			return false;
    		});  
    }
    function changeMouseover(thi){
      // var sorce=thi.childNodes[0];
      var sorce;
       var sorceSrc;
       $(thi).find("img").each(function(i) {
    		sorce = $(this);
    		sorceSrc = $(this).attr("src");
    		return false;
    	});  
       if(typeof(sorce)=="undefined"){
        sorce = thi.src;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace(".gif","_hover.gif");
       thi.src=img1.src; 
       }
       }
       else{
       //sorce = sorce.src;
       sorce=sorceSrc;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace(".gif","_hover.gif");
       //thi.childNodes[0].src=img1.src;
       $(thi).find("img").each(function(i) {
    		$(this).attr("src",img1.src);
    		return false;
    	});
       }
       }
    }
    //copy a duplicated method to handle png image
    //gu lizhi 2010-7-21
    function changeMouseoverPNG(thi){
       //var sorce=thi.childNodes[0];
       var sorce;
       var sorceSrc;
       $(thi).find("img").each(function(i) {
    		sorce = $(this);
    		sorceSrc = $(this).attr("src");
    		return false;
    	});
       if(typeof(sorce)=="undefined"){
        sorce = thi.src;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace(".png","_hover.png");
       thi.src=img1.src; 
       }
       }
       else{
       //sorce = sorce.src;
       sorce=sorceSrc;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace(".png","_hover.png");
       //thi.childNodes[0].src=img1.src; 
       $(thi).find("img").each(function(i) {
    		$(this).attr("src",img1.src);
    		return false;
    	});
       }
       }
    }
    function changeMouseout(thi){
       //var sorce=thi.childNodes[0];
        var sorce;
       var sorceSrc;
       $(thi).find("img").each(function(i) {
    		sorce = $(this);
    		sorceSrc = $(this).attr("src");
    		return false;
    	}); 
       if(typeof(sorce)=="undefined"){
        sorce = thi.src;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace("_hover.gif",".gif");
       thi.src=img1.src; 
       }
       }else{
       //sorce = sorce.src;
       sorce=sorceSrc;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace("_hover.gif",".gif");
       //thi.childNodes[0].src=img1.src; 
       $(thi).find("img").each(function(i) {
    		$(this).attr("src",img1.src);
    		return false;
    	});
       }
       }
    }
    //copy a duplicated method to handle png image
    //gu lizhi 2010-7-21
    function changeMouseoutPNG(thi){
       //var sorce=thi.childNodes[0];
       var sorce;
       var sorceSrc;
       $(thi).find("img").each(function(i) {
    		sorce = $(this);
    		sorceSrc = $(this).attr("src");
    		return false;
    	});
       if(typeof(sorce)=="undefined"){
        sorce = thi.src;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace("_hover.png",".png");
       thi.src=img1.src; 
       }
       }else{
       //sorce = sorce.src;
       sorce=sorceSrc;
       if(sorce.indexOf("_after")==-1){
       var img1 = new Image();
       img1.src=sorce.replace("_hover.png",".png");
       //thi.childNodes[0].src=img1.src; 
       $(thi).find("img").each(function(i) {
    		$(this).attr("src",img1.src);
    		return false;
    	});
       }
       }
    }
    /*IPNC-1245 yzhong4 end*/
    /*
    * Updated by dnguyen91
    * Description: Set default tab index (first tab) visible
    */
    function setDefaultTabIndex(){
		if($(".tabblock").length > 0){
			var numberOfTabs = $(".tabcurrent").length;
			if(numberOfTabs > 0){
				change_option(numberOfTabs,1);
			}
		}
	}
	setDefaultTabIndex();
	// End: Set default tab index (first tab) visible
    function getSorce(thi){
	var sorce;
	var sorceSrc;
	   $(thi).find("img").each(function(i) {
			sorce = $(this);
			sorceSrc = $(this).attr("src");
			return false;
		});  
}
    /*ILIFE-1328 xma3 end*/
    
//ILIFE-2143 by hai2
$(document).ready(function(){
	var inputItem;
	$("img[src$='search.gif'],img[src$='calendar.gif']").each(function(){
		inputItem = $(this).parent().prev();
		if(inputItem.is("input")){
			var iid = inputItem.attr("id");
			if(iid==""){
				inputItem.attr("id",inputItem.attr("name"));
			}
		}		
	})
	
	//ILIFE-2316 appened id property for <input> 
	$("input[type='checkbox']").each(function(){
		var iid = $(this).attr("id");
		if(iid==""){$(this).attr("id",$(this).attr("name"));}
	})	
})    
/* copy functions on commonScript4 */
function setCheckboxVal(obj,nameObj,checkValue,uncheckValue){
  if(obj.checked){
    $("input[name='"+nameObj+"']").val(checkValue);
  }else{
    $("input[name='"+nameObj+"']").val(uncheckValue);
  }
	changeRemoveBtnStatus();
}
</script>

<%    fw3.setFrameLoaded(2);%>

