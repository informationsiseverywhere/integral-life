<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.*' %>
<%@ page import='com.quipoz.framework.screenmodel.*' %>
<%@page import="com.csc.smart400framework.SMARTAppVars"%>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@ page import="com.csc.common.constants.PropertyConstants" %>
<%@ page import="com.csc.util.PropertyHandler" %>
	<!--This stupid bit of code exists ONLY to stop Websphere tagging "fw"
	    as an unresolved variable in this jsp fragment.
	-->
	<%String cs1ctx = request.getContextPath();
	  BaseModel bm1 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
      ScreenModel fw1 = (ScreenModel) bm1.getScreenModel();
      SMARTHTMLFormatter smartHF1 = new SMARTHTMLFormatter();
      smartHF1.setLocale(request.getLocale());
      String langCode1= PropertyLoader.getFolderName(smartHF1.getLocale().toString());
      String lang1  = bm1.getApplicationVariables().getUserLanguage().toString().toUpperCase() + "/"; 
    %>
	
	<script language='javaScript'>
    	thisField = '<%=fw1.getValue("focusField")%>';
    	lastField = '<%=fw1.getValue("focusPrevF")%>';
    	currField = '<%=fw1.getActiveField()%>';
    	nextOPrev = '<%=fw1.getValue("focusNxtPr")%>';
    	semaphore = '<%=fw1.getSemaphore()%>';
    	popupMenu = '<%=fw1.getPopupMenu()%>';
    	todayDate = '<%=fw1.getCurrentDate()%>';
    	popup     = "<%=fw1.hasPopup()%>";
    	error     = "<%=fw1.isFieldInError()%>";
    	thisElt   = null;
    	validKeys    = null;
    	<!-- rollUpEnabled and rollDownEnabled will be set in commonScript2.jsp-->
    	rollUpEnabled = true;
    	rollDownEnabled = true;
   	    cancelled = true; //Will be set to false in process action code. If true, means the user closed the browser.
	</script>

	<script language='javaScript'>
		function getCommonScreens(screen) {
			return "<%=cs1ctx%>/commonJSP/<%=lang1%>" + screen; 
		}
		function suppressit() {
			return false;
		}
		var contextPathName = "<%= request.getContextPath() %>";
		
		/*ILIFE-6401 CHANGES START*/
		var restrictCharsFlag = <%=AppConfig.isRestrictCharactersEnabled()%>
		<%String errorMessage= PropertyHandler.getString(PropertyConstants.INVALID_CHARS_ERROR,PropertyConstants.FILE_NAME_ACCEPTED_CHARACTERS_LIFE,av.getLocale());%>
		var restrictCharsErrorMessage="<%=errorMessage%>";
		var restrictCharsJson=<%=PropertyHandler.getString(PropertyConstants.KEY_ACCEPTED_CHARACTERS,PropertyConstants.FILE_NAME_ACCEPTED_CHARACTERS_LIFE,av.getLocale())%>;
		var restrictCharsKey= "<%=PropertyConstants.KEY_ACCEPTED_CHARACTERS%>"
		var jspConfigPath = "<%=AppConfig.getInstance().getJspConfigPath()%>";
		/*ILIFE-6401 CHANGES END*/
	</script>
	
<script language="javaScript" src='<%=cs1ctx%>/js/calendar1.js'></script>
<!-- Added by Ai Hao for ILIFE-169 -->
<%-- bootstrap <script language="javaScript" src='<%=cs1ctx%>/js/<%=lang1.toLowerCase()%>popcalendar.js'></script> --%>
<!--Removed by Ai Hao for ILIFE-169-->
<%//if(lang1.equalsIgnoreCase("eng/")){ %>
<!--<script language="javaScript" src='<%//=cs1ctx%>/js/popcalendar.js'></script>-->
<%//}else if(lang1.equalsIgnoreCase("chi/")){ %>
<!--<script language="javaScript" src='<%//=cs1ctx%>/js/popcalendar_cn.js' charset=UTF-8></script>-->
<%//} %>

<script language='javaScript' src='<%=cs1ctx%>/js/xmlReader.js' charset=UTF-8></script>
<script language='javaScript' src='<%=cs1ctx%>/js/commonScript1.js'></script>


<!-- This code is for the javascript functions of SuperTable @ashaik21 -->

<!--ILIFE-1328 adding the superTables.js by xma3  -->
<script language='javaScript' src='<%=cs1ctx%>/js/operateSuperTable.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/operateSuperTableNEW.js'></script>
<script language="javascript" src='<%=cs1ctx%>/js/superTable_varCode.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/superTables.js'></script>   
<script language='javaScript' src='<%=cs1ctx%>/js/operatePopupTable.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/CheckboxClickHandler.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/operateTable.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/dropdownlist_varCode.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/dropdownlist.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/Sidebar.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/replaceSelects.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/textarea.js'></script>
<Script>
	ctx = "<%=cs1ctx%>" + "/";
	lang = "<%=lang1%>";
</Script>
<form name="form1" method="post" action="<%=cs1ctx%>/process" target="_top" onSubmit="return suppressit()" onClick="selectSpecial()" autocomplete="off"> <!-- IJTI-1221 -->
<!--Ticket ILIFE-1328 yzhong4 start-->
<!--<div style='position:absolute; top:-20; left:-20'>-->
<div style='position:absolute; top:-80; left:-20'>
<!--Ticket ILIFE-1328 yzhong4 end-->
<input name='stopper1' class="invisibleSubmit" type="BUTTON" onFocus="selectFirstField();" TABINDEX=1>
<input name='busddates' id='busddates' style="visibility:hidden" value="<%=((SMARTAppVars)AppVars.getInstance()).getBusinessdate()%>">
<input name='dateFormat' id='dateFormat' style="visibility:hidden" value="<%=((SMARTAppVars)AppVars.getInstance()).getAppConfig().getDateFormat()%>">
<input name='lang1' id='lang1' style="visibility:hidden" value="<%=lang1%>">
<input name='langCode' id='langCode' style="visibility:hidden" value="<%=langCode1%>">
<input name='dropSelect' id='dropSelect' style="visibility:hidden" value="<%=resourceBundleHandler.gettingValueFromBundle("Select")%>">
<input name='imgpath' id='imgpath' style="visibility:hidden" value="<%=cs1ctx%>/screenFiles/">
<input name='hiddeninput' id='hiddeninput' style="position: relative; top:0; left:0; width:0; z-index:2;">
<input name='msgresult' id='msgresult' type="hidden" ><!-- ILIFE-5717 -->
</div>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.ACTIVE_FIELD%>" <%=AppVars.hf.getLit("value=\"\">")%>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.PLACE_HOLDER%>" value="<%=request.getSession().getAttribute(RequestParms.PLACE_HOLDER)%>"<%=AppVars.hf.getLit(">")%>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.PROCESSTIME%>" <%=AppVars.hf.getLit("value=\"\">")%>

