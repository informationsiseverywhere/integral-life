﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             

<%@page import="com.csc.util.XSSFilter"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.*' %>
<%@ page import='com.quipoz.framework.screenmodel.*' %>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel' %>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars" %>
<%@ page import="com.quipoz.framework.datatype.StringBase"%>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.properties.PropertyLoader" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%

String imageFolder = PropertyLoader.getFolderName(request.getLocale().toString());
BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
  ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw3.getScreenName(),request.getLocale());
  
  //COBOLAppVars cobolAv3 = (COBOLAppVars)bm3.getApplicationVariables();
  COBOLAppVars cobolAv3 = (COBOLAppVars)fw3.getAppVars();
  //String lang1  = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();
  String lang1  = fw3.getAppVars().getUserLanguage().toString().toUpperCase();
  AppConfig appCfg = AppConfig.getInstance();
  response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  response.addHeader("X-Content-Type-Options", "NOSNIFF");
  response.addHeader("Cache-Control", "no-store"); 
  response.addHeader("Pragma", "no-cache"); 
  String screenName = fw3.getScreenName();
%>


<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--  LINK REL="StyleSheet" HREF="theme/<%=lang1.toLowerCase() %>/style.css" TYPE="text/css" -->
<!-- bootstrap -->
<link href="bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="bootstrap/integral/integral-admin.css" rel="stylesheet" type="text/css">

<script language='javaScript' src='js/xmlReader.js'></script>
<script language='javaScript'>
	<%	char[] c = cobolAv3.additionalValidKeys.toCharArray();
	    String validKeys = "[";
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '1') {
				validKeys+= i+",";
			}
		}
		if (validKeys.endsWith("]")){
			validKeys+= "";//empty array
		}else{
			validKeys = validKeys.substring(0,validKeys.length()-1)+"]";
		}
	%>
	var validKeyArr = <%=validKeys%>;
	var isSupported = false;
	var lang1 = "<%=lang1%>";
	function isSupportKey(validKeyArr,action){
		if (validKeyArr.length > 0){
			for (var i=0;i<validKeyArr.length;i++){
				if (validKeyArr[i] == action){
					isSupported = true;
					break;
				}
			}
			if (isSupported){
				clearFField(); // fix bug46
				doAction('PFKEY0'+action);
			}else{
			<%-- /*<% if ("CHI".equals(lang1)) { %>
				alert("功能键" + action + "不能在当前屏幕使用");
			<% } else {%> --%>
			    alert('<%=resourceBundleHandler.gettingValueFromBundle("Function key")%>' + action + '<%=resourceBundleHandler.gettingValueFromBundle("is not active on this screen at this time")%>'+'.');
				//alert("Function key " + action + " is not active on this screen at this time.");
			<%--  <%}%>*/ --%>
			callCommonAlert(lang1,"No0002",action);
			}
		}else{
			//alert("no keys are supported");
			callCommonAlert(lang1,"No00020");
		}
	}

  
</script>
<%@ page session="false" %>
<title>Generic SideBar</title>


<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%
	HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel != null) {
    	baseModel.getApplicationVariables();
        ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
        //add by gulizhi
        Class svClass = fw.getVariables().getClass();


%>
<script type="text/javascript">
function OPSTo(nextField){
	var fieldName=nextField;
	var doc=parent.frames["mainForm"].document;
	if(doc.getElementById(fieldName)!=null)
	{
		doc.getElementById(fieldName).value="X";
		doAction('PFKEY0');
	}
}

function hyperLinkTo(nextField){
	if (nextField != null){
		nextField.value="X";
		doAction('PFKEY0');
	}
}
function changeImg(thi,path){
	thi.src=path;
}

function disableIcon(thi){
	thi.src = thi.src.replace(".png","_after.png");
	thi.disabled = "true";
}
function removeXfield(xfield){
	var doc=parent.frames["mainForm"].document;
	doc.getElementById(xfield.id).value="";
	doAction('PFKEY05');
}

</script>
</HEAD>
<BODY id="sideBar" style="font-size: 14px !important; background-color: #ffffff !important; border: 1px solid gainsboro !important;">
<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) Begin-->
<%
	String msgs = "No Message";
	String fontStyle=" font-family:Arial; font-weight:bold; font-size:12px;";
	String lang=request.getParameter("lang");
	if("CHI".equalsIgnoreCase(lang)){
		msgs="没有信息";
		fontStyle=" font-type:宋体; font-weight:normal; font-size:12px;";
	}
	try {
		sess = request.getSession();
		BaseModel bm = (BaseModel) sess.getAttribute( BaseModel.SESSION_VARIABLE );
		AppVars avs = bm.getApplicationVariables();
		lang=avs.getUserLanguage().toString().trim();
		if (!avs.mainFrameLoaded) {
  			for (int i=0; i<40; i++) {
				avs.waitabit(250);
	  			if (avs.mainFrameLoaded) {
		  			break;
		  		}
  			}
  		}
		if (!avs.mainFrameLoaded) {
		  	msgs = "Messages failed to load after 10 seconds."
		  		+ "<br>This is usually caused by slow response on the main form; errors may be incorrect."
		  		+ "<br>Press the refresh button here when the main form loads."
		  		+ "<button onClick='document.location.reload(false)'>Refresh errors</Button>";
		}
		else {
			String ctx = request.getContextPath() + "/";
			MessageList list = avs.getMessages();
			Iterator i = list.iterator();
			StringBuffer sb = new StringBuffer();
			while (i.hasNext()) {
				 String str = i.next().toString();
                 HTMLFormatter formatter = new HTMLFormatter();

				/* Remove any trailing ? which is historic and means "stop underlining" */
				str = QPUtilities.removeTrailing(str.trim(), "?");
				//added by wayne to parse errorno
				String[] arr = str.split("ErrorMessage");
				if (arr.length < 2)
					sb.append(str + "<br>");
				else{
					sb.append(arr[0]);
					for (int j=1;j<arr.length-1;j++){
						//bootstrap
					sb.append("<div>")
						.append("<a class='err-msg' href='#'>")
						.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?")))
						.append("</a></div>");
					}
					//bootstrap
					sb.append("<div>")
						.append("<a class='err-msg' href='#'>")
						.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[arr.length-1].substring(4), "?")))
						.append("</a></div>");
				}
			}


			msgs =  sb.toString();
				if(screenName.equals("Sr2de") || screenName.equals("Sr2g3")){//IJTI-1024
					StringBuilder sb1 = new StringBuilder("");
					String errorFile = XSSFilter.stripXSS2((String)sess.getAttribute("errorFile"));//IBPTE-1846
					String fileUploadErrorMessage = XSSFilter.stripXSS2((String)sess.getAttribute("fileUploadErrorMessage"));//IJTI-1287
					String fileUploadMessage = XSSFilter.stripXSS2((String)sess.getAttribute("fileUploadMessage"));//IBPTE-1846
					String fileUploadUtilityContext = XSSFilter.stripXSS2((String)sess.getAttribute("fileUploadUtilityContext"));//IBPTE-1846
					if(fileUploadMessage!=null){
						sb1.append(fileUploadMessage);
						sess.removeAttribute("fileUploadMessage");
					} 
					if(fileUploadErrorMessage!=null){
						//IJTI-1106 starts
						sb1.append("<div>")
						.append("<a class='err-msg'>")
						.append(fileUploadErrorMessage)
						.append("</a></div>");
						sess.removeAttribute("fileUploadErrorMessage");
						//IJTI-1106 ends
					}
				if(errorFile!=null){
						sess.removeAttribute("errorFile");
						String errorFileName = HtmlUtils.htmlEscape((String)sess.getAttribute("errorFileName"));
						if(errorFileName!=null){
							sb1.append("Download error file for more details <br/>");
							sb1.append("<a href="+'"'+"/"+fileUploadUtilityContext+"/filedownload?fileuploaderrorfile="+errorFile+
								"&errorFileName="+errorFileName+'"'+
								">"+errorFileName+"</a>");
							sess.removeAttribute("errorFileName");
							sess.removeAttribute("fileUploadUtilityContext");
						}
					}
					if(!sb1.toString().equals("")){
						msgs = sb1.toString();
					}
				}
			msgs=msgs.replace("Message:",""); //IJTI-1389
		}
	}
	catch (Exception e) {
	}
%>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
public static String emulationVer="off";
%>
<%	
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	emulationVer = "off";	
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10;
		}
	} else if (userAgent.contains("Trident/7.0")) {
			browerVersion = IE11;
		    if(AppConfig.ieEmulationEnable) emulationVer =  IE11;	
	}		
%>


<script src="bootstrap/sb/vendor/jquery/jquery.js"></script>
<script language="javascript">
   	// Ai Hao 2010-05-07
   	// if there is no error message, hide the error message panel
   	//bootstrap
   	$(document).ready(function(){
		if(<%=msgs.length()%> == 0)	$("#msg-panel").css("display","none");
		calculateMenuHeight();
   	});
   	function calculateMenuHeight(){
   		var screenBodyHeight = screen.height;
   		// ILIFE-8864 STARTS
		var logoHeight = 0; 
		if($("#sidebar-hide").length > 0){
			logoHeight = 20;
		}
		// ILIFE-8864 ENDS
		var sidebarBodyHeight = $("#sideBar").height();
   		var logoHeight = $("#topPanel").outerHeight(true);
   		var sidebar = $(".sidearea");
   		var message = $(".err-msg");
   		var messPane = $("#msg-panel");
   		if(screenBodyHeight > 1000) {
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = sidebarBodyHeight - logoHeight + 5;
   			}else{
   				var sidebarHeight = sidebarBodyHeight - logoHeight;
		}
   		}else{  
   			if(<%=msgs.length()%> == 0){
   				var sidebarHeight = screenBodyHeight - 200 + 5;   //ILIFE-6711
   			}else{
   				var sidebarHeight = screenBodyHeight - 200;   //ILIFE-6711
		}
   		}
   		 	
   		sidebar.css('height', sidebarHeight + "px");
   	}
</script>
<!-- ILIFE-8864 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<div id="sidebar-hide" style="padding-left: 93%;">
	<span id="sidebar-pin" class="glyphicon glyphicon-pushpin" style="line-height: 20px; cursor:pointer"></span>
	<span id="sidebar-unpin" class="glyphicon glyphicon-chevron-left" style="line-height: 20px; cursor:pointer"></span>
</div>

<script>
	$(document).ready(function(){
		var mainDoc=parent.frames["mainForm"];
		var msgDiv = document.getElementById("msg-panel");
		if(msgDiv.style.display !=="none"){
			mainDoc.document.getElementById("msg-panel").innerHTML += msgDiv.innerHTML;
			mainDoc.document.getElementById("msg-panel").style.display="block";
		}	
	})
	if(sessionStorage.getItem("sidebar") !== "pined"){
		$("#sidebar-pin").css("display", "block");
		$("#sidebar-unpin").css("display", "none");
	}else{
		$("#sidebar-pin").css("display", "none");
		$("#sidebar-unpin").css("display", "block");
		window.top.document.getElementsByName("realContent")[0].cols = "275,*";
	}
	$("#sidebar-hide>span").click(function(){
		if(sessionStorage.getItem("sidebar") === "pined"){
			$("#sidebar-pin").css("display", "block");
			$("#sidebar-unpin").css("display", "none");
			sessionStorage.setItem("sidebar", "unpined");
			window.top.document.getElementsByName("realContent")[0].cols = "0,*"
		}else{
			$("#sidebar-pin").css("display", "none");
			$("#sidebar-unpin").css("display", "block");
			sessionStorage.setItem("sidebar", "pined");
		}
	});
</script>
<%}%>
<!-- Menu Sidebar begin -->
<!-- bootstrap sidebar begin -->
<%if(!screenName.equals("Sr9gb")){%> <!-- ILIFE-8864 --> 
<div class="sidearea" style="overflow: auto;">
	<%	// To decide whether menus are displayed or not
	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
	boolean isShow = av.isMenuDisplayed();
	if  (!isShow){
	%>

        <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="display: block;">
                    <ul class="nav" id="side-menu">
                        <li class="active">
							<a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Extra_Info")%></a>
							<ul class="nav nav-second-level" aria-expanded="false" id='sidebar_OPTS'>
							</ul>
						</li>
					</ul>
				</div>
		</div>
		
        <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="display: block;">
                    <ul class="nav" id="side-menu">
                        <li>
							<a href="#"><%=resourceBundleHandler.gettingValueFromBundle("Functions")%></a>
							<%
							String sa[]=fw.getFormActions();
							int k=sa.length;
							String link[]=new String[k];
							String fSize[] = null;
							String interValue=null;
							int order=0;
							for (int i = 0; i < sa.length; i++) {
								fSize = sa[i].split("/");
								if(fSize!=null)
								{
									for(int j =0; j<fSize.length;j++)
									{
										if(order==0)
										{
											link[i]= resourceBundleHandler.gettingValueFromBundle(fSize[j]);
										}
										else
										{
											link[i]= link[i]+"/"+resourceBundleHandler.gettingValueFromBundle(fSize[j]);
										}
											order++;
									}
									order=0;
								}
							}
							%>							
							<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_functions'>
								<%=AppVars.hf.getHTMLFormActionButtons(link)%>
							</ul>
						</li>
					</ul>
				</div>
		</div>	
		
		<div class="panel panel-info" id='msg-panel' style="margin-bottom:0px ; border-color: #bce8f1">
		    	<div class="panel-heading" style="font-weight: bolder; text-align: center;padding-right: 40px;color: #fff !important;
    background-color: #d9edf7 !important;
     border-color: #bce8f1 !important; color: #31708f !important"><%=resourceBundleHandler.gettingValueFromBundle("Messages")%></div>
		    	<div class="panel-body">     
					<div class="row" style="padding-left: 15px !important;">	
						<%=msgs%>
					</div>
				</div>
		</div>
	<%}else{ %>
	
        <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="display: block;">
                    <ul class="nav" id="side-menu">
                       
                    </ul>
                </div>
            </div>
            
            
    <input type="text" id="lang_name" value="<%=lang %>" style="visibility: hidden;">     
     
	<script type="text/javascript">
	function genMenuHead(menuName){
		
		var languageName =document.getElementById('lang_name').value;
		//#IJTI-432 Start
	  	var validLangValue= ["eng","chi","spa","por","jap","arb"];
	  	var isValidLang=0;
	  	for(var index=0;index<validLangValue.length;index++) {
	  		if(languageName==validLangValue[index]){
	  			isValidLang=1;
	  			break;
	  		}
	      }
	  	if(isValidLang==0){
	  		languageName="eng";
	  	}
	  	//#IJTI-432 End
		if(menuName=='Proposals/New Business'){
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+'ProposalsNewBusiness'+'.'+'png'
			
		}else if(menuName=='File Upload/Download'){
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+'File Upload Download'+'.'+'png'
			
		}else{
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+menuName+'.'+'png'
		}
		menuName = '  ' + menuName;
		return "<li><a href='#' ><img src='"+imgurl+"' style='width: 20px; margin-left: 2px'>"+  menuName + "<span class='fa arrow'></span></a><ul class='nav nav-second-level'>";
		
	}
	function genUXMenuHead(menuName, UXMenuName, uri){
		var uiuxUrl = '<%=IntegralConfig.getUIUXUrl()%>';
		var languageName =document.getElementById('lang_name').value;
		//#IJTI-432 Start
	  	var validLangValue= ["eng","chi","spa","por","jap","arb"];
	  	var isValidLang=0;
	  	for(var index=0;index<validLangValue.length;index++) {
	  		if(languageName==validLangValue[index]){
	  			isValidLang=1;
	  			break;
	  		}
	      }
	  	if(isValidLang==0){
	  		languageName="eng";
	  	}
	  	//#IJTI-432 End
		if(menuName=='Proposals/New Business'){
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+'ProposalsNewBusiness'+'.'+'png'
			
		}else if(menuName=='File Upload/Download'){
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+'File Upload Download'+'.'+'png'
			
		}else{
			var imgurl ='screenFiles'+'/'+languageName+'/'+'menu'+'/'+menuName+'.'+'png'
		}
		UXMenuName = '  ' + UXMenuName;
		return "<li><a href='"+uiuxUrl+uri+"' target='_top' ><img src='"+imgurl+"' style='width: 20px; margin-left: 2px'>"+  UXMenuName + "</a></li>";
	}
	function genMenuSubItem(subMenuName,menulink){
		return "<li><a href='javascript:;' onClick=doMenuClick('" + menulink + "')>" + subMenuName + "</a></li>";
	}
	function genMenuTail(){
		return "</ul></li>";
	}
	function doMenuClick(menuLink){
		var sideFrame = parent.frames["mainForm"];
		/* ILIFE-8520 START */
		var key = parent.window.masterMenu.filter(f => f[2] == menuLink);
		if(key[0]){
			sideFrame.document.form1.activeField.value = key[0][3];
		}
		else{
			sideFrame.document.form1.activeField.value = menuLink;
		}
		/* ILIFE-8520 END */
	    if (sideFrame.document.form1.screen.value == "S0017"){
	    	sideFrame.document.form1.action_key.value = "PFKEY0";
	    }else{
	    	sideFrame.document.form1.action_key.value = "PFKEY03";
	    }	
	    sideFrame.doSub();
	}
	$(document).ready(function(){
		var uiuxMenu;
		<%if(!IntegralConfig.getUIUXMenu().isEmpty() && IntegralConfig.isUIUXIntegrationEnable()){%>
			uiuxMenu = <%=IntegralConfig.getUIUXMenu()%>;
		<%}%>
		var sideMenu = "";
		
		var sideSystemMenu = parent.window.systemMenu;
		var sideMasterMenu = parent.window.masterMenu;
		for(var i=0;i<sideSystemMenu.length;i++){
			sideMenu += genMenuHead(sideSystemMenu[i][0]);
			for(var j=0;j<sideMasterMenu.length;j++){
				if(sideMasterMenu[j][0] == sideSystemMenu[i][1]){
					sideMenu += genMenuSubItem(sideMasterMenu[j][1],sideMasterMenu[j][2]);
				}
			}
			sideMenu += genMenuTail();
		}
		if(<%=IntegralConfig.isUIUXIntegrationEnable()%>){
			for(var x=0; x < uiuxMenu.length; x++){  	
				sideMenu += genUXMenuHead(uiuxMenu[x].masterMenu, uiuxMenu[x].article, uiuxMenu[x].uri)
			}
		}
		$("#side-menu").append(sideMenu);
	});
	 </SCRIPT>
	<div class="panel panel-info" id='msg-panel' style="margin-bottom:0px;">
	    	<div class="panel-heading" style="font-weight: bolder; text-align: center;padding-right: 40px;">
	        	<%=resourceBundleHandler.gettingValueFromBundle("Messages")%>
	         </div>
	
	    	<div class="panel-body">     
				<div class="row" style="padding-left: 15px !important;">	
					<%=msgs%>
				</div>
			</div>
	</div>
	<%}%>		
					
</div>
	<%}%>  <!-- ILIFE-8864 --> 
<!-- bootstrap sidebar end -->

<Script language="javascript">
//timer=setInterval(checkload,100) ;
waitMainFormReady();
function loaddata(){

    /* Wayne Yang 2010-03-29
     For Option Switch:
     To copy code block from mainForm to the side bar page.
     In this way, no need to add extra info in *.xml.
     Just need to setup the Hyper link in SxxxxForm.jsp . Then copy them to side bar page. */
	var doc1=parent.frames["mainForm"].document;
	
   	/* Wayne Yang 2010-05-07
   	   For handle-writting functions. e.g. S4050 cancel function
   	*/
   	var obj1 = doc1.getElementById("mainForm_functions");
	if(obj1 !=null)
   	{
   		document.getElementById("sidebar_functions").innerHTML =  obj1.innerHTML;
   	}
}
function waitMainFormReady()
{
	if(parent.frames["mainForm"].document.readyState == "complete")
	{
		loaddata();
	}
	else
	{
		//alert("wait");
		setTimeout("waitMainFormReady()",100);
	}
}


//added since sidebar.js is useless
function hyperLinkTo(nextField) {
	if (nextField != null){
		nextField.value="X";
	    parent.frames["mainForm"].document.form1.action_key.value = "PFKEY0";
	    parent.frames["mainForm"].doSub();
	}	
}

function doAction(act) {
    parent.frames["mainForm"].document.form1.action_key.value = act;
    parent.frames["mainForm"].doSub();
	pageloading();
  }

  function pageloading(){
	  /* modified  ILIFE-1328 yzhong4 start*/
		 //Modified by Shawn @20100721 for new UI Frame
		var str ="<DIV style='BORDER: #455690 2px solid;Z-INDEX: 99999; LEFT: 250px;WIDTH: 200px; POSITION: absolute; TOP: 150px; HEIGHT: 120px; BACKGROUND-COLOR: #eeeeee'>"  
			str += "<TABLE style='BORDER-TOP: #ffffff 1px solid; BORDER-LEFT: #ffffff 1px solid' cellSpacing=0 cellPadding=0 width='100%' bgColor=#cfdef4 border=0>"  
				str += "<TR style='background-color:#35759b;'>"  
					str += "<TD style='FONT-SIZE: 12px;COLOR: #0f2c8c' width=30 height=24></TD>"  
						str += "<TD style='PADDING-LEFT: 4px; background-color:4d81b1; FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #eeeeee; PADDING-TOP: 4px' vAlign=center width='100%'>INTEGRAL Admin</TD>"  
							str += "<TD style='PADDING-RIGHT: 2px; PADDING-TOP: 2px' vAlign=center align=right width=19>"  
								str += "<SPAN style='FONT-WEIGHT: bold; FONT-SIZE: 12px; CURSOR: hand; COLOR: red; MARGIN-RIGHT: 4px' id='btSysClose' >&nbsp;</SPAN></TD>"  
									str += "</TR>"  
										str += "<TR style='background-color:#eeeeee;'>"  
											str += "<TD style='PADDING-RIGHT: 1px;PADDING-BOTTOM: 1px;margin: 0 auto;text-align:center;' colSpan=3 height=" + 92 + ">"  
												str += "<DIV style='PADDING-RIGHT: 8px;PADDING-LEFT: 8px; FONT-SIZE: 12px; PADDING-BOTTOM: 8px; WIDTH: 100%; COLOR: #1f336b; PADDING-TOP: 8px;HEIGHT: 100%;'><img border=0 src='" + contextPathName + "/screenFiles/waiting.gif'/><BR>"    
													str += "</DIV>"  
														str += "</TD>"  
															str += "</TR>"  
																str += "</TABLE>"  
																	str += "</DIV>"  ;
		/* modified  ILIFE-1328 yzhong4 end*/
		var bgObj=document.createElement("div"); 
		var isIe = (document.all)?true:false;//Cross browser for IE and FF.
		bgObj.setAttribute('id','loaddiv'); 
		var styleStr="Position:absolute; top:35px; z-index:9999; left:2px;width:783px;height:550px;background:#eeeeee;"; 
		bgObj.style.cssText=styleStr; 
		bgObj.innerHTML=str;
		var sidobj=document.createElement("div");
		sidobj.setAttribute('id','siddiv'); 
		var sidstyleStr="Position:absolute; top:90px; z-index:9999; left:5px;width:220px;height:470px;background:#eeeeee;"; 
		sidstyleStr+=(isIe)?"filter:alpha(opacity=50);":"opacity:50;"; 
		sidobj.style.cssText=sidstyleStr; 

		var timeout=10;
		var timer=0;
		var fun = function(){  
			timeout--; 
			if(timeout==0) {//Modified it for frameset has been changed by Michelle(2010-7-14)
				//parent.frames[1].document.body.appendChild(sidobj);
				//parent.frames[2].document.body.appendChild(bgObj);
				window.clearInterval(timer);  
			}
		}  

		timer = window.setInterval(fun,80)   
	}
</script>

<%}%>

    <!-- bootstrap -->
    <script src="bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/sb/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="bootstrap/sb/dist/js/sb-admin-2.js"></script>
    <script src="bootstrap/integral/integral-admin-sidebar.js"></script>
    
</BODY>
</HTML>
