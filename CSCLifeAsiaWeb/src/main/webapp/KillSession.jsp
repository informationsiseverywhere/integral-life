<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="org.slf4j.Logger"%>
<%@page import="org.slf4j.LoggerFactory"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Emergency stop</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.quipoz.framework.sessionfacade.*"%>
</head>
<%
    Logger LOGGER = LoggerFactory.getLogger(getClass());
	try {
		LOGGER.info("KillSession is having a go V2 ...");
		GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
		LOGGER.info("KillSession got the GD ...");
		if (gd != null) {
			BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
			if (bm != null) {
				LOGGER.info("KillSession is cleaning up ...");
				GeneralSession impl = gd.getImpl();
				if (AppVars.getInstance().getAppConfig().server.equalsIgnoreCase("EJB")) {
					LOGGER.info("KillSession is removing a Session Bean ...");
					((GeneralSessionFacade)impl).remove();
				}
				else {
					LOGGER.info("KillSession is removing a Local Bean ...");
					gd.cleanup(bm);
				}
				LOGGER.info("KillSession has cleaned up !");
			}
			else {
				LOGGER.info("KillSession got the GD, but the BaseModel is null!");
			}
		}
		else {
			LOGGER.info("Error cleaning up the EJB session, gd is null");
		}
	}
	catch (Exception e) {
		LOGGER.error("Error while cleaning up the EJB session", e);
	}
	session.invalidate();
%>
<BODY class="main">
Killing your session, please wait ...
</BODY>
<Script>
	window.close();
</Script>
</HTML>