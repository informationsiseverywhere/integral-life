<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.util.*" %>
<% String ctx = request.getContextPath() + "/";%>
<HTML LANG="EN">
  <HEAD>
  <%
	AppVars av = AppVars.getInstance();
	if(av.getAppConfig().getJspConfigPath()!=null && !av.getAppConfig().getJspConfigPath().isEmpty()){
		av.setJspConfigPath(av.getAppConfig().getJspConfigPath());
	}%>
 	<%if(av.getJspConfigPath().equals("jsp")){ %>
	 	<meta http-equiv="pragram" content="no-cache">
	  	<meta http-equiv="cache-control" content="no-cache, must-revalidate">
	  	<meta http-equiv="expires" content="0">
		<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
		<link href="<%=ctx%>bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
	<%}else{ %>
	    <LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
	    <LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
	<%} %>
    <TITLE> ERROR PAGE </TITLE>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="icon" href="screenFiles/dxc-titlebar-logo.svg"><!-- IBPLIFE-5274 -->
  <HEAD>
  
  <%if(av.getJspConfigPath().equals("jsp")){ %>
  	<FRAMESET COLS="200,*" frameBorder="0" framespacing="0">
    <FRAME SRC="<%=ctx%>error_2.jsp" NAME="frameMenu" style="border: 0;overflow-y : auto;">
    <FRAMESET ROWS="*,100" frameBorder="0" framespacing="0">
      <FRAMESET ROWS="52,*" noscroll frameBorder="0" framespacing="0">
  <%} else {%>  
    <FRAMESET COLS="150,*" frameBorder="0" framespacing="0">
    <FRAME SRC="<%=ctx%>error_2.jsp" NAME="frameMenu">
    <FRAMESET ROWS="*,100" frameBorder="0" framespacing="0">
      <FRAMESET ROWS="60,*" noscroll frameBorder="0" framespacing="0">
  <%}%>  
        <FRAME SRC="<%=ctx%>error_1.jsp" NAME="frameTitle" scrolling="no">
        <FRAME SRC="<%=ctx%>error_3.jsp" NAME="frameForm">
      </FRAMESET>
    </FRAMESET>
  </FRAMESET>
</HTML>