<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             
<%@ page session="false" %>
<%@ page session="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.Map" %> 
<%@ page import="java.util.*" %>  
<%@ page import="java.util.HashMap" %>  
<%@ page import="com.quipoz.framework.screendef.QPScreenField" %>   
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%@ page import="com.csc.life.enquiries.screens.*" %> 
<%@ page import="com.csc.life.cashdividends.screens.*" %> 
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%@ page import="com.csc.life.terminationclaims.screens.*" %> 
<%@ page import="com.csc.smart.screens.*" %>
<%@ page import="com.csc.fsu.financials.screens.*" %>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="com.csc.fsuframework.core.FeaConfg"%>

<%@page import="com.csc.life.enquiries.screens.S6241ScreenVars"%>
<%@page import="com.csc.fsu.general.screens.Sr343ScreenVars"%>
<%@page import="com.csc.smart400framework.batch.cls.Qlrsetce"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.properties.PropertyLoader"%>
<html>
<head>
<link href="bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="bootstrap/integral/integral-admin.css" rel="stylesheet" type="text/css">

<script language='javaScript' src='js/xmlReader.js'></script>
<script src="bootstrap/sb/vendor/jquery/jquery.js"></script>
<title></title>
<%--sb admin begin  --%>
<meta name="viewport" content="width=device-width, initial-scale=1">

<%--sb admin end --%>
<%
      String ctx = request.getContextPath() + "/";
	  BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
	  if ( baseModel == null) {
	    return;
	  }
	  ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	  LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
	  String screenNumber = this.getClass().getSimpleName();
	    if (screenNumber.endsWith("_jsp")) {
	        screenNumber = QPUtilities.removeTrailing(screenNumber, "_jsp");
	    } else {
	        screenNumber = screenNumber.substring(1);
	    }

	    //MIBT-57 STARTS
	    if(screenNumber.startsWith("_")){
	        screenNumber = screenNumber.replaceFirst("_","");
	    }

	    ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(
	            fw.getScreenName(), request.getLocale());
%>
<%-- <script type='text/javascript' src='<%=ctx%>dwr/engine.js'></script>
<script type='text/javascript' src='<%=ctx%>dwr/interface/testClass.js'></script>
<script type='text/javascript' src='<%=ctx%>dwr/util.js'></script> --%>

<!-- sb admin begin -->
<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"  rel="stylesheet" type="text/css">
<%-- <link href="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"> --%>
<link href="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.css" rel="stylesheet" media="screen">
<link href="<%=ctx%>bootstrap/integral/integral-admin.css"  rel="stylesheet" type="text/css">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<script src='<%=ctx%>bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/dist/js/sb-admin-2.js'></script>
<%-- <script src="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.js" charset="UTF-8"></script> --%>
<script src="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.js" charset="UTF-8"></script>
<script src="<%=ctx%>bootstrap/integral/integral-admin.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables/js/dataTables.fixedColumns.min.js"></script>

<%-- YY --%>
<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<%--sb admin end --%>

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.quipoz.COBOLFramework.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.*" %>
<%@ page import="com.quipoz.framework.datatype.*" %>
<%@ page import="com.quipoz.COBOLFramework.TableModel.*" %>
<%@ page import="com.csc.lifeasia.runtime.variables.*"%>
<%@ page import="com.csc.smart400framework.SmartVarModel" %>
<%@ page import="com.csc.smart400framework.AppVersionInfo" %>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@ page import="java.lang.reflect.Field" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.quipoz.framework.util.DataModel"%>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.resource.ResourceBundleHandler"%>

</head>
<body style="background-color: #5f249f !important"><!-- IBPLIFE-4730 -->
<%@ include file="commonScript1NEW.jsp"%>
<script>
function kill(language) {
	//x = confirm("Are you sure you want to interrupt and kill your session?");
	x = confirm(callCommonConfirm(language,"No00031"));
	if (x == true) {
		// fix bug147
		top.frames["heartbeat"].frames["heart"].clearTimeout(
			top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
		/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
		var killsession='<%=ctx%>logout?t=' + new Date();
		parent.location.replace(killsession);
	}
}

function exportExcel()
{
	 
	 parent.frames["mainForm"].getExcelSeedData();
	 
	}
</script>
<!-- ILIFE-8864 starts -->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<script>
	function openNav() {
	  window.top.document.getElementsByName("realContent")[0].cols = "275,*"
	  parent.frames["mainForm"].document.getElementById("msg-panel").style.display = "none";
	 }
	function closeNav() {
	  window.top.document.getElementsByName("realContent")[0].cols = "0,*";
	 }
	parent.frames["mainForm"].onmouseover = function(){
		if(sessionStorage.getItem("sidebar")!=="pined"){
			closeNav();
		}
	};
</script>
<%} %>
<!-- ILIFE-8864 ends -->

<%@page import="com.csc.smart400framework.SMARTAppVars"%>

<%

	if (baseModel == null) {
		return;
	}



	/* if (savedInds == null) {
	}
	appVars.isEOF(); */ /* Meaningless code to avoid a Java IDE warning message */
	av.reinitVariables();

	String userD=(String)request.getSession().getAttribute("LONGUSERID");//ILIFE-5986
	
	/* if(userD.trim().equals("")){
		userD=(String)request.getSession().getAttribute("LONGUSERID");//ILIFE-5986
	} */
	String lastLoginTime=(String)request.getSession().getAttribute("LastLoginTime");


	/*String[] time=lastLoginTime.split(" ");
	if(time.length>1){
	    lastLoginTime=resourceBundleHandler.gettingValueFromBundle("Login Time")+"&nbsp;"+time[0]+"&nbsp;"+resourceBundleHandler.gettingValueFromBundle("At")+"&nbsp;"+time[1];
		//lastLoginTime="Your last login was on&nbsp;"+time[0]+"&nbsp;at&nbsp;"+time[1];
	}else{
	    lastLoginTime=resourceBundleHandler.gettingValueFromBundle("Login Time")+"&nbsp;"+lastLoginTime;
		//lastLoginTime="Your last login was on&nbsp;"+lastLoginTime;
	}*/
	if ("never".equals(lastLoginTime)) {
        lastLoginTime = "";
	}/*else{
    	String[] time=lastLoginTime.split(" ");
    	lastLoginTime=resourceBundleHandler.gettingValueFromBundle("Login Time")+"&nbsp;"+time[0]+"&nbsp;"+resourceBundleHandler.gettingValueFromBundle("At")+"&nbsp;"+time[1];
	}*/
	else
	{
		String[] time=lastLoginTime.split(" ");
		if(time.length>1){
		    lastLoginTime=resourceBundleHandler.gettingValueFromBundle("Login time:")+"&nbsp;"+time[0]+"&nbsp;"+resourceBundleHandler.gettingValueFromBundle("At")+"&nbsp;"+time[1];
			//lastLoginTime="Your last login was on&nbsp;"+time[0]+"&nbsp;at&nbsp;"+time[1];
		}else{
		    lastLoginTime=resourceBundleHandler.gettingValueFromBundle("Login time:")+"&nbsp;"+lastLoginTime;
			//lastLoginTime="Your last login was on&nbsp;"+lastLoginTime;
		}
	}

%>



   <div class="row" style="border:0px;margin:0 0 0 0;background-color:#5f249f"><!-- IBPLIFE-4730 -->
    <%-- feedback version --%>
    <!-- ILIFE-8864 starts -->
	    <%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
	    	<div class="col-md-1" style="width:50px !important;padding-left:15px;padding-right:0px;border:0px;top:3px;">
	    		<span style="font-size:30px;cursor:pointer; color:#fff" id="openNav-btn" onmouseover="openNav()">&#9776;</span>
	    	</div>
	    <%} %>
		<!-- ILIFE-8864 ends -->
       <div class="col-md-1" style="width:80px !important;padding-left:15px;padding-right:0px;border:0px;top:-2px;"><!-- IBPLIFE-4730 -->
	         <img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/DXC_newlogo.png" width="55px" border="0"/><!-- IBPLIFE-4730 -->
        </div>
         <div class="col-md-2" style="width: 140px!important;padding-left:0px;padding-right:0px;border:0px;top:5px;">
	         <div style="font-size:15px;font-weight: bold;color: white !important;">DXC Assure Policy</div>
	         <table style="font-size: 10px !important;font-weight: bold;width:91%;">
		         <tr>
		         	<td style="color: #ffffff !important;"><%=AppConfig.getInstance().getVersion() %> </td>
		         	<td style="text-align: right; color: #fff !important;"><%=fw.getScreenName()%></td>
		         </tr>
	         </table>
        </div>
      
          <div class="col-md-2" ></div>      
   
    <%-- feedback version --%>
		<div class="col-md-7">
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; margin-top: -8px; margin-right:10px">
				<ul class="nav navbar-top-links navbar-right">	
					 
					 <%--IGB-615 Start --%>
					<%-- <%List<String> itemItem = av.findItemByTable(fw.getScreenName(),baseModel.getCompany().toString(),"1"); %> --%>
					<%List<String> itemItem = AppVars.getInstance().getExcelExportScreensName();%> <!-- ILIFE-8864 -->
					<%
					//IJTI-1625
					if (av.getAppConfig().isIDMFlagEnabled()) {
					%>
					<li><a href="<%=IntegralDMSConfig.getDMSURL()%>"
					target="_blank" style="color: #fff; font-size: 20px !important;"
					title="<%=resourceBundleHandler.gettingValueFromBundle("DMS")%>"><i
						class="fa fa-file" style="color: white;"></i></a></li>
					<%} %>

					<% 
					//IJTI-1153 starts
					if(FeaConfg.isFeatureExist("2", "UNDNB071", av,"IT")){ %>
						<li><a href="<%= IntegralConfig.getSisenseURL()%>" target="_blank" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Sisense")%>"><i class="fa fa-bar-chart" style="color:white;"></i></a></li>
					<%} 
					//IJTI-1153 ends
					%>
					<%if(itemItem.contains(fw.getScreenName())){ %> <%-- check Excel file download button enable or disable--%>
						 <li><a href="javascript:;" onClick="exportExcel();doAction('PFKEY69')" title="Download SFL data in csv file"><img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/eng/exceldownload.png" width="25px" border="0"/></a></li>
					<%}%><%-- IGB-615 End --%>	
					<!-- ILIFE-8752 start -->
				 	<%-- <li><a href="<%= AppVars.getInstance().getAppConfig().getPsUrl()%>" target="_blank" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("PS")%>"><i class="fa fa-file" style="color:white;"></i></a></li> --%>
			    	<li><a href="<%= AppVars.getInstance().getAppConfig().getPsUrl()%>" target="_blank" style="color: #fff;font-size:20px !important;top:2px;" title="<%=resourceBundleHandler.gettingValueFromBundle("Product Stage")%>"><i class="fa fa-briefcase" style="color:white;"></i></a></li>
				 	<!-- ILIFE-8752 end -->
					 <li><a href="javascript:;" onClick="doAction('PFKEY16')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Home")%>"><i class="fa fa-home"></i></a></li>
					<li><a href="javascript:;" onClick="doAction('PFKEY02')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Session Info")%>"><i class="fa fa-info-circle"></i></a></li>
				    <li><a href="javascript:;" onClick="doAction('PFKEY01')" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Help")%>"><i class="fa fa-question-circle"></i></a></li>
				    <li><a href="javascript:;" onClick="kill();return false;" style="color: #fff;font-size:20px !important;" title="<%=resourceBundleHandler.gettingValueFromBundle("Logout")%>"><i class="fa fa-sign-out"></i></a></li>
				    <%-- <%if(fw.getScreenName().equals("S0017")){ %> --%>
				    <li id="userLI" class="userInfo" style="width:max-content;" title="<%=userD%>&#13;<%=lastLoginTime%>&#13;<%=resourceBundleHandler.gettingValueFromBundle("Business Date")%>:<%=AppVars.getInstance().getBusinessdate()%>">
			        	<a href="#">               
			        	  <span class="glyphicon glyphicon-user"></span>
			        	  <span class="userspan"><%=userD%></span>
			        	</a>
			        </li>
				</ul>
			</nav>
		</div>
   </div>
     
   
   
   
</body>
</html>