package com.csc.lifeasia.runtime.variables;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.csc.lifeasia.runtime.core.LifeAsiaAppLocatorCode;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FormatterFactory;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.util.QPUtilities;

/**
 * @author daniel.peng
 * @version 1.0
 */
public class LifeAsiaBatchAppVars extends LifeAsiaAppVars implements Cloneable, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static final String ROUTINE = QPUtilities.getThisClass();

	public static final String QDATE = "QDATE";

	public static final String QTIME = "QTIME";

	public static final String QUTCOFFSET = "QUTCOFFSET";

	public static final String QMONTH = "QMONTH";

	public static final String QYEAR = "QYEAR";

	public static final long YEAR2000 = Date.valueOf("2000-01-01").getTime();

	public static final String QCENTURY = "QCENTURY";

	public static final String QDAY = "QDAY";

	public static final String QDATFMT = "QDATFMT";

	public LifeAsiaBatchAppVars() {
		super("test");
	}

	public LifeAsiaBatchAppVars(String strAppVarsName) {
		super(strAppVarsName);
		
		//START ILIFE-3261
		/** 
		 * If batch server is running. Related to IAF-625
		 */
		super.batchRunning = true;
		//END ILIFE-3261
	}

	/**
	 * <p>
	 * The purpose of this function is to implement RTVSYSVAL command.<br>
	 * Only the following system values are retrievable:
	 * <ol>
	 * <li>QDATE: the current system date, default format in iSeries is MonthDayYear (6 characters) or yydd 5
	 * characters.
	 * <li>QTIME: the current system time, format is HHmmss + millionths of a second (up to 12 characters). Note,
	 * implemented via timestamp, as written values past milliseconds will be zero. This could be fixed in Java 1.5.
	 * <li>QUTCOFFSET: the current offset from GMT, format is + or - HHmm (0 to 24 hour and 0 to 59 minute)
	 * <li>QMONTH: the current system month, format is 2 character value from 01 to 12.
	 * <li>QYEAR: the current system year, format is the last 2 character of the year (i.e 00 - 99).
	 * <li>QDAY: the current system day, format is 2 character day of the month (i.e. 01 - 31).
	 * <li>QDATFMT: the format of the QDATE returned, this is always MDY (currently only used to check the original date
	 * format and in this case it's always MDY).
	 * <li>QHOUR: the current system hour, in 24 hour format with 2 characters long.
	 * </ol>
	 *
	 * @param sysval Specifies the system value to be retrieved
	 * @param returnValue Returns the system value
	 */
	public static void retrieveSystemValue(Object osysval, BaseData returnValue) {
		String sysval = osysval == null ? "NULL" : osysval.toString().trim().toUpperCase();

		try {

			if (sysval.equals(QDATE)) {
				if (returnValue instanceof RPGDateData) {
					((RPGDateData) returnValue).setToday();
				} else if (returnValue.getLength() == 5) {
					returnValue.set(FormatterFactory.getSimpleDateFormat("yyddd").format(
					    new Date(System.currentTimeMillis())));
				} else {
					returnValue.set(FormatterFactory.getSimpleDateFormat("MMddyy").format(
					    new Date(System.currentTimeMillis())));
				}
			} else if (sysval.equals(QTIME)) {
				if (returnValue instanceof RPGTimeData) {
					((RPGTimeData) returnValue).setToday();
				} else {
					Timestamp ts = new Timestamp(System.currentTimeMillis());
					returnValue.set(FormatterFactory.getSimpleDateFormat("HHmmss").format(ts) + ts.getNanos());
				}
			} else if (sysval.equals(QUTCOFFSET)) {
				Date d = new Date(Calendar.ZONE_OFFSET);
				SimpleDateFormat formatter = FormatterFactory.getSimpleDateFormat("HHmm");
				if (Calendar.ZONE_OFFSET > 0) {
					returnValue.set("+" + formatter.format(d));
				} else {
					returnValue.set("-" + formatter.format(d));
				}
			} else if (sysval.equals(QMONTH)) {
				returnValue.set(new Date(System.currentTimeMillis()).toString().substring(5, 7));
			} else if (sysval.equals(QYEAR)) {
				returnValue.set(new Date(System.currentTimeMillis()).toString().substring(0, 4));
			} else if (sysval.equals(QCENTURY)) {
				returnValue.set(System.currentTimeMillis() > YEAR2000 ? "1" : "0");
			} else if (sysval.equals(QDAY)) {
				returnValue.set(new Date(System.currentTimeMillis()).toString().substring(8, 10));
			} else if (sysval.equals(QDATFMT)) {
				// this value is hard coded, as this is the default value in
				// iSeries and
				// QDATE is returning date in this format. The use of QDATFMT is
				// only for getting
				// existing QDATE format and change the QDATE to a new format
				// using another command.
				// Therefore, hard coding this value here will not impact the
				// result in the program.
				returnValue.set("MDY");
			} else if (sysval.equals(QHOUR)) {
				returnValue.set(new Time(System.currentTimeMillis()).toString().substring(0, 2));
			} else {
				throw new RuntimeException("System Value: " + sysval + " is not supported.");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	/*
	 * Class to resolve a program according to the application's class path. This overrides the COBOLAppVars version and
	 * actually does something with an AppLocator.
	 * @param progname
	 * @return
	 */
	public Object getProgram(Object progname) {
		addDiagnostic("Transfer-to '" + progname + "'");
		return LifeAsiaAppLocatorCode.find(progname.toString());
	}

}
