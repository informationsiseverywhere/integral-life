package com.csc.lifeasia.runtime.batch;

import com.csc.batch.quartz.BatchJobManagerFactory;

public class LifeAsiaBatchJobManagerFactory extends BatchJobManagerFactory {

	/**
	 * Returns a handle to the JobManagerQuartz instance
	 * 
	 * @param clazz the name of the job class to run in Quartz
	 */
	public LifeAsiaBatchJobManagerQuartz getQuartzInstance() {
		if (jobManager == null) {
			jobManager = new LifeAsiaBatchJobManagerQuartz();
		}
		return (LifeAsiaBatchJobManagerQuartz) jobManager;
	}
	
	/**
	 * Returns a handle to the JobManagerFactory instance
	 * 
	 * @param clazz the job class to run in Quartz
	 */
	public static LifeAsiaBatchJobManagerFactory getInstance() {
		if (instance == null) {
			instance = new LifeAsiaBatchJobManagerFactory();
		}
		return (LifeAsiaBatchJobManagerFactory) instance;
	}
	
}
