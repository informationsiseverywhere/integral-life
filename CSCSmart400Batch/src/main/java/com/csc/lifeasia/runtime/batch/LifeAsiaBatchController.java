package com.csc.lifeasia.runtime.batch;

import java.util.Date;

import com.csc.batch.BatchController;

public class LifeAsiaBatchController extends BatchController {


	public LifeAsiaBatchController() {
		super();
	}

	public void submitJob(String jobCommand, Object[] aryJobParameters, String jobName, String jobDescription,
	        String jobQ, String jobPty, String rtgDta, String log, String logCLPgm, String hold, String user,
	        Date entryDate, String rqsDta) {
		LifeAsiaBatchJobManagerFactory.getInstance().getQuartzInstance().submitJob(jobCommand, aryJobParameters, jobName,
		    jobDescription, jobQ, jobPty, rtgDta, log, logCLPgm, hold, user, entryDate, rqsDta);
	}
	
	public static LifeAsiaBatchController getInstance() {
		if (instance == null) {
			instance = new LifeAsiaBatchController();
		}
		return (LifeAsiaBatchController) instance;
	}
	
}
