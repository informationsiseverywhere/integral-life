package com.csc.lifeasia.runtime.batch;

import com.csc.batch.quartz.BatchJobManagerQuartz;
import com.csc.lifeasia.runtime.variables.LifeAsiaBatchAppVars;
import com.quipoz.framework.util.AppVars;

public class LifeAsiaBatchJobManagerQuartz extends BatchJobManagerQuartz {

	protected LifeAsiaBatchAppVars getAppVars() {
		LifeAsiaBatchAppVars av = (LifeAsiaBatchAppVars) LifeAsiaBatchAppVars.getInstance();
		if (av == null) {
			av = new LifeAsiaBatchAppVars("LifeAsiaBatch");
			AppVars.setInstance(av);
		}
		return (LifeAsiaBatchAppVars) av;
	}
}
