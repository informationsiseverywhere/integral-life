package com.csc.lifeasia.runtime.variables;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.fsu.general.dataaccess.AnumTableDAM;
import com.csc.fsuframework.variables.FsuAsiaAppVars;
import com.csc.lifeasia.runtime.core.LifeAsiaAppLocatorCode;
import com.csc.smart.dataaccess.FlddTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SmartVarModel;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datadictionarydatatype.DDScreenWindowing;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;

/**
 * Extends SMARTAppVars. Inncludes global variables applicable to Polisy.
 *
 * @author Quipoz - Max Wang
 * @version 1.0 April 2009<br>
 */
public class LifeAsiaAppVars extends FsuAsiaAppVars implements Cloneable, Serializable {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeAsiaAppVars.class);

	private static final String generalSearchSQL="SELECT SHORTDESC, LONGDESC FROM vm1dta.DESCPF WHERE DESCPFX='IT'" +
	" AND DESCCOY =? "  +
	"AND \"LANGUAGE\" = ? " +
	" AND " +
	" DESCTABL= ? ";

	/** Generated UID */
	private static final long serialVersionUID = 1260792893645326723L;

	/**
	 * Constructor with application name. Simply invokes super constructor
	 *
	 * @param pAppName -
	 */
	public LifeAsiaAppVars(String pAppName) {
		super(pAppName);
	}

	/**
	 * Class to resolve a program according to the application's class path.
	 * This overrides the COBOLAppVars version and actually does something with an
	 * AppLocator.
	 *
	 * @param progname
	 * @return
	 */
	public Object getProgram(Object progname) {
		addDiagnostic("Transfer-to '" + progname + "'");
		return LifeAsiaAppLocatorCode.find(progname.toString());
	}

	/**
	 * Routine available to free any resources eg caches. Frees cached Svcmtyp.
	 */
	public void freeResources(String... exceptFullClassNames) {
		super.freeResources(exceptFullClassNames);
	}
	//fixed bug 712	Tom Chi
	@Override
	public void rollback() throws ExtMsgException {
		// txConnection is null, means the transaction contorl has not been started yet
		if (txConnection == null) {
			addExtMessage("CPF8350" , "Commitment definition not found.");
		}
		// commit changes
		try {
			//txConnection.rollback();
			this.getHibernateSession().getTransaction().rollback();
			this.getHibernateSession().clear();
			this.getHibernateSession().getTransaction().begin();

			//added by David.Dong @ 2008-11-04 16:38
			COBOLAppVars appVars = (COBOLAppVars)AppVars.getInstance();
			List<FixedLengthStringData> list = appVars.alocnoMap.get(appVars.key);
			if(list != null){
				Varcom varcom = new Varcom();
				FixedLengthStringData anumrec = new FixedLengthStringData(10).init("ANUMREC");
				txConnection.setAutoCommit(false);
				Iterator<FixedLengthStringData> it = list.iterator();
				while(it.hasNext()){
					FixedLengthStringData s = it.next();
					AnumTableDAM anumIO = new AnumTableDAM();
					anumIO.setParams(s);
					anumIO.setFunction(varcom.writr);
					anumIO.setFormat(anumrec);
					SmartFileCode.execute(appVars, anumIO);
					it.remove();
					//Ticket#2 for dirty read - auto number that was roll backed
					LOGGER.info("LifeAsiaAppVars.rollback(), Insert auto number back to ANUMPF." +
								 " PREFIX={} AND GENKEY={} AND AUTONUM={}."
								 , new Object[]{anumIO.getPrefix(), anumIO.getGenkey(), anumIO.getAutonum()});
				}
				LOGGER.debug("========================= LifeAsiaAppVars.rollback()'s StackTrace ============================= ");
				for( StackTraceElement ste : new Throwable().getStackTrace()) {
					LOGGER.debug("	{}", ste);//IJTI-1498
				}
				LOGGER.debug("========================= End LifeAsiaAppVars.rollback()'s StackTrace ========================== ");
				txConnection.commit();
				txConnection.setAutoCommit(false);
			}
		} catch (SQLException e) {
			addDiagnostic(e.getLocalizedMessage());
			addExtMessage("CPF8359", "Rollback operation failed.");
		}

	}
	//end

	private String getRealFieldName(String tempFieldName){
		int j = tempFieldName.indexOf("Disp");
		if (j != -1){
			tempFieldName = tempFieldName.substring(0, j);
		}
		int rowNo = 0;
		/* If it is subfile field, then set subfile line no as well */
		int subfilePrefixIndex = tempFieldName.indexOf("_R");
		if (subfilePrefixIndex >= 0) {
			int subfileSuffixIndex =
				tempFieldName.lastIndexOf("_R");
			if (subfileSuffixIndex > subfilePrefixIndex) {
				rowNo = Integer.parseInt(
						tempFieldName.substring(
								subfileSuffixIndex+"_R".length()).trim());
				tempFieldName =
					tempFieldName.substring(
							subfilePrefixIndex+1, subfileSuffixIndex);
			} else {
				tempFieldName = tempFieldName.substring(subfilePrefixIndex+1);
			}
		}
		return tempFieldName;
	}


	/**
	 * return an array, first is field name, second is its table
	 * */
	private String[] findF4FieldTable(String fieldN, SmartVarModel vm, BaseModel bm){
		String[] s=new String[3];
		s[0]=fieldN;
		s[1]="";
		try {
			java.lang.reflect.Field f = vm.getClass().getField(fieldN);
			BaseData bd = (BaseData) f.get(vm);

			DD dd = new DD();
			DDScreenWindowing ddSw=dd.getScreenWindowing(bd, "S4033");
			if (ddSw != null) {
				if (ddSw.windowTable != null
				&& !ddSw.windowTable.equals("")) {
					s[1]=ddSw.windowTable;
					s[2]=ddSw.tableCompany;
					/**
					 * If  TCOY = "F"-use WSSP-FSUCO from appvar
					 * TCOY = "S"-use WSSP-COMPANY from appvar
					 * TCOY =  numeric-use directly from TCOY
					 * */
					if(s[2].trim().equalsIgnoreCase("F")){
						s[2]=bm.getFsuco().toString().trim();
					}else if(s[2].trim().equalsIgnoreCase("S")){
						s[2]=bm.getCompany().toString().trim();
					}
				}
				}else{
					if(s[0].trim().equals("agntype")){
						s[1]="T3692";
						s[2]="1";
					}else if(s[0].trim().equals("payfreq")){
						s[1]="T3590";
						s[2]="2";

					}else if(s[0].trim().endsWith("pstate")){
						s[1]="T3588";
						s[2]="2";
					}

				}
		}
		catch (Exception e) {
			s[1]="can't find window table of "+fieldN;
			LOGGER.error("findF4FieldTable(String, SmartVarModel, BaseModel)", e);
			return new String[]{fieldN,"",""};
		}
		return s;
	}
	/**
	 * Load all the f4 fields in the screen
	 *
	 * return Map<String, Map<String,String>>
	 *
	 * */
	public Map<String,Map<String,String>> loadF4Fields(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
		Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription

		StringBuilder sqlsb=new StringBuilder();
		sqlsb.append("SELECT DESCITEM, LONGDESC, DESCTABL,DESCCOY FROM vm1dta.DESCPF WHERE DESCPFX='IT'");
		//sqlsb.append(" AND DESCCOY ='1'");
		sqlsb.append(" AND \"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND DESCTABL IN(");

		String generalDropdownSearchSQL="";

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			fieldTable.put(fieldInf[1], field+","+fieldInf[2]);
			sqlsb.append("'"+fieldInf[1]+"',");
		}

		generalDropdownSearchSQL=sqlsb.toString();
		generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY DESCTABL ASC";

		Connection conn=null;

		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			LOGGER.error("loadF4Fields(String[], SmartVarModel, String, BaseModel)", e1);
		}

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = this.prepareStatement(conn, generalDropdownSearchSQL);
			LOGGER.info("Testing{}", generalDropdownSearchSQL);//IJTI-1498
			rs = this.executeQuery(ps);
			boolean found=false;
			int index=0;
			String itemtable="";//table code
			String itemcoy="";//company code
			boolean single=true;//only one record.
			//DESCITEM, LONGDESC, DESCTABL,DESCCOY
			while (rs.next()) {
					index++;
					found = true;
				if(index==1){
					itemcoy=rs.getString(4);
					itemtable=rs.getString(3);
					String coyt=fieldTable.get(itemtable).split(",")[1];
					//select fields which interact with company
					if(itemcoy.equalsIgnoreCase(coyt)){
					itemDesc.put(rs.getString(1).trim(), rs.getString(2));
					}


				}else{
					if(itemtable.equals(rs.getString(3))){
						String coyt=fieldTable.get(itemtable).split(",")[1];
						//select fields which interact with company
						itemcoy=rs.getString(4);
						if(itemcoy.equalsIgnoreCase(coyt)){
						itemDesc.put(rs.getString(1).trim(), rs.getString(2));
						}
					}else{
						single=false;
						String tempf=fieldTable.get(itemtable).split(",")[0];
						Map temp=new HashMap();
						temp.putAll(itemDesc);
						f4map.put(tempf,temp);
						itemDesc.clear();
						itemtable=rs.getString(3);
						String coyt=fieldTable.get(itemtable).split(",")[1];
						//select fields which interact with company
						itemcoy=rs.getString(4);
						if(itemcoy.equalsIgnoreCase(coyt)){
						itemDesc.put(rs.getString(1).trim(), rs.getString(2));
						}
					}

				}

			}

				String tempf=fieldTable.get(itemtable).split(",")[0];
				Map temp=new HashMap();
				temp.putAll(itemDesc);
				f4map.put(tempf,temp);

			if (found) {
				return f4map;
			}

		} catch (Exception e) {
			LOGGER.error("loadF4Fields(String[], SmartVarModel, String, BaseModel)", e);
		}
		finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}


		return f4map;
	}
	/**
	 * This method returns long description and item description from the database
	 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
	 * @param vm -  smartVarModel
	 * @param lang - Language to be passed e.g. E for English and CH for chinese
	 * @param bm - baseModel
	 * @return Map
	 * @author CSCINDIA Sep 29th 2009
	 */
	public Map<String,Map<String,String>> loadF4FieldsLong(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
		/*
		 * I18N change
		 */
		lang=this.getLanguageCode();
		/*
		 * I18N change
		 */
		StringBuilder sqlsb=new StringBuilder();

		sqlsb.append("SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL,D.DESCCOY FROM vm1dta.DESCPF D");
		sqlsb.append(" INNER JOIN vm1dta.ITEMPF I on D.DESCPFX=I.ITEMPFX ");
		sqlsb.append("AND D.DESCITEM=I.ITEMITEM AND D.DESCCOY = I.ITEMCOY AND D.DESCTABL = I.ITEMTABL");
		sqlsb.append(" WHERE I.VALIDFLAG='1' AND D.DESCPFX='IT'");
		sqlsb.append(" AND D.\"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND D.DESCTABL IN(");

		String generalDropdownSearchSQL="";
		String tblName ="";
		String company="";
		Connection conn=null;

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			tblName = "'"+fieldInf[1]+"',";
			company="'"+fieldInf[2]+"'";
			generalDropdownSearchSQL=sqlsb.toString()+tblName;//IJTI-1420
			generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1);
			generalDropdownSearchSQL=generalDropdownSearchSQL+") AND D.DESCCOY ="+company+" ORDER BY LONGDESC ASC";//IJTI-1420


			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("loadF4FieldsLong(String[], SmartVarModel, String, BaseModel)", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, generalDropdownSearchSQL);
				rs = this.executeQuery(ps);

				while (rs.next()) {
					itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("LONGDESC").trim());
				}
				f4map.put(fieldInf[0],itemDesc );
				itemDesc = new HashMap();
			}  catch (Exception e) {
			    LOGGER.error("", e);
			}finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}



	}
		return f4map;
}

	/**
	 * added by zhaohui for filter the language
	 */
	public Map<String,Map<String,String>> loadF4FieldsLongForLanguage(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
				/*
		 * I18N change
		 */
		lang=this.getLanguageCode();
		/*
		 * I18N change
		 */

		StringBuilder sqlsb=new StringBuilder();

		sqlsb.append("SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL,D.DESCCOY FROM vm1dta.DESCPF D");
		sqlsb.append(" INNER JOIN vm1dta.ITEMPF I on D.DESCPFX=I.ITEMPFX ");
		sqlsb.append("AND D.DESCITEM=I.ITEMITEM AND D.DESCCOY = I.ITEMCOY AND D.DESCTABL = I.ITEMTABL");
		sqlsb.append(" WHERE I.VALIDFLAG='1' AND D.DESCPFX='IT'");
		sqlsb.append(" AND D.\"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND D.DESCITEM like '");
		sqlsb.append(lang).append("%");
		sqlsb.append("' AND D.DESCTABL IN(");

		String generalDropdownSearchSQL="";
		String tblName ="";
		String company="";
		Connection conn=null;

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			tblName = "'"+fieldInf[1]+"',";
			company="'"+fieldInf[2]+"'";
			generalDropdownSearchSQL=sqlsb.toString()+tblName;//IJTI-1420
			generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1);
			generalDropdownSearchSQL=generalDropdownSearchSQL+") AND D.DESCCOY ="+company+" ORDER BY LONGDESC ASC,DESCITEM desc";//IJTI-1420

			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, generalDropdownSearchSQL);
				rs = this.executeQuery(ps);
				String lastDescitem = "";
				String thisDescitem;
				while (rs.next()) {
					thisDescitem = rs.getString("DESCITEM").trim();
					if(!((!lastDescitem.equals("")) && lastDescitem.contains(thisDescitem))){
						itemDesc.put(thisDescitem, rs.getString("LONGDESC").trim());
					}
					lastDescitem = thisDescitem;
				}
				f4map.put(fieldInf[0],itemDesc );
				itemDesc = null;
			}  catch (Exception e) {
			    LOGGER.error("", e);
			}finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}



	}
		return f4map;
}

	/**
	 * This method returns short description and item description from the database
	 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
	 * @param vm -  smartVarModel
	 * @param lang - Language to be passed e.g. E for English and CH for chinese
	 * @param bm - baseModel
	 * @return Map
	 * @author CSCINDIA Sep 29th 2009
	 */
	public Map<String,Map<String,String>> loadF4FieldsShort(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
		Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription

		StringBuilder sqlsb=new StringBuilder();
		sqlsb.append("SELECT DESCITEM, SHORTDESC, DESCTABL,DESCCOY FROM vm1dta.DESCPF WHERE DESCPFX='IT'");
		//sqlsb.append(" AND DESCCOY ='1'");
		sqlsb.append(" AND \"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND DESCTABL IN(");

		String generalDropdownSearchSQL="";
		String tblName ="";
		Connection conn=null;

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			tblName = "'"+fieldInf[1]+"',";
			generalDropdownSearchSQL=sqlsb.toString()+tblName;//IJTI-1420
			generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY SHORTDESC ASC";

			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("loadF4FieldsShort(String[], SmartVarModel, String, BaseModel)", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, generalDropdownSearchSQL);
				rs = this.executeQuery(ps);

				while (rs.next()) {
					itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("SHORTDESC").trim());
				}
				f4map.put(fieldInf[0],itemDesc );
				itemDesc = new HashMap();
			}  catch (Exception e) {
				LOGGER.error("loadF4FieldsShort(String[] , SmartVarModel , String lang,BaseModel)", e);
			}finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}



	}
		return f4map;
}


	/**
	 * Load F4 dropdown list by using separate Pxxxx program
	 *
	 * @param bd
	 * @param bm
	 * @param language
	 * @param isLong
	 * @return
	 *
	 * @author Wayne Yang 2010-03-24
	 */
	public Object loadF4(BaseData bd, BaseModel bm,String language, Boolean isLong){

		// First get Screen Window
		ScreenModel sm = bm.getScreenModel();
		SmartVarModel vm = (SmartVarModel)sm.getVariables();
		DDScreenWindowing ddSw = DD.getScreenWindowing(bd, sm.getScreenName());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("CO", bm.getCompany());
		map.put("FSUCO", bm.getFsuco());
		map.put("LANG", language);
		map.put("screenName", sm.getScreenName());
		if (ddSw != null) {
			if (ddSw.program1 != null && !ddSw.program1.trim().equals("")
					&& !ddSw.program1.equals("P0127")) {
				BaseData[] arr = new BaseData[5];
				FlddTableDAM flddIO = new FlddTableDAM();
				arr[1] = getField(vm, ddSw.additionalField1, flddIO);
				arr[2] = getField(vm, ddSw.additionalField2, flddIO);
				arr[3] = getField(vm, ddSw.additionalField3, flddIO);
				arr[4] = getField(vm, ddSw.additionalField4, flddIO);
				return callF4InProgram(ddSw, arr, isLong, map);
			}
		}

		return null;
	}

	private Object callF4InProgram(DDScreenWindowing ddsw, BaseData[] arr,Boolean isLong, Map<String,Object> map) {


		String whatToCallName = ddsw.program1.trim();//IJTI-1420
		Class whatToCallClass = null;

			try {
				whatToCallClass = (Class) this.getProgram(whatToCallName);
				Method m = whatToCallClass.getMethod("f4", new Class[] {
						COBOLAppVars.class, DDScreenWindowing.class, BaseData[].class, Boolean.class,Map.class});
				return m.invoke(null, this, ddsw, arr, isLong, map );
		} catch (Exception e) {
			LOGGER.error("callF4InProgram(DDScreenWindowing , BaseData[] , Boolean , Map<String,Object>)", e);
		}

		return null;
	}

	private BaseData getField(SmartVarModel vm, String s,FlddTableDAM flddIO){
		if (s == null){
			return null;
		}
		flddIO.setParams(SPACES);
		flddIO.setFunction("READR");
		flddIO.setFormat("FLDDREC");
		flddIO.setFieldId(s.toUpperCase());
		SmartFileCode.execute(this, flddIO);
		if (isEQ(flddIO.getStatuz(),"****")) {
			String Name = flddIO.getNameForCobol().trim().toLowerCase();
			return vm.getField(Name, BaseData.class);
		}
		return null;
	}
}
