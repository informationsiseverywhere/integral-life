package com.csc.lifeasia.runtime.batch;

import java.net.Socket;


import com.csc.batch.socket.BatchProcessThread;

public class LifeAsiaBatchProcessThread extends BatchProcessThread {
	
	public LifeAsiaBatchProcessThread(Socket socket) {
		super(socket);
		batchController = LifeAsiaBatchController.getInstance();
	}
	
}
