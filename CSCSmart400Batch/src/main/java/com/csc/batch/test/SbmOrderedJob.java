/**
 * 
 */
package com.csc.batch.test;

import java.util.Date;

import com.csc.batch.quartz.BatchJobManagerFactory;
import com.csc.batch.quartz.BatchJobQueueManager;

/**
 * Test the order of jobs running in the job queue. Submit a job into "night" job queue, and then submit a job into the
 * same queue. The previous job will execute some time, when it finishes, the next job will start to run. test success
 * 2007.11.15
 * 
 * @author daniel.peng
 * @version 1.0
 */
public class SbmOrderedJob {

	public static void main(String[] args) {
		BatchJobQueueManager.getInstance().start();

		BatchJobManagerFactory.getInstance().getQuartzInstance().submitJob("com.csc.batch.test.dummyjob.Job1",
		    new String[] { "F9AUTOALOC", "1", "1", "1" }, "test_job_1", null, "NIGHT", "5", null, null, null, null,
		    null, new Date(), null);

		BatchJobManagerFactory.getInstance().getQuartzInstance().submitJob("com.csc.batch.test.dummyjob.Job2",
		    new String[] { "F9AUTOALOC", "1", "1", "1" }, "test_job_2", null, "NIGHT", "5", null, null, null, null,
		    null, new Date(), null);
	}
}
