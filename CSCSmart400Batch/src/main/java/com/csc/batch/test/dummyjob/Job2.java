/**
 * 
 */
package com.csc.batch.test.dummyjob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;

/**
 * @author daniel.peng
 */
public class Job2 extends COBOLConvCodeModel {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(Job2.class);

	public void mainline(Object[] args) {
		LOGGER.error("job2 done.");
		
	}

}
