package com.csc.batch.test.dummyjob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;

public class Job1 extends COBOLConvCodeModel {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(Job1.class);
	public void mainline(Object[] args) {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
		}
		LOGGER.error("job1 done.");
	}
}
