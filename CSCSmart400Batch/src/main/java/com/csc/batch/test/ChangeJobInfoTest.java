/**
 * 
 */
package com.csc.batch.test;

import java.util.Date;

import com.csc.batch.quartz.BatchJobManagerFactory;
import com.csc.batch.quartz.BatchJobQueueManager;

/**
 * @author dianel.peng
 */
public class ChangeJobInfoTest {

	public static void main(String[] args) {
		BatchJobQueueManager.getInstance().start();

		BatchJobManagerFactory.getInstance().getQuartzInstance().submitJob(
		    "com.csc.batch.test.dummyjob.ChangeJobInfoTest_Job1", new String[] { "F9AUTOALOC", "1", "1", "1" },
		    "test_job_1", null, "NIGHT", "5", null, null, null, null, null, new Date(), null);
	}

}
