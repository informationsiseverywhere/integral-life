package com.csc.batch.quartz;

import static org.quartz.JobBuilder.newJob;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Date;
import java.util.Map;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;

import com.csc.smart400framework.SMARTAppVars;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * @author daniel.peng
 */
public class BatchJobManagerQuartz implements BatchJobManager {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobManagerQuartz.class);

	public static final String JOB_CMD = "CMD";

	public static final String JOB_CURLIB = "CURLIB";

	public static final String JOB_INLLIBL = "INLLIBL";

	public static final String JOB_INFO = "RPGJOBINFO";


	/*
	 * (non-Javadoc)
	 * @see com.csc.batch.daniel.JobManagerBase#endJob(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endJob(String strJobName, String strJobNumber, String strJobQueue) {
		BatchJobQueue jobQueue = BatchJobQueueFactory.getInstance().getJobQueue(strJobQueue);
		if (jobQueue != null) {
			
			if (strJobName != null && strJobNumber != null && strJobQueue != null) {
				String scheduleID = strJobName + strJobNumber;
				try {
					jobQueue.getScheduler().getJobDetail( new JobKey(scheduleID, strJobQueue));
					jobQueue.getScheduler().interrupt(new JobKey(scheduleID, strJobQueue));
				} catch (UnableToInterruptJobException e) {
					throw new RuntimeException(e);
				} catch (SchedulerException e) {
					throw new RuntimeException("unknown job.");
				}
			} else {
				throw new RuntimeException("unknown job.");
			}
		} else {
			throw new RuntimeException("unknown job queue.");
		}
	}

	public void delayJob(String strJobName, String strJobNumber, String strJobQueue, Date date) {

	}

	public void holdJob(String jobName, String jobNumber, String jobQ) {
		// this method is no longer needed.
	}

	public void restartJob(String jobNumber, String user, String jobName) {

	}

	public void submitJob(String strJobCommand, Object[] aryJobParameters, String strJobName, String strJobDescription,
	        String strJobQ, String strJobPty, String rtgDta, String strLog, String logCLPgm, String hold,
	        String strUser, Date dEntryDate, String rqsDta) {
		String jobQueue = strJobQ;
		String jobDesc;
		if (strJobDescription == null) {
			jobDesc = JobInfo.DEFAULT_LIB + "/" + JobInfo.DEFAULT_JOBD;
		} else {
			jobDesc = strJobDescription;
		}
		String jobName = strJobName;
		String user = strUser;
		String cmd = strJobCommand;
		Object[] jobParams = aryJobParameters;
		String jobPty = strJobPty;
		Date entryDate = dEntryDate;

		String log = strLog;
		String logLevel = null;
		String logSeverity = null;
		String logType = null;

		// retrieve params from parameter array.
		String scheduleName = "";
		String scheduleNumber = "";
		String scheduleID = "";

		if (jobParams.length == 4) {
			scheduleName = jobParams[0].toString();
			FixedLengthStringData f = new FixedLengthStringData(6);
			PackedDecimalData t = new PackedDecimalData(8, 0);
			t.setInternal(new FixedLengthStringData(jobParams[1].toString()));
			
			 new ZonedDecimalData(6).isAPartOf(f, 0).set(t.getbigdata());
			scheduleNumber = f.toString();
			scheduleID = scheduleName + scheduleNumber;
		} else {
			//Modified by Ai Hao for bug#774
			/*
			scheduleNumber = jobParams[1].toString();
			scheduleName = jobName;
			scheduleID = jobParams[1].toString();
			*/
			scheduleNumber = jobParams[0].toString();
			scheduleName = jobName;
			scheduleID = jobParams[0].toString();
		}

		SMARTAppVars appVars = getAppVars();

		// construct a new job info.
		BatchJobInfo jobInfo = new BatchJobInfo(scheduleNumber, scheduleName, null, null, entryDate, user, null,
		    jobPty, null, null, logLevel, logSeverity, logType, BatchJobUtils.ENDSTS_WAITING, jobDesc, jobPty,
		    jobQueue, cmd, null, null, log, logCLPgm, null, hold, null, null,null);
		// set the job info into the appVars.
		appVars.setJobinfo(jobInfo);

		// construct a new BatchJob JobDetail instance
		// and put it into a job queue.
		JobDetail jobDetail = newJob(BatchJob.class)
		            .withIdentity(new JobKey(scheduleID, jobQueue))
		            .build();

		Map<String, Object> dataMap = jobDetail.getJobDataMap();
		dataMap.put(BatchJobUtils.JOB_CMD, cmd);
		dataMap.put(BatchJobUtils.JOB_PARAMS, jobParams);
		dataMap.put(BatchJobUtils.BATCH_JOB_INF, jobInfo);
		dataMap.put(BatchJobUtils.BATCH_APPVARS, appVars);
		dataMap.put(BatchJobUtils.JOB_NAME, strJobName);
		dataMap.put(BatchJobUtils.JOB_DESC, strJobDescription);
		dataMap.put(BatchJobUtils.JOB_Q, strJobQ);
		dataMap.put(BatchJobUtils.JOB_PTY, strJobPty);
		dataMap.put(BatchJobUtils.JOB_RTGDTA, rtgDta);
		dataMap.put(BatchJobUtils.JOB_LOG, strLog);
		dataMap.put(BatchJobUtils.JOB_CLPGM, logCLPgm);
		dataMap.put(BatchJobUtils.JOB_HOLD, hold);
		dataMap.put(BatchJobUtils.JOB_USER, user);
		dataMap.put(BatchJobUtils.JOB_ENTRYTIME, dEntryDate);
		dataMap.put(BatchJobUtils.JOB_RQSDTA, rqsDta);
		dataMap.put(BatchJobUtils.JOB_ID_SEQ, scheduleNumber);
		dataMap.put(BatchJobUtils.JOB_USER, strUser);

		BatchJobQueue batchJobQueue = null;
		LOGGER.debug("submitJob(String, Object[], String, String, String, String, String, String, String, String, String, Date, String) - jobQueue={}", jobQueue);//IJTI-1498
		batchJobQueue = BatchJobQueueFactory.getInstance().getJobQueue(jobQueue.trim());

		if (batchJobQueue != null) {
			batchJobQueue.addJob(jobDetail);
			LOGGER.debug("submitJob(String, Object[], String, String, String, String, String, String, String, String, String, Date, String) - Add  job {} in to the queue '{}'.",scheduleName,jobQueue);//IJTI-1498
		} else {
			throw new RuntimeException("Unknown job queue.");
		}
	}

	protected SMARTAppVars getAppVars() {
		return (SMARTAppVars) SMARTAppVars.getInstance();
	}

}
