package com.csc.batch.quartz;

import java.util.Date;

/**
 * Base class for Job Managers that emulates the RPG Job Commands
 * 
 * @author Sarah Kim
 * @version 1.0 Feb 2007 manually coded
 */
public interface BatchJobManager {

	/**
	 * The Submit Job methood allows a job being run to submit another job to a job queue to be run later as a batch
	 * job.
	 * 
	 * @param jobCommand
	 * @param jobName
	 * @param jobDescription
	 * @param jobPrty
	 * @param user
	 * @param outQueue
	 * @param jobQueue
	 * @param printText
	 * @param systemLibrary
	 * @param currentLibrary
	 * @param inLibraryList
	 * @param runDate
	 * @param runTime
	 * @param inquiryMessageReply
	 * @param copyEnvVar
	 */
	void submitJob(String jobCommand, Object[] jobParameters, String strJobName, String strJobDescription,
	        String strJobQ, String strJobPty, String rtgDta, String log, String logCLPgm, String hold, String user,
	        Date entryDate, String rqsDta);

	/**
	 * The end job method ends the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param strJobName
	 * @param strJobNumber
	 * @param strJobQueue
	 */
	void endJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The hold job method holds the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param strJobName
	 * @param strJobName
	 * @param strJobQueue
	 */
	void holdJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The restart job method restarts the specified job. The job must be on a job queue, currently holded.
	 * 
	 * @param strJobName
	 * @param strJobName
	 * @param strJobQueue
	 */
	void restartJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The delayJob method delays the specified job. The job must be on a job queue, currently running.
	 * 
	 * @param strJobName
	 * @param strJobNumber
	 * @param strJobQueue
	 * @param date
	 */
	void delayJob(String strJobName, String strJobNumber, String strJobQueue, Date date);

}
