package com.csc.batch;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Date;
import java.util.Map;

import org.quartz.JobDetail;

import com.csc.batch.quartz.BatchJobManagerFactory;
import com.csc.batch.quartz.BatchJobQueue;
import com.csc.batch.quartz.BatchJobQueueFactory;
import com.csc.batch.quartz.BatchJobUtils;
import com.csc.smart400framework.SMARTAppVars;
import com.quipoz.COBOLFramework.job.JobInfo;

/**
 * The Batch Mediator class provides batch functionalities to outer componnets.
 * 
 * @author daniel.peng
 */
public class BatchController implements BatchControl {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchController.class);

	public BatchController() {
		super();
	}

	protected static BatchController instance;

	public static BatchController getInstance() {
		if (instance == null) {
			instance = new BatchController();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.csc.batch.BatchControl#submitJob(java.lang.String, java.lang.String[], java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	public void submitJob(String jobCommand, Object[] aryJobParameters, String jobName, String jobDescription,
	        String jobQ, String jobPty, String rtgDta, String log, String logCLPgm, String hold, String user,
	        Date entryDate, String rqsDta) {
		BatchJobManagerFactory.getInstance().getQuartzInstance().submitJob(jobCommand, aryJobParameters, jobName,
		    jobDescription, jobQ, jobPty, rtgDta, log, logCLPgm, hold, user, entryDate, rqsDta);
	}

	/**
	 * The end job method ends the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param zJobNumber the number of the job.
	 * @param user the user of the job.
	 * @param fJobName the name of the job.
	 */
	public void endJob(String jobName, String jobNumber, String jobQueue) {
		BatchJobManagerFactory.getInstance().getQuartzInstance().endJob(jobName, jobNumber, jobQueue);
	}

	/**
	 * The hold job method holds the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param zJobNumber the number of the job.
	 * @param user the user of the job.
	 * @param fJobName the name of the job.
	 */
	public void holdJob(String jobName, String jobNumber, String jobQueue) {
		changeJobInfo(jobName, jobNumber, jobQueue, JobInfo.ENDSTS, BatchJobUtils.ENDSTS_PENDING);
		
		BatchJobQueue jobQ = BatchJobQueueFactory.getInstance().getJobQueue(jobQueue);
		if (jobQ != null) {
			JobDetail jobDetail = jobQ.getJob(jobName + jobNumber);
			if (jobDetail != null) {
				jobQ.addPausedJob(jobDetail);
			} else {
				getBatchAppVars().addExtMessage("CPF0000", "can not find job.");
			}
		} else {
			getBatchAppVars().addExtMessage("CPF0000", "Unknown obq.");
		}
	}

	public SMARTAppVars getBatchAppVars() {
		return (SMARTAppVars) SMARTAppVars.getInstance();
	}

	/**
	 * The restart job method restarts the specified job. The job must be on a job queue, currently holded.
	 * 
	 * @param jobName the number of the job.
	 * @param jobNumber the user of the job.
	 * @param jobName the name of the job.
	 */
	public void restartJob(String jobName, String jobNumber, String jobQueue) {
		
		BatchJobQueue jobQ = BatchJobQueueFactory.getInstance().getJobQueue(jobQueue);
		if (jobQ != null) {
			JobDetail jobDetail = jobQ.getJob(jobName + jobNumber);
			if (jobDetail != null) {
				Map dataMap = jobDetail.getJobDataMap();
				submitJob((String) dataMap.get(BatchJobUtils.JOB_CMD),
				    (String[]) dataMap.get(BatchJobUtils.JOB_PARAMS), (String) dataMap.get(BatchJobUtils.JOB_NAME),
				    (String) dataMap.get(BatchJobUtils.JOB_DESC), (String) dataMap.get(BatchJobUtils.JOB_Q),
				    (String) dataMap.get(BatchJobUtils.JOB_PTY), (String) dataMap.get(BatchJobUtils.JOB_RTGDTA),
				    (String) dataMap.get(BatchJobUtils.JOB_LOG), (String) dataMap.get(BatchJobUtils.JOB_CLPGM),
				    (String) dataMap.get(BatchJobUtils.JOB_HOLD), (String) dataMap.get(BatchJobUtils.JOB_USER),
				    new Date(), (String) dataMap.get(BatchJobUtils.JOB_RQSDTA));
			} else {
				getBatchAppVars().addExtMessage("CPF0000", "can not find job.");
			}
		} else {
			getBatchAppVars().addExtMessage("CPF0000", "Unknown obq.");
		}
	}

	/**
	 * The delayJob method delays the specified job. The job must be on a job queue, currently running.
	 * 
	 * @param jobNumber the number of the job.
	 * @param user the user of the job.
	 * @param jobName the name of the job.
	 */
	public void delayJob(String jobName, String jobNumber, String jobQueue, Date entryDate) {
	
		changeJobInfo(jobName, jobNumber, jobQueue, JobInfo.ENDSTS, BatchJobUtils.ENDSTS_CANCELLED);
		BatchJobQueue jobQ = BatchJobQueueFactory.getInstance().getJobQueue(jobQueue);
		if (jobQ != null) {
			JobDetail jobDetail = jobQ.getJob(jobName + jobNumber);
			if (jobDetail != null) {
				Map dataMap = jobDetail.getJobDataMap();
				submitJob((String) dataMap.get(BatchJobUtils.JOB_CMD),
				    (String[]) dataMap.get(BatchJobUtils.JOB_PARAMS), (String) dataMap.get(BatchJobUtils.JOB_NAME),
				    (String) dataMap.get(BatchJobUtils.JOB_DESC), (String) dataMap.get(BatchJobUtils.JOB_Q),
				    (String) dataMap.get(BatchJobUtils.JOB_PTY), (String) dataMap.get(BatchJobUtils.JOB_RTGDTA),
				    (String) dataMap.get(BatchJobUtils.JOB_LOG), (String) dataMap.get(BatchJobUtils.JOB_CLPGM),
				    (String) dataMap.get(BatchJobUtils.JOB_HOLD), (String) dataMap.get(BatchJobUtils.JOB_USER),
				    entryDate, (String) dataMap.get(BatchJobUtils.JOB_RQSDTA));
			} else {
				getBatchAppVars().addExtMessage("CPF0000", "can not find job.");
			}
		} else {
			getBatchAppVars().addExtMessage("CPF0000", "Unknown obq.");
		}
	}

	/**
	 * Hold the specified job queue.
	 * 
	 * @param strJobQueueName the name of the job queue.
	 */
	public void holdJobQueue(String strJobQueueName) {
		BatchJobQueueFactory.getInstance().holdJobQueue(strJobQueueName);
	}

	/**
	 * Release the specified job queue.
	 * 
	 * @param strJobQueueName the name of the job queue.
	 */
	public void releaseJobQueue(String strJobQueueName) {
		BatchJobQueueFactory.getInstance().releaseJobQueue(strJobQueueName);
	}

	/*
	 * (non-Javadoc)
	 * @see com.csc.batch.BatchControl#changeJobInfo(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	public void changeJobInfo(String jobName, String jobNumber, String jobQueue, String attr, String value) {
	
		BatchJobQueue jobQ = BatchJobQueueFactory.getInstance().getJobQueue(jobQueue);
		if (jobQ != null) {
			JobDetail jobDetail = jobQ.getJob(jobName + jobNumber);
			if (jobDetail != null) {
				SMARTAppVars appVars = (SMARTAppVars) jobDetail.getJobDataMap().get(BatchJobUtils.BATCH_APPVARS);
				appVars.getJobInfo().changeJobInfo(attr, value);
			} else {
				getBatchAppVars().addExtMessage("CPF0000", "can not find job.");
			}
		} else {
			getBatchAppVars().addExtMessage("CPF0000", "Unknown obq.");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.csc.batch.BatchControl#changeJobInfo (java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String[])
	 */
	public void changeJobInfo(String jobName, String jobNumber, String jobQueue, String[] valuePairs) {
		
		// BatchJobQueue jobQ = BatchJobQueueFactory.getInstance().getJobQueue(
		// jobQueue);
		// if (jobQ != null) {
		// SMARTAppVars appVars = (SMARTAppVars) jobQ.getJob(
		// jobName + jobNumber).getJobDataMap().get(
		// BatchJobUtils.BATCH_APPVARS);
		// appVars.getJobInfo().changeJobInfo(valuePairs);
		// } else {
		// tmpAppVars.addExtMessage("CPF0000", "unknown jobq.");
		// }
		LOGGER.debug("changeJobInfo(String, String, String, String[]) - {}", getBatchAppVars().getJobInfo().JOBQ);//IJTI-1498
		getBatchAppVars().getJobInfo().changeJobInfo(valuePairs);
	}

}
