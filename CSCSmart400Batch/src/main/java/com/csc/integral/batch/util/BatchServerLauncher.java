package com.csc.integral.batch.util;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.csc.lifeasia.runtime.variables.LifeAsiaBatchAppVars;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.ThreadLocalStore;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

public class BatchServerLauncher {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatchServerLauncher.class);

	public static LifeAsiaBatchAppVars appVars = null;
	
	private static final String[] APPLICATION_CONTEXT = new String[] {
			"classpath*:mandatory/common.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-common.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-jobs.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-listeners.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-schedulers.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-triggers.xml"};

	private static String getFOPPrintLng(AppVars appVars) {
		Connection con = null;
		ResultSet rs = null;
		try {
			con = appVars.getTempDBConnection("DB");
			rs = con.createStatement()
					.executeQuery(
							"select dataarea_data from printlng where dataarea_id='PRINTLNG'");
			rs.next();
			return rs.getString(1);
		} catch (Exception e) {
			throw new RuntimeException(
					"Error when querying printlng from database.", e);
		} finally {
			try {
				appVars.freeDBConnection(con);
			} catch (Exception e) {
				LOGGER.error("Error when closing temp connection.", e);
						
			}
		}
	}

	public static void main(String[] args) {
		// Verifies prerequisites.
		if (ArrayUtils.getLength(args) < 1) {
			throw new IllegalArgumentException(
					"QuipozCfg.xml is expected.");
		}

		/*IJTI-932 START*/
		String validFileName="QuipozCfg.xml";
		if(args[0].contains(validFileName)) {
			System.setProperty("Quipoz.LifeAsiaWeb.XMLPath", args[0]);//IJTI-1457
		} else {
			LOGGER.error("Configuration file is not valid");
			return;
		}
		/*IJTI-932 END*/
		
		System.setProperty("org.quartz.dataSource.myDS.URL", IntegralDBProperties.getUrl());//IJTI-449
		System.setProperty("org.quartz.dataSource.myDS.user", IntegralDBProperties.getUser());//IJTI-449
		System.setProperty("org.quartz.dataSource.myDS.driver", IntegralDBProperties.getDriver());//IJTI-449
		System.setProperty("org.quartz.dataSource.myDS.password", IntegralDBProperties.getPassword());//IJTI-449
		
		ApplicationContext appCtx = new ClassPathXmlApplicationContext(
				new String[] { "classpath*:database.xml", "classpath*:com/csc/lifeasia/context/LifeContext.xml",
						"classpath:com/csc/lifeasia/context/dao/Life-DAO-" + IntegralDBProperties.getType()
								+ ".xml" });//IJTI-449
		//IJTI-401 ends

        if ("true".equals(System.getProperty("aspect4pc"))) {
            URL url = BatchServerLauncher.class.getClassLoader().getResource(
                "com/csc/integral/aop/logback-aop.xml");
            LOGGER.info("Logger run in mode aspect4pc with log configuration file: [{}]", url);
            // assume SLF4J is bound to logback in the current environment
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
            if(url.getPath()==null){
            	LOGGER.error("Wrong Url Path");
            }else{
            try {
                JoranConfigurator configurator = new JoranConfigurator();
                configurator.setContext(context);
                // Call context.reset() to clear any previous configuration, e.g. default
                // configuration. For multi-step configuration, omit calling context.reset().
                context.reset();
                configurator.doConfigure(url);
            } catch (JoranException je) {
                // StatusPrinter will handle this
                LOGGER.error("Error while reconfiguration log config file", je);
            }
            StatusPrinter.printInCaseOfErrorsOrWarnings(context);
            }
        }
        
		appVars = new LifeAsiaBatchAppVars("LifeAsiaWeb");//IJTI-1457

		AppConfig.setPrintlng(getFOPPrintLng(appVars));
		if (StringUtils.isEmpty(AppConfig.getPrintlng())) {
			throw new RuntimeException("AppConfig.printlng must NOT be empty.");
		}
		
		//ILIFE-4035 STARTS
		List<String> appContextLocations = new ArrayList<>();
		for(String contextLocation : APPLICATION_CONTEXT){
			appContextLocations.add(contextLocation);
		}
		
		if(args.length > 1){
			for(int i=1; i<args.length; i++){
				appContextLocations.add(args[i]);
			}
		}
		
		String [] appContext = new String[appContextLocations.size()];
		appContextLocations.toArray(appContext);
		
		//IJTI-401 starts
		appCtx = new ClassPathXmlApplicationContext(appContext, appCtx);
		//IJTI-401 ends
		//ILIFE-4035 ENDS

		LOGGER.info("ApplicationUse11={},Printlng={}", appVars.getApplicationUse11(), 
				AppConfig.getPrintlng());//IJTI-1498
		ThreadLocalStore.clear();
	}
}