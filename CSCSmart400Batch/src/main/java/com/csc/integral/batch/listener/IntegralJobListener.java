package com.csc.integral.batch.listener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegralJobListener implements JobListener {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegralJobListener.class);

	@Override
	public String getName() {
		return "IntegralJobListener";
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext ctx) {
		// Does nothing.

	}

	@Override
	public void jobToBeExecuted(JobExecutionContext ctx) {
		// Does nothing.

	}

	@Override
	public void jobWasExecuted(JobExecutionContext ctx, JobExecutionException ex) {
		// Does nothing.
	}

}
