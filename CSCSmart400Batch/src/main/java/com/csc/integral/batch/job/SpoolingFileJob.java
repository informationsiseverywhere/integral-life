package com.csc.integral.batch.job;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.csc.fsu.accounting.dataaccess.model.Constants;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ItempfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.integral.batch.util.BatchServerLauncher;
import com.csc.lifeasia.runtime.variables.LifeAsiaBatchAppVars;
import com.csc.smart.tablestructures.T1716rec;
import com.csc.xmltopdf.config.XMLToPDFConfig;
import com.csc.xmltopdf.pdfoutput.PDFGenerator;
import com.csc.xmltopdf.xmlconvert.XMLConverter;
import com.quipoz.COBOLFramework.common.Utils;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.ThreadLocalStore;

public class SpoolingFileJob extends IntegralJob {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpoolingFileJob.class);

	private PDFGenerator pdfGenerator;

	private XMLConverter xmlConverter;

	private String inputPath;

	private String convertedPath;

	private String templatePath;

	private String outputFile;

	private String tempfile;

	private AppConfig appConfig;
	//Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA
	private ItempfDAO itemDAO ;
	private Boolean isAiaAusDirectDebit ;
	////Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA ends

	@Override
	protected void executeImpl(JobExecutionContext ctx) throws Exception {
	    
	    	LifeAsiaBatchAppVars appVars = BatchServerLauncher.appVars;
		AppVars.setInstance(appVars);
	
	    	if(itemDAO == null) {
	    	    itemDAO = new ItempfDAOImpl();
	    	}
	    	if(isAiaAusDirectDebit == null) {
	    	    List<Itempf> items = itemDAO.getItempf("IT", "2", "TR2A5", "BTPRO029");
	    	    isAiaAusDirectDebit = !CollectionUtils.isEmpty(items);
	    	}
	    	//Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA ends

		

		pdfGenerator = (PDFGenerator) getApplicationContext().getBean("pdfGenerator");
		xmlConverter = (XMLConverter) getApplicationContext().getBean("xmlConverter");

		XMLToPDFConfig xmlToPDFConfig = new XMLToPDFConfig();
		inputPath = xmlToPDFConfig.getXmlSource();
		convertedPath = xmlToPDFConfig.getXmlACon();
		templatePath = xmlToPDFConfig.getPdfTemplate();
		outputFile = xmlToPDFConfig.getPdfOutput();
		tempfile = xmlToPDFConfig.getXmlBackup();

		appConfig = AppVars.getInstance().getAppConfig();
		File dataFile = FileSystems.getDefault().getPath(inputPath).toFile();//IJTI-729
		File[] tempFile = dataFile.listFiles();
		if (tempFile != null && tempFile.length > 0) {
			readFolderByFile(tempFile);
		}
	}

	private void readFolderByFile(File[] t) {

		for (int i = 0; i < t.length; i++) {

			File[] files = null;
			File oldfile = t[i];
			File newfile = FileSystems.getDefault().getPath(convertedPath + File.separator + t[i].getName()).toFile();//IJTI-729
			File temp = FileSystems.getDefault().getPath(tempfile + File.separator + t[i].getName()).toFile();//IJTI-729

			String fileName = t[i].getName();
			String extendsName = null;

			String user = fileName.substring(0, fileName.indexOf("-"));
			String outputUserFile = null;
			outputUserFile = outputFile.trim() + File.separator + user.trim();
			try {
				if (!(FileSystems.getDefault().getPath(outputUserFile).toFile().isDirectory())) {//IJTI-729
					FileSystems.getDefault().getPath(outputUserFile).toFile().mkdir();//IJTI-729
				}

			} catch (SecurityException e) {
				LOGGER.warn("can not make directory", e);
			}

			int j = fileName.lastIndexOf('.');

			if ((j > -1) && (j < (fileName.length()))) {
				extendsName = fileName.substring(j, fileName.length());
				fileName = fileName.substring(0, j);
			}
			fileName = fileName.substring(fileName.indexOf("-") + 1);
			String tempName = fileName.substring(0, fileName.lastIndexOf("_"));
			tempName = tempName.substring(tempName.lastIndexOf("_") + 1);
			String templateFilePath = templatePath.trim() + File.separator + tempName.trim() + ".PDF";

			File template = FileSystems.getDefault().getPath(templateFilePath).toFile();//IJTI-729
			if (extendsName.equals(".XML")) {
				xmlConverter.convert(oldfile.toString(), newfile.toString());
				if (newfile.exists()) {
					// Split XML here
					String outPutfilePath = null;
					boolean success = false;
					try {
						if (template.exists()) {
							if (Utils.isLargePDF(tempName.trim())) {
								int pagePerFile = Integer.parseInt(appConfig.getPagesPerFile());
								files = xmlConverter.splits(newfile, pagePerFile, "PAGEBREAK", convertedPath);
								for (File file : files) {
									fileName = file.getName();
									fileName = fileName.substring(fileName.indexOf("-") + 1);
									fileName = fileName.split("\\.")[0];
									//Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA
									outPutfilePath = getFilePath(fileName, outputUserFile);
									File outFile = FileSystems.getDefault().getPath(outPutfilePath).toFile();
									if(!outFile.getParentFile().exists()) {
									    outFile.getParentFile().mkdirs();
									}
									//Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA ends
									pdfGenerator
									.print(templateFilePath, file.toString(), outPutfilePath);
									
								}
							} else {
							  //Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA
							    outPutfilePath = getFilePath(fileName, outputUserFile);
							    File outFile = FileSystems.getDefault().getPath(outPutfilePath).toFile();
								if(!outFile.getParentFile().exists()) {
								    outFile.getParentFile().mkdirs();
								}
							  //Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA ends
							pdfGenerator.print(templateFilePath, newfile.toString(), outPutfilePath);

							}
						} else {
							LOGGER.info("The template for  \"{}\"  isn't exit", fileName);//IJTI-1498
							return;
						}
					} catch (Exception e) {
						LOGGER.error("Print file" + fileName + "unsuccessful!", e);
					} finally {
						oldfile.renameTo(temp);
						oldfile.delete();
						if ( files!= null && files.length > 1 ) {
							System.gc();
							for (File file : files) {
								success = file.delete();
								if (success == false) {
									LOGGER.info("Cannot delete file!!!!! {}", file.getAbsolutePath());//IJTI-1498
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * //Ticket #PINNACLE-1108:Extract File Changes after discussion with AIA
	 * @param fileName
	 * @param outputUserFile
	 * @return
	 */
	private String getFilePath(String fileName, String outputUserFile) {
	    if(Boolean.TRUE.equals(isAiaAusDirectDebit)) {
		String scheduleName = fileName.substring(fileName.indexOf("-")+1,fileName.indexOf("_"));
		scheduleName = scheduleName.substring(0, Math.min(scheduleName.length(), 4));
		String[] tmp = fileName.split("_");
		String key = tmp[2];
		if(key.length()>4) {
		    key = key.substring(key.length() - 4);
		}
		List<Itempf> itemList = itemDAO.findByMenuTable("T1716", scheduleName+key);
		if(!CollectionUtils.isEmpty(itemList)) {
		    T1716rec t1716Rec = new T1716rec();
		    t1716Rec.t1716Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		    if(Constants.NEWFILENAMEFORBATCHES.contains(scheduleName)){
		    	fileName = Constants.MWL + fileName;
		    }
		    return t1716Rec.ifspath04.toString().trim()+File.separator
			    + fileName.trim() + ".PDF";
		}
	    }
	    return outputUserFile.trim() + File.separator
		    + fileName.trim() + ".PDF";
	}
}
