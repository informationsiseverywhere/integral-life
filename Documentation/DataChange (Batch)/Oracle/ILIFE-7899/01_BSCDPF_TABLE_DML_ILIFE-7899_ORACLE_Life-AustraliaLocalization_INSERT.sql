DECLARE 
  cnt number :=0;
BEGIN 
SELECT COUNT(*) into cnt FROM VM1DTA.BSCDPF WHERE BSCHEDNAM='L2COCLISC ' AND BAUTHCODE='BAPH';
	IF ( cnt = 0 ) THEN
		insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,USRPRF,JOBNM,DATIME,DROPTEMP) values ('L2COCLISC ',1,'N','N','BAPH','QBATCH    ','5','PAXUS     ','L','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP,'N');
	END IF;
END;
/
COMMIT;