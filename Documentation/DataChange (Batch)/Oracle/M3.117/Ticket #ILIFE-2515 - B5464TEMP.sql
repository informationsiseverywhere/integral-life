-- Create table
create table B5464TEMP
(
  chdrcoy    CHAR(1),
  chdrnum    CHAR(8),
  life       CHAR(2),
  coverage   CHAR(2),
  rider      CHAR(2),
  plnsfx     NUMBER(4),
  seqno      NUMBER(2),
  tranno     NUMBER(5),
  rasnum     CHAR(8),
  rngmnt     CHAR(4),
  validflag  CHAR(1),
  currcode   CHAR(3),
  currfrom   NUMBER(8),
  currto     NUMBER(8),
  retype     CHAR(1),
  raamount   NUMBER(17,2),
  ctdate     NUMBER(8),
  cmdate     NUMBER(8),
  reasper    NUMBER(5,2),
  rrevdt     NUMBER(8),
  recovamt   NUMBER(17,2),
  cestype    CHAR(1),
  lrkcls     CHAR(4),
  chtranno   NUMBER(5),
  chcnttype  CHAR(3),
  chstatcode CHAR(2),
  chpstcde   CHAR(2),
  chbillfreq CHAR(2),
  chcntcurr  CHAR(3),
  chpolsum   NUMBER(4),
  chbillchnl CHAR(2),
  choccdate  NUMBER(8),
  chpolinc   NUMBER(4),
  chptdate   NUMBER(8),
  cocrrcd    NUMBER(8),
  cocrtable  CHAR(4),
  cosumins   NUMBER(17,2),
  cojlife    CHAR(2),
  coinstprem NUMBER(17,2),
  cochdrcoy  CHAR(1),
  cochdrnum  CHAR(8),
  colife     CHAR(2),
  cocoverage CHAR(2),
  corider    CHAR(2),
  coplnsfx   NUMBER(4),
  jlife      CHAR(2),
  lifcnum    CHAR(8),
  jlife2     CHAR(2),
  lifcnum2   CHAR(8),
  rauqnum    NUMBER(18)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
