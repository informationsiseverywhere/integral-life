DROP TABLE B5018TEMP;

CREATE TABLE  B5018TEMP (
   UNIQUE_NUMBER   numeric (17, 0) ,
	 chdrcoy  nchar (1) NULL,
	 chdrnum   nchar (8) NULL,
	 life  nchar(2) NULL,
	 coverage  nchar(2) NULL,
	 rider  nchar(2) NULL,
	 plnsfx int NULL,
	pstatcode nchar(4) NULL,
	statcode nchar(2) NULL,
	crrcd int NULL,
	cbunst int NULL,
	rcesdte int NULL,
	sumins numeric(17, 2) NULL,
	validflag nchar(1) NULL,
	currfrom int NULL,
	currto int NULL,
	  crtable   nchar(4) NULL,
	  cntcurr   nchar(3) NULL,
	  cnttype  nchar(4) NULL,
	  tranno   nchar(4) NULL,
	  cowncoy  nchar(4) NULL,
	  cownnum   nchar(8) NULL,
	  jobnm    nchar(10) NULL,
	  usrprf    nchar(10) NULL,
	  cntbranch nchar(2) NULL,
    	  BATCHID INT
) 

   
