-- Create table
create table B5023TEMP
(
  cochdrcoy   CHAR(1 CHAR),
  cochdrnum   CHAR(8 CHAR),
  colife      CHAR(2 CHAR),
  cocoverage  CHAR(2 CHAR),
  corider     CHAR(2 CHAR),
  coplnsfx    NUMBER(4),
  cocrtable   CHAR(4 CHAR),
  copstatcode CHAR(2 CHAR),
  costatcode  CHAR(2 CHAR),
  cocrrcd     NUMBER(8),
  cocbunst    NUMBER(8),
  corcesdte   NUMBER(8),
  cosumins    NUMBER(17,2),
  covalidflag CHAR(1 CHAR),
  cocurrfrom  NUMBER(8),
  cocurrto    NUMBER(8)
);

