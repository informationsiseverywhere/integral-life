 CREATE OR REPLACE VIEW  VM1DTA.ADOC_ACMVREC  (   
	UNIQUE_NUMBER
	,RDOCPFX
	,RDOCCOY
	,RDOCNUM
	,JRNSEQ
	,TRANNO
	,RECORDFORMAT
	,NONKEYS
)
AS
 
SELECT acmvpf.unique_number,
       acmvpf.rdocpfx,
       acmvpf.rdoccoy,
       acmvpf.rdocnum,
       acmvpf.jrnseq,
       acmvpf.tranno,
       'ACMVREC'                       AS RECORDFORMAT,
        SUBSTR(NVL(acmvpf.rdocpfx, ' ') || RPAD(' ', 2)  ,1, 2)
       || SUBSTR(NVL(acmvpf.rdoccoy, ' ') || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.rdocnum, ' ') || RPAD(' ', 9) ,1, 9)
       || SUBSTR(NVL(Cast(acmvpf.jrnseq AS VARCHAR2(1000)), ' ') || RPAD(' ', 4) ,1, 4)
       || SUBSTR(NVL(acmvpf.rldgpfx, ' ') || RPAD(' ', 2) ,1, 2)
       || SUBSTR(NVL(acmvpf.rldgcoy, ' ') || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.rldgacct, ' ') || RPAD(' ', 16) ,1, 16)
       || SUBSTR(NVL(acmvpf.batccoy, ' ') || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.batcbrn, ' ') || RPAD(' ', 2),1, 2)
       || SUBSTR(NVL(Cast(acmvpf.batcactyr AS VARCHAR2(1000)), ' ') || RPAD(' ', 5) ,1, 5)
       || SUBSTR(NVL(Cast(acmvpf.batcactmn AS VARCHAR2(1000)), ' ') || RPAD(' ', 3) ,1, 3)
       || SUBSTR(NVL(acmvpf.batctrcde, ' ') || RPAD(' ', 4) ,1, 4)
       || SUBSTR(NVL(acmvpf.batcbatch, ' ') || RPAD(' ', 5) ,1, 5)
       || SUBSTR(NVL(acmvpf.genlcoy, ' ')  || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.genlcur, ' ') || RPAD(' ', 3)  ,1, 3)
       || SUBSTR(NVL(acmvpf.glcode, ' ')  || RPAD(' ', 14) ,1, 14)
       || SUBSTR(NVL(acmvpf.sacscode, ' ') || RPAD(' ', 2),   1, 2)
       || SUBSTR(NVL(acmvpf.sacstyp, ' ') || RPAD(' ', 2) ,1, 2)
       || SUBSTR(NVL(Cast(acmvpf.origamt AS VARCHAR2(1000)), ' ') || RPAD(' ', 19) ,1, 19)
       || SUBSTR(NVL(acmvpf.origcurr, ' ') || RPAD(' ', 3) ,1, 3)
       || SUBSTR(NVL(Cast(acmvpf.acctamt AS VARCHAR2(1000)), ' ') || RPAD(' ', 19) ,1, 19)
       || SUBSTR(NVL(Cast(acmvpf.crate AS VARCHAR2(1000)), ' ') || RPAD(' ', 20) ,1, 20)
       || SUBSTR(NVL(Cast(acmvpf.tranno AS VARCHAR2(1000)), ' ') || RPAD(' ', 6) ,1, 6)
       || SUBSTR(NVL(acmvpf.trandesc, ' ') || RPAD(' ', 30)  ,1, 30)
       || SUBSTR(NVL(acmvpf.glsign, ' ') || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.postyear, ' ') || RPAD(' ', 4) ,1, 4)
       || SUBSTR(NVL(acmvpf.postmonth, ' ') || RPAD(' ', 2) ,1, 2)
       || SUBSTR(NVL(Cast(acmvpf.effdate AS VARCHAR2(1000)) || RPAD(' ', 9) , ' ')  ,1, 9)
       || SUBSTR(NVL(Cast(acmvpf.trdt AS VARCHAR2(1000)) || RPAD(' ', 7) , ' ')  ,1, 7)
       || SUBSTR(NVL(Cast(acmvpf.trtm AS VARCHAR2(1000)) || RPAD(' ', 7) , ' ')  , 1,7)
       || SUBSTR(NVL(Cast(acmvpf.user_t AS VARCHAR2(1000)) || RPAD(' ', 7) , ' ')  ,1, 7)
       || SUBSTR(NVL(acmvpf.termid, ' ') || RPAD(' ', 4)  ,1, 4)
       || SUBSTR(NVL(acmvpf.tranref, ' ') || RPAD(' ', 30) , 1,30)
       || SUBSTR(NVL(Cast(acmvpf. frcdate AS VARCHAR2(1000)), ' ') || RPAD(' ', 9) ,1, 9)
       || SUBSTR(NVL(acmvpf.intextind, ' ') || RPAD(' ', 1)  ,1, 1)
       || SUBSTR(NVL(Cast(acmvpf.rcamt AS VARCHAR2(1000)), ' ') || RPAD(' ', 19)  ,1, 19)
       || SUBSTR(NVL(acmvpf.reconsbr, ' ') || RPAD(' ', 10) ,1, 10)
       || SUBSTR(NVL(Cast(acmvpf.reconref AS VARCHAR2(1000)), ' ') || RPAD(' ', 10) ,1, 10)
       || SUBSTR(NVL(acmvpf.suprflg, ' ') || RPAD(' ', 1) ,1, 1)
       || SUBSTR(NVL(acmvpf.taxcode, ' ') || RPAD(' ', 2) , 1,2)
       || SUBSTR(NVL(acmvpf.stmtsort, ' ') || RPAD(' ', 9) , 1,20)
       || SUBSTR(NVL(Cast(acmvpf.creddte AS VARCHAR2(1000)), ' ') || RPAD(' ', 20) ,1, 9)
       || SUBSTR(NVL(acmvpf.usrprf, ' ') || RPAD(' ', 10) , 1,10)
       || SUBSTR(NVL(acmvpf.jobnm, ' ') || RPAD(' ', 10),  1,10) 
          AS NONKEYS
        
FROM   vm1dta.acmvpf;  