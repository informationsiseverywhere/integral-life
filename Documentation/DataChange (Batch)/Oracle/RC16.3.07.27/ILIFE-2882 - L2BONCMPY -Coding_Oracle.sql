-- Create table
create table B5018TEMP
(
  chdrcoy   CHAR(1 CHAR),
  chdrnum   CHAR(8 CHAR),
  life      CHAR(2 CHAR),
  coverage  CHAR(2 CHAR),
  rider     CHAR(2 CHAR),
  plnsfx    NUMBER(4),
  pstatcode CHAR(4 CHAR),
  statcode  CHAR(2 CHAR),
  crrcd     NUMBER(8),
  cbunst    NUMBER(8),
  rcesdte   NUMBER(8),
  sumins    NUMBER(17,2),
  validflag CHAR(1 CHAR),
  currfrom  NUMBER(8),
  currto    NUMBER(8),
  crtable   CHAR(4 CHAR),
  cntcurr   CHAR(3 CHAR),
  cnttype   CHAR(4 CHAR),
  tranno    CHAR(4 CHAR),
  cowncoy   CHAR(4 CHAR),
  cownnum   CHAR(8 CHAR),
  jobnm     CHAR(10 CHAR),
  usrprf    CHAR(10 CHAR),
  cntbranch CHAR(2 CHAR)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
