IF OBJECT_ID('BR646TEMP') IS NOT NULL 
BEGIN
   DROP TABLE [VM1DTA].[BR646TEMP]
END 


CREATE TABLE [VM1DTA].[BR646TEMP](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[PAIDBY] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[DTETRM] [int] NULL,
	[DESC_T] [nchar](50) NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[CHDRUNIQUE] [bigint] NOT NULL,
	[TRANNO] [int] NULL,
	[CHDRPFX] [nchar](2) NULL,
	[CNTCURR] [nchar](3) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[TOTPYMT] [numeric](38, 2) NULL,
	[MEDIUNIQUENO] [bigint] NOT NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[PAYMTH] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLTTYPE] [nchar](1) NULL,
	[LANGUAGE] [nchar](1) NULL,
	[SURNAME] [nchar](30) NULL,
	[INITIALS] [nchar](2) NULL,
	[SALUTL] [nchar](8) NULL,
	[LSURNAME] [nchar](60) NULL,
	[LGIVNAME] [nchar](60) NULL
) ON [PRIMARY]
GO 