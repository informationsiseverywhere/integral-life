IF OBJECT_ID('BR644TEMP') IS NOT NULL 
BEGIN
   DROP TABLE [VM1DTA].[BR644TEMP]
END 


CREATE TABLE [VM1DTA].[BR644TEMP](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[PAIDBY] [nchar](1) NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[CLNTNUM] [nchar](10) NULL,
	[DATE1] [int] NULL,
	[DATE2] [int] NULL,
	[CLTSTAT] [nvarchar](2) NULL
) ON [PRIMARY]
GO 