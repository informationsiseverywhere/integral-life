IF OBJECT_ID('BR64XTEMP') IS NOT NULL 
BEGIN
   DROP TABLE [VM1DTA].[BR64XTEMP]
END 

CREATE TABLE [VM1DTA].[BR64XTEMP](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[PAIDBY] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[DTETRM] [int] NULL,
	[DESC_T] [nchar](50) NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[CHDRTRANNO] [int] NULL,
	[CHDRUNIQUENO] [bigint] NOT NULL,
	[CHDRPFX] [nchar](2) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[OCCDATE] [int] NULL,
	[CNTCURR] [nchar](3) NULL,
	[PTDATE] [int] NULL,
	[MEDIUNIQUE] [bigint] NOT NULL
) ON [PRIMARY]
GO 