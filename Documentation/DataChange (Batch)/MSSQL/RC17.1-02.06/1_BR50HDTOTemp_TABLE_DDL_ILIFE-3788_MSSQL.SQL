
DROP TABLE VM1DTA.BR50HDTOTemp;


create table  VM1DTA.BR50HDTOTemp
(
CHDRUNIQUENUMBER numeric(18),
CHDRCOY char(1),
CHDRNUM char(8),
CNTCURR char(3),
STATCODE char(2),
PSTCDE char(2),
CNTTYPE char(3),
COVTFLAG char(2),
HPADUNIQUENUMBER numeric(18),
HPRRCVDT numeric(8),
HPROPDTE numeric(8),
HUWDCDTE numeric(8),
BILLFREQ char(2),
BTDATE numeric(8),
INCSEQNO numeric(2),
PAYRPAYRSEQNO numeric(1),
CLNTCOY char(1),
CLNTNUM char(8),
BATCHID numeric(18));