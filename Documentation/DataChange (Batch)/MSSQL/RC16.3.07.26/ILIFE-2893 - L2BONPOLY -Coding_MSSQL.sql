CREATE TABLE [B5023TEMP](
	[COCHDRCOY] [nchar](1) NULL,
	[COCHDRNUM] [nchar](8) NULL,
	[COLIFE] [nchar](2) NULL,
	[COCOVERAGE] [nchar](2) NULL,
	[CORIDER] [nchar](2) NULL,
	[COPLNSFX] [int] NULL,
	[COCRTABLE] [nchar](4) NULL,
	[COPSTATCODE] [nchar](2) NULL,
	[COSTATCODE] [nchar](2) NULL,
	[COCRRCD] [int] NULL,
	[COCBUNST] [int] NULL,
	[CORCESDTE] [int] NULL,
	[COSUMINS] [numeric](17, 2) NULL,
	[COVALIDFLAG] [nchar](1) NULL,
	[COCURRFROM] [int] NULL,
	[COCURRTO] [int] NULL
)