ALTER VIEW [VM1DTA].[ADOC_ACMVREC] (
	UNIQUE_NUMBER
	,RDOCPFX
	,RDOCCOY
	,RDOCNUM
	,JRNSEQ
	,TRANNO
	,RECORDFORMAT
	,NONKEYS
	)
	with schemabinding
AS
 SELECT acmvpf.unique_number,
       acmvpf.rdocpfx,
       acmvpf.rdoccoy,
       acmvpf.rdocnum,
       acmvpf.jrnseq,
       acmvpf.tranno,
       'ACMVREC'                       AS RECORDFORMAT,
       LEFT(Isnull(acmvpf.rdocpfx, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(acmvpf.rdoccoy, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.rdocnum, ' ') + Replicate(' ', 9), 9)
       + LEFT(Isnull(Cast(acmvpf.jrnseq AS VARCHAR(max)), ' ') + Replicate(' ', 4), 4)
       + LEFT(Isnull(acmvpf.rldgpfx, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(acmvpf.rldgcoy, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.rldgacct, ' ') + Replicate(' ', 16), 16)
       + LEFT(Isnull(acmvpf.batccoy, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.batcbrn, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(Cast(acmvpf.batcactyr AS VARCHAR(max)), ' ') + Replicate(' ', 5), 5)
       + LEFT(Isnull(Cast(acmvpf.batcactmn AS VARCHAR(max)), ' ') + Replicate(' ', 3), 3)
       + LEFT(Isnull(acmvpf.batctrcde, ' ') + Replicate(' ', 4), 4)
       + LEFT(Isnull(acmvpf.batcbatch, ' ') + Replicate(' ', 5), 5)
       + LEFT(Isnull(acmvpf.genlcoy, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.genlcur, ' ') + Replicate(' ', 3), 3)
       + LEFT(Isnull(acmvpf.glcode, ' ') + Replicate(' ', 14), 14)
       + LEFT(Isnull(acmvpf.sacscode, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(acmvpf.sacstyp, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(Cast(acmvpf.origamt AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19)
       + LEFT(Isnull(acmvpf.origcurr, ' ') + Replicate(' ', 3), 3)
       + LEFT(Isnull(Cast(acmvpf.acctamt AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19)
       + LEFT(Isnull(Cast(acmvpf.crate AS VARCHAR(max)), ' ') + Replicate(' ', 20), 20)
       + LEFT(Isnull(Cast(acmvpf.tranno AS VARCHAR(max)), ' ') + Replicate(' ', 6), 6)
       + LEFT(Isnull(acmvpf.trandesc, ' ') + Replicate(' ', 30), 30)
       + LEFT(Isnull(acmvpf.glsign, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.postyear, ' ') + Replicate(' ', 4), 4)
       + LEFT(Isnull(acmvpf.postmonth, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(Cast(acmvpf.effdate AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9)
       + LEFT(Isnull(Cast(acmvpf.trdt AS VARCHAR(max)), ' ') + Replicate(' ', 7), 7)
       + LEFT(Isnull(Cast(acmvpf.trtm AS VARCHAR(max)), ' ') + Replicate(' ', 7), 7)
       + LEFT(Isnull(Cast(acmvpf.user_t AS VARCHAR(max)), ' ') + Replicate(' ', 7), 7)
       + LEFT(Isnull(acmvpf.termid, ' ') + Replicate(' ', 4), 4)
       + LEFT(Isnull(acmvpf.tranref, ' ') + Replicate(' ', 30), 30)
       + LEFT(Isnull(Cast(acmvpf. frcdate AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9)
       + LEFT(Isnull(acmvpf.intextind, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(Cast(acmvpf.rcamt AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19)
       + LEFT(Isnull(acmvpf.reconsbr, ' ') + Replicate(' ', 10), 10)
       + LEFT(Isnull(Cast(acmvpf.reconref AS VARCHAR(max)), ' ') + Replicate(' ', 10), 10)
       + LEFT(Isnull(acmvpf.suprflg, ' ') + Replicate(' ', 1), 1)
       + LEFT(Isnull(acmvpf.taxcode, ' ') + Replicate(' ', 2), 2)
       + LEFT(Isnull(acmvpf.stmtsort, ' ') + Replicate(' ', 20), 20)
       + LEFT(Isnull(Cast(acmvpf.creddte AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9)
       + LEFT(Isnull(acmvpf.usrprf, ' ') + Replicate(' ', 10), 10)
       + LEFT(Isnull(acmvpf.jobnm, ' ') + Replicate(' ', 10), 10)
       + CONVERT(VARCHAR, datime, 121) AS NONKEYS
FROM   vm1dta.acmvpf;  