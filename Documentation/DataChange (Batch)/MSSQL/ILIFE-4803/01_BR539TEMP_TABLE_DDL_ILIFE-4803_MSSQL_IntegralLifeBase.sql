IF OBJECT_ID('VM1DTA.BR539TEMP') IS NOT NULL 
BEGIN
   DROP TABLE [VM1DTA].[BR539TEMP]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [VM1DTA].[BR539TEMP](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[LOANNUMBER] [int] NULL,
	[LOANTYPE] [nchar](1) NULL,
	[LOANCURR] [nchar](3) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[FTRANNO] [int] NULL,
	[LTRANNO] [int] NULL,
	[LOANORIGAM] [numeric](17, 2) NULL,
	[LOANSTDATE] [int] NULL,
	[LSTCAPLAMT] [numeric](17, 2) NULL,
	[LSTCAPDATE] [int] NULL,
	[NXTCAPDATE] [int] NULL,
	[LSTINTBDTE] [int] NULL,
	[NXTINTBDTE] [int] NULL,
	[TPLSTMDTY] [numeric](17, 2) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[CH_UNIQUE_NUMBER] [bigint] NOT NULL,
	[CURRFROM] [int] NULL,
	[TRANNO] [int] NULL,
	[CNTTYPE] [nchar](3) NULL,
	[CHDRPFX] [nchar](2) NULL,
	[SERVUNIT] [nchar](2) NULL,
	[OCCDATE] [int] NULL,
	[CCDATE] [int] NULL,
	[COWNPFX] [nchar](2) NULL,
	[COWNCOY] [nchar](1) NULL,
	[COWNNUM] [nchar](8) NULL,
	[COLLCHNL] [nchar](2) NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[AGNTPFX] [nchar](2) NULL,
	[AGNTCOY] [nchar](1) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[MANDREF] [nchar](5) NULL,
	[PTDATE] [int] NULL,
	[BILLCHNL] [nchar](2) NULL,
	[BILLFREQ] [nchar](2) NULL,
	[BILLCURR] [nchar](3) NULL,
	[NEXTDATE] [int] NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[MANDSTAT] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL,
	[DATIME] [datetime] NOT NULL
) ON [PRIMARY]

GO


