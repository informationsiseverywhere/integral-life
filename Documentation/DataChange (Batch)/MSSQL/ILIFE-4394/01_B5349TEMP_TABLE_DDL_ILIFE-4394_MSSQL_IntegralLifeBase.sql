/****** Object:  Table [VM1DTA].[B5349TEMP]    Script Date: 8/4/2016 12:15:38 PM ******/
IF OBJECT_ID(N'[VM1DTA].[B5349TEMP]', N'U') IS NOT NULL
BEGIN
DROP TABLE [VM1DTA].[B5349TEMP]
END
GO


CREATE TABLE [VM1DTA].[B5349TEMP](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PAYRSEQNO] [int] NULL,
	[BILLSUPR] [nchar](1) NULL,
	[BILLSPFROM] [int] NULL,
	[BILLSPTO] [int] NULL,
	[BILLCHNL] [nchar](2) NULL,
	[BILLCD] [int] NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[PAYRTRANNO] [int] NULL,
	[PAYRBILLCD] [int] NULL,
	[BTDATE] [int] NULL,
	[PTDATE] [int] NULL,
	[OUTSTAMT] [numeric](17, 2) NULL,
	[NEXTDATE] [int] NULL,
	[PAYRBILLCURR] [nchar](3) NULL,
	[BILLFREQ] [nchar](2) NULL,
	[DUEDD] [nchar](2) NULL,
	[DUEMM] [nchar](2) NULL,
	[BILLDAY] [nchar](2) NULL,
	[BILLMONTH] [nchar](2) NULL,
	[SINSTAMT01] [numeric](17, 2) NULL,
	[SINSTAMT02] [numeric](17, 2) NULL,
	[SINSTAMT03] [numeric](17, 2) NULL,
	[SINSTAMT04] [numeric](17, 2) NULL,
	[SINSTAMT05] [numeric](17, 2) NULL,
	[SINSTAMT06] [numeric](17, 2) NULL,
	[PAYRCNTCURR] [nchar](3) NULL,
	[MANDREF] [nchar](5) NULL,
	[GRUPCOY] [nchar](1) NULL,
	[GRUPNUM] [nchar](8) NULL,
	[MEMBSEL] [nchar](10) NULL,
	[TAXRELMTH] [nchar](4) NULL,
	[INCSEQNO] [int] NULL,
	[EFFDATE] [int] NULL,
	[CHDRUNIQUENO] [bigint] NULL,
	[CHDRTRANNO] [int] NULL,
	[CNTTYPE] [nchar](3) NULL,
	[OCCDATE] [int] NULL,
	[STATCODE] [nchar](2) NULL,
	[PSTCDE] [nchar](2) NULL,
	[REG] [nchar](3) NULL,
	[CHDRCNTCURR] [nchar](3) NULL,
	[BILLCURR] [nchar](3) NULL,
	[CHDRPFX] [nchar](2) NULL,
	[COWNCOY] [nchar](1) NULL,
	[COWNNUM] [nchar](8) NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[SERVUNIT] [nchar](2) NULL,
	[CCDATE] [int] NULL,
	[COLLCHNL] [nchar](2) NULL,
	[COWNPFX] [nchar](2) NULL,
	[AGNTPFX] [nchar](2) NULL,
	[AGNTCOY] [nchar](1) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[CHDRBILLCHNL] [nchar](2) NULL,
  [ACCTMETH] [nchar](1) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[MANDSTAT] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL
) ON [PRIMARY]

GO


