/****** Object:  Table [VM1DTA].[BR608TEMP]    Script Date: 06/29/2017 Author:fwang3 ******/
IF OBJECT_ID(N'[VM1DTA].[BR608TEMP]', N'U') IS NOT NULL
BEGIN
DROP TABLE [VM1DTA].[BR608TEMP]
END
GO


CREATE TABLE [VM1DTA].[BR608TEMP](
	[COMPANY] [nchar](1) NULL,
	[VRTFUND] [nchar](4) NULL,
	[UNITYP] [nchar](1) NULL,
	[NOFUNTS] [numeric](16, 5) NULL
)




