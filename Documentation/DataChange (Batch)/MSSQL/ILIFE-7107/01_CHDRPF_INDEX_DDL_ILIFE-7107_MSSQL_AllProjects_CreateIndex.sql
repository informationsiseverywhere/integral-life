IF EXISTS ( SELECT * FROM sys.indexes i inner join sys.objects o on i.object_id = o.object_id WHERE i.name = 'CHDR5349' and o.name = 'CHDRPF' )
	DROP INDEX CHDR5349 ON VM1DTA.CHDRPF
GO
CREATE INDEX CHDR5349 ON  VM1DTA.CHDRPF (CHDRCOY,CHDRNUM,VALIDFLAG,SERVUNIT);
GO