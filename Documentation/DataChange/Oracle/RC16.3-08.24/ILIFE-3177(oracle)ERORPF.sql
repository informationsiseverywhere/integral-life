REM INSERTING into ERORPF
SET DEFINE OFF;
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFU6  ';
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFU5  ';
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFU4  ';
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFU7  ';
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFU8  ';

Insert into VM1DTA.ERORPF (UNIQUE_NUMBER,ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE) values (5063391,'ER',' ','E','          ','RFU6  ','AFR Actv Cant Switch FND  ',141219,190700,'000036 ','UNDERWR1','UNDERWR1  ','UNDERWR1',to_timestamp( SYSDATE, 'DD-MON-RR HH.MI.SSXFF AM'),'      ');
Insert into VM1DTA.ERORPF (UNIQUE_NUMBER,ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE) values (5063392,'ER',' ','E','          ','RFU5  ','AFR Actv Cant Redrct Prm ',141219,192308,'000036','UNDERWR1','UNDERWR1  ','UNDERWR1',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'),'      ');
Insert into VM1DTA.ERORPF (UNIQUE_NUMBER,ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE) values (5063393,'ER',' ','E','          ','RFU4  ','AFR Not Allowed on Plan',141220,122912,'000036','UNDERWR1','UNDERWR1','UNDERWR1',to_timestamp('20-DEC-14 12.29.12.217000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'      ');
Insert into VM1DTA.ERORPF (UNIQUE_NUMBER,ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE) values (5063399,'ER',' ','E','          ','RFU7  ','Unproc Unit & AFR Nt All   ',150204,154841,'001225','UNDERWR1','UNDERWR1','UNDERWR1',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'),'      ');
Insert into VM1DTA.ERORPF (UNIQUE_NUMBER,ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE) values (5063399,'ER',' ','E','          ','RFU8  ','Select either Fund Swith or Risk Profile  ',150204,154841,'001225 ','UNDERWR1','UNDERWR1','UNDERWR1',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'),'      ');

COMMIT;


