DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM user_triggers where trigger_name = 'RCVDPF_SEQ_TR';
  IF ( cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER  TRIGGER RCVDPF_SEQ_TR DISABLE ');
  END IF;  
END;
/

