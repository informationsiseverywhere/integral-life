create table vm1dta.CLEMPPF(
CLNTPFX varchar2(2) NULL,
CLNTCOY varchar2(1) NULL,
CLNTNUM varchar2(8) NULL,
EMPLOYER varchar2(20) NULL,
EMPID varchar2(10) NULL,
TITLE varchar2(15) NULL,
USRPRF varchar2(10) NULL,
JOBNM varchar2(10) NULL,
DATIME TIMESTAMP (6) NULL
);

