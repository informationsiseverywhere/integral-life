
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'REGPPF' and column_name ='INTERESTDAYS' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE REGPPF ADD INTERESTDAYS NUMBER(3,0) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.REGPPF.INTERESTDAYS IS ''No. of Days for Interest'''); 
  END IF;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'REGPPF' and column_name ='INTERESTRATE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE REGPPF ADD INTERESTRATE NUMBER(8,5) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.REGPPF.INTERESTRATE IS ''Interest Rate'''); 
  END IF; 
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'REGPPF' and column_name ='INTERESTAMT' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE REGPPF ADD INTERESTAMT NUMBER(17,2) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.REGPPF.INTERESTAMT IS ''Interest Amount'''); 
  END IF;      
END;
/