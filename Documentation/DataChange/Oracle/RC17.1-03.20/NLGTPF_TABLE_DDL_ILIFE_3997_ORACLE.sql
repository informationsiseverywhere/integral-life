DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'NLGTPF' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE ( 
    ' CREATE TABLE VM1DTA.NLGTPF 
    (	UNIQUE_NUMBER number(18,0),
	CHDRNUM char(8 char) ,
	CHDRCOY char(1 char) ,
	TRANNO NUMBER(5, 0) ,
	EFFDATE NUMBER(8, 0) ,
	BATCACTYR NUMBER(4, 0) ,
	BATCACTMN NUMBER(2, 0) ,
	BATCTRCDE char(4 char) ,
	TRANDESC char(30 char) ,
	TRANAMT NUMBER(17, 2) ,
	FROMDATE NUMBER(8, 0) ,
	TODATE NUMBER(8, 0) ,
	NLGFLAG char(1 char) ,
	YRSINF NUMBER(3, 0) ,
	AGE NUMBER(3, 0) ,
	NLGBAL NUMBER(17, 2) ,
	AMT01 NUMBER(17, 2) ,
	AMT02 NUMBER(17, 2) ,
	AMT03 NUMBER(17, 2) ,
	AMT04 NUMBER(17, 2) ,
	VALIDFLAG char(1 char) ,
	USRPRF char(10 char) ,
	JOBNM char(10 char) ,
	DATIME TIMESTAMP(6) ,
    CONSTRAINT PK_NLGTPF PRIMARY KEY (UNIQUE_NUMBER)
        )'
    );
  END IF;  
END;
/ 