
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T5544' AND DESCITEM='SR10CNY'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5544','SR10CNY','  ','E','','SR10 CNY','Switch Basis 10 - CNY','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5544','SR10CNY','  ','S','','SR10 CNY','切换基础 10 - CNY','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;
