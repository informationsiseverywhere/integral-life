declare 
  l_count integer;
begin

  select count(*)
    into l_count
  from user_tables
  where table_name = 'VM1DTA.BD5E4TEMP';

  if l_count > 0 then 
	  execute immediate 'drop table VM1DTA.BD5E4TEMP';
  end if;

end;
/
CREATE TABLE VM1DTA.BD5E4TEMP(
	BATCHID NUMBER(*,0),
  UNIQUE_NUMBER NUMBER(18,0),
	CHDRCOY CHAR(1) ,
	CHDRNUM CHAR(8) ,	
	CNTTYPE CHAR(3) ,	
	OWNNUM CHAR(8) ,
	RDOCNUM CHAR(9) ,
	EFFDATE   NUMBER(8,0),
	ORIGAMT NUMBER(17, 2) ,
  RCPTTYPE CHAR(1) ,
     CONSTRAINT PK_BD5E4TEMP PRIMARY KEY ("UNIQUE_NUMBER") 
)
 TABLESPACE "USERS" ;


