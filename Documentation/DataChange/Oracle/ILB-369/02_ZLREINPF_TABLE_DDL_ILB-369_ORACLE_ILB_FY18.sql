   declare 
  l_count integer;
begin

  select count(*)
    into l_count
  from user_tables
  where table_name = 'VM1DTA.ZLREINPF';

  if l_count > 0 then 
	  execute immediate 'drop table VM1DTA.ZLREINPF';
  end if;

end;
/

CREATE TABLE VM1DTA.ZLREINPF
  (
    UNIQUE_NUMBER NUMBER(18,0),
    CHDRCOY       CHAR(1) ,
    CHDRNUM       CHAR(8) ,
    CNTTYPE       CHAR(3) ,
    OWNNUM        CHAR(8) ,
    RDOCNUM       CHAR(9) ,
    EFFDATE       NUMBER(8,0),
    ORIGAMT       NUMBER(17,2),
    RCPTTYPE      CHAR(1) ,
    ERORCDE       CHAR(6) ,
    USRPRF        CHAR(10) ,
    JOBNM         CHAR(10) ,
    "DATIME"        TIMESTAMP (6), 
    CONSTRAINT PK_ZLREINPF PRIMARY KEY ("UNIQUE_NUMBER") 
  )
  TABLESPACE "USERS" ;