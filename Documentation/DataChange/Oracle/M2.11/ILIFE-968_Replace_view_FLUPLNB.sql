--------------------------------------------------------
--  ILIFE-968
--  Author: vhukumagrawa
--  Replace definition by providing braces in where condition
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."FLUPLNB" ("UNIQUE_NUMBER", "FUPTYP", "CHDRCOY", "CHDRNUM", "FUPNO", "TRANNO", "LIFE", "FUPCDE", "FUPSTS", "FUPDT", "FUPRMK", "CLAMNUM", "TERMID", "USER_T", "TRTM", "TRDT", "ZAUTOIND", "JLIFE", "USRPRF", "JOBNM", "DATIME", "EFFDATE", "CRTUSER", "CRTDATE", "LSTUPUSER", "ZLSTUPDT", "FUPRCVD", "EXPRDATE") AS 
  SELECT UNIQUE_NUMBER,
            FUPTYP,
            CHDRCOY,
            CHDRNUM,
            FUPNO,
            TRANNO,
            LIFE,
            FUPCDE,
            FUPSTS,
            FUPDT,
            FUPRMK,
            CLAMNUM,
            TERMID,
            USER_T,
            TRTM,
            TRDT,
            ZAUTOIND,
            JLIFE,
            USRPRF,
            JOBNM,
            DATIME,
            EFFDATE,
            CRTUSER,
            CRTDATE,
            LSTUPUSER,
            ZLSTUPDT,
            FUPRCVD,
            EXPRDATE
       FROM FLUPPF
      WHERE (FUPTYP = 'P' OR FUPTYP = 'U')
   ORDER BY CHDRCOY,
            CHDRNUM,
            FUPNO,
            UNIQUE_NUMBER DESC;
