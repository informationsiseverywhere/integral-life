--------------------------------------------------------
--  File created - Wednesday-November-19-2014   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View FLUPALT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."FLUPALT" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "FUPNO", "TRANNO", "FUPTYP", "LIFE", "JLIFE", "FUPCDE", "FUPSTS", "FUPDT", "FUPRMK", "CLAMNUM", "USER_T", "TERMID", "TRDT", "TRTM", "ZAUTOIND", "JOBNM", "USRPRF", "DATIME", "EFFDATE", "CRTUSER", "CRTDATE", "LSTUPUSER", "ZLSTUPDT", "FUPRCVD", "EXPRDATE") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            FUPNO,
            TRANNO,
            FUPTYP,
            LIFE,
            JLIFE,
            FUPCDE,
            FUPSTS,
            FUPDT,
            FUPRMK,
            CLAMNUM,
            USER_T,
            TERMID,
            TRDT,
            TRTM,
            ZAUTOIND,
            JOBNM,
            USRPRF,
            DATIME,
            EFFDATE,
            CRTUSER,
            CRTDATE,
            LSTUPUSER,
            ZLSTUPDT,
            FUPRCVD,
            EXPRDATE
       FROM FLUPPF
      WHERE (FUPTYP = 'P' OR FUPTYP = 'A')
   ORDER BY CHDRCOY,
            CHDRNUM,
            FUPNO,
            UNIQUE_NUMBER DESC;
/
