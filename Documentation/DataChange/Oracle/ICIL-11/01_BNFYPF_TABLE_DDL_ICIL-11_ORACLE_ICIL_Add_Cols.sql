DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BNFYPF' and column_name ='SEQUENCE' ; 
 IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BNFYPF ADD  SEQUENCE CHAR(2 CHAR) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BNFYPF.SEQUENCE  IS ''Beneficiery Sequence''');
    
  END IF;
  cnt :=0;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BNFYPF' and column_name ='PAYMMETH' ; 
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BNFYPF ADD  PAYMMETH CHAR(1 CHAR) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BNFYPF.PAYMMETH IS ''Beneficiery Payment Method''');
  END IF;
  cnt :=0;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BNFYPF' and column_name ='BANKKEY' ; 
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BNFYPF ADD  BANKKEY CHAR(10 CHAR) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BNFYPF.BANKKEY  IS ''Beneficiery Bank key''');
  END IF;
  cnt :=0;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BNFYPF' and column_name ='BANKACCKEY' ; 
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BNFYPF ADD  BANKACCKEY CHAR(20 CHAR) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BNFYPF.BANKACCKEY  IS ''Beneficiery Bank     Account key''');
  END IF; 
END;
/