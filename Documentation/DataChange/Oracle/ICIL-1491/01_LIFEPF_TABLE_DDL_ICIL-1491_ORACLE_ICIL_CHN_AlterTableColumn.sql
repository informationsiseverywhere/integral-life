DECLARE 
 cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LIFEPF' and column_name ='LIFEREL' ; 
  IF (cnt > 0) THEN
    EXECUTE IMMEDIATE ('ALTER TABLE LIFEPF MODIFY LIFEREL CHAR(4 CHAR)');
  END IF;
END;
/

