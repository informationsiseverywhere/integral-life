--------------------------------------------------------
--  DDL for View CLBL
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."CLBL" ("UNIQUE_NUMBER", "BANKKEY", "BANKACCKEY", "BANKACCDSC", "FACTHOUS", "CURRCODE", "BILLDATE01", "BILLDATE02", "BILLDATE03", "BILLDATE04", "BILLAMT01", "BILLAMT02", "BILLAMT03", "BILLAMT04", "REMITTYPE", "NEWRQST", "CLNTPFX", "CLNTCOY", "CLNTNUM", "CURRFROM", "CURRTO", "CLNTREL", "BNKACTYP", "VALIDFLAG", "SCTYCDE", "USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            BANKKEY,
            BANKACCKEY,
            BANKACCDSC,
            FACTHOUS,
            CURRCODE,
            BILLDATE01,
            BILLDATE02,
            BILLDATE03,
            BILLDATE04,
            BILLAMT01,
            BILLAMT02,
            BILLAMT03,
            BILLAMT04,
            REMITTYPE,
            NEWRQST,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            CURRFROM,
            CURRTO,
            CLNTREL,
            BNKACTYP,
            VALIDFLAG,
            SCTYCDE,
            USRPRF,
            JOBNM,
            DATIME
       FROM CLBAPF
	    WHERE NOT (VALIDFLAG <> '1')
   ORDER BY BANKKEY,
            BANKACCKEY,
            CLNTCOY,
            CLNTNUM ;
/