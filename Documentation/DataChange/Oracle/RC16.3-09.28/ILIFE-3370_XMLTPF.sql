CREATE TABLE "SESS"."XMLTPF" 
("UNIQUE_NUMBER" NUMBER(18,0), 
	"SRCSEQ" NUMBER(6,2), 
	"SRCDAT" NUMBER(6,0), 
	"XMLDATA" CHAR(325 CHAR), 
	 CONSTRAINT "PK_XMLTPF" PRIMARY KEY ("UNIQUE_NUMBER") 
 ) ; 

CREATE SEQUENCE SESS.SEQ_XMLTPF
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
CREATE OR REPLACE TRIGGER "SESS"."TR_XMLTPF" before insert on XMLTPF for each row
declare
       v_pkValue  number;
begin
     select SEQ_XMLTPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
end TR_XMLTPF;
/
ALTER TRIGGER "SESS"."TR_XMLTPF" ENABLE;
 
 