DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF h 
  WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E' 
	 AND h.EROREROR = 'RPJ2'  
      AND h.ERORDESC = 'Office Number required' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
    VALUES ('ER','','E','','RPJ2', 'Office Number required','0','0','000008','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'');    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF h 
  WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E' 
	 AND h.EROREROR = 'RPJ3'  
      AND h.ERORDESC = 'Rollover Details required' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
    VALUES ('ER','','E','','RPJ3', 'Rollover Details required','0','0','000008','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'');    
  END IF;
END;
/
