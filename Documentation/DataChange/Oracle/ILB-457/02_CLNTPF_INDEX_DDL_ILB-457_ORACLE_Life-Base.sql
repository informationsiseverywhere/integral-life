SET SERVEROUTPUT ON
DECLARE
  v_sql varchar(500):= 'DROP INDEX CLNTP6353'; 
BEGIN
  EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE = -1418 OR SQLCODE = -955 THEN
        NULL;
      ELSE
         RAISE;
      END IF;  
END;
/
CREATE INDEX CLNTP6353 ON CLNTPF (CLNTPFX,CLNTCOY,CLNTNUM,VALIDFLAG);