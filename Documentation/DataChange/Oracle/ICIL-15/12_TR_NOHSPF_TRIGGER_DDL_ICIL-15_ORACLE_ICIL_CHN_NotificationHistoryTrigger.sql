
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_NOHSPF" before insert on VM1DTA.NOHSPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_NOHSPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_NOHSPF;
   /
   