
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_INVSPF" before insert on VM1DTA.INVSPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_INVSPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_INVSPF;
   /
   