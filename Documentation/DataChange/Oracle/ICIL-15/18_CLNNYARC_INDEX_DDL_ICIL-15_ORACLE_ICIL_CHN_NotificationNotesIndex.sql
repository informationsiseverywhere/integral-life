
DECLARE
	cnt number(2,1) :=0;
BEGIN
	SELECT count(*) into cnt FROM USER_INDEXES where index_name = 'VM1DTA.CLNNYARC';
	IF (cnt > 0 ) THEN
	EXECUTE IMMEDIATE (' DROP INDEX VM1DTA.CLNNYARC ');
	EXECUTE IMMEDIATE ('CREATE INDEX VM1DTA.CLNNYARC on VM1DTA.CLNNPF(CLNNCOY,NOTIFINUM)');
ELSE
	EXECUTE IMMEDIATE ('CREATE INDEX VM1DTA.CLNNYARC on VM1DTA.CLNNPF(CLNNCOY,NOTIFINUM)');
	END IF;
END;
/
