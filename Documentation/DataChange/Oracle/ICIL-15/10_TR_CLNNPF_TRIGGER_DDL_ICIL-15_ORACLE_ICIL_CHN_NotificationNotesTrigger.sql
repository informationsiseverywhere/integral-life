
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_CLNNPF" before insert on VM1DTA.CLNNPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_CLNNPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_CLNNPF;
   /
   
