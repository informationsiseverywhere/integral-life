create or replace TRIGGER "VM1DTA"."TR_DHDRPF" 
before insert on  DHDRPF
for each row  
declare
v_pkValue  number;
v_flag     exception;
v_rownum   number;
PRAGMA EXCEPTION_INIT (v_flag, -1);  -- it is assumed
begin
v_rownum := 0;
 
select SEQ_DHDRPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
 
end TR_DHDRPF;
