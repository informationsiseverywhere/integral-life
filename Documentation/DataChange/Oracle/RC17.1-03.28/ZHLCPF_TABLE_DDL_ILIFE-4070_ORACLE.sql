-- Create sequence 
create sequence SEQ_ZHLCPF
minvalue 1000000
maxvalue 99999999999999999999
start with 1000000
increment by 1
nocache;

-- Create Trgger
CREATE OR REPLACE TRIGGER "TR_ZHLCPF" before
  INSERT ON ZHLCPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_ZHLCPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_ZHLCPF;
 
