DROP TABLE VM1DTA.ZCTXPF;
CREATE TABLE VM1DTA.ZCTXPF(
	UNIQUE_NUMBER numeric(18,0) NOT NULL,
	CHDRCOY nchar(1),
	CHDRNUM nchar(8),
	CNTTYPE nchar(3),
	CLNTNUM nchar(8),
	DATEFRM int,
	DATETO int ,
	ZSGTPCT numeric(5, 2),
	ZOERPCT numeric(5, 2),
	ZDEDPCT numeric(5, 2),
	ZUNDPCT numeric(5, 2),
	ZSPSPCT numeric(5, 2),
	TOTPRCNT numeric(5, 2),
	EFFDATE int,
	USRPRF nchar(10),
	JOBNM nchar(10) ,
	DATIME TIMESTAMP,
	AMOUNT numeric(17, 2) default 0,
	ZSGTAMT numeric(17, 2) default 0,
	ZOERAMT numeric(17, 2) default 0,
	ZDEDAMT numeric(17, 2) default 0,
	ZUNDAMT numeric(17, 2) default 0,
	ZSPSAMT numeric(17, 2) default 0
 )TABLESPACE USERS;

ALTER TABLE ZCTXPF ADD (
  CONSTRAINT PK_ZCTXPF
  PRIMARY KEY
  (UNIQUE_NUMBER));

CREATE SEQUENCE SEQ_ZCTXPF
  START WITH 1000000
  MAXVALUE 99999999999999999999
  MINVALUE 1000000
  NOCYCLE
  NOCACHE
  NOORDER;



CREATE OR REPLACE TRIGGER "TR_ZCTXPF" before insert on ZCTXPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_ZCTXPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_ZCTXPF;
/  


