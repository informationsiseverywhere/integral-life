DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVTPF' and column_name ='LNKGSUBREFNO' ; 
  IF ( cnt > 0 ) THEN
    EXECUTE IMMEDIATE ('ALTER TABLE COVTPF MODIFY LNKGSUBREFNO VARCHAR2(50)');
  END IF;  
END;
/