DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.itempf 
  where itemcoy = '2' and itemtabl = 'T5687' and itemitem like 'TRM1    ';
  IF ( cnt != 0 ) THEN
    update vm1dta.itempf set GENAREA=utl_raw.cast_to_raw('AN01BC01BCP1                    DC01    PM02Y        NF05SC01000PM01                       RCP200                 RCP1O    TRTERM    TR01                                    ') where itemcoy = '2' and itemtabl = 'T5687' and itemitem like 'TRM1    '; 
	commit;
  END IF;
  
  cnt :=0;
  SELECT count(1) INTO cnt 
  FROM vm1dta.itempf 
  where itemcoy = '2' and itemtabl = 'T5688' and itemitem like 'TRM     ';
   IF ( cnt != 0 ) THEN
    update vm1dta.itempf set GENAREA=utl_raw.cast_to_raw('   006Y    FLAT01000201    Y 010101                                                                                                                                           ') where itemcoy = '2' and itemtabl = 'T5688' and itemitem like 'TRM     ';
	commit;
  END IF;
  
END;
/