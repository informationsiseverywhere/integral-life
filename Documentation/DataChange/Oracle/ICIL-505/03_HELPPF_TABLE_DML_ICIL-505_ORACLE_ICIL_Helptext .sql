
SET DEFINE OFF;
delete from VM1DTA.HELPPF where HELPITEM = 'PAYEENO' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYEENO',1,'1','Payee: This field represents the client number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;


delete from VM1DTA.HELPPF where HELPITEM = 'PAYEENO' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYEENO',1,'1','Payee: This field represents the client number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;





delete from VM1DTA.HELPPF where HELPITEM = 'PAYMTHBF' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',1,'1','A code indicating the payment type ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',2,'1','(e.g. Cash, Automatic Cheque, etc.) must be input for each','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',3,'1','Payment being created.The Payment Type determines','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',4,'1','the data which must be input on the payment details','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',5,'1','screen in order to create a  payment request','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',6,'1','(e.g. if the Payment Type indicates a manual cheque,','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',7,'1','the Cheque Number must be input on the details screen).','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',8,'1','When creating a payment, the standard window facility exists','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','PAYMTHBF',9,'1','to assist in the selection of the Payment Type required.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;




delete from VM1DTA.HELPPF where HELPITEM = 'PAYMTHBF' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',1,'1','A code indicating the payment type','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',2,'1','(e.g. Cash, Automatic Cheque, etc.) must be input for each ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',3,'1','Payment being created.The Payment Type determines ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',4,'1','the data which must be input on the payment details ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',5,'1','screen in order to create a  payment request','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',6,'1','(e.g. if the Payment Type indicates a manual cheque,','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',7,'1','the Cheque Number must be input on the details screen).','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',8,'1','When creating a payment, the standard window facility exists','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','PAYMTHBF',9,'1','to assist in the selection of the Payment Type required.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;



delete from VM1DTA.HELPPF where HELPITEM = 'BANKACTKEY' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','BANKACTKEY',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;


delete from VM1DTA.HELPPF where HELPITEM = 'BANKACTKEY' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','BANKACTKEY',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;




delete from VM1DTA.HELPPF where HELPITEM = 'CRDTCRD' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sh533     ','CRDTCRD',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;


delete from VM1DTA.HELPPF where HELPITEM = 'CRDTCRD' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sh533     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sh533     ','CRDTCRD',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
