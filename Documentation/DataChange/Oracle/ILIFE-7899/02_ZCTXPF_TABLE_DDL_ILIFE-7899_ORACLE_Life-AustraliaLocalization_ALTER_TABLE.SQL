-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZCTXPF' and column_name ='COCONTAMNT' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE ZCTXPF ADD COCONTAMNT NUMERIC(17,2)');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN ZCTXPF.COCONTAMNT  IS '' Co-Contribution Amount ''');
  END IF;  
END;
/
commit;

DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZCTXPF' and column_name ='LISCAMNT' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE ZCTXPF ADD LISCAMNT NUMERIC(17,2)');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN ZCTXPF.LISCAMNT  IS '' LISC Amount ''');
  END IF;  
END;
/
commit;