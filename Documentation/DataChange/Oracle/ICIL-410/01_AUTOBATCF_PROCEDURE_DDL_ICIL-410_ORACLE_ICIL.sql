create or replace PROCEDURE          "AUTOBATCF" (
    v_BPFX   CHAR,
    v_BCOY   CHAR,
    v_BBRN   CHAR,
    v_BACTYR NUMBER,
    v_BACTMN NUMBER,
    v_BTRCDE CHAR,
    v_USRPRF CHAR,
    v_JOBNM  CHAR,
    v_BACBCH OUT NUMBER )
AS
  v_UNIQ NUMBER(19,0);
BEGIN
  SELECT unique_number , batcbatch   INTO v_UNIQ, v_BACBCH
  FROM VM1DTA.BATCFPF
  WHERE BATCCOY  = v_BCOY   AND BATCPFX    = v_BPFX   AND BATCACTMN  = v_BACTMN   AND BATCACTYR  = v_BACTYR   AND BATCTRCDE  = v_BTRCDE AND BATCBRN = v_BBRN; 
  v_BACBCH := v_BACBCH + 1 ;
  UPDATE VM1DTA.BATCFPF SET BATCBATCH = v_BACBCH WHERE UNIQUE_NUMBER = v_UNIQ;

  EXCEPTION
	WHEN NO_DATA_FOUND THEN  -- catches all 'no data found' errors
  BEGIN
      v_BACBCH := 1 ;
      INSERT
      INTO VM1DTA.BATCFPF
            (
              BATCPFX,
              BATCCOY,
              BATCBRN,
              BATCACTYR,
              BATCACTMN,
              BATCTRCDE,
              BATCBATCH,
              USRPRF,
              JOBNM,
              DATIME
            )
            VALUES
            (
              v_BPFX,
              v_BCOY,
              v_BBRN,
              v_BACTYR,
              v_BACTMN,
              v_BTRCDE,
              v_BACBCH,
              v_USRPRF,
              v_JOBNM,
              SYSTIMESTAMP
            );
  END;
END;
/
COMMIT;