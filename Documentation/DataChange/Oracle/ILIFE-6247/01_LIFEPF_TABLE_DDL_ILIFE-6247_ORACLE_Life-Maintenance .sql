DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LIFEPF' and column_name ='TRDT' ; 
  IF (cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.LIFEPF MODIFY TRDT NUMBER(10) ');
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LIFEPF' and column_name ='TRTM' ; 
  IF (cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.LIFEPF MODIFY TRTM NUMBER(10) ');
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LIFEPF' and column_name ='USER_T' ; 
  IF (cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.LIFEPF MODIFY USER_T NUMBER(10) ');
  END IF;
END;
/