
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."SURDCLM" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "TRANNO", "LIFE", "JLIFE", "COVERAGE", "RIDER", "CRTABLE", "SHORTDS", "LIENCD", "CURRCD", "PLNSFX", "EMV", "ACTVALUE", "TYPE_T", "VRTFUND", "USRPRF", "JOBNM", "DATIME","OTHERADJST") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            CRTABLE,
            SHORTDS,
            LIENCD,
            CURRCD,
            PLNSFX,
            EMV,
            ACTVALUE,
            TYPE_T,
            VRTFUND,
            USRPRF,
            JOBNM,
            DATIME,
			OTHERADJST
       FROM SURDPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            TRANNO,
            PLNSFX,
            LIFE,
            COVERAGE,
            RIDER,
            CRTABLE,
            UNIQUE_NUMBER DESC;
 