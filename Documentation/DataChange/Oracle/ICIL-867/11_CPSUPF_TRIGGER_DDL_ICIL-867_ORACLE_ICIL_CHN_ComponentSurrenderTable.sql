

CREATE OR REPLACE TRIGGER "VM1DTA"."TR_CPSUPF" before insert on VM1DTA.CPSUPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_CPSUPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_CPSUPF;
   /
   
 