
set define off;
delete from VM1DTA.DESCPF where DESCTABL='TR2A5' and DESCITEM='SUSUR008' and DESCCOY='2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUSUR008','  ','E',' ','SUSUR008','Trad Component Surrender','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUSUR008','  ','S',' ','SUSUR008','Trad Component Surrender','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;