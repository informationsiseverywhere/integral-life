
SET DEFINE OFF;

delete from VM1DTA.HELPPF where HELPITEM = 'ADJAMT' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',1,'1','This field is a data entry field','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',2,'1','which allows user to enter any adjustment ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',3,'1','amount.The adjustment amount can ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',4,'1','be either positive or negative.','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',5,'1','This value shall be added to the Refund ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',6,'1','Premium to form the Refund Amount(the final ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','ADJAMT',7,'1','amount that is to be refunded to the client).','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;

delete from VM1DTA.HELPPF where HELPITEM = 'ADJAMT' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',1,'1','This field is a data entry field','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',2,'1','which allows user to enter any adjustment ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',3,'1','amount.The adjustment amount can ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',4,'1','be either positive or negative.','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',5,'1','This value shall be added to the Refund ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',6,'1','Premium to form the Refund Amount(the final ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','ADJAMT',7,'1','amount that is to be refunded to the client).','UNDERWR1  ','UNDERWR1',current_timestamp);

/*reqntype*/

delete from VM1DTA.HELPPF where HELPITEM = 'REQNTYPE' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',1,'1','This is a drop-down list and the ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',2,'1','selection is referring to table T3672.  ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',3,'1','It should be defaulted to the method of payout chosen at','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',4,'1','the time of creating the contract and all other ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',5,'1','details are to follow as per defined ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',6,'1','during contract issue but allow user ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',7,'1','to change accordingly. This ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','REQNTYPE',8,'1','is a mandatory field. ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;

delete from VM1DTA.HELPPF where HELPITEM = 'REQNTYPE' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',1,'1','This is a drop-down list and the ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',2,'1','selection is referring to table T3672.  ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',3,'1','It should be defaulted to the method of payout chosen at','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',4,'1','the time of creating the contract and all other ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',5,'1','details are to follow as per defined ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',6,'1','during contract issue but allow user ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',7,'1','to change accordingly. This ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','REQNTYPE',8,'1','is a mandatory field. ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;



delete from VM1DTA.HELPPF where HELPITEM = 'BANKACCKEY' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','BANKACCKEY',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;


delete from VM1DTA.HELPPF where HELPITEM = 'BANKACCKEY' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','BANKACCKEY',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;




delete from VM1DTA.HELPPF where HELPITEM = 'CRDTCRD' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','Sd5lh     ','CRDTCRD',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;


delete from VM1DTA.HELPPF where HELPITEM = 'CRDTCRD' and HELPLANG = 'S' and HELPTYPE = 'F' AND HELPPROG='Sd5lh     ';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',1,'1','This field represents the Bank Account number ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',2,'1','or Credit cards number of Payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',3,'1','If the method of payout is direct credit then this field ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',4,'1','should be treated as Bank Account Number of the payee ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',5,'1','and If the Method of Payout is Credit Card Payout then ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',6,'1','this field should be treated as Credit card ','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','Sd5lh     ','CRDTCRD',7,'1','number of the payee.','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;

