CREATE OR REPLACE TRIGGER "VM1DTA"."TR_BNFYPF"  
before insert on  BNFYPF
for each row
declare
v_pkValue  number;

begin
select SEQ_BNFYPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;

end TR_BNFYPF;