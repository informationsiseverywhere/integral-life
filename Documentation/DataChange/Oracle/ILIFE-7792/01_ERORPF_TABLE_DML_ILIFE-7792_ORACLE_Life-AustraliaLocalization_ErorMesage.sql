DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF h 
  WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E' AND h.EROREROR = 'RRMR  ';
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
    VALUES ('ER',' ','E','          ','RRMR  ', 'Beneficiary add/mod not allowed',0,0,'',null,'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,' ');    
  END IF;
END;
/