DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('GE') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','GE',' ','E',' ','General','General','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('TE') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','TE',' ','E',' ','Textile','Textile','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('OR') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','OR',' ','E',' ','Oil Refina','Oil Refinary','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('DR') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','DR',' ','E',' ','Driving','Driving','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('EL') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','EL',' ','E',' ','Electric','Electric Industry','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('OG') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','OG',' ','E',' ','Oil Gas','Oil and Gas','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('MM') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','MM',' ','E',' ','Merchant','Merchant Marine','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('GA') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','GA',' ','E',' ','Garage','Garage','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('CO') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','CO',' ','E',' ','Constructi','Construction','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('AV') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','AV',' ','E',' ','Aviation','Aviation','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('CH') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','CH',' ','E',' ','Chemical','Chemical','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('EN') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','EN',' ','E',' ','Entertainm','Entertainment','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('FO') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','FO',' ','E',' ','Food','Food','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('HO') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','HO',' ','E',' ','Hospitalit','Hospitality','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('RF') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','RF',' ','E',' ','TV&FILM','Rad TV & Film','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('HE') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','HE',' ','E',' ','Health','Health','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('DI') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','DI',' ','E',' ','Diver ','Diver','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);

DELETE FROM VM1DTA.DESCPF WHERE DESCTABL IN ('T3628') AND DESCITEM IN ('AG') AND LANGUAGE='E';

INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','9','T3628','AG',' ','E',' ','Agricultur','Agriculture','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP);


