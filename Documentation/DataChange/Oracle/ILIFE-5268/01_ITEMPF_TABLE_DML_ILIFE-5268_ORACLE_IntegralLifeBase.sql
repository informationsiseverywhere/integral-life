DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ITEMPF 
  where ITEMTABL='T6633' and ITEMITEM IN('CED','GCS','GCSA','GCSD','GCSE','GCSP') ;
  IF ( cnt > 0 ) THEN
       UPDATE ITEMPF set TABLEPROG='P6633' where ITEMTABL='T6633' and ITEMITEM IN('CED','GCS','GCSA','GCSD','GCSE','GCSP');
  end if;
end;
/