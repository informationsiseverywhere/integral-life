
set define off;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYDH'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDH','  ','E','','Death Clm','All Products with DH status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDH','  ','S','','Death Clm','All Products with DH status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYCF'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYCF','  ','E','','CFI','All Products with CF status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYCF','  ','S','','CFI','All Products with CF status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYDC'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDC','  ','E','','Declined','All Products with DC status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDC','  ','S','','Declined','All Products with DC status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYEX'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYEX','  ','E','','Expiry','All Products with EX status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYEX','  ','S','','Expiry','All Products with EX status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYLA'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYLA','  ','E','','Lapsed','All Products with LA status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYLA','  ','S','','Lapsed','All Products with LA status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYMA'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYMA','  ','E','','Matured','All Products with MA status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYMA','  ','S','','Matured','All Products with MA status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYNT'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYNT','  ','E','','Not Taken','All Products with NT status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYNT','  ','S','','Not Taken','All Products with NT status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYPO'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPO','  ','E','','Postpone','All Products with PO status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPO','  ','S','','Postpone','All Products with PO status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYPS'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPS','  ','E','','Proposal','All Products with PS status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPS','  ','S','','Proposal','All Products with PS status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYPU'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPU','  ','E','','	Paid Up','All Products with PU status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPU','  ','S','','	Paid Up','All Products with PU status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR630' and DESCITEM='***CNYWD'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYWD','  ','E','','Withdrawn','All Products with WD status','UNDERWR1','UNDERWR1',current_timestamp);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYWD','  ','S','','Withdrawn','All Products with WD status','UNDERWR1','UNDERWR1',current_timestamp);	
end if;
end;
/
commit;


