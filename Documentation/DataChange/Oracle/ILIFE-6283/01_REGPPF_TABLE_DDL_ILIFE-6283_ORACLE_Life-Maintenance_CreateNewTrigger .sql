CREATE OR REPLACE TRIGGER "VM1DTA"."TR_REGPPF" 
before insert on  REGPPF
for each row
declare
v_pkValue  number;
v_flag     exception;
v_rownum   number;
PRAGMA EXCEPTION_INIT (v_flag, -1);  -- it is assumed
begin
select SEQ_REGPPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
end TR_REGPPF;