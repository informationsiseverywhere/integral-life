set define off;
DECLARE cnt number(2,1) :=0;
BEGIN
	SELECT count(*) into cnt FROM user_tables where table_name = 'EXTNPF' ;  
  IF ( cnt = 0 ) THEN
  EXECUTE IMMEDIATE ( 
'CREATE TABLE VM1DTA.EXTNPF
   (	
		UNIQUE_NUMBER NUMBER(18,0), 
		RDOCPFX VARCHAR2(2) NULL,
		RDOCCOY VARCHAR2(1) NULL,
		RDOCNUM VARCHAR2(9) NULL,
		REFERENCE VARCHAR2(12) NULL,
		VALIDFLAG VARCHAR2(1) NULL,
		CONSTRAINT PK_EXTNPF PRIMARY KEY (UNIQUE_NUMBER)
   )'
  );
  END IF;
END;
/
   
DECLARE cnt2 number(2,1) :=0;
BEGIN
	SELECT count(*) into cnt2 FROM user_sequences where sequence_name='SEQ_EXTNPF';
IF( cnt2=0) THEN
EXECUTE IMMEDIATE(
'CREATE SEQUENCE  VM1DTA.SEQ_EXTNPF  MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 1000000 NOCACHE  NOORDER  NOCYCLE'
);
END IF;
END;
/
   
create or replace
TRIGGER VM1DTA.TR_EXTNPF 
before insert on  VM1DTA.EXTNPF
for each row
declare
v_pkValue  number;
v_rownum   number;
begin
v_rownum := 0;
select SEQ_EXTNPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
end TR_EXTNPF;
/