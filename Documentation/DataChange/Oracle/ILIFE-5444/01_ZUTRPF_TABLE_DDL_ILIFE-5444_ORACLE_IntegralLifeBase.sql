DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'ZUTRPF' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMediate (
    'CREATE TABLE VM1DTA.ZUTRPF 
         (	UNIQUE_NUMBER NUMBER(18,0), 
            CHDRPFX VARCHAR2(2 CHAR), 
            CHDRCOY VARCHAR2(1 CHAR),
			CHDRNUM VARCHAR2(8 CHAR),
            LIFE VARCHAR2(2 CHAR), 
            COVERAGE VARCHAR2(2 CHAR),
            RIDER VARCHAR2(2 CHAR), 
            PLNSFX NUMBER(4,0),
            TRANNO NUMBER (5,0),
            RSUNIN VARCHAR2 (1 CHAR),
            RUNDTE NUMBER (8,0),
            BATCTRCDE VARCHAR2 (4 CHAR),
            TRDT NUMBER (6,0),
            TRTM NUMBER (6,0),
            USER_T NUMBER (6,0),
            EFFDATE NUMBER (8,0),
            VALIDFLAG VARCHAR2 (1 CHAR),
            DATESUB NUMBER (8,0),
			CRTUSER VARCHAR2 (10 CHAR),
			USRPRF VARCHAR2 (10 CHAR),
			JOBNM VARCHAR2 (10 CHAR),
			DATIME TIMESTAMP (6),
            CONSTRAINT PK_ZUTRPF PRIMARY KEY (UNIQUE_NUMBER)
        )'
    );
  END IF;  
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_sequences where sequence_name = 'SEQ_ZUTRPF' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (
'CREATE SEQUENCE  "VM1DTA"."SEQ_ZUTRPF"  MINVALUE 1 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE'
 );
  END IF;  
END;
/


CREATE OR REPLACE TRIGGER "VM1DTA"."TR_ZUTRPF" before insert on VM1DTA.ZUTRPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_ZUTRPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_ZUTRPF;
   /
   