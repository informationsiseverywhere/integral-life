delete from VM1DTA.HELPPF where helpitem='INDXFLG' and helpprog='S5654';


insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',1,'1','This determines whether the premiums and/or Sums Assured by ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',2,'1','this Contract may be subject to Automatic Increase          ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',3,'1','processing, for example, to keep pace with inflation.       ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',4,'1','The ''Automatic Increase Contract Rules'' Table contains an   ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',5,'1','entry for each Product. These entries indicate whether      ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',6,'1','Increases are Mandatory, Optional or Not Applicable for each','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',7,'1','Contract Type.                                              ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',8,'1','When Increases are �O � Optional�, this field is enabled and','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',9,'1','defaults to ''Y'', indicating that Increases are to apply to  ','UNDERWR1  ','UNDERWR1',sysdate);


insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',10,'1','this contract. User can overwrite this to ''N'' if Increases  ','UNDERWR1  ','UNDERWR1',sysdate);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values ('','HP',' ','E','F','S5654     ','INDXFLG   ',11,'1','are not required.                                           ','UNDERWR1  ','UNDERWR1',sysdate);

Commit;