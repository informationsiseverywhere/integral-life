
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."MANDLNB" ("UNIQUE_NUMBER", "TERMID", "TRDT", "TRTM", "USER_T", "PAYRCOY", "PAYRNUM", "MANDREF", "VALIDFLAG", "BANKKEY", "BANKACCKEY", "TIMESUSE", "MANDSTAT", "STATCHDATE", "LUSEDATE", "LUSEAMT", "LUSESCODE", "MANDAMT", "EFFDATE", "DETLSUMM", "USRPRF", "JOBNM", "DATIME","CRCIND") AS 
  SELECT UNIQUE_NUMBER,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            PAYRCOY,
            PAYRNUM,
            MANDREF,
            VALIDFLAG,
            BANKKEY,
            BANKACCKEY,
            TIMESUSE,
            MANDSTAT,
            STATCHDATE,
            LUSEDATE,
            LUSEAMT,
            LUSESCODE,
            MANDAMT,
            EFFDATE,
            DETLSUMM,
            USRPRF,
            JOBNM,
            DATIME,
            CRCIND            
       FROM MANDPF
      WHERE VALIDFLAG = '1'
   ORDER BY PAYRCOY, PAYRNUM, MANDREF DESC   