DROP VIEW SALH1;
CREATE OR REPLACE FORCE VIEW VM1DTA.SALH1 ("UNIQUE_NUMBER", "CLNTCOY", "CLNTNUM", "INCSEQNO", "TAXYR", "DECGRSAL", "EVIDDATE", "CURRCODE", "USER_T", "TERMID", "TRDT", "TRTM", "USRPRF", "JOBNM", "DATIME", "VALIDFLAG")
AS
  SELECT UNIQUE_NUMBER,CLNTCOY,CLNTNUM,INCSEQNO,
    TAXYR,DECGRSAL,EVIDDATE,CURRCODE,USER_T,
    TERMID,TRDT,TRTM,USRPRF,JOBNM,DATIME,VALIDFLAG
  FROM SALHPF
  ORDER BY CLNTCOY,CLNTNUM,INCSEQNO,TAXYR DESC;
    
DROP VIEW SALH;
CREATE OR REPLACE FORCE VIEW VM1DTA.SALH ("UNIQUE_NUMBER", "CLNTCOY", "CLNTNUM", "INCSEQNO", "TAXYR", "DECGRSAL", "EVIDDATE", "CURRCODE", "USER_T", "TERMID", "TRDT", "TRTM", "USRPRF", "JOBNM", "DATIME", "VALIDFLAG")
AS
   SELECT UNIQUE_NUMBER,CLNTCOY,CLNTNUM,INCSEQNO,
    TAXYR,DECGRSAL,EVIDDATE,CURRCODE,USER_T,
    TERMID,TRDT,TRTM,USRPRF,JOBNM,DATIME,VALIDFLAG
  FROM SALHPF
  ORDER BY CLNTCOY,CLNTNUM,INCSEQNO,TAXYR DESC;
COMMIT;
