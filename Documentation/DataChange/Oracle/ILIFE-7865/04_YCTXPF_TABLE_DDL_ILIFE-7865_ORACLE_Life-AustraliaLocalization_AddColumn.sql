-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'YCTXPF' and column_name ='NOTRCVD' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE YCTXPF ADD NOTRCVD VARCHAR2(1) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN YCTXPF.NOTRCVD  IS '' S290 Notice Received ''');
  END IF;  
END;
/
commit;