SET DEFINE OFF;

delete from VM1DTA.HELPPF where HELPITEM = 'BILLFREQ' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='SR674';
COMMIT;
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SR674','BILLFREQ',1,'1','Disallowing contract�s Billing Frequency to be changed after','UNDERWR1  ','UNDERWR1',current_timestamp);
	 
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SR674','BILLFREQ',2,'1','contract is issued can be set at product level in table','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SR674','BILLFREQ',3,'1','T5688 by setting �Billing Frequency Change not allowed','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SR674','BILLFREQ',4,'1','field in T5688 is ticked','UNDERWR1  ','UNDERWR1',current_timestamp);

commit;




























