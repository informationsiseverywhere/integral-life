
SET DEFINE OFF;
delete from VM1DTA.ERORPF where ERORPFX='ER' and (ERORLANG='S' or ERORLANG='E') and EROREROR like 'RRAR%';
delete from VM1DTA.ERORPF where ERORPFX='ER' and (ERORLANG='S' or ERORLANG='E') and EROREROR like 'RRAS%';
delete from VM1DTA.ERORPF where ERORPFX='ER' and (ERORLANG='S' or ERORLANG='E') and EROREROR like 'RRAT%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAR  ','Postcode to be numeric',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAR  ','Postcode to be numeric',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAS  ','Postcode to be 6-digits',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAS  ','Postcode to be 6-digits',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAT  ','Postcode is mandatory',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAT  ','Postcode is mandatory',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);


commit; 