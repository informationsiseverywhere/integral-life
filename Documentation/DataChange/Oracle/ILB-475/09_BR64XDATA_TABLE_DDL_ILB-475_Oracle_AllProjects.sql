DECLARE
	cnt number(2,1):=0;
BEGIN
	SELECT count(*)into cnt FROM user_tables where table_name='BR64XTEMP';
	IF( cnt=1 ) THEN
		EXECUTE IMMEDIATE('DROP TABLE VM1DTA.BR64XTEMP');
			END IF;
END;
/

DECLARE
cnt number(2,1):=0;
BEGIN
SELECT count(*)into cnt FROM user_tables where table_name='BR64XDATA';
IF( cnt = 0 )THEN
EXECUTE IMMEDIATE(
'

CREATE TABLE VM1DTA.BR64XDATA(
	BATCHID NUMBER,
	UNIQUE_NUMBER NUMBER,
	CHDRCOY CHAR(1 CHAR),
	CHDRNUM CHAR(8 CHAR),
	SEQNO NUMBER,
	EXMCODE CHAR(10 CHAR),
	ZMEDTYP CHAR(8 CHAR),
	PAIDBY CHAR(1 CHAR),
	LIFE CHAR(2 CHAR),
	JLIFE CHAR(2 CHAR),
	EFFDATE NUMBER,
	INVREF CHAR(15 CHAR),
	ZMEDFEE NUMBER(17, 2),
	CLNTNUM CHAR(8 CHAR),
	DTETRM NUMBER,
	DESC_T CHAR(50 CHAR),
	MEMBER_NAME CHAR(10 CHAR),
	CHDRTRANNO NUMBER,
	CHDRUNIQUENO NUMBER,
	CHDRPFX CHAR(2 CHAR),
	CNTTYPE CHAR(3 CHAR),
	OCCDATE NUMBER,
	CNTCURR CHAR(3 CHAR),
	PTDATE NUMBER,
	MEDIUNIQUE NUMBER)');
	
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.BATCHID IS ''BATCHID''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.UNIQUE_NUMBER IS ''UNIQUE_NUMBER''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CHDRCOY  IS ''CHDRCOY''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CHDRNUM IS ''CHDRNUM''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.SEQNO IS ''SEQNO''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.EXMCODE IS ''EXMCODE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.ZMEDTYP IS ''ZMEDTYP''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.PAIDBY IS ''PAIDBY''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.LIFE IS ''LIFE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.JLIFE  IS ''JLIFE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.EFFDATE IS ''EFFDATE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.INVREF IS ''INVREF''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.ZMEDFEE IS ''ZMEDFEE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CLNTNUM IS ''CLNTNUM''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.DTETRM IS ''DTETRM''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.DESC_T IS ''DESC_T''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.MEMBER_NAME IS ''MEMBER_NAME''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CHDRTRANNO IS ''CHDRTRANNO''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CHDRUNIQUENO IS ''CHDRUNIQUENO''');	
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CHDRPFX IS ''CHDRPFX''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CNTTYPE IS ''CNTTYPE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.OCCDATE IS ''OCCDATE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.CNTCURR IS ''CNTCURR''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.PTDATE IS ''PTDATE''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR64XDATA.MEDIUNIQUE IS ''MEDIUNIQUE''');	
	EXECUTE IMMEDIATE ('COMMENT ON TABLE VM1DTA.BR64XDATA IS ''TEMP FILE FOR BR64X''');
	
	  END IF; 
 END;
/

