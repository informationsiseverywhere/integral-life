

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='AGMTN014'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Agency Sales License' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='AGMTN014' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 销售资格类型' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='AGMTN014' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='UNWRT001'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Risk Amount Display' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='UNWRT001' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 保额显示' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='UNWRT001' and LANGUAGE='S';		
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='9' AND DESCTABL='TR2A5' and DESCITEM='AGMTN015'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='9CH Bancassurance-agent input' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='AGMTN015' and LANGUAGE='E';
	update DESCPF set LONGDESC ='9CH 银行保险代理人输入' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='AGMTN015' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CTCNL002'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH China freelook cancel' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTCNL002' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH China freelook cancel' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTCNL002' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='9' AND DESCTABL='TR2A5' and DESCITEM='NBPRP056'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='9CH Occupation Class FeaConfg' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='NBPRP056' and LANGUAGE='E';
	update DESCPF set LONGDESC ='9CH 职业类别Fea设定' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='NBPRP056' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSLND002'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Policy Loans Debt' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSLND002' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 保单贷款' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSLND002' and LANGUAGE='S';	
end if;
end;
/
commit;
 

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='SUSUR002'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH China full surrender' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUSUR002' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH China full surrender' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUSUR002' and LANGUAGE='S';	
end if;
end;
/
commit;  

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSCOM004'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Disallow TXN in colng pred' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSCOM004' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 犹豫期内不允许' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSCOM004' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='SUOTR006'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Change of Annuity Method' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUOTR006' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH Change of Annuity Method' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUOTR006' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSCOM006'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Annuity Products' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSCOM006' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 年金产品' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSCOM006' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSPUP001'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Paid Up Processing' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSPUP001' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 缴清处理' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSPUP001' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='SUOTR007'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Cash Dividend' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUOTR007' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH Cash Dividend' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='SUOTR007' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSMIN003'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Follow-ups Maintenance' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSMIN003' and LANGUAGE='E';	
	update DESCPF set LONGDESC ='2CH 跟进维护' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSMIN003' and LANGUAGE='S';	
end if;
end;
/
commit;

  
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='NBPRP084'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Contract Proposal Enquiry' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='NBPRP084' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH Contract Proposal Enquiry' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='NBPRP084' and LANGUAGE='S';	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='BTPRO012'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Waiver Provision' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='BTPRO012' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH Waiver Provision' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='BTPRO012' and LANGUAGE='S';		
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='BTPRO013'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Batch Proces of L2REGPAY' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='BTPRO013' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH L2REGPAY Batch程序' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='BTPRO013' and LANGUAGE='S';	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CTENQ003'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Contract Inquiries APL/PL' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTENQ003' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH APL/PL保单查询' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTENQ003' and LANGUAGE='S';	
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CSLND004'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Deferred Policy Loan' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSLND004' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 保单查询' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CSLND004' and LANGUAGE='S';	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' and DESCITEM='CTENQ002'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='2CH Contract Enquiry' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTENQ002' and LANGUAGE='E';
	update DESCPF set LONGDESC ='2CH 保单查询' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='CTENQ002' and LANGUAGE='S';	
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='9' AND DESCTABL='TR2A5' and DESCITEM='CHNNFLS1'; 
  IF ( cnt > 0 ) THEN
	update DESCPF set LONGDESC ='9CH China localization fields' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='CHNNFLS1' and LANGUAGE='E';	
	update DESCPF set LONGDESC ='9CH 中国新加字段' where DESCPFX='IT' and DESCCOY='9' and DESCTABL='TR2A5' and DESCITEM='CHNNFLS1' and LANGUAGE='S';
end if;
end;
/
commit;