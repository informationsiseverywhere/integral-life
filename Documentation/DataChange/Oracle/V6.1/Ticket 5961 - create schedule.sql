DECLARE 
VAL1 NUMBER := 0;
VAL2 NUMBER := 0;

BEGIN

VAL1 := VM1DTA.SEQ_BSCDPF.NEXTVAL;
SELECT MAX(UNIQUE_NUMBER) INTO VAL2 FROM VM1DTA.BSCDPF;

IF (VAL2 > VAL1) THEN
  
  EXECUTE IMMEDIATE 'ALTER SEQUENCE  VM1DTA.SEQ_BSCDPF INCREMENT BY ' || (VAL2 - VAL1 + 1);  
  
  EXECUTE IMMEDIATE 'ALTER SEQUENCE  VM1DTA.SEQ_BSCDPF INCREMENT BY 1';
  
END IF;

END;
/

DECLARE 
VAL1 NUMBER := 0;
VAL2 NUMBER := 0;

BEGIN

VAL1 := VM1DTA.SEQ_BSCTPF.NEXTVAL;
SELECT MAX(UNIQUE_NUMBER) INTO VAL2 FROM VM1DTA.BSCTPF;

IF (VAL2 > VAL1) THEN
  
  EXECUTE IMMEDIATE 'ALTER SEQUENCE  VM1DTA.SEQ_BSCTPF INCREMENT BY ' || (VAL2 - VAL1 + 1);  
  
  EXECUTE IMMEDIATE 'ALTER SEQUENCE  VM1DTA.SEQ_BSCTPF INCREMENT BY 1';
  
END IF;

END;
/

