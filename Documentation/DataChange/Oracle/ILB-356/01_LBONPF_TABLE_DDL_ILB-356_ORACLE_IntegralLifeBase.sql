DECLARE 
  cnt number(2,1);
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'LBONPF' ;
IF ( cnt = 0  ) THEN  
	EXECUTE IMMediate ('CREATE TABLE LBONPF(   
        UNIQUE_NUMBER  number(10)  ,
        CHDRCOY   VARCHAR2 (1) NULL,
        CHDRNUM   VARCHAR2 (8) NULL,
        LIFE   VARCHAR2 (2) NULL,
        COVERAGE   VARCHAR2 (2) NULL,
        RIDER   VARCHAR2 (2) NULL,
        PLANSFX   VARCHAR2 (4) NULL,
        EFFDATE   number(8)  NULL,
        TRANNO   number(8)   NULL,
        TRCDE   VARCHAR2 (4) NULL,
        TRNAMT   number(10) NULL,
        NETPRE   number(10) NULL,
        UEXTPC   number(10) NULL,
        UALPRC   number(10) NULL,
        USRPRF   VARCHAR2 (10) NULL,
        JOBNM   VARCHAR2 (10) NULL,
        DATIME   TIMESTAMP (6)  NULL)        '
	);
END IF ;
END;
/