
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZCHXPF' and column_name ='STATCODE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.ZCHXPF ADD STATCODE NCHAR(2) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN ZCHXPF.STATCODE IS ''STATCODE ''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZCHXPF' and column_name ='PSTCDE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.ZCHXPF ADD PSTCDE NCHAR(2) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN ZCHXPF.PSTCDE IS ''PSTCDE ''');
  END IF;  
END;
/

COMMIT;