DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5353DATA' and column_name ='CHDRNLGFLG' ; 
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE ('ALTER TABLE VM1DTA.B5353DATA ADD CHDRNLGFLG NCHAR(1)');
  END IF;
END;
/