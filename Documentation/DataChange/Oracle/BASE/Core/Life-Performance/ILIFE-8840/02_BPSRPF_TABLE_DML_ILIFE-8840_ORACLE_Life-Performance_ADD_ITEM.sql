DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF 
  WHERE BPRIORPROC='LONX-CR' AND BSUBSEQPRC='#BR538';
  IF ( cnt = 0 ) THEN
insert into VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','LONX-CR','2','#BR538    ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF 
  WHERE BPRIORPROC='#BR538' AND BSUBSEQPRC='#BR539';
  IF ( cnt = 0 ) THEN
insert into VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','#BR538    ','2','#BR539    ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF 
  WHERE BPRIORPROC='LONX-CV' AND BSUBSEQPRC='#BR534';
  IF ( cnt = 0 ) THEN
insert into VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','LONX-CV','2','#BR534    ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF 
  WHERE BPRIORPROC='#BR534' AND BSUBSEQPRC='#BR535';
  IF ( cnt = 0 ) THEN
insert into VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','#BR534    ','2','#BR535    ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

COMMIT;


