﻿
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
   WHERE DESCCOY='2' AND DESCTABL='TD5GF' AND LANGUAGE = 'S' AND DESCITEM = 'CCI**';
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET  LONGDESC='中国重疾险' WHERE DESCCOY='2' AND DESCTABL='TD5GF' AND LANGUAGE = 'S' AND DESCITEM = 'CCI**';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='TD5GF' AND LANGUAGE = 'S' AND DESCITEM = 'CDA**';
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET  LONGDESC='中国递延年金' WHERE DESCCOY='2' AND DESCTABL='TD5GF' AND LANGUAGE = 'S' AND DESCITEM = 'CDA**';
end if;
end;
/
commit;

DECLARE 
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T5688' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T5688' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='TR52Y' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='TR52Y' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3623' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3623' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3588' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3588' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3590' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3590' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T6691' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T6691' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T5400' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T5400' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T5606' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T5606' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T5606' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T5606' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3629' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3629' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE
  cnt number :=0;
BEGIN
  SELECT count(1) INTO cnt
  FROM vm1dta.DESCPF
  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3590' AND LANGUAGE = 'S' AND SHORTDESC != LONGDESC AND length(trim(LONGDESC))<=10;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set SHORTDESC = trim(LONGDESC)  WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL='T3590' AND LANGUAGE = 'S' AND length(trim(LONGDESC))<=10;
  END IF;
END;
/
COMMIT;