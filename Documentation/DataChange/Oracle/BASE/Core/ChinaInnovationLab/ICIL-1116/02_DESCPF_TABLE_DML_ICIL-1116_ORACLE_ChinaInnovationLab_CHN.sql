
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T3614' and DESCITEM='AUS98'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'T3614', 'AUS98   ', '  ', 'S', '', '          ', '澳大利亚1998  ', 'UNDERWR1  ', 'UNDERWR1  ', current_timestamp);
  end if;
  IF ( cnt > 0 ) THEN
update  VM1DTA.DESCPF set LONGDESC='澳大利亚1998  ' where DESCCOY='2' and DESCTABL='T3614' and DESCITEM='AUS98' and LANGUAGE = 'S';
  end if;
end;
/
commit;
