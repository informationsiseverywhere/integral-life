
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'NOTIPF' and column_name ='ACCIDENTDESC' ; 
  IF ( cnt > 0 ) THEN
 EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOTIPF modify (ACCIDENTDESC VARCHAR2(3000 CHAR)) ');
  END IF;       
END;
/ 
commit;