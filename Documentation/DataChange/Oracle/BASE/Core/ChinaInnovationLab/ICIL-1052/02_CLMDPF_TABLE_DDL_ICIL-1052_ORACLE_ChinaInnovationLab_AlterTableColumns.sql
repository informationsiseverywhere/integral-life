DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMDPF' and column_name ='CLAIMNO' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE CLMDPF ADD CLAIMNO VARCHAR2(9 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.CLMDPF.CLAIMNO IS ''CLAIM NUMBER'''); 
  END IF;       
END;
/ 

DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMDPF' and column_name ='CLAIMNOTIFINO' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE CLMDPF ADD CLAIMNOTIFINO VARCHAR2(8 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.CLMDPF.CLAIMNOTIFINO IS ''CLAIMNOTIFINO'''); 
  END IF;       
END;
/ 