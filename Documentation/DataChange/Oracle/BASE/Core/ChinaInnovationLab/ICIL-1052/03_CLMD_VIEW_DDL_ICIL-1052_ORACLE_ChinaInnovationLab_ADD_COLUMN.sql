
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."CLMDCLM" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "TRANNO", "LIFE", "JLIFE", "COVERAGE", "RIDER", "VALIDFLAG", "CRTABLE", "SHORTDS", "LIENCD", "CNSTCUR", "EMV", "ACTVALUE", "VIRTFND", "TYPE_T", "ANNYPIND", "CLAIMNO", "CLAIMNOTIFINO", "USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            VALIDFLAG,
            CRTABLE,
            SHORTDS,
            LIENCD,
            CNSTCUR,
            EMV,
            ACTVALUE,
            VIRTFND,
            TYPE_T,
            ANNYPIND,
			CLAIMNO,
			CLAIMNOTIFINO,
			USRPRF,
            JOBNM,
            DATIME
       FROM CLMDPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            COVERAGE,
            RIDER,
            CRTABLE;
/