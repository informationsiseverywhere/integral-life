CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."MREL" ("UNIQUE_NUMBER", "MCLTCOY", "MCLTNUM", "TERMID", "TRDT", "TRTM", "USER_T", "CLTRELN", "CLNTCOY", "CLNTNUM", "USRPRF", "JOBNM", "DATIME", "DTEEFF", "DTETER") AS 
  SELECT UNIQUE_NUMBER,
            MCLTCOY,
            MCLTNUM,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            CLTRELN,
            CLNTCOY,
            CLNTNUM,
            USRPRF,
            JOBNM,
            DATIME,
            DTEEFF,
            DTETER
       FROM VM1DTA.CRELPF
	   WHERE VALIDFLAG = '1'
	   ;