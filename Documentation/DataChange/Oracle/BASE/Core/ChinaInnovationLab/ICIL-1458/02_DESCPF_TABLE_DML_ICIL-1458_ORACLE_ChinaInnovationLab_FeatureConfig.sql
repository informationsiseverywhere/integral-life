
SET DEFINE OFF;
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5G4' and DESCITEM='CDA1' and LANGUAGE='S'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','TD5G4','CDA1',' ','S',' ','CDA1','中国延期年金','ASHARMA228','ASHARMA228',sysdate );
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='PS' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='保单延期' where  DESCTABL = 'T3623' AND DESCITEM='PS' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='SU' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='保单退保' where  DESCTABL = 'T3623' AND DESCITEM='SU' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='DC' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='保单拒保' where  DESCTABL = 'T3623' AND DESCITEM='DC' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='FL' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='犹豫期退保' where  DESCTABL = 'T3623' AND DESCITEM='FL' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='CF' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='取消－从起始日 ' where  DESCTABL = 'T3623' AND DESCITEM='CF' AND DESCCOY='2';
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3623' and DESCITEM='AP' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='组成更改-增加建议书' where  DESCTABL = 'T3623' AND DESCITEM='AP' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3588' and DESCITEM='HA' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='保费假期' where  DESCTABL = 'T3588' AND DESCITEM='HA' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3588' and DESCITEM='CF' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='取消自起始日' where  DESCTABL = 'T3588' AND DESCITEM='CF' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3588' and DESCITEM='FL' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='犹豫期退保' where  DESCTABL = 'T3588' AND DESCITEM='FL' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T3588' and DESCITEM='PP' and LANGUAGE='S'; 
  IF ( cnt != 0 ) THEN
	UPDATE VM1DTA.DESCPF SET shortdesc ='犹豫期退保' where  DESCTABL = 'T3588' AND DESCITEM='PP' AND DESCCOY='2';
end if;
end;
/
commit;

DECLARE 
  cnt number(3,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3688' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3688' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3676' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3676' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(4,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3695' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3695' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(4,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T5500' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T5500' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T5687' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T5687' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T1680' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T1680' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  WHERE DESCCOY='2' AND DESCTABL='T3639' AND LANGUAGE = 'S' AND ITEMSEQ IS NULL ;
  IF ( cnt != 0 ) THEN
    update VM1DTA.DESCPF set ITEMSEQ='  ' WHERE DESCCOY='2' AND DESCTABL='T3639' AND LANGUAGE = 'S';
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1691' AND DESCITEM='SPR2DE' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1691','SPR2DE ',' ','S','loMIQG7U< k','FileUPDW ','文件上传/下载','UNDERWR1 ','UNDERWR1 ',sysdate );
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' AND DESCITEM='SRH4' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','SRH4 ',' ','S','loMwQ=<F k','FUH ','文件上传历史','UNDERWR1 ','UNDERWR1 ',sysdate );
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' AND DESCITEM='FUD1' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','FUD1 ',' ','S','loMwQ=<2 k','FUD1 ','文件上传/下载','UNDERWR1 ','UNDERWR1 ',sysdate );
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='TR2A5' AND DESCITEM='FLUPL001' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','TR2A5','FLUPL001',' ','S','loMwQ=<!( k','FLUPL001 ','文件上传','UNDERWR1 ','UNDERWR1 ',sysdate );
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5HP' AND DESCITEM='ACCR01' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'TD5HP', 'ACCR01  ', '  ', 'S', '', 'ACCR01    ', 'ACCR01                        ', 'UNDERWR1', 'UNDERWR1', sysdate);
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5G4' AND DESCITEM='AAIB' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'TD5G4', 'AAIB  ', '  ', 'S', '', 'AAIB    ', '意外保险利益', 'UNDERWR1', 'UNDERWR1', TO_TIMESTAMP('2018-11-08 15:17:01.709000', 'YYYY-MM-DD HH24:MI:SS.FF6'));
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5G4' AND DESCITEM='ACCR' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'TD5G4', 'ACCR  ', '  ', 'S', '', 'ACCR    ', '加速危机保障附加险', 'UNDERWR1', 'UNDERWR1', TO_TIMESTAMP('2018-11-08 15:17:01.709000', 'YYYY-MM-DD HH24:MI:SS.FF6'));
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5G4' AND DESCITEM='DAN2' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'TD5G4', 'DAN2  ', '  ', 'S', '', 'DAN2    ', '延期年金2', 'UNDERWR1', 'UNDERWR1', TO_TIMESTAMP('2018-11-08 15:17:01.709000', 'YYYY-MM-DD HH24:MI:SS.FF6'));
  END IF;
END;
/
COMMIT;










