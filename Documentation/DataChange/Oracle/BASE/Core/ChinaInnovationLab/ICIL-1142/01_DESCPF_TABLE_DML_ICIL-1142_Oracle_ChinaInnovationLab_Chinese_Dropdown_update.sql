﻿
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.DESCPF 
  where DESCCOY='2' AND DESCTABL='T3676' AND DESCITEM='4' AND LANGUAGE = 'S'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF(DESCPFX,DESCCOY,DESCTABL,DESCITEM,LANGUAGE,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) VALUES ('IT','2','T3676','4','S','Journal','日记','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
  ELSE
    Update vm1dta.DESCPF set LONGDESC='日记' where LANGUAGE = 'S' and DESCCOY='2' and DESCTABL='T3676' and DESCITEM='4';
  END IF;
END;
/
COMMIT;




