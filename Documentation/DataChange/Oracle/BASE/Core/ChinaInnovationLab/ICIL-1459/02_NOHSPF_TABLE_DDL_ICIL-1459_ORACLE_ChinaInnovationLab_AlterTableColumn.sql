DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'NOHSPF' and column_name ='NOTIHSNUM' ; 
  IF ( cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY NOTIHSNUM VARCHAR2(14 CHAR) ');
	EXECUTE IMMEDIATE (' UPDATE VM1DTA.NOHSPF t SET NOTIHSNUM = substr((REPLACE(t.NOTIHSNUM,''CLMNTF'')),1,8) ');
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY NOTIHSNUM VARCHAR2(8 CHAR) ');
  END IF;  
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'NOHSPF' and column_name ='TRANSCODE' ; 
  IF ( cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY TRANSCODE VARCHAR2(14 CHAR) ');
	EXECUTE IMMEDIATE (' UPDATE VM1DTA.NOHSPF t SET TRANSCODE = substr(TRANSCODE,1,4) ');
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY TRANSCODE VARCHAR2(4 CHAR) ');
  END IF;  
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'NOHSPF' and column_name ='TRANSDESC' ; 
  IF ( cnt > 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY TRANSDESC VARCHAR2(1000 CHAR) ');
	EXECUTE IMMEDIATE (' UPDATE VM1DTA.NOHSPF t SET TRANSDESC = substr(TRANSDESC,1,30) ');
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.NOHSPF MODIFY TRANSDESC VARCHAR2(30 CHAR) ');
  END IF;  
END;
/
