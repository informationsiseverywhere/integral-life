
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'TR4BZ' AND DESCITEM='T542' AND LANGUAGE='E';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','TR4BZ','T542','  ','E','','T542','Maturity Payout','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END IF;
END;
/

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'THSUZ' AND DESCITEM='T542' AND LANGUAGE='E';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','THSUZ','T542','  ','E','','T542','Maturity Payout','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END IF;
END;
/

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'T2636' AND DESCITEM='PXMCLAMT' AND LANGUAGE='E';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T2636','PXMCLAMT','  ','E','','MATH      ','Maturity Payout','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END IF;
END;
/



COMMIT;