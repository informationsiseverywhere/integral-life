
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='TRDT' ; 
  IF ( cnt > 0 ) THEN
		EXECUTE IMMEDIATE (' Alter table VM1DTA.CLMHPF MODIFY TRDT NUMBER(8)  ');

  END IF;  
END;
/
commit;