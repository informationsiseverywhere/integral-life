DECLARE 
 cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'GLPRPF' and column_name ='RGHEADER' ; 
  IF (cnt > 0) THEN
    EXECUTE IMMEDIATE ('Alter table VM1DTA.GLPRPF modify  RGHEADER VARCHAR2(140 CHAR) ');
  END IF;
END;
/
  