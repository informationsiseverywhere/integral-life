DECLARE 
 cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'GPAYPF' and column_name ='EBANKDTL' ; 
  IF (cnt > 0) THEN
    EXECUTE IMMEDIATE ('Alter table VM1DTA.GPAYPF modify  EBANKDTL VARCHAR2(440 CHAR)');
  END IF;
END;
/
 