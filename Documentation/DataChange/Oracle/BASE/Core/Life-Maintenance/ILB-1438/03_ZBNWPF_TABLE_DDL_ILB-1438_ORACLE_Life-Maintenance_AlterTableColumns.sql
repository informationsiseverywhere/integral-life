DECLARE 
 cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZBNWPF' and column_name ='ZBNWREC' ; 
  IF (cnt > 0) THEN
    EXECUTE IMMEDIATE ('Alter table VM1DTA.ZBNWPF modify  ZBNWREC VARCHAR2(1500 CHAR)');
  END IF;
END;
/
 