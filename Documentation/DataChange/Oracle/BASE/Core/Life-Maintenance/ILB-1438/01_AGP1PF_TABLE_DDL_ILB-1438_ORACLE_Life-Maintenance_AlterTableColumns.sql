DECLARE 
 cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'AGP1PF' and column_name ='TEXTFLD' ; 
  IF (cnt > 0) THEN
    EXECUTE IMMEDIATE ('Alter table VM1DTA.AGP1PF modify  TEXTFLD VARCHAR2(250 CHAR)');
  END IF;
END;
/
