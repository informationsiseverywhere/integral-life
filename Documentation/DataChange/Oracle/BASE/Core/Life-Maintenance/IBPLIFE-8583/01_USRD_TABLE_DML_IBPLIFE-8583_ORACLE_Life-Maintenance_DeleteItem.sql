
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.USRD WHERE  USERID='UNDERWR1  ' AND LONGUSERID <> 'UNDERWR1  ';
  IF ( cnt > 0 ) THEN 
	DELETE FROM VM1DTA.USRD WHERE  USERID='UNDERWR1  ' AND LONGUSERID <> 'UNDERWR1  ';
  END IF;
END;
/
COMMIT;