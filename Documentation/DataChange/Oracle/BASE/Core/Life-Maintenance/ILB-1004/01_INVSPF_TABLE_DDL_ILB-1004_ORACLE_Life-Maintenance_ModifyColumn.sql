
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'INVSPF' and column_name ='INVESRESULT' ; 
  IF ( cnt > 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.INVSPF MODIFY INVESRESULT nvarchar2(1000) ');

  END IF;  
END;
/
commit;