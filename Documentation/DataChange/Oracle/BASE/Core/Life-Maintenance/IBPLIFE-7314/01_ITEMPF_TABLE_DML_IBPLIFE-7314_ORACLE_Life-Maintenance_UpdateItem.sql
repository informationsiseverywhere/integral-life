DECLARE
    cnt number:=0;
BEGIN
    SELECT COUNT(1) into cnt
    FROM VM1DTA.ITEMPF
    WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%EBMIM%';
    IF ( cnt > 0 ) THEN
      update vm1dta.itempf set GENAREA=utl_raw.cast_to_raw(REPLACE(utl_raw.cast_to_varchar2(GENAREA),'EBMIM','BMIM ')) 
     WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%EBMIM%';
	END IF;
	
	SELECT COUNT(1) into cnt
    FROM VM1DTA.ITEMPF
    WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%SBMIM%';
    IF ( cnt > 0 ) THEN
      update vm1dta.itempf set GENAREA=utl_raw.cast_to_raw(REPLACE(utl_raw.cast_to_varchar2(GENAREA),'SBMIM','BMIM ')) 
      WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%SBMIM%';
	END IF;
	
	SELECT COUNT(1) into cnt
    FROM VM1DTA.ITEMPF
    WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%EBMII%';
    IF ( cnt > 0 ) THEN
      update vm1dta.itempf set GENAREA=utl_raw.cast_to_raw(REPLACE(utl_raw.cast_to_varchar2(GENAREA),'EBMII','BMII ')) 
      WHERE itemtabl = 'TR675' AND ITEMCOY = '2'  and itempfx = 'IT' and utl_raw.cast_to_varchar2(GENAREA) like '%EBMII%';
	END IF;
	
END;
/ 
commit;


