
DECLARE 
  cnt number(2,1) :=0;
BEGIN
	SELECT count(1) INTO cnt 
	FROM VM1DTA.ITEMPF 
	WHERE ITEMTABL = 'TR529' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEN1' AND ITMFRM='19900101' AND ITMTO='19990131';
	 IF ( cnt > 0 ) THEN
		UPDATE VM1DTA.ITEMPF  SET ITMTO='99999999' WHERE ITEMTABL = 'TR529' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEN1' AND ITMFRM='19900101' AND ITMTO='19990131';
		
END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
	SELECT count(1) INTO cnt 
	FROM VM1DTA.ITEMPF 
	WHERE ITEMTABL = 'TR529' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEND' AND ITMFRM='19900101' AND ITMTO='19990131';
	 IF ( cnt > 0 ) THEN
		UPDATE VM1DTA.ITEMPF  SET ITMTO='99999999' WHERE ITEMTABL = 'TR529' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEND' AND ITMFRM='19900101' AND ITMTO='19990131';
		
END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
	SELECT count(1) INTO cnt 
	FROM VM1DTA.ITEMPF 
	WHERE ITEMTABL = 'TR530' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEN1' AND ITMFRM='19900101' AND ITMTO='19991231';
	 IF ( cnt > 0 ) THEN
		UPDATE VM1DTA.ITEMPF  SET ITMTO='99999999' WHERE ITEMTABL = 'TR530' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AEN1' AND ITMFRM='19900101' AND ITMTO='19991231';
		
END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
	SELECT count(1) INTO cnt 
	FROM VM1DTA.ITEMPF 
	WHERE ITEMTABL = 'TR51E' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AENAEND';
	 IF ( cnt > 0 ) THEN
		UPDATE VM1DTA.ITEMPF  SET VALIDFLAG='2' WHERE ITEMTABL = 'TR51E' AND ITEMCOY= '2' AND ITEMPFX = 'IT' AND ITEMITEM = 'AENAEND';
		
END IF;
END;
/
COMMIT;
