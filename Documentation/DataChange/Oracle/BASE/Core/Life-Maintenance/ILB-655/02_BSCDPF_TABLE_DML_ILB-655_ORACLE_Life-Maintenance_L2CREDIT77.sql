DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCDPF 
  WHERE BSCHEDNAM = 'L2CREDIT77' and BAUTHCODE = 'B273' and PRODCODE='L'; 
  IF ( cnt = 0 ) THEN
 insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME) values ('L2CREDIT77',1,'N','N','B273','QBATCH','5','PAXUS','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP) ; 
  END IF;
END;
/

COMMIT;