
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5H7' AND DESCPFX='HE' AND language='E'; 
  IF ( cnt > 0 ) THEN
    update VM1DTA.DESCPF set shortdesc='' , longdesc='Accumulated Amt Interest Rules' where descpfx='HE' and desccoy='2' and desctabl='TD5H7' and language='E';
  	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5H7' AND DESCPFX='HE' AND language='S'; 
  IF ( cnt > 0 ) THEN
    update VM1DTA.DESCPF set shortdesc='' , longdesc='Accumulated Amt Interest Rules' where descpfx='HE' and desccoy='2' and desctabl='TD5H7' and language='E';
    update VM1DTA.DESCPF set shortdesc='' , longdesc='Accumulated Amt Interest Rules' where descpfx='HE' and desccoy='2' and desctabl='TD5H7' and language='S';
	
end if;
end;
/
commit;
