
delete from VM1DTA.DESCPF where DESCTABL='T5645' and DESCITEM='PR532' and DESCCOY = '2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T5645' AND DESCITEM='PR532'; 
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T5645','PR532   ','  ','E',' ','End Mat   ','Endowment and Maturity        ','UNDERWR1  ','UNDERWR1  ',current_timestamp) ;
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T5645','PR532   ','  ','S',' ','End Mat   ','Endowment and Maturity        ','UNDERWR1  ','UNDERWR1  ',current_timestamp) ;
end if;
end;
/
commit;


delete from VM1DTA.DESCPF where DESCTABL='T5677' and DESCITEM='T6A1UM08' and DESCCOY = '2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T5677' AND DESCITEM='T6A1UM08'; 
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5677','T6A1UM08','  ','E',' ','          ','T6A1UM08                      ','UNDERWR1  ','UNDERWR1  ',current_timestamp);
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5677','T6A1UM08','  ','S',' ','          ','T6A1UM08                      ','UNDERWR1  ','UNDERWR1  ',current_timestamp);
end if;
end;
/
commit;



