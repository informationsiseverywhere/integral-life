

DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRKV';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRKV  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRKV  ','Claim Amount < 0.00', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRKV  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRKV  ','Claim Amount < 0.00', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
 COMMIT; 