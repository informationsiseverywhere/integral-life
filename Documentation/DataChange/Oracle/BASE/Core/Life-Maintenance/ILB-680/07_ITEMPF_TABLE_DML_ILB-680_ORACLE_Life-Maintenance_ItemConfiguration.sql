DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ITEMPF h 
  WHERE h.ITEMPFX = 'IT' AND h.ITEMCOY='2' AND h.ITEMTABL = 'TD5GF' AND h.ITEMITEM='***'
      AND h.TABLEPROG ='PD5GF' AND h.VALIDFLAG = '1';
  IF ( cnt = 0 ) THEN
    Insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values ('IT','2','TD5GF','***','  ',' ','PD5GF','1',0,0,'324D4D4646303030303630303030303535303539393939303534393939303130303330303130303330','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);  
  END IF;
END;
/


COMMIT;