
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFEREV" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "TRANNO", "VALIDFLAG", "LIFE", "JLIFE", "TERMID", "TRDT", "TRTM", "USER_T", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            VALIDFLAG,
            LIFE,
            JLIFE,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            DATIME
       FROM LIFEPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            TRANNO,
            UNIQUE_NUMBER DESC;
