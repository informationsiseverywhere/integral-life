
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFEENQ" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "JLIFE", "LIFCNUM", "SMOKING", "OCCUP", "PURSUIT01", "PURSUIT02", "VALIDFLAG", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            LIFCNUM,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            VALIDFLAG,
            DATIME
       FROM LIFEPF
      WHERE NOT (VALIDFLAG <> '1')
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
