
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LFCLLNB" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFCNUM", "VALIDFLAG", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFCNUM,
            VALIDFLAG,
            DATIME
       FROM LIFEPF
      WHERE VALIDFLAG = '3'
   ORDER BY CHDRCOY, LIFCNUM;
