
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."ZLIFELC" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFCNUM", "VALIDFLAG", "LIFE", "JLIFE", "STATCODE", "TRANNO", "CURRFROM", "CURRTO", "LCDTE", "LIFEREL", "CLTDOB", "CLTSEX", "ANBCCD", "AGEADM", "SELECTION", "SMOKING", "OCCUP", "PURSUIT01", "PURSUIT02", "MARTAL", "SBSTDL", "TERMID", "TRDT", "TRTM", "USER_T", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFCNUM,
            VALIDFLAG,
            LIFE,
            JLIFE,
            STATCODE,
            TRANNO,
            CURRFROM,
            CURRTO,
            LCDTE,
            LIFEREL,
            CLTDOB,
            CLTSEX,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            SBSTDL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            DATIME
       FROM LIFEPF
      WHERE NOT (VALIDFLAG = '2')
   ORDER BY CHDRCOY, LIFCNUM, UNIQUE_NUMBER DESC;
