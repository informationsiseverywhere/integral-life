
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFEPTS" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "STATCODE", "TRANNO", "LIFE", "JLIFE", "LCDTE", "LIFCNUM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            STATCODE,
            TRANNO,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
            DATIME
       FROM LIFEPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
