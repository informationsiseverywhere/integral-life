
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFERAS" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "LIFE", "JLIFE", "ANBCCD", "CLTSEX", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            LIFE,
            JLIFE,
            ANBCCD,
            CLTSEX,
            DATIME
       FROM LIFEPF
      WHERE NOT (VALIDFLAG <> '1')
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
