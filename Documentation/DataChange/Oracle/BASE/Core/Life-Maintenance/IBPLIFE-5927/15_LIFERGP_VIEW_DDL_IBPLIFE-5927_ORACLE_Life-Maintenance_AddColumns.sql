
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFERGP" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "LIFE", "JLIFE", "ANBCCD", "CLTSEX", "CLTDOB", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            LIFE,
            JLIFE,
            ANBCCD,
            CLTSEX,
            CLTDOB,
            DATIME
       FROM LIFEPF
      WHERE NOT (VALIDFLAG <> '1')
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
