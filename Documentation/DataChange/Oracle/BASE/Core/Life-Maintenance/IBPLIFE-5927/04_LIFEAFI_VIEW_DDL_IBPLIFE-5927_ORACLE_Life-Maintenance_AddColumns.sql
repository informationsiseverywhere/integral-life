
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFEAFI" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "TRANNO", "CURRFROM", "CURRTO", "STATCODE", "LIFE", "JLIFE", "LCDTE", "LIFCNUM", "LIFEREL", "CLTSEX", "ANBCCD", "AGEADM", "SELECTION", "SMOKING", "OCCUP", "PURSUIT01", "PURSUIT02", "MARTAL", "TERMID", "TRDT", "TRTM", "USER_T", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            TRANNO,
            CURRFROM,
            CURRTO,
            STATCODE,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
            LIFEREL,
            CLTSEX,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            DATIME
       FROM LIFEPF
      WHERE NOT (VALIDFLAG = '3')
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            TRANNO,
            UNIQUE_NUMBER DESC;
