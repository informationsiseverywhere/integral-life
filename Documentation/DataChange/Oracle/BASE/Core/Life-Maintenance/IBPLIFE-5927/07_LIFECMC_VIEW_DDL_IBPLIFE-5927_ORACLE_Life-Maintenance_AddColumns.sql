
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFECMC" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "JLIFE", "ANBCCD", "CLTDOB", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            ANBCCD,
            CLTDOB,
            DATIME
       FROM LIFEPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
