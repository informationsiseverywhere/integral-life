
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."LIFECFI" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "STATCODE", "TRANNO", "LIFE", "JLIFE", "TERMID", "TRDT", "TRTM", "USER_T", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            STATCODE,
            TRANNO,
            LIFE,
            JLIFE,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            DATIME
       FROM LIFEPF
      WHERE VALIDFLAG = '1'
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            UNIQUE_NUMBER DESC;
