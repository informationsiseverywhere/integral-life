CREATE OR REPLACE FORCE EDITIONABLE VIEW "VM1DTA"."COVRTPM" 
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, CURRTO, JLIFE, STATCODE, CBCVIN, 
 PSTATCODE, STATREASN, CRRCD, ANBCCD, SEX, REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, CRINST01, CRINST02, 
 CRINST03, CRINST04, CRINST05, PRMCUR, TERMID, TRDT, TRTM, USER_T, STFUND, STSECT, STSSECT, CRTABLE, RCESDTE, PCESDTE, BCESDTE,
 NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, PCESTRM, BCESTRM, SUMINS, SICURR, VARSI, MORTCLS, LIENCD, RATCLS, INDXIN, BNUSIN, 
 DPCD, DPAMT, DPIND, TMBEN, SINGP, INSTPREM, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, 
 PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID, CBUNST, USRPRF, JOBNM, DATIME)
 AS
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            VALIDFLAG,
            TRANNO,
            CURRFROM,
            CURRTO,
            JLIFE,
            STATCODE,
            CBCVIN,
            PSTATCODE,
            STATREASN,
            CRRCD,
            ANBCCD,
            SEX,
            REPTCD01,
            REPTCD02,
            REPTCD03,
            REPTCD04,
            REPTCD05,
            REPTCD06,
            CRINST01,
            CRINST02,
            CRINST03,
            CRINST04,
            CRINST05,
            PRMCUR,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            STFUND,
            STSECT,
            STSSECT,
            CRTABLE,
            RCESDTE,
            PCESDTE,
            BCESDTE,
            NXTDTE,
            RCESAGE,
            PCESAGE,
            BCESAGE,
            RCESTRM,
            PCESTRM,
            BCESTRM,
            SUMINS,
            SICURR,
            VARSI,
            MORTCLS,
            LIENCD,
            RATCLS,
            INDXIN,
            BNUSIN,
            DPCD,
            DPAMT,
            DPIND,
            TMBEN,
            SINGP,
            INSTPREM,
            EMV01,
            EMV02,
            EMVDTE01,
            EMVDTE02,
            EMVINT01,
            EMVINT02,
            CAMPAIGN,
            STSMIN,
            RTRNYRS,
            PCAMTH,
            PCADAY,
            PCTMTH,
            PCTDAY,
            RCAMTH,
            RCADAY,
            RCTMTH,
            RCTDAY,
            JLLSID,
            CBUNST,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.COVRPF;
	    /
		COMMIT;