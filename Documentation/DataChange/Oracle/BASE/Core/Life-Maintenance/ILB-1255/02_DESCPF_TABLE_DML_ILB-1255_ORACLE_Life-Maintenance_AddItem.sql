DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCTABL='TR384' AND DESCITEM ='***BAEC' AND DESCCOY='2' AND LANGUAGE='E';  
  IF ( cnt = 0 ) THEN

Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TR384','***BAEC','  ','E',' ','Letter','Lapse Reinstatement Letter','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);  


end if;
end;
/
commit;