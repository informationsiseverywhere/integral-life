DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='TD5E6' and DESCITEM='CCI' and DESCCOY='2' and Language ='E'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TD5E6','CCI','  ','E',' ','Auto Reins','Auto Reinstatement            ','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;
