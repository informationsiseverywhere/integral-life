DECLARE 
  cnt number :=0;
BEGIN  
  SELECT COUNT(1) INTO  cnt FROM VM1DTA.HELPPF where HELPITEM = 'CINCRMIN' and HELPLANG = 'E' and HELPTYPE = 'F';
  IF(cnt > 0 ) THEN
   DELETE FROM  VM1DTA.HELPPF where HELPITEM = 'CINCRMIN' and HELPLANG = 'E' and HELPTYPE = 'F';

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','1','1','The Minimum Contribution Increase Limits are in the internal','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','2','1','Accounting Currency Any currency conversion is done before','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','3','1','edit checking.','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','4','1','The amount represents the Minimum Contribution Increase ass','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','5','1','ociated with a particular Premium Payment Frequency.','UNDERWR1  ','UNDERWR1',current_timestamp);
  
   ELSIF (cnt=0) THEN
  
  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','1','1','The Minimum Contribution Increase Limits are in the internal','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','2','1','Accounting Currency Any currency conversion is done before','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','3','1','edit checking.','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','4','1','The amount represents the Minimum Contribution Increase ass','UNDERWR1  ','UNDERWR1',current_timestamp);

   insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMIN','5','1','ociated with a particular Premium Payment Frequency.','UNDERWR1  ','UNDERWR1',current_timestamp);
  
  END IF;
END;
/
commit;

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM VM1DTA.HELPPF where HELPITEM = 'CINCRMAX' and HELPLANG = 'E' and HELPTYPE = 'F';
  IF(cnt > 0 ) THEN
  DELETE FROM  VM1DTA.HELPPF where HELPITEM = 'CINCRMAX' and HELPLANG = 'E' and HELPTYPE = 'F';

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',1,'1','The Maximum Contribution Increase Limits are in the internal','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',2,'1','Accounting Currency.Any currency conversion is done','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',3,'1','before edit checking.','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',4,'1','The amount represents the Maximum Contribution Increase ass','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',5,'1','ociated with a particular Premium Payment Frequency.','UNDERWR1  ','UNDERWR1',current_timestamp);
  
  ELSIF (cnt=0) THEN

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',1,'1','The Maximum Contribution Increase Limits are in the internal','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',2,'1','Accounting Currency.Any currency conversion is done','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',3,'1','before edit checking.','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',4,'1','The amount represents the Maximum Contribution Increase ass','UNDERWR1  ','UNDERWR1',current_timestamp);

  insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','CINCRMAX',5,'1','ociated with a particular Premium Payment Frequency.','UNDERWR1  ','UNDERWR1',current_timestamp);
  
  END IF;
END;
/
commit;
