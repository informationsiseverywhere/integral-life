DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRK6' AND ERORDESC = 'Withdrawal % > Max Withdrawal %';
IF ( cnt = 0 ) THEN
  insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME)
  values ('ER',' ','E','     ','RRK6', 'Withdrawal % > Max Withdrawal %',NULL,NULL,'','','','',CURRENT_TIMESTAMP);
END IF;
END;
/
COMMIT;

--NEXT SCRIPT

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRK7' AND ERORDESC = 'Withdrawal Amt > Max Withdrawal %';
IF ( cnt = 0 ) THEN
  insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME)
  values ('ER',' ','E','     ','RRK7', 'Withdrawal Amt > Max Withdrawal %',NULL,NULL,'','','','',CURRENT_TIMESTAMP);
END IF;
END;
/
COMMIT;


