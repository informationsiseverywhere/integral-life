DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.FLDTPF F 
  WHERE F.FDID = 'MAXWITH'  AND F.LANG = 'E';
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.FLDTPF (FDID, LANG, FDTX, COLH01, COLH02, COLH03, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME)
     VALUES ('MAXWITH', 'E', 'Maximum Withdrawal %', 'Maximum Withdrawal %', '', '', '', 12, 50408, 183103, 'UNDERWR1', 'UNDERWR1', current_timestamp);
  END IF;
END;

COMMIT;