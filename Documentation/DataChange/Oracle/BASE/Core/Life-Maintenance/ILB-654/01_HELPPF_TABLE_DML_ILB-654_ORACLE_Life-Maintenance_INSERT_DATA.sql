DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5542' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This is the percentage to be applied to the gross surrender' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5542', 'MAXWITH','1','','1','This is the percentage to be applied to the gross surrender','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
COMMIT;

--NEXT SCRIPT

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5542' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'value for calculating the maximum withdrawal amount.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5542', 'MAXWITH','2','','1','value for calculating the maximum withdrawal amount.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
COMMIT;
