DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.UTRCPF WHERE USERID = '18b' AND COMPANY= '2' AND TRANSCD= 'SBDY' AND USRPRF = 'UNDERWR1';
	IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.UTRCPF(USERID, COMPANY, TRANSCD, USRPRF, JOBNM, DATIME) VALUES ('18b', '2', 'SBDY', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;