DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRAZ  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRAZ  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRAZ  ','Agency Mgr. not on file', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRAZ  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRAZ  ','代理人经理不在文件内', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
COMMIT;


DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRBX  ';
COMMIT;
    
DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRBX  ' ;
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRBX  ','Duplicate Agent Ref Code', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRBX  ' ;
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRBX  ','代理人参考代码重复', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /

COMMIT;
