
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='EXADTXT' ; 
  IF ( cnt > 0 ) THEN
		EXECUTE IMMEDIATE (' Alter table VM1DTA.EXCLPF MODIFY EXADTXT nvarchar2(1500)  ');

  END IF;  
END;
/
commit;

