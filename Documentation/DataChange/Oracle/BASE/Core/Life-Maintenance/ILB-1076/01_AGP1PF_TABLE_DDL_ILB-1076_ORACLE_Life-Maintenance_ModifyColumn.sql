
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'AGP1PF' and column_name ='TEXTFLD' ; 
  IF ( cnt > 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.AGP1PF MODIFY TEXTFLD varchar(250) ');

  END IF;  
END;
/
commit;