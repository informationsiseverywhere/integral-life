﻿
set define off;
delete from VM1DTA.DESCPF where DESCTABL='TR2A5' and DESCITEM='CSCOM004' and DESCCOY='2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','CSCOM004','  ','E',' ','CSCOM004  ','Disallow TXN in cooling period','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','CSCOM004','  ','S',' ','CSCOM004  ','犹豫期内不允许','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;