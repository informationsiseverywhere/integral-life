
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T3695' AND DESCITEM='AP'; 
  IF ( cnt = 0 ) THEN
	insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T3695','AP','  ','E','','AP','Annuity Accrued Suspense','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
end if;
end;
/
commit;





