DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T5472' AND DESCITEM='RCL1BRFS' AND LANGUAGE='E';
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
VALUES ('IT','2','T5472','RCL1BRFS','  ','E','vdcB-j','          ','Auto Maturity                 ','RSURYA    ','RSURYA',CURRENT_TIMESTAMP);

END IF;
END;
/
COMMIT;