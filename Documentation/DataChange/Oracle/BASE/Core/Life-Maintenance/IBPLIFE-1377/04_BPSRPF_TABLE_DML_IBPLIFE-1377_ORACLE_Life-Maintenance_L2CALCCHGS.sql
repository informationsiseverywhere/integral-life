DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BBILL-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BBILL-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BBILL-FM','2','BR2YS','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BR2YS' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BR2YS' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BR2YS','2','BR2YT','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
   END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BR2YT' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BR2YT' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BR2YT','2','FMC-COVR','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='FMC-COVR' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='FMC-COVR' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','FMC-COVR','2','B5103-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5103-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5103-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5103-FM','2','B5104-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
   END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5104-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5104-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5104-FM','2','CRT-UTRX7','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-UTRX7' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-UTRX7' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','CRT-UTRX7','2','B5100FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5100FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5100FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5100FM','2','B5102C-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5102C-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5102C-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5102C-FM','2','CRT-HITX4','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-HITX4' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-HITX4' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','CRT-HITX4','2','BH508C-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BH508C-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BH508C-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BH508C-FM','2','BH509C-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BH509C-FM' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BH509C-FM' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BH509C-FM','2','BBILL-FX','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BBILL-FX' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BBILL-FX' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BBILL-FX','2','B6527-FX','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B6527-FX' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B6527-FX' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B6527-FX','2','B6528-FX','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B6528-FX' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B6528-FX' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B6528-FX','2','BBX-COVR','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BBX-COVR' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BBX-COVR' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BBX-COVR','2','B5103-FX','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5103-FX' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5103-FX' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','B5103-FX','2','B5104-FX','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5104-FX' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5104-FX' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5104-FX','2','CRT-UTRX8','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-UTRX8' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-UTRX8' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','CRT-UTRX8','2','B5100MC','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
   END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5100MC' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5100MC' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5100MC','2','B5102MC','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='B5102MC' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5102MC' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5102MC','2','CRT-HITX5','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-HITX5' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='CRT-HITX5' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','CRT-HITX5','2','BH508C-MC','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  DELETE FROM VM1DTA.BPSRPF where BPRIORPROC='BH508C-MC' AND BPRIORCOY='2';
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='BH508C-MC' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
  INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','BH508C-MC','2','BH509C-MC','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

COMMIT;