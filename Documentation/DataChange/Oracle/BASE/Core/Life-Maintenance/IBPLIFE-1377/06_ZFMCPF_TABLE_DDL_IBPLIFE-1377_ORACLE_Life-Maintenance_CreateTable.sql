DECLARE 
  cnt number := 0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'ZFMCPF' ;  
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (
			'CREATE TABLE VM1DTA.ZFMCPF(  
			UNIQUE_NUMBER NUMBER(18,0) GENERATED BY DEFAULT AS IDENTITY,
			CHDRCOY CHAR(1 CHAR) ,
			CHDRNUM CHAR(8 CHAR) ,
			VALIDFLAG CHAR(1 CHAR) ,
			LIFE CHAR(2 CHAR) ,
			JLIFE CHAR(2 CHAR) ,
			COVERAGE CHAR(2 CHAR) ,
			RIDER CHAR(2 CHAR) ,
			PLNSFX NUMBER(1) ,
			PRMCUR CHAR(3 CHAR) ,
			ZFMCDAT NUMBER(8) ,
			SUMINS NUMBER(17,2) ,
			PCESDTE NUMBER(8) ,
			CRTABLE CHAR(4 CHAR) ,
			MORTCLS CHAR(1 CHAR) ,
			ZNOFDUE NUMBER(8) ,
			TRANNO NUMBER(8) ,
			USRPRF CHAR(10 CHAR),
			JOBNME CHAR(10 CHAR),
			DATIME TIMESTAMP(6),
			JOBNM CHAR(10 CHAR),
			CONSTRAINT PK_ZFMCPF PRIMARY KEY (UNIQUE_NUMBER)
		)'
	);
	EXECUTE IMMEDIATE ('COMMENT ON TABLE ZFMCPF IS ''Fund Management Charges''');
END IF;  
END;
/

COMMIT;