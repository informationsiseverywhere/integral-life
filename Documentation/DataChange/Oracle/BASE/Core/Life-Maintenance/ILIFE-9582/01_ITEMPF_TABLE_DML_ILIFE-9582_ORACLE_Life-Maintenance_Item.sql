
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ITEMPF WHERE  ITEMPFX='IT' AND ITEMCOY='2' AND ITEMTABL='T1675' AND ITEMITEM='T6725318';
  IF ( cnt > 0 ) THEN 
	UPDATE VM1DTA.ITEMPF SET GENAREA='202020202020202041422020202020202020202020202050353031365044354A4C20' WHERE ITEMPFX='IT' AND ITEMCOY='2' AND ITEMTABL='T1675' AND ITEMITEM='T6725318';
  END IF;
END;
/
COMMIT;