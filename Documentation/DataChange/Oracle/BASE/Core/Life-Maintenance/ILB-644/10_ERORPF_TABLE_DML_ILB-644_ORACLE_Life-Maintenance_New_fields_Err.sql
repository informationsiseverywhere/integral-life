
SET DEFINE OFF;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRBN  ';
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAW  ';
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAX  ';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRBN  ','Must enter Self',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRAW  ','Must be greater than Birth Date',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRAX  ','Not defined in TD5G2',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
	SELECT count(1) INTO cnt 
	FROM VM1DTA.ERORPF 
	where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAV  ';
	IF ( cnt = 1 ) THEN
		delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAV  ';
	end if;
end;
/
commit;