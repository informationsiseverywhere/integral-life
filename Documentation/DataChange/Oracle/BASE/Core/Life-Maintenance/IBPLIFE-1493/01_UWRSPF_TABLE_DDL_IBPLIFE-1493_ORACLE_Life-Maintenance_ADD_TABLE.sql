BEGIN
  
    EXECUTE IMMEDIATE 
    ('CREATE TABLE "VM1DTA"."UWRSPF"(
    "UNIQUE_NUMBER" NUMBER(18,0),
    "CHDRNUM" CHAR(8 CHAR),
	"CHDRCOY" CHAR(1 CHAR),
	"REASON" CHAR(100 CHAR),	
	"TRANNO" NUMBER(5,0),	
	"TRANCDE"  CHAR(4 CHAR),       
    "TRDT"  NUMBER(6,0),     
    "TRTM"  NUMBER(6,0), 
	"USER_T" NUMBER(8,0),
    "CRTABLE" CHAR(4 CHAR),
    CONSTRAINT PK_UWRSPF PRIMARY KEY(UNIQUE_NUMBER))');
    
    EXECUTE IMMEDIATE 
  ('CREATE SEQUENCE  "VM1DTA"."SEQ_UWRSPF"  MINVALUE 1 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 1' );
    
    EXECUTE IMMEDIATE 
  ('create or replace TRIGGER "VM1DTA"."TR_UWRSPF" before insert on UWRSPF for each row declare 
v_pkValue  number;      begin      select SEQ_UWRSPF.nextval into v_pkValue from dual;      :New.unique_number := v_pkValue;    end TR_UWRSPF;' );
    
    EXECUTE IMMEDIATE 
  ('ALTER TRIGGER "VM1DTA"."TR_UWRSPF" ENABLE');    
    
END;
/
