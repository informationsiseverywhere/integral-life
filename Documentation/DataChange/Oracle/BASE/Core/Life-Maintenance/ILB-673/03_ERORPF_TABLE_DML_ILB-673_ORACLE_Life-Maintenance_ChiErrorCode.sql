DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDF%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDF  ','代理没有必要的许可证','0','0','0',' ','  ','  ','   ',current_timestamp); 
	
end if;
end;
/
commit;




DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDD%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDD  ','银行网点没有必要的许可证','0','0','0',' ','  ','  ','   ',current_timestamp); 
	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRD9%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	 INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	 VALUES('ER',' ','S','          ','RRD9  ','银行服务经理没有必要的许可证','0','0','0',' ','  ','  ','   ',current_timestamp); 
	
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDA%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDA  ','银行柜员没有必要的许可证','0','0','0',' ','  ','  ','   ',current_timestamp); 
	  
	
end if;
end;
/
commit;
  

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDE%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
 VALUES('ER',' ','S','          ','RRDE  ', ' 银行网点许可证过期','0','0','0',' ','  ','  ','   ',current_timestamp);  
	  
	
end if;
end;
/
commit;
  

 
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDB%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDB  ', ' 银行服务经理许可证过期','0','0','0',' ','  ','  ','   ',current_timestamp); 	
	  
	
end if;
end;
/
commit;
  
 


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF 
  where EROREROR LIKE 'RRDC%' AND ERORLANG='S';
  IF ( cnt = 0 ) THEN
  
	INSERT INTO ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDC  ', '银行网点许可证过期','0','0','0',' ','  ','  ','   ',current_timestamp);
	  
	
end if;
end;
/
commit;
  


 

