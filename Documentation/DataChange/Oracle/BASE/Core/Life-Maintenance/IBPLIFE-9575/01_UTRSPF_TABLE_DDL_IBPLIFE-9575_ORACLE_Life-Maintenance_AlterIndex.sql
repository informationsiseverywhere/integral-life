DROP INDEX UTRSMAT;
DROP INDEX UTRSCLM;
DROP INDEX UTRSDCC;
DROP INDEX UTRSBRK;

--------------------------------------------------------
--  DDL for Index UTRSBRK
--------------------------------------------------------

  CREATE INDEX "UTRSBRK" ON "UTRSPF" ("CHDRCOY", "CHDRNUM", "COVERAGE", "RIDER", "PLNSFX", "LIFE", "VRTFND", "UNITYP", "UNIQUE_NUMBER" DESC);
--------------------------------------------------------
--  DDL for Index UTRSCLM
--------------------------------------------------------

  CREATE INDEX "UTRSCLM" ON "UTRSPF" ("CHDRCOY", "CHDRNUM", "PLNSFX", "LIFE", "COVERAGE", "RIDER", "VRTFND", "UNITYP", "UNIQUE_NUMBER" DESC);
--------------------------------------------------------
--  DDL for Index UTRSDCC
--------------------------------------------------------

  CREATE INDEX "UTRSDCC" ON "UTRSPF" ("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "VRTFND", "UNITYP", "PLNSFX", "UNIQUE_NUMBER" DESC);
--------------------------------------------------------
--  DDL for Index UTRSMAT
--------------------------------------------------------

  CREATE INDEX "UTRSMAT" ON "UTRSPF" ("CHDRCOY", "CHDRNUM", "COVERAGE", "RIDER", "VRTFND", "UNITYP", "PLNSFX", "LIFE", "UNIQUE_NUMBER" DESC); 