DELETE FROM VM1DTA.ERORPF where EROREROR LIKE 'RRFP%' AND ERORLANG='E';
COMMIT;

 DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRFP  ';
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
VALUES('ER',' ','E','          ','RRFP  ','Payment method not allowed','0','0','0',' ','  ','  ','   ',current_timestamp); 
  END IF;
  END;
  /
 COMMIT; 
  
DELETE FROM VM1DTA.ERORPF where EROREROR LIKE 'RRFP%' AND ERORLANG='S';
COMMIT;

 DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRFP  ';
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
VALUES('ER',' ','S','          ','RRFP  ','Payment method not allowed','0','0','0',' ','  ','  ','   ',current_timestamp); 
  END IF;
  END;
  /
 COMMIT; 


 DELETE FROM VM1DTA.ERORPF where EROREROR LIKE 'RRHF%' AND ERORLANG='E';
 COMMIT;
 
  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRHF  ';
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
VALUES('ER',' ','E','          ','RRHF  ','Item not on TD5H7','0','0','0',' ','  ','  ','   ',current_timestamp); 
  END IF;
  END;
  /
 COMMIT; 
  
DELETE FROM VM1DTA.ERORPF where EROREROR LIKE 'RRHF%' AND ERORLANG='S';
 COMMIT;
 
  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRHF  ';
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
VALUES('ER',' ','S','          ','RRHF  ','Item not on TD5H7','0','0','0',' ','  ','  ','   ',current_timestamp); 
  END IF;
  END;
  /
 COMMIT; 
  

 

