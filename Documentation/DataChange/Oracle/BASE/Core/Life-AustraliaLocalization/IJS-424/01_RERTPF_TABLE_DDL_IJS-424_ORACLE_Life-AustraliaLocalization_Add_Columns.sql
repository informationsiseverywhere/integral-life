DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RERTPF' and column_name ='TRANNO' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.RERTPF ADD TRANNO INT ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.RERTPF.TRANNO IS ''TRANNO''' ); 
  END IF;     
END;
/ 



