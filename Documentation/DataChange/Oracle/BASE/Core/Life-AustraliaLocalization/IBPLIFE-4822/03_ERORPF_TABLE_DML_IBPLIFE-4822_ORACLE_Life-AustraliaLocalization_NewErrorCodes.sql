DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND EROREROR = 'RUPM';
  IF ( cnt = 0 ) THEN
   INSERT INTO VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 VALUES ( 'ER', '', 'E','          ','RUPM  ','Billing Day change not allowed', '', '', '', '', 'UNDERWR1', 'UNDERWR1',CURRENT_TIMESTAMP,'');
  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND EROREROR = 'RUPN';
  IF ( cnt = 0 ) THEN
   INSERT INTO VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 VALUES ( 'ER', '', 'E','          ','RUPN  ','Billing record generated', '', '', '', '', 'UNDERWR1', 'UNDERWR1',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND EROREROR = 'RUPL';
  IF ( cnt = 0 ) THEN
   INSERT INTO VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 VALUES ( 'ER', '', 'E','          ','RUPL  ','Temp Change field is required', '', '', '', '', 'UNDERWR1', 'UNDERWR1',CURRENT_TIMESTAMP,'');
  END IF;
END;
/


commit;










