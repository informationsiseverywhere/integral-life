DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'PAYRPF' and column_name ='ORGBILLCD' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE PAYRPF ADD ORGBILLCD INT ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN PAYRPF.ORGBILLCD IS ''Orignal billing renewal date ''');
  END IF;  
END;
/
