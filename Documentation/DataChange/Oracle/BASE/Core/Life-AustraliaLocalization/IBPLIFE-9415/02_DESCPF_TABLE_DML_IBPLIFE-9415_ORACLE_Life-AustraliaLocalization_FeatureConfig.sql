
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TR2A5' AND DESCITEM='CSLRI008' AND LANGUAGE='E'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','TR2A5','CSLRI008','','E','','CSLRI008','Adjust Reassurance','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END IF;
END;
/
COMMIT;