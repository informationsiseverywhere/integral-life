
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVRPF' and column_name ='COMMPREM' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.COVRPF ADD COMMPREM NUMBER(17, 2)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN COVRPF.COMMPREM IS ''Commission Premium''');
  END IF;  
END;
/


Commit;