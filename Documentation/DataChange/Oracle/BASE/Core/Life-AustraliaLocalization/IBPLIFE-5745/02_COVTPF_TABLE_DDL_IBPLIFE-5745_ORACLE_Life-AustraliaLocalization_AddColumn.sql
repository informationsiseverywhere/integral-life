DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVTPF' and column_name ='COMMPREM' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.COVTPF ADD COMMPREM NUMBER(17,2)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN COVTPF.COMMPREM IS ''Used for Commission''');
  END IF;  
END;
/

Commit;