DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LINSPF' and column_name ='PRORAMT' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE LINSPF ADD PRORAMT NUMBER(17,2) ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN LINSPF.PRORAMT IS ''prorate amount ''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LINSPF' and column_name ='PRORCNTFEE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE LINSPF ADD PRORCNTFEE NUMBER(17,2) ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN LINSPF.PRORCNTFEE IS ''prorate cntfee ''');
  END IF;  
END;
/