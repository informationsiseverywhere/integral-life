DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVRPF' and column_name ='REINSTATED' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE COVRPF ADD REINSTATED CHAR(1 CHAR) ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN COVRPF.REINSTATED IS ''reinstate flag ''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVRPF' and column_name ='PRORATEPREM' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE COVRPF ADD PRORATEPREM NUMBER(17,2) ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN COVRPF.PRORATEPREM IS ''prorated premium ''');
  END IF;  
END;
/