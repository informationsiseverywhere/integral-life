DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='VALIDFLAG' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD VALIDFLAG nchar(1) ');
	END IF;  
	SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='TRANNO' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD TRANNO int ');
	
  END IF;  
END;
/ 