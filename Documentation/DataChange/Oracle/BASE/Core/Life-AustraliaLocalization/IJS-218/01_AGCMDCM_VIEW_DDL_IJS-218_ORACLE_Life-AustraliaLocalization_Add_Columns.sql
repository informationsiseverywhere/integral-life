


  CREATE OR REPLACE VIEW "VM1DTA"."AGCMDMN" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "TRANNO", "AGNTNUM", "EFDATE", "ANNPREM", "BASCMETH", "INITCOM", "BASCPY", "COMPAY", "COMERN", "SRVCPY", "SCMDUE", "SCMEARN", "RNWCPY", "RNLCDUE", "RNLCEARN", "AGCLS", "TERMID", "TRDT", "TRTM", "USER_T", "VALIDFLAG", "CURRFROM", "CURRTO", "PTDATE", "SEQNO", "CEDAGENT", "OVRDCAT", "DORMFLAG","INITCOMMGST", "USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            TRANNO,
            AGNTNUM,
            EFDATE,
            ANNPREM,
            BASCMETH,
            INITCOM,
            BASCPY,
            COMPAY,
            COMERN,
            SRVCPY,
            SCMDUE,
            SCMEARN,
            RNWCPY,
            RNLCDUE,
            RNLCEARN,
            AGCLS,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            VALIDFLAG,
            CURRFROM,
            CURRTO,
            PTDATE,
            SEQNO,
            CEDAGENT,
            OVRDCAT,
            DORMFLAG,
			INITCOMMGST,
            USRPRF,
            JOBNM,
            DATIME
       FROM AGCMPF
      WHERE VALIDFLAG = '1' AND DORMFLAG <> 'Y'
   ORDER BY CHDRCOY,
            CHDRNUM,
            AGNTNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 ;

