DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5353DATA' and column_name ='PAYORGBILLCD' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE B5349DATA ADD PAYORGBILLCD INT ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN B5349DATA.PAYORGBILLCD IS ''Orignal billing renewal date ''');
  END IF;  
END;
/


