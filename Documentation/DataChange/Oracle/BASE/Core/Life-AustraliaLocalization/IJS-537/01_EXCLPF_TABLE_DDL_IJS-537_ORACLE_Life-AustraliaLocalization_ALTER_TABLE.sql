DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='LIFE' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD LIFE nchar(2) ');
	END IF; 
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='COVERAGE' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD COVERAGE nchar(2) ');
	END IF; 
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='RIDER' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD RIDER nchar(2) ');
	END IF; 
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'EXCLPF' and column_name ='PLNSFX' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE EXCLPF ADD PLNSFX int ');
	
  END IF;  
END;
/  