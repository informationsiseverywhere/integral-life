
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'SURDPF' and column_name ='COMMCLAW' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.SURDPF ADD COMMCLAW NUMBER(17,2) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.SURDPF.COMMCLAW IS ''COMMCLAW''' ); 
  END IF;     
END;
/ 