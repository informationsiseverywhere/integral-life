DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='HE' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM=' ' and ITEMSEQ='  ' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('HE','2','TJL67',' ','  ','E','','IT','Record Types','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='26' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','26','  ','E','','26','NB Proposal','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='24' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','24','  ','E','','24','Contract Issue','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='29' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','29','  ','E','','29','Component Change Proposal','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='33' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','33','  ','E','','33','Add Rider/Increase SA approval','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='22' and LANGUAGE='E';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','22','  ','E','','22','Decrease SA / Termination','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;


DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='HE' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM=' ' and ITEMSEQ='  ' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('HE','2','TJL67',' ','  ','J','','IT','レコード種別','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='26' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','26','  ','J','','26','新契約申込回答','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='24' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','24','  ','J','','24','成立収録回答','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='29' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','29','  ','J','','29','保全申込回答','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;

DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='33' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','33','  ','J','','33','保全成立・不成立収録回答','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;


DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TJL67' and DESCITEM='22' and LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
		values ('IT','2','TJL67','22','  ','J','','22','登録内容変更回答','UNDERWR1','UNDERWR1',current_timestamp);
	END IF;
END;
/
commit;