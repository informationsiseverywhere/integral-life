DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BSCDPF WHERE bauthcode = 'B254' and PRODCODE ='L' and BSCHEDNAM ='L2DDAPLYJP';
  IF ( cnt = 0 ) THEN
	Insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) values ('L2DDAPLYJP',1,'N','N','B254','QBATCH    ','5','PAXUS     ','L','UNDERWR1','UNDERWR1  ',CURRENT_TIMESTAMP,'Y');
END IF;
END;
/

COMMIT;