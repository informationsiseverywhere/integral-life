
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR = 'RUKY';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUKY','Payment method not allowed', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR = 'RUKZ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUKZ','Risk option not allowed for channel', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR = 'RUKY';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUKY','1P-Cashlessの販売経路は口座振替のみ選択可', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR = 'RUKZ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUKZ','責任開始期に関する特約は1P-Cashlessの販売経路の選択不可', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/


commit;







