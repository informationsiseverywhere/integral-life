DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='TR2A5' and DESCITEM='NBPRP117' and DESCCOY='2' and Language ='E'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TR2A5','NBPRP117','  ','E',' ','NBPRP117  ','2JP Anti-Social Groups','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

/* Create SubMenu Item  in T1690 */
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1690' and DESCITEM='PJL34' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1690','PJL34','  ','E',' ','Life','LINC Anti-Social','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

/* ADD desc in T1688 */
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='TAS3' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','TAS3','  ','E',' ','Life','LINC Anti-Social Update','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='TAS4' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','TAS4','  ','E',' ','Life','LINC Anti-Social Enquire','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='SAS5' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','SAS5','  ','E',' ','Life','LINC Anti-Social','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZBN' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZBN','  ','J',' ','JZBN      ','Anti-Social BN                ','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZLF' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZLF','  ','J',' ','JZLF','Anti-Social LF','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZOW' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZOW','  ','J',' ','JZOW','Anti-Social PO','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZBN' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZBN','  ','E',' ','JZBN      ','Anti-Social BN                ','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5661' and DESCITEM='JZLF' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZLF','  ','E',' ','JZLF','Anti-Social LF','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5661' and DESCITEM='JZOW' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZOW','  ','E',' ','JZOW','Anti-Social PO','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='        ' and LANGUAGE = 'E' and DESCCOY='2' AND DESCPFX='HE'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('HE','2','TJL37','        ','  ','E',' ','          ','Gender Mapping','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='女' and LANGUAGE = 'E' and DESCCOY='2'  AND DESCPFX='IF'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','女','  ','E',' ','F','Female','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='男' and LANGUAGE = 'E' and DESCCOY='2'  AND DESCPFX='IF' ; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','男','  ','E',' ','M','Male','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='不明' and LANGUAGE = 'E' and DESCCOY='2'  AND DESCPFX='IF'  ; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','不明','  ','E',' ','U','Unknown','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5660' and DESCITEM='S' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T5660','S','  ','E',' ','Reviewed  ','Status Reviewed','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5661' and DESCITEM='EZBN' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T5661','EZBN','  ','E',' ','EZBN','Anti-Social BN','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5661' and DESCITEM='EZLF' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T5661','EZLF','  ','E',' ','EZLF','Anti-Social LF','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5661' and DESCITEM='EZOW' and LANGUAGE = 'E' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T5661','EZOW','  ','E',' ','EZOW','Anti-Social PO','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

/*JAPAN Codes */
/* Create SubMenu Item  in T1690 */
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1690' and DESCITEM='PJL34' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1690','PJL34','  ','J',' ','Life','LINC反社情報','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

/* ADD desc in T1688 */
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='TAS3' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','TAS3','  ','J',' ','Life','LINC反社情報　更新','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='TAS4' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','TAS4','  ','J',' ','Life','LINC反社情報　照会','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T1688' and DESCITEM='SAS5' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T1688','SAS5','  ','J',' ','Life','LINC反社情報','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZBN' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZBN','  ','J',' ','JZBN      ','反社会勢力該当あり（受取人）','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZLF' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZLF','  ','J',' ','JZLF','反社会勢力該当あり（被保険者）','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='T5661' and DESCITEM='JZOW' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','T5661','JZOW','  ','J',' ','JZOW','反社会勢力該当あり（契約者）','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='        ' and LANGUAGE = 'J' and DESCCOY='2' AND DESCPFX='HE'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('HE','2','TJL37','        ','  ','J',' ','          ','性別テーブル','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='女' and LANGUAGE = 'J' and DESCCOY='2'  AND DESCPFX='IF'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','女','  ','J',' ','F','女性','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='男' and LANGUAGE = 'J' and DESCCOY='2'  AND DESCPFX='IF' ; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','男','  ','J',' ','M','男性','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='TJL37' and DESCITEM='不明' and LANGUAGE = 'J' and DESCCOY='2'  AND DESCPFX='IF'  ; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TJL37','不明','  ','J',' ','U','不明','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  WHERE DESCTABL='T5660' and DESCITEM='S' and LANGUAGE = 'J' and DESCCOY='2'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T5660','S','  ','J',' ','Reviewed  ','状況確認','UNDERWR1  ','UNDERWR1',current_timestamp);

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCTABL='TR2A5' and DESCITEM='NBPRP117' and DESCCOY='2' and Language ='J'; 
  IF ( cnt = 0 ) THEN
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TR2A5','NBPRP117','  ','J',' ','NBPRP117  ','反社会勢力（２ＪＰ）','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;