DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.FLDDPF  	
  WHERE FDID = 'KANJINAME';
   
  IF ( cnt = 0 ) THEN
insert INTO VM1DTA.FLDDPF(FDID, NMPG, TYPE_T, EDIT, LENF, DECP, WNTP, WNST, TABW, TCOY, ERRCD, PROG01, PROG02, PROG03, PROG04, CFID, TERMID, 
USER_T, TRDT, TRTM, ADDF01, ADDF02, ADDF03, ADDF04, CURRFDID, ALLOCLEN, DBDATIME, SFDATIME, DBCSCAP, USRPRF, JOBNM, DATIME, TYPE) 
VALUES('KANJINAME ', 'KANJINAME               ', 'A', ' ', 30, 0, 'W', 'G', '     ', ' ', '    ', 'PJL35', ' ', ' ', ' ', ' ',' ', 0, 
0, 0, ' ', ' ', ' ', ' ', ' ', 0, sysdate, sysdate,'N', 'LHAMMOND', 'DSP14', sysdate, ' ');

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.FLDDPF  	
  WHERE FDID = 'ANTISOCNO';
   
  IF ( cnt = 0 ) THEN
insert INTO VM1DTA.FLDDPF(FDID, NMPG, TYPE_T, EDIT, LENF, DECP, WNTP, WNST, TABW, TCOY, ERRCD, PROG01, PROG02, PROG03, PROG04, CFID, TERMID, 
USER_T, TRDT, TRTM, ADDF01, ADDF02, ADDF03, ADDF04, CURRFDID, ALLOCLEN, DBDATIME, SFDATIME, DBCSCAP, USRPRF, JOBNM, DATIME, TYPE) 
VALUES('ANTISOCNO ', 'ANTISOCNO               ', 'A', ' ', 8, 0, 'W', 'G', '     ', ' ', '    ', 'PJL35', ' ', ' ', ' ', ' ',' ', 0, 
0, 0, ' ', ' ', ' ', ' ', ' ', 0, sysdate, sysdate,'N', 'LHAMMOND', 'DSP14', sysdate, ' ');

end if;
end;
/
commit;