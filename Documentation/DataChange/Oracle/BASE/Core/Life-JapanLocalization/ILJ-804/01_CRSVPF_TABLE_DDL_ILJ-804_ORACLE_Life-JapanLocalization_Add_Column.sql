DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CRSVPF' and column_name ='PAYDATE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE CRSVPF ADD PAYDATE int ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN CRSVPF.PAYDATE IS '' PAY DATE''');
  END IF;  
END;
/
Commit;