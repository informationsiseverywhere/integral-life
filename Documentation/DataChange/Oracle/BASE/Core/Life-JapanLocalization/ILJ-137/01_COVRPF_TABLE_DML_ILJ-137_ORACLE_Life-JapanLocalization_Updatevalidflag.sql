

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00005025';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00005025';
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00005288';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00005288';
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00007037';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00007037';
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00007463';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00007463';
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00021820';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00021820';
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.COVRPF e WHERE e.chdrnum = '00021848';
  IF ( cnt > 0 ) THEN
  UPDATE vm1dta.covrpf SET validflag = '2' WHERE chdrnum = '00021848';
END IF;
END;
/

commit;