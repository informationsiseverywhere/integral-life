DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='ZCLAIMER' ; 
  IF ( cnt > 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF MODIFY ZCLAIMER CHAR(8 CHAR)');
  END IF;  
END;
/
commit;
