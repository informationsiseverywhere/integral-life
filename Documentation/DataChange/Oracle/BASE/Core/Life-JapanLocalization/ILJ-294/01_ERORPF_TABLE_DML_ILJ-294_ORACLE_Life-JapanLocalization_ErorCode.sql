DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
 where ERORPFX='ER' AND ERORLANG='J' AND EROREROR = 'JL25  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ('ER', ' ', 'J','          ','JL25  ','契約をリバースし再成立する必要があります。', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/
commit;