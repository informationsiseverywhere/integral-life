
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CHDRPF' and column_name ='CONCOMMFLG' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CHDRPF ADD CONCOMMFLG NCHAR(1) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CHDRPF.CONCOMMFLG IS ''Contract Commencement Flag ''');
  END IF;  
END;
/
