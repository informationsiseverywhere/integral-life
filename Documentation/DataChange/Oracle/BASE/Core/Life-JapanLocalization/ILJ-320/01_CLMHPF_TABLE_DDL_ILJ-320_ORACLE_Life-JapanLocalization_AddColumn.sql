
DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='CLAMVAL' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD CLAMVAL NUMBER(17, 2)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.CLAMVAL IS ''Claim Value''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='CLAIM' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD CLAIM CHAR(9 CHAR)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.CLAIM IS ''Claim No''');
  END IF;  
END;
/


DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='KCAUSE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD KCAUSE CHAR(2 CHAR)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.KCAUSE IS ''Cause of death for Death Claims''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='ZASSESOR' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD ZASSESOR CHAR(4 CHAR)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.ZASSESOR IS ''Cause of death for Death Claims''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='ZCLAIMER' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD ZCLAIMER CHAR(4 CHAR)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.ZCLAIMER IS ''Used for Death Claims''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='WUEPAMT' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD WUEPAMT CHAR(4 CHAR)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.WUEPAMT IS ''Used for Death Claims''');
  END IF;  
END;
/

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CLMHPF' and column_name ='CONDTE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CLMHPF ADD CONDTE NUMBER(8)');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CLMHPF.CONDTE IS ''Used for Death Claims''');
  END IF;  
END;
/

Commit;