﻿DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGN'  AND LANGUAGE='J'; 
  IF ( cnt = 1 ) THEN
  DELETE FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGN'  AND LANGUAGE='J'; 
  END IF;
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGN'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T1688','BBGN','  ','J','','Life','保険料請求（口振1P）','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGO'  AND LANGUAGE='J';
  IF ( cnt = 1 ) THEN
  DELETE FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGO'  AND LANGUAGE='J';
  END IF;
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGO'  AND LANGUAGE='J';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T1688','BBGO','  ','J','','Life','保険料収納（口振1P）','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGP'  AND LANGUAGE='J'; 
  IF ( cnt = 1 ) THEN
  DELETE FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGP'  AND LANGUAGE='J'; 
  END IF;
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='BBGP'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T1688','BBGP','  ','J','','Life','口振抽出対象','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='TBGD'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T1688','TBGD','  ','J','','Life','仮承諾','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T1688' AND DESCPFX='IT' AND DESCITEM='TBGE'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T1688','TBGE','  ','J','','Life','仮承諾リバース','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T3695' AND DESCPFX='IT' AND DESCITEM='SK'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T3695','SK','  ','J','','IPContSusp','契約仮受金（1P）','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T3623' AND DESCPFX='IT' AND DESCITEM='SI'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T3623','SI','  ','J','','Sub Issue','仮承諾','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T5682' AND DESCPFX='IT' AND DESCITEM='SI'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T5682','SI','  ','J','','Sub Issue','仮承諾','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCCOY='2' AND DESCTABL='T5681' AND DESCPFX='IT' AND DESCITEM='NR'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T5681','NR','  ','J','','Not Recv','1P未入金','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/
commit;