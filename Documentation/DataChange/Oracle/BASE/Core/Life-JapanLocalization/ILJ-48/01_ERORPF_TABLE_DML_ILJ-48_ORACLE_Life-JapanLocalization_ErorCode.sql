
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR like 'JL25%';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL25  ','Contract should be reversed and reissue', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/
commit;


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR like 'JL26%';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL26  ','Date of Death < RCD', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/
commit;
  
   