DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF
  WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'RFL8'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
VALUES ('ER',' ','J','','RFL8','TR52Z 受取人タイプなし','' ,'','','' ,'UNDERWR1' ,'UNDERWR1' ,CURRENT_TIMESTAMP,' ');


  else 	
   UPDATE VM1DTA.ERORPF SET ERORDESC='TR52Z 受取人タイプなし'
WHERE  ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'RFL8';

end if;
end;
/
commit;
