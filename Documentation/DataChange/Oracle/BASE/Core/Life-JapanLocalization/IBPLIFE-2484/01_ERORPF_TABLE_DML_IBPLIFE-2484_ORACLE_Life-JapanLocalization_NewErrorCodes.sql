﻿DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR = 'RULV';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RULV','Sacs Type required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR = 'RULW';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RULW','Sacs Code required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR = 'RULV';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RULV','サブアカウントタイプ入力必要', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR = 'RULW';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RULW','サブアカウントコード入力必要', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

commit;