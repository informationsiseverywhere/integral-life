DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5348' AND BPRIORCOY='2';
  IF ( cnt > 0 ) THEN
	UPDATE VM1DTA.BPSRPF SET BSUBSEQPRC='B5349J' where BPRIORPROC='B5348' and BPRIORCOY='2';
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5349J' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5349J','2','B5349','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5352' AND BPRIORCOY='2';
  IF ( cnt > 0 ) THEN
	UPDATE VM1DTA.BPSRPF SET BSUBSEQPRC='B5353J' where BPRIORPROC='B5352' and BPRIORCOY='2';
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPSRPF where BPRIORPROC='B5353J' AND BPRIORCOY='2';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.BPSRPF ( BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ( '2','B5353J','2','B5353','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

COMMIT;