DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5349DATA' and column_name ='RCOPT' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.B5349DATA ADD RCOPT NCHAR(1) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN B5349DATA.RCOPT IS ''Risk Commencement Option''');
  END IF;  
END;
/
commit;

DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5349DATA' and column_name ='SRCEBUS' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.B5349DATA ADD SRCEBUS NCHAR(2) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN B5349DATA.SRCEBUS IS ''Source of Business''');
  END IF;  
END;
/
commit;


