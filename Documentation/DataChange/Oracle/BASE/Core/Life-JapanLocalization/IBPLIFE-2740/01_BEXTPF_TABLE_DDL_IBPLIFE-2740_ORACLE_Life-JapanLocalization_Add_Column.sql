DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BEXTPF' and column_name ='BANKACTYPE' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.BEXTPF ADD BANKACTYPE NUMBER(1) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN BEXTPF.BANKACTYPE IS ''Bank Account Type''');
  END IF;  
END;
/
commit;

