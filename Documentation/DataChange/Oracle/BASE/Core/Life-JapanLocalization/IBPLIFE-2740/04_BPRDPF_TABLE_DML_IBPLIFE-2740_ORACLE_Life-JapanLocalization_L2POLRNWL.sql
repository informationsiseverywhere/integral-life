DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPRDPF WHERE BPROCESNAM='B5349J' AND COMPANY='2';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.BPRDPF ( COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,
	BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,
	BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('2','B5349J','00:00:00','00:00 ','B5349',
	null,1,1,'P6671','        ','BBGN','N','10',50,50,5000,'    ','          ','          ','BL','JPN',10,'*DATABASE ','3','3',0,
	' ',' ','L','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.BPRDPF WHERE BPROCESNAM='B5353J' AND COMPANY='2';
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.BPRDPF ( COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,
	BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,
	BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('2','B5353J','00:00:00','00:00 ','B5353',
	null,1,1,'P6671','        ','BBGO','N','10',50,50,5000,'    ','          ','LA','CO','JPN',10,'*DATABASE ','3','3',0,
	' ',' ','L','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
  END IF;
END;
/


COMMIT;