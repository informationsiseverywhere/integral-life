DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND ERORCOY=' ' AND EROREROR = 'JL81';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','','JL81','value of 1 to 100 is required', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND ERORCOY=' ' AND EROREROR = 'JL82';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','','JL82', 'Total Payment Amount not match', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='E' AND ERORCOY=' ' AND EROREROR = 'JL80';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','','JL80', 'Payee is required', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;