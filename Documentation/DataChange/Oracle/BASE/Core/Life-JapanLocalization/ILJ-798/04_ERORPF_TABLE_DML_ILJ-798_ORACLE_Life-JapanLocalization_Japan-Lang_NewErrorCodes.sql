DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='J' AND ERORCOY=' ' AND EROREROR = 'JL81';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','','JL81','1〜100の値が必要です', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='J' AND ERORCOY=' ' AND EROREROR = 'JL82';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','','JL82','合計支払金額が一致しません', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf 
  WHERE  ERORPFX = 'ER' AND ERORLANG ='J' AND ERORCOY=' ' AND EROREROR = 'JL80';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','','JL80','支払者が必要です', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/
Commit;