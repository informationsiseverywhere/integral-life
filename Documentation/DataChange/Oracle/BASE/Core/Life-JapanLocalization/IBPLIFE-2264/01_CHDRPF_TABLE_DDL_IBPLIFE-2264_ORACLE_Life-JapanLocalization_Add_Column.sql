DECLARE 
  cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'CHDRPF' and column_name ='RCOPT' ; 
  IF ( cnt = 0 ) THEN
		EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.CHDRPF ADD RCOPT NCHAR(1) NULL ');
		EXECUTE IMMEDIATE ('COMMENT ON COLUMN CHDRPF.RCOPT IS ''Risk Commencement Option''');
  END IF;  
END;
/
commit;

