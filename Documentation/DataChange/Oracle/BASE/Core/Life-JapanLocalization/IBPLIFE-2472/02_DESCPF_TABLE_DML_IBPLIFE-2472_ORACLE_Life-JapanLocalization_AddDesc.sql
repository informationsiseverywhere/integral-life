
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T6661' AND DESCITEM='TBGD'  AND LANGUAGE='E'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T6661','TBGD','  ','E','','TentAppl','Tentative Approval','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T5679' AND DESCITEM='TBGE'  AND LANGUAGE='E'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T5679','TBGE','  ','E','','TentAppR','Tentative Approval Reversal','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T6661' AND DESCITEM='TBGD'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T6661','TBGD','  ','J','','TentAppl','仮承諾','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt FROM VM1DTA.DESCPF where DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T5679' AND DESCITEM='TBGE'  AND LANGUAGE='J'; 
  IF ( cnt = 0 ) THEN
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','T5679','TBGE','  ','J','','TentAppR','仮承諾リバース','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
END IF;
END;
/
commit;