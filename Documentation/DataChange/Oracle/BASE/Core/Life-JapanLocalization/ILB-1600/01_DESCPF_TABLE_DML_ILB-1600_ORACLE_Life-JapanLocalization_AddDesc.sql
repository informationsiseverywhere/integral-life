DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(1) into cnt 
	FROM VM1DTA.DESCPF
	WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T1660' AND DESCITEM='PL      ' AND LANGUAGE='J';
	IF ( cnt > 0 ) THEN
		UPDATE VM1DTA.DESCPF SET SHORTDESC='Pcode look', LONGDESC=N'郵便番号検索' WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T1660' AND DESCITEM='PL      ' AND LANGUAGE='J';
	END IF;
END;
/
commit;