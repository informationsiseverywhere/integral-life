
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL33  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL33  ','Contract Date < Effective Date in T5688', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL29  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL29  ','Declaration Date + 30 days < Contract Date', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL30  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL30  ','Date of Confirmation Intent > Proposal Date', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL31  ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL31  ','Decimal Places invalid', '', '', '', '', 'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,'  ');
END IF;
END;
/
commit;




