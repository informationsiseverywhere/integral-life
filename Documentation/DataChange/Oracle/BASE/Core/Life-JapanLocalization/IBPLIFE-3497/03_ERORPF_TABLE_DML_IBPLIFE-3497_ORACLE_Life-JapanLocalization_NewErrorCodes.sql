DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUML' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUML','Effective Date before the Policy loan date', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUML' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUML','効力発生日が契約者貸付日より前です', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMM' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMM','Overpaid Premiums Present, Reversal required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMM' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMM','過払保険料が有るため、リバーサルが必要です。', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMN' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMN','Invalid Claim Number', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMN' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMN','無効なクレーム番号です', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMO' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMO','Contract Cancellation already registered', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMO' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMO','契約解除はすでに登録されています', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMP' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMP','Contract Cancellation already approved', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMP' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMP','契約解除はすでに承認されています', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMQ' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMQ','Refund Option is required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMQ' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMQ','返金選択が必要です', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMR' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMR','Premium Already Paid', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/



DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMR' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMR','Premium Already Paid', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/



DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMS' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMS','Negative Total, not allowed', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMS' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMS','合計額がマイナスです', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMT' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMT','No refund available', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMT' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMT','返金がありません', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMU' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMU','Contract Cancellation not registered', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMU' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMU','契約解除が登録されていません', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMV' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMV','Invalid Claim Type', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMV' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMV','無効なクレームタイプです', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMW' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMW','Selection is required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMW' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMW','選択が必要です', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMX' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMX','Rider Cancellation already registered', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/



DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMX' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMX','特約解除はすでに登録されています', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMY' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMY','Rider Cancellation already approved', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMY' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMY','特約解除はすでに承認されています', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMZ' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUMZ','Rider Cancellation is not registered', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUMZ' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUMZ','特約解除が登録されていません', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUN0' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUN0','Contract Cancellation not approved', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUN0' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUN0','契約解除は承認されていません', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUN1' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUN1','Rider Cancellation not approved', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUN1' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUN1','特約解除は承認されていません', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/
