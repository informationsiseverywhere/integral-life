

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1690' and DESCITEM='PLJ30' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1690','PLJ30','  ','E',' ','PLJ30  ','Agency Maintenance Sub Menu','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='SAS6' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','SAS6','  ','E',' ','SAS6  ','Agency Maintenance Sub Menu','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TAS7' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAS7','  ','E',' ','TAS7  ','Agency Maintenance','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TAS8' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAS8','  ','E',' ','TAS8  ','Agency Appointment','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TAS8' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAS8','  ','E',' ','TAS8  ','Agency Modify','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TAS9' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAS9','  ','E',' ','TAS9  ','Agency Enquiry','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TASA' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TASA','  ','E',' ','TASA  ','Agency Terminate','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' and DESCITEM='TASB' AND LANGUAGE='E';	
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TASB','  ','E',' ','TASB','Agency Reinstate','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/
commit;

 

