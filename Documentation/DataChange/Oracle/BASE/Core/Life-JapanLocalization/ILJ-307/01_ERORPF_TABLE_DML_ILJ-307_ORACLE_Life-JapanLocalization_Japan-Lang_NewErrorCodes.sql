DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL23';
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL23','契約日が正しくありません', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

COMMIT;