
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOM' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUOM','> allowable period', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

 
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOM' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUOM','> 許容期間', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUON' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUON','Did you complete the underwriting by medical examination?', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUON' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUON','診査による査定を完了しましたか？', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOO' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUOO','Sum insured is over the total limit. Do you want to proceed?', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOO' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUOO','通算限度額を超えています。処理を続けますか？', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/





DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOB' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RUOB','Repay Loans first', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/




DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF 
   where ERORPFX='ER'  AND EROREROR='RUOB' AND ERORLANG='J';
  IF ( cnt = 0 ) THEN
		insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'J','          ','RUOB','貸付返済を先に行う', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

COMMIT;