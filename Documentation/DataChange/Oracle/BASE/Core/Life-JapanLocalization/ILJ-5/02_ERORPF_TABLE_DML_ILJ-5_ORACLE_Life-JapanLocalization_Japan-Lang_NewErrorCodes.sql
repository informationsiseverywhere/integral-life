 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL15' AND ERORDESC='有効期限開始日必須'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL15','有効期限開始日必須', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/


 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL16' AND ERORDESC='有効期限終了日必須'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL16','有効期限終了日必須', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL17' AND ERORDESC='有効期限開始日が有効期限終了日より後'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL17','有効期限開始日が有効期限終了日より後', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL18' AND ERORDESC='資格番号必須'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL18','資格番号必須', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL13' AND ERORDESC='番号は英数字のみ'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'J','','JL13','番号は英数字のみ', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

COMMIT;
