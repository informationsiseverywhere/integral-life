 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL15' AND ERORDESC='Start Date is required'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'E','','JL15','Start Date is required', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/


 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL16' AND ERORDESC='Expiry Date is required'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'E','','JL16','Expiry Date is required', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL17' AND ERORDESC='Expiry Date <= Start Date'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'E','','JL17','Expiry Date <= Start Date', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL18' AND ERORDESC='License Number is required'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'E','','JL18','License Number is required', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

 DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.erorpf e
  WHERE e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='JL13' AND ERORDESC='No. should be alphanumeric'  ;
  IF ( cnt = 0 ) THEN
   insert into VM1DTA.erorpf (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', '', 'E','','JL13','No. should be alphanumeric', '', '', '', '', '', '',CURRENT_TIMESTAMP,'');
  END IF;
END;
/

COMMIT;
