DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(*) into cnt 
	FROM VM1DTA.DESCPF
	WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T5687' AND DESCITEM='HBNJ    ' AND LANGUAGE='J';
	IF ( cnt = 0 ) THEN
		INSERT INTO VM1DTA.DESCPF(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES('IT', '2', 'T5687', 'HBNJ    ', '', 'J', '', 'HBNJ      ', '入院保障                          ', 'UNDERWR1', 'UNDERWR1', CURRENT_TIMESTAMP);
	END IF;
END;
/
commit;