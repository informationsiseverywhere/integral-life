DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BR50HDTODATA' and column_name ='DECLDATE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' alter table BR50HDTODATA add DECLDATE NUMBER(8) ');
	EXECUTE IMMEDIATE ('comment on column BR50HDTODATA.decldate is ''DECLDATE''');
  END IF;  
END;
/
Commit;