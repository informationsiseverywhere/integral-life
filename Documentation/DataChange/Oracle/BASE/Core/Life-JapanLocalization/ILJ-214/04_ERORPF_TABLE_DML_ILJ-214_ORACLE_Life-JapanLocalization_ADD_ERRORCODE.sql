DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL65' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL65  ','Date exceed Next Payment Date', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL66' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JJL66','Partial Payment cannot be > than the Total Annuity Fund', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL68' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL68','Annuity Payment Frequency not required', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL69' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL69','First date Cannot be > Final Payment date', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL70' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL70','First Payment Date should = Final Payment Date for Annuity Payment option = Deferred', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL65' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL65',N'日付が次回支払日より後', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL66' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JJL66',N'一部支払額＞年金原資は不可', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL68' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL68',N'年金支払回数不要', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL69' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL69',N'初回支払日＞最終支払日は不可', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF 
   where ERORPFX='ER'  AND EROREROR='JL70' AND ERORLANG='E';
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','JL70',N'年金支払方法が据置の場合は初回支払日＝最終支払日', '', '', '', '', 'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'  ');
END IF;
END;
/


COMMIT;


