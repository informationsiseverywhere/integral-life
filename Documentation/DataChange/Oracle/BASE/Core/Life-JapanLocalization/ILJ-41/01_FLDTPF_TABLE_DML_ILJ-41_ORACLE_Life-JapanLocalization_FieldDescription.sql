
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM FLDTPF f WHERE f.FDID = 'FWCONDT' AND f.LANG='E';
  IF ( cnt = 0 ) THEN
    insert into FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
	values('FWCONDT','E','Forward Contract Date','Forward','Contract','Date','','0','0','0','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM FLDTPF f WHERE f.FDID = 'CONCOMMFLG' AND f.LANG='E';
  IF ( cnt = 0 ) THEN
    insert into FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
	values('CONCOMMFLG','E','Contract Commencement Flag','Contract','Commencement','Flag','','0','0','0','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;
