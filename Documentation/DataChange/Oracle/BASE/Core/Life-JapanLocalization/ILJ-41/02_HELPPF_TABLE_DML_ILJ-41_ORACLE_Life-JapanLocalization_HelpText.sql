
DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Contract Commencement Flag Rules';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','1','','1','Contract Commencement Flag Rules','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = '================================';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','2','','1','================================','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = '.                                                           ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','3','','1','.                                                           ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'If the Payment Method and Billing Frequency combination is';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','4','','1','If the Payment Method and Billing Frequency combination is','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'NOT found, then no check and validation need to be done.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','5','','1','NOT found, then no check and validation need to be done.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = '.                                                           ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','6','','1','.                                                           ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'The Contract Type key can also be set up as *** to provide a';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','7','','1','The Contract Type key can also be set up as *** to provide a','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '8' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'standard set of rules which are accessed by all contracts ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','8','','1','standard set of rules which are accessed by all contracts ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'S' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '9' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'unless their own type has set up in the table independently.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','S','SJL05', ' ','9','','1','unless their own type has set up in the table independently.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Forward Contract Date set as Y means Contract Date is to ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','1','','1','Forward Contract Date set as Y means Contract Date is to ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'be the 1st day of the next month following the Risk ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','2','','1','be the 1st day of the next month following the Risk ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Commencement Date and if user did not enter the correct ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','3','','1','Commencement Date and if user did not enter the correct ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Contract Date, system is to trigger error message-Contract ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','4','','1','Contract Date, system is to trigger error message-Contract ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Date incorrect and does not allow user to proceed.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','5','','1','Date incorrect and does not allow user to proceed.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = '.                                                           ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','6','','1','.                                                           ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Forward Contract Date set as N this means Contract Date ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','7','','1','Forward Contract Date set as N this means Contract Date ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '8' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'has to be the same as Risk Commencement Date and if user did';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','8','','1','has to be the same as Risk Commencement Date and if user did','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '9' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = ' not enter the correct Contract Date, system is to trigger ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','9','','1',' not enter the correct Contract Date, system is to trigger ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '10' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'error message-Contract Date incorrect and does not allow ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','10','','1','error message-Contract Date incorrect and does not allow ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '11' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'user to proceed.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'FWCONDT','11','','1','user to proceed.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Contract Commencement Flag set as Y means user can have a ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','1','','1','Contract Commencement Flag set as Y means user can have a ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'tick on this field on the proposal screen and if user does ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','2','','1','tick on this field on the proposal screen and if user does ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'not tick on this field, system will not display any error ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','3','','1','not tick on this field, system will not display any error ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'or warning message.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','4','','1','or warning message.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = '.                                                           ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','5','','1','.                                                           ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'Contract Commencement Flag set as N means user cannot have';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','6','','1','Contract Commencement Flag set as N means user cannot have','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = ' a tick on this field on the proposal screen and if user ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','7','','1',' a tick on this field on the proposal screen and if user ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '8' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'proceed to have a tick on this field, system is to trigger ';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','8','','1','proceed to have a tick on this field, system is to trigger ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.HELPPF h 	
	WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' AND h.HELPPROG ='SJL05' AND h.HELPSEQ = '9' AND h.VALIDFLAG = '1' 
	AND h.HELPLINE = 'error message-Contract Commencement Flag incorrect.';
  IF ( cnt = 0 ) THEN
    insert into VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) 
	values('HP',' ','E','F','SJL05', 'CONCOMMFLG','9','','1','error message-Contract Commencement Flag incorrect.','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);
END IF;
END;
/
commit;
