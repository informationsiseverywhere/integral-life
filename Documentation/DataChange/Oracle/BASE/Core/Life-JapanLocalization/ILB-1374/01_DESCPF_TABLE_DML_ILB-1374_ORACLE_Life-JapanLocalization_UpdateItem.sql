DECLARE 
	cnt number:=0;
BEGIN
	SELECT COUNT(1) into cnt 
	FROM VM1DTA.DESCPF
	WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T3672' AND DESCITEM='4       ' AND LANGUAGE='J';
	IF ( cnt > 0 ) THEN
		update VM1DTA.DESCPF set LONGDESC=N'銀行振込                          ' where DESCPFX='IT' and DESCCOY='2' and DESCTABL='T3672' and DESCITEM='4       ' and LANGUAGE='J';
	END IF;
END;
/
commit;