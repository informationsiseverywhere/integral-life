   
declare 
  l_count integer;
begin

  select count(*)
    into l_count
  from user_triggers
  where trigger_name = 'VM1DTA.TR_RCVDPF';

  if l_count > 0 then 
	  execute immediate 'drop trigger VM1DTA.TR_RCVDPF';
  end if;

end;
/