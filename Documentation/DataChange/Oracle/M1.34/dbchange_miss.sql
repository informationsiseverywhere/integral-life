﻿drop sequence SEQ_WAKLPF;

create sequence SEQ_WAKLPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_WAKLPF" before insert on WAKLPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_WAKLPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_WAKLPF;
/

drop sequence SEQ_SPLUPF;

create sequence SEQ_SPLUPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_SPLUPF" before insert on SPLUPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_SPLUPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_SPLUPF;
/

drop sequence SEQ_SPLBPF;

create sequence SEQ_SPLBPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_SPLBPF" before insert on SPLBPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_SPLBPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_SPLBPF;
/

drop sequence SEQ_SPLDPF;

create sequence SEQ_SPLDPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_SPLDPF" before insert on SPLDPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_SPLDPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_SPLDPF;
/

drop sequence SEQ_GSMTPF;

create sequence SEQ_GSMTPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_GSMTPF" before insert on GSMTPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_GSMTPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_GSMTPF;
/

drop sequence SEQ_GSMDPF;

create sequence SEQ_GSMDPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_GSMDPF" before insert on GSMDPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_GSMDPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_GSMDPF;
/

drop sequence SEQ_GAXTPF;

create sequence SEQ_GAXTPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_GAXTPF" before insert on GAXTPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_GAXTPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_GAXTPF;
/

drop sequence SEQ_MYP1PF;

create sequence SEQ_MYP1PF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_MYP1PF" before insert on MYP1PF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_MYP1PF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_MYP1PF;
/

drop sequence SEQ_MYP2PF;

create sequence SEQ_MYP2PF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_MYP2PF" before insert on MYP2PF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_MYP2PF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_MYP2PF;
/

drop sequence SEQ_MYP3PF;

create sequence SEQ_MYP3PF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_MYP3PF" before insert on MYP3PF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_MYP3PF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_MYP3PF;
/

drop sequence SEQ_MYXTPF;

create sequence SEQ_MYXTPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_MYXTPF" before insert on MYXTPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_MYXTPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_MYXTPF;
/

drop sequence SEQ_PITXPF;

create sequence SEQ_PITXPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_PITXPF" before insert on PITXPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_PITXPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_PITXPF;
/

drop sequence SEQ_DCERPF;

create sequence SEQ_DCERPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_DCERPF" before insert on DCERPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_DCERPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_DCERPF;
/

drop sequence SEQ_DCEXPF;

create sequence SEQ_DCEXPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_DCEXPF" before insert on DCEXPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_DCEXPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_DCEXPF;
/

drop sequence SEQ_LTRIPF;

create sequence SEQ_LTRIPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger TR_LTRIPF
before insert on  LTRIPF
for each row
declare
v_pkValue  number;
v_flag     exception;
v_rownum   number;
PRAGMA EXCEPTION_INIT (v_flag, -1);  -- it is assumed
begin
v_rownum := 0;
SELECT COUNT(*) INTO v_rownum  FROM LTRIPF
WHERE (RSKPFX= :New.RSKPFX AND RSKCOY= :New.RSKCOY AND CHDRNO= :New.CHDRNO AND
RSKNO= :New.RSKNO AND DTEEFF= :New.DTEEFF AND TRANNO= :New.TRANNO AND PREMCL=
:New.PREMCL AND UWYEAR= :New.UWYEAR AND FRDATE= :New.FRDATE)
;
if (v_rownum > 0 ) then
RAISE v_flag;
else
select SEQ_LTRIPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
end if;
end TR_LTRIPF;
/

drop sequence SEQ_LTSIPF;

create sequence SEQ_LTSIPF
increment by 1
start with 1000000
  maxvalue 99999999999999999999
  minvalue 1000000
nocycle
  cache 10
noorder;

create trigger "TR_LTSIPF" before insert on LTSIPF for each row
declare
       v_pkValue  number;
     begin
     select               SEQ_LTSIPF.nextval into v_pkValue from dual;
     :New.unique_number :=        v_pkValue;
   end TR_LTSIPF;
/
