
DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem = 'APINTCAL' and desctabl='T5645' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='年金利息计算'  WHERE descitem = 'APINTCAL' and desctabl='T5645' and language='S' and desccoy in ('2','0','9'); 

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = '***CNYSU' and desctabl='TR630' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='具有‘SU’状态的全部产品'  WHERE descitem = '***CNYSU' and desctabl='TR630' and language='S' and desccoy in ('2','0','9'); 

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE  descitem = '***SGDSU' and desctabl='TR630' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='具有‘SU’状态的全部产品'  WHERE  descitem = '***SGDSU' and desctabl='TR630' and language='S' and desccoy in ('2','0','9');  

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE  descitem = 'RUXT512    ' and desctabl='TR384' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='退保'  WHERE  descitem = 'RUXT512    ' and desctabl='TR384' and language='S' and desccoy in ('2','0','9'); 

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF  WHERE descitem = 'TWLT512  ' and desctabl='TR384' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='退保'  WHERE descitem = 'TWLT512  ' and desctabl='TR384' and language='S' and desccoy in ('2','0','9');

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF  WHERE  descitem = ' ' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='用户POS权限限制'  WHERE  descitem = ' ' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem =  'PL1' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='保单服务级别1'  WHERE descitem =  'PL1' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem =  'PL2' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='保单服务级别2'  WHERE descitem =  'PL2' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 



end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem =  'PL3' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='保单服务级别3'  WHERE descitem =  'PL3' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 



end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem =  'PL4' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='保单服务级别4'  WHERE descitem =  'PL4' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 



end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem =  'PL5' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='保单服务级别5'  WHERE descitem =  'PL5' and desctabl='TD5H6' and language='S' and desccoy in ('2','0','9'); 



end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem =  'LMJAPUP' and desctabl='T1685' and language='S' and desccoy in ('2','0','9');  

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='缴清处理'  WHERE descitem =  'LMJAPUP' and desctabl='T1685' and language='S' and desccoy in ('2','0','9');  



end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem =  'LPUPPLN ' and desctabl='T1685' and language='S' and desccoy in ('2','0','9');  

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='缴清处理'  WHERE descitem =  'LPUPPLN ' and desctabl='T1685' and language='S' and desccoy in ('2','0','9');  



end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF  WHERE descitem =  'CSPUP001' and desctabl='TR2A5' and language='S' and desccoy in ('2','0','9');  

IF ( cnt > 0 ) THEN



update VM1DTA.DESCPF SET LONGDESC='缴清处理'  WHERE descitem =  'CSPUP001' and desctabl='TR2A5' and language='S' and desccoy in ('2','0','9');  



end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'PD5HC   ' and desctabl='T5645' and language='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金领取'  WHERE descitem = 'PD5HC   ' and desctabl='T5645' and language='S' and desccoy in ('2','0','9');
 

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'TAFU    ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金领取'  WHERE descitem = 'TAFU    ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');
 

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'CDA     ' and desctabl='TD5H7' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='中国延期年金'  WHERE descitem = 'CDA     ' and desctabl='TD5H7' and language ='S' and desccoy in ('2','0','9');
 


end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'CDA1    ' and desctabl='TD5G4' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='中国延期年金'  WHERE descitem = 'CDA1    ' and desctabl='TD5G4' and language ='S' and desccoy in ('2','0','9');

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = '**      ' and desctabl='T5651' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='系统默认值'  WHERE descitem = '**      ' and desctabl='T5651' and language ='S' and desccoy in ('2','0','9');

end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'J***    ' and desctabl='T3716' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='系统默认值'  WHERE descitem = 'J***    ' and desctabl='T3716' and language ='S' and desccoy in ('2','0','9');
 

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem = '****    ' and desctabl='T3716' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='系统默认值'  WHERE descitem = '****    ' and desctabl='T3716' and language ='S' and desccoy in ('2','0','9');
 

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem = '***     ' and desctabl='TD5H7' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='系统默认值'  WHERE descitem = '***     ' and desctabl='TD5H7' and language ='S' and desccoy in ('2','0','9');
 


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'DE          ' and desctabl='T3695' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='红利存款'  WHERE descitem = 'DE          ' and desctabl='T3695' and language ='S' and desccoy in ('2','0','9');
 

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1)INTO cnt from VM1DTA.DESCPF WHERE descitem = 'AP                 ' and desctabl='T3695' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金应计暂存'  WHERE descitem = 'AP                 ' and desctabl='T3695' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF  WHERE descitem = 'BAFW    ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='新年金计息'  WHERE descitem = 'BAFW    ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF  WHERE descitem = 'BD5HV       ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金利息资本化'  WHERE descitem = 'BD5HV       ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF  WHERE descitem = 'BD5HU ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='年金计息'  WHERE descitem = 'BD5HU ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'APINTCAL' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='年金计息计算'  WHERE descitem = 'APINTCAL' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'PD5HC   ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金领取'  WHERE descitem = 'PD5HC   ' and desctabl='T5645' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'TAFU       ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');


IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金领取'  WHERE descitem = 'TAFU       ' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem = 'A              ' and desctabl='T6694' and language ='S' and desccoy in ('2','0','9');


IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='累积利息'  WHERE descitem = 'A              ' and desctabl='T6694' and language ='S' and desccoy in ('2','0','9');



end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem like '%PM00%' and desctabl='T5675' and language ='S' and desccoy in ('2','0','9');

IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='部分退保无计算'  WHERE descitem like '%PM00%' and desctabl='T5675' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem like '%SUSUR004%' and desctabl='TR2A5' and language ='S' and desccoy in ('2','0','9');



IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='部分退保年金产品'  WHERE descitem like '%SUSUR004%' and desctabl='TR2A5' and language ='S' and desccoy in ('2','0','9');



end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem like '%SUSUR004%' and desctabl='TR2A5' and language ='S' and desccoy in ('2','0','9');


IF ( cnt > 0 ) THEN

update VM1DTA.DESCPF SET LONGDESC='部分退保年金产品'  WHERE descitem like '%SUSUR004%' and desctabl='TR2A5' and language ='S' and desccoy in ('2','0','9');

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem like '%TAFT    %' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');


IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金细节变更'  WHERE descitem like '%TAFT    %' and desctabl='T1688' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN

select count(1) INTO cnt from VM1DTA.DESCPF WHERE descitem like '%TAFT    %' and desctabl='T5679' and language ='S' and desccoy in ('2','0','9');


IF ( cnt > 0 ) THEN


update VM1DTA.DESCPF SET LONGDESC='年金细节变更'  WHERE descitem like '%TAFT    %' and desctabl='T5679' and language ='S' and desccoy in ('2','0','9');


end if;
end;
/
commit;

