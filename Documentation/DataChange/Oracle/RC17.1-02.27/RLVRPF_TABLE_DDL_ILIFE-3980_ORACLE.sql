DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'RLVRPF' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMediate ( 
    ' CREATE TABLE VM1DTA.RLVRPF
         (	UNIQUE_NUMBER NUMBER(18,0), 
            CHDRNUM CHAR(8 CHAR), 
            CHDRCOY CHAR(1 CHAR),
            MEMACCNUM CHAR(20 CHAR),
            FNDUSINUM CHAR(11 CHAR),
            SRVSTRTDT INT,
            TAXFREEAMT NUMBER(17,2),
            KIWISVRCOM  NUMBER(17,2),
            TAXEDAMT NUMBER(17,2),
            UNTAXEDAMT  NUMBER(17,2),
            PRSRVDAMT NUMBER(17,2),
            KIWIAMT NUMBER(17,2),
            UNRESTRICT NUMBER(17,2),
            RSTRICT NUMBER(17,2),
            TOTTRFAMT NUMBER(17,2),
            MONEYIND CHAR(1 CHAR),
            USRPRF CHAR(10 CHAR), 
            JOBNM CHAR(10 CHAR), 
            DATIME TIMESTAMP (7),
            CONSTRAINT PK_RLVRPF PRIMARY KEY (UNIQUE_NUMBER)
        )'
    );
  END IF;  
END;
/
COMMIT;