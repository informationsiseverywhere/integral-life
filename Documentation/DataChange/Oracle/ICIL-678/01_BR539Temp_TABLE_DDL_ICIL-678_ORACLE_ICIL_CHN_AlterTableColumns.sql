DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BR539Temp' and column_name ='CNT' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BR539Temp ADD CNT NUMBER ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BR539Temp.CNT IS ''CONTRACT COUNT'''); 
  END IF;
END;
/ 