set define off;

delete from VM1DTA.DESCPF where DESCTABL='TD5GF' and DESCCOY='2' and (LANGUAGE = 'E' OR LANGUAGE = 'S') and DESCPFX = 'IT'; 
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='*****'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','*****','  ','E','','*****','Default item','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','*****','  ','S','','*****','Default item','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCI**'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCI**','  ','E','','CCI**','China Critical Illness-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCI**','  ','S','','CCI**','China Critical Illness-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDA**'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDA**','  ','E','','CDA**','ChinaDeferred Annuity-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDA**','  ','S','','CDA**','ChinaDeferred Annuity-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCIAG'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIAG','  ','E','','CCIAG','China Critical Illness-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIAG','  ','S','','CCIAG','China Critical Illness-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCIBA'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIBA','  ','E','','CCIBA','China Critical Illness-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIBA','  ','S','','CCIBA','China Critical Illness-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;



DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDAAG'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDAAG','  ','E','','CDAAG','ChinaDeferred Annuity-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDAAG','  ','S','','CDAAG','ChinaDeferred Annuity-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;


DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDABA'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDABA','  ','E','','CDABA','ChinaDeferred Annuity-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDABA','  ','S','','CDABA','ChinaDeferred Annuity-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

end if;
end;
/
commit;


