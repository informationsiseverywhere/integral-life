DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'GENAREAJ'
      and table_name = 'ITEMPF';

  if (v_column_exists = 0) then
      execute immediate 'alter table ITEMPF ADD (GENAREAJ VARCHAR(4000))';
  end if;
end;
/