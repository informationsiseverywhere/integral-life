DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'BATCH_JOB_INSTANCE' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE ( 
    'CREATE TABLE BATCH_JOB_INSTANCE  (
    JOB_INSTANCE_ID NUMBER(19,0)  NOT NULL PRIMARY KEY ,
    VERSION NUMBER(19,0) ,
    JOB_NAME VARCHAR2(100) NOT NULL,
    JOB_KEY VARCHAR2(32) NOT NULL,
    constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
    ) '
    );
  END IF;  
END;
/
