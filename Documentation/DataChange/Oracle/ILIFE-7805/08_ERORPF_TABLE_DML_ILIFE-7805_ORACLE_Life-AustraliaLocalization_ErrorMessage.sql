DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRMW';
IF ( cnt = 0 ) THEN
  insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
  values ('ER',' ','E','S6351     ','RRMW', 'Single Prm Top up not allowed','','','','','UNDERWR1','UNDERWR1',sysDate(),'');
END IF;
END;
/
  
COMMIT;

