DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVRPF' and column_name ='SINGPREMTYPE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE COVRPF ADD SINGPREMTYPE VARCHAR2(3 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.COVRPF.SINGPREMTYPE IS ''Single Premium Type'''); 
  END IF;  
END;
/
