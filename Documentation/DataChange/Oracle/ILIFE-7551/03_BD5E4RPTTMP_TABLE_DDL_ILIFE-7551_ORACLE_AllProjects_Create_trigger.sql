
CREATE OR REPLACE EDITIONABLE TRIGGER "VM1DTA"."TR_BD5E4RPTTMP" before insert on BD5E4RPTTMP for each row
declare
       v_pkValue  number;
     begin
     select SEQ_BD5E4RPTTMP.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_BD5E4RPTTMP;



/
