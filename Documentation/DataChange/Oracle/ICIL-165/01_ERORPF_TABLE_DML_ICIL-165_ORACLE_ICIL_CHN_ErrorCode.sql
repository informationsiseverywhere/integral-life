

DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RFWI';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RFWI  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RFWI  ','Invalid Characters are included', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RFWI  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RFWI  ','Invalid Characters are included', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
 COMMIT; 