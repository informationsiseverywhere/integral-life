create table ZHLBPF(UNIQUE_NUMBER varchar2(10) NOT NULL ,
CHDRCOY varchar2(2),
CHDRNUM varchar2(8),
BNYCLT varchar2(8),
BNYPC number(5,2),
VALIDFLAG varchar2(1),
TRANNO number(5),
TRDT number(6),
TRTM number(6),
USR number(6),
ACTVALUE number(17,2),
ZCLMADJST number(17,2),
ZHLDCLMV number(17,2),
ZHLDCLMA number(17,2),
USRPRF varchar2(10),
JOBNM varchar2(10),
DATIME timestamp NULL,
PRIMARY KEY(UNIQUE_NUMBER)
) ;




























CREATE OR REPLACE FORCE VIEW ZHLB ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "BNYCLT", "BNYPC", "VALIDFLAG", "TRANNO", "TRDT", "TRTM", "USR", "ACTVALUE", "ZCLMADJST", "ZHLDCLMV", "ZHLDCLMA", "USRPRF", "JOBNM", "DATIME")
AS
  SELECT UNIQUE_NUMBER,
    CHDRCOY,
    CHDRNUM,
    BNYCLT,
    BNYPC,
    VALIDFLAG,
    TRANNO,
    TRDT,
    TRTM,
    USR,
    ACTVALUE,
    ZCLMADJST,
    ZHLDCLMV,
    ZHLDCLMA,
    USRPRF,
    JOBNM,
    DATIME
  FROM ZHLBPF
  ORDER BY CHDRCOY,
    CHDRNUM,
    BNYCLT;
    
    
    

    create table ZHLCPF(UNIQUE_NUMBER varchar2(10) NOT NULL ,
CHDRCOY varchar2(2),
CHDRNUM varchar2(8),
LIFE varchar2(2),
JLIFE varchar(2),
COVERAGE varchar2(2),
RIDER  varchar2(2),
CRTABLE  varchar2(4),
VALIDFLAG varchar2(1),
TRANNO number(5),
TRDT number(6),
TRTM number(6),
USR number(6),
ACTVALUE number(17,2),
ZCLMADJST number(17,2),
ZHLDCLMV number(17,2),
ZHLDCLMA number(17,2),
USRPRF varchar2(10),
JOBNM varchar2(10),
DATIME timestamp NULL,
PRIMARY KEY(UNIQUE_NUMBER)
) ;
    
    CREATE OR REPLACE FORCE VIEW ZHLC ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "JLIFE", "COVERAGE", "RIDER", "CRTABLE", "VALIDFLAG", "TRANNO", "TRDT", "TRTM", "USR", "ACTVALUE", "ZCLMADJST", "ZHLDCLMV", "ZHLDCLMA", "USRPRF", "JOBNM", "DATIME")
AS
  SELECT UNIQUE_NUMBER,
    CHDRCOY,
    CHDRNUM,
    LIFE,
    JLIFE,
    COVERAGE,
    RIDER,
    CRTABLE,
    VALIDFLAG,
    TRANNO,
    TRDT,
    TRTM,
    USR,
    ACTVALUE,
    ZCLMADJST,
    ZHLDCLMV,
    ZHLDCLMA,
    USRPRF,
    JOBNM,
    DATIME
  FROM ZHLCPF
  ORDER BY CHDRCOY,
    CHDRNUM,
    LIFE,
    COVERAGE,
    RIDER;
    
  INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTB'
           ,'Claim Hold Already Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTC'
           ,'Coverage Hold Does Not Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTD'
           ,'Beneficiary Hold Does Not Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTE'
           ,'Hold Claim Amount should not  be > Claim amount'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTF'
           ,'Hold Adjustment should not be > Total Adjustment'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTG'
           ,'Release Claim Amount should not be > Hold Claim Amount'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTH'
           ,'Release Adjustment Amount should not be > Hold Adjustment'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');    
    
    INSERT INTO erorpf
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTS'
           ,'Claim is on Hold Status'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  

