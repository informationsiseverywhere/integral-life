DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FUNDPOOL'
      and table_name = 'HITRPF';
     

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE  VM1DTA.HITRPF  ADD (FUNDPOOL NCHAR(1) ,HCOVERAGE NCHAR(1), HRIDER NCHAR(1),HCRTABLE NCHAR(1),DEALCDTE NUMERIC(8),TRNAMT NUMERIC(17,2),PCUNIT NUMERIC(5,2),ALOCRATE NUMERIC(5,2),ENHCPC NUMERIC(5,2),UNIQK NUMERIC(18),ORGTRNO NUMERIC(5),ORGTRCDE NCHAR(4))';
  end if;
end;
/


