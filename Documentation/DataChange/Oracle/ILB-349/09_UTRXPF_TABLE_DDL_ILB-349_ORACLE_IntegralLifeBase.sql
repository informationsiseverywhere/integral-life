    
   DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FUNDPOOL'
      and table_name = 'UTRXPF';
     

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE  VM1DTA.UTRXPF   ADD  ( FUNDPOOL NCHAR(1))';
  end if;
end;
/


