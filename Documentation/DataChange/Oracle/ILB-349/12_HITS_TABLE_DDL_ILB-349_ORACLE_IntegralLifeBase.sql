CREATE OR REPLACE VIEW "VM1DTA"."HITS" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "ZINTBFND", "ZCURPRMBAL", "ZLSTUPDT", "FUNDPOOL","USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            ZINTBFND,
            ZCURPRMBAL,
            ZLSTUPDT,
            FUNDPOOL,
            USRPRF,
            JOBNM,
            DATIME
       FROM HITSPF
ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            ZINTBFND