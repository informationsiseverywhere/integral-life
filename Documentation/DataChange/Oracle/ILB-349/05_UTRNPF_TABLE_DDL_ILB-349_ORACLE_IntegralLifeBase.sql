  DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FUNDPOOL'
      and table_name = 'UTRNPF';
     

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE VM1DTA.UTRNPF  ADD (FUNDPOOL	NCHAR(1),HLIFE	NCHAR(2),HCOVERAGE	NCHAR(2),HRIDER	NCHAR(2),
HCRTABLE	NCHAR(4),DEALCDTE	NUMERIC(8),TRNAMT	NUMERIC(17,2),PCUNIT	NUMERIC(17,2),ALOCRATE	NUMERIC(17,2),ENHCPC	NUMERIC(17,2),	
UNIQK		NUMERIC(8),
ORGTRNO	NUMERIC(5),
ORGTRCDE	NCHAR(4))';
  end if;
end;
/

