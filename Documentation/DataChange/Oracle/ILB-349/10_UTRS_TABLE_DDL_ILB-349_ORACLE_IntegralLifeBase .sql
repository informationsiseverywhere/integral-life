CREATE OR REPLACE VIEW  "VM1DTA"."UTRS" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "VRTFND", "UNITYP", "CURUNTBAL", "CURDUNTBAL","FUNDPOOL") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            VRTFND,
            UNITYP,
            CURUNTBAL,
            CURDUNTBAL,
            FUNDPOOL
       FROM UTRSPF
ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            VRTFND,
            UNITYP;