--------------------------------------------------------
--  DDL for View RBNK
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."RBNK" 
  ("UNIQUE_NUMBER", "RDOCPFX", "RDOCCOY", "RDOCNUM", "CHQNUM", "TCHQDATE", 
  "BANKDESC01", "BANKDESC02", "BANKDESC03", "RCPTREV", "ZCHQTYP", "PAYTYPE", 
  "BANKKEY", "DOCORIGAMT", "ORIGCCY", "SCRATE", "DOCACCTAMT", "ACCTCCY", 
  "MYFLG", "USRPRF", "JOBNM", "DATIME", "RCPTSTAT", "CNRSNCD") 
  AS 
  SELECT UNIQUE_NUMBER,
            RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            CHQNUM,
            TCHQDATE,
            BANKDESC01,
            BANKDESC02,
            BANKDESC03,
            RCPTREV,
            ZCHQTYP,
            PAYTYPE,
            BANKKEY,
            DOCORIGAMT,
            ORIGCCY,
            SCRATE,
            DOCACCTAMT,
            ACCTCCY,
            MYFLG,
            USRPRF,
            JOBNM,
            DATIME,
            RCPTSTAT,
            CNRSNCD
       FROM RBNKPF
   ORDER BY RDOCPFX, RDOCCOY, RDOCNUM
;
