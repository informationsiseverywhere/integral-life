DECLARE 
  cnt number(2,1);
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'BACRLTEMP1' ;
IF ( cnt > 0  ) THEN  
	EXECUTE IMMediate ('CREATE TABLE BACRLTEMP1(UNIQUE_NUMBER number(18) NOT NULL,
			BACRTAPREC char(500) NULL,
			MEMBER_NAME char(10) NULL),
        )'
	);
END IF ;
END;
/