
DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRH7  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRH7  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRH7  ','Cvg/Rider without Paid-up mthd', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRH7  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRH7  ','Cvg/Rider without Paid-up mthd', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
COMMIT;


DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRH8  ';
COMMIT;
    
DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRH8  ' ;
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRH8  ','select all IF-PP cvg/riders', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
  DECLARE 	
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRH8  ' ;
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRH8  ','select all IF-PP cvg/riders', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /

COMMIT;



