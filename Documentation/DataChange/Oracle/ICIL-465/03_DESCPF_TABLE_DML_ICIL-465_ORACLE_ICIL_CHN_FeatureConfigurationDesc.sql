
set define off;
delete from VM1DTA.DESCPF where DESCTABL='TR2A5' and DESCITEM='CSPUP001' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','CSPUP001','  ','E',' ','CSPUP001','Paid Up Processing','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','CSPUP001','  ','S',' ','CSPUP001','Paid Up Processing','UNDERWR1  ','UNDERWR1',current_timestamp);
COMMIT;

