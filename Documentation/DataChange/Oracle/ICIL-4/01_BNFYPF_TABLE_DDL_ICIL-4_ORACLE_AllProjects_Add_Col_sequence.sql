
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'BNFYPF' and column_name ='SEQUENCE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE BNFYPF ADD SEQUENCE CHAR(2) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN BNFYPF.SEQUENCE IS ''Beneficiary Sequence''');
  END IF;  
END;
/