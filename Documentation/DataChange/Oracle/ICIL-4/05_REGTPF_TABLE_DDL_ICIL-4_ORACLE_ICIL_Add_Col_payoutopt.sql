
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'REGTPF' and column_name ='PAYOUTOPT' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE REGTPF ADD PAYOUTOPT CHAR(8) ');
    EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.REGTPF.PAYOUTOPT IS ''Payment out''');
  END IF;
END;
/