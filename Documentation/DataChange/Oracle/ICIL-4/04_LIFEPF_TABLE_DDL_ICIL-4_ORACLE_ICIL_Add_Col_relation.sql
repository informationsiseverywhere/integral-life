
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'LIFEPF' and column_name ='RELATION' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE LIFEPF ADD RELATION CHAR(4) ');
     EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.LIFEPF.RELATION IS ''Relation drop down''');
  END IF;
END;
/