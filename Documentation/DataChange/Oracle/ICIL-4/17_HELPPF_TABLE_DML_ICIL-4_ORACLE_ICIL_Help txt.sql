
SET DEFINE OFF;
delete from VM1DTA.HELPPF where HELPITEM = 'REFUNDOVER' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',1,'1','Refund Overpay Premium                                      ','UNDERWR1  ','UNDERWR1',current_timestamp);
	 
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',2,'1','For proposal, if suspense amount great than or equal to     ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',3,'1','premium amount  before actual billing, the user has 2       ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',4,'1','options for the overpayment of premium;                     ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',5,'1','1.  It is checkbox filed, ticked=Y, unticked=N;             ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',6,'1','2.  Y: refund the overpayment of premium to the user        ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',7,'1','when issue.                                                 ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',8,'1','3.  N or Space: Leave the overpayment of prem in            ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REFUNDOVER',9,'1','contract suspense, when issue.                              ','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','S6353     ','REFUNDOVER',1,'1','Refund Overpay Premium                                      ','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'RELATIONWI' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','RELATIONWI',1,'1','Relationship with Owner','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'SEQUENCE' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','SEQUENCE',1,'1','Beneficiary Sequence','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'IDEXPIREDA' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','IDEXPIREDA',1,'1','ID Expire Date','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'ISPERMANEN' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','ISPERMANEN',1,'1','Permanent ID','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'PAYOUTOPT' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','PAYOUTOPT',1,'1','Annuity Payout Option','UNDERWR1  ','UNDERWR1',current_timestamp);
commit;