  
CREATE OR REPLACE FORCE VIEW BNFYLNB AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            BNYCLT,
            TRANNO,
            VALIDFLAG,
            CURRFR,
            CURRTO,
            BNYRLN,
            BNYCD,
            BNYPC,
            TERMID,
            USER_T,
            TRTM,
            TRDT,
            EFFDATE,
            USRPRF,
            JOBNM,
            DATIME,
            BNYTYPE,CLTRELN,RELTO,SELFIND,REVCFLG,ENDDATE,SEQUENCE
       FROM BNFYPF
      WHERE VALIDFLAG = '1';
commit;