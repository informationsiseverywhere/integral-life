	  
CREATE OR REPLACE FORCE VIEW BNFYENQ AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            BNYCLT,
            VALIDFLAG,
            CURRFR,
            CURRTO,
            BNYRLN,
            BNYCD,
            BNYPC,
            EFFDATE,
            USRPRF,
            JOBNM,
            DATIME,
            BNYTYPE,CLTRELN,RELTO,SELFIND,REVCFLG,ENDDATE,SEQUENCE
       FROM BNFYPF
      WHERE VALIDFLAG = '1';
commit;