DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM vm1dta.itempf 
  where itemcoy = '2' and itemtabl='T5687' and itemitem like 'AADB    ' and itmfrm =20200101;
  IF ( cnt != 0 ) THEN
    update vm1dta.itempf set itmto=20901231 where itemcoy = '2' and itemtabl='T5687' and itemitem like 'AADB    ' and itmfrm =20200101;   
	commit;
  END IF;
END;
/