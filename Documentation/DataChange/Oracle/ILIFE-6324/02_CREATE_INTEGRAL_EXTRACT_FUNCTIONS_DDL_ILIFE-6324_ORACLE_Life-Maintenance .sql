create or replace FUNCTION INTEGRAL_EXTRACT (dp in varchar2, v_date in varchar2 )
RETURN VARCHAR2
AS
result VARCHAR(4);
BEGIN
	  IF (dp = 'YEAR') then
	     select  substr( v_date,1,4) into result from dual;
	  ELSE IF (dp = 'MONTH') then
	     select substr( v_date,5,2) into result from dual;
	   END IF;
	  END IF;
	  	  return result;
END;
/