declare
vcnt number := 0;
BEGIN
   select COUNT(*) into vcnt from descpf where descpfx = 'IT' and desccoy = 0 and desctabl= 'T1693' and descitem='7' and language= 'E';
   IF(vcnt > 1)
   then    
     delete from descpf where unique_number in (select max(unique_number) from descpf where descpfx = 'IT' and desccoy = 0 and desctabl= 'T1693' and descitem='7' and language= 'E' );   
    end if;
END;
/
