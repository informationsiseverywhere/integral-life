
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RCVDPF' and column_name ='DIALDOWNOPTION' ; 
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE RCVDPF ADD  DIALDOWNOPTION NCHAR(3)');
  END IF;
END;
/
