DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5125' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5125', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5123' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5123', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr679' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr679', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6242' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6242', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5127' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5127', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh515' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh515', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr518' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr518', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr516' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr516', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S6259' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S6259', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr50o' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr50o', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr676' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr676', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr576' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr576', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='S5154' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','S5154', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr570' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr570', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr580' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr580', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr680' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr680', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr571' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr571', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh504' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh504', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sh514' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sh514', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '1' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'This Dial Down Option allows user' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','1','','1','This Dial Down Option allows user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '2' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to select percentage of premium to be discounted' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','2','','1','to select percentage of premium to be discounted','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '3' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'based on a certain percentage of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','3','','1','based on a certain percentage of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '4' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'to be sacrificed.Option�100�means no sacrifice of commission ' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','4','','1','to be sacrificed.Option�100�means no sacrifice of commission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '5' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'and premium not discounted.Option�080�denotes 20% sacrifices' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','5','','1','and premium not discounted.Option�080�denotes 20% sacrifices','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '6' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'of commission inreturn for 5% discount of premium.Mapping of' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','6','','1','of commission inreturn for 5% discount of premium.Mapping of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.HELPPF h 
  WHERE h.HELPPFX = 'HP' AND h.HELPLANG='E' AND h.HELPTYPE = 'F' 
      AND h.HELPPROG ='Sr546' AND h.HELPSEQ = '7' AND h.VALIDFLAG = '1' 
      AND h.HELPLINE = 'the commission and premium is to be done in VPMS.' ;
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.HELPPF(HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
    VALUES ('HP','','E','F','Sr546', '','7','','1','the commission and premium is to be done in VPMS.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP );    
  END IF;
END;
/

