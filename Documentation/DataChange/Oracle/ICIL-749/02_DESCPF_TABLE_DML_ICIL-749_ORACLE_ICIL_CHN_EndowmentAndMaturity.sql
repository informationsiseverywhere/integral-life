
set define off;
delete from VM1DTA.DESCPF where DESCTABL='T5645' and DESCITEM='PR532' and DESCCOY = '2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T5645','PR532   ','  ','E',' ','End Mat   ','Endowment and Maturity        ','UNDERWR1  ','UNDERWR1  ',current_timestamp) ;
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T5645','PR532   ','  ','S',' ','End Mat   ','Endowment and Maturity        ','UNDERWR1  ','UNDERWR1  ',current_timestamp) ;
COMMIT;


set define off;
delete from VM1DTA.DESCPF where DESCTABL='T5677' and DESCITEM='T6A1UM08' and DESCCOY = '2' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5677','T6A1UM08','  ','E',' ','          ','T6A1UM08                      ','UNDERWR1  ','UNDERWR1  ',current_timestamp);
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5677','T6A1UM08','  ','S',' ','          ','T6A1UM08                      ','UNDERWR1  ','UNDERWR1  ',current_timestamp);
COMMIT;



