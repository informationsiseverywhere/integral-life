
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'INVSPF' and column_name ='NOTIFINUM' ; 
  IF ( cnt > 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.INVSPF modify (NOTIFINUM VARCHAR2(14 CHAR)) ');
	EXECUTE IMMEDIATE (' UPDATE VM1DTA.INVSPF SET NOTIFINUM = substr((REPLACE(notifinum,''CLMNTF'')),1,8) WHERE NOTIFINUM IS NOT NULL ');
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.INVSPF modify (NOTIFINUM VARCHAR2(8 CHAR)) ');
  END IF;       
END;
/ 
commit;

DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'INVSPF' and column_name ='CLAIMANT' ; 
  IF ( cnt > 0 ) THEN
	
	EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.INVSPF modify (CLAIMANT VARCHAR2(8 CHAR)) ');
  END IF;       
END;
/ 
commit;