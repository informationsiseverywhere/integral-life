DECLARE
   cnt number(2,1) :=0;
BEGIN
SELECT count(1) INTO cnt FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'L2CLNTUPLD'; 
  IF ( cnt = 0 ) THEN
		insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('E','L2CLNTUPLD','Client Bulk Upload                                ','UNDERWR1','UNDERWR1  ',current_timestamp);
end if;
end;
/

commit;