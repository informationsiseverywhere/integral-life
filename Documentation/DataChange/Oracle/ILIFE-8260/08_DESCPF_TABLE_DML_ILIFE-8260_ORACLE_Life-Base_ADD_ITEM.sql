DECLARE
   cnt number(2,1) :=0;
BEGIN
SELECT count(1) INTO cnt FROM DESCPF WHERE DESCTABL='T1688' AND DESCITEM='BARD' AND DESCCOY = 2; 
  IF ( cnt = 0 ) THEN
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','BARD    ','  ','E',' ','Bulk Cl Cr','Bulk Client Creation          ','UNDERWR1  ','UNDERWR1',current_timestamp);
end if;
end;
/

commit;