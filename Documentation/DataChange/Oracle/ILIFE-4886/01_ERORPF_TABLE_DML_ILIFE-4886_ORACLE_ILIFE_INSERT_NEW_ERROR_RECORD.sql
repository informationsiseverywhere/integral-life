 DECLARE 
   cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM VM1DTA.ERORPF e
   WHERE  e.EROREROR='RPY5';
   IF ( cnt = 0 ) THEN
   INSERT INTO  VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
   VALUES('ER',' ','E','          ','RPY5','Surrender Factor not in T5617','0','0','0',' ','  ','  ','   ',current_timestamp) ;
   END IF;
 END;
 /
 commit; 