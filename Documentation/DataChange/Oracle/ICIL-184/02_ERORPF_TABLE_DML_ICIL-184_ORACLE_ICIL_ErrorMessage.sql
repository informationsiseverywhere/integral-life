DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RREL  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RREL  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RREL  ','Repayment amount required', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RREL  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RREL  ','需要还款金额', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
 COMMIT; 
  
 DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RREM  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RREM  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RREM  ','Repayment < min Amt required', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RREM  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RREM  ','还款金额小于最低还款额', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
COMMIT;


DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRFB  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRFB  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRFB  ','Loan Repayment Approved', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRFB  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRFB  ','批准贷款还款', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
COMMIT;


DELETE FROM VM1DTA.ERORPF WHERE EROREROR='RRFH  ';
COMMIT;

  DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
 where e.ERORPFX='ER' AND e.ERORLANG='E' AND e.EROREROR='RRFH  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'E','          ','RRFH  ','Prev Loan Repay not completed', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
    DECLARE 
  cnt number(2,1) :=0;
 BEGIN
   SELECT count(1) INTO cnt 
   FROM ERORPF e 
  where e.ERORPFX='ER' AND e.ERORLANG='S' AND e.EROREROR='RRFH  ';
  IF ( cnt = 0 ) THEN
    insert into ERORPF (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
	 values ( 'ER', ' ', 'S','          ','RRFH  ','Prev Loan Repay not completed', '', '', '', '', '', '',sysDate(),'');
  END IF;
  END;
  /
  
COMMIT;
  
  
 




