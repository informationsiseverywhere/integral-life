
DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T1675' and DESCITEM='TAFPD5H1'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFPD5H1','  ','E','','Life','Policy Loan/APL Repayment   ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFPD5H1','  ','S','','寿险','保单贷款/自动贷款还款','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T1675' and DESCITEM='TAFQD5H1'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFQD5H1','  ','E','','Life','Policy Loan/APL Repayment  ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFQD5H1','  ','S','','寿险','保单贷款/自动贷款还款','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
end if;
end;
/
commit;

DECLARE
   cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='T1675' and DESCITEM='TAFRD5H1'; 
  IF ( cnt = 0 ) THEN
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFRD5H1','  ','E','','Life','Policy Loan/APL Repayment ','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAFRD5H1','  ','S','','寿险','保单贷款/自动贷款还款','UNDERWR1','UNDERWR1',LOCALTIMESTAMP);	
end if;
end;
/
commit;
