

set define off;

delete from VM1DTA.DESCPF where DESCTABL='T1688' and DESCITEM='TAFP' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFP','  ','E',' ','TAFP  ','Loan Repmt Proposal','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFP','  ','S',' ','TAFP  ','贷款还款申请','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.DESCPF where DESCTABL='T1688' and DESCITEM='TAFQ' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFQ','  ','E',' ','TAFQ  ','Loan Repmt Proposal Reversal','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFQ','  ','S',' ','TAFQ  ','贷款还款申请还原','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.DESCPF where DESCTABL='T1688' and DESCITEM='TAFR' and (LANGUAGE = 'E' OR LANGUAGE = 'S');
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFR','  ','E',' ','TAFR  ','Loan Repmt Approval','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','TAFR','  ','S',' ','TAFR  ','贷款还款批准','UNDERWR1  ','UNDERWR1',current_timestamp);

COMMIT;
