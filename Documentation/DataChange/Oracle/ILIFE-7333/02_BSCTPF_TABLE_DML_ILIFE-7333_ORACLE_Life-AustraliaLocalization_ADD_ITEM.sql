DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCTPF 
  WHERE BSCHEDNAM = 'L2AUTOMATY' and DESC_T='Auto Maturity Batch                               ' ;
  IF ( cnt = 0 ) THEN
 insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('E','L2AUTOMATY','Auto Maturity Batch                               ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
  END IF;
END;
/

COMMIT;
