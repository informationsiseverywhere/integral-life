
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(*) into cnt FROM user_tables where table_name = 'BACRLDATA' ;  
  IF ( cnt = 0 ) THEN
    EXECUTE IMMediate (
    ' CREATE TABLE VM1DTA.BACRLDATA 
   ( UNIQUE_NUMBER NUMBER(18,0),
	 BACRTAPREC CHAR(500 CHAR) NULL,
	 MEMBER_NAME CHAR(10 CHAR) NULL,
	 CONSTRAINT PK_BACRLDATA PRIMARY KEY (UNIQUE_NUMBER))'
    );
  END IF;  

	EXECUTE IMMEDIATE ('COMMENT ON TABLE VM1DTA.BACRLDATA IS ''To store return file data to process in L2retbnk batch'''); 
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BACRLDATA.BACRTAPREC IS ''BACRTAPREC'''); 
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.BACRLDATA.MEMBER_NAME IS ''MEMBER NAME'''); 
		
END;
/

   
