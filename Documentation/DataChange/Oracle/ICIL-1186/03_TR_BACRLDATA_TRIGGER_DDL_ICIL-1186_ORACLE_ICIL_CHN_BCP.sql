
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_BACRLDATA" before insert on VM1DTA.BACRLDATA for each row
declare
       v_pkValue  number;
     begin
     select SEQ_BACRLDATA.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_BACRLDATA;
   /
