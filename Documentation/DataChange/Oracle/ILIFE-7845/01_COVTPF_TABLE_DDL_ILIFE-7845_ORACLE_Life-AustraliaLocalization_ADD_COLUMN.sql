DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'COVTPF' and column_name ='RISKPREM' ; 
  IF ( cnt = 0 ) THEN
   EXECUTE IMMEDIATE (' ALTER TABLE COVTPF ADD RISKPREM NUMBER(17,2) DEFAULT 0');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN COVTPF.RISKPREM  IS '' risk premium added ''');
  END IF;  
END;
/
commit;
