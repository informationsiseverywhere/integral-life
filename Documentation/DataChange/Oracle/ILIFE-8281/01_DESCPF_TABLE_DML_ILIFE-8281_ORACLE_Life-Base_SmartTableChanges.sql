DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1691' AND DESCITEM ='EPR2G3  ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1691','EPR2G3  ','  ','E','','EPR2G3    ','Document Management           ','ASHARMA228','ASHARMA228',sysdate );

DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1688' AND DESCITEM ='SRHA    ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','SRHA    ','  ','E','','DMS Search','Document Search               ','ASHARMA228','ASHARMA228',sysdate );

DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1688' AND DESCITEM ='SRH9    ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','SRH9    ','  ','E','','DMS       ','Document History              ','ASHARMA228','ASHARMA228',sysdate );

DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1688' AND DESCITEM ='SRH8    ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','SRH8    ','  ','E','','DMS Upload','Document Upload               ','ASHARMA228','ASHARMA228',sysdate );

DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1688' AND DESCITEM ='MRH7    ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1688','MRH7    ','  ','E','','DMS       ','Document Management           ','ASHARMA228','ASHARMA228',sysdate );

DELETE FROM VM1DTA.DESCPF WHERE DESCPFX = 'IT' AND DESCCOY='2' AND DESCTABL = 'T1691' AND DESCITEM ='EPR2DE  ';
INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT','2','T1691','EPR2DE  ','  ','E','','EPR2DE    ','File Upload                   ','RSURYA    ','RSURYA    ',sysdate );

commit;