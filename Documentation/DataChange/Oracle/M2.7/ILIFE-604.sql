update COVTPF set plnsfx=0 where plnsfx is NULL;
commit;

CREATE OR REPLACE FORCE VIEW "VM1DTA"."COVTTRM" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "SEQNBR", "TERMID", "TRDT", "TRTM", "USER_T", "CRTABLE", "RCESDTE", "PCESDTE", "RCESAGE", "PCESAGE", "RCESTRM", "PCESTRM", "SUMINS", "SINGP", "INSTPREM", "ZBINSTPREM", "ZLINSTPREM", "MORTCLS", "LIENCD", "JLIFE", "POLINC", "NUMAPP", "SEX01", "SEX02", "ANBCCD01", "ANBCCD02", "BILLFREQ", "BILLCHNL", "EFFDATE", "PAYRSEQNO", "CNTCURR", "BCESAGE", "BCESTRM", "BCESDTE", "BAPPMETH", "ZDIVOPT", "PAYCOY", "PAYCLT", "PAYMTH", "BANKKEY", "BANKACCKEY", "PAYCURR", "FACTHOUS", "USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
			PLNSFX,
            SEQNBR,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            CRTABLE,
            RCESDTE,
            PCESDTE,
            RCESAGE,
            PCESAGE,
            RCESTRM,
            PCESTRM,
            SUMINS,
            SINGP,
            INSTPREM,
            ZBINSTPREM,
            ZLINSTPREM,
            MORTCLS,
            LIENCD,
            JLIFE,
            POLINC,
            NUMAPP,
            SEX01,
            SEX02,
            ANBCCD01,
            ANBCCD02,
            BILLFREQ,
            BILLCHNL,
            EFFDATE,
            PAYRSEQNO,
            CNTCURR,
            BCESAGE,
            BCESTRM,
            BCESDTE,
            BAPPMETH,
            ZDIVOPT,
            PAYCOY,
            PAYCLT,
            PAYMTH,
            BANKKEY,
            BANKACCKEY,
            PAYCURR,
            FACTHOUS,
            USRPRF,
            JOBNM,
            DATIME
       FROM COVTPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            SEQNBR;