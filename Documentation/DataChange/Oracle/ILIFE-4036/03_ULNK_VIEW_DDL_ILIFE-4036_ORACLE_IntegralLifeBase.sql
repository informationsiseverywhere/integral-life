
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."ULNK" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "JLIFE", "COVERAGE", "RIDER", "PLNSFX", "TRANNO", "UALFND01", "UALFND02", "UALFND03", "UALFND04", "UALFND05", "UALFND06", "UALFND07", "UALFND08", "UALFND09", "UALFND10", "UALPRC01", "UALPRC02", "UALPRC03", "UALPRC04", "UALPRC05", "UALPRC06", "UALPRC07", "UALPRC08", "UALPRC09", "UALPRC10", "USPCPR01", "USPCPR02", "USPCPR03", "USPCPR04", "USPCPR05", "USPCPR06", "USPCPR07", "USPCPR08", "USPCPR09", "USPCPR10", "PRCAMTIND", "CURRFROM", "CURRTO", "VALIDFLAG", "PTOPUP", "USRPRF", "JOBNM", "DATIME", "FNDSPL") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            TRANNO,
            UALFND01,
            UALFND02,
            UALFND03,
            UALFND04,
            UALFND05,
            UALFND06,
            UALFND07,
            UALFND08,
            UALFND09,
            UALFND10,
            UALPRC01,
            UALPRC02,
            UALPRC03,
            UALPRC04,
            UALPRC05,
            UALPRC06,
            UALPRC07,
            UALPRC08,
            UALPRC09,
            UALPRC10,
            USPCPR01,
            USPCPR02,
            USPCPR03,
            USPCPR04,
            USPCPR05,
            USPCPR06,
            USPCPR07,
            USPCPR08,
            USPCPR09,
            USPCPR10,
            PRCAMTIND,
            CURRFROM,
            CURRTO,
            VALIDFLAG,
            PTOPUP,
            USRPRF,
            JOBNM,
            DATIME,
			FNDSPL
       FROM ULNKPF
      WHERE NOT (PTOPUP <> ' ')
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            UNIQUE_NUMBER DESC
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 ;
 
