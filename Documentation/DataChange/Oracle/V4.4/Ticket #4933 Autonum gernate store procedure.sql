
-- +===============================================================================================================================
-- |  Description:    
-- |		
-- |	  pick up polisy number proceduce
-- |
-- +================================================================================================================================
CREATE OR REPLACE PACKAGE types
AS
    TYPE ref_cursor IS REF CURSOR;
END;
/
create or replace PROCEDURE getAutoNumber(in_prefix CHAR, in_company CHAR, in_genkey CHAR, anum_cursor OUT types.ref_cursor) AS            
       
        AnRow ANUMPF%ROWTYPE;
        
        -- lock record to prevent duplicated auto number
        CURSOR SELECTED_ROWS IS
        SELECT * FROM VM1DTA.ANUMPF AN WHERE AN.PREFIX=in_prefix  AND AN.COMPANY=in_company AND AN.GENKEY=in_genkey 
        ORDER BY PREFIX ASC, COMPANY ASC, GENKEY ASC, AUTONUM ASC, UNIQUE_NUMBER ASC FOR UPDATE;
        
      BEGIN
		  -- pick one auto number
          OPEN SELECTED_ROWS;
          LOOP
              FETCH SELECTED_ROWS INTO anRow;
              IF SELECTED_ROWS%NOTFOUND THEN
                RAISE NO_DATA_FOUND;
              END IF;  
              
              -- return cursor for ResultSet  
              OPEN anum_cursor FOR 
              SELECT anRow.UNIQUE_NUMBER as UNIQUE_NUMBER, anRow.PREFIX as PREFIX , anRow.GENKEY as GENKEY, anRow.AUTONUM as AUTONUM
                    , anRow.USRPRF as USRPRF, anRow.JOBNM as JOBNM, anRow.DATIME as DATIME 
              FROM DUAL;

              DELETE from  ANUMPF AN
              WHERE AN.UNIQUE_NUMBER = anRow.UNIQUE_NUMBER;
              
              COMMIT;     
              EXIT;-- pick one only
          END LOOP;
          CLOSE SELECTED_ROWS;
          
END getAutoNumber;