execute dbms_stats.unlock_schema_stats ('VM1DTA'); 
execute dbms_stats.gather_schema_stats( ownname => 'VM1DTA',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('PAXUS'); 
execute dbms_stats.gather_schema_stats( ownname => 'PAXUS',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('VM1COM'); 
execute dbms_stats.gather_schema_stats( ownname => 'VM1COM',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('VM1EXC'); 
execute dbms_stats.gather_schema_stats( ownname => 'VM1EXC',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('QUIPOZ'); 
execute dbms_stats.gather_schema_stats( ownname => 'QUIPOZ',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('SESS'); 
execute dbms_stats.gather_schema_stats( ownname => 'SESS',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);

execute dbms_stats.unlock_schema_stats ('CSCSORT'); 
execute dbms_stats.gather_schema_stats( ownname => 'CSCSORT',estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,method_opt => 'FOR ALL COLUMNS SIZE AUTO', cascade => true);