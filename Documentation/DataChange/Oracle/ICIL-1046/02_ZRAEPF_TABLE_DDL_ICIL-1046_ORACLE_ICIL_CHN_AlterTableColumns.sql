DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZRAEPF' and column_name ='WDRGPYMOP' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE ZRAEPF ADD WDRGPYMOP CHAR(8 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.ZRAEPF.WDRGPYMOP IS ''WITHDRWAL PAYMENT METHOD'''); 
  END IF;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZRAEPF' and column_name ='WDBANKKEY' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE ZRAEPF ADD WDBANKKEY CHAR(10 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.ZRAEPF.WDBANKKEY IS ''WITHDRWAL BANK KEY'''); 
  END IF; 
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'ZRAEPF' and column_name ='WDBANKACCKEY' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE ZRAEPF ADD WDBANKACCKEY CHAR(20 CHAR) ');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.ZRAEPF.WDBANKACCKEY IS ''WITHDRWAL BANK KEY DESCRIPTION'''); 
  END IF;       
END;
/ 
  