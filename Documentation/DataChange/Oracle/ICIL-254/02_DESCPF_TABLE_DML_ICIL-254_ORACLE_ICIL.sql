﻿
delete from vm1dta.descpf where descpfx = 'IT' and desccoy = '2' and desctabl = 'T5645' and descitem = 'BD5HS   ';
delete from vm1dta.descpf where descpfx = 'IT' and desccoy = '2' and desctabl = 'T6598' and descitem = 'SC07    ';
delete from vm1dta.descpf where descpfx = 'HE' and desccoy = '2' and desctabl in ('TD5HP', 'TD5HQ', 'TD5HO');
delete from vm1dta.descpf where desccoy = '2' and desctabl in ('TD5HE', 'TD5HN');
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'T5645', 'BD5HS   ', ' ', 'E', 'LYKS', 'AutoRefund', 'Auto Refund Transaction Basis', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'T5645', 'BD5HS   ', ' ', 'S', 'LYKS', '自动退款', '自动退款', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HP', '        ', ' ', 'E', 'LYKS', 'RemainDay ', 'Part Surrender Basis RemainDay', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HP', '        ', ' ', 'S', 'LYKS', '剩余天数', '减保剩余天数', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HE', '        ', ' ', 'E', 'LYKS', 'TranBasPay', 'Tran Basis Payment Criteria', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HE', '        ', ' ', 'S', 'LYKS', '付款规则', '付款规则', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HE', 'TA83CNY ', ' ', 'E', 'LYKS', 'TranPSurr', 'Traditional Part Surrender', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HE', 'TA83CNY ', ' ', 'S', 'LYKS', '部分退保', '传统部分退保', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HE', 'T510CNY ', ' ', 'E', 'LYKS', 'NTranPSur', 'NonTraditional Part Surrender', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HE', 'T510CNY ', ' ', 'S', 'LYKS', '部分退保', '非传统部分退保', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HQ', '        ', ' ', 'E', 'LYKS', 'CPSurrBas', 'Component Part Surrender Basis', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HQ', '        ', ' ', 'S', 'LYKS', '退款规则', '退款规则', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HN', '        ', ' ', 'E', 'LYKS', 'PSurrCalBa', 'Part Surrender Calculate Basis', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HN', '        ', ' ', 'S', 'LYKS', '退款计算', '部分退款计算规则', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HN', 'D       ', ' ', 'E', 'LYKS', 'RemainDays', 'Remaining Number of Days', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HN', 'D       ', ' ', 'S', 'LYKS', '剩余天数', '剩余天数', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HN', 'M       ', ' ', 'E', 'LYKS', 'RemainMon', 'Remaining Number of Month', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'TD5HN', 'M       ', ' ', 'S', 'LYKS', '剩余月', '剩余月', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HO', '        ', ' ', 'E', 'LYKS', 'PSurrRM', 'Part Surr Basis Remain Months', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('HE', '2', 'TD5HO', '        ', ' ', 'S', 'LYKS', '退款月', '部分退款保持月', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'T6598', 'SC07    ', ' ', 'E', 'LYKS', 'SC07', 'Surrender Traditional 07', 'XMA3', 'XMA3', sysdate);
insert into vm1dta.descpf(DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
values('IT', '2', 'T6598', 'SC07    ', ' ', 'S', 'LYKS', 'SC07', '退保配置07', 'XMA3', 'XMA3', sysdate);
commit;      