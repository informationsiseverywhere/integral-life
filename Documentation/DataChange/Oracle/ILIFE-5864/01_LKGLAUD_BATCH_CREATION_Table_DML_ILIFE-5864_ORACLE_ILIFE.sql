set define off;
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCTPF f
  where bschednam = 'LKGLAUD';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCT (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME)
 values ('E','LKGLAUD','GL Audit Report                                   ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCDPF f
  where bauthcode = 'B233' and PRODCODE='L' AND bschednam = 'LKGLAUD'; 
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) 
values ('LKGLAUD',1,'N','N','B233','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,null);

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSPDPF f
  where bschednam = 'LKGLAUD' and COMPANY='7';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) 
values ('LKGLAUD','7','GLA-GLRPPF','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF f
where BPRIORPROC IN ('GLA-GLRPPF','GLA-TRANEX','GLA-REPORT') and  BPRIORCOY='7';
  IF ( cnt = 0 ) THEN
  Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GLA-TRANEX','7','GLA-REPORT','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GLA-GLRPPF','7','GLA-TRANEX','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);


  END IF;
END;
/




DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPRDPF f
 where BPROCESNAM IN ('GLA-GLRPPF','GLA-TRANEX','GLA-REPORT') and  COMPANY='7';
  IF ( cnt = 0 ) THEN
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','GLA-GLRPPF','00:00:00','23:59:59','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'GLRPPF    ','          ','          ','7A        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1  ','TAKAFULCOM',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','GLA-REPORT','00:00:00','23:59:59','B3615     ','                                                                                                                                                                                                                                                ',1,1,'P3613','        ','    ',' ','10',50,50,5000,'          ','          ','          ','7A        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1  ','TAKAFULCOM',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','GLA-TRANEX','00:00:00','23:59:59','B3614     ','                                                                                                                                                                                                                                                ',1,1,'P3613','        ','    ',' ','10',50,50,5000,'          ','          ','          ','7A        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1  ','TAKAFULCOM',CURRENT_TIMESTAMP);


  END IF;
END;
/



DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPPDPF f
 where bschednam = 'LKGLAUD' and COMPANY='7';
  IF ( cnt = 0 ) THEN
  Insert into VM1DTA.BPPDPF (BSCHEDNAM,BPARPSEQNO,COMPANY,BPARPPROG,USRPRF,JOBNM,DATIME) 
  values ('LKGLAUD',1,'7','P3613','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);

  END IF;
END;
/
