
set define off;
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCTPF f
  where bschednam = 'LKGLUPDATE';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCT (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME)
 values ('E','LKGLUPDATE','GL Update                                         ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCDPF f
  where bauthcode = 'B211' and PRODCODE='L' AND bschednam = 'LKGLUPDATE'; 
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) 
values ('LKGLUPDATE',1,'N','N','B211','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'Y');

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSPDPF f
  where bschednam = 'LKGLUPDATE' and COMPANY='7';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) 
values ('LKGLUPDATE','7','GL-BATC-CL','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF f
where BPRIORPROC IN ('GL-BATC-CL','GL-BAKX-CR','GL-BATC-EX','GL-GEXT-CR',
  'GL-TRAN-EX','GL-GTRN-CR','GLDELAY   ','GL-GLUPD  ','GL-ZSUN-CR','Br59c     '
  ,'GL-ZIFJ-CR','Br58h     ','GL-ZIFJ-C2','Br58i     ','GL-ZIFJ-C3',
  'Br58j     ') and  BPRIORCOY='7';
  IF ( cnt = 0 ) THEN
  
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','Br58h     ','7','GL-ZIFJ-C2','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','Br58i     ','7','GL-ZIFJ-C3','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','Br59c     ','7','GL-ZIFJ-CR','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-BAKX-CR','7','GL-BATC-EX','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-BATC-CL','7','GL-BAKX-CR','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-BATC-EX','7','GL-GEXT-CR','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GLDELAY   ','7','GL-GLUPD  ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-GEXT-CR','7','GL-TRAN-EX','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-GLUPD  ','7','GL-ZSUN-CR','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-GTRN-CR','7','GLDELAY   ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-TRAN-EX','7','GL-GTRN-CR','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-C2','7','Br58i     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-C3','7','Br58j     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-CR','7','Br58h     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) values ('7','GL-ZSUN-CR','7','Br59c     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);


  END IF;
END;
/




DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPRDPF f
 where BPROCESNAM IN ('GL-BATC-CL','GL-BAKX-CR','GL-BATC-EX','GL-GEXT-CR',
  'GL-TRAN-EX','GL-GTRN-CR','GLDELAY   ','GL-GLUPD  ','GL-ZSUN-CR','Br59c     '
  ,'GL-ZIFJ-CR','Br58h     ','GL-ZIFJ-C2','Br58i     ','GL-ZIFJ-C3',
  'Br58j     ') and  COMPANY='7';
  IF ( cnt = 0 ) THEN

Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','Br58h     ','00:00:00','00:00:00','Br58h     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'INTFNDJRN ','          ','X1        ','M1        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','Br58i     ','00:00:00','00:00:00','Br58i     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',10,50,5000,'INTFNDJRN ','Br58j     ','X2        ','M1        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','Br58j     ','00:00:00','00:00:00','Br58j     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'INTFNDJRN ','          ','X3        ','M1        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','Br59c     ','00:00:00','00:00:00','Br59c     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'INTSUNEXT1','          ','X1        ','M1        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-BAKX-CR','00:00:00','23:59:59','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'BAKXPF    ','          ','          ','M1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-BATC-CL','00:00:00','23:59:59','          ','CALL PGM(BCHPROG)                                                                                                                                                                                                                               ',1,1,'     ','        ','    ',' ','10',50,50,5000,'B0237     ','B3610     ','          ','          ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-BATC-EX','00:00:00','23:59:59','B0321     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'B3610     ','Y         ','          ','M1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GLDELAY   ','00:00:00','23:59:59','          ','DLYJOB 1                                                                                                                                                                                                                                        ',1,1,'     ','GLUPD   ','    ',' ','10',50,50,5000,'          ','          ','          ','          ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-GEXT-CR','00:00:00','23:59:59','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'GEXTPF    ','          ','          ','M1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-GLUPD  ','00:00:23','23:59:00','B3612     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'****      ','          ','          ','          ','          ',10,'*DATABASE ','2','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-GTRN-CR','00:00:00','23:59:59','B3611     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'****      ','          ','          ','M1        ','          ',10,'*DATABASE ','3','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-TRAN-EX','00:00:00','23:59:59','B3610     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'          ','          ','          ','M1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-C2','00:00:00','00:00:00','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'ZIFJPF    ','          ','          ','X2        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-C3','00:00:00','00:00:00','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'ZIFJPF    ','          ','          ','X3        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-ZIFJ-CR','00:00:00','00:00:00','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','**',50,50,5000,'ZIFJPF    ','          ','          ','X1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) values ('7','GL-ZSUN-CR','00:00:00','00:00:00','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','10',50,50,5000,'ZSUNPF    ','          ','          ','X1        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

