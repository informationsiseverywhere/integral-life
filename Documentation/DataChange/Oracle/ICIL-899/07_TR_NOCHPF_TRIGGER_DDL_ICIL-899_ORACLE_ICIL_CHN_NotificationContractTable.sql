CREATE OR REPLACE TRIGGER "VM1DTA"."TR_NOCHPF" before insert on VM1DTA.NOCHPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_NOCHPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_NOCHPF;
   /
