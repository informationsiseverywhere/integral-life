
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_NOTIPF" before insert on VM1DTA.NOTIPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_NOTIPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_NOTIPF;
   /
   
   
