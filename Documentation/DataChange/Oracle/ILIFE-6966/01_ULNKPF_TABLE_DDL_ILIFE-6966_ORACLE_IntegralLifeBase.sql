 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FNDSPL'
      and table_name = 'ULNKPF';
     

  if (v_column_exists = 0) then
      execute immediate ' ALTER TABLE VM1DTA.ULNKPF ADD FNDSPL nchar(4)  NOT NULL ';
  end if;
end;
/