DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FUNDPOOL'
      and table_name = 'INCIPF';
     

  if (v_column_exists = 0) then
      execute immediate ' ALTER TABLE  	VM1DTA.INCIPF  ADD (FUNDPOOL NCHAR(1) , PREMST	NUMERIC(17,2),PREMCURR	NCHAR(3),PCUNIT	NUMERIC(5,2),USPLITPC	NUMERIC(5,2) ) ';
  end if;
end;
/

