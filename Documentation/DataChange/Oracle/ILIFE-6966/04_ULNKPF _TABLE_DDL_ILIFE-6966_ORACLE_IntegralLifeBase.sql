 
 
 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FUNDPOOL01'
      and table_name = 'ULNKPF';
     

  if (v_column_exists = 0) then
      execute immediate '  ALTER TABLE  VM1DTA.ULNKPF   ADD  (FUNDPOOL01 NCHAR(1),FUNDPOOL02 NCHAR(1),FUNDPOOL03 NCHAR(1),FUNDPOOL04 NCHAR(1),FUNDPOOL05 NCHAR(1),
FUNDPOOL06 NCHAR(1),FUNDPOOL07 NCHAR(1),FUNDPOOL08 NCHAR(1),FUNDPOOL09 NCHAR(1),FUNDPOOL10 NCHAR(1)) ';
  end if;
end;
/
 