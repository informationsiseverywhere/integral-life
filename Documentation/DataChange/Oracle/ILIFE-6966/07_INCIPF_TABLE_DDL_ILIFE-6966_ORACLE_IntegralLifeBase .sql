  DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'PREMST05'
      and table_name = 'INCIPF';
     

  if (v_column_exists = 0) then
      execute immediate ' ALTER TABLE VM1DTA.INCIPF ADD (PREMST05 NUMERIC(17,2),PREMST06 NUMERIC(17,2),PREMST07 NUMERIC(17,2),PREMCURR05 NUMERIC(17,2),PREMCURR06 NUMERIC(17,2) ,PREMCURR07 NUMERIC(17,2),PCUNIT05 NUMERIC(5,2),PCUNIT06 NUMERIC(5,2),PCUNIT07 NUMERIC(5,2), USPLITPC05 NUMERIC(5,2),USPLITPC06 NUMERIC(5,2), USPLITPC07 NUMERIC(5,2))';
  end if;
end;
/

  