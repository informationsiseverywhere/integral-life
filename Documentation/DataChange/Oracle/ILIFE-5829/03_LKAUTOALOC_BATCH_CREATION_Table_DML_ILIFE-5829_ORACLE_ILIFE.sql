DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCTPF f
  WHERE bschednam = 'LKAUTOALOC';
  IF ( cnt = 0 ) THEN
 Insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values 
 ('E','LKAUTOALOC','Automatic Number Allocation - Company 7           ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCDPF f
  WHERE bauthcode = 'B239' and PRODCODE='L' and bschednam = 'LKAUTOALOC'; 
  IF ( cnt = 0 ) THEN
 Insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) values 
 ('LKAUTOALOC',1,'N','N','B239','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,null);
  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSPDPF f
  where bschednam = 'LKAUTOALOC' and COMPANY='7';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) 
values ('LKAUTOALOC','7','B2283     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPRDPF f
  where BPROCESNAM IN ('B2283') and  COMPANY='7';  
  IF ( cnt = 0 ) THEN
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,JOBNM,USRPRF,DATIME) 
values ('7','B2283     ','00:00:00','23:59:59','          ','CALL PGM(BCHPROG)                                                                                                                                                                                                                               ',1,1,'     ','        ','    ',' ','10',50,50,5000,'B2283     ','          ','          ','AN        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

commit;  

