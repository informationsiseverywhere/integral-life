

SET DEFINE OFF;
delete from VM1DTA.FLDTPF where FDID='AGENTCODE' and FDTX='AGENTCODE';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('AGENTCODE','E','AGENTCODE','Agent Ref number',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('AGENTCODE','S','AGENTCODE','??????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='DISTCODE' and FDTX='DISTCODE';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('DISTCODE','E','DISTCODE','District Code',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('DISTCODE','S','DISTCODE','????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='AGENCYMGR' and FDTX='AGENCYMGR';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('AGENCYMGR','E','AGENCYMGR','Agency Manager',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('AGENCYMGR','S','AGENCYMGR','?????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='BNKIND' and FDTX='BNKIND';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKIND','E','BNKIND','Bancas Indicator',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKIND','S','BNKIND','??????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='BNKINTROLE' and FDTX='BNKINTROLE';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKINTROLE','E','BNKINTROLE','Bank Internal Role',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKINTROLE','S','BNKINTROLE','??????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='BNKOUT' and FDTX='BNKOUT';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKOUT','E','BNKOUT','Bank Outlet',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKOUT','S','BNKOUT','????',0,0,CURRENT_TIMESTAMP);


delete from VM1DTA.FLDTPF where FDID='BNKTEL' and FDTX='BNKTEL';
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKTEL','E','BNKTEL','Bank Teller',0,0,CURRENT_TIMESTAMP);
insert into VM1DTA.FLDTPF (FDID,LANG,FDTX,COLH01,TRDT,TRTM,DATIME) values ('BNKTEL','S','BNKTEL','????',0,0,CURRENT_TIMESTAMP);


COMMIT;

