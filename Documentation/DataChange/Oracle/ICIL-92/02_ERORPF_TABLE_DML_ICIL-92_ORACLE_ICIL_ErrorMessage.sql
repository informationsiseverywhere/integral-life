DELETE FROM VM1DTA.ERORPF WHERE EROREROR = 'RRC3  ';
COMMIT;
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF h 
  WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E' AND h.EROREROR = 'RRC3  ';
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
    VALUES ('ER',' ','E','          ','RRC3  ', 'Entry not allowed',0,0,'36        ',null,'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,' ');    
  END IF;
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.ERORPF h 
  WHERE h.ERORPFX = 'ER' AND h.ERORLANG='S' AND h.EROREROR = 'RRC3  ';
  IF ( cnt = 0 ) THEN
    INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
    VALUES ('ER',' ','S','          ','RRC3  ', '不允许进入',0,0,'36        ',null,'UNDERWR1','UNDERWR1',LOCALTIMESTAMP,' ');    
  END IF;
END;
/


COMMIT;
