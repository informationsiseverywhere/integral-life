
DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'PYOUPF' and column_name ='RLDGACCT' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE PYOUPF ADD RLDGACCT CHAR(16) ');
              EXECUTE IMMEDIATE ('COMMENT ON COLUMN PYOUPF.RLDGACCT IS ''RLDGACCT''');
  END IF;   
END;
/
