

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97B2498') AND  COMPANY='2';
  IF ( cnt > 0 ) THEN 
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97B2498', '00:00:00', '00:00:00', 'B2498     ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR346', 'DEBITS02', '    ', ' ', '20', 50, 50, 5000, '          ', 'D         ', '97        ', '          ', '          ', 100, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97B3289') AND  COMPANY='2' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97B3289', '00:00:00', '00:00:00', 'B3289     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '20', 50, 50, 5000, '          ', 'BR359     ', '97        ', 'DD        ', '          ', 10, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BACO') AND  COMPANY='2';
  IF ( cnt > 0 ) THEN 
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BACO ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '20', 50, 50, 5000, 'BACOPF    ', '          ', '          ', 'L2        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BACT') AND  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BACT ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '20', 50, 50, 5000, 'BACTPF    ', '          ', '          ', 'L2        ', '          ', 100, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BR22E') AND  COMPANY='2' ;
  IF ( cnt > 0 ) THEN 
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BR22E', '00:00:00', '00:00:00', '          ', 'CALL PGMBCHPROG                                                                                                                                                                                                                                 ', 1, 1, '     ', '        ', '    ', ' ', '20', 50, 50, 5000, 'CR22E     ', '          ', '97        ', 'L2        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BR359') AND  COMPANY='2' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BR359', '00:00:00', '00:00:00', 'BR359     ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR346', '        ', '    ', ' ', '20', 50, 50, 5000, '          ', '          ', '97        ', 'DDL2      ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ',CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BR360') AND  COMPANY='2' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BR360', '00:00:00', '00:00:00', '          ', 'CALL PGMBCHPROG                                                                                                                                                                                                                                 ', 1, 1, '     ', '        ', '    ', ' ', '20', 50, 50, 5000, 'CR360     ', 'DD97      ', '          ', 'L2        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;




