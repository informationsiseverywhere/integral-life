
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF where DESCtabl='TR376' and DESCCOY='9' and DESCITEM like 'GCDC    ' ;
  IF ( cnt > 0 ) THEN 
	DELETE from VM1DTA.DESCPF where DESCtabl='TR376' and DESCCOY='9' and DESCITEM like 'GCDC    ';

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCtabl ='TR376' and DESCITEM ='GCDC    ' and DESCCOY='9'	;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO  VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
	VALUES
	( 'IT', '9', 'TR376', 'GCDC    ', '  ', 'E', 'GCDC      ', 'Gateway CC Recurring         ', 'UNDERWR1  ', 'UNDERWR1  ',   		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
	END IF;
END;
/
COMMIT;


----------------------------------------------------------------------------------------------------------------------------------
DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF where DESCtabl='T3672' and DESCCOY='2' and DESCITEM like 'G       ' ;
  IF ( cnt > 0 ) THEN 
	DELETE from VM1DTA.DESCPF where DESCtabl='T3672' and DESCCOY='2' and DESCITEM like 'G       ';

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCtabl ='T3672' and DESCITEM ='G       ' and DESCCOY='2' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO  VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
	VALUES
	( 'IT', '2', 'T3672', 'G       ', '  ', 'E', 'G         ', 'Gateway CC Recurring         ', 'UNDERWR1  ', 'UNDERWR1  ',   		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
	END IF;
END;
/
COMMIT;



-------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOT1 ' ;
  IF ( cnt > 0 ) THEN 
	DELETE from VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOT1 ';

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCtabl ='TR371' and DESCITEM ='GCDCOT1 ' and DESCCOY='9' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO  VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
	VALUES
	( 'IT', '9', 'TR371', 'GCDCOT1 ', '  ', 'E', 'GCDCOT1   ', 'Gateway CC Credit Trailer    ', 'UNDERWR1  ', 'UNDERWR1  ',   		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
	END IF;
END;
/
COMMIT;


-------------------------------------------------------------------------------------------------------------------------------------------

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOD1 ' ;
  IF ( cnt > 0 ) THEN 
	DELETE from VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOD1 ';

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCtabl ='TR371' and DESCITEM ='GCDCOD1 ' and DESCCOY='9' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO  VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
	VALUES
	( 'IT', '9', 'TR371', 'GCDCOD1 ', '  ', 'E', 'GCDCOD1   ', 'Gateway CC Credit Detail     ', 'UNDERWR1  ', 'UNDERWR1  ',   		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
	END IF;
END;
/
COMMIT;


-------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOA1 ' ;
  IF ( cnt > 0 ) THEN 
	DELETE from VM1DTA.DESCPF where DESCtabl='TR371' and DESCCOY='9' and DESCITEM like 'GCDCOA1 ';

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCtabl ='TR371' and DESCITEM ='GCDCOA1 ' and DESCCOY='9' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO  VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) 
	VALUES
	( 'IT', '9', 'TR371', 'GCDCOA1 ', '  ', 'E', 'GCDCOA1   ', 'Gateway CC Credit Header     ', 'UNDERWR1  ', 'UNDERWR1  ',   		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
	END IF;
END;
/
COMMIT;
----------------------------------------------------------------------------------------------------------------------------------------------