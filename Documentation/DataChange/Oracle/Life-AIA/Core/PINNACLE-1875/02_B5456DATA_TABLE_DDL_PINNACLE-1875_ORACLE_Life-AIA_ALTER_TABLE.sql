DECLARE
cnt number(2,1) := 0;
BEGIN
SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5456DATA' and column_name ='RRTDAT' ;
IF ( cnt = 0 )THEN
EXECUTE IMMEDIATE ('ALTER TABLE B5456DATA ADD RRTDAT int DEFAULT 0 ');
END IF;
END;
/
COMMIT;

DECLARE
cnt number(2,1) := 0;
BEGIN
SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'B5456DATA' and column_name ='RRTFRM' ;
IF ( cnt = 0 )THEN
EXECUTE IMMEDIATE ('ALTER TABLE B5456DATA ADD RRTFRM int DEFAULT 0 ');
END IF;
END;
/