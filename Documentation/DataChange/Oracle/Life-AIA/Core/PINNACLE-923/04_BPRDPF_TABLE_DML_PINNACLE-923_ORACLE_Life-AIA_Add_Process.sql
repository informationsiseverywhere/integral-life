DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRGCRT') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRGCRT ', '00:00:00', '23:59:59', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', 'Y', '10', 50, 50, 5000, 'GCRTPF    ', '          ', '          ', 'L2        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBH5GE') and  COMPANY='2'  ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBH5GE', '00:00:00', '23:59:59', 'BH5GE     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', 'Y', '10', 50, 50, 5000, 'GCRT      ', '          ', 'L2        ', '          ', '          ', 100, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBH5GK') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF (COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBH5GK', '00:00:00', '23:59:59', 'BH5GK     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'GCRT      ', '          ', 'L2        ', '          ', '          ', 100, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ',CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;




