CREATE OR REPLACE FORCE VIEW "VM1DTA"."LINPREV" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "INSTFROM", "INSTTO", "INSTAMT01", "INSTAMT02", "INSTAMT03", "INSTAMT04", "INSTAMT05", "INSTAMT06", "PAYFLAG", "USRPRF", "JOBNM", "DATIME", "PRORATEREC") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            INSTFROM,
            INSTTO,
            INSTAMT01,
            INSTAMT02,
            INSTAMT03,
            INSTAMT04,
            INSTAMT05,
            INSTAMT06,
            PAYFLAG,
            USRPRF,
            JOBNM,
            DATIME,
			PRORATEREC
       FROM VM1DTA.LINSPF
      WHERE PAYFLAG = 'P';