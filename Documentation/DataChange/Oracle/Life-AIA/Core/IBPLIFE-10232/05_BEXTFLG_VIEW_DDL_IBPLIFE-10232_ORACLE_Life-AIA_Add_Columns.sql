
CREATE OR REPLACE VIEW "VM1DTA"."BEXTFLG"("UNIQUE_NUMBER",
            "FACTHOUS",
            "CHDRCOY",
            "CHDRNUM",
            "BILLDATE",
            "CHDRPFX",
            "SERVUNIT",
            "CNTTYPE",
            "CNTCURR",
            "OCCDATE",
            "CCDATE",
            "PTDATE",
            "BTDATE",
            "BILLCHNL",
            "BANKCODE",
            "INSTFROM",
            "INSTTO",
            "INSTBCHNL",
            "INSTCCHNL",
            "INSTFREQ",
            "INSTAMT01",
            "INSTAMT02",
            "INSTAMT03",
            "INSTAMT04",
            "INSTAMT05",
            "INSTAMT06",
            "INSTAMT07",
            "INSTAMT08",
            "INSTAMT09",
            "INSTAMT10",
            "INSTAMT11",
            "INSTAMT12",
            "INSTAMT13",
            "INSTAMT14",
            "INSTAMT15",
            "INSTJCTL",
            "GRUPKEY",
            "MEMBSEL",
            "BANKKEY",
            "BANKACCKEY",
            "COWNPFX",
            "COWNCOY",
            "COWNNUM",
            "PAYRPFX",
            "PAYRCOY",
            "PAYRNUM",
            "CNTBRANCH",
            "AGNTPFX",
            "AGNTCOY",
            "AGNTNUM",
            "PAYFLAG",
            "BILFLAG",
            "OUTFLAG",
            "SUPFLAG",
            "MANDREF",
            "BILLCD",
            "SACSCODE",
            "SACSTYP",
            "GLMAP",
            "MANDSTAT",
            "DDDEREF",
            "EFFDATEX",
            "DDRSNCDE",
            "CANFLAG",
            "COMPANY",
            "JOBNO",
            "JOBNM",
            "USRPRF",
            "DATIME",
			"RDOCNUM") AS

SELECT UNIQUE_NUMBER,
            FACTHOUS,
            CHDRCOY,
            CHDRNUM,
            BILLDATE,
            CHDRPFX,
            SERVUNIT,
            CNTTYPE,
            CNTCURR,
            OCCDATE,
            CCDATE,
            PTDATE,
            BTDATE,
            BILLCHNL,
            BANKCODE,
            INSTFROM,
            INSTTO,
            INSTBCHNL,
            INSTCCHNL,
            INSTFREQ,
            INSTAMT01,
            INSTAMT02,
            INSTAMT03,
            INSTAMT04,
            INSTAMT05,
            INSTAMT06,
            INSTAMT07,
            INSTAMT08,
            INSTAMT09,
            INSTAMT10,
            INSTAMT11,
            INSTAMT12,
            INSTAMT13,
            INSTAMT14,
            INSTAMT15,
            INSTJCTL,
            GRUPKEY,
            MEMBSEL,
            BANKKEY,
            BANKACCKEY,
            COWNPFX,
            COWNCOY,
            COWNNUM,
            PAYRPFX,
            PAYRCOY,
            PAYRNUM,
            CNTBRANCH,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            PAYFLAG,
            BILFLAG,
            OUTFLAG,
            SUPFLAG,
            MANDREF,
            BILLCD,
            SACSCODE,
            SACSTYP,
            GLMAP,
            MANDSTAT,
            DDDEREF,
            EFFDATEX,
            DDRSNCDE,
            CANFLAG,
            COMPANY,
            JOBNO,
            JOBNM,
            USRPRF,
            DATIME,
			RDOCNUM
     FROM VM1DTA.BEXTPF;
      