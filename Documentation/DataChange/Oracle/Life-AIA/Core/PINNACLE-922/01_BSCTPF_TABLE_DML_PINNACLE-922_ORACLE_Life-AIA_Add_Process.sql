DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BSCTPF WHERE bschednam='L2GCCR' AND LANGUAGE='E';
  IF ( cnt = 0 ) THEN 
INSERT INTO VM1DTA.BSCTPF ( LANGUAGE, BSCHEDNAM, DESC_T, USRPRF, JOBNM, DATIME) VALUES ( 'E', 'L2GCCR    ', 'Gateway CC Recurring Extraction and File Create   ', 'UNDERWR1  ', 'UNDERWR1  ',CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



