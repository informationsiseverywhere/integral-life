DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRB3289') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRB3289', '00:00:00', '00:00:00', 'B3289     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, '          ', 'BR359     ', '96        ', 'CC        ', '          ', 10, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBACO') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBACO ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'BACOPF    ', '          ', '          ', 'L7        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBACT') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF (  COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBACT ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'BACTPF    ', '          ', '          ', 'L7        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBH5FX') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBH5FX', '00:00:00', '00:00:00', 'BH5FX     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', 'BBK2', 'Y', '10', 50, 50, 5000, '          ', 'A         ', '96        ', '          ', '          ', 100, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBH5FY') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBH5FY', '00:00:00', '00:00:00', 'BH5FY     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', 'BBK2', 'Y', '10', 50, 50, 5000, '          ', 'A         ', '          ', '          ', '          ', 100, '*DATABASE ', '2', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBR359') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBR359', '00:00:00', '00:00:00', 'BR359     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, '          ', '          ', '96        ', 'CCL7      ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;


DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('LGCCRBR360') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ('2', 'LGCCRBR360', '00:00:00', '00:00:00', '          ', 'CALL PGM(BCHPROG)                                                                                                                                                                                                                               ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'CR360     ', 'AIAA_     ', 'PURCHASES_', 'L7        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;