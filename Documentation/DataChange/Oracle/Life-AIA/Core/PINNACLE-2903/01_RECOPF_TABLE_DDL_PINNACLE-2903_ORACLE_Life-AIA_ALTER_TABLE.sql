DECLARE 
	cnt number :=0;
BEGIN    
	SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RECOPF' and column_name ='ANNPREM' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE RECOPF ADD ANNPREM numeric(17,2) ');
	
  END IF;  
END;
/
COMMIT;