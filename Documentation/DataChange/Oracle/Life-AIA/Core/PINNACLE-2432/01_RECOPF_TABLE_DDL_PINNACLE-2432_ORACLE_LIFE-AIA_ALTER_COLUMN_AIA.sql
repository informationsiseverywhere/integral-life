DECLARE
cnt number(2,1) := 0;
BEGIN
SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RECOPF' and column_name ='RPRATE' ;
IF ( cnt = 0 )THEN
EXECUTE IMMEDIATE ('ALTER TABLE RECOPF ADD RPRATE int DEFAULT 0 ');
END IF;
END;
/
COMMIT;

