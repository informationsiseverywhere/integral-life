
DECLARE 
	cnt number :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RACDPF' and column_name ='PENDCSTTO' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE RACDPF ADD PENDCSTTO NUMBER(8) ');
	END IF;  
	SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RACDPF' and column_name ='PRORATEFLAG' ; 
  IF ( cnt = 0 ) THEN
	EXECUTE IMMEDIATE (' ALTER TABLE RACDPF ADD PRORATEFLAG nchar(1) ');
	
  END IF;  
END;
/



