DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL ='TR371' AND DESCITEM LIKE ('97DC%') ;
  IF ( cnt > 0 ) THEN 
	DELETE FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL ='TR371' AND DESCITEM LIKE ('97DC%');
	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR371' AND DESCITEM IN ('97DCOA1') AND LANGUAGE = 'E' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '9', 'TR371', '97DCOA1 ', '  ', 'E','97DCOA1   ',  'AIA Direct Credit Header      ', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP );

	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR371' AND DESCITEM IN ('97DCOD1') AND LANGUAGE = 'E';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '9', 'TR371', '97DCOD1 ', '  ', 'E','97DCOD1   ',  'AIA Direct Credit Detail      ', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP );

	END IF;
END;
/
COMMIT;

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR371' AND DESCITEM IN ('97DCOT1') AND LANGUAGE = 'E' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.DESCPF ( DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '9', 'TR371', '97DCOT1 ', '  ', 'E','97DCOT1   ',  'AIA Direct Credit Trailer     ', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP );

	END IF;
END;
/
COMMIT;