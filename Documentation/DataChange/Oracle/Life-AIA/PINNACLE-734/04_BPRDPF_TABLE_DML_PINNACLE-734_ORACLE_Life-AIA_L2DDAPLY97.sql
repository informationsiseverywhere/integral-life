

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('B3290-97  ') and  COMPANY='2' ;
  IF ( cnt > 0 ) THEN 
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'B3290-97  ', '00:00:00', '00:00:00', 'B3290     ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR344', '        ', '    ', ' ', '40', 50, 50, 5000, '          ', '97        ', 'UPBBAL    ', '2A        ', '          ', 100, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('B3291-97  ') and  COMPANY='2'  ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF (  COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'B3291-97  ', '00:00:00', '00:00:00', 'B3291     ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR344', 'DDAPPLY ', 'TR3T', 'Y', '10', 50, 50, 5000, '3         ', '97        ', 'UPBBAL    ', '2A        ', '          ', 100, '*DATABASE ', '3', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;




DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('B3721-97  ') and  COMPANY='2';
  IF ( cnt > 0 ) THEN 
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'B3721-97  ', '00:00:00', '00:00:00', 'B3721     ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR344', '        ', '    ', ' ', '40', 50, 50, 5000, '          ', '97        ', '          ', 'FP        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('B3722-97  ') and  COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'B3722-97  ', '00:00:00', '00:00:00', 'B3722     ', '                                                                                                                                                                                                                                                ', 2, 1, 'PR344', '        ', '    ', ' ', '40', 50, 50, 5000, '          ', '97        ', '          ', 'FP        ', '          ', 10, '*DATABASE ', '3', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ',  CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;





DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('BBAX-CR97 ') and  COMPANY='2' ;
  IF ( cnt > 0 ) THEN 
	INSERT  INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'BBAX-CR97 ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 2, 2, 'PR344', '        ', '    ', ' ', '40', 50, 50, 5000, 'BBAXPF    ', '97        ', '          ', 'FP        ', '          ', 100, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ',  CURRENT_TIMESTAMP);
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('FDD97BEXX') and  COMPANY='2'  ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'FDD97BEXX ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, 'PR344', '        ', '    ', ' ', '10', 50, 50, 5000, 'BEXXPF    ', '          ', '          ', '2A        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ',  CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;




