DECLARE 
  cnt number(2,1) := 0;
BEGIN
  SELECT count(*) into cnt FROM sys.columns WHERE name = 'RRTDAT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA');  
  IF ( cnt = 0  )THEN
  ALTER TABLE VM1DTA.B5456DATA ADD RRTDAT int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name='MS_Description', @value='<co-contribution added>' , @level0type='SCHEMA',@level0name='VM1DTA', @level1type='TABLE',@level1name='B5456DATA', @level2type='COLUM',@level2name='RRTDAT ';
		END IF;  
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) := 0;
BEGIN
  SELECT count(*) into cnt FROM sys.columns WHERE name = 'RRTFRM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA');  
  IF ( cnt = 0  )THEN
  ALTER TABLE VM1DTA.B5456DATA ADD RRTFRM int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name='MS_Description', @value='<co-contribution added>' , @level0type='SCHEMA',@level0name='VM1DTA', @level1type='TABLE',@level1name='B5456DATA', @level2type='COLUM',@level2name='RRTFRM ';
		END IF;  
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) := 0;
BEGIN
  SELECT count(*) into cnt FROM sys.columns WHERE name = 'CORRTDAT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA');  
  IF ( cnt = 0  )THEN
  ALTER TABLE VM1DTA.B5456DATA ADD CORRTDAT int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name='MS_Description', @value='<co-contribution added>' , @level0type='SCHEMA',@level0name='VM1DTA', @level1type='TABLE',@level1name='B5456DATA', @level2type='COLUM',@level2name='CORRTDAT ';
		END IF;  
END;
/
COMMIT;

DECLARE 
  cnt number(2,1) := 0;
BEGIN
  SELECT count(*) into cnt FROM sys.columns WHERE name = 'CORRTFRM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA');  
  IF ( cnt = 0  )THEN
		ALTER TABLE VM1DTA.B5456DATA ADD CORRTFRM int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name='MS_Description', @value='<co-contribution added>' , @level0type='SCHEMA',@level0name='VM1DTA', @level1type='TABLE',@level1name='B5456DATA', @level2type='COLUM',@level2name='CORRTFRM ';
		END IF;  
END;
/
COMMIT;
