
CREATE OR REPLACE VIEW "VM1DTA"."RACDRCO"("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "RASNUM", "TRANNO", "SEQNO", "VALIDFLAG", "CURRCODE", "CURRFROM", "CURRTO", "RETYPE", "RNGMNT", "RAAMOUNT", "CTDATE", "CMDATE", "REASPER", "RREVDT", "RECOVAMT", "CESTYPE", "OVRDIND", "LRKCLS", "JOBNM", "USRPRF", "DATIME","RRTDAT","RRTFRM") AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            RASNUM,
            TRANNO,
            SEQNO,
            VALIDFLAG,
            CURRCODE,
            CURRFROM,
            CURRTO,
            RETYPE,
            RNGMNT,
            RAAMOUNT,
            CTDATE,
            CMDATE,
            REASPER,
            RREVDT,
            RECOVAMT,
            CESTYPE,
            OVRDIND,
            LRKCLS,
            JOBNM,
            USRPRF,
            DATIME,
			RRTDAT,
			RRTFRM


       FROM VM1DTA.RACDPF;
      