

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2CLSACMTH' and COMPANY='2';
  IF ( cnt > 0 ) THEN 
	delete from VM1DTA.BSPDPF where bschednam = 'L2CLSACMTH' and COMPANY='2';
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2CLSACMTH' and COMPANY='2';
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BSPDPF ( BSCHEDNAM, COMPANY, BPROCESNAM, USRPRF, JOBNM, DATIME) VALUES ('L2CLSACMTH', '2', '#BH5EO    ', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP );

	END IF;
END;
/
COMMIT;
