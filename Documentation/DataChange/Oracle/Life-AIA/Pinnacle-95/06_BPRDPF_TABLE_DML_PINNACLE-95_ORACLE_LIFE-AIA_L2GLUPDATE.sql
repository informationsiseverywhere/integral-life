





DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('GL-TRAN-EX','GL-GTRN-CR', 'ZSND-CR', 'ZSUN-CP')  and  COMPANY='2' ;
  IF ( cnt > 0 ) THEN 
	delete from vm1dta.BPRDPF where BPROCESNAM IN ('GL-TRAN-EX','GL-GTRN-CR', 'ZSND-CR', 'ZSUN-CP')  and  COMPANY='2';
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BPRDPF WHERE BPROCESNAM IN ('GL-TRAN-EX','GL-GTRN-CR', 'ZSND-CR', 'ZSUN-CP')  and  COMPANY='2' ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'GL-TRANSUN', '00:00:00', '23:59:59', 'BH5GG     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, '          ', '          ', '2U        ', 'M1        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'GL-GTRN-CR', '00:00:00', '23:59:59', 'B3611     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, '****      ', '          ', '2U        ', 'M1        ', '          ', 10, '*DATABASE ', '3', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'ZSND-CR   ', '00:00:00', '00:00:00', 'CRTTMPF   ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'ZSNDPF    ', '          ', '          ', '2U        ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);
	INSERT INTO VM1DTA.BPRDPF ( COMPANY, BPROCESNAM, BERLSTRTIM, BLATSTRTIM, BPROGRAM, BCOMANDSTG, BTHRDSTPRC, BTHRDSSPRC, BPARPPROG, RRULE, BAUTHCODE, BCHCLSFLG, BDEFBRANCH, BSCHEDPRTY, BSYSJOBPTY, BSYSJOBTIM, BPSYSPAR01, BPSYSPAR02, BPSYSPAR03, BPSYSPAR04, BPSYSPAR05, BCYCPERCMT, BPRCRUNLIB, BPRESTMETH, BCRITLPROC, BMAXCYCTIM, MULBRN, MULBRNTP, PRODCODE, USRPRF, JOBNM, DATIME) VALUES ( '2', 'ZSUN-CP   ', '00:00:00', '00:00:00', 'BH5EQ     ', '                                                                                                                                                                                                                                                ', 1, 1, '     ', '        ', '    ', ' ', '10', 50, 50, 5000, 'ZSND      ', '          ', '2U        ', '          ', '          ', 10, '*DATABASE ', '1', '3', 0, ' ', ' ', 'L', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;





