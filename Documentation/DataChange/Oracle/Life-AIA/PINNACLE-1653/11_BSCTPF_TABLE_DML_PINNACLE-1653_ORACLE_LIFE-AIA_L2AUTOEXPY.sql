

DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'L2AUTOEXPY'  ;
  IF ( cnt > 0 ) THEN 
	delete FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'L2AUTOEXPY';
	END IF;
END;
/
COMMIT;



DECLARE
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
	FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'L2AUTOEXPY'  ;
  IF ( cnt = 0 ) THEN 
	
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE, BSCHEDNAM, DESC_T, USRPRF, JOBNM, DATIME, CREATED_AT) VALUES ('E', 'L2AUTOEXPY', 'Auto Expiry Batch                                 ', 'UNDERWR1  ', 'UNDERWR1  ', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

	END IF;
END;
/
COMMIT;



