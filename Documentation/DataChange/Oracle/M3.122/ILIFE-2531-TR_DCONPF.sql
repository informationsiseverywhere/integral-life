SET DEFINE OFF;
CREATE OR REPLACE TRIGGER "VM1DTA"."TR_DCONPF" 
before insert on  DCONPF
for each row
declare
v_pkValue  number;
v_flag     exception;
v_rownum   number;
PRAGMA EXCEPTION_INIT (v_flag, -1);  -- it is assumed
begin
v_rownum := 0;
SELECT COUNT(*) INTO v_rownum  FROM DCONPF
WHERE (DRYSUBR= :New.DRYSUBR AND BSHDTHRDNO= :New.BSHDTHRDNO AND DRYENTCOY=
:New.DRYENTCOY AND DRYENTBRN= :New.DRYENTBRN AND EFFDATE= :New.EFFDATE AND
DRYRUNNO= :New.DRYRUNNO)
;
if (v_rownum > 0 ) then
RAISE v_flag;
else
select SEQ_DCONPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
end if;
end TR_DCONPF;