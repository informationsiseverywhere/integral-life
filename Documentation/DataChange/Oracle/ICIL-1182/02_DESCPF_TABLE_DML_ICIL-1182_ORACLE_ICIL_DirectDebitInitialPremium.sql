
SET DEFINE OFF;
--TR2A5
delete 	from VM1DTA.DESCPF 	WHERE DESCPFX='IT' and DESCCOY='2' and DESCTABL='TR2A5' and DESCITEM='NBPRP105';
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','NBPRP105','  ','E',' ','NBPRP105','2CH Direct Debit for Init Prem','UNDERWR1','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','NBPRP105','  ','S',' ','NBPRP105','2CH 直接借记初始保费','UNDERWR1','UNDERWR1',current_timestamp);
commit;
--T5645
delete 	from VM1DTA.DESCPF 	WHERE DESCPFX='IT' and DESCCOY='2' and DESCTABL='T5645' and DESCITEM='PR556';
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','PR556','  ','E',' ','PR556','Underwritting Approval Update','UNDERWR1','UNDERWR1',current_timestamp);
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','PR556','  ','S',' ','PR556','核保审批更新','UNDERWR1','UNDERWR1',current_timestamp);
commit;