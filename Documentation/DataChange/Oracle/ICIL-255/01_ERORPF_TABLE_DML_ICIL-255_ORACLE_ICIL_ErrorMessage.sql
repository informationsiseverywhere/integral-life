
SET DEFINE OFF;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR='RRFL  ';
COMMIT;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRFL  ';
COMMIT;

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','S','          ','RRFL  ','Warning: SV Debt > Total Premium paid',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRFL  ','Warning: SV Debt > Total Premium paid',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;



delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR='RRFO  ';
COMMIT;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRFO  ';
COMMIT;

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','S','          ','RRFO  ','Do you want to proceed?',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRFO  ','Do you want to proceed?',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;



delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR='RRFN  ';
COMMIT;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRFN  ';
COMMIT;

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','S','          ','RRFN  ','Surrender amt exceeds the limit',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRFN  ','Surrender amt exceeds the limit',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;




delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR='RRFM  ';
COMMIT;
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRFM  ';
COMMIT;

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','S','          ','RRFM  ','Withdraw Annuity',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER',' ','E','          ','RRFM  ','Withdraw Annuity',0,0,' ',null,'UNDERWR1','CHINALOC',current_timestamp);
COMMIT;

 