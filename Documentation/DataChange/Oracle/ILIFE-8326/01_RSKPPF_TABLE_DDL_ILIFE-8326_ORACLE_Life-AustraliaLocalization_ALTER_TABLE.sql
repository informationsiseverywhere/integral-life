DECLARE 
  cnt number(2,1) :=0;
BEGIN  
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'RSKPPF' and column_name ='POLFEE' ; 
  IF ( cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE RSKPPF ADD POLFEE NUMERIC(17,2) DEFAULT ''0''');
	EXECUTE IMMEDIATE ('COMMENT ON COLUMN VM1DTA.RSKPPF.POLFEE IS ''POLFEE'''); 
  END IF;  
END;
/
