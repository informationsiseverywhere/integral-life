declare
vcnt number := 0;
BEGIN
   SELECT COUNT(*) into vcnt  FROM VM1DTA.ERORPF WHERE eroreror like 'H017  ' AND erorlang='J';
   IF(vcnt > 1)
   then    
 UPDATE VM1DTA.ERORPF SET  erordesc=N'未決のフォローアップ' WHERE eroreror like 'H017  ' AND erorlang='J';
  end if;
END;
/
COMMIT;