
DECLARE 
cnt NUMBER(2,1):=0;
BEGIN
SELECT COUNT(1) INTO cnt
FROM VM1DTA.FLDTPF
WHERE FDID LIKE '%FUPCDESA%' AND LANG='E';
IF(cnt > 0 ) THEN
DELETE FROM VM1DTA.FLDTPF WHERE FDID LIKE '%FUPCDESA%' AND LANG='E';
INSERT INTO  VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TRDT,TRTM,DATIME) 
VALUES('FUPCDESA','E','FOLLOW-UP CODE FOR SCREEN','Follow Up','Code for','Screen',0,0,CURRENT_TIMESTAMP);
ELSIF (cnt=0) THEN
INSERT INTO  VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TRDT,TRTM,DATIME) 
VALUES('FUPCDESA','E','FOLLOW-UP CODE FOR SCREEN','Follow Up','Code for','Screen',0,0,CURRENT_TIMESTAMP);
END IF;
END;
/
COMMIT;


DECLARE 
cnt NUMBER(2,1):=0;
BEGIN
SELECT COUNT(1) INTO cnt
FROM VM1DTA.FLDTPF
WHERE FDID LIKE '%FUPCDESA%' AND LANG='S';
IF(cnt > 0 ) THEN
DELETE FROM VM1DTA.FLDTPF WHERE FDID LIKE '%FUPCDESA%' AND LANG='S';
INSERT INTO  VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TRDT,TRTM,DATIME) 
VALUES('FUPCDESA','S','FOLLOW-UP CODE FOR SCREEN','Follow Up','Code for','Screen',0,0,CURRENT_TIMESTAMP);
ELSIF (cnt=0) THEN
INSERT INTO  VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TRDT,TRTM,DATIME) 
VALUES('FUPCDESA','S','FOLLOW-UP CODE FOR SCREEN','Follow Up','Code for','Screen',0,0,CURRENT_TIMESTAMP);
END IF;
END;
/
COMMIT;