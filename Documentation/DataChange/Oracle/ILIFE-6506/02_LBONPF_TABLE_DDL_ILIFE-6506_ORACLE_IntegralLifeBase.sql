
 CREATE SEQUENCE VM1DTA.SEQ_LBONPF  START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER VM1DTA.TR_LBONPF
 BEFORE INSERT ON VM1DTA.LBONPF FOR EACH ROW
 WHEN (NEW.UNIQUE_NUMBER IS NULL)
BEGIN
 SELECT VM1DTA.SEQ_LBONPF.NEXTVAL INTO :NEW.UNIQUE_NUMBER FROM DUAL;
END;
/