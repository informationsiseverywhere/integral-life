
set define off;
DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCTPF f
  where bschednam = 'LKBCHISSUE';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) 
values ('E','LKBCHISSUE','LIFE Batch Contract Issue                         ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSCDPF f
  where bauthcode = 'T642' and PRODCODE='L' AND bschednam = 'LKBCHISSUE'; 
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP)
 values ('LKBCHISSUE',1,'Y','N','T642','QBATCH    ','5','*JOBD     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,null);

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BSPDPF f
  where bschednam = 'LKBCHISSUE' and COMPANY='7';
  IF ( cnt = 0 ) THEN
Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) 
values ('LKBCHISSUE','7','BCHISS-CR ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);

  END IF;
END;
/


DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPSRPF f
where BPRIORPROC IN ('BCHISS-CR','B5221','B5222') and  BPRIORCOY='7';
  IF ( cnt = 0 ) THEN  
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('7','BCHISS-CR ','7','B5221     ','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('7','B5221     ','7','B5222     ','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
  END IF;
END;
/




DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPRDPF f
 where BPROCESNAM IN ('BCHISS-CR','B5221','B5222') and  COMPANY='7';
  IF ( cnt = 0 ) THEN

Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','B5221     ','00:00:00','00:00:00','B5221     ','                                                                                                                                                                                                                                                ',1,1,'P6671','        ','T642','N','10',50,50,5000,'          ','          ','          ','IS        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','B5222     ','00:00:00','00:00:00','B5222     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','T642','N','10',50,50,5000,'T600      ','T607      ','          ','IS        ','          ',100,'*DATABASE ','3','3',0,' ',' ','L','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('7','BCHISS-CR ','00:00:00','00:00:00','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','T642','N','10',50,50,5000,'ZCHXPF    ','          ','          ','IS        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);


  END IF;
END;
/

DECLARE 
  cnt number(2,1) :=0;
BEGIN
  SELECT count(1) INTO cnt 
  FROM VM1DTA.BPPDPF f
 where bschednam = 'LKBCHISSUE' and COMPANY='7';
  IF ( cnt = 0 ) THEN

Insert into VM1DTA.BPPDPF (BSCHEDNAM,BPARPSEQNO,COMPANY,BPARPPROG,USRPRF,JOBNM,DATIME) 
values ('LKBCHISSUE',1,'7','P6671','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);


  END IF;
END;
/
