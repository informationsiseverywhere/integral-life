CREATE FUNCTION VM1DTA.INTEGRAL_EXTRACT (@dp varchar(11), @v_date  varchar(8) )
RETURNS VARCHAR(4) 
AS 
BEGIN
	  DECLARE @result VARCHAR(4);
	
	  IF (@dp = 'YEAR')
	  SELECT @result = DATEPART(year, @v_date);  
	  ELSE
	  BEGIN
	  IF (@dp = 'MONTH')
	  SELECT @result = DATEPART(month, @v_date);
	  END;  
	  return @result;
END;
