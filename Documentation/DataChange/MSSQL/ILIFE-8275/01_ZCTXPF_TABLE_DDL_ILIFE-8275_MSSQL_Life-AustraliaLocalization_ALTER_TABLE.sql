-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ROLLFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCTXPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[ZCTXPF] ADD ROLLFLAG NCHAR(1) DEFAULT 'N';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Roll Flag added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZCTXPF', @level2type=N'COLUMN',@level2name=N'ROLLFLAG ';
	END
GO