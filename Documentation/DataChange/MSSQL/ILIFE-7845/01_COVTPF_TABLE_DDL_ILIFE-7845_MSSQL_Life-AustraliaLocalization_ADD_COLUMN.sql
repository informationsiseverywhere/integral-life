-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RISKPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVTPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[COVTPF] ADD RISKPREM NUMERIC(17,2) NOT NULL DEFAULT '0';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<risk premium added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVTPF', @level2type=N'COLUMN',@level2name=N'RISKPREM ';
	END
GO