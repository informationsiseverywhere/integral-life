-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'RSKPPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN
		CREATE TABLE [VM1DTA].[RSKPPF]
		(
			UNIQUE_NUMBER BIGINT IDENTITY(1,1) NOT NULL,
			CHDRCOY NCHAR(1) NULL,
			CHDRNUM NCHAR(8) NULL,
			LIFE NCHAR(8) NULL,
			COVERAGE NCHAR(8) NULL,
			RIDER NCHAR(8) NULL,
			CNTTYPE NCHAR(3) NULL,
			CRTABLE NCHAR(8) NULL,
			DATEFRM INT NULL,
			DATETO INT NULL,
			INSTPREM NUMERIC(17,2),
			RISKPREM NUMERIC(17,2),
			EFFDATE INT NULL,
			USRPRF NCHAR(10) NULL,
			JOBNM NCHAR(10) NULL,
			DATIME DATETIME2 NULL
		);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Premium table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'LIFE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coverage' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rider' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'RIDER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date From' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'DATEFRM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date To' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'DATETO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inst Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'INSTPREM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Risk Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'RISKPREM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jobnm' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'DATIME';
	END
GO