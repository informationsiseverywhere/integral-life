GO
IF EXISTS ( SELECT * FROM sys.views where name = 'COVTLNB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
                DROP VIEW [VM1DTA].COVTLNB
GO

/****** Object:  View [VM1DTA].[COVTLNB]    Script Date: 2/19/2019 12:25:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [VM1DTA].[COVTLNB] (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, SEQNBR, TERMID, TRDT, TRTM, USER_T, CRTABLE, RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, PCESTRM, SUMINS, SINGP, INSTPREM, ZBINSTPREM, ZLINSTPREM, LOADPER, RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, ZSTPDUTY01, MORTCLS, LIENCD, JLIFE, POLINC, NUMAPP, SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ, BILLCHNL, EFFDATE, PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE, BAPPMETH, ZDIVOPT, PAYCOY, PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR, FACTHOUS,LNKGNO,LNKGSUBREFNO, TPDTYPE, LNKGIND, SINGPREMTYPE, RISKPREM,RSUNIN,RUNDTE,GMIB,GMDB,GMWB, USRPRF, JOBNM, DATIME)
AS
  SELECT UNIQUE_NUMBER,
    CHDRCOY,
    CHDRNUM,
    LIFE,
    COVERAGE,
    RIDER,
    PLNSFX,
    SEQNBR,
    TERMID,
    TRDT,
    TRTM,
    USER_T,
    CRTABLE,
    RCESDTE,
    PCESDTE,
    RCESAGE,
    PCESAGE,
    RCESTRM,
    PCESTRM,
    SUMINS,
    SINGP,
    INSTPREM,
    ZBINSTPREM,
    ZLINSTPREM,
     LOADPER, 
    RATEADJ, 
    FLTMORT, 
    PREMADJ, AGEADJ,
	ZCLSTATE,
	ZSTPDUTY01,
    MORTCLS,
    LIENCD,
    JLIFE,
    POLINC,
    NUMAPP,
    SEX01,
    SEX02,
    ANBCCD01,
    ANBCCD02,
    BILLFREQ,
    BILLCHNL,
    EFFDATE,
    PAYRSEQNO,
    CNTCURR,
    BCESAGE,
    BCESTRM,
    BCESDTE,
    BAPPMETH,
    ZDIVOPT,
    PAYCOY,
    PAYCLT,
    PAYMTH,
    BANKKEY,
    BANKACCKEY,
    PAYCURR,
    FACTHOUS,
	LNKGNO,
	LNKGSUBREFNO,
	TPDTYPE,
	LNKGIND,
	SINGPREMTYPE,
	RISKPREM,
	RSUNIN,
	RUNDTE,
	GMIB,
	GMDB,
	GMWB,
    USRPRF,
    JOBNM,
    DATIME
  FROM vm1dta.COVTPF ;




GO


