
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'RPDETPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[RPDETPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[LOANNUM] [numeric](2) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRPFX] [nchar](2) NOT NULL,
	[REPAYAMT] [numeric](17, 2) NOT NULL,
	[REPAYSTATUS] [nchar](1) NOT NULL,
	[APPROVALDATE]  [nchar](10)NOT NULL,
	[REPAYMETHOD]  [nchar](3)NOT NULL,	
	[TRANNO]   [numeric](5) NULL,       
    [USERID] [nchar](10) NULL,    
	[JOBNM] [nchar](10) NULL,    	
    [DATIME] [datetime2](6) NOT NULL,
	[LOANTYPE] [nchar](1) NOT NULL,
	[EFFTDATE] [nchar](10) NOT NULL,
	[LOANCURR] [nchar](3) NOT NULL,
	[INTEREST] [numeric](3) NULL, 
	[PRINAMNT] [numeric](17, 2) NOT NULL,
	[CURRAMNT] [numeric](17, 2) NOT NULL,
	[ACCRDINT] [numeric](17, 2) NOT NULL,
	[PENDINT] [numeric](17, 2) NOT NULL,
	[STATUS] [nchar](1) NOT NULL,
CONSTRAINT [PK_RPDETPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
) ON [PRIMARY] 
GO


