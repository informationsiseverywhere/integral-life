SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create table SEQ_ATREQNO (
      SeqID int identity(1,1) primary key,
      SeqVal varchar(1)
);
GO

CREATE procedure [VM1DTA].[GetNewSeqValSeqAtreqNo]
as
begin
      declare @NewSeqValue int
      set NOCOUNT ON
      insert into SEQ_ATREQNO (SeqVal) values ('a')
      
      set @NewSeqValue = scope_identity()
      
      delete from SEQ_ATREQNO WITH (READPAST)
return @NewSeqValue
end
GO