	
	DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRGF  ';
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND H.[ERORCOY]='2' AND h.[EROREROR] = 'RRGF  ')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','2','E','','RRGF  ', 'Bill Freq Change not allowed','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO
	
	
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S' AND H.[ERORCOY]='2' AND h.[EROREROR] = 'RRGF  ')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','2','S','','RRGF  ', '该合同不允许更改付款频率','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO

	