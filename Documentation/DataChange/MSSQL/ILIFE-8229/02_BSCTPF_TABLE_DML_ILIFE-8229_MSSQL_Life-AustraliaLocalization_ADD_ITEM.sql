SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.BSCTPF  	
	WHERE BSCHEDNAM = 'L2CC96' AND  DESC_T='Credit Card Extract - Fact House 96' )	
	insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('E','L2CC96','Credit Card Extract - Fact House 96','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.BSCTPF  	
	WHERE BSCHEDNAM = 'L2CCAPLY96' AND DESC_T='Credit Card Apply - Fact House 96' )
	insert into VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('E','L2CCAPLY96','Credit Card Apply - Fact House 96','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF  
GO
