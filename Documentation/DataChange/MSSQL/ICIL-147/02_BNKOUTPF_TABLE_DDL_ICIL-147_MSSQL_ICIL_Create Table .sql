
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'BNKOUTPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[BNKOUTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[AGNTNUM] [nchar](8) NOT NULL,
	[BNKOUT] [nchar](20) NOT NULL,
	[BNKOUTNAME] [nchar](47) NOT NULL,
	[BNKSM] [nchar](10) NOT NULL,
	[BNKSMNAME] [nchar](47) NOT NULL,
	[BNKTEL] [nchar](20) NOT  NULL,
	[BNKTELNAME] [nchar](47) NOT NULL,
	[USRPRF]   [nchar](10) NULL,       
    [JOBNM] [nchar](10) NULL,     
    [DATIME] [datetime2](7) NOT NULL,
CONSTRAINT [PK_BNKOUTPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
) ON [PRIMARY] 
GO


