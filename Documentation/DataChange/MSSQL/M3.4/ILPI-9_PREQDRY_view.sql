/****** Object:  View [VM1DTA].[PREQDRY] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[PREQDRY](RDOCPFX, RDOCCOY, RDOCNUM, JRNSEQ, BRANCH, RLDGCOY, SACSCODE, SACSTYP, RLDGACCT, GENLCOY, GENLCUR, GLCODE, GLSIGN, POSTMONTH, POSTYEAR, EFFDATE, TRANDESC, TRANREF, ORIGAMT, ACCTAMT, ORIGCCY, CRATE, TAXCAT, FRCDATE, RCAMT, TRDT, TRTM, USER_T, TERMID, CNTTOT, EXTRA, AGNTPFX, AGNTCOY, AGNTNUM, JOBNM, USRPRF, DATIME, UNIQUE_NUMBER) AS
SELECT      RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            JRNSEQ,
            BRANCH,
            RLDGCOY,
            SACSCODE,
            SACSTYP,
            RLDGACCT,
            GENLCOY,
            GENLCUR,
            GLCODE,
            GLSIGN,
            POSTMONTH,
            POSTYEAR,
            EFFDATE,
            TRANDESC,
            TRANREF,
            ORIGAMT,
            ACCTAMT,
            ORIGCCY,
            CRATE,
            TAXCAT,
            FRCDATE,
            RCAMT,
            TRDT,
            TRTM,
            USER_T,
            TERMID,
            CNTTOT,
            EXTRA,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            JOBNM,
            USRPRF,
            DATIME,
            UNIQUE_NUMBER
       FROM VM1DTA.PREQPF

GO






