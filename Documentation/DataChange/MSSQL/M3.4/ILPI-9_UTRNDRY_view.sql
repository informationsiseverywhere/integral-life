/****** Object:  View [VM1DTA].[UTRNDRY]******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[UTRNDRY](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TRANNO, TERMID, TRDT, TRTM, USER_T, JCTLJNUMPR, VRTFND, UNITSA, STRPDATE, NDFIND, NOFUNT, UNITYP, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, PRICEDT, MONIESDT, PRICEUSED, CRTABLE, CNTCURR, NOFDUNT, FDBKIND, COVDBTIND, UBREPR, INCINUM, INCIPERD01, INCIPERD02, INCIPRM01, INCIPRM02, USTMNO, CNTAMNT, FNDCURR, FUNDAMNT, FUNDRATE, SACSCODE, SACSTYP, GENLCDE, CONTYP, TRIGER, TRIGKY, PRCSEQ, SVP, DISCFA, PERSUR, CRCDTE, CIUIND, SWCHIND, BSCHEDNAM, BSCHEDNUM, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,

            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            TRANNO,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            JCTLJNUMPR,
            VRTFND,
            UNITSA,
            STRPDATE,
            NDFIND,
            NOFUNT,
            UNITYP,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            PRICEDT,
            MONIESDT,
            PRICEUSED,
            CRTABLE,
            CNTCURR,
            NOFDUNT,
            FDBKIND,
            COVDBTIND,
            UBREPR,
            INCINUM,
            INCIPERD01,
            INCIPERD02,
            INCIPRM01,
            INCIPRM02,
            USTMNO,
            CNTAMNT,
            FNDCURR,
            FUNDAMNT,
            FUNDRATE,
            SACSCODE,
            SACSTYP,
            GENLCDE,
            CONTYP,
            TRIGER,
            TRIGKY,
            PRCSEQ,
            SVP,
            DISCFA,
            PERSUR,
            CRCDTE,
            CIUIND,
            SWCHIND,
            BSCHEDNAM,
            BSCHEDNUM,
            JOBNM,
            USRPRF,
            DATIME




       FROM VM1DTA.UTRNPF
       WHERE FDBKIND =' '

GO


