ALTER VIEW "VM1DTA"."UTRSSWH" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "PLNSFX", "LIFE", "COVERAGE", "RIDER", "VRTFND", "UNITYP", "CURUNTBAL", "CURDUNTBAL","FUNDPOOL") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PLNSFX,
            LIFE,
            COVERAGE,
            RIDER,
            VRTFND,
            UNITYP,
            CURUNTBAL,
            CURDUNTBAL,
            FUNDPOOL
       FROM UTRSPF;