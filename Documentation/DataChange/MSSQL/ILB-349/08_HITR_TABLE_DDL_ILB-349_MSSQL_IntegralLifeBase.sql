ALTER VIEW VM1DTA.HITR (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, PRCSEQ, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, ZINTBFND, FUNDAMNT, FNDCURR, FUNDRATE, ZRECTYP, EFFDATE, TRANNO, CNTCURR, CNTAMNT, CNTTYP, CRTABLE, COVDBTIND, ZINTAPPIND, FDBKIND, TRIGER, TRIGKY, SWCHIND, PERSUR, SVP, ZLSTINTDTE, ZINTRATE, ZINTEFFDT, ZINTALLOC, INCINUM, INCIPERD01, INCIPERD02, INCIPRM01, INCIPRM02, USTMNO, SACSCODE, SACSTYP, GENLCDE,FUNDPOOL, USRPRF, JOBNM, DATIME) AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            PRCSEQ,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            ZINTBFND,
            FUNDAMNT,
            FNDCURR,
            FUNDRATE,
            ZRECTYP,
            EFFDATE,
            TRANNO,
            CNTCURR,
            CNTAMNT,
            CNTTYP,
            CRTABLE,
            COVDBTIND,
            ZINTAPPIND,
            FDBKIND,
            TRIGER,
            TRIGKY,
            SWCHIND,
            PERSUR,
            SVP,
            ZLSTINTDTE,
            ZINTRATE,
            ZINTEFFDT,
            ZINTALLOC,
            INCINUM,
            INCIPERD01,
            INCIPERD02,
            INCIPRM01,
            INCIPRM02,
            USTMNO,
            SACSCODE,
            SACSTYP,
            GENLCDE,
            FUNDPOOL,
            USRPRF,
            JOBNM,
            DATIME
       FROM HITRPF