 IF NOT EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'HITRPF'
                 AND COLUMN_NAME = 'FUNDPOOL')
BEGIN
ALTER TABLE  VM1DTA.HITRPF  ADD FUNDPOOL NCHAR(1) ,HCOVERAGE NCHAR(1), HRIDER NCHAR(1),HCRTABLE NCHAR(1),DEALCDTE NUMERIC(8),TRNAMT NUMERIC(17,2),PCUNIT NUMERIC(5,2),ALOCRATE NUMERIC(5,2),ENHCPC NUMERIC(5,2),UNIQK NUMERIC(18),ORGTRNO NUMERIC(5),ORGTRCDE NCHAR(4);

END