
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('RLDGACCT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='PYOUPF'))
BEGIN
	ALTER TABLE [VM1DTA].[PYOUPF] ADD RLDGACCT CHAR(16);
EXEC sys.sp_addextendedproperty 
@name=N'MS_Description', 
@value=N'<RLDGACCT>' ,
 @level0type=N'SCHEMA',@level0name=N'VM1DTA',
 @level1type=N'TABLE',@level1name=N'PYOUPF',
 @level2type=N'COLUMN',@level2name=N'RLDGACCT';
END
GO
