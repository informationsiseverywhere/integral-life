IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'LINKAGEPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[LINKAGEPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[LINKREF] [nchar](50) NULL,
	[LINKTYPE] [nchar](50) NULL,
	[LINKINFO] [nvarchar](4000) NULL,
	[USERNAME] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[UPDTUSER] [nchar](10) NULL,
	[UPDTIME] [datetime2](6) NULL,
	CONSTRAINT PK_LINKAGEPF PRIMARY KEY(UNIQUE_NUMBER)); 
GO
