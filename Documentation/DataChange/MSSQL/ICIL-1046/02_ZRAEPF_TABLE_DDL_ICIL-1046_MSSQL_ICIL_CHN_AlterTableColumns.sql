

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDRGPYMOP' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ZRAEPF ADD WDRGPYMOP CHAR(8);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<WITHDRWAL PAYMENT METHOD>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZRAEPF', @level2type=N'COLUMN',@level2name=N'WDRGPYMOP ';
	END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDBANKKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ZRAEPF ADD WDBANKKEY  CHAR(10);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<WITHDRWAL BANK KEY>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZRAEPF', @level2type=N'COLUMN',@level2name=N'WDBANKKEY ';
	END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDBANKACCKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))         
    BEGIN
		 ALTER TABLE VM1DTA.ZRAEPF ADD WDBANKACCKEY  CHAR(20);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<WITHDRWAL BANK KEY DESCRIPTION>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZRAEPF', @level2type=N'COLUMN',@level2name=N'WDBANKACCKEY ';
	END
GO
  