
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'DCDBPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN				
CREATE TABLE [VM1DTA].[DCDBPF]
  (
  [UNIQUE_NUMBER]    [bigint] NOT NULL,
	[CHDRCOY]    [nchar](1),
    [CHDRNUM]    [nchar](8),
	[CHDRPFX] 	 [nchar](2),
	[CNTTYPE]    [nchar](3),
    [CNTCURR]    [nchar](3),
    [BANKKEY]    [nchar](10),
    [BANKACCKEY] [nchar](20),    
    [COWNNUM]    [nchar](8),
    [COWNCOY]    [nchar](1),
	[PAYRPFX]    [nchar](1),
	[PAYRCOY]    [nchar](1),
	[PAYRNUM]    [nchar](8),
	[MANDREF]	 [nchar](5), 
	[FACTHOUS]	 [nchar](5), 
	[AMOUNT]	 [numeric](17, 2),
	[INSTFROM]	 [int],
    [INSTTO]     [int],
    [DBDCDATE]   [int],
    [TRANNO]     [numeric](5, 0),
	[JOBNO]     [numeric](8, 0),
    [USRPRF]     [nchar](10),
    [JOBNM]      [nchar](10),
	[DATIME] [datetime2](6) );
   
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DCDBPF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'CHDRPFX ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNTTYPE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNTCURR' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'CNTCURR ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANKKEY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'BANKKEY ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANKACCKEY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'BANKACCKEY ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COWNNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'COWNNUM ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COWNCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'COWNCOY ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYRPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'PAYRPFX ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYRCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'PAYRCOY ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYRNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'PAYRNUM ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MANDREF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'MANDREF ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FACTHOUS' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'FACTHOUS ';


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMOUNT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'AMOUNT ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INSTFROM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'INSTFROM ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INSTTO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'INSTTO ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DBDCDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'DBDCDATE ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRANNO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'TRANNO ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOBNO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'JOBNO ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USRPRF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'USRPRF ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOBNM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'JOBNM ';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DATIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'DCDBPF', @level2type=N'COLUMN',@level2name=N'DATIME ';
END
GO


  
