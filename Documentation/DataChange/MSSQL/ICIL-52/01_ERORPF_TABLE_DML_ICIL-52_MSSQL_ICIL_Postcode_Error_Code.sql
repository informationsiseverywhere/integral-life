GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR like 'RRAR%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAR  ','Postcode to be numeric',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO


GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR like 'RRAR%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAR  ','Postcode to be numeric',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO


GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR like 'RRAS%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAS  ','Postcode to be 6-digits',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO


GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR like 'RRAS%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAS  ','Postcode to be 6-digits',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO

GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR like 'RRAT%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','E','          ','RRAT  ','Postcode is mandatory',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO


GO
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='S' and EROREROR like 'RRAT%';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME)
values('ER',' ','S','          ','RRAT  ','Postcode is mandatory',0,0,'  ',null,'UNDERWR1','CHINALOC',current_timestamp);
GO

