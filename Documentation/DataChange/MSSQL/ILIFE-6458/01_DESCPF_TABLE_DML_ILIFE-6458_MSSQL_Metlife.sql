IF EXISTS (SELECT * FROM VM1DTA.DESCPF  WHERE desctabl='T3629' AND language= 'J' and descitem = 'MYR     ' and desccoy = '2')
UPDATE
  vm1dta.descpf
SET shortdesc='MYR       ', longdesc = 'Malaysia Ringgit'
WHERE desctabl='T3629' AND language= 'J' and descitem = 'MYR     ' and desccoy = '2'
GO

IF EXISTS (SELECT * FROM VM1DTA.DESCPF  WHERE desctabl='T3629' AND language= 'J' and descitem = 'YEN     ' and desccoy = '2')
delete from vm1dta.descpf WHERE desctabl='T3629' AND language= 'J' and descitem = 'YEN     ' and desccoy = '2'
GO





IF EXISTS (SELECT * FROM VM1DTA.DESCPF  WHERE desctabl='T3573' AND language= 'J' and descitem = 'GOV     ' and desccoy = '9')
UPDATE
  vm1dta.descpf
SET shortdesc='GOV       ', longdesc = '政府機関省庁'
WHERE desctabl='T3573' AND language= 'J' and descitem = 'GOV     ' and desccoy = '9'
GO