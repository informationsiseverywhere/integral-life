GO
IF EXISTS ( SELECT * FROM sys.views where name = 'REGPENQ' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
                DROP VIEW VM1DTA.REGPENQ
GO

/****** Object:  View VM1DTA.REGPENQ    Script Date: 2/19/2019 12:24:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW VM1DTA.REGPENQ(UNIQUE_NUMBER,CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,VALIDFLAG,TRANNO,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCOY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,RGPYSTAT,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,APRVDATE,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,CERTDATE,CLAMPARTY,TERMID,TRDT,TRTM,USER_T,ZCLMRECD,INCURDT,USRPRF,JOBNM,ADJAMT,REASONCD,NETAMT,REASON,OVRPERMENTLY,OVRSUMIN,DATIME)
AS
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PLNSFX,
            LIFE,
            COVERAGE,
            RIDER,
            RGPYNUM,
            VALIDFLAG,
            TRANNO,
            SACSCODE,
            SACSTYPE,
            GLACT,
            DEBCRED,
            DESTKEY,
            PAYCOY,
            PAYCLT,
            RGPYMOP,
            REGPAYFREQ,
            CURRCD,
            PYMT,
            PRCNT,
            TOTAMNT,
            RGPYSTAT,
            PAYREASON,
            CLAIMEVD,
            BANKKEY,
            BANKACCKEY,
            CRTDATE,
            APRVDATE,
            FPAYDATE,
            NPAYDATE,
            REVDTE,
            LPAYDATE,
            EPAYDATE,
            ANVDATE,
            CANCELDATE,
            RGPYTYPE,
            CRTABLE,
            CERTDATE,
            CLAMPARTY,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            ZCLMRECD,
            INCURDT,
            USRPRF,
            JOBNM,
            ADJAMT,
            REASONCD,
            NETAMT,
            REASON,
			OVRPERMENTLY,
			OVRSUMIN,
            DATIME
      FROM VM1DTA.REGPPF WHERE VALIDFLAG = '1';  
GO

SET QUOTED_IDENTIFIER OFF
GO


