IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('OVRPERMENTLY') and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
BEGIN
	ALTER TABLE REGPPF ADD OVRPERMENTLY NCHAR(1);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.REGPPF.OVRPERMENTLY IS OVRPERMENTLY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'OVRPERMENTLY';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('OVRSUMIN') and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
BEGIN
	ALTER TABLE REGPPF ADD OVRSUMIN NUMERIC(17, 2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.REGPPF.OVRSUMIN IS OVRSUMIN' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'OVRSUMIN';
END
GO
