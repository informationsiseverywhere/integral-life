
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'RGPDETPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[RGPDETPF]( 

	   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
	   [CHDRCOY] [nchar](1) ,
	   [CHDRNUM] [nchar](8) ,
	   [PLNSFX] [int] ,
	   [LIFE] [nchar](2) ,
	   [COVERAGE] [nchar](2) ,
	   [RIDER] [nchar](2) ,
	    [CURRCD] [nchar](3) ,
	    [APCAPLAMT] [int] ,
		[APINTLAMT] [int] ,
		[APLSTCAPDATE] [int] ,
		[APNXTCAPDATE] [int] ,
		[APLSTINTBDTE] [int] ,
		[APNXTINTBDTE] [int] ,
		[VALIDFLAG] [nchar](1) ,
        [TRANNO] [nchar](5) ,
	   [USRPRF] [nchar](10) ,
	   [JOBNM] [nchar](10) ,
	   [DATIME] [datetime2](6) ,
	   CONSTRAINT [PK_RGPDETPF] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)ON [PRIMARY]

GO