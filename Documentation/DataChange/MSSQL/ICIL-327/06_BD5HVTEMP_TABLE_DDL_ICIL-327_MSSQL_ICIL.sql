

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BD5HVTEMP' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
				
CREATE TABLE [VM1DTA].[BD5HVTEMP]
  (
	[BATCHID]       [bigint],
	[CNT] [int],
    [CHDRCOY]    [nchar](1),
    [CHDRNUM]    [nchar](8),
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[PLNSFX]        [int],
    [LIFE]          [nchar](2),
    [COVERAGE]      [nchar](2),
    [RIDER]         [nchar](2),    
    [CURRCD]   [nchar](3),
    [VALIDFLAG]  [nchar](1),
	[APCAPLAMT]    [numeric](17, 2),
	[APLSTCAPDATE]	[int], 
	[APNXTCAPDATE]	[int], 
	[APLSTINTBDTE]	[int], 
	[APNXTINTBDTE]	[int],
    [TRANNO]     [int],
    [CNTTYPE]    [nchar](3),
    [CHDRPFX]    [nchar](2),
    [SERVUNIT]   [nchar](2),
    [OCCDATE]    [int],
    [CCDATE]     [int],
    [COWNPFX]    [nchar](2),
    [COWNCOY]    [nchar](1),
    [COWNNUM]    [nchar](8),
    [COLLCHNL]   [nchar](2),
    [CNTBRANCH]  [nchar](2),
    [CNTCURR]    [nchar](3),
    [POLSUM]     [int],
    [POLINC]     [int],
    [AGNTPFX]    [nchar](2),
    [AGNTCOY]    [nchar](1),
    [AGNTNUM]    [nchar](8),
   [DATIME] [datetime2](6) ) ON [PRIMARY]

GO