-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ZSTPDUTY01' and object_id in (SELECT object_id FROM sys.tables WHERE name ='INCRPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[INCRPF] ADD ZSTPDUTY01 NUMERIC(17, 2) DEFAULT '0';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<stamp duty>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INCRPF', @level2type=N'COLUMN',@level2name=N'ZSTPDUTY01 ';
	END
GO