IF OBJECT_ID (N'VM1DTA.CLEMPPF', N'U') IS  NULL
create table vm1dta.CLEMPPF(
[CLNTPFX] [varchar](2) NULL,
[CLNTCOY] [varchar](1) NULL,
[CLNTNUM] [varchar](8) NULL,
[EMPLOYER] [varchar](20) NULL,
[EMPID] [varchar](10) NULL,
[TITLE] [varchar](15) NULL,
[USRPRF] [varchar](10) NULL,
[JOBNM] [varchar](10) NULL,
[DATIME] [datetime2](6) NULL,
); 
