IF EXISTS ( SELECT * FROM sys.tables where name = 'BACRLTEMP1' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE [BACRLTEMP1]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BACRLTEMP1' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [BACRLTEMP1](
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[BACRTAPREC] [nchar](500) NULL,
	[MEMBER_NAME] [nchar](10) NULL);
GO
