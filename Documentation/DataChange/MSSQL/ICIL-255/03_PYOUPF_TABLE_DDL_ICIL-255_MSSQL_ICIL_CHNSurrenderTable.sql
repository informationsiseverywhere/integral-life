
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'PYOUPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[PYOUPF]( 

	   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,

	   [CHDRPFX] [char](2) NULL,
           [CHDRCOY] [char](1) NULL,
           [CHDRNUM] [char](8) NULL,
	   [TRANNO] [numeric](5,0) NULL,
	   [BATCTRCDE] [char](4) NULL,
	   [PAYRNUM] [char](8) NULL,
           [REQNTYPE] [char](1) NULL,
           [BANKACCKEY] [char](20) NULL,
           [BANKKEY CHAR] [char](10) NULL,
           [BANKDESC CHAR] [char](60) NULL,
           [PAYMENTFLAG CHAR] [char](1) NULL,
           [REQNNO] [char](9) NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar](10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   CONSTRAINT [PK_PYOUPF] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)ON [PRIMARY]

GO