

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INTERESTDAYS' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
	BEGIN
	ALTER TABLE [VM1DTA].[REGPPF] ADD INTERESTDAYS NUMERIC(3,0) null ;
		  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<NO. OF DAYS FOR INTEREST>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'INTERESTDAYS';
	END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INTERESTRATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
	BEGIN
	ALTER TABLE [VM1DTA].[REGPPF] ADD INTERESTRATE NUMERIC(8,5) null ;
		  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<INTEREST RATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'INTERESTRATE';
	END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INTERESTAMT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
	BEGIN
	ALTER TABLE [VM1DTA].[REGPPF] ADD INTERESTAMT NUMERIC(17,2) null ;
		  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<INTEREST AMOUNT>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'INTERESTAMT';
	END
GO
