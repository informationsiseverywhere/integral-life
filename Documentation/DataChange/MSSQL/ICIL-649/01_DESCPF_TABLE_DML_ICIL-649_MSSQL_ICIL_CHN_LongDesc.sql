

update vm1dta.descpf set longdesc ='2CH Agency Sales License' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'AGMTN014' and language='E';
update vm1dta.descpf set longdesc =N'2CH 销售资格类型' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'AGMTN014' and language='S';

update vm1dta.descpf set longdesc ='2CH Risk Amount Display' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'UNWRT001' and language='E';
update vm1dta.descpf set longdesc =N'2CH 保额显示' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'UNWRT001' and language='S';

update vm1dta.descpf set longdesc ='9CH Agent Mainteance' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'AGMTN015' and language='E';
update vm1dta.descpf set longdesc =N'9CH 银行保险代理人输入' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'AGMTN015' and language='S';

update vm1dta.descpf set longdesc ='2CH China freelook cancel' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTCNL002' and language='E';
update vm1dta.descpf set longdesc ='2CH China freelook cancel' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTCNL002' and language='S';

update vm1dta.descpf set longdesc ='9CH Occupation Class FeaConfg' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'NBPRP056' and language='E';
update vm1dta.descpf set longdesc =N'9CH 职业类别Fea设定' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'NBPRP056' and language='S';

update vm1dta.descpf set longdesc ='2CH Policy Loans Debt' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSLND002' and language='E';
update vm1dta.descpf set longdesc =N'2CH 保单贷款' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSLND002' and language='S';

update vm1dta.descpf set longdesc ='2CH China full surrender' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUSUR002' and language='E';
update vm1dta.descpf set longdesc ='2CH China full surrender' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUSUR002' and language='S';

update vm1dta.descpf set longdesc ='2CH Disallow TXN in colng pred' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSCOM004' and language='E';
update vm1dta.descpf set longdesc =N'2CH 犹豫期内不允许' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSCOM004' and language='S';

update vm1dta.descpf set longdesc ='2CH Change of Annuity Method' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUOTR006' and language='E';
update vm1dta.descpf set longdesc ='2CH Change of Annuity Method' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUOTR006' and language='S';

update vm1dta.descpf set longdesc ='2CH Annuity Products' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSCOM006' and language='E';
update vm1dta.descpf set longdesc =N'2CH 年金产品' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSCOM006' and language='S';

update vm1dta.descpf set longdesc ='2CH Paid Up Processing' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSPUP001' and language='E';
update vm1dta.descpf set longdesc =N'2CH 缴清处理' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSPUP001' and language='S';

update vm1dta.descpf set longdesc ='2CH Cash Dividend' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUOTR007' and language='E';
update vm1dta.descpf set longdesc ='2CH Cash Dividend' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'SUOTR007' and language='S';

update vm1dta.descpf set longdesc ='2CH Follow-ups Maintenance' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSMIN003' and language='E';
update vm1dta.descpf set longdesc =N'2CH 跟进维护' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSMIN003' and language='S';

update vm1dta.descpf set longdesc ='2CH Contract Proposal Enquiry' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'NBPRP084' and language='E';
update vm1dta.descpf set longdesc ='2CH Contract Proposal Enquiry' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'NBPRP084' and language='S';

update vm1dta.descpf set longdesc ='2CH Waiver Provision' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'BTPRO012' and language='E';
update vm1dta.descpf set longdesc ='2CH Waiver Provision' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'BTPRO012' and language='S';

update vm1dta.descpf set longdesc ='2CH Batch Proces of L2REGPAY' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'BTPRO013' and language='E';
update vm1dta.descpf set longdesc =N'2CH L2REGPAY Batch程序' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'BTPRO013' and language='S';

update vm1dta.descpf set longdesc ='2CH Contract Inquiries APL/PL' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTENQ003' and language='E';
update vm1dta.descpf set longdesc =N'2CH APL/PL保单查询' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTENQ003' and language='S';

update vm1dta.descpf set longdesc ='2CH Deferred Policy Loan' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSLND004' and language='E';
update vm1dta.descpf set longdesc =N'2CH 贷款起息日设定' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CSLND004' and language='S';

update vm1dta.descpf set longdesc ='2CH Contract Enquiry' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTENQ002' and language='E';
update vm1dta.descpf set longdesc =N'2CH 保单查询' where descpfx='IT' and desccoy='2' and desctabl like 'TR2A5' and descitem like 'CTENQ002' and language='S';

update vm1dta.descpf set longdesc ='9CH China localization fields' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'CHNNFLS1' and language='E';
update vm1dta.descpf set longdesc =N'9CH 中国新加字段' where descpfx='IT' and desccoy='9' and desctabl like 'TR2A5' and descitem like 'CHNNFLS1' and language='S';