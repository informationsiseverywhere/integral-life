SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[ADRSPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[ADDRCD] [nchar](11) NULL,
	[ADDRCDNW] [nchar](11) NULL,
	[KPOSTCD] [nchar](7) NULL,
	[BARCD] [nchar](13) NULL,
	[BARCDLEN] [int] NULL,
	[POSTCDINFA] [nchar](1) NULL,
	[POSTCDINFB] [nchar](1) NULL,
	[PACFLAG] [nchar](1) NULL,
	[PACCD] [nchar](11) NULL,
	[PRFCTIND] [int] NULL,
	[PRFCT] [nchar](8) NULL,
	[CITY] [nchar](24) NULL,
        [OAZA] [nchar](36) NULL,
	[CHOME] [nchar](24) NULL,


	[PRFCTLEN] [int]  NULL,
	[CITYLEN] [int]  NULL,
	[OAZALEN] [int]  NULL,
	[CHOMELEN] [int]  NULL,
	[TTLADLEN] [int]  NULL,

	[KJPRFCT] [nchar](8) NULL,
	[KJCITY] [nchar](24) NULL,
        [KJOAZA] [nchar](36) NULL,
	[KJCHOME] [nchar](24) NULL,

	[KJPRFCTLEN] [int]  NULL,
	[KJCITYLEN] [int]  NULL,
	[KJOAZALEN] [int]  NULL,
	[KJCHOMELEN] [int]  NULL,
	[KJTTLADLEN] [int]  NULL,

	[JISLVL01] [int]  NULL,
	[JISLVL02] [int]  NULL,
	[JISLVL03] [int]  NULL,
	[JISLVL04] [int]  NULL,
	[JISLVL05] [int]  NULL,
	[JISLVL06] [int]  NULL,
	[JISLVL07] [int]  NULL,
	

	[OAZAFLAG] [int]  NULL,
	[AZAFLAG] [int]  NULL,
	[ADALFLAG] [int]  NULL,
	[ALIASFLAG] [int]  NULL,
	[EFCTYYYYMM] [int]  NULL,
	[ABRGYYYYMM] [int]  NULL,
	[ADCDYYYYMM] [int]  NULL,	
	[ADDRYYYYMM] [int]  NULL,
	[POSTYYYYMM] [int]  NULL,
	[BARYYYYMM] [int]  NULL,
	[PACYYYYMM] [int]  NULL,
	[ALIYYYYMM] [int]  NULL,
	[CHOYYYMM] [int]  NULL,

	[POSTCDOLD] [nchar](5) NULL,
	[ADDRFIL] [nchar](8) NULL,
	[MAINTCD] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
        [DATIME] [datetime2](6) NULL,

 CONSTRAINT [PK_ADRSPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO