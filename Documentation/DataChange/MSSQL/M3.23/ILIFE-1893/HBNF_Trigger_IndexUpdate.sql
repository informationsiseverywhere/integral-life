CREATE TRIGGER VM1DTA.TR_HBNFPF ON VM1DTA.HBNFPF
AFTER INSERT
AS

if exists ( select * from HBNFPF New
    inner join inserted old on old.CHDRCOY= New.CHDRCOY AND old.CHDRNUM= New.CHDRNUM AND old.LIFE=New.LIFE AND old.COVERAGE=New.COVERAGE AND old.RIDER=New.RIDER)
begin
    rollback
    RAISERROR ('Duplicate Data', 16, 1);
end

