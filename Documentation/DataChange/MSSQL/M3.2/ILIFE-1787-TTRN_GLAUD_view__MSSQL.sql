--- View  ------
CREATE  VIEW "VM1DTA"."TTRN_GLAUD" ("UNIQUE_NUMBER", "BATCCOY", "BATCBRN", "BATCACTYR", "BATCACTMN", "BATCTRCDE", "BATCBATCH", "EFFDATE", "GENLCOY", "GENLCUR", "GLCODE", "GLSIGN", "ORIGCURR", "POSTMONTH", "POSTYEAR", "ORIGAMT", "ACCTAMT", "SACSCODE", "SACSTYP", "RLDGPFX", "RDOCPFX", "RLDGCOY", "RDOCCOY", "RLDGACCT", "RDOCNUM", "TRANNO", "USRPRF", "JRNSEQ", "TRANREF", "TRANDESC", "RECORDFORMAT")
AS
  SELECT UNIQUE_NUMBER,
    BATCCOY,
    BATCBRN,
    BATCACTYR,
    BATCACTMN,
    BATCTRCDE,
    BATCBATCH,
    EFFDATE,
    GENLCOY,
    GENLCUR,
    GLCODE,
    GLSIGN,
    ORIGCURR,
    POSTMONTH,
    POSTYEAR,
    ORIGAMT,
    ACCTAMT,
    SACSCODE,
    SACSTYP,
    RLDGPFX,
    RDOCPFX,
    RLDGCOY,
    RDOCCOY,
    RLDGACCT,
    RDOCNUM,
    TRANNO,
    USRPRF,
    JRNSEQ,
    TRANREF,
    TRANDESC,
    RECORDFORMAT
  FROM
    (SELECT UNIQUE_NUMBER,
      BATCCOY,
      BATCBRN,
      BATCACTYR,
      BATCACTMN,
      BATCTRCDE,
      BATCBATCH,
      EFFDATE,
      GENLCOY,
      GENLCUR,
      GLCODE,
      GLSIGN,
      ORIGCCY ORIGCURR,
      POSTMONTH,
      POSTYEAR,
      CASE
        WHEN GLSIGN = '-'
        THEN ORIGAMT * (-1)
        ELSE ORIGAMT
      END ORIGAMT,
      CASE
        WHEN GLSIGN = '-'
        THEN ACCTAMT * (-1)
        ELSE ACCTAMT
      END ACCTAMT,
      sacscode,
      Sacstyp,
      rldgpfx,
      rdocpfx,
      rldgcoy,
      rdoccoy,
      rldgacct,
      rdocnum,
      tranno,
      usrprf,
      JRNSEQ,
      ' ' TRANREF,
      TRANDESC,
      'RTRNREC' RECORDFORMAT
    FROM VM1DTA.RTRNPF
    UNION ALL
    SELECT UNIQUE_NUMBER,
      BATCCOY,
      BATCBRN,
      BATCACTYR,
      BATCACTMN,
      BATCTRCDE,
      BATCBATCH,
      EFFDATE,
      GENLCOY,
      GENLCUR,
      GLCODE,
      GLSIGN,
      ORIGCURR,
      POSTMONTH,
      POSTYEAR,
      CASE
        WHEN GLSIGN = '-'
        THEN ORIGAMT * (-1)
        ELSE ORIGAMT
      END ORIGAMT,
      CASE
        WHEN GLSIGN = '-'
        THEN ACCTAMT * (-1)
        ELSE ACCTAMT
      END ACCTAMT,
      sacscode,
      Sacstyp,
      rldgpfx,
      rdocpfx,
      rldgcoy,
      rdoccoy,
      rldgacct,
      rdocnum,
      tranno,
      usrprf,
      JRNSEQ,
      TRANREF,
      TRANDESC,
      'ACMVREC' RECORDFORMAT
    FROM VM1DTA.ACMVPF
    UNION ALL
    SELECT UNIQUE_NUMBER,
      BATCCOY,
      BATCBRN,
      BATCACTYR,
      BATCACTMN,
      BATCTRCDE,
      BATCBATCH,
      EFFDATE,
      GENLCOY,
      ACCTCURR GENLCUR,
      GLCODE,
      GLSIGN,
      ORIGCURR,
      POSTMONTH,
      POSTYEAR,
      --ORIGAMT,
      CASE
        WHEN GLSIGN = '-'
        THEN TRANAMT * (-1)
        ELSE TRANAMT
      END ORIGAMT,
      ACCTAMT,
      sacscode,
      Sacstyp,
      rldgpfx,
      rdocpfx,
      rldgcoy,
      rdoccoy,
      rldgacct,
      rdocnum,
      tranno,
      usrprf,
      JRNSEQ,
      TRANREF,
      TRANDESC,
      'ZTRNREC' RECORDFORMAT
    FROM
      (SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE01 GLCODE,
        GLSIGN01 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT01 TRANAMT,
        ---
        /*CASE
        WHEN GLSIGN01 = '-'
        THEN TRANAMT01 * (-1)
        ELSE TRANAMT01
        END ORIGAMT, */
        CASE
          WHEN GLSIGN01 = '-'
          THEN TRANAMT01 * (-1) * CRATE
          ELSE TRANAMT01 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp01 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        01 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE02 GLCODE,
        GLSIGN02 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT02 TRANAMT,
        ---
        /*  CASE
        WHEN GLSIGN02 = '-'
        THEN TRANAMT02 * (-1)
        ELSE TRANAMT02
        END ORIGAMT, */
        CASE
          WHEN GLSIGN02 = '-'
          THEN TRANAMT02 * (-1) * CRATE
          ELSE TRANAMT02 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp02 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        02 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE03 GLCODE,
        GLSIGN03 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT03 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN03 = '-'
        THEN TRANAMT03 * (-1)
        ELSE TRANAMT03
        END ORIGAMT, */
        CASE
          WHEN GLSIGN03 = '-'
          THEN TRANAMT03 * (-1) * CRATE
          ELSE TRANAMT03 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp03 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        03 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE04 GLCODE,
        GLSIGN04 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT04 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN04 = '-'
        THEN TRANAMT04 * (-1)
        ELSE TRANAMT04
        END ORIGAMT,*/
        CASE
          WHEN GLSIGN04 = '-'
          THEN TRANAMT04 * (-1) * CRATE
          ELSE TRANAMT04 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp04 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        04 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE05 GLCODE,
        GLSIGN05 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT05 TRANAMT,
        ---
        /*CASE
        WHEN GLSIGN05 = '-'
        THEN TRANAMT05 * (-1)
        ELSE TRANAMT05
        END ORIGAMT,*/
        CASE
          WHEN GLSIGN05 = '-'
          THEN TRANAMT05 * (-1) * CRATE
          ELSE TRANAMT05 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp05 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        05 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE06 GLCODE,
        GLSIGN06 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT06 TRANAMT,
        ---
        /*CASE
        WHEN GLSIGN06 = '-'
        THEN TRANAMT06 * (-1)
        ELSE TRANAMT06
        END ORIGAMT,*/
        CASE
          WHEN GLSIGN06 = '-'
          THEN TRANAMT06 * (-1) * CRATE
          ELSE TRANAMT06 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp06 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        06 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE07 GLCODE,
        GLSIGN07 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT07 TRANAMT,
        ---
        /*CASE
        WHEN GLSIGN07 = '-'
        THEN TRANAMT07 * (-1)
        ELSE TRANAMT07
        END ORIGAMT,*/
        CASE
          WHEN GLSIGN07 = '-'
          THEN TRANAMT07 * (-1) * CRATE
          ELSE TRANAMT07 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp07 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        07 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE08 GLCODE,
        GLSIGN08 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT08 TRANAMT,
        ---
        /*  CASE
        WHEN GLSIGN08 = '-'
        THEN TRANAMT08 * (-1)
        ELSE TRANAMT08
        END ORIGAMT, */
        CASE
          WHEN GLSIGN08 = '-'
          THEN TRANAMT08 * (-1) * CRATE
          ELSE TRANAMT08 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp08 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        08 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE09 GLCODE,
        GLSIGN09 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT09 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN09 = '-'
        THEN TRANAMT09 * (-1)
        ELSE TRANAMT09
        END ORIGAMT, */
        CASE
          WHEN GLSIGN09 = '-'
          THEN TRANAMT09 * (-1) * CRATE
          ELSE TRANAMT09 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp09 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        09 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE10 GLCODE,
        GLSIGN10 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT10 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN10 = '-'
        THEN TRANAMT10 * (-1)
        ELSE TRANAMT10
        END ORIGAMT, */
        CASE
          WHEN GLSIGN10 = '-'
          THEN TRANAMT10 * (-1) * CRATE
          ELSE TRANAMT10 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp10 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        10 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE11 GLCODE,
        GLSIGN11 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT11 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN11 = '-'
        THEN TRANAMT11 * (-1)
        ELSE TRANAMT11
        END ORIGAMT, */
        CASE
          WHEN GLSIGN11 = '-'
          THEN TRANAMT11 * (-1) * CRATE
          ELSE TRANAMT11 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp11 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        11 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE12 GLCODE,
        GLSIGN12 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT12 TRANAMT,
        ---
        /*  CASE
        WHEN GLSIGN12 = '-'
        THEN TRANAMT12 * (-1)
        ELSE TRANAMT12
        END ORIGAMT, */
        CASE
          WHEN GLSIGN12 = '-'
          THEN TRANAMT12 * (-1) * CRATE
          ELSE TRANAMT12 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp12 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        12 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE13 GLCODE,
        GLSIGN13 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT13 TRANAMT,
        ---
        /* CASE
        WHEN GLSIGN13 = '-'
        THEN TRANAMT13 * (-1)
        ELSE TRANAMT13
        END ORIGAMT, */
        CASE
          WHEN GLSIGN13 = '-'
          THEN TRANAMT13 * (-1) * CRATE
          ELSE TRANAMT13 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp13 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        -- rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        13 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE14 GLCODE,
        GLSIGN14 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT14 TRANAMT,
        ---
        /*  CASE
        WHEN GLSIGN14 = '-'
        THEN TRANAMT14 * (-1)
        ELSE TRANAMT14
        END ORIGAMT, */
        CASE
          WHEN GLSIGN14 = '-'
          THEN TRANAMT14 * (-1) * CRATE
          ELSE TRANAMT14 * CRATE
        END ACCTAMT,
        ---
        sacscode,
        Sacstyp14 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        14 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      UNION ALL
      SELECT UNIQUE_NUMBER,
        RLDGACCT,
        ACCTCURR,
        BATCCOY,
        BATCBRN,
        BATCACTYR,
        BATCACTMN,
        BATCTRCDE,
        BATCBATCH,
        CRATE,
        EFFDATE,
        GENLCOY,
        GLCODE15 GLCODE,
        GLSIGN15 GLSIGN,
        ORIGCURR,
        POSTMONTH,
        POSTYEAR,
        TRANAMT15 TRANAMT,
        ---
        /*  CASE
        WHEN GLSIGN15 = '-'
        THEN TRANAMT15 * (-1)
        ELSE TRANAMT15
        END ORIGAMT,  */
        VM1DTA.Get_Acctamt(UNIQUE_NUMBER) ACCTAMT,
        ---
        sacscode,
        Sacstyp15 Sacstyp,
        rldgpfx,
        rdocpfx,
        rldgcoy,
        rdoccoy,
        --rldgacct,
        rdocnum,
        --tranno,
        TRANNO,
        usrprf,
        15 JRNSEQ,
        ' ' TRANREF,
        ' ' TRANDESC
      FROM VM1DTA.ZTRNPF
      ) ZTRN_GL
    ) TTRN_GLAUDF;

