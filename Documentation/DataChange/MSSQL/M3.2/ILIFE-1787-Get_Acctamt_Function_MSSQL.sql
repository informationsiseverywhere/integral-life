create Function Get_Acctamt (@v_unieq_number  BIGINT )
RETURNS FLOAT 
AS 
BEGIN
  DECLARE @total FLOAT,  @acctamt FLOAT,  @glsign_01 char, @glsign_02 char, @glsign_03 char, @glsign_04 char, @glsign_05 char, @glsign_06 char, @glsign_07 char, @glsign_08 char;
  DECLARE @glsign_09 char, @glsign_10 char, @glsign_11 char, @glsign_12 char, @glsign_13 char, @glsign_14 char, @glsign_15 char;
  DECLARE @ta_01 FLOAT, @ta_02 FLOAT, @ta_03 FLOAT, @ta_04 FLOAT, @ta_05 FLOAT, @ta_06 FLOAT, @ta_07 FLOAT, @ta_08 FLOAT;
  DECLARE @ta_09 FLOAT, @ta_10 FLOAT, @ta_11 FLOAT, @ta_12 FLOAT, @ta_13 FLOAT, @ta_14 FLOAT, @ta_15 FLOAT, @v_crate FLOAT;
  
  SELECT @glsign_01 = GLSIGN01, @glsign_02 = GLSIGN02, @glsign_03 = GLSIGN03, @glsign_04 = GLSIGN04, @glsign_05 = GLSIGN05, @glsign_06 = GLSIGN06, @glsign_07 = GLSIGN07, 
         @glsign_08 = GLSIGN08, @glsign_09 = GLSIGN09, @glsign_10 = GLSIGN10, @glsign_11 = GLSIGN11, @glsign_12 = GLSIGN12, @glsign_13 = GLSIGN13, @glsign_14 = GLSIGN14, @glsign_15 = GLSIGN15,
         @ta_01 = TRANAMT01, @ta_02 = TRANAMT02, @ta_03 = TRANAMT03, @ta_04 = TRANAMT04, @ta_05 = TRANAMT05, @ta_06 = TRANAMT06, @ta_07 = TRANAMT07, @ta_08 = TRANAMT08, @ta_09 = TRANAMT09, 
		 @ta_10 = TRANAMT10, @ta_11 = TRANAMT11, @ta_12 = TRANAMT12, @ta_13 = TRANAMT13, @ta_14 = TRANAMT14, @ta_15 = TRANAMT15, @v_crate = CRATE 
         
  FROM ZTRNPF
  WHERE unique_number = @v_unieq_number;
  
  set @acctamt = 0;
  set @total = 0;
  
  IF (@glsign_01 = '+') 
		set @acctamt = @acctamt - @ta_01 * @v_crate; 
  ELSE 
     set @acctamt = @acctamt - (-1)*(@ta_01)*(@v_crate); 
  
  
  IF (@glsign_02 = '+')  
		set @acctamt = @acctamt - @ta_02 * @v_crate;
  ELSE 
		set @acctamt = @acctamt - (-1)*(@ta_02)*(@v_crate);
  

  IF (@glsign_03 = '+')  
		set @acctamt = @acctamt - @ta_03 * @v_crate;
  ELSE 
		set @acctamt = @acctamt - (-1)*(@ta_03)*(@v_crate);
  
  
  IF (@glsign_04 = '+')  
		set @acctamt = @acctamt - @ta_04 * @v_crate;
  ELSE 
		set @acctamt = @acctamt - (-1)*(@ta_04)*(@v_crate);
  

  IF (@glsign_05 = '+')  
	set @acctamt = @acctamt - @ta_05 * @v_crate;
  ELSE 
	set @acctamt = @acctamt - (-1)*(@ta_05)*(@v_crate);
  
  
  IF (@glsign_06 = '+')  
  set @acctamt = @acctamt - @ta_06 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_06)*(@v_crate);
  

  IF (@glsign_07 = '+')  
  set @acctamt = @acctamt - @ta_07 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_07)*(@v_crate);
 
  
  IF (@glsign_08 = '+') 
  set @acctamt = @acctamt - @ta_08 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_08)*(@v_crate);
  

  IF (@glsign_09 = '+')  
  set @acctamt = @acctamt - @ta_09 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_09)*(@v_crate);
  
  
  IF (@glsign_10 = '+')  
  set @acctamt = @acctamt - @ta_10 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_10)*(@v_crate);
  

  IF (@glsign_11 = '+')  
  set @acctamt = @acctamt - @ta_11 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_11)*(@v_crate);
  
  
  IF (@glsign_12 = '+')  
  set @acctamt = @acctamt - @ta_12 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_12)*(@v_crate);
 

  IF (@glsign_13 = '+')  
  set @acctamt = @acctamt - @ta_13 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_13)*(@v_crate);
  
  
  IF (@glsign_14 = '+')  
  set @acctamt = @acctamt - @ta_14 * @v_crate;
  ELSE 
  set @acctamt = @acctamt - (-1)*(@ta_14)*(@v_crate);

   
RETURN @acctamt;

END;

