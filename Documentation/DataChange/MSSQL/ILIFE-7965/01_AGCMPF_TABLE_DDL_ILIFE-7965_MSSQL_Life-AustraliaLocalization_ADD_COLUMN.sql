IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('INITCOMMGST') and object_id in (SELECT object_id FROM sys.tables WHERE name ='AGCMPF'))
BEGIN
	ALTER TABLE AGCMPF ADD INITCOMMGST NUMERIC(17, 2) DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'GST On Initial Commission' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'AGCMPF', @level2type=N'COLUMN',@level2name=N'INITCOMMGST';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('RNWLCOMMGST') and object_id in (SELECT object_id FROM sys.tables WHERE name ='AGCMPF'))
BEGIN
	ALTER TABLE AGCMPF ADD RNWLCOMMGST NUMERIC(17, 2) DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'GST On Renewal Commission' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'AGCMPF', @level2type=N'COLUMN',@level2name=N'RNWLCOMMGST';
END
GO
