

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDRGPYMOP' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))       
                ALTER TABLE VM1DTA.REGPPF ADD WDRGPYMOP CHAR(1);

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDBANKKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))       
                ALTER TABLE VM1DTA.REGPPF ADD WDBANKKEY  CHAR(10);

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'WDBANKACCKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))         
                ALTER TABLE VM1DTA.REGPPF ADD WDBANKACCKEY  CHAR(20);