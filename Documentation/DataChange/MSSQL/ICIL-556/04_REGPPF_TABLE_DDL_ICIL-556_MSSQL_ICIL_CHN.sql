﻿
 ALTER TABLE VM1DTA.REGPPF ADD  ADJAMT DECIMAL(17,2);
 ALTER TABLE VM1DTA.REGPPF ADD  REASONCD VARCHAR(128); 
 ALTER TABLE VM1DTA.REGPPF ADD  NETAMT DECIMAL(17,2); 
 ALTER TABLE VM1DTA.REGPPF ADD  REASON VARCHAR(128) ;
 
 EXEC sp_addextendedproperty 
@name = N'MS_Description', @value = 'ADJUSTMENT AMOUNT',
@level0type = N'Schema', @level0name =VM1DTA, 
@level1type = N'Table',  @level1name = REGPPF, 
@level2type = N'Column', @level2name = ADJAMT; 

 EXEC sp_addextendedproperty 
@name = N'MS_Description', @value = 'ADJUSTMENT CODE',
@level0type = N'Schema', @level0name =VM1DTA, 
@level1type = N'Table',  @level1name = REGPPF, 
@level2type = N'Column', @level2name = REASONCD; 

 EXEC sp_addextendedproperty 
@name = N'MS_Description', @value = 'NET CLEAM AMOUNT',
@level0type = N'Schema', @level0name =VM1DTA, 
@level1type = N'Table',  @level1name = REGPPF, 
@level2type = N'Column', @level2name = NETAMT; 

 EXEC sp_addextendedproperty 
@name = N'MS_Description', @value = 'REASONCD DESCRIPTION',
@level0type = N'Schema', @level0name =VM1DTA, 
@level1type = N'Table',  @level1name = REGPPF, 
@level2type = N'Column', @level2name = REASON; 