IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('ZSLRYSPCT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCTXPF'))
ALTER TABLE ZCTXPF ADD ZSLRYSPCT NUMERIC(5,2);
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('ZSLRYSAMT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCTXPF'))
ALTER TABLE ZCTXPF ADD ZSLRYSAMT NUMERIC(17, 2);