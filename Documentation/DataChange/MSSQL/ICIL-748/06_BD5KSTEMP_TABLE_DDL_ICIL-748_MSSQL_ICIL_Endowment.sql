
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BD5KSTEMP' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN				
CREATE TABLE [VM1DTA].[BD5KSTEMP]
  (
  [BATCHID]       [bigint],
	[CNT] [int],
    [CHDRCOY]    [nchar](1),
    [CHDRNUM]    [nchar](8),
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[PLNSFX]        [int],
    [LIFE]          [nchar](2),
    [COVERAGE]      [nchar](2),
    [RIDER]         [nchar](2),    
    [PAYCURR]   [nchar](3),
    [VALIDFLAG]  [nchar](1),
	[FLAG]  [nchar](1),
	[APCAPLAMT]    [numeric](17, 2),
	[APINTAMT]    [numeric](17, 2),
	[APLSTCAPDATE]	[int], 
	[APNXTCAPDATE]	[int], 
	[APLSTINTBDTE]	[int], 
	[APNXTINTBDTE]	[int],
    [TRANNO]     [int],
    [CNTTYPE]    [nchar](3),
    [CHDRPFX]    [nchar](2),
    [SERVUNIT]   [nchar](2),
    [OCCDATE]    [int],
    [CCDATE]     [int],
    [COWNPFX]    [nchar](2),
    [COWNCOY]    [nchar](1),
    [COWNNUM]    [nchar](8),
    [COLLCHNL]   [nchar](2),
    [CNTBRANCH]  [nchar](2),
    [CNTCURR]    [nchar](3),
    [POLSUM]     [int],
    [POLINC]     [int],
    [AGNTPFX]    [nchar](2),
    [AGNTCOY]    [nchar](1),
    [AGNTNUM]    [nchar](8),
   [DATIME] [datetime2](6) );
   
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TEMP FILE FOR BD5KS' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BATCHID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'BATCHID';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CHDRNUM ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PLNSFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'PLNSFX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LIFE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'LIFE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COVERAGE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'COVERAGE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RIDER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'RIDER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYCURR' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'PAYCURR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VALID FLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'FLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APCAPLAMT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APCAPLAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APINTAMT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APINTAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APLSTCAPDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APLSTCAPDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APNXTCAPDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APNXTCAPDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APLSTINTBDTE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APLSTINTBDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APNXTINTBDTE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'APNXTINTBDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRANNO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'TRANNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNTTYPE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CNTTYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHDRPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CHDRPFX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SERVUNIT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'SERVUNIT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OCCDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'OCCDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CCDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CCDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COWNPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'COWNPFX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COWNCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'COWNCOY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COWNNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'COWNNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COLLCHNL' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'COLLCHNL';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNTBRANCH' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CNTBRANCH';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CNTCURR' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'CNTCURR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POLSUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'POLSUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POLINC' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'POLINC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AGNTPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'AGNTPFX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AGNTCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'AGNTCOY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AGNTNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'AGNTNUM ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DATIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BD5KSTEMP', @level2type=N'COLUMN',@level2name=N'DATIME ';
END
GO


  
