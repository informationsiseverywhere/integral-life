
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APCAPLAMT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APCAPLAMT NUMERIC(17,2);
EXEC sys.sp_addextendedproperty 
@name=N'MS_Description', 
@value=N'<CAPITALIZATION AMOUNT>' ,
 @level0type=N'SCHEMA',@level0name=N'VM1DTA',
 @level1type=N'TABLE',@level1name=N'ZRAEPF',
 @level2type=N'COLUMN',@level2name=N'APCAPLAMT';
END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APINTAMT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APINTAMT NUMERIC(17,2);
EXEC sys.sp_addextendedproperty
@name=N'MS_Description', 
@value=N'<INTEREST AMOUNT>' ,
@level0type=N'SCHEMA',@level0name=N'VM1DTA', 
@level1type=N'TABLE',@level1name=N'ZRAEPF',
 @level2type=N'COLUMN',@level2name=N'APINTAMT';
END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APLSTCAPDATE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APLSTCAPDATE NUMERIC(8,2);
EXEC sys.sp_addextendedproperty
 @name=N'MS_Description', 
@value=N'<LAST CAPITALISATION DATE>' ,
@level0type=N'SCHEMA',@level0name=N'VM1DTA',
@level1type=N'TABLE',@level1name=N'ZRAEPF',
 @level2type=N'COLUMN',@level2name=N'APLSTCAPDATE';
END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APNXTCAPDATE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APNXTCAPDATE NUMERIC(8,2);
EXEC sys.sp_addextendedproperty
 @name=N'MS_Description', 
@value=N'<NEXT CAPITALISATION DATE>' ,
@level0type=N'SCHEMA',@level0name=N'VM1DTA',
@level1type=N'TABLE',@level1name=N'ZRAEPF', 
@level2type=N'COLUMN',@level2name=N'APNXTCAPDATE';
END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('FLAG') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF]  ADD FLAG NCHAR(1);

 EXEC sys.sp_addextendedproperty 
@name = N'MS_Description', 
@value = N'<ENDOWMENT FLAG>', 
@level0type = N'SCHEMA', @level0name = N'VM1DTA', 
@level1type = N'TABLE',  @level1name = N'ZRAEPF',
@level2type = N'COLUMN', @level2name = N'FLAG';
END
GO


GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APLSTINTBDTE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APLSTINTBDTE NUMERIC(8,2);
EXEC sys.sp_addextendedproperty 
@name=N'MS_Description', 
@value=N'<LAST INTEREST BILLING DATE>' , 
@level0type=N'SCHEMA',@level0name=N'VM1DTA', 
@level1type=N'TABLE',@level1name=N'ZRAEPF', 
@level2type=N'COLUMN',@level2name=N'APLSTINTBDTE';
END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('APNXTINTBDTE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))
BEGIN
	ALTER TABLE [VM1DTA].[ZRAEPF] ADD APNXTINTBDTE NUMERIC(8,2);
EXEC sys.sp_addextendedproperty
 @name=N'MS_Description', 
@value=N'<NEXT INTEREST BILLING DATE>' , 
@level0type=N'SCHEMA',@level0name=N'VM1DTA',
@level1type=N'TABLE',@level1name=N'ZRAEPF', 
@level2type=N'COLUMN',@level2name=N'APNXTINTBDTE';
END
GO