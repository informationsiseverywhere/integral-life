IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E'AND h.EROREROR ='RFWT' AND h.ERORDESC = 'EndDate cannot > CessDate'  )
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFWT'
           ,'EndDate cannot > CessDate'
           ,'0'
           ,'0'
           ,'000008'
           ,''
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
           
GO
