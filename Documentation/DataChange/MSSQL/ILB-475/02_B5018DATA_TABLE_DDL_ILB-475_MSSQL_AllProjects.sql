IF EXISTS ( SELECT * FROM sys.tables where name = 'B5018TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[B5018TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'B5018DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[B5018DATA](
	[UNIQUE_NUMBER] [numeric](17, 0) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[LIFE] [nchar](2) NULL,
	[COVERAGE] [nchar](2) NULL,
	[RIDER] [nchar](2) NULL,
	[PLNSFX] [int] NULL,
	[PSTATCODE] [nchar](4) NULL,
	[STATCODE] [nchar](2) NULL,
	[CRRCD] [int] NULL,
	[CBUNST] [int] NULL,
	[RCESDTE] [int] NULL,
	[SUMINS] [numeric](17, 2) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[CURRFROM] [int] NULL,
	[CURRTO] [int] NULL,
	[CRTABLE] [nchar](4) NULL,
	[CNTCURR] [nchar](3) NULL,
	[CNTTYPE] [nchar](4) NULL,
	[TRANNO] [nchar](4) NULL,
	[COWNCOY] [nchar](4) NULL,
	[COWNNUM] [nchar](8) NULL,
	[JOBNM] [nchar](10) NULL,
	[USRPRF] [nchar](10) NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[BATCHID] [int] NULL
) ON [PRIMARY]

GO
exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR B5018DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA'

go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'COVERAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'COVERAGE'
go
exec sp_addextendedproperty 'MS_Description', 'RIDER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'RIDER'
go
exec sp_addextendedproperty 'MS_Description', 'PLNSFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'PLNSFX'
go
exec sp_addextendedproperty 'MS_Description', 'PSTATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'PSTATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'STATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'STATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'CRRCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CRRCD'
go
exec sp_addextendedproperty 'MS_Description', 'CBUNST', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CBUNST'
go
exec sp_addextendedproperty 'MS_Description', 'RCESDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'RCESDTE'
go
exec sp_addextendedproperty 'MS_Description', 'SUMINS', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'SUMINS'
go
exec sp_addextendedproperty 'MS_Description', 'VALIDFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'VALIDFLAG'
go
exec sp_addextendedproperty 'MS_Description', 'CURRFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CURRFROM'
go
exec sp_addextendedproperty 'MS_Description', 'CURRTO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CURRTO'
go
exec sp_addextendedproperty 'MS_Description', 'CRTABLE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CRTABLE'
go
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CNTCURR'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'COWNCOY'
go
exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'COWNNUM'
go
exec sp_addextendedproperty 'MS_Description', 'JOBNM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'JOBNM'
go
exec sp_addextendedproperty 'MS_Description', 'USRPRF', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'USRPRF'	
go
exec sp_addextendedproperty 'MS_Description', 'CNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'CNTBRANCH'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5018DATA', 'COLUMN',
                            'BATCHID'							
go
