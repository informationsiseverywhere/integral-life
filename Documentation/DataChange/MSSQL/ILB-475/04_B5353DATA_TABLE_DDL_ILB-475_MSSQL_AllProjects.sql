IF EXISTS ( SELECT * FROM sys.tables where name = 'B5353TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[B5353TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'B5353DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[B5353DATA](
	[CHDRNUM] [nchar](8) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[INSTFROM] [int] NULL,
	[CNTCURR] [nchar](3) NULL,
	[BILLCURR] [nchar](3) NULL,
	[PAYRSEQNO] [int] NULL,
	[CBILLAMT] [numeric](17, 2) NULL,
	[BILLCD] [int] NULL,
	[BILLCHNL] [nchar](2) NULL,
	[INSTAMT01] [numeric](17, 2) NULL,
	[INSTAMT02] [numeric](17, 2) NULL,
	[INSTAMT03] [numeric](17, 2) NULL,
	[INSTAMT04] [numeric](17, 2) NULL,
	[INSTAMT05] [numeric](17, 2) NULL,
	[INSTAMT06] [numeric](17, 2) NULL,
	[INSTFREQ] [nchar](2) NULL,
	[CHDRCNTTYPE] [nchar](3) NULL,
	[CHDROCCDATE] [int] NULL,
	[CHDRSTATCODE] [nchar](2) NULL,
	[CHDRPSTATCODE] [nchar](2) NULL,
	[CHDRTRANNO] [int] NULL,
	[CHDRINSTTOT01] [numeric](17, 2) NULL,
	[CHDRINSTTOT02] [numeric](17, 2) NULL,
	[CHDRINSTTOT03] [numeric](17, 2) NULL,
	[CHDRINSTTOT04] [numeric](17, 2) NULL,
	[CHDRINSTTOT05] [numeric](17, 2) NULL,
	[CHDRINSTTOT06] [numeric](17, 2) NULL,
	[CHDROUTSTAMT] [numeric](17, 2) NULL,
	[CHDRAGNTNUM] [nchar](8) NULL,
	[CHDRCNTCURR] [nchar](3) NULL,
	[CHDRPTDATE] [int] NULL,
	[CHDRRNWLSUPR] [nchar](1) NULL,
	[CHDRRNWLSPFROM] [int] NULL,
	[CHDRRNWLSPTO] [int] NULL,
	[CHDRCOWNCOY] [nchar](1) NULL,
	[CHDRCOWNNUM] [nchar](8) NULL,
	[CHDRCNTBRANCH] [nchar](2) NULL,
	[CHDRAGNTCOY] [nchar](1) NULL,
	[CHDRREGISTER] [nchar](3) NULL,
	[CHDRCHDRPFX] [nchar](2) NULL,
	[PAYRTAXRELMTH] [nchar](4) NULL,
	[PAYRINCOMESEQNO] [int] NULL,
	[PAYRTRANNO] [int] NULL,
	[PAYROUTSTAMT] [numeric](17, 2) NULL,
	[PAYRBILLCHNL] [nchar](2) NULL,
	[PAYRMANDREF] [nchar](5) NULL,
	[PAYRCNTCURR] [nchar](3) NULL,
	[PAYRBILLFREQ] [nchar](2) NULL,
	[PAYRBILLCD] [int] NULL,
	[PAYRPTDATE] [int] NULL,
	[PAYRBTDATE] [int] NULL,
	[PAYRNEXTDATE] [int] NULL,
	[LSINSTTO] [int] NULL,
	[CUNIQUE_NUMBER] [bigint] NULL,
	[LSUNIQUE_NUMBER] [bigint] NULL,
	[PSUNIQUE_NUMBER] [bigint] NULL,
	[BATCHNUM] [int] NULL,
	[AGLFDTEAPP] [int] NULL,
	[AGLFDTEEXP] [int] NULL,
	[AGLFDTETRM] [int] NULL,
	[HCSDZDIVOPT] [nchar](4) NULL,
	[HCSDZCSHDIVMTH] [nchar](4) NULL,
	[HDISHCAPNDT] [int] NULL,
	[MEMBER_NAME] [nchar](10) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR B5353DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA'

go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'INSTFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTFROM'
go
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CNTCURR'
go
exec sp_addextendedproperty 'MS_Description', 'BILLCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'BILLCURR'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRSEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRSEQNO'
go
exec sp_addextendedproperty 'MS_Description', 'CBILLAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CBILLAMT'
go
exec sp_addextendedproperty 'MS_Description', 'BILLCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'BILLCD'
go
exec sp_addextendedproperty 'MS_Description', 'BILLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'BILLCHNL'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT01', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT01'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT02', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT02'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT03', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT03'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT04', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT04'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT05', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT05'
go
exec sp_addextendedproperty 'MS_Description', 'INSTAMT06', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTAMT06'
go
exec sp_addextendedproperty 'MS_Description', 'INSTFREQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'INSTFREQ'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDROCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDROCCDATE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRSTATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRSTATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPSTATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRPSTATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                           'CHDRTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT01', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT01'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT02', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT02'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT03' , 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT03'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT04', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT04'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT05', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT05'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRINSTTOT06', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRINSTTOT06'
go    
exec sp_addextendedproperty 'MS_Description', 'CHDROUTSTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDROUTSTAMT'
go	
exec sp_addextendedproperty 'MS_Description', 'CHDRAGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRAGNTNUM'
go	
exec sp_addextendedproperty 'MS_Description', 'CHDRCNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCNTCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRPTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRPTDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRRNWLSUPR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRRNWLSUPR'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRRNWLSPFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRRNWLSPFROM'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRRNWLSPTO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRRNWLSPTO'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCOWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCOWNCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCOWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCOWNNUM'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCNTBRANCH'							
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRAGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRAGNTCOY'
go	
exec sp_addextendedproperty 'MS_Description', 'CHDRREGISTER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRREGISTER'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CHDRCHDRPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRTAXRELMTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRTAXRELMTH'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRINCOMESEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRINCOMESEQNO'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRTRANNO'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYROUTSTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYROUTSTAMT'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRBILLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRBILLCHNL'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRMANDREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRMANDREF'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRCNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRCNTCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'PAYRBILLFREQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRBILLFREQ'							
go   
exec sp_addextendedproperty 'MS_Description', 'PAYRBILLCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRBILLCD'							
go
exec sp_addextendedproperty 'MS_Description', 'PAYRPTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRPTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRBTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRBTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRNEXTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PAYRNEXTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'LSINSTTO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'LSINSTTO'
go
exec sp_addextendedproperty 'MS_Description', 'CUNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'CUNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'LSUNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'LSUNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'PSUNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'PSUNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'BATCHNUM'
go
exec sp_addextendedproperty 'MS_Description', 'AGLFDTEAPP', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'AGLFDTEAPP'
go
exec sp_addextendedproperty 'MS_Description', 'AGLFDTEEXP', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'AGLFDTEEXP'
go
exec sp_addextendedproperty 'MS_Description', 'AGLFDTETRM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'AGLFDTETRM'
go
exec sp_addextendedproperty 'MS_Description', 'HCSDZDIVOPT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'HCSDZDIVOPT'
go
exec sp_addextendedproperty 'MS_Description', 'HCSDZCSHDIVMTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'HCSDZCSHDIVMTH'
go
exec sp_addextendedproperty 'MS_Description', 'HDISHCAPNDT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'HDISHCAPNDT'
go
exec sp_addextendedproperty 'MS_Description', 'MEMBER_NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5353DATA', 'COLUMN',
                            'MEMBER_NAME'
go
