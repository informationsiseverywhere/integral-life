﻿IF EXISTS ( SELECT * FROM sys.tables where name = 'B5456TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[B5456TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'B5456DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[B5456DATA](
	[chdrcoy] [char](1) NULL,
	[chdrnum] [char](8) NULL,
	[life] [char](2) NULL,
	[coverage] [char](2) NULL,
	[rider] [char](2) NULL,
	[plnsfx] [numeric](4, 0) NULL,
	[seqno] [numeric](2, 0) NULL,
	[rasnum] [char](8) NULL,
	[rngmnt] [char](4) NULL,
	[currfrom] [numeric](8, 0) NULL,
	[tranno] [numeric](5, 0) NULL,
	[validflag] [char](1) NULL,
	[currcode] [char](3) NULL,
	[currto] [numeric](8, 0) NULL,
	[retype] [char](1) NULL,
	[raamount] [numeric](17, 2) NULL,
	[ctdate] [numeric](8, 0) NULL,
	[cmdate] [numeric](8, 0) NULL,
	[reasper] [numeric](5, 2) NULL,
	[recovamt] [numeric](17, 2) NULL,
	[cestype] [char](1) NULL,
	[lrkcls] [char](4) NULL,
	[chtranno] [numeric](5, 0) NULL,
	[chcnttype] [char](3) NULL,
	[chstatcode] [char](2) NULL,
	[chpstcde] [char](2) NULL,
	[chbillfreq] [char](2) NULL,
	[chcntcurr] [char](3) NULL,
	[chpolsum] [numeric](4, 0) NULL,
	[chbillchnl] [char](2) NULL,
	[choccdate] [numeric](8, 0) NULL,
	[chpolinc] [numeric](4, 0) NULL,
	[cocrrcd] [numeric](8, 0) NULL,
	[cocrtable] [char](4) NULL,
	[cosumins] [numeric](17, 2) NULL,
	[cojlife] [char](2) NULL,
	[coinstprem] [numeric](17, 2) NULL,
	[cochdrcoy] [char](1) NULL,
	[cochdrnum] [char](8) NULL,
	[colife] [char](2) NULL,
	[cocoverage] [char](2) NULL,
	[corider] [char](2) NULL,
	[comortcls] [char](1) NULL,
	[corcesdte] [numeric](8, 0) NULL,
	[covalidflag] [char](1) NULL,
	[rasaagntnum] [char](8) NULL,
	[anbccd01] [numeric](3, 0) NULL,
	[cltsex01] [char](1) NULL,
	[anbccd02] [numeric](3, 0) NULL,
	[cltsex02] [char](1) NULL,
	[maxagerate] [numeric](18, 0) NULL,
	[maxoppc] [numeric](18, 0) NULL,
	[maxinsprm] [numeric](18, 0) NULL,
	[oppcdiv] [numeric](18, 0) NULL,
	[rauq] [numeric](18, 0) NULL,
	[chuq] [numeric](18, 0) NULL,
	[chcurm] [numeric](8, 0) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR B5456DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA'
go
exec sp_addextendedproperty 'MS_Description', 'chdrcoy', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chdrcoy'
go
exec sp_addextendedproperty 'MS_Description', 'chdrnum', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chdrnum'
go
exec sp_addextendedproperty 'MS_Description', 'life', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'life'
go
exec sp_addextendedproperty 'MS_Description', 'coverage', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'coverage'
go
exec sp_addextendedproperty 'MS_Description', 'rider', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'rider'
go
exec sp_addextendedproperty 'MS_Description', 'plnsfx', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'plnsfx'
go
exec sp_addextendedproperty 'MS_Description', 'seqno', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'seqno'
go
exec sp_addextendedproperty 'MS_Description', 'rasnum', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'rasnum'
go
exec sp_addextendedproperty 'MS_Description', 'rngmnt', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'rngmnt'
go
exec sp_addextendedproperty 'MS_Description', 'currfrom', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'currfrom'
go
exec sp_addextendedproperty 'MS_Description', 'tranno', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'tranno'
go
exec sp_addextendedproperty 'MS_Description', 'validflag', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'validflag'
go
exec sp_addextendedproperty 'MS_Description', 'currcode', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'currcode'
go
exec sp_addextendedproperty 'MS_Description', 'currto', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'currto'
go
exec sp_addextendedproperty 'MS_Description', 'retype', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'retype'
go
exec sp_addextendedproperty 'MS_Description', 'raamount', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'raamount'
go
exec sp_addextendedproperty 'MS_Description', 'ctdate', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'ctdate'
go
exec sp_addextendedproperty 'MS_Description', 'cmdate', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cmdate'
go
exec sp_addextendedproperty 'MS_Description', 'reasper', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'reasper'
go
exec sp_addextendedproperty 'MS_Description', 'recovamt', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'recovamt'
go
exec sp_addextendedproperty 'MS_Description', 'cestype', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cestype'
go
exec sp_addextendedproperty 'MS_Description', 'lrkcls', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'lrkcls'
go
exec sp_addextendedproperty 'MS_Description', 'chtranno', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chtranno'
go
exec sp_addextendedproperty 'MS_Description', 'chcnttype' , 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chcnttype'
go
exec sp_addextendedproperty 'MS_Description', 'chstatcode', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chstatcode'
go
exec sp_addextendedproperty 'MS_Description', 'chpstcde', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chpstcde'
go
exec sp_addextendedproperty 'MS_Description', 'chbillfreq', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chbillfreq'
go
exec sp_addextendedproperty 'MS_Description', 'chcntcurr', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chcntcurr'
go	
exec sp_addextendedproperty 'MS_Description', 'chpolsum', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chpolsum'
go	
exec sp_addextendedproperty 'MS_Description', 'chbillchnl', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chbillchnl'
go 
exec sp_addextendedproperty 'MS_Description', 'choccdate', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'choccdate'
go 
exec sp_addextendedproperty 'MS_Description', 'chpolinc', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chpolinc'
go 
exec sp_addextendedproperty 'MS_Description', 'cocrrcd', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cocrrcd'
go 
exec sp_addextendedproperty 'MS_Description', 'cocrtable', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cocrtable'
go 
exec sp_addextendedproperty 'MS_Description', 'cosumins', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cosumins'
go 
exec sp_addextendedproperty 'MS_Description', 'cojlife', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cojlife'
go 
exec sp_addextendedproperty 'MS_Description', 'coinstprem', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'coinstprem'
go 
exec sp_addextendedproperty 'MS_Description', 'cochdrcoy', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cochdrcoy'
go	
exec sp_addextendedproperty 'MS_Description', 'cochdrnum', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cochdrnum'
go 
exec sp_addextendedproperty 'MS_Description', 'colife', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'colife'
go 
exec sp_addextendedproperty 'MS_Description', 'cocoverage', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cocoverage'
go 
exec sp_addextendedproperty 'MS_Description', 'corider', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'corider'
go 
exec sp_addextendedproperty 'MS_Description', 'comortcls', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'comortcls'
go 
exec sp_addextendedproperty 'MS_Description', 'corcesdte', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'corcesdte'
go 
exec sp_addextendedproperty 'MS_Description', 'covalidflag', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'covalidflag'
go 
exec sp_addextendedproperty 'MS_Description', 'rasaagntnum', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'rasaagntnum'
go
exec sp_addextendedproperty 'MS_Description', 'anbccd01', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'anbccd01'
go 
exec sp_addextendedproperty 'MS_Description', 'cltsex01', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cltsex01'							
go   
exec sp_addextendedproperty 'MS_Description', 'anbccd02', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'anbccd02'							
go
exec sp_addextendedproperty 'MS_Description', 'cltsex02', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'cltsex02'
go
exec sp_addextendedproperty 'MS_Description', 'maxagerate', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'maxagerate'
go
exec sp_addextendedproperty 'MS_Description', 'maxinsprm', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'maxinsprm'
go
exec sp_addextendedproperty 'MS_Description', 'oppcdiv', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'oppcdiv'
go
exec sp_addextendedproperty 'MS_Description', 'rauq', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'rauq'
go
exec sp_addextendedproperty 'MS_Description', 'chuq', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chuq'
go
exec sp_addextendedproperty 'MS_Description', 'chcurm', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5456DATA', 'COLUMN',
                            'chcurm'
go

