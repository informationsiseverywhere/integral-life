﻿IF EXISTS ( SELECT * FROM sys.tables where name = 'BR64XTEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR64XTEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR64XDATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR64XDATA](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[PAIDBY] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[DTETRM] [int] NULL,
	[DESC_T] [nchar](50) NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[CHDRTRANNO] [int] NULL,
	[CHDRUNIQUENO] [bigint] NOT NULL,
	[CHDRPFX] [nchar](2) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[OCCDATE] [int] NULL,
	[CNTCURR] [nchar](3) NULL,
	[PTDATE] [int] NULL,
	[MEDIUNIQUE] [bigint] NOT NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR64XDATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'SEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'SEQNO'
go
exec sp_addextendedproperty 'MS_Description', 'EXMCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'EXMCODE'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'ZMEDTYP'
go
exec sp_addextendedproperty 'MS_Description', 'PAIDBY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'PAIDBY'
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'JLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'JLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'EFFDATE'
go
exec sp_addextendedproperty 'MS_Description', 'INVREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'INVREF'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDFEE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'ZMEDFEE'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'DTETRM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'DTETRM'
go
exec sp_addextendedproperty 'MS_Description', 'DESC_T', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'DESC_T'
go
exec sp_addextendedproperty 'MS_Description', 'MEMBER_NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'MEMBER_NAME'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CHDRTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRUNIQUENO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CHDRUNIQUENO'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CHDRPFX'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'OCCDATE'
go
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'CNTCURR'
go
exec sp_addextendedproperty 'MS_Description', 'PTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'PTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'MEDIUNIQUE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR64XDATA', 'COLUMN',
                            'MEDIUNIQUE'
go

