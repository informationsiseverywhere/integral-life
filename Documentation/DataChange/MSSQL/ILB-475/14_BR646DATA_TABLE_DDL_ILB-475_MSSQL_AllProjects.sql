IF EXISTS ( SELECT * FROM sys.tables where name = 'BR646TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR646TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR646DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[BR646DATA](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[PAIDBY] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[DTETRM] [int] NULL,
	[DESC_T] [nchar](50) NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[CHDRUNIQUE] [bigint] NOT NULL,
	[TRANNO] [int] NULL,
	[CHDRPFX] [nchar](2) NULL,
	[CNTCURR] [nchar](3) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[TOTPYMT] [numeric](38, 2) NULL,
	[MEDIUNIQUENO] [bigint] NOT NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[PAYMTH] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLTTYPE] [nchar](1) NULL,
	[LANGUAGE] [nchar](1) NULL,
	[SURNAME] [nchar](30) NULL,
	[INITIALS] [nchar](2) NULL,
	[SALUTL] [nchar](8) NULL,
	[LSURNAME] [nchar](60) NULL,
	[LGIVNAME] [nchar](60) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR646DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA'

go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CHDRCOY'							
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'SEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'SEQNO'
go
exec sp_addextendedproperty 'MS_Description', 'EXMCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'EXMCODE'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'ZMEDTYP'
go
exec sp_addextendedproperty 'MS_Description', 'PAIDBY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'PAIDBY'
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'JLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'JLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'EFFDATE'
go
exec sp_addextendedproperty 'MS_Description', 'INVREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'INVREF'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDFEE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'ZMEDFEE'							
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'DTETRM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'DTETRM'
go
exec sp_addextendedproperty 'MS_Description', 'DESC_T', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'DESC_T'
go
exec sp_addextendedproperty 'MS_Description', 'MEMBER_NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'MEMBER_NAME'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRUNIQUE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CHDRUNIQUE'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CHDRPFX'
go
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CNTCURR'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'TOTPYMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'TOTPYMT'
go
exec sp_addextendedproperty 'MS_Description', 'MEDIUNIQUENO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'MEDIUNIQUENO'
go
exec sp_addextendedproperty 'MS_Description', 'BANKKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'BANKKEY'
go
exec sp_addextendedproperty 'MS_Description', 'BANKACCKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'BANKACCKEY'							
go
exec sp_addextendedproperty 'MS_Description', 'PAYMTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'PAYMTH'
go
exec sp_addextendedproperty 'MS_Description', 'FACTHOUS', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'FACTHOUS'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CLNTPFX'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CLNTCOY'		
go
exec sp_addextendedproperty 'MS_Description', 'CLTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'CLTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'LANGUAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'LANGUAGE'
go
exec sp_addextendedproperty 'MS_Description', 'SURNAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'SURNAME'
go
exec sp_addextendedproperty 'MS_Description', 'INITIALS', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'INITIALS'
go
exec sp_addextendedproperty 'MS_Description', 'SALUTL', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'SALUTL'
go
exec sp_addextendedproperty 'MS_Description', 'LSURNAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'LSURNAME'							
go
exec sp_addextendedproperty 'MS_Description', 'LGIVNAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR646DATA', 'COLUMN',
                            'LGIVNAME'					
go



