﻿IF EXISTS ( SELECT * FROM sys.tables where name = 'B5349TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[B5349TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'B5349DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[B5349DATA](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PAYRSEQNO] [int] NULL,
	[BILLSUPR] [nchar](1) NULL,
	[BILLSPFROM] [int] NULL,
	[BILLSPTO] [int] NULL,
	[BILLCHNL] [nchar](2) NULL,
	[BILLCD] [int] NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[PAYRTRANNO] [int] NULL,
	[PAYRBILLCD] [int] NULL,
	[BTDATE] [int] NULL,
	[PTDATE] [int] NULL,
	[OUTSTAMT] [numeric](17, 2) NULL,
	[NEXTDATE] [int] NULL,
	[PAYRBILLCURR] [nchar](3) NULL,
	[BILLFREQ] [nchar](2) NULL,
	[DUEDD] [nchar](2) NULL,
	[DUEMM] [nchar](2) NULL,
	[BILLDAY] [nchar](2) NULL,
	[BILLMONTH] [nchar](2) NULL,
	[SINSTAMT01] [numeric](17, 2) NULL,
	[SINSTAMT02] [numeric](17, 2) NULL,
	[SINSTAMT03] [numeric](17, 2) NULL,
	[SINSTAMT04] [numeric](17, 2) NULL,
	[SINSTAMT05] [numeric](17, 2) NULL,
	[SINSTAMT06] [numeric](17, 2) NULL,
	[PAYRCNTCURR] [nchar](3) NULL,
	[MANDREF] [nchar](5) NULL,
	[GRUPCOY] [nchar](1) NULL,
	[GRUPNUM] [nchar](8) NULL,
	[MEMBSEL] [nchar](10) NULL,
	[TAXRELMTH] [nchar](4) NULL,
	[INCSEQNO] [int] NULL,
	[EFFDATE] [int] NULL,
	[CHDRUNIQUENO] [bigint] NULL,
	[CHDRTRANNO] [int] NULL,
	[CNTTYPE] [nchar](3) NULL,
	[OCCDATE] [int] NULL,
	[STATCODE] [nchar](2) NULL,
	[PSTCDE] [nchar](2) NULL,
	[REG] [nchar](3) NULL,
	[CHDRCNTCURR] [nchar](3) NULL,
	[BILLCURR] [nchar](3) NULL,
	[CHDRPFX] [nchar](2) NULL,
	[COWNCOY] [nchar](1) NULL,
	[COWNNUM] [nchar](8) NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[SERVUNIT] [nchar](2) NULL,
	[CCDATE] [int] NULL,
	[COLLCHNL] [nchar](2) NULL,
	[COWNPFX] [nchar](2) NULL,
	[AGNTPFX] [nchar](2) NULL,
	[AGNTCOY] [nchar](1) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[CHDRBILLCHNL] [nchar](2) NULL,
	[ACCTMETH] [nchar](1) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[MANDSTAT] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR B5349DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRNUM'						
go
exec sp_addextendedproperty 'MS_Description', 'PAYRSEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PAYRSEQNO'						
go
exec sp_addextendedproperty 'MS_Description', 'BILLSUPR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLSUPR'
go
exec sp_addextendedproperty 'MS_Description', 'BILLSPFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLSPFROM'
go
exec sp_addextendedproperty 'MS_Description', 'BILLSPTO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLSPTO '
go
exec sp_addextendedproperty 'MS_Description', 'BILLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLCHNL'
go
exec sp_addextendedproperty 'MS_Description', 'BILLCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLCD'
go
exec sp_addextendedproperty 'MS_Description', 'MEMBER_NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'MEMBER_NAME'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PAYRTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRBILLCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PAYRBILLCD'
go
exec sp_addextendedproperty 'MS_Description', 'BTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'PTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'OUTSTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'OUTSTAMT'
go
exec sp_addextendedproperty 'MS_Description', 'NEXTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'NEXTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRBILLCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PAYRBILLCURR'
go
exec sp_addextendedproperty 'MS_Description', 'BILLFREQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLFREQ'
go
exec sp_addextendedproperty 'MS_Description', 'DUEDD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'DUEDD'
go
exec sp_addextendedproperty 'MS_Description', 'DUEMM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'DUEMM'
go
exec sp_addextendedproperty 'MS_Description', 'BILLDAY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLDAY'
go

exec sp_addextendedproperty 'MS_Description', 'BILLMONTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLMONTH'
go

exec sp_addextendedproperty 'MS_Description', 'SINSTAMT01', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT01'
go

exec sp_addextendedproperty 'MS_Description', 'SINSTAMT02', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT02'
go
exec sp_addextendedproperty 'MS_Description', 'SINSTAMT03', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT03'
go
exec sp_addextendedproperty 'MS_Description', 'SINSTAMT04', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT04'
go
exec sp_addextendedproperty 'MS_Description', 'SINSTAMT05', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT05'
go	
exec sp_addextendedproperty 'MS_Description', 'SINSTAMT06', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SINSTAMT06'
go	
exec sp_addextendedproperty 'MS_Description', 'PAYRCNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PAYRCNTCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'MANDREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'MANDREF'
go 
exec sp_addextendedproperty 'MS_Description', 'GRUPCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'GRUPCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'GRUPNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'GRUPNUM'
go 
exec sp_addextendedproperty 'MS_Description', 'MEMBSEL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'MEMBSEL'
go 
exec sp_addextendedproperty 'MS_Description', 'TAXRELMTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'TAXRELMTH'
go 
exec sp_addextendedproperty 'MS_Description', 'INCSEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'INCSEQNO'						
go 
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'EFFDATE'							
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRUNIQUENO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRUNIQUENO'				
go	
exec sp_addextendedproperty 'MS_Description', 'CHDRTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRTRANNO'
go 
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CNTTYPE'
go 
exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'OCCDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'STATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'STATCODE'
go 
exec sp_addextendedproperty 'MS_Description', 'PSTCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'PSTCDE'
go 
exec sp_addextendedproperty 'MS_Description', 'REG', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'REG'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRCNTCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'BILLCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BILLCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRPFX'							
go   
exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'COWNCOY'							
go
exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'COWNNUM'
go
exec sp_addextendedproperty 'MS_Description', 'CNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CNTBRANCH'
go
exec sp_addextendedproperty 'MS_Description', 'SERVUNIT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'SERVUNIT'
go
exec sp_addextendedproperty 'MS_Description', 'CCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CCDATE'
go
exec sp_addextendedproperty 'MS_Description', 'COLLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'COLLCHNL'
go
exec sp_addextendedproperty 'MS_Description', 'COWNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'COWNPFX'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'AGNTPFX'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'AGNTCOY'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'AGNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRBILLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CHDRBILLCHNL'
go
exec sp_addextendedproperty 'MS_Description', 'ACCTMETH', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'ACCTMETH'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CLNTPFX'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CLNTCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'BANKKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BANKKEY'							
go
exec sp_addextendedproperty 'MS_Description', 'BANKACCKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'BANKACCKEY'
go
exec sp_addextendedproperty 'MS_Description', 'MANDSTAT', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'MANDSTAT'
go
exec sp_addextendedproperty 'MS_Description', 'FACTHOUS', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5349DATA', 'COLUMN',
                            'FACTHOUS'							
go


