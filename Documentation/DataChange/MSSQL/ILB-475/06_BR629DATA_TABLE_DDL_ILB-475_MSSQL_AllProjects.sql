IF EXISTS ( SELECT * FROM sys.tables where name = 'BR629TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR629TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR629DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[BR629DATA](
	[CHDRPFX] [nchar](2) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[COWNPFX] [nchar](2) NULL,
	[COWNCOY] [nchar](1) NULL,
	[COWNNUM] [nchar](8) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[OCCDATE] [numeric](8, 0) NULL,
	[TRANNO] [numeric](5, 0) NULL,
	[STATCODE] [nchar](2) NULL,
	[PSTCDE] [nchar](2) NULL,
	[ZSUFCDTE] [numeric](8, 0) NULL,
	[LSURNAME] [nchar](60) NULL,
	[LGIVNAME] [nchar](60) NULL,
	[LSURNAME2] [nchar](60) NULL,
	[LGIVNAME2] [nchar](60) NULL,
	[CLTDOB] [numeric](8, 0) NULL,
	[LIFCNUM] [nchar](8) NULL
) ON [PRIMARY]

GO
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'CHDRPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'CHDRCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'CHDRNUM'
go 
exec sp_addextendedproperty 'MS_Description', 'COWNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'COWNPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'COWNCOY'					
go 
exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'COWNNUM'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'CNTTYPE'
go 
exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'OCCDATE'							
go   
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'TRANNO'							
go
exec sp_addextendedproperty 'MS_Description', 'STATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'STATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'PSTCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'PSTCDE'
go
exec sp_addextendedproperty 'MS_Description', 'ZSUFCDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'ZSUFCDTE'		
go 
exec sp_addextendedproperty 'MS_Description', 'LSURNAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'LSURNAME'
go 
exec sp_addextendedproperty 'MS_Description', 'LGIVNAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'LGIVNAME'
go 
exec sp_addextendedproperty 'MS_Description', 'LSURNAME2', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'LSURNAME2'
go 
exec sp_addextendedproperty 'MS_Description', 'LGIVNAME2', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'LGIVNAME2'					
go 
exec sp_addextendedproperty 'MS_Description', 'CLTDOB', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'CLTDOB'							
go
exec sp_addextendedproperty 'MS_Description', 'LIFCNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR629DATA', 'COLUMN',
                            'LIFCNUM'

go 

