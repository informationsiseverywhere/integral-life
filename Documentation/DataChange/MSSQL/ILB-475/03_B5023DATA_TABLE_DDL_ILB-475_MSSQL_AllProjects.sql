IF EXISTS ( SELECT * FROM sys.tables where name = 'B5023TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[B5023TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'B5023DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[B5023DATA](
	[COCHDRCOY] [nchar](1) NULL,
	[COCHDRNUM] [nchar](8) NULL,
	[COLIFE] [nchar](2) NULL,
	[COCOVERAGE] [nchar](2) NULL,
	[CORIDER] [nchar](2) NULL,
	[COPLNSFX] [numeric](4, 0) NULL,
	[COCRTABLE] [nchar](4) NULL,
	[COPSTATCODE] [nchar](2) NULL,
	[COSTATCODE] [nchar](2) NULL,
	[COCRRCD] [numeric](8, 0) NULL,
	[COCBUNST] [numeric](8, 0) NULL,
	[CORCESDTE] [numeric](8, 0) NULL,
	[COSUMINS] [numeric](17, 2) NULL,
	[COVALIDFLAG] [nchar](1) NULL,
	[COCURRFROM] [numeric](8, 0) NULL,
	[COCURRTO] [numeric](8, 0) NULL
) ON [PRIMARY]

GO
exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR B5023DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA'

go
exec sp_addextendedproperty 'MS_Description', 'COCHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'COCHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'COLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'COCOVERAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCOVERAGE'
go
exec sp_addextendedproperty 'MS_Description', 'CORIDER', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'CORIDER'
go
exec sp_addextendedproperty 'MS_Description', 'COPLNSFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COPLNSFX'
go
exec sp_addextendedproperty 'MS_Description', 'COCRTABLE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCRTABLE'
go
exec sp_addextendedproperty 'MS_Description', 'COPSTATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COPSTATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'COSTATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COSTATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'COCRRCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCRRCD'
go
exec sp_addextendedproperty 'MS_Description', 'COCBUNST', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCBUNST'
go
exec sp_addextendedproperty 'MS_Description', 'CORCESDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'CORCESDTE'
go
exec sp_addextendedproperty 'MS_Description', 'COSUMINS', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COSUMINS'
go
exec sp_addextendedproperty 'MS_Description', 'COVALIDFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COVALIDFLAG'
go
exec sp_addextendedproperty 'MS_Description', 'COCURRFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCURRFROM'
go
exec sp_addextendedproperty 'MS_Description', 'COCURRTO', 'SCHEMA', 'VM1DTA', 'TABLE', 'B5023DATA', 'COLUMN',
                            'COCURRTO'
go
