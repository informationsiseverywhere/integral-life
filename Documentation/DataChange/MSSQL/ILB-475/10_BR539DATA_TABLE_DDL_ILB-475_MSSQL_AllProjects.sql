IF EXISTS ( SELECT * FROM sys.tables where name = 'BR539TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR539TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR539DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR539DATA](
	[CNT] [int] NULL,
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[LOANNUMBER] [int] NULL,
	[LOANTYPE] [nchar](1) NULL,
	[LOANCURR] [nchar](3) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[FTRANNO] [int] NULL,
	[LTRANNO] [int] NULL,
	[LOANORIGAM] [numeric](17, 2) NULL,
	[LOANSTDATE] [int] NULL,
	[LSTCAPLAMT] [numeric](17, 2) NULL,
	[LSTCAPDATE] [int] NULL,
	[NXTCAPDATE] [int] NULL,
	[LSTINTBDTE] [int] NULL,
	[NXTINTBDTE] [int] NULL,
	[TPLSTMDTY] [numeric](17, 2) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[CH_UNIQUE_NUMBER] [bigint] NOT NULL,
	[CURRFROM] [int] NULL,
	[TRANNO] [int] NULL,
	[CNTTYPE] [nchar](3) NULL,
	[CHDRPFX] [nchar](2) NULL,
	[SERVUNIT] [nchar](2) NULL,
	[OCCDATE] [int] NULL,
	[CCDATE] [int] NULL,
	[COWNPFX] [nchar](2) NULL,
	[COWNCOY] [nchar](1) NULL,
	[COWNNUM] [nchar](8) NULL,
	[COLLCHNL] [nchar](2) NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[AGNTPFX] [nchar](2) NULL,
	[AGNTCOY] [nchar](1) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[MANDREF] [nchar](5) NULL,
	[PTDATE] [int] NULL,
	[BILLCHNL] [nchar](2) NULL,
	[BILLFREQ] [nchar](2) NULL,
	[BILLCURR] [nchar](3) NULL,
	[NEXTDATE] [int] NULL,
	[BANKKEY] [nchar](10) NULL,
	[BANKACCKEY] [nchar](20) NULL,
	[MANDSTAT] [nchar](2) NULL,
	[FACTHOUS] [nchar](2) NULL,
	[DATIME] [datetime] NOT NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR539DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA'
go
exec sp_addextendedproperty 'MS_Description', 'CNT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CNT'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'LOANNUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LOANNUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'LOANTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LOANTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'LOANCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LOANCURR'
go
exec sp_addextendedproperty 'MS_Description', 'VALIDFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'VALIDFLAG'
go
exec sp_addextendedproperty 'MS_Description', 'FTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'FTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'LTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'LOANORIGAM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LOANORIGAM'
go
exec sp_addextendedproperty 'MS_Description', 'LOANSTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LOANSTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'LSTCAPLAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LSTCAPLAMT'
go
exec sp_addextendedproperty 'MS_Description', 'LSTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LSTCAPDATE'
go
exec sp_addextendedproperty 'MS_Description', 'NXTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'NXTCAPDATE'
go
exec sp_addextendedproperty 'MS_Description', 'LSTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'LSTINTBDTE'
go
exec sp_addextendedproperty 'MS_Description', 'NXTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'NXTINTBDTE'
go
exec sp_addextendedproperty 'MS_Description', 'TPLSTMDTY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'TPLSTMDTY'
go
exec sp_addextendedproperty 'MS_Description', 'TERMID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'TERMID'
go
exec sp_addextendedproperty 'MS_Description', 'USER_T', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'USER_T'
go
exec sp_addextendedproperty 'MS_Description', 'TRDT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'TRDT'
go
exec sp_addextendedproperty 'MS_Description', 'TRTM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'TRTM'
go
exec sp_addextendedproperty 'MS_Description', 'USRPRF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'USRPRF'
go
exec sp_addextendedproperty 'MS_Description', 'JOBNM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'JOBNM'
go
exec sp_addextendedproperty 'MS_Description', 'CH_UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CH_UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CURRFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CURRFROM'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CHDRPFX'
go
exec sp_addextendedproperty 'MS_Description', 'SERVUNIT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'SERVUNIT'
go	
exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'OCCDATE'
go	
exec sp_addextendedproperty 'MS_Description', 'CCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CCDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'COWNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'COWNPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'COWNCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'COWNNUM'
go 
exec sp_addextendedproperty 'MS_Description', 'COLLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'COLLCHNL'
go 
exec sp_addextendedproperty 'MS_Description', 'CNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CNTBRANCH'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'AGNTPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'AGNTCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'AGNTNUM'
go	
exec sp_addextendedproperty 'MS_Description', 'CLNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CLNTPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'CLNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CLNTCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'CLNTNUM'
go 
exec sp_addextendedproperty 'MS_Description', 'MANDREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'MANDREF'
go 
exec sp_addextendedproperty 'MS_Description', 'PTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'PTDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'BILLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BILLCHNL'
go 
exec sp_addextendedproperty 'MS_Description', 'BILLFREQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BILLFREQ'
go 
exec sp_addextendedproperty 'MS_Description', 'BILLCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BILLCURR'
go
exec sp_addextendedproperty 'MS_Description', 'NEXTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'NEXTDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'BANKKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BANKKEY'							
go   
exec sp_addextendedproperty 'MS_Description', 'BANKACCKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'BANKACCKEY'							
go
exec sp_addextendedproperty 'MS_Description', 'MANDSTAT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'MANDSTAT'
go
exec sp_addextendedproperty 'MS_Description', 'FACTHOUS', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'FACTHOUS'
go
exec sp_addextendedproperty 'MS_Description', 'DATIME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR539DATA', 'COLUMN',
                            'DATIME'
go

