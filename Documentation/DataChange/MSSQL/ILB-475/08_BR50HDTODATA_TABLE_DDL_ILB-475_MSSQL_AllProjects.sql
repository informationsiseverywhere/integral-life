﻿IF EXISTS ( SELECT * FROM sys.tables where name = 'BR50HDTOTemp' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR50HDTOTemp]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR50HDTODATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR50HDTODATA](
	[CHDRUNIQUENUMBER] [numeric](18, 0) NULL,
	[CHDRCOY] [char](1) NULL,
	[CHDRNUM] [char](8) NULL,
	[CNTCURR] [char](3) NULL,
	[STATCODE] [char](2) NULL,
	[PSTCDE] [char](2) NULL,
	[CNTTYPE] [char](3) NULL,
	[COVTFLAG] [char](2) NULL,
	[HPADUNIQUENUMBER] [numeric](18, 0) NULL,
	[HPRRCVDT] [numeric](8, 0) NULL,
	[HPROPDTE] [numeric](8, 0) NULL,
	[HUWDCDTE] [numeric](8, 0) NULL,
	[BILLFREQ] [char](2) NULL,
	[BTDATE] [numeric](8, 0) NULL,
	[INCSEQNO] [numeric](2, 0) NULL,
	[PAYRPAYRSEQNO] [numeric](1, 0) NULL,
	[CLNTCOY] [char](1) NULL,
	[CLNTNUM] [char](8) NULL,
	[BATCHID] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR50HDTODATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRUNIQUENUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CHDRUNIQUENUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CHDRCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CNTCURR'
go
exec sp_addextendedproperty 'MS_Description', 'STATCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'STATCODE'
go
exec sp_addextendedproperty 'MS_Description', 'PSTCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'PSTCDE'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'COVTFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'COVTFLAG'
go
exec sp_addextendedproperty 'MS_Description', 'HPADUNIQUENUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'HPADUNIQUENUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'HPRRCVDT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'HPRRCVDT'
go
exec sp_addextendedproperty 'MS_Description', 'HPROPDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'HPROPDTE'
go
exec sp_addextendedproperty 'MS_Description', 'HUWDCDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'HUWDCDTE'
go
exec sp_addextendedproperty 'MS_Description', 'BILLFREQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'BILLFREQ'
go
exec sp_addextendedproperty 'MS_Description', 'BTDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'BTDATE'
go
exec sp_addextendedproperty 'MS_Description', 'INCSEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'INCSEQNO'
go
exec sp_addextendedproperty 'MS_Description', 'PAYRPAYRSEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'PAYRPAYRSEQNO'							
go
exec sp_addextendedproperty 'MS_Description', 'CLNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CLNTCOY'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR50HDTODATA', 'COLUMN',
                            'BATCHID'
go


