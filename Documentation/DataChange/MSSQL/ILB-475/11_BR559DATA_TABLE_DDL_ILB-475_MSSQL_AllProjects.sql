IF EXISTS ( SELECT * FROM sys.tables where name = 'BR559TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR559TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR559DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR559DATA](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[PAIDBY] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[DTETRM] [int] NULL,
	[DESC_T] [nchar](50) NULL,
	[MEMBER_NAME] [nchar](10) NULL,
	[LIFCNUM] [nchar](8) NULL
) ON [PRIMARY]

GO
exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR559DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'CHDRCOY'						
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'SEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'SEQNO'
go
exec sp_addextendedproperty 'MS_Description', 'EXMCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'EXMCODE'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'ZMEDTYP'
go
exec sp_addextendedproperty 'MS_Description', 'PAIDBY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'PAIDBY'
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'JLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'JLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'EFFDATE'
go
exec sp_addextendedproperty 'MS_Description', 'INVREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'INVREF'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDFEE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'ZMEDFEE'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'DTETRM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'DTETRM'
go
exec sp_addextendedproperty 'MS_Description', 'DESC_T', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'DESC_T'
go
exec sp_addextendedproperty 'MS_Description', 'MEMBER_NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'MEMBER_NAME'
go
exec sp_addextendedproperty 'MS_Description', 'LIFCNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR559DATA', 'COLUMN',
                            'LIFCNUM'
go

