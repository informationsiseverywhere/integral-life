IF EXISTS ( SELECT * FROM sys.tables where name = 'BR644TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR644TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR644DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR644DATA](
	[BATCHID] [bigint] NULL,
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[PAIDBY] [nchar](1) NULL,
	[EXMCODE] [nchar](10) NULL,
	[ZMEDTYP] [nchar](8) NULL,
	[EFFDATE] [int] NULL,
	[INVREF] [nchar](15) NULL,
	[ZMEDFEE] [numeric](17, 2) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[CLNTNUM] [nchar](10) NULL,
	[DATE1] [int] NULL,
	[DATE2] [int] NULL,
	[CLTSTAT] [nvarchar](2) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR644DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA'

go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'CHDRCOY'							
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'SEQNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'SEQNO'						
go
exec sp_addextendedproperty 'MS_Description', 'PAIDBY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'PAIDBY'						
go
exec sp_addextendedproperty 'MS_Description', 'EXMCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'EXMCODE'
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'ZMEDTYP'
go
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'EFFDATE'
go
exec sp_addextendedproperty 'MS_Description', 'INVREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'INVREF'						
go
exec sp_addextendedproperty 'MS_Description', 'ZMEDFEE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'ZMEDFEE'													
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'JLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'JLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'CLNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'CLNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'DATE1', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'DATE1'
go
exec sp_addextendedproperty 'MS_Description', 'DATE2', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'DATE2'
go
exec sp_addextendedproperty 'MS_Description', 'CLTSTAT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR644DATA', 'COLUMN',
                            'CLTSTAT'

go

