IF EXISTS ( SELECT * FROM sys.tables where name = 'FLUPPFTEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[FLUPPFTEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'FLUPPFDATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[FLUPPFDATA](
	[BATCHID] [numeric](18, 0) NULL,
	[UNIQUE_NUMBER] [numeric](18, 0) NULL,
	[CHDRCOY] [nvarchar](1) NULL,
	[CHDRNUM] [nvarchar](8) NULL,
	[FUPNO] [numeric](2, 0) NULL,
	[TRANNO] [numeric](5, 0) NULL,
	[FUPTYP] [nvarchar](1) NULL,
	[LIFE] [nvarchar](2) NULL,
	[JLIFE] [nvarchar](2) NULL,
	[FUPCDE] [nvarchar](3) NULL,
	[FUPSTS] [nvarchar](1) NULL,
	[FUPDT] [numeric](8, 0) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR FLUPPFDATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA'

go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'CHDRCOY'							
go
exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'CHDRNUM'
go
exec sp_addextendedproperty 'MS_Description', 'FUPNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'FUPNO'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'FUPTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'FUPTYP'
go
exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'LIFE'
go
exec sp_addextendedproperty 'MS_Description', 'JLIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'JLIFE'
go
exec sp_addextendedproperty 'MS_Description', 'FUPCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'FUPCDE'
go
exec sp_addextendedproperty 'MS_Description', 'FUPSTS', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'FUPSTS'
go
exec sp_addextendedproperty 'MS_Description', 'FUPDT', 'SCHEMA', 'VM1DTA', 'TABLE', 'FLUPPFDATA', 'COLUMN',
                            'FUPDT'
go							