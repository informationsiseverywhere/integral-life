﻿IF EXISTS ( SELECT * FROM sys.tables where name = 'BR608TEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[BR608TEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BR608DATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[BR608DATA](
	[COMPANY] [nchar](1) NULL,
	[VRTFUND] [nchar](4) NULL,
	[UNITYP] [nchar](1) NULL,
	[NOFUNTS] [numeric](16, 5) NULL
) ON [PRIMARY]

GO
exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BR608DATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR608DATA'
go
exec sp_addextendedproperty 'MS_Description', 'COMPANY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR608DATA', 'COLUMN',
                            'COMPANY'
go
exec sp_addextendedproperty 'MS_Description', 'VRTFUND', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR608DATA', 'COLUMN',
                            'VRTFUND'
go
exec sp_addextendedproperty 'MS_Description', 'UNITYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR608DATA', 'COLUMN',
                            'UNITYP'							
go
exec sp_addextendedproperty 'MS_Description', 'NOFUNTS', 'SCHEMA', 'VM1DTA', 'TABLE', 'BR608DATA', 'COLUMN',
                            'NOFUNTS'
go

