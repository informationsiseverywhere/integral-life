IF EXISTS ( SELECT * FROM sys.tables where name = 'AGPYPFTEMP' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE VM1DTA.[AGPYPFTEMP]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'AGPYPFDATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))


CREATE TABLE [VM1DTA].[AGPYPFDATA](
	[UNIQUE_NUMBER] [numeric](18, 0) NULL,
	[BATCHID] [numeric](10, 0) NULL,
	[AGNTPFX] [varchar](2) NULL,
	[AGNTCOY] [varchar](1) NULL,
	[AGNTNUM] [varchar](8) NULL,
	[AGNTBR] [varchar](2) NULL,
	[ARACDE] [varchar](3) NULL,
	[BATCPFX] [varchar](2) NULL,
	[BATCCOY] [varchar](1) NULL,
	[BATCBRN] [varchar](2) NULL,
	[BATCACTYR] [numeric](4, 0) NULL,
	[BATCACTMN] [numeric](2, 0) NULL,
	[BATCTRCDE] [varchar](4) NULL,
	[BATCBATCH] [varchar](5) NULL,
	[PAYPFX] [varchar](2) NULL,
	[PAYCOY] [varchar](1) NULL,
	[BANKKEY] [varchar](10) NULL,
	[BANKACCKEY] [varchar](20) NULL,
	[REQNPFX] [varchar](2) NULL,
	[REQNCOY] [varchar](1) NULL,
	[REQNBCDE] [varchar](2) NULL,
	[REQNNO] [varchar](9) NULL,
	[TRANAMT] [numeric](17, 2) NULL,
	[SACSCODE] [varchar](2) NULL,
	[SACSTYP] [varchar](2) NULL,
	[RDOCNUM] [varchar](9) NULL,
	[TRANNO] [numeric](5, 0) NULL,
	[JRNSEQ] [numeric](3, 0) NULL,
	[ORIGAMT] [numeric](17, 2) NULL,
	[TRANREF] [varchar](30) NULL,
	[TRANDESC] [varchar](30) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[ACCTAMT] [numeric](17, 2) NULL,
	[GENLCOY] [varchar](1) NULL,
	[GENLCUR] [varchar](3) NULL,
	[GLCODE] [varchar](14) NULL,
	[GLSIGN] [varchar](1) NULL,
	[POSTYEAR] [varchar](4) NULL,
	[POSTMONTH] [varchar](2) NULL,
	[EFFDATE] [numeric](8, 0) NULL,
	[RCAMT] [numeric](17, 2) NULL,
	[FRCDATE] [numeric](8, 0) NULL,
	[TRDT] [numeric](6, 0) NULL,
	[TERMID] [varchar](4) NULL,
	[RLDGCOY] [varchar](1) NULL,
	[RLDGACCT] [varchar](16) NULL,
	[ORIGCURR] [varchar](3) NULL,
	[SUPRFLG] [varchar](1) NULL,
	[TRTM] [numeric](6, 0) NULL,
	[JOBNM] [varchar](10) NULL
) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR AGPYPFDATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA'
go
exec sp_addextendedproperty 'MS_Description', 'UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'BATCHID', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCHID'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'AGNTPFX'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'AGNTCOY'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'AGNTNUM'
go
exec sp_addextendedproperty 'MS_Description', 'AGNTBR', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'AGNTBR'
go
exec sp_addextendedproperty 'MS_Description', 'ARACDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'ARACDE'
go
exec sp_addextendedproperty 'MS_Description', 'BATCPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCPFX'
go
exec sp_addextendedproperty 'MS_Description', 'BATCCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCCOY'
go
exec sp_addextendedproperty 'MS_Description', 'BATCBRN', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCBRN'
go
exec sp_addextendedproperty 'MS_Description', 'BATCACTYR', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCACTYR'
go
exec sp_addextendedproperty 'MS_Description', 'BATCACTMN', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCACTMN'
go
exec sp_addextendedproperty 'MS_Description', 'BATCTRCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCTRCDE'
go
exec sp_addextendedproperty 'MS_Description', 'BATCBATCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BATCBATCH'
go
exec sp_addextendedproperty 'MS_Description', 'PAYPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'PAYPFX'
go
exec sp_addextendedproperty 'MS_Description', 'PAYCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'PAYCOY'
go
exec sp_addextendedproperty 'MS_Description', 'BANKKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BANKKEY'
go
exec sp_addextendedproperty 'MS_Description', 'BANKACCKEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'BANKACCKEY'
go
exec sp_addextendedproperty 'MS_Description', 'REQNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'REQNPFX'
go
exec sp_addextendedproperty 'MS_Description', 'REQNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'REQNCOY'
go
exec sp_addextendedproperty 'MS_Description', 'REQNBCDE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'REQNBCDE'
go
exec sp_addextendedproperty 'MS_Description', 'REQNNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'REQNNO'
go
exec sp_addextendedproperty 'MS_Description', 'TRANAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRANAMT'
go
exec sp_addextendedproperty 'MS_Description', 'SACSCODE' , 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'SACSCODE'
go
exec sp_addextendedproperty 'MS_Description', 'SACSTYP', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'SACSTYP'
go
exec sp_addextendedproperty 'MS_Description', 'RDOCNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'RDOCNUM'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'JRNSEQ', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'JRNSEQ'
go
exec sp_addextendedproperty 'MS_Description', 'ORIGAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'ORIGAMT'
go	
exec sp_addextendedproperty 'MS_Description', 'TRANREF', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRANREF'
go	
exec sp_addextendedproperty 'MS_Description', 'TRANDESC', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRANDESC'
go 
exec sp_addextendedproperty 'MS_Description', 'CRATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'CRATE'
go 
exec sp_addextendedproperty 'MS_Description', 'ACCTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'ACCTAMT'
go 
exec sp_addextendedproperty 'MS_Description', 'GENLCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'GENLCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'GENLCUR', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'GENLCUR'
go 
exec sp_addextendedproperty 'MS_Description', 'GLCODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'GLCODE'
go 
exec sp_addextendedproperty 'MS_Description', 'GLSIGN', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'GLSIGN'						
go 
exec sp_addextendedproperty 'MS_Description', 'POSTYEAR', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'POSTYEAR'							
go
exec sp_addextendedproperty 'MS_Description', 'POSTMONTH', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'POSTMONTH'							
go	
exec sp_addextendedproperty 'MS_Description', 'EFFDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'EFFDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'RCAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'RCAMT'
go 
exec sp_addextendedproperty 'MS_Description', 'FRCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'FRCDATE'
go 
exec sp_addextendedproperty 'MS_Description', 'TRDT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRDT'
go 
exec sp_addextendedproperty 'MS_Description', 'TERMID', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TERMID'
go 
exec sp_addextendedproperty 'MS_Description', 'RLDGCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'RLDGCOY'
go
exec sp_addextendedproperty 'MS_Description', 'RLDGACCT', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'RLDGACCT'
go 
exec sp_addextendedproperty 'MS_Description', 'ORIGCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'ORIGCURR'
go 
exec sp_addextendedproperty 'MS_Description', 'SUPRFLG', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'SUPRFLG'
go 
exec sp_addextendedproperty 'MS_Description', 'TRTM', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'TRTM'							
go   
exec sp_addextendedproperty 'MS_Description', 'JOBNM', 'SCHEMA', 'VM1DTA', 'TABLE', 'AGPYPFDATA', 'COLUMN',
                            'JOBNM'							
go



