

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BD5LWDATA' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
				
CREATE TABLE [VM1DTA].[BD5LWDATA]
  (
	[RLDGCOY] [nchar](1), 
	[RLDGACCT] [nchar](16), 
	[SACSCODE] [nchar](2), 
	[SACSTYP] [nchar](2), 
	[SACSCURBAL] [numeric](17, 2), 
	[CHDRCOY] [nchar](1), 
	[CHDRNUM] [nchar](8), 
	[CNTBRANCH] [nchar](2), 
	[CNTTYPE] [nchar](3), 
	[STATCODE] [nchar](2), 
	[OCCDATE] [int], 
	[PTDATE] [int], 
	[POLSUM] [int], 
	[CNTCURR] [nchar](3) )

GO

exec sp_addextendedproperty 'MS_Description', 'BD5LW DATA TABLE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA'
go

exec sp_addextendedproperty 'MS_Description', 'SUB LEDGER COMPANY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'RLDGCOY'
go

exec sp_addextendedproperty 'MS_Description', 'SUB LEDGER ACCOUNT ENTITY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA',
                            'COLUMN', 'RLDGACCT'
go

exec sp_addextendedproperty 'MS_Description', 'SUB-ACCOUNT CODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'SACSCODE'
go

exec sp_addextendedproperty 'MS_Description', 'SUB-ACCOUNT TYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'SACSTYP'
go

exec sp_addextendedproperty 'MS_Description', 'CURRENT BALANCE AMOUNT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA',
                            'COLUMN', 'SACSCURBAL'
go

exec sp_addextendedproperty 'MS_Description', 'CONTRACT HEADER COMPANY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA',
                            'COLUMN', 'CHDRCOY'
go

exec sp_addextendedproperty 'MS_Description', 'CONTRACT NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'CHDRNUM'
go

exec sp_addextendedproperty 'MS_Description', 'BRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN', 'CNTBRANCH'
go

exec sp_addextendedproperty 'MS_Description', 'CONTRACT TYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'CNTTYPE'
go

exec sp_addextendedproperty 'MS_Description', 'STATUS CODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'STATCODE'
go

exec sp_addextendedproperty 'MS_Description', 'ORIGINAL COMMENCEMENT DATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA',
                            'COLUMN', 'OCCDATE'
go

exec sp_addextendedproperty 'MS_Description', 'PAID TO DATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN',
                            'PTDATE'
go

exec sp_addextendedproperty 'MS_Description', 'POLICIES SUMMARISED CURRENTLY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA',
                            'COLUMN', 'POLSUM'
go

exec sp_addextendedproperty 'MS_Description', 'CURRENCY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5LWDATA', 'COLUMN', 'CNTCURR'
go