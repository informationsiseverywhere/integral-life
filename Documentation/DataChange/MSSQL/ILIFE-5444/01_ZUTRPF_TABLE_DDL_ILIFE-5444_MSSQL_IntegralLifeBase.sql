IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'ZUTRPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[ZUTRPF]( 
	   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
       [CHDRPFX] [nchar](2) NULL,
       [CHDRCOY] [nchar](1) NULL,
       [CHDRNUM] [nchar](8) NULL,
	   [LIFE] [nchar](2) NULL,
	   [COVERAGE] [nchar](2) NULL,
	   [RIDER] [nchar](2) NULL,
	   [PLNSFX] [int] NULL,
	   [TRANNO] [int] NULL,
	   [RSUNIN] [nchar](1) NULL,
	   [RUNDTE] [int] NULL,
	   [BATCTRCDE] [nchar](4) NULL,
	   [TRDT] [int] NULL,
	   [TRTM] [int] NULL,
	   [USER_T] [int] NULL,
	   [EFFDATE] [int] NULL,
	   [VALIDFLAG] [nchar](1) NULL,
	   [DATESUB] [int] NULL,
	   [CRTUSER] [nchar](10) NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar](10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   )   
		
GO