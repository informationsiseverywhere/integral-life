	
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('INTERESTRATE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE [VM1DTA].[CLMHPF] ADD INTERESTRATE NUMERIC(8,5);
EXEC sys.sp_addextendedproperty 
@name=N'MS_Description', 
@value=N'<INTEREST RATE>' ,
 @level0type=N'SCHEMA',@level0name=N'VM1DTA',
 @level1type=N'TABLE',@level1name=N'CLMHPF',
 @level2type=N'COLUMN',@level2name=N'INTERESTRATE';
END
GO
