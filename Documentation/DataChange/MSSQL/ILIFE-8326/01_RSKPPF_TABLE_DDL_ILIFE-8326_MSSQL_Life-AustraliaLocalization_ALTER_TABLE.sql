-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'POLFEE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RSKPPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[RSKPPF] ADD POLFEE NUMERIC(17,2) NOT NULL DEFAULT '0';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<co-contribution added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RSKPPF', @level2type=N'COLUMN',@level2name=N'POLFEE ';
	END
GO
SET QUOTED_IDENTIFIER OFF  
GO

