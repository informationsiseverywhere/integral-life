
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.tables where name = 'SLCKPF' and type='U' and schema_id=(select schema_id from sys.schemas where name='VM1DTA'))
	DELETE FROM VM1DTA.SLCKPF WHERE (COMPANY='2' OR COMPANY='9') and datediff(dd,datime,getdate())>1 
GO

SET QUOTED_IDENTIFIER OFF
GO

