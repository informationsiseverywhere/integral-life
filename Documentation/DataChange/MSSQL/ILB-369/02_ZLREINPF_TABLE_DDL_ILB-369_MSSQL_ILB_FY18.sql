IF EXISTS ( SELECT * FROM sys.tables where name = 'ZLREINPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE [ZLREINPF]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'ZLREINPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
 

CREATE TABLE [VM1DTA].[ZLREINPF](
       [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
       [CHDRCOY] [nchar](1) NULL,
       [CHDRNUM] [nchar](8) NULL,
       [CNTTYPE] [nchar](3) NULL,
       [OWNNUM] [nchar](8) NULL,
       [RDOCNUM] [nchar](9) NULL,
       [EFFDATE] [int] NULL,
       [ORIGAMT] [numeric](17, 2) NULL,
       [RCPTTYPE] [nchar](1) NULL,	   
	   [ERORCDE] [nchar](6) NULL, 	   
       [USRPRF] [nchar](10) NULL,
       [JOBNM] [nchar](10) NULL,
       [DATIME] [datetime2](6) NULL,
CONSTRAINT [PK_ZLREINPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



GO

