if not exists (select * from syscolumns where id=object_id('VM1DTA.BR539Temp') and name='CNT') 
	ALTER TABLE VM1DTA.BR539Temp  add CNT int;
GO



EXEC sp_addextendedproperty 
@name = N'MS_Description', @value = 'CONTRACT COUNT',
@level0type = N'Schema', @level0name =VM1DTA, 
@level1type = N'Table',  @level1name =BR539Temp, 
@level2type = N'Column', @level2name =CNT; 