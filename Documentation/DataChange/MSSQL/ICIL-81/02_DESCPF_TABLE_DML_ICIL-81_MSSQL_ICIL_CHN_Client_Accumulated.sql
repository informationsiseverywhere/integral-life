
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='T1660' AND d.DESCITEM='CS' AND d.LANGUAGE='E') 

Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1660','CS','  ','E','','Risk Amnt','Client Accumulated Risk Amount','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1660','CS','  ','S','', N'保额',N'客户累计保额','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO



GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='T1661' AND d.DESCITEM='D5G5X' AND d.LANGUAGE='E') 

Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','D5G5X','  ','E','','  ','Switching for clnt acc rsk amt','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','D5G5X','  ','S','',' ',N'根据风险为核保客户转换                   ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

GO