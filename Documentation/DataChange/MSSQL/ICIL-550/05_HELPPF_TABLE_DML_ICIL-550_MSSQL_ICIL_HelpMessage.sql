
delete from VM1DTA.HELPPF where HELPITEM = 'REINSTPERD' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='SD5J1';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','REINSTPERD',1,'1','This is the period in months between the Last Paid to date','UNDERWR1  ','UNDERWR1',current_timestamp);
	 
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','REINSTPERD',2,'1','and the reinstatement effective date.','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'MEDICALCHK' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='SD5J1';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','MEDICALCHK',1,'1','TThis is the period in days between the Last Paid to date and','UNDERWR1  ','UNDERWR1',current_timestamp);
	 
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','MEDICALCHK',2,'1','the reinstatement effective date after which customer must','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','MEDICALCHK',3,'1','go for medical checks before reinstatement.','UNDERWR1  ','UNDERWR1',current_timestamp);

delete from VM1DTA.HELPPF where HELPITEM = 'SUBROUTINE' and HELPLANG = 'E' and HELPTYPE = 'F' AND HELPPROG='SD5J1';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','SUBROUTINE',1,'1','This is the subroutine which calculates the total','UNDERWR1  ','UNDERWR1',current_timestamp);
	 
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','SD5J1','SUBROUTINE',2,'1','outstanding premium due in the contract.','UNDERWR1  ','UNDERWR1',current_timestamp);



























