IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'LIFE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))	
BEGIN
ALTER TABLE [VM1DTA].[RLVRPF] ADD LIFE NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<life added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'LIFE ';
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COVERAGE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))	
BEGIN
ALTER TABLE [VM1DTA].[RLVRPF] ADD COVERAGE NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<coverage  added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'COVERAGE ';
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RIDER' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))	
ALTER TABLE [VM1DTA].[RLVRPF] ADD RIDER NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<rider added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'RIDER ';
END


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'VALIDFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))	
BEGIN
ALTER TABLE [VM1DTA].[RLVRPF] ADD VALIDFLAG NCHAR(1);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<validflag added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG ';
END
