
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'EXCLPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[EXCLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CRTABLE] [nchar](8) NULL,
	[SEQNBR] [int] NULL,
	[EXCDA] [nchar](8) NULL,
	[EFFDATE] [int] NULL,
	[SUBSEQNBR] [int] NULL,
	[EXADTXT] [nchar](1500) NULL,
	[PRNTSTAT] [nchar](15) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_EXCLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

