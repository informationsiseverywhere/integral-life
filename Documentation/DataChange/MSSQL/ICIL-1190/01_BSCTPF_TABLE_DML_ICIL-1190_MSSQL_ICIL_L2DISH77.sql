
IF NOT EXISTS (	SELECT * FROM VM1DTA.BSCTPF 	
	WHERE BSCHEDNAM = 'L2DISH77' AND LANGUAGE='E')
	INSERT INTO VM1DTA.BSCTPF ([LANGUAGE], [BSCHEDNAM], [DESC_T], [JOBNM], [USRPRF], [DATIME]) VALUES ('E','L2DISH77','Batch Dishonour for Factoring House 77','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BSCTPF 	
	WHERE BSCHEDNAM = 'L2DISH77' AND LANGUAGE='S')
	INSERT INTO VM1DTA.BSCTPF ([LANGUAGE], [BSCHEDNAM], [DESC_T], [JOBNM], [USRPRF], [DATIME]) VALUES ('S','L2DISH77','结算中心77的批拒付','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO