IF NOT EXISTS (SELECT * FROM sys.tables where name = 'CPBNFYPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[CPBNFYPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[BNYCLT] [nchar](8) NOT NULL,
	[TRANNO]   INT NULL,   
	[VALIDFLAG] [nchar](1) NOT NULL,
	[CURRFR]   INT NOT NULL, 
	[CURRTO]   INT NOT NULL, 
	[BNYRLN] [nchar](2) NULL,
	[BNYCD] [nchar](1) NULL,
	[BNYPC] [numeric](5, 2) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T]   [numeric](6) NULL,   
	[TRTM]   INT NULL,   
	[TRDT]   INT NULL,   
	[EFFDATE]  INT NULL,   
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NOT NULL,
	[BNYTYPE] [nchar](2) NULL,    
	[CLTRELN] [nchar](4) NULL,   
	[RELTO] [nchar](1) NULL,
	[SELFIND] [nchar](1) NULL,
	[REVCFLG] [nchar](1) NULL,
	[ENDDATE]  INT NULL, 
	[SEQUENCE] [nchar](2) NULL,
	[PAYMMETH] [nchar](1) NULL,
    [BANKKEY] [nchar](10) NULL,    
	[BANKACCKEY] [nchar](20) NULL,
	[AMOUNT] [numeric](17, 2) NULL,
CONSTRAINT [PK_CPBNFYPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
) ON [PRIMARY] 
GO


exec sp_addextendedproperty 'MS_Description', 'CLAIM PAYEES BENEFICIARY PHYSI', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF'
go

exec sp_addextendedproperty 'MS_Description', 'CONT HEADER COMPANY', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'CHDRCOY'
go

exec sp_addextendedproperty 'MS_Description', 'CONTRACT NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'CHDRNUM'
go

exec sp_addextendedproperty 'MS_Description', 'BENEFICIARY CLIENT NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF',
                            'COLUMN', 'BNYCLT'
go

exec sp_addextendedproperty 'MS_Description', 'TRANSACTION NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'TRANNO'
go

exec sp_addextendedproperty 'MS_Description', 'VALID FLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'VALIDFLAG'
go

exec sp_addextendedproperty 'MS_Description', 'CURRENT FROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'CURRFR'
go

exec sp_addextendedproperty 'MS_Description', 'CURRENT TO', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'CURRTO'
go

exec sp_addextendedproperty 'MS_Description', 'BENEFICIARY RELATION', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'BNYRLN'
go

exec sp_addextendedproperty 'MS_Description', 'BENEFICIARY CODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'BNYCD'
go

exec sp_addextendedproperty 'MS_Description', 'BENEFICIARY PERCENTAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF',
                            'COLUMN', 'BNYPC'
go

exec sp_addextendedproperty 'MS_Description', 'TERMINAL ID', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'TERMID'
go

exec sp_addextendedproperty 'MS_Description', 'USER NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'USER_T'
go

exec sp_addextendedproperty 'MS_Description', 'TRANSACTION TIME', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'TRTM'
go

exec sp_addextendedproperty 'MS_Description', 'TRANSACTION DATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'TRDT'
go

exec sp_addextendedproperty 'MS_Description', 'EFFECTIVE DATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'EFFDATE'
go

exec sp_addextendedproperty 'MS_Description', 'USER PROFILE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'USRPRF'
go

exec sp_addextendedproperty 'MS_Description', 'JOB NAME', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'JOBNM'
go

exec sp_addextendedproperty 'MS_Description', 'DATE TIME', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'DATIME'
go

exec sp_addextendedproperty 'MS_Description', 'BENEFICIARY TYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'BNYTYPE'
go

exec sp_addextendedproperty 'MS_Description', 'CLIENT RELATIONSHIP CODE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF',
                            'COLUMN', 'CLTRELN'
go

exec sp_addextendedproperty 'MS_Description', 'RELATED TO', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'RELTO'
go

exec sp_addextendedproperty 'MS_Description', 'SELFIND', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'SELFIND'
go

exec sp_addextendedproperty 'MS_Description', 'RECEIVE FLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'REVCFLG'
go

exec sp_addextendedproperty 'MS_Description', 'END DATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'ENDDATE'
go

exec sp_addextendedproperty 'MS_Description', 'SEQUENCE', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'SEQUENCE'
go

exec sp_addextendedproperty 'MS_Description', 'PAY METHOD', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'PAYMMETH'
go

exec sp_addextendedproperty 'MS_Description', 'BANK KEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'BANKKEY'
go

exec sp_addextendedproperty 'MS_Description', 'BANK ACCOUNT KEY', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN',
                            'BANKACCKEY'
go

exec sp_addextendedproperty 'MS_Description', 'AMOUNT', 'SCHEMA', 'VM1DTA', 'TABLE', 'CPBNFYPF', 'COLUMN', 'AMOUNT'
go
