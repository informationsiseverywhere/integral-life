IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'RLVRPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[RLVRPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[MEMACCNUM] [nchar](20) NULL,
	[FNDUSINUM] [NCHAR](11) NULL,
	[SRVSTRTDT] [int] NULL,
	[TAXFREEAMT] [numeric](17, 2) NULL,
	[KIWISVRCOM] [numeric](17, 2) NULL,
	[TAXEDAMT] [numeric](17, 2) NULL,
	[UNTAXEDAMT] [numeric](17, 2) NULL,
	[PRSRVDAMT] [numeric](17, 2) NULL,
	[KIWIAMT] [numeric](17, 2) NULL,
	[UNRESTRICT] [numeric](17, 2) NULL,
	[RSTRICT] [numeric](17, 2) NULL,
	[TOTTRFAMT] [numeric](17, 2) NULL,
	[MONEYIND] [nchar](1) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](7) NULL,
 CONSTRAINT [PK_RLVRPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




	
