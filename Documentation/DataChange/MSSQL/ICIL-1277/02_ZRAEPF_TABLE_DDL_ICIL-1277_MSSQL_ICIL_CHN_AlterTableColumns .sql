

IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'APLSTCAPDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))         
					ALTER TABLE VM1DTA.ZRAEPF ALTER COLUMN APLSTCAPDATE int;
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'APNXTCAPDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))         
                 ALTER TABLE VM1DTA.ZRAEPF ALTER COLUMN APNXTCAPDATE int;				
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'APLSTINTBDTE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))         
                  ALTER TABLE VM1DTA.ZRAEPF ALTER COLUMN APLSTINTBDTE int;			
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'APNXTINTBDTE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZRAEPF'))         
                 ALTER TABLE VM1DTA.ZRAEPF ALTER COLUMN APNXTINTBDTE int;