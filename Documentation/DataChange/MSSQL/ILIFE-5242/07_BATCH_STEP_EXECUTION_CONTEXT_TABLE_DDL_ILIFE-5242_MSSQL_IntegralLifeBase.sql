IF NOT EXISTS (SELECT * FROM sys.tables where name = 'BATCH_STEP_EXECUTION_CONTEXT' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[BATCH_STEP_EXECUTION_CONTEXT](
[STEP_EXECUTION_ID] [BIGINT] NOT NULL PRIMARY KEY,
[SHORT_CONTEXT] [VARCHAR](2500) NOT NULL,
[SERIALIZED_CONTEXT] [TEXT] NULL,
constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
) ;
GO