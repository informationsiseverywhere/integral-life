

IF EXISTS ( SELECT * FROM sys.tables where name = 'LBONPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE [LBONPF]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'LBONPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
 

CREATE TABLE [VM1DTA].[LBONPF](
       [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
       [CHDRCOY] [varchar](1) NULL,
       [CHDRNUM] [varchar](8) NULL,
       [LIFE] [varchar](2) NULL,
       [COVERAGE] [varchar](2) NULL,
       [RIDER] [varchar](2) NULL,
       [PLANSFX] [varchar](4) NULL,
       [EFFDATE] [int] NULL,
       [TRANNO] [int] NULL,
       [TRCDE] [varchar](4) NULL,
       [TRNAMT] [int] NULL,
       [NETPRE] [int] NULL,
       [UEXTPC] [int] NULL,
       [UALPRC] [int] NULL,
       [USRPRF] [varchar](10) NULL,
       [JOBNM] [varchar](10) NULL,
       [DATIME] [datetime2](6) NULL,
CONSTRAINT [PK_LBONPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



GO

