GO
/****** Object:  StoredProcedure [VM1DTA].[AUTOBATCF]    Script Date: 10/10/2018 1:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE[VM1DTA].[AUTOBATCF]
(@BPFX VARCHAR(2),@BCOY VARCHAR(1),@BBRN VARCHAR(2),@BACTYR INT,@BACTMN INT,@BTRCDE VARCHAR(4),@USRPRF VARCHAR(10),@JOBNM VARCHAR(10),@BACBCH INT OUTPUT)
AS
BEGIN TRANSACTION;
	SET NOCOUNT ON;
	DECLARE @UNIQ bigint;
	SELECT @UNIQ = unique_number,@BACBCH= batcbatch from VM1DTA.BATCFPF WITH (UPDLOCK)  where BATCCOY=@BCOY AND BATCPFX =@BPFX AND BATCACTMN= @BACTMN AND BATCACTYR=@BACTYR AND BATCTRCDE=@BTRCDE AND BATCBRN=@BBRN ;
	IF(@BACBCH is null)
		BEGIN
			SELECT @UNIQ = unique_number,@BACBCH= batcbatch from VM1DTA.BATCFPF WITH (ROWLOCK,HOLDLOCK,UPDLOCK)  where BATCCOY=@BCOY AND BATCPFX =@BPFX AND BATCACTMN= @BACTMN AND BATCACTYR=@BACTYR AND BATCTRCDE=@BTRCDE AND BATCBRN=@BBRN ;
			IF(@BACBCH is null)
				BEGIN
					SET @BACBCH = 1;
					INSERT INTO VM1DTA.BATCFPF (BATCPFX,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,USRPRF,JOBNM,DATIME)
					VALUES(@BPFX,@BCOY,@BBRN,@BACTYR,@BACTMN,@BTRCDE,@BACBCH,@USRPRF,@JOBNM,CURRENT_TIMESTAMP);	
				END;
			ELSE
				BEGIN
					set @BACBCH=@BACBCH+1 ;	
					UPDATE VM1DTA.BATCFPF SET BATCBATCH=@BACBCH WHERE UNIQUE_NUMBER=@UNIQ;	
			END;	

	END;
	ELSE
		BEGIN
			set @BACBCH=@BACBCH+1 ;	
			UPDATE VM1DTA.BATCFPF SET BATCBATCH=@BACBCH WHERE UNIQUE_NUMBER=@UNIQ;	
		END;		
COMMIT TRANSACTION;

