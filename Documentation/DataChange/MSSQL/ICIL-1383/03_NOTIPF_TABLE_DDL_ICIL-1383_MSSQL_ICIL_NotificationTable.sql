
IF EXISTS (SELECT * FROM sys.columns WHERE name = 'NOTIFINUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
		ALTER TABLE [VM1DTA].[NOTIPF] ALTER COLUMN NOTIFINUM CHAR(8) null
	GO
										
										
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CHDRNUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
		BEGIN
		ALTER TABLE [VM1DTA].[NOTIPF] ADD CHDRNUM char(8) null ;
              EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<chdrnum>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOTIPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM ';
		END
GO										

										
