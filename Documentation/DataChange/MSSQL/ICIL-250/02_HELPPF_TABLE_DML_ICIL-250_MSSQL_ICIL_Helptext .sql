
delete from VM1DTA.HELPPF where HELPITEM = 'MTHSCOMPAD' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPAD',1,'1','this is the number of months user can process               ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPAD',2,'1','the component add and effective date of change              ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPAD',3,'1','shall be the contract anniversary date.                     ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPAD',1,'1',N'用户可在此输入能够处理的','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPAD',2,'1',N'组成添加和生效日变更的月度数','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPAD',3,'1',N'以保单周年日为准。','UNDERWR1  ','UNDERWR1',current_timestamp);


delete from VM1DTA.HELPPF where HELPITEM = 'MTHSCOMPMO' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPMO',1,'1','this is the number of months user can process               ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPMO',2,'1','the component modify and effective date of change           ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','MTHSCOMPMO',3,'1','shall be the contract anniversary date.                     ','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPMO',1,'1',N'用户可在此输入能够处理的','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPMO',2,'1',N'组成修改和生效日变更的月度数','UNDERWR1  ','UNDERWR1',current_timestamp);
insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','S','F','          ','MTHSCOMPMO',3,'1',N'以保单周年日为准。','UNDERWR1  ','UNDERWR1',current_timestamp);
