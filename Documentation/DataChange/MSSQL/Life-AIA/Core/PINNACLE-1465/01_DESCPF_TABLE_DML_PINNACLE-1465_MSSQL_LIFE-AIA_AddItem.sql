SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM  VM1DTA.DESCPF WHERE  DESCTABL='TH5F9' AND DESCITEM='SCTYP9RA') 
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5F9','SCTYP9RA','  ','E','05kn 
\G 52 j','SCTYP9RA  ','Field-SJITRNCDE9, SASCTYPE-RA ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP)
GO
SET QUOTED_IDENTIFIER OFF
GO