
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'GCRTPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

CREATE TABLE [VM1DTA].[GCRTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRNUM] [nchar](8) NULL,
	[VIND] [nchar](2) NULL,
	[AMOUNT]  [numeric](17, 2) NULL,
	[DUPLDSR] [nchar](8) NULL,
	[REASON] [nchar](8) NULL,
	[THREADNO] [int] NULL,
	[JOBNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[CREATED_AT] [datetime2](6) NULL,
 CONSTRAINT [PK_GCRTPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

SET QUOTED_IDENTIFIER OFF
GO