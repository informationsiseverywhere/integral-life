
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PENDCSTTO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RACDPF'))	
ALTER TABLE [VM1DTA].[RACDPF] ADD PENDCSTTO int;

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORATEFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RACDPF'))	
ALTER TABLE [VM1DTA].[RACDPF] ADD PRORATEFLAG nchar(1);

GO
