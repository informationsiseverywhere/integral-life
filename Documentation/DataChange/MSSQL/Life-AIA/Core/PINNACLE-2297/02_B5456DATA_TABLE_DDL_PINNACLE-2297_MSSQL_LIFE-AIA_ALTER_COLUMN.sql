
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PENDCSTTO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA'))	
ALTER TABLE [VM1DTA].[B5456DATA] ADD PENDCSTTO int;

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORATEFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5456DATA'))	
ALTER TABLE [VM1DTA].[B5456DATA] ADD PRORATEFLAG nchar(1);

GO
