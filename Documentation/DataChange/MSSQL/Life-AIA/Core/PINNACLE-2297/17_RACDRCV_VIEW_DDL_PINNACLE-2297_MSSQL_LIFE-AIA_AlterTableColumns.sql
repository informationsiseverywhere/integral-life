
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[RACDRCV] AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            RASNUM,
            TRANNO,
            SEQNO,
            VALIDFLAG,
            CURRCODE,
            CURRFROM,
            CURRTO,
            RETYPE,
            RNGMNT,
            RAAMOUNT,
            CTDATE,
            CMDATE,
            REASPER,
            RREVDT,
            RECOVAMT,
            CESTYPE,
            OVRDIND,
            LRKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			RRTDAT,
			RRTFRM,
			PENDCSTTO,
            PRORATEFLAG			
       FROM VM1DTA.RACDPF
      WHERE VALIDFLAG = '4' AND RECOVAMT <> 0
GO



