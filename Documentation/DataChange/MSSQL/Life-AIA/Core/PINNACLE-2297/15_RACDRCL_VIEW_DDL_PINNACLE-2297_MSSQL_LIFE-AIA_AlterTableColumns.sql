
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[RACDRCL] AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            RASNUM,
            TRANNO,
            SEQNO,
            VALIDFLAG,
            CURRCODE,
            CURRFROM,
            CURRTO,
            RETYPE,
            RNGMNT,
            RAAMOUNT,
            CTDATE,
            CMDATE,
            REASPER,
            RREVDT,
            RECOVAMT,
            CESTYPE,
            OVRDIND,
            LRKCLS,
            JOBNM,
            USRPRF,
            DATIME,
			RRTDAT,
			RRTFRM,
			PENDCSTTO,
            PRORATEFLAG			
       FROM VM1DTA.RACDPF
      WHERE VALIDFLAG = '3'
GO



