SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM sys.views where name = 'TTRNGL' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].TTRNGL
GO
CREATE VIEW [VM1DTA].[TTRNGL] ( 
UNIQUE_NUMBER,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,EFFDATE,GENLCOY,GENLCUR,GLCODE,GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,ORIGAMT,ACCTAMT,RECORDFORMAT,NONKEY
) AS	
SELECT UNIQUE_NUMBER,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,EFFDATE,GENLCOY,GENLCUR,GLCODE,GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,ORIGAMT,ACCTAMT,RECORDFORMAT,NONKEY 
FROM (
	SELECT UNIQUE_NUMBER,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,EFFDATE,GENLCOY,GENLCUR,GLCODE,GLSIGN,ORIGCCY ORIGCURR,POSTMONTH,POSTYEAR,
      CASE WHEN GLSIGN = '-'
			THEN ORIGAMT * (-1) 
		     ELSE ORIGAMT END
		ORIGAMT,
		CASE WHEN GLSIGN = '-'
			THEN ACCTAMT * (-1) 
		     ELSE ACCTAMT END
    ACCTAMT,'RTRNREC' RECORDFORMAT, 'TRANDATE'+':'+ISNULL(CAST(TRANDATE as VARCHAR(8)),'')+','+'RDOCPFX'+':'+ISNULL(RDOCPFX,'')+','+'RDOCCOY'+':'+ISNULL(RDOCCOY,'')+','+'RLDGCOY'+':'+ISNULL(RLDGCOY,'')+','+'BANKCODE'+':'+ISNULL(BANKCODE,'')+','+'ACCTYP'+':'+ISNULL(ACCTYP,'')+','+'RDOCNUM'+':'+ISNULL(RDOCNUM,'')+','+'SACSCODE'+':'+ISNULL(SACSCODE,'') +','+'RLDGACCT'+':'+ISNULL(RLDGACCT,'')+','+'SACSTYP'+':'+ISNULL(SACSTYP,'') NONKEY FROM VM1DTA.RTRNPF UNION ALL
	SELECT UNIQUE_NUMBER,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,EFFDATE,GENLCOY,GENLCUR,GLCODE,GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,
      CASE WHEN GLSIGN = '-'
			THEN ORIGAMT * (-1) 
		     ELSE ORIGAMT 
		     END
		ORIGAMT,
		CASE WHEN GLSIGN = '-'
			THEN ACCTAMT * (-1)
		     ELSE ACCTAMT END
		ACCTAMT,'ACMVREC' RECORDFORMAT, 'RLDGACCT'+':'+ISNULL(RLDGACCT,'')+','+'RDOCPFX'+':'+ISNULL(RDOCPFX,'')+','+'RDOCCOY'+':'+ISNULL(RDOCCOY,'')+','+'RDOCNUM'+':'+ISNULL(RDOCNUM,'')+','+
		'TRANNO'+':'+ISNULL(CAST(TRANNO as VARCHAR(8)),'')+','+'RLDGPFX'+':'+ISNULL(RLDGPFX,'')+','+'RLDGCOY'+':'+ISNULL(RLDGCOY,'')+','+'SACSCODE'+':'+ISNULL(SACSCODE,'')+','+ 
		'SACSTYP'+':'+ISNULL(SACSTYP,'')+','+'CRATE'+':'+ISNULL(CAST(CRATE as VARCHAR(15)),'')+','+'TRANDATE'+':'+ISNULL(CAST(TRANDATE as VARCHAR(8)),'')+','+'TRANREF'+':'+ISNULL(TRANREF,'')
		NONKEY FROM VM1DTA.ACMVPF UNION ALL
	SELECT UNIQUE_NUMBER,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,EFFDATE,GENLCOY,ACCTCURR GENLCUR,GLCODE,GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,
		CASE WHEN GLSIGN = '-'
			THEN TRANAMT * (-1) 
		     ELSE TRANAMT 
		     END
		ORIGAMT,
		CASE WHEN GLSIGN = '-'
			THEN TRANAMT * (-1) * CRATE
		     ELSE TRANAMT * CRATE END
		ACCTAMT,'ZTRNREC' RECORDFORMAT, '' NONKEY
		FROM (
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE01 GLCODE,GLSIGN01 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT01 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE02 GLCODE,GLSIGN02 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT02 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE03 GLCODE,GLSIGN03 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT03 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE04 GLCODE,GLSIGN04 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT04 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE05 GLCODE,GLSIGN05 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT05 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE06 GLCODE,GLSIGN06 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT06 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE07 GLCODE,GLSIGN07 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT07 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE08 GLCODE,GLSIGN08 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT08 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE09 GLCODE,GLSIGN09 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT09 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE10 GLCODE,GLSIGN10 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT10 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE11 GLCODE,GLSIGN11 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT11 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE12 GLCODE,GLSIGN12 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT12 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE13 GLCODE,GLSIGN13 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT13 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE14 GLCODE,GLSIGN14 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT14 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF UNION ALL
			SELECT UNIQUE_NUMBER,RLDGACCT,ACCTCURR,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,CRATE,EFFDATE,GENLCOY,GLCODE15 GLCODE,GLSIGN15 GLSIGN,ORIGCURR,POSTMONTH,POSTYEAR,TRANAMT15 TRANAMT,TRANNO FROM VM1DTA.ZTRNPF 
		    ) ZTRN_GL
      ) TTRNGLF
GO

SET QUOTED_IDENTIFIER OFF
GO