SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM [VM1DTA].BPPDPF WHERE [BSCHEDNAM] = 'L2RETBNK97' and [COMPANY]='2') 
	INSERT [VM1DTA].[BPPDPF] ( [BSCHEDNAM], [BPARPSEQNO], [COMPANY], [BPARPPROG], [USRPRF], [JOBNM], [DATIME], [CREATED_AT]) VALUES ( N'L2RETBNK97', 1, N'2', N'', N'UNDERWR1  ', N'UNDERWR1  ',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)

SET QUOTED_IDENTIFIER OFF
GO