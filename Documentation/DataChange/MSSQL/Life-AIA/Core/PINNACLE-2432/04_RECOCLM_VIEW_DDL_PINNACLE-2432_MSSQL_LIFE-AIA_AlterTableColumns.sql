SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[RECOCLM] AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            RASNUM,
            SEQNO,
            VALIDFLAG,
            COSTDATE,
            RETYPE,
            RNGMNT,
			SRARAMT,
            RAAMOUNT,
            CTDATE,
			ORIGCURR,
			PREM,
			COMPAY,
			TAXAMT,
			REFUNDFE,
			BATCCOY,
			BATCBRN,
			BATCACTYR,
			BATCACTMN,
			BATCTRCDE,
			BATCBATCH,
			TRANNO,
			RCSTFRQ,
            USRPRF,
            JOBNM,
            DATIME,
			RPRATE
       FROM VM1DTA.RECOPF

GO
SET QUOTED_IDENTIFIER OFF  
GO
