SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM sys.views where name = 'RACDCSN' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].RACDCSN
GO



CREATE VIEW [VM1DTA].[RACDCSN](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, RASNUM, TRANNO, SEQNO, VALIDFLAG, CURRCODE, CURRFROM, CURRTO, RETYPE, RNGMNT, RAAMOUNT, CTDATE, CMDATE, REASPER, RREVDT, RECOVAMT, CESTYPE, OVRDIND, LRKCLS, JOBNM, USRPRF, DATIME,RRTDAT,RRTFRM) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            RASNUM,
            TRANNO,
            SEQNO,
            VALIDFLAG,
            CURRCODE,
            CURRFROM,
            CURRTO,
            RETYPE,
            RNGMNT,
            RAAMOUNT,
            CTDATE,
            CMDATE,
            REASPER,
            RREVDT,
            RECOVAMT,
            CESTYPE,
            OVRDIND,
            LRKCLS,
            JOBNM,
            USRPRF,
            DATIME,
			RRTDAT,
			RRTFRM


       FROM VM1DTA.RACDPF
       WHERE NOT (VALIDFLAG = '2' OR VALIDFLAG = '4')
GO
SET QUOTED_IDENTIFIER OFF