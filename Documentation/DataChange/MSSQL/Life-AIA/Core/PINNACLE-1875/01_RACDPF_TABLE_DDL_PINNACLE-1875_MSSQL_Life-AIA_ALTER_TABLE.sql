SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RRTDAT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RACDPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[RACDPF] ADD RRTDAT int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<co-contribution added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RACDPF', @level2type=N'COLUMN',@level2name=N'RRTDAT ';
	END
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RRTFRM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RACDPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[RACDPF] ADD RRTFRM int NOT NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<co-contribution added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RACDPF', @level2type=N'COLUMN',@level2name=N'RRTFRM ';
	END
GO
SET QUOTED_IDENTIFIER OFF 