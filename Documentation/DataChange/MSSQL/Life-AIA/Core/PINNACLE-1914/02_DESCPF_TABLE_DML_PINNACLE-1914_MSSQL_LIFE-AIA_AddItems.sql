SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR371' AND DESCITEM IN ('96CCOL1') AND LANGUAGE = 'E')
INSERT [VM1DTA].[DESCPF] ([DESCPFX], [DESCCOY], [DESCTABL], [DESCITEM], [ITEMSEQ], [LANGUAGE],  [SHORTDESC], [LONGDESC], [USRPRF], [JOBNM], [DATIME], [CREATED_AT]) VALUES ( N'IT', N'9', N'TR371', N'97DCOL1 ', N'  ', N'E',  N'97DCOL1   ', N'AIA Direct Credit Debit Line  ', N'UNDERWR1  ', N'UNDERWR1  ', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
GO
SET QUOTED_IDENTIFIER OFF
GO