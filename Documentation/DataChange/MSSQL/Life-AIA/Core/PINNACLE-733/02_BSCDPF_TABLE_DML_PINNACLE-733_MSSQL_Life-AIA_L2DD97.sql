SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM [VM1DTA].BSCDPF WHERE [bauthcode] = 'B253' and [PRODCODE]='L' and [BSCHEDNAM]='L2DD97') 
	INSERT [VM1DTA].[BSCDPF] ( [BSCHEDNAM], [BNOFTHREDS], [BUNIQINSYS], [BDEBUGMODE], [BAUTHCODE], [JOBQ], [BJOBQPRTY], [BJOBDESC], [PRODCODE], [USRPRF], [JOBNM], [DATIME], [DROPTEMP], [BATCHFLAG], [CREATED_AT]) VALUES ( N'L2DD97    ', 1, N'N', N'N', N'B253', N'QBATCH    ', N'5', N'PAXUS     ', N'L', N'UNDERWR1  ', N'UNDERWR1  ', CURRENT_TIMESTAMP, N'Y', N'N ', CURRENT_TIMESTAMP)

GO
SET QUOTED_IDENTIFIER OFF
GO