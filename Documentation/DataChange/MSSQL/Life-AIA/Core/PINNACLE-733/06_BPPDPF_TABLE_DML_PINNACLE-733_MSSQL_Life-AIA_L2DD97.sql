SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM [VM1DTA].BPPDPF WHERE [BSCHEDNAM] = 'L2DD97' AND [COMPANY]='2') 
	INSERT [VM1DTA].[BPPDPF] ( [BSCHEDNAM], [BPARPSEQNO], [COMPANY], [BPARPPROG], [USRPRF], [JOBNM], [DATIME], [CREATED_AT]) VALUES ( N'L2DD97    ', 1, N'2', N'PR346', N'UNDERWR1  ', N'UNDERWR1  ', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)

GO

SET QUOTED_IDENTIFIER OFF
GO