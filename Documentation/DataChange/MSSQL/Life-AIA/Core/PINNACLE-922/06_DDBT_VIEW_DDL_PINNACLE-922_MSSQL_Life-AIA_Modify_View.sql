/******USE [INT77DB_162]
GO
 Object:  View [VM1DTA].[DDDCMIB_DCBTREC]    Script Date: 16-08-2021 17:03:58 ******/
 
IF EXISTS (SELECT * FROM sys.views where name = 'DDDCMIB_DCBTREC' and schema_id = (select schema_id from sys.schemas where name = 'VM1DTA'))
	DROP VIEW VM1DTA.DDDCMIB_DCBTREC
	GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [VM1DTA].[DDDCMIB_DCBTREC] (
    UNIQUE_NUMBER
    ,COMPANY
    ,FACTHOUS
    ,BILLTYP
    ,CNTTYPE
    ,BILLCD
    ,PAYRCOY
    ,PAYRNUM
    ,MANDREF
    ,RECORDFORMAT
    ,NONKEYS
    )
AS
 SELECT dcbtpf.unique_number,
       dcbtpf.company,
       dcbtpf.facthous,
       dcbtpf.billtyp,
       dcbtpf.cnttype,
       dcbtpf.billcd,
       dcbtpf.payrcoy,
       dcbtpf.payrnum,
       dcbtpf.mandref,
       'DCBTREC'                       AS RECORDFORMAT,
       dcbtpf.company
       + dcbtpf.facthous
       + dcbtpf.billtyp
       + dcbtpf.cnttype
       + Cast(dcbtpf.billcd AS VARCHAR(max))
       + dcbtpf.payrcoy
       + dcbtpf.payrnum
       + dcbtpf.mandref
       + dcbtpf.bankkey
       + dcbtpf.bankacckey
       + Cast(dcbtpf.debitamt AS VARCHAR(max))
       + dcbtpf.ddtrancode
       + dcbtpf.currcode
       + dcbtpf.bankaccdsc
       + Cast(dcbtpf.jobno AS VARCHAR(max))
       + dcbtpf.termid
       + Cast(dcbtpf.user_t AS VARCHAR(max))
       +  Cast(dcbtpf.trdt AS VARCHAR(max))
       + Cast(dcbtpf.trtm AS VARCHAR(max))
       + dcbtpf.dhnflag
       + Cast(dcbtpf.subdat AS VARCHAR(max))
       + dcbtpf.mandstat
       + Cast(dcbtpf.lapday AS VARCHAR(max))
       + dcbtpf.rdocnum
       + dcbtpf.usrprf
       + dcbtpf.jobnm
       + CONVERT(VARCHAR, datime, 121) AS NONKEYS
FROM   vm1dta.dcbtpf;  
GO

SET QUOTED_IDENTIFIER OFF
GO