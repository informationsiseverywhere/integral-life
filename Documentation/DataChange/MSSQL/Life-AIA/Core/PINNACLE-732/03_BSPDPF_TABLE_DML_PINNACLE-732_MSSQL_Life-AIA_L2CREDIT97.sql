﻿
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2CREDIT97' AND COMPANY='2' )
	Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME)
	values ('L2CREDIT97','2','DC-EXTA','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF