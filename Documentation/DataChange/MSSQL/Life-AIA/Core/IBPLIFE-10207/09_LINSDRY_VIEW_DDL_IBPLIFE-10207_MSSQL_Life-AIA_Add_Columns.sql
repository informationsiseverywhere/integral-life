IF EXISTS ( SELECT * FROM sys.views where name = 'LINSDRY' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
DROP VIEW [VM1DTA].[LINSDRY]
GO

CREATE VIEW [VM1DTA].[LINSDRY](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTCURR, VALIDFLAG, BRANCH, INSTFROM, INSTTO, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, INSTJCTL, BILLCHNL, PAYFLAG, DUEFLG, TRANSCODE, CBILLAMT, BILLCURR, MANDREF, BILLCD, PAYRSEQNO, TAXRELMTH, ACCTMETH, JOBNM, USRPRF, DATIME, PRORATEREC ) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            CNTCURR,
            VALIDFLAG,
            BRANCH,
            INSTFROM,
            INSTTO,
            INSTAMT01,
            INSTAMT02,
            INSTAMT03,
            INSTAMT04,
            INSTAMT05,
            INSTAMT06,
            INSTFREQ,
            INSTJCTL,
            BILLCHNL,
            PAYFLAG,
            DUEFLG,
            TRANSCODE,
            CBILLAMT,
            BILLCURR,
            MANDREF,
            BILLCD,
            PAYRSEQNO,
            TAXRELMTH,
            ACCTMETH,
            JOBNM,
            USRPRF,
            DATIME,
			PRORATEREC 
       FROM VM1DTA.LINSPF
      WHERE PAYFLAG <> 'P' AND BILLCHNL <> 'N' AND VALIDFLAG = '1'
GO
