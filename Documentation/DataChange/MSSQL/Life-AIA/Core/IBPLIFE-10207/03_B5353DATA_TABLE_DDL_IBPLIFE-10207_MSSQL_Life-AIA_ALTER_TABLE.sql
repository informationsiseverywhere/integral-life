-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORATEREC' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5353DATA'))
	BEGIN
		ALTER TABLE [VM1DTA].[B5353DATA] ADD PRORATEREC NCHAR(1) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Proraterec added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5353DATA', @level2type=N'COLUMN',@level2name=N'PRORATEREC ';
	END
GO