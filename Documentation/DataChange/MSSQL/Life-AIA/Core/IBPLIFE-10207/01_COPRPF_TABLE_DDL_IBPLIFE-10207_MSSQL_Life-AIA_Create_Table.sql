IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'COPRPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE VM1DTA.COPRPF( 
		   UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL ,
           CHDRCOY nchar(1) NULL,
           CHDRNUM nchar(8) NULL,
		   LIFE nchar(2) NULL,
		   JLIFE nchar(2) NULL,
		   COVERAGE nchar(2) NULL,
		   RIDER nchar(2) NULL,
		   PLNSFX int NULL,
		   VALIDFLAG nchar(1) NULL,
		   TRANNO int NULL,
		   SUMINS numeric(17, 2) NULL,
		   SINGP numeric(17, 2) NULL,
		   ZSTPDUTY01 numeric(17, 2) NULL,
		   RISKPREM numeric(17, 2) NULL,
		   COMMPREM numeric(17, 2) NULL,
		   INSTPREM numeric(17, 2) NULL,
		   ZBINSTPREM numeric(17, 2) NULL,
		   ZLINSTPREM numeric(17, 2) NULL,
		   INSTFROM int NULL,
		   INSTTO int NULL,
		   USRPRF nchar(10) NULL,
		   JOBNM nchar(10) NULL,
		   DATIME datetime2(6) NULL,  
		   CREATED_AT datetime2(6) NULL
	   CONSTRAINT PK_COPRPF PRIMARY KEY CLUSTERED 
	   (
		UNIQUE_NUMBER ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)ON [PRIMARY]
	)ON [PRIMARY]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'COPRPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COPRPF TABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF';
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'UNIQUE_NUMBER' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CHDRCOY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CHDRNUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'LIFE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'LIFE';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'JLIFE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JLIFE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'JLIFE';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COVERAGE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COVERAGE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RIDER' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RIDER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'RIDER';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PLNSFX' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PLAN SUFFIX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'PLNSFX';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'VALIDFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VALIDFLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'TRANNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transcation No' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'SUMINS' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SUM INSURED' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'SUMINS';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'SINGP' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SINGLE PREMIUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'SINGP';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ZSTPDUTY01' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'STAMP DUTY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'ZSTPDUTY01';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RISKPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RISKPREMIUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'RISKPREM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COMMPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMMPREM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'COMMPREM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INSTPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INSTALLMENT PREMIUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'INSTPREM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ZBINSTPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BASE INSTALLMENT PREMIUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'ZBINSTPREM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ZLINSTPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LOADED INSTALMENT PREMIUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'ZLINSTPREM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INSTFROM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INSTAL FROM DATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'INSTFROM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'INSTTO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INSTAL TO DATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'INSTTO';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'USRPRF' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER PROFILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'JOBNM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOB Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'DATIME' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'DATIME';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CREATED_AT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COPRPF'))
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COPRPF', @level2type=N'COLUMN',@level2name=N'CREATED_AT';

GO

