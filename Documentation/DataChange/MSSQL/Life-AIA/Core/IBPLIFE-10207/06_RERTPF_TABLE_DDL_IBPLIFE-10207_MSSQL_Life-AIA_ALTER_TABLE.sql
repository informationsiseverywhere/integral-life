-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COMMPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RERTPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[RERTPF] ADD COMMPREM numeric(17, 2);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Commprem added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'COMMPREM ';
	END
GO