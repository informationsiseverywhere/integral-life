IF EXISTS ( SELECT * FROM sys.views where name = 'LINSREV' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
DROP VIEW [VM1DTA].[LINSREV]
GO

CREATE VIEW [VM1DTA].[LINSREV](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, INSTTO, INSTAMT06, PAYFLAG, USRPRF, JOBNM, DATIME, PRORATEREC) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            INSTTO,
            INSTAMT06,
            PAYFLAG,
            USRPRF,
            JOBNM,
            DATIME,
			PRORATEREC
       FROM vm1dta.LINSPF
      WHERE NOT (PAYFLAG = 'P')
GO