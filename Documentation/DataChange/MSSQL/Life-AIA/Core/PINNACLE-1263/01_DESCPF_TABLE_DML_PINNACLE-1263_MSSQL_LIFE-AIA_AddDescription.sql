SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM  VM1DTA.DESCPF WHERE  DESCTABL='TR2A5' AND DESCITEM='CSOTH017') 
insert into VM1DTA.DESCPF ( DESCPFX , DESCCOY , DESCTABL , DESCITEM , ITEMSEQ , LANGUAGE , TRANID , SHORTDESC , LONGDESC , USRPRF , JOBNM , DATIME ) values ('IT','2','TR2A5','CSOTH017  ','  ','E','0078','OccDisable','Occupation Code Disable','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP)
GO
SET QUOTED_IDENTIFIER OFF
GO