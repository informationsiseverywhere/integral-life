IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('LIFE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))
BEGIN
	ALTER TABLE RLVRPF ADD LIFE NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.RLVRPF.LIFE IS Rollover LIFE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'LIFE';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('COVERAGE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))
BEGIN
	ALTER TABLE RLVRPF ADD COVERAGE NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.RLVRPF.COVERAGE IS Rollover COVERAGE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('RIDER') and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))
BEGIN
	ALTER TABLE RLVRPF ADD RIDER NCHAR(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.RLVRPF.RIDER IS Rollover RIDER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'RIDER';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('VALIDFLAG') and object_id in (SELECT object_id FROM sys.tables WHERE name ='RLVRPF'))
BEGIN
	ALTER TABLE RLVRPF ADD VALIDFLAG NCHAR(1);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.RLVRPF.VALIDFLAG IS Rollover VALIDFLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'RLVRPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
END
GO
