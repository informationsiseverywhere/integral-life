DROP TABLE VM1DTA.[NOCHPF]
GO
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'NOCHPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[NOCHPF]( 
		   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
           [Notificoy] [char](1) NULL,
           [NotifiNum] [char](14) NULL,
		   [Notificnum] [char](8) NULL,
		   [Claimant] [char](8) NULL,
		   [CHDRNUM] [char](8) NULL,
		   [RELATIONCNUM] [char](8) NULL,
		   [IncidentT] [char](8) NULL,
		   [doctor] [char](8) NULL,
		   [medicalpd] [char](10) NULL,
		   [USRPRF] [char](14) NULL,
		   [JOBNM] [char](10) NULL,
		   [claimat] [numeric](17, 2) ,
		   [NOTIFIDATE] [datetime2](6) NULL,
		   [INCURDATE] [datetime2](6) NULL,
		   [deathDate] [datetime2](6) NULL,
		   [causedate] [datetime2](6) NULL,   
		   [admintdate] [datetime2](6) NULL,
		   [dischargedate] [datetime2](6) NULL,
		   [DATIME] [datetime2](6) NULL,
		   [Location] [nchar](200) NULL,
		   [rclaimReson] [nchar](10) NULL,
		   [hospitalle] [nchar](10) NULL,
		   [accidentdesc] [nchar](3000) NULL,
		   [notifistatus] [nchar](20) NULL, 
		   [RCLAIMREASON] [nchar](20) NULL, 
		   [DIAGNOSIS] [nchar](20) NULL, 
		   
		   
	   CONSTRAINT [PK_NOCHPF] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)ON [PRIMARY]
	)ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NOTIFICATION CONTRACT TABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONT HEADER COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'NOTIFICOY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Notification Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'NOTIFINUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'NOTIFICNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claimant' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'CLAIMANT';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'chdrnum' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relationship to Life Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'RELATIONCNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notification Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'NOTIFIDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Location' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'LOCATION';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Incident Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'INCIDENTT';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Death' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'DEATHDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cause of Death' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'CAUSEDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Regular Claim Reason' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'RCLAIMREASON';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'CLAIMAT';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'doctor' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'DOCTOR';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Medical Provider' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'MEDICALPD';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hospital Level' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'HOSPITALLE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Diagnosis' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'DIAGNOSIS';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Admit Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'ADMINTDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discharge Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'DISCHARGEDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accident Description' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'ACCIDENTDESC';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accident Description' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOCHPF', @level2type=N'COLUMN',@level2name=N'NOTIFISTATUS';
	
GO
