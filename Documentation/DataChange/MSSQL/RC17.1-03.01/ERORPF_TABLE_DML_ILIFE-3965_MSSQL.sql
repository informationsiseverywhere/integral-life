
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E'AND h.EROREROR ='RPJ2' AND h.ERORDESC = 'Only Allow Annual Billing'  )
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RPJ2'
           ,'Only Allow Annual Billing'
           ,'0'
           ,'0'
           ,'000008'
           ,''
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
           
GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E'AND h.EROREROR ='RPJ3' AND h.ERORDESC = 'Rollover Details required'  )
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RPJ3'
           ,'Rollover Details required'
           ,'0'
           ,'0'
           ,'000008'
           ,''
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
           
GO





