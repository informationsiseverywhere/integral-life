IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('LNKGIND') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVRPF'))
BEGIN
	ALTER TABLE COVRPF ADD LNKGIND NVARCHAR(1);
			  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<LINKED TPD>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVRPF', @level2type=N'COLUMN',@level2name=N'LNKGIND';
	
END
GO