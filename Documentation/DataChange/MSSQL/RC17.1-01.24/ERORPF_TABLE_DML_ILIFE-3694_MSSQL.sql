IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
WHERE h.ERORPFX = 'ER' AND h.ERORLANG='E'AND h.EROREROR ='RFXO' AND h.ERORDESC = 'Only 1 input required'  )
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFXO'
           ,'Only 1 input required'
           ,'20508'
           ,'150933'
           ,'000039'
           ,'QPADEV002F'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
           
GO