-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'YCTXPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN
		CREATE TABLE [VM1DTA].[YCTXPF]
		(
			UNIQUE_NUMBER BIGINT IDENTITY(1,1) NOT NULL,
			CHDRCOY NCHAR(1) NULL,
			CHDRNUM NCHAR(8) NULL,
			CNTTYPE NCHAR(3) NULL,
			VALIDFLAG NCHAR(1) NULL,
			STAXDT INT NULL,
			ETAXDT INT NULL,
			TRANNO INT NULL,
			BATCTRCDE NCHAR(4) NULL,
			TOTALTAXAMT NUMERIC(17,2),
			RPOFFSET NUMERIC(17,2),
			EFFDATE INT NULL,
			USRPRF NCHAR(10) NULL,
			JOBNM NCHAR(10) NULL,
			DATIME DATETIME2 NULL
		);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'annual tax table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tax Start Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'STAXDT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tax End Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'ETAXDT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch Trans Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'BATCTRCDE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total tax Amt' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'TOTALTAXAMT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Risk Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'RPOFFSET';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jobnm' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'DATIME';
	END
GO