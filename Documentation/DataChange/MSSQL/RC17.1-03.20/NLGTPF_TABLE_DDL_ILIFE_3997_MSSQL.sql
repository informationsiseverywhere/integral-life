IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'NLGTPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

	CREATE TABLE [VM1DTA].[NLGTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[TRANNO] [numeric](5, 0) NULL,
	[EFFDATE] [numeric](8, 0) NULL,
	[BATCACTYR] [numeric](4, 0) NULL,
	[BATCACTMN] [numeric](2, 0) NULL,
	[BATCTRCDE] [nchar](4) NULL,
	[TRANDESC] [nchar](30) NULL,
	[TRANAMT] [numeric](17, 2) NULL,
	[FROMDATE] [numeric](8, 0) NULL,
	[TODATE] [numeric](8, 0) NULL,
	[NLGFLAG] [nchar](1) NULL,
	[YRSINF] [numeric](3, 0) NULL,
	[AGE] [numeric](3, 0) NULL,
	[NLGBAL] [numeric](17, 2) NULL,
	[AMT01] [numeric](17, 2) NULL,
	[AMT02] [numeric](17, 2) NULL,
	[AMT03] [numeric](17, 2) NULL,
	[AMT04] [numeric](17, 2) NULL,
	[VALIDFLAG] [char](1) NULL,
	[USRPRF] [char](10) NULL,
	[JOBNM] [char](10) NULL,
	[DATIME] [datetime2](7) NULL,
	)
	GO  

