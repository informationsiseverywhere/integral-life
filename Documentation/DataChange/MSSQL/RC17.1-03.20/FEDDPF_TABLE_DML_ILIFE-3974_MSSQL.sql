IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'FEDDPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[FEDDPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRPFX][nchar](2) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[STAT_CHANGE_DAT][DECIMAL](8)NULL,
	[VALIDFLAG][nchar](1)NULL,
	[FEDDFLAG][nchar](1)NULL,
	[FPVFLAG][nchar](1)NULL,
	[USER_PROFILE][nchar](10)NULL,
	[JOBNAME][nchar](10)NULL,
	[DATIME] [datetime2](6) NULL, 
) ON [PRIMARY]

GO
