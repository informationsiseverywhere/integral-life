﻿
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5HE' AND DESCITEM='TA74CNY' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5HE','TA74CNY','  ','E',' ','CashDvdW  ','Cash Divident Withdrawal','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5HE' AND DESCITEM='TA74CNY' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5HE','TA74CNY','  ','S',' ','CashDvdW  ','Cash Divident Withdrawal','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO
