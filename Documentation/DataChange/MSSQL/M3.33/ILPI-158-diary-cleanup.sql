--cleanup is required before we start testing on diary
--this will disable old contracts, so that they are not picked by diary. This is necessary because old contracts might have some junk data
--or they might have been processed through batch.
--it will also clear some files, which otherwise might have some junk data
delete from dsinpf;
delete from drptpf;
delete from dconpf;
delete from dlogpf;
delete from dhdrpf;
delete from dtrdpf;