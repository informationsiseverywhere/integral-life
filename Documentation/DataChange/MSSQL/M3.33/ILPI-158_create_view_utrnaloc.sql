SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[UTRNALOC](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, TERMID, TRDT, TRTM, USER_T, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, JCTLJNUMPR, FUNDRATE, UNITSA, STRPDATE, NDFIND, NOFUNT, NOFDUNT, MONIESDT, PRICEDT, PRICEUSED, UBREPR, CRTABLE, CNTCURR, FDBKIND, COVDBTIND, INCINUM, INCIPERD01, INCIPERD02, INCIPRM01, INCIPRM02, USTMNO, CNTAMNT, FNDCURR, FUNDAMNT, SACSCODE, SACSTYP, GENLCDE, CONTYP, TRIGER, TRIGKY, PRCSEQ, SVP, DISCFA, PERSUR, CRCDTE, CIUIND, SWCHIND, BSCHEDNAM, BSCHEDNUM, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            VRTFND,
            UNITYP,
            TRANNO,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            JCTLJNUMPR,
            FUNDRATE,
            UNITSA,
            STRPDATE,
            NDFIND,
            NOFUNT,
            NOFDUNT,
            MONIESDT,
            PRICEDT,
            PRICEUSED,
            UBREPR,
            CRTABLE,
            CNTCURR,
            FDBKIND,
            COVDBTIND,
            INCINUM,
            INCIPERD01,
            INCIPERD02,
            INCIPRM01,
            INCIPRM02,
            USTMNO,
            CNTAMNT,
            FNDCURR,
            FUNDAMNT,
            SACSCODE,
            SACSTYP,
            GENLCDE,
            CONTYP,
            TRIGER,
            TRIGKY,
            PRCSEQ,
            SVP,
            DISCFA,
            PERSUR,
            CRCDTE,
            CIUIND,
            SWCHIND,
            BSCHEDNAM,
            BSCHEDNUM,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.UTRNPF
      WHERE FDBKIND = ' '

GO