
delete from VM1DTA.HELPPF where HELPITEM = 'ZHPLEINT' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','ZHPLEINT',1,'1','User is required to enter the amount and system','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','ZHPLEINT',2,'1','will use this amount to process the calculation','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','ZHPLEINT',3,'1','and validation and provide error message or','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','ZHPLEINT',4,'1','allow to process accordingly','UNDERWR1  ','UNDERWR1',current_timestamp);


delete from VM1DTA.HELPPF where HELPITEM = 'REPYMOP' and HELPLANG = 'E' and HELPTYPE = 'F';

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',1,'1','User is to select the repayment method.','UNDERWR1  ','UNDERWR1',current_timestamp);


insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',2,'1','If the method selected is Direct Debit,the bank details','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',3,'1','is required to input the client�s bank account.','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',4,'1','If the method selected is Credit Card, the bank details','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',5,'1','is required to input the client�s credit card details.','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',6,'1','If the method is selected as cash, system is to check the','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',7,'1','suspense account and if the amount is sufficient,then will','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',8,'1','allow user to proceed, else will trigger error message','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',9,'1','�insufficient amt in suspense ac�.The drop-down list','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',10,'1','consists of only 3 options: Direct Debit, Credit Card,Cash.','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',11,'1','Please refers to section 5.1.12 where the method is being','UNDERWR1  ','UNDERWR1',current_timestamp);

insert into VM1DTA.HELPPF (TRANID,HELPPFX,HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME) values (' ','HP',' ','E','F','          ','REPYMOP',12,'1','defined.','UNDERWR1  ','UNDERWR1',current_timestamp);


