
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'RPLNPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[RPLNPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[REPYMOP] [nchar](8) NOT NULL,	
	[REPAYMENTAMT] [numeric](17, 2) NOT NULL,
	[TOTALPLPRNCPL] [numeric](17, 2) NOT NULL,
	[TOTALPLINT] [numeric](17, 2) NOT NULL,
	[TOTALAPLPRNCPL] [numeric](17, 2) NOT  NULL,
	[TOTALAPLINT] [numeric](17, 2) NOT NULL,
	[REMNGPLPRNCPL] [numeric](17, 2) NOT NULL,
	[REMNGPLINT] [numeric](17, 2) NOT NULL,
	[REMNGAPLPRNCPL] [numeric](17, 2) NOT NULL,
	[REMNGAPLINT] [numeric](17, 2) NOT NULL,
	[STATUS] [nchar](1) NOT NULL,
	[APRVDATE]  [nchar](10)NOT NULL,
	[TRANNO]  [nchar](5)NOT NULL,	
	[USRPRF]   [nchar](10) NULL,       
    [JOBNM] [nchar](10) NULL,     
    [DATIME] [datetime2](7) NOT NULL,
	[MANDREF]  [nchar](5)NOT NULL,
CONSTRAINT [PK_RPLNPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
) ON [PRIMARY] 
GO


