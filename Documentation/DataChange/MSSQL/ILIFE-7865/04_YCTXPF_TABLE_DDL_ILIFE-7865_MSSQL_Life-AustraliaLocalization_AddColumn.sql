IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('NOTRCVD') and object_id in (SELECT object_id FROM sys.tables WHERE name ='YCTXPF'))
BEGIN
	ALTER TABLE YCTXPF ADD NOTRCVD NVARCHAR(1);
			  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<S290 Notice Received>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'YCTXPF', @level2type=N'COLUMN',@level2name=N'NOTRCVD';
	
END
GO