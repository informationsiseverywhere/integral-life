SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTAMT','1','','1','This field holds the revised amount of contributions      ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTAMT','2','','1','that the employer contributes on behalf of the member up to','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTAMT','3','','1','the end of the current tax period in relation to the ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTAMT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTAMT','4','','1','Superannuation Guarantee or a SGC Penalty Voucher.   ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERAMT','1','','1','This field holds the revised amount of contributions ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERAMT','2','','1','that the employer contributes on behalf of the ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERAMT','3','','1','member up to the end of the current tax period. This ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERAMT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERAMT','4','','1','field does not include any Superannuation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERAMT' AND HELPSEQ = '5') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERAMT','5','','1',' Guarantee contributions.          ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDAMT','1','','1','This field holds the revised amount of contributions ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDAMT','2','','1','that the member will be claiming as a tax deduction ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDAMT','3','','1','for the current tax period.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDAMT','1','','1','This field holds the revised amount of contributions ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDAMT','2','','1','that the member will not be claiming as a tax ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDAMT','3','','1','deduction in the current tax period. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSAMT','1','','1','This field holds the revised amount of spouse ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSAMT','2','','1','contributions that the member will not be claiming as ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSAMT','3','','1','a tax deduction in the current tax period. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYAMT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYAMT','1','','1','This field holds the revised amount of salary sacrifice ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYAMT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYAMT','2','','1','contributions that the member will not be claiming as ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYAMT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYAMT','3','','1','a tax deduction in the current tax period.   ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','1','','1','This field holds the revised percentage of ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','2','','1','contributions that the employer contributes on behalf ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','3','','1','of the member up to the end of the current tax period ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','4','','1','in relation to the Superannuation Guarantee or a SGC ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '5') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','5','','1','Penalty Voucher. The percentage entered can be ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSGTPCT' AND HELPSEQ = '6') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSGTPCT','6','','1','either 0 to 100%. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','1','','1','This field holds the revised percentage of ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','2','','1','contributions that the employer contributes on behalf ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','3','','1','of the member up to the end of the current tax period. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','4','','1','This field does not include any Superannuation ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '5') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','5','','1','Guarantee contributions. The percentage entered can ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVOERPCT' AND HELPSEQ = '6') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVOERPCT','6','','1','be 0 to 100%.                                 ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDPCT','1','','1','This field holds the revised percentage of ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDPCT','2','','1','contributions that the member will be claiming as a ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDPCT','3','','1','tax deduction for the current tax period. The ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVDEDPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVDEDPCT','4','','1','percentage entered can be 0 to 100%.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDPCT','1','','1','This field holds the revised percentage of ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDPCT','2','','1','contributions that the member will not be claiming as ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDPCT','3','','1','a tax deduction in the current tax period. The ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVUNDPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVUNDPCT','4','','1','percentage entered can be 0 to 100%.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSPCT','1','','1','This field holds the revised percentage of spouse ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSPCT','2','','1','contributions that the member will not be claiming as ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSPCT','3','','1','a tax deduction in the current tax period. The ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSPSPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSPSPCT','4','','1','percentage entered can be 0 to 100%.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYPCT' AND HELPSEQ = '1') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYPCT','1','','1','This field holds the revised percentage of salary ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYPCT' AND HELPSEQ = '2') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYPCT','2','','1','sacrifice contributions that the member will not be ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYPCT' AND HELPSEQ = '3') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYPCT','3','','1','claiming as a tax deduction in the current tax period. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.HELPPF WHERE HELPLANG='E' AND HELPITEM='REVSLYPCT' AND HELPSEQ = '4') 
BEGIN
INSERT INTO VM1DTA.HELPPF (HELPPFX, HELPCOY,HELPLANG,HELPTYPE,HELPPROG,HELPITEM,HELPSEQ,TRANID,VALIDFLAG,HELPLINE,USRPRF,JOBNM,DATIME)
VALUES('HP','','E','F','','REVSLYPCT','4','','1','The percentage entered can be 0 to 100%.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
END