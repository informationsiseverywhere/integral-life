
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'SEQUENCE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BNFYPF'))
	BEGIN
	ALTER TABLE [VM1DTA].[BNFYPF] ADD SEQUENCE NCHAR(2) null ;
		  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Beneficiary Sequence>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BNFYPF', @level2type=N'COLUMN',@level2name=N'SEQUENCE';
	END
GO

IF EXISTS ( SELECT * FROM sys.views where name = 'BNFYLNB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.BNFYLNB
GO
CREATE VIEW [VM1DTA].[BNFYLNB]
AS
SELECT        UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, CURRFR, CURRTO, BNYRLN, BNYCD, BNYPC, TERMID, USER_T, TRTM, TRDT, 
                         EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE
FROM            VM1DTA.BNFYPF
WHERE        (VALIDFLAG = '1')
GO

IF EXISTS ( SELECT * FROM sys.views where name = 'BNFYENQ' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.BNFYENQ
GO
CREATE VIEW [VM1DTA].[BNFYENQ]
AS
SELECT        UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, VALIDFLAG, CURRFR, CURRTO, BNYRLN, BNYCD, BNYPC, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, 
                         CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE
FROM            VM1DTA.BNFYPF
WHERE        (VALIDFLAG = '1')
GO