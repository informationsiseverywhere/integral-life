
if not exists (select * from syscolumns where id=object_id('VM1DTA.LIFEPF') and name='relation') 
	alter table VM1DTA.LIFEPF add relation NCHAR(4);
GO

IF EXISTS ( SELECT * FROM sys.views where name = 'LIFELNB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFELNB
GO
CREATE VIEW [VM1DTA].[LIFELNB]
AS
SELECT        UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, STATCODE, TRANNO, CURRFROM, CURRTO, LIFE, JLIFE, LCDTE, LIFCNUM, LIFEREL, ANBCCD, CLTSEX, 
                         CLTDOB, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, SBSTDL, TERMID, TRDT, TRTM, USER_T, RELATION
FROM            VM1DTA.LIFEPF
GO