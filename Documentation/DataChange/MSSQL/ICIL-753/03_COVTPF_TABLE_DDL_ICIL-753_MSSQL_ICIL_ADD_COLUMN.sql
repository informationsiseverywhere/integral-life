IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CASHVALARER') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVTPF'))
BEGIN
	ALTER TABLE COVTPF ADD CASHVALARER NUMERIC(17,2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'<Cash Value in Arrears>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'<COVTPF>', @level2type=N'COLUMN',@level2name=N'<CASHVALARER>';
END
GO
