
IF EXISTS ( SELECT * FROM sys.views where name = 'CHDRSUR' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.CHDRSUR
GO
CREATE VIEW [VM1DTA].[CHDRSUR]
AS
SELECT UNIQUE_NUMBER,
 CHDRCOY,                
 CHDRNUM,               
 SERVUNIT,                
 CNTTYPE,                
 POLINC,                      
 POLSUM,                   
 NXTSFX,                
 TRANNO,                   
 TRANID,                
 VALIDFLAG,              
 CURRFROM,               
 CURRTO,                    
 AVLISU,                 
 STATCODE,              
 STATDATE,               
 STATTRAN,               
 PSTCDE,                 
 PSTDAT,                     
 PSTTRN,                   
 TRANLUSED,                
 OCCDATE,                 
 CCDATE,                   
 COWNPFX,                
 COWNCOY,                
 COWNNUM,                  
 JOWNNUM,                
 CNTBRANCH,              
 AGNTPFX,                 
 AGNTCOY,                  
 AGNTNUM,              
 CNTCURR,                 
 PAYPLAN,                 
 ACCTMETH,                
 BILLFREQ,               
 BILLCHNL,              
 COLLCHNL,             
 BILLDAY,                
 BILLMONTH,              
 BILLCD,                     
 BTDATE,                     
 PTDATE,                    
 USRPRF,                 
 JOBNM,                  
 DATIME,
 SRCEBUS 

FROM   CHDRPF
GO
	
	