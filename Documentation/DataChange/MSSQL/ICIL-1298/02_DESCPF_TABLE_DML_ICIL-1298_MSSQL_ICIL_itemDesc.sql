
GO
delete from VM1DTA.DESCPF where DESCTABL='TD5GF' and DESCCOY='2' and (LANGUAGE = 'E' OR LANGUAGE = 'S') and DESCPFX = 'IT'; 
GO

GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='*****')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','*****','  ','E','','*****','Default item','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','*****','  ','S','','*****','Default item','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO


GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCI**')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCI**','  ','E','','CCI**','China Critical Illness-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCI**','  ','S','','CCI**','China Critical Illness-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO


GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDA**')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDA**','  ','E','','CDA**','ChinaDeferred Annuity-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDA**','  ','S','','CDA**','ChinaDeferred Annuity-Default','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO


GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCIAG')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIAG','  ','E','','CCIAG','China Critical Illness-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIAG','  ','S','','CCIAG','China Critical Illness-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO


GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CCIBA')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIBA','  ','E','','CCIBA','China Critical Illness-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CCIBA','  ','S','','CCIBA','China Critical Illness-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO

GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDAAG')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDAAG','  ','E','','CDAAG','ChinaDeferred Annuity-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDAAG','  ','S','','CDAAG','ChinaDeferred Annuity-Agent','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO


GO
IF NOT EXISTS (	
  SELECT *  
  FROM DESCPF 
  where DESCCOY='2' AND DESCTABL='TD5GF' AND DESCITEM='CDABA')
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDABA','  ','E','','CDABA','ChinaDeferred Annuity-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5GF','CDABA','  ','S','','CDABA','ChinaDeferred Annuity-BancAss','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);		
GO