
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYDH') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDH','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYCF') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYCF','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYDC') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYDC','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYEX') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYEX','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYLA') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYLA','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO



GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYMA') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYMA','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYNT') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYNT','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO



GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYPO') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPO','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYPS') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPS','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYPU') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYPU','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO



GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ITEMPF i WHERE i.ITEMCOY='2' AND i.ITEMTABL='TR630' AND i.ITEMITEM='***CNYWD') 
insert into VM1DTA.ITEMPF (ITEMPFX,ITEMCOY,ITEMTABL,ITEMITEM,ITEMSEQ,TRANID,TABLEPROG,VALIDFLAG,ITMFRM,ITMTO,GENAREA,USRPRF,JOBNM,DATIME) values
 ('IT','2','TR630','***CNYWD','  ','  ','PR630 ','1',0,0,CONVERT(VARBINARY(2000),'0x203030303030303039393939393939393030303030303030303030303030303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020',1),'UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO











