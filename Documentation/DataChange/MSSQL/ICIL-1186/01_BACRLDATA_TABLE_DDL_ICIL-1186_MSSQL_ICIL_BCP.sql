
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BACRLDATA' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[BACRLDATA]( 
		   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
           [BACRTAPREC] [char](500) NULL,
           [MEMBER_NAME] [char](10) NULL,
		   
	   CONSTRAINT [PK_BACRLDATA] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)ON [PRIMARY]
	)ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To store return file data to process in L2retbnk batch' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BACRLDATA';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BACRLDATA', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BACRTAPREC' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BACRLDATA', @level2type=N'COLUMN',@level2name=N'BACRTAPREC';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MEMBER_NAME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BACRLDATA', @level2type=N'COLUMN',@level2name=N'MEMBER_NAME';

GO

   
