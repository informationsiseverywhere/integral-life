-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'COCONTPF' and schema_id = (select schema_id from sys.schemas where name='COCONTPF'))
	BEGIN
		CREATE TABLE [VM1DTA].[COCONTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[DATERCVD] [int] NULL,
	[COCONTAMNT] [numeric](17, 2) NOT NULL DEFAULT '0',
	[LISCAMNT] [NUMERIC] (17, 2) NOT NULL DEFAULT '0',
	[PRCDFLAG] [nvarchar](1) NULL,
 CONSTRAINT [PK_COCONTPF] PRIMARY KEY CLUSTERED 
([UNIQUE_NUMBER] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Co-Contribution & LISC Table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Received' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'DATERCVD';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Co-Contribution Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'COCONTAMNT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LISC Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'LISCAMNT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Processed Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COCONTPF', @level2type=N'COLUMN',@level2name=N'PRCDFLAG';
		
	END
GO