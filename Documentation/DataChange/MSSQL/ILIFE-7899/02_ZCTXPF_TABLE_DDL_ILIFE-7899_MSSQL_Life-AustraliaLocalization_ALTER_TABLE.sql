-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COCONTAMNT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCTXPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[ZCTXPF] ADD COCONTAMNT NUMERIC(17,2) NOT NULL DEFAULT '0';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<co-contribution added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZCTXPF', @level2type=N'COLUMN',@level2name=N'COCONTAMNT ';
	END
GO

-- -- -- -- -- -- -- -- ADD NEW COLUMN TO TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'LISCAMNT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCTXPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[ZCTXPF] ADD LISCAMNT NUMERIC(17,2) NOT NULL DEFAULT '0';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<lisc added>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZCTXPF', @level2type=N'COLUMN',@level2name=N'LISCAMNT ';
	END
GO
