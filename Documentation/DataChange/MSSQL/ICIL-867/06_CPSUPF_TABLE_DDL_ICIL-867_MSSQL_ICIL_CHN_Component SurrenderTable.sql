
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'CPSUPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
	CREATE TABLE [VM1DTA].[CPSUPF]( 

	   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
	   [CHDRPFX] [char](2) NULL,
       [CHDRCOY] [char](1) NULL,
       [CHDRNUM] [char](8) NULL,
	   [TRANNO] [numeric](5,0) NULL,
	   [BATCTRCDE] [char](4) NULL,
	   [RIDER] [char](2) NULL,
	   [CRTABLE] [char](4) NULL,
	   [CNTTYPE] [char](4) NULL,
	   [COMPRSKCODE] [char](2) NULL,
	   [COMPPREMCODE] [char](2) NULL,
	   [COMPSUMSS] [numeric](17,2),
	   [COMPBILPRM] [numeric](17,2),
	   [REFPRM] [numeric](17,2),
	   [REFADJUST] [numeric](17,2),
	   [REFAMT] [numeric](17,2),
	   [STATUS] [char](1) NULL,
	   [PAYRNUM] [char](8) NULL,
       [REQNTYPE] [char](1) NULL,
       [BANKACCKEY] [char](20) NULL,
       [BANKKEY] [char](10) NULL,
       [BANKDESC] [char](60) NULL,
	   [COMPSTATUS] [char](2) NULL,
	   [VALIDFLAG]  [char](1)NULL,
	   [LIFE]  [char](2)NULL,
	   [COVERAGE]  [char](2)NULL,
	   [PLNSFX]  [numeric](2)NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar](10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   CONSTRAINT [PK_CPSUPF] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)ON [PRIMARY]
	
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cpsupf table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONT HEADER PREFIX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'CHDRPFX';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONT HEADER COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONTRACT NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRANSACTION NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BATCH TRANSACTION CODE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'BATCTRCDE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rider' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'RIDER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'CRTABLE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Risk Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COMPRSKCODE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Premium Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COMPPREMCODE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Sum Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COMPSUMSS';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Billing Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COMPBILPRM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'REFPRM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refaund Adjustment' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'REFADJUST';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refaund amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'REFAMT';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'STATUS';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PayerNumber' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'PAYRNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'METHOD OF PAYOUT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'REQNTYPE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANK ACCOUNT KEY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'BANKACCKEY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANK KEY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'BANKKEY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANK DESCRIPTION' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'BANKDESC';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COMPSTATUS';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'validFlag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'life number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'LIFE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coverage' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PLNSFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'PLNSFX';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER PROFILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOB NAME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and Time' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CPSUPF', @level2type=N'COLUMN',@level2name=N'DATIME';
END	

GO