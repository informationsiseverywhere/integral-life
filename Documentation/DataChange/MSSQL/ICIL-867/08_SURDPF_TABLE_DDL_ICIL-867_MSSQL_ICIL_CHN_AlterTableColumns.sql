

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'OTHERADJST' and object_id in (SELECT object_id FROM sys.tables WHERE name ='SURDPF'))
BEGIN       
    ALTER TABLE VM1DTA.SURDPF ADD OTHERADJST numeric(17,2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<OTHERADJST AMOUNT>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'<SURDPF>', @level2type=N'COLUMN',@level2name=N'<OTHERADJST>';
END
GO
			
