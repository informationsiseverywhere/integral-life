IF EXISTS ( SELECT * FROM sys.views where name = 'SURDCLM' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
DROP VIEW [VM1DTA].[SURDCLM]
GO

  CREATE  VIEW [VM1DTA].[SURDCLM] (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, TRANNO, LIFE, JLIFE, COVERAGE, RIDER, CRTABLE, SHORTDS, LIENCD, CURRCD, PLNSFX, EMV, ACTVALUE, TYPE_T, VRTFUND, USRPRF, JOBNM, DATIME,OTHERADJST) AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            CRTABLE,
            SHORTDS,
            LIENCD,
            CURRCD,
            PLNSFX,
            EMV,
            ACTVALUE,
            TYPE_T,
            VRTFUND,
            USRPRF,
            JOBNM,
            DATIME,
			OTHERADJST
       FROM VM1DTA.SURDPF
  
GO




