SET QUOTED_IDENTIFIER ON  
GO

IF EXISTS ( SELECT * FROM sys.views where name = 'SURHCLM' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].SURHCLM
GO

CREATE VIEW [VM1DTA].[SURHCLM](UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
			PLNSFX,
            LIFE,
            JLIFE,
            EFFDATE,
            CURRCD,
            POLICYLOAN,
            TDBTAMT,
            TAXAMT,
            OTHERADJST,
            REASONCD,
            RESNDESC,
            CNTTYPE,
            ZRCSHAMT,
			USRPRF,
            JOBNM,
            DATIME,
			SUSPENSEAMT)
AS
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
			PLNSFX,
            LIFE,
            JLIFE,
            EFFDATE,
            CURRCD,
            POLICYLOAN,
            TDBTAMT,
            TAXAMT,
            OTHERADJST,
            REASONCD,
            RESNDESC,
            CNTTYPE,
            ZRCSHAMT,
			USRPRF,
            JOBNM,
            DATIME,
			SUSPENSEAMT
       FROM SURHPF;
  
GO
SET QUOTED_IDENTIFIER OFF  
GO



