IF EXISTS ( SELECT * FROM sys.views where name = 'COVRRNL' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
	DROP VIEW [VM1DTA].COVRRNL
END
GO

/****** Object:  View [VM1DTA].[COVRRNL]    Script Date: 4/9/2019 4:33:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[COVRRNL](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, TRANNO, CURRFROM, CURRTO, JLIFE, STATCODE, PSTATCODE, CRRCD, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, PRMCUR, CRTABLE, PCESDTE, RCESDTE, SUMINS, SICURR, MORTCLS, SINGP, INSTPREM, ZBINSTPREM, ZLINSTPREM, RRTDAT, RRTFRM, BBLDAT, CBANPR, CRDEBT, ANBCCD, SEX, TERMID, TRDT, TRTM, USER_T, STFUND, STSECT, STSSECT, LIENCD, RATCLS, INDXIN, BNUSIN, PAYRSEQNO, CPIDTE, VARSI, CBUNST, USRPRF, JOBNM, DATIME, STATREASN, REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, REPTCD06, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, RCESTRM, PCESTRM, BCESTRM, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, CHGOPT, FUNDSP, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, RCTMTH, RCTDAY, JLLSID, CBCVIN, CBRVPR, ICANDT, EXALDT, IINCDT, BAPPMETH, ZSTPDUTY01, ZCLSTATE) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            VALIDFLAG,
            TRANNO,
            CURRFROM,
            CURRTO,
            JLIFE,
            STATCODE,
            PSTATCODE,
            CRRCD,
            CRINST01,
            CRINST02,
            CRINST03,
            CRINST04,
            CRINST05,
            PRMCUR,
            CRTABLE,
            PCESDTE,
            RCESDTE,
            SUMINS,
            SICURR,
            MORTCLS,
            SINGP,
            INSTPREM,
            ZBINSTPREM,
            ZLINSTPREM,
            RRTDAT,
            RRTFRM,
            BBLDAT,
            CBANPR,
            CRDEBT,
            ANBCCD,
            SEX,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            STFUND,
            STSECT,
            STSSECT,
            LIENCD,
            RATCLS,
            INDXIN,
            BNUSIN,
            PAYRSEQNO,
            CPIDTE,
            VARSI,
            CBUNST,
            USRPRF,
            JOBNM,
            DATIME,
            STATREASN,
            REPTCD01,
            REPTCD02,
            REPTCD03,
            REPTCD04,
            REPTCD05,
            REPTCD06,
            BCESDTE,
            NXTDTE,
            RCESAGE,
            PCESAGE,
            BCESAGE,
            RCESTRM,
            PCESTRM,
            BCESTRM,
            DPCD,
            DPAMT,
            DPIND,
            TMBEN,
            EMV01,
            EMV02,
            EMVDTE01,
            EMVDTE02,
            EMVINT01,
            EMVINT02,
            CAMPAIGN,
            STSMIN,
            RTRNYRS,
            RSUNIN,
            RUNDTE,
            CHGOPT,
            FUNDSP,
            PCAMTH,
            PCADAY,
            PCTMTH,
            PCTDAY,
            RCAMTH,
            RCADAY,
            RCTMTH,
            RCTDAY,
            JLLSID,
            CBCVIN,
            CBRVPR,
            ICANDT,
            EXALDT,
            IINCDT,
            BAPPMETH,
			ZSTPDUTY01,
			ZCLSTATE

       FROM VM1DTA.COVRPF


GO