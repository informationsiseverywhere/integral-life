
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'AGSLPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE [VM1DTA].[AGSLPF]( 

	   [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
	   [CLNTNUM] [nchar](8) NULL,
       [AGNTNUM ] [nchar](8) NULL,
	   [AGTYPE] [nchar](2) NULL,
	   [TLAGLICNO] [nchar](15) NULL,
	   [AGNCYSALICDE] [nchar](8) NULL,
	   [FROMDATE] [int] NULL,
	   [TODATE] [int] NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar](10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   CONSTRAINT [PK_AGSLPF] PRIMARY KEY CLUSTERED 
	   (
		[UNIQUE_NUMBER] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)ON [PRIMARY]

GO