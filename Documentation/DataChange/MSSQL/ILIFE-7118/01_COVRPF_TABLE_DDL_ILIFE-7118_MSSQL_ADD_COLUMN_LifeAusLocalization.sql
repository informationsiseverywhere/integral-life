IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('TPDTYPE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVRPF'))
ALTER TABLE COVRPF ADD TPDTYPE VARCHAR(5);
