﻿IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'SEQUENCE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BNFYPF'))	
	ALTER TABLE VM1DTA.BNFYPF ADD SEQUENCE CHAR(2);
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PAYMMETH' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BNFYPF'))	
	ALTER TABLE VM1DTA.BNFYPF ADD PAYMMETH CHAR(1);
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'BANKKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BNFYPF'))	
	ALTER TABLE VM1DTA.BNFYPF ADD BANKKEY CHAR(10);
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'BANKACCKEY' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BNFYPF'))	
	ALTER TABLE VM1DTA.BNFYPF ADD BANKACCKEY CHAR(20);