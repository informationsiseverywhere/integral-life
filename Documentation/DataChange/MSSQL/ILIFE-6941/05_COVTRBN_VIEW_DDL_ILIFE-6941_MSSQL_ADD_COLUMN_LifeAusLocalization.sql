IF EXISTS ( SELECT * FROM sys.views where name = 'COVTRBN' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
                DROP VIEW [VM1DTA].COVTRBN
GO

CREATE VIEW [VM1DTA].[COVTRBN](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, SEQNBR, TERMID, TRDT, TRTM, USER_T, CRTABLE, RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, PCESTRM, SUMINS, SINGP, INSTPREM, MORTCLS, LIENCD, JLIFE, POLINC, NUMAPP, SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ, BILLCHNL, EFFDATE, ZBINSTPREM, ZLINSTPREM, PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE, BAPPMETH, ZDIVOPT, PAYCOY, PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR, FACTHOUS, ZSTPDUTY01, ZCLSTATE,LNKGNO,LNKGSUBREFNO, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            SEQNBR,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            CRTABLE,
            RCESDTE,
            PCESDTE,
            RCESAGE,
            PCESAGE,
            RCESTRM,
            PCESTRM,
            SUMINS,
            SINGP,
            INSTPREM,
            MORTCLS,
            LIENCD,
            JLIFE,
            POLINC,
            NUMAPP,
            SEX01,
            SEX02,
            ANBCCD01,
            ANBCCD02,
            BILLFREQ,
            BILLCHNL,
            EFFDATE,
            ZBINSTPREM,
            ZLINSTPREM,
            PAYRSEQNO,
            CNTCURR,
            BCESAGE,
            BCESTRM,
            BCESDTE,
            BAPPMETH,
            ZDIVOPT,
            PAYCOY,
            PAYCLT,
            PAYMTH,
            BANKKEY,
            BANKACCKEY,
            PAYCURR,
            FACTHOUS,
			ZSTPDUTY01,
			ZCLSTATE,
			LNKGNO,
			LNKGSUBREFNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.COVTPF


GO