IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('SINGPREMTYPE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVTPF'))
BEGIN
	ALTER TABLE COVTPF ADD SINGPREMTYPE NVARCHAR(3);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Single Premium Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'COVTPF', @level2type=N'COLUMN',@level2name=N'SINGPREMTYPE';
END
GO