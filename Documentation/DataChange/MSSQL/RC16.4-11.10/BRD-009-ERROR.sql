INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFW4'
           ,'Occupation Risk not insurable'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFW5'
           ,'Special Risk Occupation'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');