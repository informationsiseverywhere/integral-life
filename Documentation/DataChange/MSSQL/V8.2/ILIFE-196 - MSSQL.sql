DROP VIEW [VM1DTA].[BRUP]
GO

CREATE VIEW [VM1DTA].[BRUP](UNIQUE_NUMBER, CLNTCOY, CLNTNUM, BRUPDTE, DISCHDT, CRTUSER, DTECRT, LSTUPUSER, ZLSTUPDT, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            CLNTCOY,
            CLNTNUM,
            BRUPDTE,
            DISCHDT,
            CRTUSER,
            DTECRT,
            LSTUPUSER,
            ZLSTUPDT,
            JOBNM,
            USRPRF,
            DATIME
       FROM VM1DTA.BRUPPF
	   WHERE VALIDFLAG = '1';
GO

update VM1DTA.ITEMPF 
set ITEMPFX='IT', ITEMCOY='2', ITEMTABL='T1675', ITEMITEM='T6012465', ITEMSEQ='  ', TRANID='              ', TABLEPROG='P1675 ', VALIDFLAG='1', ITMFRM=0, ITMTO=0, GENAREA= cast('        UZAB           P2500P3100PR208PR22R                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ' AS varbinary(2000)), USRPRF='UNDERWR1  ', JOBNM='UNDERWR1', DATIME=GETDATE() 
WHERE 1=1  and ITEMPFX='IT' and ITEMCOY='2' and ITEMTABL='T1675' and ITEMITEM='T6012465' and ITEMSEQ='  ' AND  VALIDFLAG = '1';

GO

update VM1DTA.ITEMPF 
set ITEMPFX='IT', ITEMCOY='2', ITEMTABL='T1675', ITEMITEM='T6D02465', ITEMSEQ='  ', TRANID='              ', TABLEPROG='P1675 ', VALIDFLAG='1', ITMFRM=0, ITMTO=0, GENAREA= cast('        UZAB           P2500P3100PR208PR22R                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ' AS varbinary(2000)), USRPRF='UNDERWR1  ', JOBNM='UNDERWR1', DATIME=GETDATE() 
WHERE 1=1  and ITEMPFX='IT' and ITEMCOY='2' and ITEMTABL='T1675' and ITEMITEM='T6D02465' and ITEMSEQ='  ' AND  VALIDFLAG = '1';

GO

update VM1DTA.ITEMPF 
set ITEMPFX='IT', ITEMCOY='2', ITEMTABL='T1675', ITEMITEM='T6D52465', ITEMSEQ='  ', TRANID='              ', TABLEPROG='P1675 ', VALIDFLAG='1', ITMFRM=0, ITMTO=0, GENAREA= cast('        UZAB           P2500P3100PR208PR22R                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ' AS varbinary(2000)), USRPRF='UNDERWR1  ', JOBNM='UNDERWR1', DATIME=GETDATE() 
WHERE 1=1  and ITEMPFX='IT' and ITEMCOY='2' and ITEMTABL='T1675' and ITEMITEM='T6D52465' and ITEMSEQ='  ' AND  VALIDFLAG = '1';

GO

update VM1DTA.ITEMPF 
set ITEMPFX='IT', ITEMCOY='2', ITEMTABL='T1675', ITEMITEM='T6032465', ITEMSEQ='  ', TRANID='              ', TABLEPROG='P1675 ', VALIDFLAG='1', ITMFRM=0, ITMTO=0, GENAREA= cast('        UZAB           P2500P3100PR208PR22R                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ' AS varbinary(2000)), USRPRF='UNDERWR1  ', JOBNM='UNDERWR1', DATIME=GETDATE() 
WHERE 1=1  and ITEMPFX='IT' and ITEMCOY='2' and ITEMTABL='T1675' and ITEMITEM='T6032465' and ITEMSEQ='  ' AND  VALIDFLAG = '1';

GO

update VM1DTA.ITEMPF 
set ITEMPFX='IT', ITEMCOY='2', ITEMTABL='T1675', ITEMITEM='T6022465', ITEMSEQ='  ', TRANID='              ', TABLEPROG='P1675 ', VALIDFLAG='1', ITMFRM=0, ITMTO=0, GENAREA= cast('        UZAB           P2500P3100PR208PR22R                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ' AS varbinary(2000)), USRPRF='UNDERWR1  ', JOBNM='UNDERWR1', DATIME=GETDATE() 
WHERE 1=1  and ITEMPFX='IT' and ITEMCOY='2' and ITEMTABL='T1675' and ITEMITEM='T6022465' and ITEMSEQ='  ' AND  VALIDFLAG = '1';

GO	
