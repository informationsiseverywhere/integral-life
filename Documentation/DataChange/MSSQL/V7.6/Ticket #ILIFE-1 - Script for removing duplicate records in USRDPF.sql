BEGIN TRANSACTION;
	-- delete duplicated users
	DELETE FROM VM1DTA.USRDPF
	WHERE USERNUM NOT IN
						(
							SELECT MIN(USERNUM)
							FROM VM1DTA.USRDPF
							GROUP BY USERID
						)

COMMIT TRANSACTION;