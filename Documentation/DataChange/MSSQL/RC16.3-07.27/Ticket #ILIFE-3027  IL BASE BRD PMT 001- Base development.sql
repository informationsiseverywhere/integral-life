create table VM1DTA.ZHLBPF([UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
CHDRCOY varchar(2),
CHDRNUM varchar(8),
BNYCLT varchar(8),
BNYPC decimal(5,2),
VALIDFLAG varchar(1),
TRANNO int,
TRDT int,
TRTM int,
USR int,
ACTVALUE decimal(17,2),
ZCLMADJST decimal(17,2),
ZHLDCLMV decimal(17,2),
ZHLDCLMA decimal(17,2),
USRPRF varchar(10),
JOBNM varchar(10),
DATIME datetime2(7) NULL,
PRIMARY KEY(UNIQUE_NUMBER)
);

CREATE VIEW "VM1DTA"."ZHLB" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "BNYCLT","BNYPC","VALIDFLAG", "TRANNO", "TRDT", "TRTM", "USR", "ACTVALUE", "ZCLMADJST", "ZHLDCLMV", "ZHLDCLMA","USRPRF", "JOBNM", "DATIME" 
) AS 
SELECT TOP 100 PERCENT 
UNIQUE_NUMBER,CHDRCOY,CHDRNUM,BNYCLT,BNYPC,VALIDFLAG,TRANNO,TRDT,TRTM,USR,ACTVALUE,ZCLMADJST,ZHLDCLMV,ZHLDCLMA,USRPRF,JOBNM,DATIME

       FROM ZHLBPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            BNYCLT ;
            
               create table VM1DTA.ZHLCPF([UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL ,
CHDRCOY varchar(2),
CHDRNUM varchar(8),
LIFE varchar(2),
JLIFE varchar(2),
COVERAGE varchar(2),
RIDER  varchar(2),
CRTABLE  varchar(4),
VALIDFLAG varchar(1),
TRANNO int,
TRDT int,
TRTM int,
USR int,
ACTVALUE decimal(17,2),
ZCLMADJST decimal(17,2),
ZHLDCLMV decimal(17,2),
ZHLDCLMA decimal(17,2),
USRPRF varchar(10),
JOBNM varchar(10),
DATIME datetime2(7) NULL,
PRIMARY KEY(UNIQUE_NUMBER)
);         
             CREATE VIEW "VM1DTA"."ZHLC" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "LIFE","JLIFE", "COVERAGE", "RIDER", "CRTABLE", "VALIDFLAG", "TRANNO", "TRDT", "TRTM", "USR", "ACTVALUE", "ZCLMADJST", "ZHLDCLMV", "ZHLDCLMA","USRPRF", "JOBNM", "DATIME" 
) AS 
SELECT TOP 100 PERCENT 
UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,VALIDFLAG,TRANNO,TRDT,TRTM,USR,ACTVALUE,ZCLMADJST,ZHLDCLMV,ZHLDCLMA,USRPRF,JOBNM,DATIME

       FROM ZHLCPF
   ORDER BY CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
        
           RIDER ; 






         
         
         
         
            
           
           
           
           
           

           
  INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTB'
           ,'Claim Hold Already Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');           
           
   INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTC'
           ,'Coverage Hold Does Not Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTD'
           ,'Beneficiary Hold Does Not Exist'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTE'
           ,'Hold Claim Amount should not  be > Claim amount'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTF'
           ,'Hold Adjustment should not be > Total Adjustment'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTG'
           ,'Release Claim Amount should not be > Hold Claim Amount'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');  
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTH'
           ,'Release Adjustment Amount should not be > Hold Adjustment'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');        
               
               INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
            VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFTS'
           ,'Claim is on Hold Status'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
               ,'');    