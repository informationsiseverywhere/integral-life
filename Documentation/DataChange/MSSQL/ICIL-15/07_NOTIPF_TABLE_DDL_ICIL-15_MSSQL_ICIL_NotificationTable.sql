IF EXISTS (SELECT * FROM sys.columns WHERE name in('CAUSEDATE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
BEGIN
	EXECUTE SP_RENAME 'NOTIPF.CAUSEDATE','CAUSEDEATH';
	 ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN CAUSEDEATH VARCHAR(30);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Death Cause' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOTIPF', @level2type=N'COLUMN',@level2name=N'CAUSEDEATH';
END
GO
