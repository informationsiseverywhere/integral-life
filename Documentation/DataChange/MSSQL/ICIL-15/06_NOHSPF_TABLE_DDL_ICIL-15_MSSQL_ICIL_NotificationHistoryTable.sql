
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'NOHSPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE VM1DTA.NOHSPF( 
		   UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL ,
           NOTIHYCOY char(1) NULL,
           NOTIHSNUM char(14) NULL,
		   TRANSNO numeric(18,0) NULL,
		   TRANSCODE char(14) NULL,
		   TRANSDATE numeric(8,0) NULL,
		   TRANSDESC nchar(1000) NULL,
		   EFFECTDATE numeric(8,0) NULL,
		   USRPRF char(14) NULL,
		   JOBNM char(10) NULL,
		   DATIME datetime2(6) NULL,  
	   CONSTRAINT PK_NOHSPF PRIMARY KEY CLUSTERED 
	   (
		UNIQUE_NUMBER ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)ON [PRIMARY]
	)ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NOTIFICATION HOSTORY TABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONT HEADER COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'NOTIHYCOY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Notification Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'NOTIHSNUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'TRANSNO';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'TRANSCODE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'TRANSDATE';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Description' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'TRANSDESC';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOHSPF', @level2type=N'COLUMN',@level2name=N'EFFECTDATE';
	
GO
