
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'INVSPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	CREATE TABLE VM1DTA.INVSPF( 
		   UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL ,
           INVSCOY char(1) NULL,
           NOTIFINUM char(14) NULL,
		   LIFENUM char(8) NULL,
		   CLAIMANT char(8) NULL,
		   RELATIONSHIP nchar(50) NULL,
		   INVESRESULT nchar(1000) NULL,
		   USRPRF char(14) NULL,
		   JOBNM char(10) NULL,
		   DATIME datetime2(6) NULL,  
	   CONSTRAINT PK_INVSPF PRIMARY KEY CLUSTERED 
	   (
		UNIQUE_NUMBER ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)ON [PRIMARY]
	)ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INVESTIGATION RESULTS TABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONT HEADER COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'INVSCOY';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Notification Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'NOTIFINUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'LIFENUM';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claimant' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'CLAIMANT';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relationship to Life Assured' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'RELATIONSHIP';
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Investigation Results' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'INVESRESULT';
	
GO
