
 BEGIN TRANSACTION

IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='TR2A5' and DESCITEM='SUOTR007' and LANGUAGE = 'E')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUOTR007','  ','E',' ','SUOTR007','Surrenders','UNDERWR1  ','UNDERWR1',current_timestamp);

 GO 
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='TR2A5' and DESCITEM='SUOTR007' and LANGUAGE = 'S')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUOTR007','  ','S',' ','SUOTR007','Surrenders','UNDERWR1  ','UNDERWR1',current_timestamp);
 GO 


 COMMIT TRANSACTION

  