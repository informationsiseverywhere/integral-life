ALTER TABLE IJNLPF ADD ZCURPRMBAL NUMERIC(17,2);


ALTER VIEW [VM1DTA].[IJNL](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, SEQNO, ZINTBFND, ZRECTYP, PRCSEQ, TRANNO, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, CRTABLE, CNTCURR, FDBKIND, INCINUM, INCIPERD01, INCIPERD02, INCIPRM01, INCIPRM02, CNTAMNT, FNDCURR, FUNDAMNT, FUNDRATE, SACSCODE, SACSTYPE, GENLCDE, CONTYP, TRIGER, TRIGKY, SVP, PERSUR, SWCHIND, EFFDATE, JOBNM, USRPRF, DATIME, ZCURPRMBAL) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            SEQNO,
            ZINTBFND,
            ZRECTYP,
            PRCSEQ,
            TRANNO,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            CRTABLE,
            CNTCURR,
            FDBKIND,
            INCINUM,
            INCIPERD01,
            INCIPERD02,
            INCIPRM01,
            INCIPRM02,
            CNTAMNT,
            FNDCURR,
            FUNDAMNT,
            FUNDRATE,
            SACSCODE,
            SACSTYPE,
            GENLCDE,
            CONTYP,
            TRIGER,
            TRIGKY,
            SVP,
            PERSUR,
            SWCHIND,
            EFFDATE,
            JOBNM,
            USRPRF,
            DATIME,
			ZCURPRMBAL
       FROM VM1DTA.IJNLPF

GO


