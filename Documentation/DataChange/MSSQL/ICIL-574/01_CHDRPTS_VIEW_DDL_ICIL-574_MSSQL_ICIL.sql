IF EXISTS ( SELECT * FROM sys.views where name = 'CHDRPTS' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].[CHDRPTS]
GO
CREATE VIEW [VM1DTA].[CHDRPTS] (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SERVUNIT, CNTTYPE, POLINC, POLSUM, NXTSFX, TRANNO, TRANID, 
 VALIDFLAG, CURRFROM, CURRTO, AVLISU, STATCODE, STATDATE, STATTRAN, PSTCDE, PSTDAT, PSTTRN,
 TRANLUSED, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, JOWNNUM, CNTBRANCH, CNTCURR, PAYPLAN,
 ACCTMETH, BILLFREQ, BILLCHNL, COLLCHNL, BILLDAY, BILLMONTH, BILLCD, BTDATE, PTDATE, JOBNM, USRPRF, 
 DATIME, NLGFLG, REQNTYPE, PAYCLT, ZMANDREF, BANKKEY, BANKACCKEY) AS 
SELECT   UNIQUE_NUMBER, CHDRCOY, CHDRNUM, SERVUNIT, CNTTYPE, POLINC, POLSUM, NXTSFX, TRANNO, TRANID, 
                VALIDFLAG, CURRFROM, CURRTO, AVLISU, STATCODE, STATDATE, STATTRAN, PSTCDE, PSTDAT, PSTTRN, 
                TRANLUSED, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, JOWNNUM, CNTBRANCH, CNTCURR, PAYPLAN, 
                ACCTMETH, BILLFREQ, BILLCHNL, COLLCHNL, BILLDAY, BILLMONTH, BILLCD, BTDATE, PTDATE, JOBNM, USRPRF, 
                DATIME, NLGFLG, REQNTYPE, PAYCLT, ZMANDREF, BANKKEY, BANKACCKEY
FROM      VM1DTA.CHDRPF
WHERE   (SERVUNIT = 'LP')

