GO
IF EXISTS(SELECT * FROM VM1DTA.BPSRPF WHERE BPRIORPROC IN ('B5469T','B5469F') AND  BPRIORCOY='7')
BEGIN
	DELETE FROM VM1DTA.BPSRPF WHERE BPRIORPROC IN ('B5469T','B5469F') AND  BPRIORCOY='7';
  
	INSERT INTO VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) VALUES ('7','B5469T    ','7','B5469F   ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
	END
GO
IF NOT EXISTS(SELECT * FROM VM1DTA.BPSRPF WHERE BPRIORPROC IN ('B5469T','B5469F') AND  BPRIORCOY='7')
BEGIN
  INSERT INTO VM1DTA.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,JOBNM,USRPRF,DATIME) VALUES ('7','B5469T    ','7','B5469F   ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
	END
GO