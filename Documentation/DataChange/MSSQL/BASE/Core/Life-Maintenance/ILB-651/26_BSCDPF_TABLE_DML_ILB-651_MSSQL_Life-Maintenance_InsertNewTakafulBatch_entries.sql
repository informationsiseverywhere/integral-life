GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B540' AND PRODCODE='L' AND BSCHEDNAM = 'LKRACOST')
    DELETE FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B540' AND PRODCODE='L' AND BSCHEDNAM = 'LKRACOST'; 
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) VALUES ('LKRACOST',1,'N','N','B540','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N');
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B540' AND PRODCODE='L' AND BSCHEDNAM = 'LKRACOST')
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) VALUES ('LKRACOST',1,'N','N','B540','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N');
GO