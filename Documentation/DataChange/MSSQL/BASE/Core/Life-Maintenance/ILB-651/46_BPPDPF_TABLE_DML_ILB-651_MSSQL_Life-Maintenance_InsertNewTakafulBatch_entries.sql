GO
IF EXISTS(SELECT * FROM VM1DTA.BPPDPF WHERE BSCHEDNAM = 'LKANNIVPRC' AND COMPANY='7')
	DELETE FROM VM1DTA.BPPDPF WHERE BSCHEDNAM = 'LKANNIVPRC' AND COMPANY='7';
	INSERT INTO VM1DTA.BPPDPF (BSCHEDNAM,BPARPSEQNO,COMPANY,BPARPPROG,USRPRF,JOBNM,DATIME) VALUES ('LKANNIVPRC',1,'7','     ','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
GO
IF NOT EXISTS(SELECT * FROM VM1DTA.BPPDPF WHERE BSCHEDNAM = 'LKANNIVPRC' AND COMPANY='7')
	INSERT INTO VM1DTA.BPPDPF (BSCHEDNAM,BPARPSEQNO,COMPANY,BPARPPROG,USRPRF,JOBNM,DATIME) VALUES ('LKANNIVPRC',1,'7','     ','UNDERWR1','TAKAFULCOM',CURRENT_TIMESTAMP);
GO