GO
IF EXISTS(SELECT * FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'LKRACOST' AND COMPANY='7')
	DELETE FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'LKRACOST' AND COMPANY='7';
	INSERT INTO VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) VALUES ('LKRACOST','7','B5456     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO
IF NOT EXISTS(SELECT * FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'LKRACOST' AND COMPANY='7')
	INSERT INTO VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) VALUES ('LKRACOST','7','B5456     ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO