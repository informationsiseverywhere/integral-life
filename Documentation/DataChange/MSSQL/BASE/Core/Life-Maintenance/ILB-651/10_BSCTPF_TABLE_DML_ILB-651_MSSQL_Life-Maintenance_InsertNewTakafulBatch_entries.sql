GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAGORSPAY')
    DELETE FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAGORSPAY';
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKAGORSPAY','Agent Statement/Payment - New OR Structure        ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAGORSPAY')
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKAGORSPAY','Agent Statement/Payment - New OR Structure        ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO