GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSUSPREP')
    DELETE FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSUSPREP';
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKSUSPREP','Suspense Reporting                                ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSUSPREP')
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKSUSPREP','Suspense Reporting                                ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
GO