	
	SET QUOTED_IDENTIFIER ON  
	GO
	
	DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRSV';
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RRSV')
	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER',' ','E','          ','RRSV', 'Client to provide evidence','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	
	GO
	SET QUOTED_IDENTIFIER OFF  
	GO
	
	
	SET QUOTED_IDENTIFIER ON  
	GO
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S' AND h.[EROREROR] = 'RRSV')
	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER',' ','S','          ','RRSV', 'Client to provide evidence','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	
	GO
	SET QUOTED_IDENTIFIER OFF  
	GO




	