SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RRSN')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) VALUES ('ER',' ','E','          ','RRSN  ', 'Invalid Notification Number','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S' AND h.[EROREROR] = 'RRSN')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) VALUES ('ER',' ','S','          ','RRSN  ', N'无效报案号码','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

SET QUOTED_IDENTIFIER OFF
GO