
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRBN';
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAW';
delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAX';

insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER','','E','','RRBN  ','Must enter Self',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER','','E','','RRAW  ','Must be greater than Birth Date',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);
insert into VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME) values('ER','','E','','RRAX  ','Not defined in TD5G2',0,0,'000036',null,'UNDERWR1','CHINALOC',current_timestamp);

IF EXISTS (	SELECT * FROM VM1DTA.ERORPF WHERE ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAV') 
	delete from VM1DTA.ERORPF where ERORPFX='ER' and ERORLANG='E' and EROREROR='RRAV';
GO