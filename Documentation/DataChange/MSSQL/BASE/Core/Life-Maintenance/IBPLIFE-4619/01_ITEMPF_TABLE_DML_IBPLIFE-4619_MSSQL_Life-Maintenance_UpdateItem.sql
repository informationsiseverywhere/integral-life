SET QUOTED_IDENTIFIER ON  
GO
	IF EXISTS (SELECT * FROM VM1DTA.ITEMPF WHERE ITEMPFX='IT' AND ITEMCOY='2' AND ITEMTABL='T1675' AND ITEMITEM='T329A84S')
		
		UPDATE VM1DTA.ITEMPF SET VALIDFLAG ='2' where  ITEMTABL = 'T1675' AND ITEMITEM='T329A84S' AND ITEMCOY='2';

GO

	IF EXISTS (SELECT * FROM VM1DTA.ITEMPF WHERE ITEMPFX='IT' AND ITEMCOY='2' AND ITEMTABL='T1675' AND ITEMITEM='T331A84S')
	
		UPDATE VM1DTA.ITEMPF SET VALIDFLAG ='2' where  ITEMTABL = 'T1675' AND ITEMITEM='T331A84S' AND ITEMCOY='2';
GO

	
SET QUOTED_IDENTIFIER OFF
GO
