IF EXISTS (SELECT * FROM [VM1DTA].BPPDPF WHERE [BSCHEDNAM] = 'L2RETBNK77' and [COMPANY]='2' ) 
	delete from vm1dta.BPPDPF where bschednam = 'L2RETBNK77' and COMPANY='2'
GO


IF NOT EXISTS (SELECT * FROM [VM1DTA].BPPDPF WHERE [BSCHEDNAM] = 'L2RETBNK77' and [COMPANY]='2') 
	insert into VM1DTA.BPPDPF (BSCHEDNAM,BPARPSEQNO,COMPANY,BPARPPROG,USRPRF,JOBNM,DATIME) values ('L2RETBNK77',1,'2','PR21Z','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO