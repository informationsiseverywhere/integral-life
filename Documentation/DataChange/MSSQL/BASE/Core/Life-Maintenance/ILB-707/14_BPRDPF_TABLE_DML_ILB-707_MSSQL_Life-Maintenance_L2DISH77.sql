
IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-BKDF-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
values ('2','F-BKDF-77','00:00:00','23:59:59','CRTTMPF','                                                                                                                                                                                                                                                ',1,1,'  ','        ','B350',' ','10',50,50,5000,'BKDFPF','          ','          ','L2        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-CCDF-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
values ('2','F-CCDF-77','00:00:00','23:59:59','CRTTMPF','                                                                                                                                                                                                                                                ',1,1,'  ','        ','B350',' ','10',50,50,5000,'CCDFPF','          ','          ','L2        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-BR22D-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
values ('2','F-BR22D-77','00:00:00','23:59:59','BR22D','                                                                                                                                                                                                                                                ',1,1,'P8404','        ','B350',' ','10',50,50,5000,'  ','77','          ','L2        ','          ',10,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-SDDC-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
 values ('2','F-SDDC-77','00:00:00','23:59:59','CRTTMPF','                                                                                                                                                                                                                                                ',1,1,'  ','        ','B350',' ','10',50,50,5000,'SDDCPF','          ','          ','L2        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-BR22C-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
values ('2','F-BR22C-77','00:00:00','23:59:59','BR22C','                                                                                                                                                                                                                                                ',1,1,'P8404','        ','B350',' ','10',50,50,5000,'  ','77','          ','L2        ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='F-B8404-77' AND COMPANY='2')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[JOBNM],[USRPRF],[DATIME]) 
values ('2','F-B8404-77','00:00:00','23:59:59','B8404','                                                                                                                                                                                                                                                ',1,1,'P8404','        ','B350',' ','10',50,50,5000,'  ','77','          ','        ','          ',10,'*DATABASE ','3','1',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO





