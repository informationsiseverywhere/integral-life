SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.descpf where DESCTABL='THSUZ' AND DESCCOY='2' AND descitem='T510' ) 
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
VALUES('IT','2','THSUZ','T510','  ','E','T510','Part Surrender','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP)

IF NOT EXISTS (SELECT * FROM VM1DTA.descpf where DESCTABL='TR4BZ' AND DESCCOY='2' AND descitem='T510' ) 
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
VALUES('IT','2','TR4BZ','T510','  ','E','PS','Part Surrender','ASHARMA228','ASHARMA228',CURRENT_TIMESTAMP)

GO
SET QUOTED_IDENTIFIER OFF 
GO

