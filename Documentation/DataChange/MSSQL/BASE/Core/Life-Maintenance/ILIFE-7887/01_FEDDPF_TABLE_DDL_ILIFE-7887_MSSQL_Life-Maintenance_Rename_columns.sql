IF EXISTS (SELECT * FROM sys.columns WHERE name = 'STAT_CHANGE_DAT' and object_id in (SELECT object_id FROM sys.tables WHERE name = 'FEDDPF'))
		BEGIN
	EXEC sp_RENAME 'VM1DTA.FEDDPF.STAT_CHANGE_DAT', 'STATUSCHANGEDAT', 'COLUMN';
	    END                                   
		


IF EXISTS (SELECT * FROM sys.columns WHERE name = 'USER_PROFILE' and object_id in (SELECT object_id FROM sys.tables WHERE name = 'FEDDPF'))
		BEGIN
	EXEC sp_RENAME 'VM1DTA.FEDDPF.USER_PROFILE', 'USRPRF', 'COLUMN';
		END
	



IF EXISTS (SELECT * FROM sys.columns WHERE name = 'JOBNAME' and object_id in (SELECT object_id FROM sys.tables WHERE name = 'FEDDPF'))
		BEGIN
	EXEC sp_RENAME 'VM1DTA.FEDDPF.JOBNAME', 'JOBNM', 'COLUMN';
		END
	
		
		