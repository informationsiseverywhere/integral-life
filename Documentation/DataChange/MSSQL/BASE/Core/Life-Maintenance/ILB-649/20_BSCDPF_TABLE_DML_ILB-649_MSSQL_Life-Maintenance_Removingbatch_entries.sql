SET QUOTED_IDENTIFIER ON  
GO

IF  EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BSCHEDNAM = 'L2ARCALLM' AND PRODCODE='L')
    DELETE FROM VM1DTA.BSCDPF WHERE BSCHEDNAM = 'L2ARCALLM' AND PRODCODE='L';
GO

SET QUOTED_IDENTIFIER OFF 
GO