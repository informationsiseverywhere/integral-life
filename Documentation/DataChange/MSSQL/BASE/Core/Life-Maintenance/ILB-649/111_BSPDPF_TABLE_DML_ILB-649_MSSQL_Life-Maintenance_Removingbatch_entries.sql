SET QUOTED_IDENTIFIER ON  
GO

IF  EXISTS (SELECT * FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2INITLINS' AND COMPANY='2')
    DELETE FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2INITLINS' AND COMPANY='2';
GO

SET QUOTED_IDENTIFIER OFF 
GO