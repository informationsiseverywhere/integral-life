IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COVERAGE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVPPF'))
	BEGIN
	ALTER TABLE VM1DTA.COVPPF ADD COVERAGE NCHAR(2) NULL ;
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.COVERAGE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
	
	END
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RIDER' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVPPF'))
	BEGIN
	ALTER TABLE VM1DTA.COVPPF ADD RIDER NCHAR(2) NULL;
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.RIDER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'RIDER';
	
	END
	