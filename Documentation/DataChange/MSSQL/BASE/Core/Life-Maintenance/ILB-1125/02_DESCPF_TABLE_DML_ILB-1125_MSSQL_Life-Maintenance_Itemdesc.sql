
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TH528' AND DESCITEM='CCI1****' AND LANGUAGE='E')
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
VALUES ('IT','2','TH528','CCI1****','','E','','CCI1****','China Critical Illness','UNDERWR1 ','UNDERWR1 ',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TH528' AND DESCITEM='CDA1****' AND LANGUAGE='E')
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
VALUES ('IT','2','TH528','CDA1****','','E','','CDA1****','China Deferred Annuity Vested','UNDERWR1 ','UNDERWR1 ',CURRENT_TIMESTAMP);
GO

SET QUOTED_IDENTIFIER OFF  
GO