
DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRBU';


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E'  AND h.[EROREROR] = 'RRBU')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','RRBU  ', 'Bank outlet not found','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO
	
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S' AND h.[EROREROR] = 'RRBU')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','S','','RRBU  ', N'银行网点','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO





DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRB0';
	
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E'  AND h.[EROREROR] = 'RRB0')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','RRB0  ', 'Bank Oult. to be min 14 digits','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S'  AND h.[EROREROR] = 'RRB0')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','S','','RRB0  ', N'银行网点最少是14位','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO

	

DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRDO';
	
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RRDO')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','RRDO  ', 'Bank Teller not found','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S'  AND h.[EROREROR] = 'RRDO')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','S','','RRDO  ', N'银行柜员没找到','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO




DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRDP';
	
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RRDP')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','RRDP  ', 'Bank Teller To be min 10 digits','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S'  AND h.[EROREROR] = 'RRDP')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','S','','RRDP  ', N'银行柜员最少要10个位数','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO







