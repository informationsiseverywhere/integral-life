
SET QUOTED_IDENTIFIER ON  
GO
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'NotifiDate' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))  
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN NotifiDate varchar(8);
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN NotifiDate int;
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'Incurdate' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))         
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN Incurdate varchar(8);
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN Incurdate int;
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'deathDate' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))         
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN deathDate varchar(8);
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN deathDate int;		
				
				
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'admintdate' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))         
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN admintdate varchar(8);
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN admintdate int;
					
IF  EXISTS (SELECT * FROM sys.columns WHERE name = 'dischargedate' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))         
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN dischargedate varchar(8);
					ALTER TABLE VM1DTA.NOTIPF ALTER COLUMN dischargedate int;
					
GO
SET QUOTED_IDENTIFIER OFF  
GO