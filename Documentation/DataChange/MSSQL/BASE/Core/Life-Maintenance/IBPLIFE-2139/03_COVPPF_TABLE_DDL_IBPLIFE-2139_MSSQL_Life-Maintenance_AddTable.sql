IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'COVPPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
create TABLE [VM1DTA].[COVPPF](
       [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
       [CHDRPFX] [nchar](2) NULL,
	   [CHDRCOY] [nchar](1) NULL,
	   [CHDRNUM] [nchar](8) NULL,
       [VALIDFLAG] [nchar](1) NULL,
	   [EFFDATE] [int] NULL,
	   [TRCODE] [nchar](4) NULL,
	   [COVRPRPSE] [nchar](100) NULL,
	   [CRTABLE] [nchar](4) NULL,
       [USRPRF] [nchar](10) NULL,
       [JOBNM] [nchar](10) NULL,
       [DATIME] [datetime2](6) NULL,
	   
	   CONSTRAINT [PK_COVPPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COVPPF Table file' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.CHDRPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'CHDRPFX'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.CHDRCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.COVPPF.CHDRNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'EFFDATE'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'TRCODE'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COVERAGE PURPOSE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'COVRPRPSE'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CRTABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'CRTABLE'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER PROFILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'USRPRF';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOB NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'JOBNM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TIMESTAMP' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVPPF', @level2type=N'COLUMN',@level2name=N'DATIME';

END
GO