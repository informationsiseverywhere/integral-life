
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'THS04' AND DESCITEM='***TA7A' AND LANGUAGE='E' )
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','THS04','***TA7A','  ','E','','***TA7A','SMS Notification for TA7A','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF  
GO