IF EXISTS ( SELECT * FROM sys.tables where name = 'BD5HUDATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE [VM1DTA].[BD5HUDATA]
GO

IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BD5HUDATA' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
CREATE TABLE [VM1DTA].[BD5HUDATA]
  (
    [BATCHID]       [bigint],
	[CNT] [int],
    [UNIQUE_NUMBER] [bigint] NOT NULL,
    [CHDRCOY]       [nchar](1),
    [CHDRNUM]       [nchar](8),
    [PLNSFX]        [int],
    [LIFE]          [nchar](2),
    [COVERAGE]      [nchar](2),
    [RIDER]         [nchar](2),    
	[CURRCD]        [nchar](3),
    [VALIDFLAG]     [nchar](1),
    [RPTRANNO]        [int],   
	[APCAPLAMT]    [numeric](17, 2),
	[APINTAMT]     [numeric](17, 2),
	[APLSTCAPDATE]	[int], 
	[APNXTCAPDATE]	[int], 
	[APLSTINTBDTE]	[int], 
	[APNXTINTBDTE]	[int],
    [USRPRF]           [nchar](10),
    [JOBNM]            [nchar](10),
    [CH_UNIQUE_NUMBER] [bigint] NOT NULL,
    [CURRFROM]         [int],
    [TRANNO]           [int],
    [CNTTYPE]          [nchar](3),
    [CHDRPFX]          [nchar](2),
    [SERVUNIT]         [nchar](2),
    [OCCDATE]          [int],
    [CCDATE]           [int],
    [COWNPFX]          [nchar](2),
    [COWNCOY]          [nchar](1),
    [COWNNUM]          [nchar](8),
    [COLLCHNL]         [nchar](2),
    [CNTBRANCH]        [nchar](2),
    [AGNTPFX]          [nchar](2),
    [AGNTCOY]          [nchar](1),
    [AGNTNUM]          [nchar](8),
    [DATIME] [datetime2](6) ) ON [PRIMARY]

GO


exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BD5HUDATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA'
go

exec sp_addextendedproperty 'MS_Description', 'BATCH ID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'BATCHID'
go

exec sp_addextendedproperty 'MS_Description', 'CNT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CNT'
go

exec sp_addextendedproperty 'MS_Description', 'UNIQUE NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go

exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CHDRCOY'
go

exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CHDRNUM'
go

exec sp_addextendedproperty 'MS_Description', 'PLNSFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'PLNSFX'
go

exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'LIFE'
go

exec sp_addextendedproperty 'MS_Description', 'COVERAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'COVERAGE'
go

exec sp_addextendedproperty 'MS_Description', 'RIDER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'RIDER'
go

exec sp_addextendedproperty 'MS_Description', 'CURRCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CURRCD'
go

exec sp_addextendedproperty 'MS_Description', 'VALIDFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'VALIDFLAG'
go

exec sp_addextendedproperty 'MS_Description', 'RPTRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'RPTRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'APCAPLAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APCAPLAMT'
go

exec sp_addextendedproperty 'MS_Description', 'APINTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APINTAMT'
go

exec sp_addextendedproperty 'MS_Description', 'APLSTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APLSTCAPDATE'
go


exec sp_addextendedproperty 'MS_Description', 'APNXTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APNXTCAPDATE'
go
exec sp_addextendedproperty 'MS_Description', 'APLSTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APLSTINTBDTE'
go
exec sp_addextendedproperty 'MS_Description', 'APNXTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'APNXTINTBDTE'
go
exec sp_addextendedproperty 'MS_Description', 'USRPRF', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'USRPRF'
go

exec sp_addextendedproperty 'MS_Description', 'JOBNM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'JOBNM'
go
exec sp_addextendedproperty 'MS_Description', 'CH_UNIQUE_NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CH_UNIQUE_NUMBER'
go
exec sp_addextendedproperty 'MS_Description', 'CURRFROM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CURRFROM'
go
exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CHDRPFX'
go

exec sp_addextendedproperty 'MS_Description', 'SERVUNIT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'SERVUNIT'
go

exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'OCCDATE'
go

exec sp_addextendedproperty 'MS_Description', 'CCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CCDATE'
go

exec sp_addextendedproperty 'MS_Description', 'COWNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'COWNPFX'
go

exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'COWNCOY'
go

exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'COWNNUM'
go
    
exec sp_addextendedproperty 'MS_Description', 'COLLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'COLLCHNL'
go	
 
exec sp_addextendedproperty 'MS_Description', 'CNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'CNTBRANCH'
go	
exec sp_addextendedproperty 'MS_Description', 'AGNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'AGNTPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'AGNTCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'AGNTNUM'
go    
exec sp_addextendedproperty 'MS_Description', 'DATIME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HUDATA', 'COLUMN',
                            'DATIME'
go    