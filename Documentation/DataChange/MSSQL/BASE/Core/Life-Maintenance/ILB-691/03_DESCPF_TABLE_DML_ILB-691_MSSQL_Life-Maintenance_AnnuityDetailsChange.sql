
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1675' AND DESCITEM='TAFTD5HF' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1675','TAFTD5HF','  ','E',' ','TAFTD5HF  ','TAFTD5HF                      ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1688' AND DESCITEM='TAFT' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAFT    ','  ','E',' ','Life      ','Annuity Details Change        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T5679' AND DESCITEM='TAFT' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5679','TAFT    ','  ','E',' ','Annuity De','Annuity Details Change        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T6694' AND DESCITEM='A' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T6694','A       ','  ','E',' ','A         ','Accumulate Interest           ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T3695' AND DESCITEM='AN' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T3695','AN      ','  ','E',' ','AN        ','Annuity Interest              ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCPFX='HE' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('HE','2','TD5H7','        ','  ','E','              ','          ','Annuity Interest Rules        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCITEM='CDA' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5H7','CDA     ','  ','E',' ','CDA       ','China Deferred Annuity        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCITEM='***' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5H7','***     ','  ','E',' ','Default','Default','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1690' AND DESCITEM='P5229' AND DESCCOY='2' AND LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1690','P5229   ','  ','E',null,'Life      ','Vesting                       ','UNDERWR1  ','LVSTPCGPAR',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1675' AND DESCITEM='TAFTD5HF' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1675','TAFTD5HF','  ','S',' ','TAFTD5HF  ','TAFTD5HF                      ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1688' AND DESCITEM='TAFT' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1688','TAFT    ','  ','S',' ','Life      ','Annuity Details Change        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T5679' AND DESCITEM='TAFT' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T5679','TAFT    ','  ','S',' ','Annuity De','Annuity Details Change        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T6694' AND DESCITEM='A' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T6694','A       ','  ','S',' ','A         ','Accumulate Interest           ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T3695' AND DESCITEM='AN' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T3695','AN      ','  ','S',' ','AN        ','Annuity Interest              ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCPFX='HE' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('HE','2','TD5H7','        ','  ','S','              ','          ','Annuity Interest Rules        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCITEM='CDA' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5H7','CDA     ','  ','S',' ','CDA       ','China Deferred Annuity        ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='TD5H7' AND DESCITEM='***' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','TD5H7','***     ','  ','S',' ','Default','Default','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP);
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL='T1690' AND DESCITEM='P5229' AND DESCCOY='2' AND LANGUAGE='S') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values 
('IT','2','T1690','P5229   ','  ','S',null,'Life      ','Vesting                       ','UNDERWR1  ','LVSTPCGPAR',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='TR2A5' and DESCITEM='SUOTR006' and DESCCOY='2' and LANGUAGE = 'E')
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUOTR006','  ','E',' ','BSD-POS024','2CH Change of Annuity Method','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
 
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='TR2A5' and DESCITEM='SUOTR006' and DESCCOY='2' and LANGUAGE = 'S')
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','SUOTR006','  ','S',' ','BSD-POS024','2CH Change of Annuity Method','UNDERWR1  ','UNDERWR1',current_timestamp);
GO 

SET QUOTED_IDENTIFIER OFF  
GO

