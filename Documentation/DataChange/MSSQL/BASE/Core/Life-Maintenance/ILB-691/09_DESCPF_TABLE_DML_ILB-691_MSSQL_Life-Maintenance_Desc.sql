
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T5645' AND DESCITEM='APINTCAL') 
  
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','APINTCAL','  ','E','','APINTCAL','Annuity interest calculation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','APINTCAL','  ','S','','APINTCAL','Annuity interest calculation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO



IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T5645' AND DESCITEM='BD5HU') 
  
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','BD5HU','  ','E','','BD5HU','Annutity interest Billing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','BD5HU','  ','S','','BD5HU','Annutity interest Billing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	


GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T5645' AND DESCITEM='BD5HV')
  
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','BD5HV','  ','E','','BD5HV','Annuity Int Capitalization','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5645','BD5HV','  ','S','','BD5HV','Annuity Int Capitalization','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' AND DESCITEM='BAFW')
  
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','BAFW','  ','E','','BAFW','New Annuity Interest Billing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','BAFW','  ','S','','BAFW','New Annuity Interest Billing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

GO


IF NOT EXISTS (	 SELECT *  FROM VM1DTA.DESCPF 
  where DESCCOY='2' AND DESCTABL='T1688' AND DESCITEM='BAFX')
  
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','BAFX','  ','E','','BAFX','New Annuity Capitalization Int','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1688','BAFX','  ','S','','BAFX','New Annuity Capitalization Int','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

GO
SET QUOTED_IDENTIFIER OFF  
GO
