IF EXISTS ( SELECT * FROM sys.tables where name = 'BD5HVDATA' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP TABLE [VM1DTA].[BD5HVDATA]
GO



IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BD5HVDATA' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
				
CREATE TABLE [VM1DTA].[BD5HVDATA]
  (
	[BATCHID]       [bigint],
	[CNT] [int],
    [CHDRCOY]    [nchar](1),
    [CHDRNUM]    [nchar](8),
	[UNIQUE_NUMBER] [bigint] NOT NULL,
	[PLNSFX]        [int],
    [LIFE]          [nchar](2),
    [COVERAGE]      [nchar](2),
    [RIDER]         [nchar](2),    
    [CURRCD]   [nchar](3),
    [VALIDFLAG]  [nchar](1),
	[APCAPLAMT]    [numeric](17, 2),
	[APINTAMT]     [numeric](17, 2),
	[APLSTCAPDATE]	[int], 
	[APNXTCAPDATE]	[int], 
	[APLSTINTBDTE]	[int], 
	[APNXTINTBDTE]	[int],
    [TRANNO]     [int],
    [CNTTYPE]    [nchar](3),
    [CHDRPFX]    [nchar](2),
    [SERVUNIT]   [nchar](2),
    [OCCDATE]    [int],
    [CCDATE]     [int],
    [COWNPFX]    [nchar](2),
    [COWNCOY]    [nchar](1),
    [COWNNUM]    [nchar](8),
    [COLLCHNL]   [nchar](2),
    [CNTBRANCH]  [nchar](2),
    [CNTCURR]    [nchar](3),
    [POLSUM]     [int],
    [POLINC]     [int],
    [AGNTPFX]    [nchar](2),
    [AGNTCOY]    [nchar](1),
    [AGNTNUM]    [nchar](8),
   [DATIME] [datetime2](6) ) ON [PRIMARY]

GO

exec sp_addextendedproperty 'MS_Description', 'TEMP FILE FOR BD5HVDATA', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA'
go

exec sp_addextendedproperty 'MS_Description', 'BATCH ID', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'BATCHID'
go

exec sp_addextendedproperty 'MS_Description', 'CNT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CNT'
go

exec sp_addextendedproperty 'MS_Description', 'UNIQUE NUMBER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'UNIQUE_NUMBER'
go

exec sp_addextendedproperty 'MS_Description', 'CHDRCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CHDRCOY'
go

exec sp_addextendedproperty 'MS_Description', 'CHDRNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CHDRNUM'
go

exec sp_addextendedproperty 'MS_Description', 'PLNSFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'PLNSFX'
go

exec sp_addextendedproperty 'MS_Description', 'LIFE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'LIFE'
go

exec sp_addextendedproperty 'MS_Description', 'COVERAGE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'COVERAGE'
go

exec sp_addextendedproperty 'MS_Description', 'RIDER', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'RIDER'
go

exec sp_addextendedproperty 'MS_Description', 'CURRCD', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CURRCD'
go

exec sp_addextendedproperty 'MS_Description', 'VALIDFLAG', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'VALIDFLAG'
go

exec sp_addextendedproperty 'MS_Description', 'APCAPLAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APCAPLAMT'
go

exec sp_addextendedproperty 'MS_Description', 'APINTAMT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APINTAMT'
go

exec sp_addextendedproperty 'MS_Description', 'APLSTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APLSTCAPDATE'
go


exec sp_addextendedproperty 'MS_Description', 'APNXTCAPDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APNXTCAPDATE'
go
exec sp_addextendedproperty 'MS_Description', 'APLSTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APLSTINTBDTE'
go
exec sp_addextendedproperty 'MS_Description', 'APNXTINTBDTE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'APNXTINTBDTE'
go

exec sp_addextendedproperty 'MS_Description', 'TRANNO', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'TRANNO'
go
exec sp_addextendedproperty 'MS_Description', 'CNTTYPE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CNTTYPE'
go
exec sp_addextendedproperty 'MS_Description', 'CHDRPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CHDRPFX'
go

exec sp_addextendedproperty 'MS_Description', 'SERVUNIT', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'SERVUNIT'
go

exec sp_addextendedproperty 'MS_Description', 'OCCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'OCCDATE'
go

exec sp_addextendedproperty 'MS_Description', 'CCDATE', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CCDATE'
go

exec sp_addextendedproperty 'MS_Description', 'COWNPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'COWNPFX'
go

exec sp_addextendedproperty 'MS_Description', 'COWNCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'COWNCOY'
go

exec sp_addextendedproperty 'MS_Description', 'COWNNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'COWNNUM'
go
    
exec sp_addextendedproperty 'MS_Description', 'COLLCHNL', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'COLLCHNL'
go	
 
exec sp_addextendedproperty 'MS_Description', 'CNTBRANCH', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CNTBRANCH'
go	
exec sp_addextendedproperty 'MS_Description', 'CNTCURR', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'CNTCURR'
go

exec sp_addextendedproperty 'MS_Description', 'POLSUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'POLSUM'
go

exec sp_addextendedproperty 'MS_Description', 'POLINC', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'POLINC'
go	
	
exec sp_addextendedproperty 'MS_Description', 'AGNTPFX', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'AGNTPFX'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTCOY', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'AGNTCOY'
go 
exec sp_addextendedproperty 'MS_Description', 'AGNTNUM', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'AGNTNUM'
go    
exec sp_addextendedproperty 'MS_Description', 'DATIME', 'SCHEMA', 'VM1DTA', 'TABLE', 'BD5HVDATA', 'COLUMN',
                            'DATIME'
go    