SET QUOTED_IDENTIFIER ON  
GO
delete from vm1dta.BPSRPF where BPRIORPROC IN ('#BR539') and  BPRIORCOY='2';

IF NOT EXISTS (	SELECT * from VM1DTA.BPSRPF  where BPRIORPROC IN ('#BR539') and  BPRIORCOY='2')
Insert into vm1dta.BPSRPF (BPRIORCOY,BPRIORPROC,BSUBSEQCOY,BSUBSEQPRC,USRPRF,JOBNM,DATIME) values ('2','#BR539','2','#BD5HU    ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

delete from vm1dta.BPRDPF where BPROCESNAM IN ('#BD5HU') and  COMPANY='2';

IF NOT EXISTS (	SELECT * from VM1DTA.BPRDPF  where BPROCESNAM IN ('#BD5HU') and  COMPANY='2')
Insert into vm1dta.BPRDPF (COMPANY,BPROCESNAM,BERLSTRTIM,BLATSTRTIM,BPROGRAM,BCOMANDSTG,BTHRDSTPRC,BTHRDSSPRC,BPARPPROG,RRULE,BAUTHCODE,BCHCLSFLG,BDEFBRANCH,BSCHEDPRTY,BSYSJOBPTY,BSYSJOBTIM,BPSYSPAR01,BPSYSPAR02,BPSYSPAR03,BPSYSPAR04,BPSYSPAR05,BCYCPERCMT,BPRCRUNLIB,BPRESTMETH,BCRITLPROC,BMAXCYCTIM,MULBRN,MULBRNTP,PRODCODE,USRPRF,JOBNM,DATIME) values ('2','#BD5HU    ','00:00:00','23:59:59','BD5HU     ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','BAFW',' ','10',50,50,5000,'          ','          ','          ','          ','          ',100,'*DATABASE ','1','3',0,' ',' ','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF  
GO
