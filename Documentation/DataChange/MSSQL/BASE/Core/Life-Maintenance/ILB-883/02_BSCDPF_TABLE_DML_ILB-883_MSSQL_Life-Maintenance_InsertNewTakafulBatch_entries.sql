GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B618' AND PRODCODE='L' AND BSCHEDNAM = 'LKNEWAGTPY')
    DELETE FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B618' AND PRODCODE='L' AND BSCHEDNAM = 'LKNEWAGTPY'; 
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP,BATCHFLAG) VALUES ('LKNEWAGTPY',1,'N','N','B618','QBATCH    ','4','*JOBD     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N','N');
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B618' AND PRODCODE='L' AND BSCHEDNAM = 'LKNEWAGTPY')
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP,BATCHFLAG) VALUES ('LKNEWAGTPY',1,'N','N','B618','QBATCH    ','4','*JOBD     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N','N');
GO