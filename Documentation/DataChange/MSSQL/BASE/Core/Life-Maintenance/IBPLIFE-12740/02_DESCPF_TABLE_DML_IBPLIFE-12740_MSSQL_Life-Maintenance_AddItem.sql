
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'TR4BZ' AND DESCITEM='T514' AND LANGUAGE='E' )
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','TR4BZ','T514','  ','E','','ML','Manual Lapse Processing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
	
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'THSUZ' AND DESCITEM='T514' AND LANGUAGE='E' )
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','THSUZ','T514','  ','E','','T514','Manual Lapse Processing','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
	
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL = 'TR384' AND DESCITEM='***T514' AND LANGUAGE='E' )
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
	VALUES ('IT','2','TR384','***T514','  ','E','','***T514','Terminate Component','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF  
GO