﻿
SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT * FROM sys.columns WHERE name = 'INVESRESULT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='INVSPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[INVSPF] ALTER COLUMN INVESRESULT nvarchar(1000) null;
	END
GO
SET QUOTED_IDENTIFIER OFF  
GO