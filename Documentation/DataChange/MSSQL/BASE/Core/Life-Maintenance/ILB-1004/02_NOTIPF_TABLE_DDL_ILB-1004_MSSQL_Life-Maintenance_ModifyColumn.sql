﻿SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT * FROM sys.columns WHERE name = 'accidentdesc' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[NOTIPF] ALTER COLUMN accidentdesc nvarchar(3000) null;
	END
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT * FROM sys.columns WHERE name = 'Location' and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[NOTIPF] ALTER COLUMN [Location] nvarchar(200) null;
	END
GO
SET QUOTED_IDENTIFIER OFF  
GO
