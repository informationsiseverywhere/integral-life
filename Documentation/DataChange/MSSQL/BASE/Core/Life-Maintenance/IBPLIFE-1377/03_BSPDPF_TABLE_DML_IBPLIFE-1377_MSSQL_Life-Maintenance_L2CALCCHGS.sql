SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.BSPDPF WHERE BSCHEDNAM = 'L2CALCCHGS' AND COMPANY='2' )
INSERT INTO VM1DTA.BSPDPF ( BSCHEDNAM,COMPANY,BPROCESNAM,USRPRF,JOBNM,DATIME) values ( 'L2CALCCHGS','2','BBILL-FM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
