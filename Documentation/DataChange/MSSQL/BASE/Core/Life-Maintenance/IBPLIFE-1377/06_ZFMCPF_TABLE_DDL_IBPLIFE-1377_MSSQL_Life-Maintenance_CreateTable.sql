IF NOT EXISTS (SELECT * FROM sys.tables where name = 'ZFMCPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN

CREATE TABLE [VM1DTA].[ZFMCPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[COVERAGE] [nchar](2) NULL,
	[RIDER] [nchar](2) NULL,
	[PLNSFX] [int] NULL,
	[PRMCUR] [nchar](3) NULL,
	[ZFMCDAT] [int] NULL,
	[SUMINS] [numeric](17, 2) NULL,
	[PCESDTE] [int] NULL,
	[CRTABLE] [nchar](4) NULL,
	[MORTCLS] [nchar](1) NULL,
	[ZNOFDUE] [int] NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNME] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[JOBNM] [nchar](10) NULL,

 CONSTRAINT [PK_ZFMCPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.ZFMCPF.PK_ZFMCPF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZFMCPF', @level2type=N'CONSTRAINT',@level2name=N'PK_ZFMCPF';
END
GO



	

	
	
	
	
	