IF EXISTS (SELECT * FROM sys.columns WHERE name = 'ZRECRUIT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='AGLFPF'))      
  
             alter table VM1DTA.AGLFPF drop constraint DF__AGLFPF__ZRECRUIT__15C09D84
            
             ALTER TABLE VM1DTA.AGLFPF ALTER COLUMN ZRECRUIT NCHAR(8);
           
             ALTER TABLE [VM1DTA].[AGLFPF] ADD constraint DF__AGLFPF__ZRECRUIT__15C09D84 DEFAULT (' ') FOR [ZRECRUIT]
            
    
GO
