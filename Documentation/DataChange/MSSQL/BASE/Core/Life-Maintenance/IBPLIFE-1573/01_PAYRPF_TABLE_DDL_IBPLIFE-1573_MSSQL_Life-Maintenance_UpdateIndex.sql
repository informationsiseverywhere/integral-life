SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'PAYRARC' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYRPF'))
	BEGIN
		ALTER INDEX [PAYRARC] ON [VM1DTA].[PAYRPF] REBUILD PARTITION = ALL WITH (FILLFACTOR = 85);	
	END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'PAYRGRP' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYRPF'))
	BEGIN
		ALTER INDEX [PAYRGRP] ON [VM1DTA].[PAYRPF] REBUILD PARTITION = ALL WITH (FILLFACTOR = 85);	
	END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'PAYRPF_L2POLRNWL' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYRPF'))
	BEGIN
		ALTER INDEX [PAYRPF_L2POLRNWL] ON [VM1DTA].[PAYRPF] REBUILD PARTITION = ALL WITH (FILLFACTOR = 85);	
	END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'PAYRREV' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYRPF'))
	BEGIN
		ALTER INDEX [PAYRREV] ON [VM1DTA].[PAYRPF] REBUILD PARTITION = ALL WITH (FILLFACTOR = 85);	
	END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UQ_PAYRPF' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYRPF'))
	BEGIN
		ALTER INDEX [UQ_PAYRPF] ON [VM1DTA].[PAYRPF] REBUILD PARTITION = ALL WITH (FILLFACTOR = 85);	
	END
GO
SET QUOTED_IDENTIFIER OFF
GO


