	DELETE FROM [VM1DTA].[ERORPF] WHERE EROREROR='RRC3';
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E'  AND h.[EROREROR] = 'RRC3')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER',' ','E','','RRC3  ', 'Entry not allowed','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO
	
	
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='S'  AND h.[EROREROR] = 'RRC3')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER',' ','S','','RRC3  ', N'不允许进入','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO


	