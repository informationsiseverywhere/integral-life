
IF EXISTS (SELECT * FROM sys.columns WHERE name = 'CLAIMNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))       
    BEGIN
		 ALTER TABLE VM1DTA.CLMDPF ALTER COLUMN CLAIMNO NCHAR(9);
		
	END
GO


IF EXISTS (SELECT * FROM sys.columns WHERE name = 'CLAIMNOTIFINO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))       
    BEGIN
		ALTER TABLE VM1DTA.CLMDPF ALTER COLUMN CLAIMNOTIFINO NCHAR(8);
		
	END
GO
