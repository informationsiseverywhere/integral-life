
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='TD5J1' AND d.DESCITEM='TWL' AND d.LANGUAGE='E') 

Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TD5J1','TWL','  ','E','','TWL','Traditional Whole Life','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TD5J1','TWL','  ','S','','TWL','Traditional Whole Life','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='TD5J2' AND d.DESCITEM='TWL1' AND d.LANGUAGE='E') 

Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TD5J2','TWL1','  ','E','','TWL','Traditional Whole Life','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TD5J2','TWL1','  ','S','','TWL','Traditional Whole Life','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
