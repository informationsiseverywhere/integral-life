SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='T5661' and DESCITEM='EHRO' and LANGUAGE = 'E')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5661','EHRO','  ','E',' ','EHRO','High Risk Occupation','UNDERWR1  ','UNDERWR1',current_timestamp);

GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='T5661' and DESCITEM='EHRO' and LANGUAGE = 'S')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5661','EHRO','  ','S',' ','EHRO','High Risk Occupation','UNDERWR1  ','UNDERWR1',current_timestamp);

GO
SET QUOTED_IDENTIFIER OFF  
GO
 
SET QUOTED_IDENTIFIER ON  
GO
 IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='T5661' and DESCITEM='SHRO' and LANGUAGE = 'S')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5661','SHRO','  ','S',' ','SHRO','High Risk Occupation','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
 IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCTABL='T5661' and DESCITEM='SHRO' and LANGUAGE = 'E')INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T5661','SHRO','  ','E',' ','SHRO','High Risk Occupation','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO


 
