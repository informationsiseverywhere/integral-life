GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDF%' AND E.ERORLANG='S') 
  
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDF  ', N'代理没有必要的许可证','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP); 
	
GO

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDD%' AND E.ERORLANG='S') 
 
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDD  ',N'银行网点没有必要的许可证','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP); 

GO



GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRD9%' AND E.ERORLANG='S') 
  
	 INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	 VALUES('ER',' ','S','          ','RRD9  ',N'银行服务经理没有必要的许可证','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP); 
	
GO


GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDA%' AND E.ERORLANG='S') 
  
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDA  ',N'银行柜员没有必要的许可证','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP); 
	  
GO
  

GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDE%' AND E.ERORLANG='S') 
  
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
 VALUES('ER',' ','S','          ','RRDE  ', N'银行网点许可证过期','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP);  
	  
GO
  

 
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDB%' AND E.ERORLANG='S') 
  
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDB  ', N'银行服务经理许可证过期','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP); 	
	  
GO
  
 
GO
IF NOT EXISTS (	SELECT * FROM VM1DTA.ERORPF E WHERE E.EROREROR LIKE 'RRDC%' AND E.ERORLANG='S')
  
	INSERT INTO VM1DTA.ERORPF (ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,ERORFILE,DATIME)
	VALUES('ER',' ','S','          ','RRDC  ', N'银行网点许可证过期','0','0','0',' ','  ','  ','   ',CURRENT_TIMESTAMP);
	  
GO
  


 

