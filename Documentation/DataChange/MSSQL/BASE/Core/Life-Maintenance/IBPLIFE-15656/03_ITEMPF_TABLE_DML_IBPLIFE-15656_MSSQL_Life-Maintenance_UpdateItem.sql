
SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT * FROM VM1DTA.ITEMPF  WHERE ITEMTABL = 'TD5E6' AND ITEMCOY='2' AND ITEMITEM='END')
UPDATE  VM1DTA.ITEMPF SET  ITMFRM = '19900101' , ITMTO ='99999999'  WHERE ITEMTABL = 'TD5E6' AND ITEMCOY='2' AND ITEMITEM='END';
GO 

SET QUOTED_IDENTIFIER OFF  
GO 