
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T3616' AND DESCITEM='MG' AND LANGUAGE='E')
INSERT INTO VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
VALUES ('IT','2','T3616','MG',' ','E','','MGMFEE','Management Fee','UNDERWR1','UNDERWR1', CURRENT_TIMESTAMP);
GO

SET QUOTED_IDENTIFIER OFF
GO