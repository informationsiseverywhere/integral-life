
IF EXISTS ( SELECT * FROM sys.views where name = 'LIFEREV' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFEREV
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFEREV](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, TRANNO, VALIDFLAG, LIFE, JLIFE, TERMID, TRDT, TRTM, USER_T, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            VALIDFLAG,
            LIFE,
            JLIFE,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
			DATIME

       FROM VM1DTA.LIFEPF

GO


