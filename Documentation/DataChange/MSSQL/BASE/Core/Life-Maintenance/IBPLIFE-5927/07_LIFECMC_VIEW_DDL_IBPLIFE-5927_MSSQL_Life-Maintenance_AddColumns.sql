IF EXISTS ( SELECT * FROM sys.views where name = 'LIFECMC' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFECMC
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFECMC](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, ANBCCD, CLTDOB, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            ANBCCD,
            CLTDOB,
			DATIME

       FROM VM1DTA.LIFEPF

GO

