
IF EXISTS ( SELECT * FROM sys.views where name = 'LIFERNL' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFERNL
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFERNL](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, VALIDFLAG, STATCODE, CURRFROM, CURRTO, ANBCCD, CLTSEX, LCDTE, LIFCNUM, LIFEREL, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, CLTDOB, SBSTDL, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            JLIFE,
            VALIDFLAG,
            STATCODE,
            CURRFROM,
            CURRTO,
            ANBCCD,
            CLTSEX,
            LCDTE,
            LIFCNUM,
            LIFEREL,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            CLTDOB,
            SBSTDL,
			DATIME
       FROM VM1DTA.LIFEPF

GO


