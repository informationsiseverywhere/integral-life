IF EXISTS ( SELECT * FROM sys.views where name = 'LIFECFI' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFECFI
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFECFI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, STATCODE, TRANNO, LIFE, JLIFE, TERMID, TRDT, TRTM, USER_T, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            STATCODE,
            TRANNO,
            LIFE,
            JLIFE,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
			DATIME
       FROM vm1dta.LIFEPF
      WHERE VALIDFLAG = '1'

GO