IF EXISTS ( SELECT * FROM sys.views where name = 'LFCLLNB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LFCLLNB
GO

/****** Object:  View [VM1DTA].[LFCLLNB]    Script Date: 22-Jul-21 8:54:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LFCLLNB](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFCNUM, VALIDFLAG, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFCNUM,
            VALIDFLAG,
			DATIME
       FROM VM1DTA.LIFEPF
      WHERE VALIDFLAG = '3'

GO


