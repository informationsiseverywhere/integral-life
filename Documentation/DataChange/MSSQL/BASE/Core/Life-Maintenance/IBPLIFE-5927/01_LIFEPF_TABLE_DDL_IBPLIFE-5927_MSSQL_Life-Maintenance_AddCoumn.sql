IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('DATIME') and object_id in (SELECT object_id FROM sys.tables WHERE name ='LIFEPF'))
BEGIN
	ALTER TABLE VM1DTA.LIFEPF ADD [DATIME] [datetime2](6) NULL ;
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for DATIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'LIFEPF', @level2type=N'COLUMN',@level2name=N'DATIME';
END
GO

