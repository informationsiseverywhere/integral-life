IF EXISTS ( SELECT * FROM sys.views where name = 'LIFE' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFE
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFE](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, TRANNO, CURRFROM, CURRTO, STATCODE, LIFE, JLIFE, LCDTE, LIFCNUM, LIFEREL, ANBCCD, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, TERMID, TRDT, TRTM, USER_T, CLTSEX, CLTDOB, SBSTDL, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            TRANNO,
            CURRFROM,
            CURRTO,
            STATCODE,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
            LIFEREL,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            CLTSEX,
            CLTDOB,
            SBSTDL,
			DATIME
       FROM VM1DTA.LIFEPF

GO


