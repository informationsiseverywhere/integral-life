IF EXISTS ( SELECT * FROM sys.views where name = 'LIFEAFI' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFEAFI
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFEAFI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, TRANNO, CURRFROM, CURRTO, STATCODE, LIFE, JLIFE, LCDTE, LIFCNUM, LIFEREL, CLTSEX, ANBCCD, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, TERMID, TRDT, TRTM, USER_T, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            TRANNO,
            CURRFROM,
            CURRTO,
            STATCODE,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
            LIFEREL,
            CLTSEX,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
			DATIME
       FROM VM1DTA.LIFEPF
      WHERE NOT (VALIDFLAG = '3')

GO


