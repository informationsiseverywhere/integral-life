IF EXISTS ( SELECT * FROM sys.views where name = 'ZLIFELC' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.ZLIFELC
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[ZLIFELC](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFCNUM, VALIDFLAG, LIFE, JLIFE, STATCODE, TRANNO, CURRFROM, CURRTO, LCDTE, LIFEREL, CLTDOB, CLTSEX, ANBCCD, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, SBSTDL, TERMID, TRDT, TRTM, USER_T, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFCNUM,
            VALIDFLAG,
            LIFE,
            JLIFE,
            STATCODE,
            TRANNO,
            CURRFROM,
            CURRTO,
            LCDTE,
            LIFEREL,
            CLTDOB,
            CLTSEX,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            SBSTDL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
			DATIME
       FROM VM1DTA.LIFEPF
      WHERE NOT (VALIDFLAG = '2')

GO


