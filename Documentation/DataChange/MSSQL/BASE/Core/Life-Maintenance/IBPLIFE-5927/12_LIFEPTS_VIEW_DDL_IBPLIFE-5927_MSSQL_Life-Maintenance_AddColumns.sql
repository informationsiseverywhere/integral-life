IF EXISTS ( SELECT * FROM sys.views where name = 'LIFEPTS' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFEPTS
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFEPTS](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, STATCODE, TRANNO, LIFE, JLIFE, LCDTE, LIFCNUM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            STATCODE,
            TRANNO,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
			DATIME

       FROM VM1DTA.LIFEPF

GO