IF EXISTS ( SELECT * FROM sys.views where name = 'LIFETRN' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFETRN
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFETRN](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, STATCODE, TRANNO, CURRFROM, CURRTO, LIFE, JLIFE, LCDTE, LIFCNUM, LIFEREL, CLTDOB, CLTSEX, ANBCCD, AGEADM, SELECTION, SMOKING, OCCUP, PURSUIT01, PURSUIT02, MARTAL, SBSTDL, TERMID, TRDT, TRTM, USER_T, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            STATCODE,
            TRANNO,
            CURRFROM,
            CURRTO,
            LIFE,
            JLIFE,
            LCDTE,
            LIFCNUM,
            LIFEREL,
            CLTDOB,
            CLTSEX,
            ANBCCD,
            AGEADM,
            SELECTION,
            SMOKING,
            OCCUP,
            PURSUIT01,
            PURSUIT02,
            MARTAL,
            SBSTDL,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
			DATIME

       FROM VM1DTA.LIFEPF

GO


