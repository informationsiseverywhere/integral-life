IF EXISTS ( SELECT * FROM sys.views where name = 'LIFERAS' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW VM1DTA.LIFERAS
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[LIFERAS](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, VALIDFLAG, LIFE, JLIFE, ANBCCD, CLTSEX, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            VALIDFLAG,
            LIFE,
            JLIFE,
            ANBCCD,
            CLTSEX,
			DATIME
       FROM VM1DTA.LIFEPF
      WHERE NOT (VALIDFLAG <> '1')

GO


