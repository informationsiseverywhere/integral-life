 IF EXISTS (SELECT * FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMIN' AND LANG='E')
 DELETE FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMIN' AND LANG='E';

INSERT INTO VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
VALUES('CINCRMIN','E','Minimum Contribution Increase','Minimum','Contribution','Increase','','0','0','0','UNDERWR1','UNDERWR1',current_timestamp);
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMIN' AND LANG='E')

INSERT INTO VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
VALUES('CINCRMIN','E','Minimum Contribution Increase','Minimum','Contribution','Increase','','0','0','0','UNDERWR1','UNDERWR1',current_timestamp);
GO

IF EXISTS (SELECT * FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMAX' AND LANG='E')
 DELETE FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMAX' AND LANG='E';

INSERT INTO VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
VALUES('CINCRMAX','E','Maximum Contribution Increase','Maximum','Contribution','Increase','','0','0','0','UNDERWR1','UNDERWR1',current_timestamp);
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.FLDTPF WHERE FDID = 'CINCRMAX' AND LANG='E')

INSERT INTO VM1DTA.FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME) 
VALUES('CINCRMAX','E','Maximum Contribution Increase','Maximum','Contribution','Increase','','0','0','0','UNDERWR1','UNDERWR1',current_timestamp);
GO
