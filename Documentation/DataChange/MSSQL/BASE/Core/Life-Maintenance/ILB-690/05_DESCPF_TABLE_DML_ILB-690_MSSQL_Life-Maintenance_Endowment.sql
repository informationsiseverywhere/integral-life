
SET QUOTED_IDENTIFIER ON  
GO

IF EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='TD5H7'  AND d.DESCPFX='HE' AND d.LANGUAGE='E') 
update descpf set shortdesc='' , longdesc='Accumulated Amt Interest Rules' where descpfx='HE' and desccoy='2' and desctabl='TD5H7' and language='E';
GO

IF EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='TD5H7'  AND d.DESCPFX='HE' AND d.LANGUAGE='E') 
update descpf set shortdesc='' , longdesc='Accumulated Amt Interest Rules' where descpfx='HE' and desccoy='2' and desctabl='TD5H7' and language='S';
GO

SET QUOTED_IDENTIFIER OFF  
GO



