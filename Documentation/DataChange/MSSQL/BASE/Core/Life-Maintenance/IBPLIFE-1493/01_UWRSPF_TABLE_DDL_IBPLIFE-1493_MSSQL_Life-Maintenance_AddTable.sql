IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'UWRSPF' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
CREATE TABLE [VM1DTA].[UWRSPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[REASON] [nchar](100) NULL,
	[TRANNO] [int] NULL,
	[TRANCDE] [nchar](4) NULL,
	[TERMID] [nchar](4) NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[USER_T] [int] NULL,
	[crtable] [nchar](4) NULL
) 

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UWRSPF Table file' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.CHDRCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.CHDRNUM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.REASON' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'REASON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.TRANNO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'TRANNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.TRANCDE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'TRANCDE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.TERMID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'TERMID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.TRDT' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'TRDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.TRTM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'TRTM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VM1DTA.UWRSPF.USER_T' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'USER_T'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CRTABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'UWRSPF', @level2type=N'COLUMN',@level2name=N'CRTABLE'


END
GO

