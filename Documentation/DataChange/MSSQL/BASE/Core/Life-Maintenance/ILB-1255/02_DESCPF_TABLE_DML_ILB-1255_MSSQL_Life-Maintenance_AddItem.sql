
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF
  where DESCCOY='2' AND DESCTABL='TR384' AND DESCITEM='***BAEC' AND LANGUAGE='E')

Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR384','***BAEC','  ','E','','Letter','Lapse Reinstatement Letter','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	

GO
SET QUOTED_IDENTIFIER OFF  
GO