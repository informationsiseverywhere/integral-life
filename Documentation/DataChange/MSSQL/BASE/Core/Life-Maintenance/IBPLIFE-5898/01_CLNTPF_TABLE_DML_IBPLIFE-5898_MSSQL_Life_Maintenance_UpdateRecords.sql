SET QUOTED_IDENTIFIER ON
GO

 
IF EXISTS (SELECT * FROM VM1DTA.CLNTPF WHERE CLNTNUM = '50195387' AND UNIQUE_NUMBER = '5634' AND CLNTCOY = '9')
UPDATE VM1DTA.CLNTPF SET VALIDFLAG = '1' WHERE CLNTNUM = '50195387' AND UNIQUE_NUMBER = '5634' AND CLNTCOY = '9';

 
GO
SET QUOTED_IDENTIFIER OFF