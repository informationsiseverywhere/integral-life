
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM QUIPOZ.APP_MESSAGE_VAR WHERE MESSAGE_FILE='PAXMSG' AND MESSAGE_ID='ECT6000' )
INSERT INTO QUIPOZ.APP_MESSAGE_VAR(MESSAGE_FILE,MESSAGE_ID,MESSAGE_VAR_NO,MESSAGE_VAR_LN,MESSAGE_DEC_PLACE,MESSAGE_VAR_TY)
VALUES('PAXMSG','ECT6000','1','8','1','*CHAR');
INSERT INTO QUIPOZ.APP_MESSAGE_VAR(MESSAGE_FILE,MESSAGE_ID,MESSAGE_VAR_NO,MESSAGE_VAR_LN,MESSAGE_DEC_PLACE,MESSAGE_VAR_TY)
VALUES('PAXMSG','ECT6000','2','10','1','*CHAR');
GO

IF NOT EXISTS (SELECT * FROM QUIPOZ.APP_MESSAGE_VAR WHERE MESSAGE_FILE='PAXMSG' AND MESSAGE_ID='ECT6420' ) 
INSERT INTO QUIPOZ.APP_MESSAGE_VAR(MESSAGE_FILE,MESSAGE_ID,MESSAGE_VAR_NO,MESSAGE_VAR_LN,MESSAGE_DEC_PLACE,MESSAGE_VAR_TY)
VALUES('PAXMSG','ECT6420','1','8','1','*CHAR');
GO

IF NOT EXISTS (SELECT * FROM QUIPOZ.APP_MESSAGE WHERE MESSAGE_FILE='PAXMSG' AND MESSAGE_ID='ECT6000' )
INSERT INTO QUIPOZ.APP_MESSAGE (MESSAGE_FILE,MESSAGE_ID,MESSAGE_DS,MESSAGE_LONG_DS)
VALUES('PAXMSG','ECT6000','0','Proposal &1 is &2');
GO

IF NOT EXISTS (SELECT * FROM QUIPOZ.APP_MESSAGE WHERE MESSAGE_FILE='PAXMSG' AND MESSAGE_ID='ECT6420' )
INSERT INTO QUIPOZ.APP_MESSAGE (MESSAGE_FILE,MESSAGE_ID,MESSAGE_DS,MESSAGE_LONG_DS)
VALUES('PAXMSG','ECT6420','0','Contract &1 is issued');
GO

SET QUOTED_IDENTIFIER OFF  
GO