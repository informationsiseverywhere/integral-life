GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAUTOMATY')
BEGIN
    DELETE FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAUTOMATY';
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKAUTOMATY','Auto Maturity Batch                               ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKAUTOMATY')
BEGIN
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKAUTOMATY','Auto Maturity Batch                               ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
END
GO