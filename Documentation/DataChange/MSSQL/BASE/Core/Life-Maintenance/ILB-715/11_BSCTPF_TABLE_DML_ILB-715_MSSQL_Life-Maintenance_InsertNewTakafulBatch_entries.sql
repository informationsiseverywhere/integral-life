GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSNDBNK')
BEGIN
    DELETE FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSNDBNK';
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKSNDBNK','Receipt Bank Clearing Extract (none Cash & CCard) ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
END
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCTPF WHERE BSCHEDNAM = 'LKSNDBNK')
BEGIN
	INSERT INTO VM1DTA.BSCTPF (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) VALUES ('E','LKSNDBNK','Receipt Bank Clearing Extract (none Cash & CCard) ','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP);
END
GO