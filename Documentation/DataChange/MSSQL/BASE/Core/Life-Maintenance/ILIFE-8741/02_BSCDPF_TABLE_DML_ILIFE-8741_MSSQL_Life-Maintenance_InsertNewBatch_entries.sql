GO
IF  EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B515' AND PRODCODE='L' AND BSCHEDNAM = 'LKREGPAY')
    DELETE FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B515' AND PRODCODE='L' AND BSCHEDNAM = 'LKREGPAY'; 
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) VALUES ('LKREGPAY',1,'N','N','B515','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N');
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.BSCDPF WHERE BAUTHCODE = 'B515' AND PRODCODE='L' AND BSCHEDNAM = 'LKREGPAY')
	INSERT INTO VM1DTA.BSCDPF (BSCHEDNAM,BNOFTHREDS,BUNIQINSYS,BDEBUGMODE,BAUTHCODE,JOBQ,BJOBQPRTY,BJOBDESC,PRODCODE,JOBNM,USRPRF,DATIME,DROPTEMP) VALUES ('LKREGPAY',1,'N','N','B515','QBATCH    ','5','PAXUS     ','L','TAKAFULCOM','UNDERWR1  ',CURRENT_TIMESTAMP,'N');
GO