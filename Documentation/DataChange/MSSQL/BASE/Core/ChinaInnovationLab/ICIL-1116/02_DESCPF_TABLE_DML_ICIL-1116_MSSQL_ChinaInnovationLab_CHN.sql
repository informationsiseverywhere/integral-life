
BEGIN TRANSACTION
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE DESCCOY='2' AND DESCTABL='T3614' and DESCITEM='AUS98' and LANGUAGE = 'S')
	INSERT INTO VM1DTA.DESCPF (DESCPFX, DESCCOY, DESCTABL, DESCITEM, ITEMSEQ, LANGUAGE, TRANID, SHORTDESC, LONGDESC, USRPRF, JOBNM, DATIME) VALUES ('IT', '2', 'T3614', 'AUS98   ', '  ', 'S', '', '      ', N'澳大利亚1998  ', 'UNDERWR1  ', 'UNDERWR1  ', current_timestamp);
else
	UPDATE VM1DTA.DESCPF SET LONGDESC= N'澳大利亚1998  ' where DESCCOY='2' AND DESCTABL='T3614' and DESCITEM='AUS98' and LANGUAGE = 'S';
	GO 

 COMMIT TRANSACTION

