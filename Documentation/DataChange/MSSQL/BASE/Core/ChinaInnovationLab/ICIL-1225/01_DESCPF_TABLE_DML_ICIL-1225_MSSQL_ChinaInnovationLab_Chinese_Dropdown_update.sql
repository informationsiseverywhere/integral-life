﻿
IF NOT EXISTS (	 SELECT * FROM VM1DTA.DESCPF h  where language = 'S' and desccoy='2' and desctabl='T3672' and descitem='4')
	INSERT INTO VM1DTA.DESCPF(DESCPFX,DESCCOY,DESCTABL,DESCITEM,LANGUAGE,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) VALUES ('IT','2','T3672','4','S','Direct Crd',N'直接贷方','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
else
	Update vm1dta.descpf set shortdesc=N'Direct Crd',Longdesc=N'直接贷方' where language = 'S' and desccoy='2' and desctabl='T3672' and descitem='4';
GO

IF NOT EXISTS (	 SELECT * FROM VM1DTA.DESCPF h  where language = 'S' and desccoy='2' and desctabl='T5687' and descitem='ACCR')
	INSERT INTO VM1DTA.DESCPF(DESCPFX,DESCCOY,DESCTABL,DESCITEM,LANGUAGE,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) VALUES ('IT','2','T5687','ACCR','S','ACCR',N'加速危机保障附加险','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
else
	Update vm1dta.descpf set Longdesc=N'加速危机保障附加险' where language = 'S' and desccoy='2' and desctabl='T5687' and descitem='ACCR';
GO


