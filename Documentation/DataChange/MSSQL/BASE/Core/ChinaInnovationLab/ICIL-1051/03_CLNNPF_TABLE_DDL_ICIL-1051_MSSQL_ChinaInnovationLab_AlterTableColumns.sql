
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CLAIMNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLNNPF'))       
    BEGIN
		ALTER TABLE VM1DTA.CLNNPF ADD CLAIMNO CHAR(9);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<CLAIMNO>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CLNNPF', @level2type=N'COLUMN',@level2name=N'CLAIMNO ';
	END
GO


IF EXISTS (SELECT * FROM sys.columns WHERE name = 'NOTIFINUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLNNPF'))       
    BEGIN
		ALTER TABLE VM1DTA.CLNNPF ALTER COLUMN NOTIFINUM VARCHAR(14);
		UPDATE VM1DTA.CLNNPF SET NOTIFINUM = (REPLACE(NOTIFINUM,'CLMNTF',''));
		ALTER TABLE VM1DTA.CLNNPF ALTER COLUMN NOTIFINUM VARCHAR(8) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<NOTIFINUM>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CLNNPF', @level2type=N'COLUMN',@level2name=N'NOTIFINUM ';
	END
GO

