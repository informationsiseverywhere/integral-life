
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CLAIMNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='INVSPF'))       
    BEGIN
		ALTER TABLE VM1DTA.INVSPF ADD CLAIMNO CHAR(9);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<CLAIMNO>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'INVSPF', @level2type=N'COLUMN',@level2name=N'CLAIMNO ';
	END
GO

