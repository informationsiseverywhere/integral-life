

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF d WHERE d.DESCCOY='2' AND d.DESCTABL='TR272' AND d.DESCITEM='ECQ*77' AND d.LANGUAGE='E') 
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR272','ECQ*77','  ','E','','ECQ*77','Auto Cheque Print.Bank77Chn.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR272','ECQ*77','  ','S','','ECQ*77',N'自动支票打印日银银行','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

