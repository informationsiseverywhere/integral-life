IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CLAIMNO') and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
BEGIN
	ALTER TABLE REGPPF ADD CLAIMNO CHAR(9);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.REGPPF.CLAIMNO IS CLAIMNO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'CLAIMNO';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CLAIMNOTIFINO') and object_id in (SELECT object_id FROM sys.tables WHERE name ='REGPPF'))
BEGIN
	ALTER TABLE REGPPF ADD CLAIMNOTIFINO CHAR(8);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'COMMENT ON COLUMN VM1DTA.REGPPF.CLAIMNOTIFINO IS CLAIMNOTIFINO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'REGPPF', @level2type=N'COLUMN',@level2name=N'CLAIMNOTIFINO';
END
GO
