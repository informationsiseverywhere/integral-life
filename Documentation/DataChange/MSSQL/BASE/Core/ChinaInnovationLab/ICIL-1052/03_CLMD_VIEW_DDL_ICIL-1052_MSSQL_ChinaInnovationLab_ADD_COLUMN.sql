SET QUOTED_IDENTIFIER ON  
GO


IF EXISTS ( SELECT * FROM sys.views where name = 'CLMDCLM' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].CLMDCLM
GO

CREATE VIEW [VM1DTA].[CLMDCLM](UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            VALIDFLAG,
            CRTABLE,
            SHORTDS,
            LIENCD,
            CNSTCUR,
            EMV,
            ACTVALUE,
            VIRTFND,
            TYPE_T,
            ANNYPIND,
			CLAIMNO,
			CLAIMNOTIFINO,
			USRPRF,
            JOBNM,
            DATIME)
AS
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            LIFE,
            JLIFE,
            COVERAGE,
            RIDER,
            VALIDFLAG,
            CRTABLE,
            SHORTDS,
            LIENCD,
            CNSTCUR,
            EMV,
            ACTVALUE,
            VIRTFND,
            TYPE_T,
            ANNYPIND,
			CLAIMNO,
			CLAIMNOTIFINO,
			USRPRF,
            JOBNM,
            DATIME
       FROM CLMDPF;
  
GO
SET QUOTED_IDENTIFIER OFF  
GO



