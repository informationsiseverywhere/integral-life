

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('TRANSNO') and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
BEGIN
	 ALTER TABLE VM1DTA.NOTIPF ADD TRANSNO nchar(5);
	 EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOTIPF', @level2type=N'COLUMN',@level2name=N'TRANSNO';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('VALIDFLAG') and object_id in (SELECT object_id FROM sys.tables WHERE name ='NOTIPF'))
BEGIN
	 ALTER TABLE VM1DTA.NOTIPF ADD VALIDFLAG nchar(1);
	 EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'NOTIPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
END
GO 
