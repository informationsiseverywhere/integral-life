 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [VM1DTA].[AUDIT_ITEMPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[OLDITEMPFX] [nchar](2) NULL,
	[OLDITEMCOY] [nchar](1) NULL,
	[OLDITEMTABL] [nchar](5) NULL,
	[OLDITEMITEM] [nchar](8) NULL,
	[OLDITEMSEQ] [nchar](2) NULL,
	[OLDTRANID] [nchar](14) NULL,
	[OLDTABLEPROG] [nchar](6) NULL,
	[OLDVALIDFLAG] [nchar](1) NULL,
	[OLDITMFRM] [int] NULL,
	[OLDITMTO] [int] NULL,
	[OLDGENAREA] [varbinary](2000) NULL,
	[OLDUSRPRF] [nchar](10) NULL,
	[OLDJOBNM] [nchar](10) NULL,
	[OLDDATIME] [datetime2](6) NULL,
	[OLDGENAREAJ] [nvarchar](4000) NULL,
	[NEWITEMPFX] [nchar](2) NULL,
	[NEWITEMCOY] [nchar](1) NULL,
	[NEWITEMTABL] [nchar](5) NULL,
	[NEWITEMITEM] [nchar](8) NULL,
	[NEWITEMSEQ] [nchar](2) NULL,
	[NEWTRANID] [nchar](14) NULL,
	[NEWTABLEPROG] [nchar](6) NULL,
	[NEWVALIDFLAG] [nchar](1) NULL,
	[NEWITMFRM] [int] NULL,
	[NEWITMTO] [int] NULL,
	[NEWGENAREA] [varbinary](2000) NULL,
	[NEWUSRPRF] [nchar](10) NULL,
	[NEWJOBNM] [nchar](10) NULL,
	[NEWDATIME] [datetime2](6) NULL,
	[NEWGENAREAJ] [nvarchar](4000) NULL,
	[USERID] [nvarchar](10) NULL, 
	[ACTION] [nvarchar](10) NULL, 
	[SYSTEMDATE] [datetime2](6) NULL,
 CONSTRAINT [PK_AUDIT_ITEMPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITEMPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITEMPFX'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITEMCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITEMCOY'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITEMTABL' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITEMTABL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITEMITEM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITEMITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITEMSEQ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITEMSEQ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDTRANID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDTRANID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDTABLEPROG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDTABLEPROG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDVALIDFLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDVALIDFLAG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITMFRM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITMFRM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDITMTO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDITMTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDGENAREA' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDGENAREA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDUSRPRF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDUSRPRF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDJOBNM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDJOBNM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDDATIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDDATIME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.OLDGENAREAJ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'OLDGENAREAJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITEMPFX' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITEMPFX'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITEMCOY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITEMCOY'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITEMTABL' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITEMTABL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITEMITEM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITEMITEM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITEMSEQ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITEMSEQ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWTRANID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWTRANID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWTABLEPROG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWTABLEPROG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWVALIDFLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWVALIDFLAG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITMFRM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITMFRM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWITMTO' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWITMTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWGENAREA' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWGENAREA'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWUSRPRF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWUSRPRF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWJOBNM' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWJOBNM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWDATIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWDATIME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.NEWGENAREAJ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'NEWGENAREAJ'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.USERID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'USERID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.ACTION' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'ACTION'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF.SYSTEMDATE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF', @level2type=N'COLUMN',@level2name=N'SYSTEMDATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'VM1DTA.AUDIT_ITEMPF' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AUDIT_ITEMPF'
GO

