SET QUOTED_IDENTIFIER ON
GO


IF EXISTS ( SELECT OBJ.* FROM sys.indexes As IX INNER JOIN sys.objects as OBJ on IX.object_id = OBJ.object_id WHERE Schema_name(Schema_ID)='VM1DTA' 
            and IX.name = 'IDX_Life_PE_ZRAEPF_10062022' and OBJ.name = 'ZRAEPF' )
	DROP INDEX [IDX_Life_PE_CHDRPF_10062022] ON [VM1DTA].[CHDRPF]
GO
CREATE NONCLUSTERED INDEX [IDX_Life_PE_ZRAEPF_10062022] ON [VM1DTA].[ZRAEPF]
(
	[CHDRNUM] ASC,
	[VALIDFLAG] ASC,
	[FLAG] ASC
)

INCLUDE ([CHDRCOY], 
		[APCAPLAMT],
		[APINTAMT],
		[APLSTCAPDATE],
		[APNXTCAPDATE],
		[APLSTINTBDTE],
		[APNXTINTBDTE],
		[PAYCURR])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO


SET QUOTED_IDENTIFIER OFF
GO