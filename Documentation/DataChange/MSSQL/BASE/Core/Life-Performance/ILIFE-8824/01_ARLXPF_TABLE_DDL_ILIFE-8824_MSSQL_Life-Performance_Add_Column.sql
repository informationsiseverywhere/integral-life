
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COWNNUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ARLXPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ARLXPF ADD COWNNUM NCHAR(8) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<COWNNUM>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ARLXPF', @level2type=N'COLUMN',@level2name=N'COWNNUM';
	END
GO