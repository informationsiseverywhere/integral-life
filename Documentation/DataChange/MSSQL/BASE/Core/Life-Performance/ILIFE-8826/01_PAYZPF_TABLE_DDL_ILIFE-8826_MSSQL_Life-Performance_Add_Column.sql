
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ZNFOPT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='PAYZPF'))       
    BEGIN
		ALTER TABLE VM1DTA.PAYZPF ADD ZNFOPT NCHAR(3) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<ZNFOPT>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PAYZPF', @level2type=N'COLUMN',@level2name=N'ZNFOPT';
	END
GO