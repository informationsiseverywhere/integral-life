IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CHDRNLGFLG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5353DATA'))       
    BEGIN
		ALTER TABLE VM1DTA.B5353DATA ADD CHDRNLGFLG NCHAR(1) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<CHDRNLGFLG>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5353DATA', @level2type=N'COLUMN',@level2name=N'CHDRNLGFLG';
	END
GO

