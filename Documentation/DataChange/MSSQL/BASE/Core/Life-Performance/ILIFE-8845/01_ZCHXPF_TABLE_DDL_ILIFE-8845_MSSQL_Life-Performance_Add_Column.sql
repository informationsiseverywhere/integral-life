
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'STATCODE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCHXPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ZCHXPF ADD STATCODE NCHAR(2) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<STATCODE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZCHXPF', @level2type=N'COLUMN',@level2name=N'STATCODE';
	END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PSTCDE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ZCHXPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ZCHXPF ADD PSTCDE NCHAR(2) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PSTCDE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ZCHXPF', @level2type=N'COLUMN',@level2name=N'PSTCDE';
	END
GO