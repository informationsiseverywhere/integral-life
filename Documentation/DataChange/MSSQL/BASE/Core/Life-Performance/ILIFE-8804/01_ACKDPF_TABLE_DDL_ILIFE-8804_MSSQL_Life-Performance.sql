-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'ACKDPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN
		CREATE TABLE [VM1DTA].[ACKDPF]
		(
			UNIQUE_NUMBER BIGINT IDENTITY(1,1) NOT NULL,
			CHDRCOY NCHAR(1) NULL,
			CHDRNUM NCHAR(8) NULL,
			DLVRMODE NCHAR(4) NULL,
			DESPDATE INT NULL,
			PACKDATE INT NULL,
			DEEMDATE INT NULL,
			NXTDTE INT NULL
		);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'acknowledgement table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Mode' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'DLVRMODE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policy Despatch Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'DESPDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policy Acknowledgement Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'PACKDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deemed Received Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'DEEMDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next Activity Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'NXTDTE';
		
	END
GO
