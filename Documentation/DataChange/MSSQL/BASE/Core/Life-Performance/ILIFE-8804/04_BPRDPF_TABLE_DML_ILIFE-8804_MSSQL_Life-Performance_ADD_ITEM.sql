IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='POLACK-CRT' AND COMPANY='2' AND BPROGRAM= 'CRTTMPF   ')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],
[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],
[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[USRPRF],[JOBNM],[DATIME]) 
values ('2','POLACK-CRT','00:00:00','23:59:59','CRTTMPF   ','                                                                                                                                                                                                                                                ',1,1,'     ','        ','    ',' ','  ',50,50,5000,'ACKDPF    ','          ','          ','PA        ','          ',100,'*DATABASE ','1','3',0,'Y','I','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.BPRDPF WHERE BPROCESNAM='BR51P' AND COMPANY='2' AND BPROGRAM= 'BR51P     ')
insert into VM1DTA.BPRDPF ([COMPANY],[BPROCESNAM],[BERLSTRTIM],[BLATSTRTIM],[BPROGRAM],[BCOMANDSTG],[BTHRDSTPRC],[BTHRDSSPRC],[BPARPPROG],[RRULE],[BAUTHCODE],[BCHCLSFLG],
[BDEFBRANCH],[BSCHEDPRTY],[BSYSJOBPTY],[BSYSJOBTIM],[BPSYSPAR01],[BPSYSPAR02],[BPSYSPAR03],[BPSYSPAR04],[BPSYSPAR05],[BCYCPERCMT],[BPRCRUNLIB],[BPRESTMETH],[BCRITLPROC],
[BMAXCYCTIM],[MULBRN],[MULBRNTP],[PRODCODE],[USRPRF],[JOBNM],[DATIME])
values ('2','BR51P','00:00:00','23:59:59','BR51P     ','                                                                                                                                                                                                                                                ',1,1,'P6671','        ','BR7O','N','  ',50,50,5000,'          ','          ','          ','PA        ','          ',100,'*DATABASE ','1','3',0,'Y','I','L','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO

IF EXISTS (	SELECT * FROM VM1DTA.BPRDPF  	
	WHERE company = '2' and BPROCESNAM like 'BR52P     ') 
update VM1DTA.BPRDPF set BPSYSPAR04='PA        ' where company = '2' and BPROCESNAM like 'BR52P     ';
GO

