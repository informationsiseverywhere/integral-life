-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'LONXPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN
		CREATE TABLE [VM1DTA].[LONXPF]
		(
			UNIQUE_NUMBER BIGINT IDENTITY(1,1) NOT NULL,
			CHDRCOY NCHAR(1) NULL,
			CHDRNUM NCHAR(8) NULL,
			LOANNUMBER INT NULL,
			LOANTYPE NCHAR(1) NULL, 
			LOANCURR NCHAR(3) NULL, 
			VALIDFLAG NCHAR(1) NULL, 
			FTRANNO INT NULL, 
			LTRANNO INT NULL, 
			LOANORIGAM [numeric](17, 2) NULL, 
			LOANSTDATE INT NULL, 
			LSTCAPLAMT [numeric](17, 2) NULL, 
			LSTCAPDATE INT NULL, 
			NXTCAPDATE INT NULL, 
			LSTINTBDTE INT NULL, 
			NXTINTBDTE INT NULL
		);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interest Bearing table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loan Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LOANNUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loan Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LOANTYPE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loan Currency' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LOANCURR';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Validflag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'First Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'FTRANNO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LTRANNO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loan Original Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LOANORIGAM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loan Start Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LOANSTDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Capitalisation Loan Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LSTCAPLAMT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Capitalisation Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LSTCAPDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next Capitalisation Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'NXTCAPDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Interest Billing Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'LSTINTBDTE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next Interest Billing Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'LONXPF', @level2type=N'COLUMN',@level2name=N'NXTINTBDTE';
		
	END
GO

