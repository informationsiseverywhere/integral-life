GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'HPADUQN' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ACKDPF'))       
    BEGIN
		ALTER TABLE VM1DTA.ACKDPF ADD HPADUQN bigint NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<HPADUQN>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ACKDPF', @level2type=N'COLUMN',@level2name=N'HPADUQN';
	END
GO