-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'ANNYREGPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
CREATE TABLE VM1DTA.ANNYREGPF(

UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL,
	CHDRNUM nchar(8) NULL,
	CHDRCOY nchar(1) NULL,
	OCCDATE int NULL,
	CNTBRANCH nchar(2) NULL,
	COWNNUM nchar(8) NULL,
	LIFCNUM nchar(8) NULL,
	CLTSEX nchar(1) NULL,
	ANNUITYPE nchar(2) NULL,
	BENEFTYPE nchar(3) NULL,
	TOTANNUFUND numeric(17, 2) NULL,
	ANNPMNTOPT nchar(2) NULL,
	ANNUPMNTTRM int NULL,
	PARTPMNTAMT numeric(17, 2) NULL,
	ANNPMNTFREQ nchar(2) NULL,
	DFRRDTRM int NULL,
	DFRRDAMT numeric(17, 2) NULL,
	ANNUPMNTAMT numeric(17, 2) NULL,
	FUNDSETUPDT int NULL,
	PMNTSTARTDT int NULL,
	DOCACCPTDT int NULL,
	FRSTPMNTDT int NULL,
	ANNIVERSDT int NULL,
	FINALPMNTDT int NULL,
	ANNPSTCDE nchar(2) NULL,
	PAYEE nchar(8) NULL,
	PMNTCURR nchar(3) NULL,
	RGPYMOP nchar(2) NULL,
	PAYEEACCNUM nchar(20) NULL,
	BANKCD nchar(4) NULL,
	BRANCHCD nchar(3) NULL,
	BANKACCDSC nchar(20) NULL,
	BNKACTYP nchar(4) NULL,
	USRPRF nchar(10) NULL,
	DATIME datetime2(6) NULL,	
 	CONSTRAINT [PK_ANNYREGPF] PRIMARY KEY CLUSTERED 
 (
       [UNIQUE_NUMBER] ASC
 )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY REGISTRATION FILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONTRACT NUMBER', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONTRACT DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'OCCDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BRANCH NUMBER', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'CNTBRANCH';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CONTRACT OWNER', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'COWNNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LIFE ASSURED', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'LIFCNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GENDER', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'CLTSEX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY TYPE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNUITYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BENEFIT TYPE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'BENEFTYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOTAL ANNUITY FUND', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'TOTANNUFUND';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY PAYMENT OPTION', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNPMNTOPT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY PAYMENT TERM', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNUPMNTTRM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PARTIAL PAYMENT AMOUNT', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'PARTPMNTAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY PAYMENT FREQUENCY', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNPMNTFREQ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DEFERRED TERM', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'DFRRDTRM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DEFERRED AMOUNT', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'DFRRDAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUITY PAYMENT AMOUNT', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNUPMNTAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FUND SETUP DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'FUNDSETUPDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYMENT START DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'PMNTSTARTDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOCUMENT ACCEPTANCE DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'DOCACCPTDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FIRST PAYMENT DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'FRSTPMNTDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNIVERSARY DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNIVERSDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FINAL PAYMENT DATE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'FINALPMNTDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ANNUAL PAYMENT STATUS', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'ANNPSTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYEE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'PAYEE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYMENT CURRENCY', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'PMNTCURR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYMENT METHOD', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'RGPYMOP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PAYEE ACCOUNT NUMBER', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'PAYEEACCNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANK CODE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'BANKCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BRANCH CODE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'BRANCHCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ACCOUNT HOLDER NAME', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'BANKACCDSC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BANK ACCOUNT TYPE', @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'BNKACTYP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER PROFILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TIMESTAMP' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYREGPF', @level2type=N'COLUMN',@level2name=N'DATIME';
END
GO

