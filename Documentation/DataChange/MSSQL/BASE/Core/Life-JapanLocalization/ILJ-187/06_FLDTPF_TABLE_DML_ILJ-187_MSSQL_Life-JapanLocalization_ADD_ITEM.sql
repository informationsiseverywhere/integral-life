SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[FLDTPF]	WHERE FDID = 'ANNUITYPE' AND LANG = 'E' )
INSERT INTO [VM1DTA].[FLDTPF] (FDID, LANG, FDTX, COLH01, COLH02, COLH03, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME) VALUES ( 'ANNUITYPE', 'E', 'ANNUITY TYPE', 'ANNUITY', 'TYPE', '              ', 'WS10', 120, 860101, 0, 'UNDERWR1  ', 'UNDERWR1', CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF  
GO
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[FLDTPF]	WHERE FDID = 'BENEFTYPE' AND LANG = 'E' )
INSERT INTO [VM1DTA].[FLDTPF] (FDID, LANG, FDTX, COLH01, COLH02, COLH03, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME) VALUES ( 'BENEFTYPE', 'E', 'BENEFIT TYPE', 'BENEFIT', 'TYPE', '              ', 'WS10', 120, 860101, 0, 'UNDERWR1  ', 'UNDERWR1', CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF  
GO