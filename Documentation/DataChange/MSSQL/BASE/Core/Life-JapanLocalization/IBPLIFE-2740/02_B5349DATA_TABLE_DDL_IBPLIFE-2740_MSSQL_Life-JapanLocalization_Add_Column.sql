SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RCOPT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5349DATA'))       
    BEGIN
		ALTER TABLE VM1DTA.B5349DATA ADD RCOPT NCHAR(1) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<RCOPT>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5349DATA', @level2type=N'COLUMN',@level2name=N'RCOPT ';
	END
GO
SET QUOTED_IDENTIFIER OFF

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'SRCEBUS' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5349DATA'))       
    BEGIN
		ALTER TABLE VM1DTA.B5349DATA ADD SRCEBUS NCHAR(2) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<SRCEBUS>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5349DATA', @level2type=N'COLUMN',@level2name=N'SRCEBUS ';
	END
GO
SET QUOTED_IDENTIFIER OFF

