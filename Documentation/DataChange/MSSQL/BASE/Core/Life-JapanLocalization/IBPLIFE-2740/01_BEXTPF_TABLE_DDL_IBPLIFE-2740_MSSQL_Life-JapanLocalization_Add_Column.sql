SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'BANKACTYPE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BEXTPF'))       
    BEGIN
		ALTER TABLE VM1DTA.BEXTPF ADD BANKACTYPE numeric(1) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<BANKACTYPE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BEXTPF', @level2type=N'COLUMN',@level2name=N'BANKACTYPE ';
	END
GO
SET QUOTED_IDENTIFIER OFF

