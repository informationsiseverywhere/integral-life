
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5AG' AND DESCPFX='IT' AND DESCITEM='2021C**' AND LANGUAGE = 'E')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','2','TH5AG','2021C**','  ','E',' ','2021','Cash 2021','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);

GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5AG' AND DESCPFX='IT' AND DESCITEM='2021C**' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','2','TH5AG','2021C**','  ','J',' ','2021',N'现金 2021','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF