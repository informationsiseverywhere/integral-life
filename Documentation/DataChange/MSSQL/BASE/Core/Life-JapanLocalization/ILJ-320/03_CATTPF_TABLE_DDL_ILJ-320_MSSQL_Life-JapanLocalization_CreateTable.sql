
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'CATTPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN


CREATE TABLE [VM1DTA].[CATTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](10) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[COVERAGE] [nchar](2) NULL,
	[RIDER] [nchar](2) NULL,
	[CLAIM] [nchar](9) NULL,
	[CLAMTYP] [nchar](2) NULL,
	[CLAMSTAT] [nchar](2) NULL,
	[TRANNO] [int] NULL,
	[TRCODE] [nchar](4) NULL,
	[CLAMAMT] [numeric](17, 2) NULL,
	[DTOFDEATH] [int] NULL,
	[CONDTE] [int] NULL,
	[CAUSEOFDTH] [nchar](4) NULL,
	[EFFDATE] [int] NULL,
	[LIFCNUM] [nchar](8) NULL,
	[SEQENUM] [nchar](3) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[DOCEFFDATE] [int] NULL,
	[FLAG] [nchar](1) NULL,
	[KPAYDATE] [int] NULL,
	[INTBASEDT] [int] NULL,
	[FULFILLTRM] [nchar](4) NULL,
	[KARIFLAG] [nchar](1) NULL,
	[INTCALFLG] [nchar](1) NULL,
	[VALIDFLAG] [nchar](1) NULL
 CONSTRAINT [PK_CATTPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pre Registration Table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'LIFE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Joint Life' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'JLIFE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coverage' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rider' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'RIDER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Number ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CLAIM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Type ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPFF', @level2type=N'COLUMN',@level2name=N'CLAMTYP';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Status ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CLAMSTAT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number  ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Code ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'TRCODE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Amount ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CLAMAMT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date OF Death ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'DTOFDEATH';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CONDTE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cause of Death ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'CAUSEOFDTH';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Death ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lifcnum ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'LIFCNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'SEQENUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job Name ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'DATIME';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document Effective death ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'DOCEFFDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FLAG ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'FLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'KPAYDATE ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'KPAYDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INTBASEDT ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'INTBASEDT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FULFILLTRM ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'FULFILLTRM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'KARIFLAG ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'KARIFLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INTCALFLG ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'INTCALFLG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VALID FLAG' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CATTPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
	END
GO


