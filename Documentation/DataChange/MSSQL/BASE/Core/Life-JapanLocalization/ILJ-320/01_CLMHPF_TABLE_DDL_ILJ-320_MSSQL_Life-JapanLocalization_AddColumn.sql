IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CLAMVAL') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD CLAMVAL NUMERIC(17, 2) DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Claim Value' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'CLAMVAL';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CLAIM') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD CLAIM nchar(9);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Claim No' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'CLAIM';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('KCAUSE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD KCAUSE nchar(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Cause of death for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'KCAUSE';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('ZASSESOR') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD ZASSESOR nchar(4);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Cause of death for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'ZASSESOR';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('ZCLAIMER') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD ZCLAIMER nchar(4);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'ZCLAIMER';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('WUEPAMT') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD WUEPAMT nchar(4) DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'WUEPAMT';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CONDTE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ADD CONDTE int DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMHPF', @level2type=N'COLUMN',@level2name=N'CONDTE';
END
GO