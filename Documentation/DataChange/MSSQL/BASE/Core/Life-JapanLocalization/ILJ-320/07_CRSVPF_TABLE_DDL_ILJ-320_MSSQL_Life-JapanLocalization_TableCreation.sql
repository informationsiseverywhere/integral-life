
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'CRSVPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN
		CREATE TABLE VM1DTA.CRSVPF(
		
UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL,
WCLMCOY nchar(1) NULL,
WCLMPFX nchar(2) NULL,			
CHDRNUM nchar(12) NULL,
TRANNO nchar(5) NULL,	
TRCODE nchar(4) NULL,	
CONDTE int NULL,
CLAIM nchar(9) NULL,
BALO numeric(13, 2) NOT NULL DEFAULT '0',
WPAID numeric(13, 2) NOT NULL DEFAULT '0',
WPRCL nchar(4) NULL,	
WREQAMT numeric(13, 2) NOT NULL DEFAULT '0',
WRSCD nchar(2) NULL,	
TERMID nchar(4) NULL,	
TRDT int NULL,
TRTM int NULL,
VALIDFLAG nchar(1) NULL,
USER_T int NULL,
USRPRF nchar(10) NULL,
JOBNM nchar(10) NULL,
DATIME DATETIME2(6) NULL
	
 CONSTRAINT [PK_CRSVPF] PRIMARY KEY CLUSTERED 
([UNIQUE_NUMBER] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Reserve Table' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'WCLMCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'TRCODE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Contact' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'CONDTE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'CLAIM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Actual Value ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'BALO';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Amount Paid' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'WPAID';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registered payment amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'WREQAMT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Terminal ID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'TERMID';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of processing' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'TRDT';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of processing' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'TRTM';
		
		
	END
GO