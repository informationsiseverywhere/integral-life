IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('RIIND') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMDPF ADD RIIND nchar(1);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for Death Claims' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMDPF', @level2type=N'COLUMN',@level2name=N'RIIND';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('SACSCODE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMDPF ADD SACSCODE char(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMDPF', @level2type=N'COLUMN',@level2name=N'SACSCODE';
END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('SACSTYPE') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMDPF ADD SACSTYPE char(2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMDPF', @level2type=N'COLUMN',@level2name=N'SACSTYPE';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('GLMAP') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMDPF ADD GLMAP nchar(10);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMDPF', @level2type=N'COLUMN',@level2name=N'GLMAP';
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('CLAIM') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMDPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMDPF ADD CLAIM nchar(9);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Claim No' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'CLMDPF', @level2type=N'COLUMN',@level2name=N'CLAIM';
END
GO