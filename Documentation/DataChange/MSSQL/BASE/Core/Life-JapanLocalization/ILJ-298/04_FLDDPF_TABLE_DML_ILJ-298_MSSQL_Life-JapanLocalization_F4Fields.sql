﻿
GO
IF NOT EXISTS (	Select * from VM1DTA.FLDDPF WHERE FDID = 'KANJINAME')

insert INTO VM1DTA.FLDDPF(FDID, NMPG, TYPE_T, EDIT, LENF, DECP, WNTP, WNST, TABW, TCOY, ERRCD, PROG01, PROG02, PROG03, PROG04, CFID, TERMID, 
USER_T, TRDT, TRTM, ADDF01, ADDF02, ADDF03, ADDF04, CURRFDID, ALLOCLEN, DBDATIME, SFDATIME, DBCSCAP, USRPRF, JOBNM, DATIME, TYPE) 
VALUES('KANJINAME ', 'KANJINAME               ', 'A', ' ', 30, 0, 'W', 'G', '     ', ' ', '    ', 'PJL35', ' ', ' ', ' ', ' ',' ', 0, 
0, 0, ' ', ' ', ' ', ' ', ' ', 0, getdate(), getdate(),'N', 'LHAMMOND', 'DSP14', getdate(), ' ');
GO


GO
IF NOT EXISTS (	Select * from VM1DTA.FLDDPF WHERE FDID = 'ANTISOCNO')
insert INTO VM1DTA.FLDDPF(FDID, NMPG, TYPE_T, EDIT, LENF, DECP, WNTP, WNST, TABW, TCOY, ERRCD, PROG01, PROG02, PROG03, PROG04, CFID, TERMID, 
USER_T, TRDT, TRTM, ADDF01, ADDF02, ADDF03, ADDF04, CURRFDID, ALLOCLEN, DBDATIME, SFDATIME, DBCSCAP, USRPRF, JOBNM, DATIME, TYPE) 
VALUES('ANTISOCNO ', 'ANTISOCNO               ', 'A', ' ', 8, 0, 'W', 'G', '     ', ' ', '    ', 'PJL35', ' ', ' ', ' ', ' ',' ', 0, 
0, 0, ' ', ' ', ' ', ' ', ' ', 0, getdate(), getdate(),'N', 'LHAMMOND', 'DSP14', getdate(), ' ');
GO