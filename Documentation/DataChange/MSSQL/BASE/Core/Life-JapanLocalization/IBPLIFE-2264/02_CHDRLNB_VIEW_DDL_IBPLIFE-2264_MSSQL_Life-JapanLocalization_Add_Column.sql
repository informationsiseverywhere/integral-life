
SET XACT_ABORT ON;
GO

begin tran

IF EXISTS ( SELECT * FROM sys.views where name = 'CHDRLNB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].CHDRLNB
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[VM1DTA].[CHDRLNB]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [VM1DTA].[CHDRLNB] (UNIQUE_NUMBER, CHDRPFX, CHDRCOY, CHDRNUM, SERVUNIT, CNTTYPE, POLINC, POLSUM, NXTSFX, TRANNO, TRANID, VALIDFLAG, CURRFROM, CURRTO, AVLISU, STATCODE, STATDATE, STATTRAN, PSTCDE, PSTDAT, PSTTRN, TRANLUSED, OCCDATE, CCDATE, REPTYPE, REPNUM, COWNPFX, COWNCOY, COWNNUM, JOWNNUM, PAYRPFX, PAYRCOY, PAYRNUM, DESPPFX, DESPCOY, DESPNUM, ASGNPFX, ASGNCOY, ASGNNUM, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, CNTCURR, BILLCURR, PAYPLAN, ACCTMETH, BILLFREQ, BILLCHNL, COLLCHNL, BILLDAY, BILLMONTH, BILLCD, BTDATE, PTDATE, SINSTFROM, SINSTTO, SINSTAMT01, SINSTAMT02, SINSTAMT03, SINSTAMT04, SINSTAMT05, SINSTAMT06, INSTFROM, INSTTO, INSTTOT01, INSTTOT02, INSTTOT03, INSTTOT04, INSTTOT05, INSTTOT06, FACTHOUS, BANKKEY, BANKACCKEY, GRUPKEY, MEMBSEL, CAMPAIGN, SRCEBUS, STCA, STCB, STCC, STCD, STCE, REG, STMDTE, MANDREF, MPLNUM, USRPRF, JOBNM, DATIME, ZMANDREF, REQNTYPE, PAYCLT,SCHMNO, NLGFLG,COWNNUM2,COWNNUM3,COWNNUM4,COWNNUM5,CUSTREFNUM,CONCOMMFLG,ONWEROCCUP,RCOPT )

AS

SELECT UNIQUE_NUMBER,
            CHDRPFX,
            CHDRCOY,
            CHDRNUM,
            SERVUNIT,
            CNTTYPE,
            POLINC,
            POLSUM,
            NXTSFX,
            TRANNO,
            TRANID,
            VALIDFLAG,
            CURRFROM,
            CURRTO,
            AVLISU,
            STATCODE,
            STATDATE,
            STATTRAN,
            PSTCDE,
            PSTDAT,
            PSTTRN,
            TRANLUSED,
            OCCDATE,
            CCDATE,
            REPTYPE,
            REPNUM,
            COWNPFX,
            COWNCOY,
            COWNNUM,
            JOWNNUM,
            PAYRPFX,
            PAYRCOY,
            PAYRNUM,
            DESPPFX,
            DESPCOY,
            DESPNUM,
            ASGNPFX,
            ASGNCOY,
            ASGNNUM,
            CNTBRANCH,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            CNTCURR,
            BILLCURR,
            PAYPLAN,
            ACCTMETH,
            BILLFREQ,
            BILLCHNL,
            COLLCHNL,
            BILLDAY,
            BILLMONTH,
            BILLCD,
            BTDATE,
            PTDATE,
            SINSTFROM,
            SINSTTO,
            SINSTAMT01,
            SINSTAMT02,
            SINSTAMT03,
            SINSTAMT04,
            SINSTAMT05,
            SINSTAMT06,
            INSTFROM,
            INSTTO,
            INSTTOT01,
            INSTTOT02,
            INSTTOT03,
            INSTTOT04,
            INSTTOT05,
            INSTTOT06,
            FACTHOUS,
            BANKKEY,
            BANKACCKEY,
            GRUPKEY,
            MEMBSEL,
            CAMPAIGN,
            SRCEBUS,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            REG,
            STMDTE,
            MANDREF,
            MPLNUM,
            USRPRF,
            JOBNM,
            DATIME,
			ZMANDREF,
			REQNTYPE,
			PAYCLT,
			SCHMNO,
			NLGFLG,
			COWNNUM2,
			COWNNUM3,
			COWNNUM4,
			COWNNUM5,
			CUSTREFNUM,
			CONCOMMFLG, 
			ONWEROCCUP,
			RCOPT
FROM VM1DTA.CHDRPF WHERE SERVUNIT = ''LP'' '

GO
commit tran