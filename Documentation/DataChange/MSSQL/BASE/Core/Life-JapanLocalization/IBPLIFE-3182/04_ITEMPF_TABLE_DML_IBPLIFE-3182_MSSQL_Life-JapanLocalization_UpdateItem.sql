SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM VM1DTA.ITEMPF WHERE ITEMTABL = 'T5679'  AND ITEMITEM LIKE 'BR6W    ' AND ITEMCOY = '2')
UPDATE VM1DTA.ITEMPF SET GENAREA = CONVERT(varbinary(2000), '                        PSUWSI                                                                                                                                          NTNTNTNT    NTNTNTNTNT                                                                                ') WHERE ITEMTABL = 'T5679'  AND ITEMITEM LIKE 'BR6W    ' AND ITEMCOY = '2';
GO

SET QUOTED_IDENTIFIER OFF