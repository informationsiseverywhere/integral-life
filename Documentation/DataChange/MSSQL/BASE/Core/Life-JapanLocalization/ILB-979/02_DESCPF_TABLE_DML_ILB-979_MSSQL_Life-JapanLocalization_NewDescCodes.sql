 
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='        ' AND DESCCOY='2' AND LANGUAGE='E' AND DESCPFX='HE')
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TJL76','        ','  ','E','','          ','Agent Hierarchy Register Class','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='1' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','1','  ','E','','Add','Add','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='2' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','2','  ','E','','Modify','Modify','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='3' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','3','  ','E','','Delete','Delete','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='4' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','4','  ','E','','Restore','Restoration','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='        ' AND DESCCOY='2' AND LANGUAGE='E' AND DESCPFX='HE')
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TJL77','        ','  ','E','','          ','Approve Status in Hierarchy','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='1' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL77','1','  ','E','','Approve','Approved','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='0' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL77','0','  ','E','','Unapprove','Unapproved','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL57X' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL57X','  ','E','','JL57X','JL57X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL74X' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL74X','  ','E','','JL74X','JL74X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL75X' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL75X','  ','E','','JL75X','JL75X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1675' AND DESCITEM='TAXMJL61' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAXMJL61','  ','E','','TAXMJL61','TAXMJL61','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1675' AND DESCITEM='TAXJJL74' AND DESCCOY='2' AND LANGUAGE='E' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAXJJL74','  ','E','','TAXJJL74','TAXJJL74','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO



IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='        ' AND DESCCOY='2' AND LANGUAGE='J' AND DESCPFX='HE')
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TJL76','        ','  ','J','','          ',N'募集階層登録区分','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='1' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','1','  ','J','',N'登録',N'登録','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='2' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','2','  ','J','',N'変更',N'変更','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='3' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','3','  ','J','',N'削除',N'削除','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL76' AND DESCITEM='4' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL76','4','  ','J','',N'復旧',N'復旧','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='        ' AND DESCCOY='2' AND LANGUAGE='J' AND DESCPFX='HE')
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TJL77','        ','  ','J','','          ',N'承認ステータス','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='1' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL77','1','  ','J','',N'承認済',N'承認済','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'TJL77' AND DESCITEM='0' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TJL77','0','  ','J','',N'未承認',N'未承認','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL57X' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL57X','  ','J','','JL57X','JL57X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL74X' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL74X','  ','J','','JL74X','JL74X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1661' AND DESCITEM='JL75X' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1661','JL75X','  ','J','','JL75X','JL75X','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1675' AND DESCITEM='TAXMJL61' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAXMJL61','  ','J','','TAXMJL61','TAXMJL61','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO


IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1675' AND DESCITEM='TAXJJL74' AND DESCCOY='2' AND LANGUAGE='J' )
insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','T1675','TAXJJL74','  ','J','','TAXJJL74','TAXJJL74','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);	
GO



SET QUOTED_IDENTIFIER OFF  
GO
